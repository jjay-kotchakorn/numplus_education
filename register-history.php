<?php 
include_once "lib/lib.php";
include_once "./share/datatype.php";
include_once "share/course.php";
global $db;
$member_info = $_SESSION["login"]["info"];
$member_id = $member_info['member_id'];
$apay = datatype(" and a.active='T'", "pay_status", true);
$arr_pay = array();
foreach ($apay as $key => $value) {
	$arr_pay[$value["pay_status_id"]] = $value["name"];
}
?>
	<div class="container">
		<div class="headerblue">ประวัติการสมัคร</div>
		<div class="navigation">
			<div class="step" id="coursetype_select">
			<?php 
			$q = "select coursetype_id, name from coursetype where active='T'";
			$rs = $db->get($q);
			$current_id = ($_GET["current_id"]) ? $_GET["current_id"] : 1;
			if($rs){
			 	foreach ($rs as $key => $v) {
			 		$active = ($current_id==$v["coursetype_id"]) ? "active" : "";
			 		echo '<div class="level '.$active.'" data="'.$v["coursetype_id"].'">'.$v["name"].'</div>';
			 	}
			}

			$q = "select a.coursetype_id, a.register_id
				  from register a 
				  where a.active='T'
				  	and a.pay_status<>9
				  	and a.member_id=$member_id
			";
			$array_course = $db->get($q);
			$array_id = array();
			if($array_course){
				foreach ($array_course as $v) {
					$array_id[$v["coursetype_id"]] .= ",".$v["register_id"];
				}
				$ids = trim($array_id[$current_id], ",");
			}
			 ?>
			</div>
			<?php 
				$s = date("Y")+543 - 4;
				$e = date("Y")+543;
				$date_start = $_GET["date_start"];
				$date_stop = $_GET["date_stop"];
				if ($dateStart || $dateStop) {
					if (!$dateStart && $dateStop)
						$dateStart = $dateStop;
					if (!$dateStop && $dateStart)
						$dateStop = $dateStart;
					$t = $dateStart;
					if ($dateStart > $dateStop) {
						$dateStart = $dateStop;
						$dateStop = $t;
					}
				}
			 ?>
			<div class="findbydate">
            <table id="tb-regis-history-findbydate" >

            </table>
		</div>
	</div>
	<table width="100%" align="center" cellpadding="10" cellspacing="0" class="history">
		<tbody>
			<tr class="grayheader">
				<td style="width:120px;">วัน</td>
				<td style="width:100px;">เวลา</td>
				<td width="20%">สถานที่</td>
				<td width="25%">หลักสูตร</td>
				<td width="8%">วันที่ลงทะเบียน</td>
				<td>ประเภท<br>การชำระเงิน</td>
				<td>พิมพ์ใบชำระเงิน</td>
				<td>สถานะ<br>การชำระเงิน</td>
				<?php if($current_id==1){?>
					<td>ผลการอบรม</td>
				<?php } else if($current_id==2){?>
					<td>ผลการสอบ</td>
					<td>ผลสอบหมดอายุ</td>
				<?php } else if($current_id==3){?>
					<!-- <td>ผลการอบรม + สอบ</td> -->
					<td>ผลการอบรม</td>
					<td>ผลการสอบ</td>
					<td>ผลสอบหมดอายุ</td>
				<?php } else if($current_id==4){?>
					<td>ผลการอบรมต่ออายุ</td>
					<!-- <td>จำนวนชั่วโมงที่นับ</td> -->
				<?php } ?>
				<td>หมายเหตุ</td>
			</tr>
			<?php 
			if($ids){
				$con = " and a.register_id in ($ids) and a.member_id=$member_id and a.active='T'";
				if($date_start){
					$con_stert = ($date_start-543)."-01-01 00:00:00";
					$con_stop = ($date_stop-543)."-12-31 23:59:00";
					$con .= " and (a.rectime>='$con_stert' and a.rectime<='$con_stop')";
				}
				$array_data = get_register($con);

				// d($array_data);

				$i = 1;
				if($array_data){
					foreach ($array_data as $key => $v) {
						$register_id = $v["register_id"];
						$sCourse_id = $v["course_id"];
						$con = " and a.course_id in ($sCourse_id)";
						$r = get_course($con);
						$title = "";
						$parent_id = 0;
						$price = 0;
						// d($v); die();
						
						foreach ($r as $key => $row) {
							// d($row); die();
							$course_id = $row['course_id'];
							$section_id = $row['section_id'];
							$code = $row['code'];
							$title .= $row['title'].",";
							$set_time = $row['set_time'];
							$life_time = $row['life_time'];
							$status = $row['status']; 
							$parent_id = (int)$row['parent_id'];
						}
						//$q = "select a.course_detail_id from register_course_detail a where a.register_id={$register_id} and a.course_id in ($sCourse_id)";
						$q = "select * from register_course_detail a where a.register_id={$register_id}";
						// echo $q;

						$cd = $db->get($q);
						$parent_title = "";
						if($parent_id>0){
							$q = "select title from course where active='T' and course_id=$parent_id";
							$t = $db->data($q);
							$parent_title = "<strong>{$t}</strong>";	
						}

						$title = trim($title, ",");
						$price = $v["course_price"];
						$discount = $v["course_discount"];

						$display_date = "";
						$display_time = "";
						$display_address = "";
						$display_result = "";
						$display_te_results = "";

						// d($cd);
						// echo "<hr>";

						if($cd){   					 
							if ( count($cd)>1 ) {
								
								$title = "";
								$display_result = "";
								$display_te_results = "";
								// d($cd);
								// die();
								foreach ($cd as $kk => $vv) {
									$row = get_course_detail(" and a.course_detail_id={$vv["course_detail_id"]}");
									// d($row);
									if($row) $row = $row[0];
									$course_detail_id = $row['course_detail_id'];
									$course_id = $row['course_id'];
									$day = $row['day'].".";
									$date = $row['date'];
									$time = $row['time'];
									$display_date .= $day." ".revert_date($date).",";
									$display_time .= $time.",";
									$display_address .= $row['address_detail']." ".$row['address'].",";

									$cos_info = get_course("", $course_id);
									$cos_info = $cos_info[0];
									$title .= " - ".$cos_info['title'].",";

									// d($cos_info); //die();

									if($course_id>0 && $course_detail_id>0 && $v["register_id"]>0){
										$q = "SELECT a.*
												,b.name as result_name 
												,c.name as te_result_name 
											from register_course_detail a 
											left join register_result b on a.register_result_id=b.register_result_id
											left join register_result c on a.register_te_result_id=c.register_result_id
										  	where a.active='T' 
										  		and a.register_id={$v["register_id"]} 
										  		and a.course_id={$course_id} 
										  		and a.course_detail_id={$course_detail_id}
										;"; 
										// echo $q."<br>";
										$results = $db->rows($q); 
										// d($results);
										if( !empty($results) ){

											if ( empty($results["register_result_id"])&&empty($results["register_te_result_id"]) ) {
												$q = "SELECT result
														, te_results 
													FROM register 
													WHERE register_id={$v["register_id"]}
												";
												$reg_info = $db->rows($q);
												// d($reg_info);
												if ( !empty($reg_info["result"]) || !empty($reg_info["te_results"]) ) {
													if ( !empty($reg_info["result"]) ) {
														$rs = 0;
														$result_id = $reg_info["result"];
														$q = "SELECT name FROM register_result WHERE register_result_id={$result_id}
														";
														$rs = $db->data($q);
														$rs = !empty($rs) ? $rs : "";
														$display_result .= $rs.",";
													}//end if
													if ( !empty($reg_info["te_results"]) ) {
														$rs = 0;
														$result_id = $reg_info["te_results"];
														$q = "SELECT name FROM register_result WHERE register_result_id={$result_id}
														";
														$rs = $db->data($q);
														$rs = !empty($rs) ? $rs : "";
														$display_te_results .= $rs.",";
													}//end if										
												}//end if
											}else{
												$rs = 0;
												$result_id = $results["register_result_id"];
												$q = "SELECT name FROM register_result WHERE register_result_id={$result_id}";
												$rs = $db->data($q);
												$rs = !empty($rs) ? $rs : "";
												$display_result .= $rs.",";

												$rs = 0;
												$result_id = $results["register_te_result_id"];
												$q = "SELECT name FROM register_result WHERE register_result_id={$result_id}";
												$rs = $db->data($q);
												$rs = !empty($rs) ? $rs : "";
												$display_te_results .= $rs.",";								
											}//en else

										}else{
											$display_te_results .= "0,";						
											$display_result .= "0,";								
										}//end else
									}//end if
								}//end loop $vv	
								

								$display_date = str_replace(",", "<br><br>", $display_date);
								$display_time = str_replace(",", "<br><br>", $display_time);
								$display_address = str_replace(",", "<br><br>", $display_address);
								$title = str_replace(",", "<br><br>", $title);
								$display_result = str_replace(",", "<br><br>", $display_result);
								$display_te_results = str_replace(",", "<br><br>", $display_te_results);
/*
								echo $display_date;
								echo $display_time;
								echo $display_address;
								echo $title;
								echo $display_result;
								echo $display_te_results;
*/
								//die();

							}else{
								// d($cd);
								// echo "<hr>";
								foreach ($cd as $kk => $vv) {
								
									$row = get_course_detail(" and a.course_detail_id={$vv["course_detail_id"]}");
									if($row) $row = $row[0];

									// $course_detail_id = $row['course_detail_id'];
									// $course_id = $row['course_id'];

									$course_detail_id = $vv['course_detail_id'];
									$course_id = $vv['course_id'];

									$day = $row['day'].".";
									$date = $row['date'];
									$time = $row['time'];
									$display_date .= $day." ".revert_date($date).",";
									$display_time .= $time.",";
									$display_address .= $row['address_detail']." ".$row['address'].",";

									if($course_id> 0 && $course_detail_id > 0 && $v["register_id"]>0){
										$q = "select a.*
												,b.name as result_name 
												from register_course_detail a left join register_result b on a.register_result_id=b.register_result_id
											  	where a.register_id={$v["register_id"]} and a.course_id={$course_id} and a.course_detail_id={$course_detail_id}"; 

										// echo $q;

										$t = $db->rows($q); 

										// d($t);
										// echo "<hr>";

										if($t){
											$rs = 0;
											$result_id = $t["register_result_id"];
											$q = "SELECT name FROM register_result WHERE register_result_id={$result_id}
											";
											$rs = $db->data($q);
											$rs = !empty($rs) ? $rs : "";
											$display_result .= $rs.",";		
											$rs = 0;
											$result_id = $t["register_te_result_id"];
											$q = "SELECT name FROM register_result WHERE register_result_id={$result_id}
											";
											$rs = $db->data($q);
											$rs = !empty($rs) ? $rs : "";
											$display_te_results .= $rs.",";									
										}else{
											$display_te_results .= "0,";						
											$display_result .= "0,";								
										}//end else
									}//end if

								}//end loop $vv
								
								$display_date = str_replace(",", "<br><br>", $display_date);
								$display_time = str_replace(",", "<br><br>", $display_time);
								$display_address = str_replace(",", "<br><br>", $display_address);
								$title = str_replace(",", "<br><br>", $title);
								$display_result = str_replace(",", "<br><br>", $display_result);
								$display_te_results = str_replace(",", "<br><br>", $display_te_results);

							}//end else	

						}else{
							$row = get_course_detail(" and a.course_detail_id={$v["course_detail_id"]}");
							if($row) $row = $row[0];
							$course_detail_id = $row['course_detail_id'];
							$course_id = $row['course_id'];
							$day = $row['day'];
							if ($day) {
								$day = $day.".";
							}
							$date = $row['date'];
							$time = $row['time'];
							$display_date .= " ".$day." ".revert_date($date).",";
							$display_time .= $time.",";
							$display_address .= $row['address']." ".$row['address_detail'].",";

							$rs = 0;
							$result_id = $v["result"];
							$q = "SELECT name FROM register_result WHERE register_result_id={$result_id}
							";
							$rs = $db->data($q);
							$rs = !empty($rs) ? $rs : "";
							$display_result .= $rs.",";	

							$rs = 0;
							$result_id = $v["te_results"];
							$q = "SELECT name FROM register_result WHERE register_result_id={$result_id}
							";
							$rs = $db->data($q);
							$rs = !empty($rs) ? $rs : "";
							$display_te_results .= $rs.",";

							// $display_result .= $v["result"].",";									
							// $display_te_results .= $v["te_results"].",";							
						}//end else


						$id = $v["register_id"];
						$content = "";
						$display_content = "";
						$pay = $v["pay_id"];
						$status = $v["pay_status"];
						$pay_icon = "print2.jpg";
						$pay_type = "";
						$print_style = "";
						if($v["pay"]=="at_ati"){
							$pay_type = "ชำระเงินสดผ่าน ATI";
						}else if($v["pay"]=="at_asco"){
							$pay_type = "ชำระเงินสดผ่าน ASCO";
						}else if($v["pay"]=="paysbuy"){
							$pay_type = "ชำระเงินช่องทางอื่นๆ (paysbuy)";							
							//$print_style = ($status==1) ? "" : "visibility:hidden";
							$pay_icon = "paysbuy-small.png";
						}else if($v["pay"]=="bill_payment"){
							$pay_type = "ชำระเงินผ่าน Bill-payment";
						}else if($v["pay"]=="walkin"){
							if($v["pay_status"]==5 || $v["pay_status"]==6)
								$pay_type = $arr_pay[$v["pay_status"]];
							else	
								$pay_type = "ชำระเงินสดผ่าน ATI";
						}else if($v["pay"]=="importfile"){
							if($v["pay_status"]==5 || $v["pay_status"]==6)
								$pay_type = $arr_pay[$v["pay_status"]];
							else	
								$pay_type = "ชำระเงินสดผ่าน ATI";

						}else if($v["pay"]=="mpay"){	
							$pay_type = "ชำระเงินผ่าน mPAY";
							$pay_icon = "mpay_icon.png";

							$q = "SELECT payment_mpay_register_list_id 
							FROM payment_mpay_register_list
							WHERE register_id={$id}
						";
						$reprint_url = $db->data($q);
 						}

						$status_pay = $arr_pay[$status];
						if($status!="1"){
							//$print_style = "visibility:hidden";
						}


						$paytype_id = $v["pay"];

						$curr_date = date('Y-m-d');
						$reg_date = $v["date"];
						$reg_date = explode(" ", $reg_date);
						$cal_date = strtotime($curr_date) - strtotime($reg_date[0]);

						$expi_date = $v["expire_date"];
						$expi_date = explode(" ", $expi_date);
						$expi_date = $expi_date[0]." 23:59:59";
						//$expi_date = explode(" ", $expi_date);
						$curr_date = date('Y-m-d H:i:s');
						//$curr_date = date('Y-m-d 23:59:59');
						$chk_expi_date = (strtotime($curr_date) - strtotime($expi_date)) ;

						//echo $cal_date;
						//echo $chk_expi_date."<br>";
						if ( ($cal_date==0) && ($paytype_id=="paysbuy") && $status==1 ) {
							$td_stment = "<td style='{$print_style}'><a href='#' onclick='reprint(".$id.",\"{$paytype_id}\")'><img src='images/{$pay_icon}'/></a></td>";
						}elseif ( ($v["pay"]=="bill_payment") && ($chk_expi_date<=0) && $status==1 ) {
							$td_stment = "<td style='{$print_style}'><a href='#' onclick='reprint(".$id.",\"{$paytype_id}\")'><img src='images/{$pay_icon}'/></a></td>";
						}elseif ( ($v["pay"]=="at_ati") && ($chk_expi_date<=0) && $status==1 ) {
							$td_stment = "<td style='{$print_style}'><a href='#' onclick='reprint(".$id.",\"{$paytype_id}\")'><img src='images/{$pay_icon}'/></a></td>";
						}elseif ( ($v["pay"]=="mpay") && ($chk_expi_date<=0) && $status==1 ) {
							$td_stment = "<td style='{$print_style}'><a href='#' onclick='reprint(".$id.",\"{$paytype_id}\")'><img style='width:30px;'; src='images/{$pay_icon}'/></a></td>";
						}else{
							$td_stment = "<td></td>";
						}//end else

						// var_dump($display_te_results);
						// echo "<hr>";
						// d($display_date);
/*
						if ( count($display_date)>1 ) {
							$tmp = "";
							foreach ($display_date as $key => $dd) {
								$tmp .= $dd.",";
							}//end loop $dd
							$display_date = $tmp;
						}else{

						}//end else
*/

						$arr_display_date = explode(",", trim($display_date, ","));
						$arr_display_time = explode(",", trim($display_time, ","));
						$arr_display_address = explode(",", trim($display_address, ","));
						$arr_title  = explode(",", trim($title, ","));
						$arr_display_results = explode(",", trim($display_result, ","));			
						$arr_display_te_results = explode(",", trim($display_te_results, ","));		

						$content = "";
						$content_have_parent = "";


/*
						d($arr_display_date);
						d($arr_display_time);
						d($arr_display_address);
						d($arr_title);	
						d($arr_display_results);
						d($arr_display_te_results);
*/


						foreach ($arr_display_date as $key => $display_date) {
							$result = "";
							$te_result = "";
							// echo $arr_title[$key]."--->".$arr_display_results[$key]."<br>";
							// echo $arr_title[$key]."--->".$arr_display_te_results[$key]."<br>";

							$display_time = $arr_display_time[$key];
							$display_address = $arr_display_address[$key];
							$title = $arr_title[$key];
							
							$result = trim($arr_display_results[$key]);
							$result = ($result=="0") ? "" : $result;
							$te_result = trim($arr_display_te_results[$key]);
							$te_result = ($te_result=="0") ? "" : $te_result;



/*
							$result_id = count($arr_display_results)<2 ? $arr_display_results[$key] : $display_result;
							$te_results_id = count($arr_display_te_results)<2 ? $arr_display_te_results[$key] : $display_te_results;
*/
							// var_dump($result_id);
							// var_dump($te_results_id);
							// echo "<hr>";
							// die();

/*
							$result_id = rtrim($result_id, ",");
							$result_ids = explode(",", $result_id);							
							$count_rs_id = count($result_ids);
						*/	
							// $te_results_id = rtrim($te_results_id, ",");
							// $te_results_ids = explode(",", $te_results_id);							
							// $count_te_rs_id = count($te_results_ids);
/*
							if ( $count_rs_id<2 ) {								
								
								$q = "SELECT name FROM register_result WHERE register_result_id={$result_id}
								";
								$result = $db->data($q);
							}else{
								// $display_date = "<br>".$display_date;
								// $display_time = "<br>".$display_time;
								// $display_address = "<br>".$display_address;

								foreach ($result_ids as $key => $r) {
									// echo $r."+<br>";
									if ( !empty($r) ) {
										$q = "SELECT name FROM register_result WHERE register_result_id={$r}
										";
										$rs = $db->data($q);
										$result .= $rs."<br><br>";									
									}else{
										$result .= "-<br><br>";
									}//end else									
								}//end loop $r
							}//end else

							$te_results_id = rtrim($result_id, ",");
							$te_results_ids = explode(",", $te_results_id);
							$count_te_rs_ids = count($te_results_ids);

							if ( $count_te_rs_ids<2 ) {
								$q = "SELECT name FROM register_result WHERE register_result_id={$te_results_id}
								";
								$te_result = $db->data($q);
							}else{			
								// d($te_results_ids);	die();			
								foreach ($te_results_ids as $key => $te_results_id) {
									// echo $te_results_id."+<br>";
									if ( !empty($te_results_id) ) {
										$q = "SELECT name FROM register_result WHERE register_result_id={$te_results_id}
										";
										$te_result_name = $db->data($q);
										$te_result .= $te_result_name."<br><br>";	
									}else{
										$te_result .= "-<br><br>";
									}//end else									
								}//end loop $r
								// echo $te_result; die();
							}//end else
					
*/							
/*
							//get resul & te_result
							$q = "SELECT a.*
									, b.name AS result_name
									, c.name AS te_result_name
								FROM register_course_detail a 
								LEFT JOIN register_result b ON b.register_result_id=a.register_result_id
								LEFT JOIN register_result c ON c.register_result_id=a.register_te_result_id
								WHERE a.active='T'
									AND a.register_id={$register_id} 
									AND a.course_id IN ($sCourse_id)
							";
							// echo "$q";
							$reg_cd = $db->rows($q);

							if ( empty($reg_cd) ) {
								$q = "SELECT
										a.result,
										a.result_date,
										a.result_by,
										a.te_results,
										b.name AS result_name,	
										c.name AS te_result_name							
									FROM register a
									LEFT JOIN register_result b ON b.register_result_id=a.result
									LEFT JOIN register_result c ON c.register_result_id=a.te_results
									WHERE a.register_id = {$register_id}
								";
								$reg_cd = $db->rows($q);
							}//end if

						// d($reg_cd); //die();
							if ( !empty($reg_cd["result_name"]) || !empty($reg_cd["te_result_name"]) ) {
								$result = "";
								$te_result = "";
								if ( !empty($reg_cd["result_name"]) ) {
									$result = $reg_cd["result_name"];
								}//end if
								if ( !empty($reg_cd["te_result_name"]) ) {
									$te_result = $reg_cd["te_result_name"];
								}//end if

							}//end if

*/	

							$td = "";
							if($current_id==1){
								$td = "<td>{$te_result}</td><td></td>";
								$col_td = 2;
							}else if($current_id==2){
/*								
								$result = trim($result);
								if ( preg_match("^([0-9]){2,3})(.[0-9]{2})?$([0-9]{1})?$", $result) ) {
									$rs = 0;
									$result_id = $result;
									$q = "SELECT name FROM register_result WHERE register_result_id={$result_id}
									";
									$result = $db->data($q);
								}//end if
*/
								$td = "<td>{$result}</td><td></td><td></td>";
								$col_td = 3;
							}else if($current_id==3){
								$td = "<td>{$te_result}</td><td>{$result}</td><td></td><td></td>";
								$col_td = 4;
							}else if($current_id==4){
								//$td = "<td>{result}</td><td>{$set_time}</td>";
								$td = "<td>{$te_result}</td><td></td>";
								$col_td = 2;
							}
							$content = "
								<tr>
									<td style='text-align:left;padding:0px 10px 0px 10px;max-width:110px;'>".$display_date."</td>
									<td style='text-align:left;padding:0px 5px 0px 5px;max-width:100px;'>".$display_time."</td>
									<td style='text-align:left;padding:0px 10px 0px 10px;max-width:300px;'>".$display_address."</td>
									<td style='text-align:left;padding:0px 10px 0px 10px;max-width:300px;'>{$parent_title}".$title."</td>
									<td>".revert_date($v['date'])."</td>
									<td>".$pay_type."</td>"
									.$td_stment.
									"<td>".$status_pay."</td>"
									.$td."
								</tr>";	
						
							$col_span = 8+$col_td;
							$content_have_parent = "
								<tr>
									 <td colspan='".$col_span."'>".$parent_title."</td>
								</tr>
								<tr>
									<td style='text-align:left;padding:0px 10px 0px 10px;max-width:110px;'>".$display_date."</td>
									<td style='text-align:left;padding:0px 5px 0px 5px;max-width:100px;'>".$display_time."</td>
									<td style='text-align:left;padding:0px 10px 0px 10px;max-width:300px;'>".$display_address."</td>
									<td style='text-align:left;padding:0px 10px 0px 10px;max-width:300px;'>".$title."</td>
									<td>".revert_date($v['date'])."</td>
									<td>".$pay_type."</td>"
									.$td_stment.
									"<td>".$status_pay."</td>"
									.$td."
								</tr>";	

								if ( !empty($parent_title) ) {
									$display_content .= $content_have_parent;
								}else{
									$display_content .= $content;									
								}//ense else
							}//end loop $display_date

							echo $display_content;

						$i++; 
						// die();
						// echo "<hr>";
					}//end loop $v
				}//end if
			}?>
		</tbody>
	</table>
	<table width="100%" cellpadding="10">
		<tbody>
			<tr>
				<td>
					<a href="#" onClick="redirect_home();"><div class="buttonw" style="float:left;">กลับสู่หน้าหลัก</div> </a>
				</td>
				<td style="text-align:right;" valign="middle">
					<?php /*?><strong>ข้อมูลแสดงเมื่อ</strong>  <?php echo revert_date(date('Y-m-d')) . ' ' . date('H:i:s'); ?><?php */?> </td>
				</tr>
			</tbody>
		</table>
	</div>

<script type="text/javascript">
   $(document).ready(function() {
   		$("#coursetype_select .level").click(function(event) {
   			var id = $(this).attr("data");
   			window.open('index.php?p=register-history&current_id='+id,'_self');
   		});
      $("#find-date").click(function(event) {
			var current_id = "<?php echo $current_id; ?>";
			var date_start = $("select[name=date_start]").val();
			var date_stop = $("select[name=date_stop]").val();
			var url = 'index.php?p=register-history&current_id='+current_id;
				url = url+'&date_start='+date_start;
				url = url+'&date_stop='+date_stop;
			redirect(url);     
      });

	});
   	function reprint(id, pay_type){
   		/*if (pay_type=='paysbuy') {
   			var confirm_btn = alert("สำหรับช่องทางชำระแบบเงินสด\nท่านสามารถ Print แบบฟอร์มชำระเงินย้อนหลังได้ที่ E-Mail ที่ส่งให้ท่านจาก Paysbuy เท่านั้น");
   		};*/
   		
		var url;
		switch(pay_type){
			case 'paysbuy': 
				url = "./paysbuy/paynow.php?register_id="+id;
				//url = "#";
				window.open(url,'_blank');			
				break;
			case 'bill_payment': 
				url = 'invoice.php?register_id='+id;
				window.open(url,'_blank');
				break;
			case 'at_ati': 
				url = 'invoice-ati.php?register_id='+id;
				window.open(url,'_blank');
				break;
			case 'at_asco': 
				url = 'invoice-ati.php?register_id='+id;
				window.open(url,'_blank');
				break;
			case 'mpay': 
			    url = 'invoice-ati.php?register_id='+id+"&pay_type=mpay";
				window.open(url,'_blank');
				break;
		}    
		//window.open(url,'_blank');
	}

function redirect_home(){
	window.open('index.php?p=select_s1','_self');
}
</script>