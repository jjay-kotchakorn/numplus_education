<?php
/**
 * This example shows making an SMTP connection with authentication.
 */

//SMTP needs accurate times, and the PHP time zone MUST be set
//This should be done in your php.ini, but this is how to do it if you don't have access to that
//date_default_timezone_set('Etc/UTC');

require 'PHPMailerAutoload.php';

//Create a new PHPMailer instance
$mail = new PHPMailer;
//Tell PHPMailer to use SMTP
$mail->isSMTP();
//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
//$mail->SMTPDebug = 2; 
//Ask for HTML-friendly debug output
$topic =$_POST['topic'];
$detail = $_POST['detail'];
$Form_name = $_POST['Form_name'];
$tel = $_POST['tel'];
$school = $_POST['school'];

$email = $_POST['email'];

$Body = "<html>
<head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
    
</head>
<body>
<style type='text/css'>
    .contrainer{margin: 10px 0 0 10px; }
    .contrainer tr { font-size: 16px; font-family: 'Tahoma'; line-height: 8px;}
    .title p { font-size: 18px ; font-family:'Tahoma';  line-height:10px; color: red ;  } 
</style>
    <div class='contrainer'>
    <div class='title'>
    <p>This is Automatic Email</p></div>
        <td>
            <tr>
                <tr>
                    <p>หัวข้อ		:	$topic</p>
	            </tr>
            <tr>
                <tr>
                     <p>รายละเอียด	:	$detail</p>
	            </tr>
            <tr>
                <tr>
                    <p>ชื่อผู้ส่ง	:	$Form_name</p>
	            </tr>
            <tr>
                <tr>
                    <p>เบอร์โทรศัพท์	:	$tel</p>
	            </tr>
            <tr>
                <tr>
                    <p>โรงเรียน	:	$school</p>
	            </tr>
            <tr>
                <tr>
                    <p>Email	:	$email</p>
	            </tr>
        </td>
    </div>
</body>
</html>";
//$Body = htmlspecialchars($Body, ENT_QUOTES);

$mail->CharSet = "utf-8";
$mail->ContentType = "text/html";

$mail->Debugoutput = 'html'; 
//Set the hostname of the mail server
$mail->Host = "mail.numplus.in.th"; 
//Set the SMTP port number - likely to be 25, 465 or 587
$mail->Port = 25; 
//Whether to use SMTP authentication
$mail->SMTPAuth = true; 
//Username to use for SMTP authentication
$mail->Username = "thictyouth@numplus.in.th"; 
//Password to use for SMTP authentication
$mail->Password = "Test@1234"; //Set who the message is to be sent from
$mail->setFrom('thictyouth@numplus.in.th', 'Thailand ICT Youth Challenge 2014'); 
//Set an alternative reply-to address
// $mail->addReplyTo('thictyouth@numplus.in.th', 'Koom2'); 
//Set who the message is to be sent to
$mail->addAddress('pornchai@numplus.com', 'You'); 
//Set Sent to CC
$mail->addCC('chunwatchara@numplus.com'); 
//Set the subject line
$mail->Subject = 'Contact of Thailand ICT Youth Challenge 2014';   
//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
$mail->msgHTML($Body); 

//Replace the plain text body with one created manually
// $mail->AltBody = 'This is a Automatic Mail'; 
//Attach an image file


//send the message, check for errors
if (!$mail->send()) {
    echo "Mailer Error: " ;
} else {
    echo "Message sent!";
}
