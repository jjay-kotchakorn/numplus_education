<?php 
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";
include_once "./share/course.php";
global $db;

?>

<!DOCTYPE html>
<html lang="en">
<?php include ('include/header.php'); ?>
<body>
  <!-- nav -->
  <?php include ('include/top-menu.php'); ?>

  <form action='' method="POST">
   <div class="contrainer">
    <div class="mainsite">
      <div class="label">
        <p>สถาบันฝึกอบรม สมาคมบริษัทหลักทรัพย์ไทย ASCO Training Institute (ATI)</p>
      </div>
      <div class="select-zone">
        <div class="row">
          <div class="bottom-center-zone" style="width:47%;margin-top:80px;text-align: center;">
          <label style="color:#000; font-size: 24px;font-weight: bold;font-weight: bold;">สถาบันฝึกอบรม สมาคมบริษัทหลักทรัพย์ไทย ASCO Training Institute (ATI)</label>
          <br><br>
          <label style="color:#000; font-size: 24px;font-weight: bold;">การลงทะเบียนของท่านเกิดข้อผิดพลาด อาจจะเกิดจากระบบ Internet ของท่านไม่เสถียร</label>
          <br><br>
          <label style="color:#000; font-size: 24px;font-weight: bold;">หากท่านต้องการทำรายการใหม่อีกครั้งกรุณาคลิ๊ก <a href="#" onclick="goToindex();">ที่นี่ </a>หรือติดต่อผู้ดูแลระบบ</label>
        </div>
        <!-- <div class="bottom" id="action2" onclick="goToindex();"><a href="#">กลับไปทำรายการเพิ่มเติม</a></div>                  -->
      </div>
    </div>
  </div>
</div>

<!-- <form id='formBack' name='formBack' method='post' action='' enctype='application/x-www-form-urlencoded'>
  <input name="sec_id" type="hidden" value="<?php //echo $sec_id; ?>" id="sec_id" />
  <input name="sec_action" type="hidden" value="<?php //echo $sec_action; ?>" id="sec_action" />
  <input name="ch_submit_section" type="hidden" value="yes" id="ch_submit_section" />
</form> -->

<!-- footer -->
<?php //include ('include/footer.php') ?>

</body>
</html>

<script type="text/javascript">

  $(document).ready(function(){


  });

  function goBack(){
    $('#formBack').attr('action', 'index.php?p=new-condition-one');
    $('#formBack').attr('target', '_self');
    $('#formBack').submit();
  }
  function goToindex(){
    window.open('index.php?p=main','_self');
  }
</script>

<style>
.billinfo {
  padding: 20px;
  padding-top: 20px;
  padding-bottom: 20px;
  background-color: #FFF;
  border: 2px solid #dcdcdc;
  border-radius: 10px;
  font-size: 24px;
}
</style>