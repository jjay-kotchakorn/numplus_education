<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" type="text/css" />
<link rel="stylesheet" href="bootstrap/css/bootstrap.css" type="text/css" />
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<style>
@font-face {
	font-family: 'circularregular';
	src: url('font/circular-webfont.eot');
	src: url('font/circular-webfont.eot?#iefix') format('embedded-opentype'),  url('font/circular-webfont.woff2') format('woff2'),  url('font/circular-webfont.woff') format('woff'),  url('font/circular-webfont.ttf') format('truetype'),  url('font/circular-webfont.svg#circularregular') format('svg');
	font-weight: normal;
	font-style: normal;
}

body {
margin-top: 100px;
font-family: "circularregular";	
}
	.billinfo {
    padding: 20px;
    padding-top: 20px;
    padding-bottom: 20px;
    background-color: #FFF;
    border: 1px solid #dcdcdc;
    border-radius: 10px;
    font-size: 24px;
   }

 .img-responsive{
   	display: block;
max-width: 100px;
height: auto;
margin: auto;
   }

   .buttonb{
	background: linear-gradient(to bottom, #6580b5 0%, #6580b5 56%, #6580b5 56%, #4264a1 56%, #4264a1 100%);
	margin-top:20px;
	margin-bottom:20px;
	float:right;
	border-radius:5px;
	color:#FFF;
	padding:10px;
	font-size:20px;
	}



</style>

</head>
<body>

<form action='' method="POST">
 <div class="container">
 	 <div class="row">
        

    <div class="col-sm-12">
   <img class="img-responsive" src="img001.png" alt="Another alt text">
</div>

      <!-- <div class="col-sm-12" style="text-align: center;"> -->
            <!-- <h3>MPay ขอขอบคุณ</h3> -->
        <!-- </div>  -->
</div>
<br>
<br>

    <div class="billinfo">
    <div class="control-group">
      <label class="control-label">สวัสดี คุณ กชกร กู้สวัสดิ์,</label>
      <div class="form-group">
        <p>ขอขอบคุณที่คุณเลือกใช้บริการ MPay เราขอยืนยันว่าคำสั่งซื้อหมายเลข 359141235 ของคุณได้รับการจัดส่งเรียบร้อยแล้ว เราหวังว่าคุณจะมีความสุขกับการใช้สินค้าชิ้นใหม่ของคุณจาก MPay</p>
      </div>
      <hr>
        <label class="control-label">Hi กชกร กู้สวัสดิ์,</label>
      <div class="form-group">
        <p>Thank you for choosing MPay! We are pleased to inform you that your order 359141235 has been shipped in full. We hope you enjoy your purchase on MPay.</p>
      </div>
    </div>
  </div>
 <!-- Button -->
  <div class="buttonb"> 
    กลับหน้าแรก
</div>
</form>
</body>
</html>