<?php
/* 
$url = "http://demo.paysbuy.com/api_paynow/api_paynow.asmx?WSDL";
$urlRedirect = "http://demo.paysbuy.com/paynow.aspx";

$psbID = "0101635373";
$username = "atiasco_merchant@paysbuy.com";
$secureCode = "4D6B0DF687DAFC3BE1948504B4BB8B6C";
$paypal_amt = "";
$curr_type = "TH";
$com = "";
$method = "2"; //6 for Counter Service
$language = "T";


//Change to your URL
//$resp_front_url = "https://register.ati-asco.org/home/index.php?p=new-step-five"; 
//$resp_back_url = "https://register.ati-asco.org/home/paysbuy/result_back.php";

$resp_front_url = "http://devsite.numplus.com/index.php?p=new-step-five"; 
$resp_back_url = "http://devsite.numplus.com/paysbuy/result_back.php";

//Optional data
$opt_fix_redirect = "1"; //autu-redirect to Merchant
$opt_fix_method = ""; // Show only CS
 */

// $url_payment_dev = "https://chillchill.ais.co.th:8002/AISMPAYPartnerInterface/InterfaceService?";

$url_payment_dev = "https://sandbox.mpay.co.th/mpay-sandbox-api/InterfaceService?";
$url_payment_prod = "https://www.mpay.co.th/AISMPAYPartnerInterface/InterfaceService";

// $url_response_mpay = "http://mpay-ati.numplus.com/mpay/mpay-inquiry.php";
$url_response_dev = "http://devsite.numplus.com/mpay/mpay-inquiry.php";
$url_response_prepro = "http://ati-pp.numplus.com/mpay/mpay-inquiry.php";
$url_response_prod = "https://register.ati-asco.org/mpay/mpay-inquiry.php";

// $url_end_point_dev = "http://devsite.numplus.com";
// $url_end_point_prod = "https://register.ati-asco.org";

$url_for_payment = $url_payment_dev;
$url_for_response = $url_response_dev;
//$url_end_point = $url_end_point_dev;

$merchantId = "21474";
$sid = "dae67d342acacd094b8926f8165fc3cc6b17184b5e15dce72077de6db4b07a8d";
$SecretKey = "SaltTEPS";
$currency = "THB";

$projectCode = "TEPS";
$command = "";
$redirectUrl = $url_for_response;
$orderId = "";

/*Amount of purchased order (Original product amount) and last 2 digits are decimal.*/ 
$purchaseAmt = "";

/*Method of payment
Values as belows:
- '1' = mPAY
- '2' = PS
- '3' = ATM
- '4' = Credit Card
- '6' = mPay Wallet.*/
$paymentMethod = "1";

$productDesc = "";
$ref1 = ""; 
$ref2 = "";
$ref3 = "";
$ref4 = "";

/*Application Authentication SHA256 (sid+merchantId+orderId+ amount+SecretKey[request])*/
$integrityStr = "";

/*Y= send sms to buyer
N= don’t send sms to buyer
(default value is “N”)*/
$smsFlag = "N";

/*Buyer’s mobile number
Or the recipient that the message will be sent to.
Format : 66xxxxxxxxx (international format)*/ 
$smsMobile = "";

/*Buyer’s mobile number for payment
Format : 66xxxxxxxxx (international format)*/
$mobileNo = "";

/*Reference for Creditcard Number*/
$tokenKey = "";

/*Order duration (unit is minute)
e.g. order will expire after created 7 days, so this parameter is "10080" (the value from : 7 days *24 hours *60 minutes)*/
$orderExpire = "10080";

?>