<?php
// ini_set('default_charset', 'utf-8');
header('Content-type: text/html; charset=utf-8');
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/course.php";
include("lib/nusoap.php");
include("lib/config.php");
//require "./vendor/autoload.php";
global $db;
$debug = true;
$debug = false; 
//d($db);
$datetime_now = date('Y-m-d H:i:s');
$date_now = date('Y-m-d');

function convertToUTF8($str) {
    $enc = mb_detect_encoding($str);

    if ($enc && $enc != 'UTF-8') {
        return iconv($enc, 'UTF-8', $str);
    } else {
        return $str;
        // return $enc;
    }//end else
}//end func

//179659 179643
//var_dump($debug); die();
//for debug
if ( $debug ) {	
	if ( !empty($_GET["register_id"]) ) {
		echo "!! Open mode maintenance !!";
		die();
	}//end if
	$register_id = 179643;
	//$payment_mpay_register_list_id = 47;
}//end if

// $curl = new Curl();
// d($curl); die();

if ( !empty($_GET["register_id"]) || $debug  ) {
	$register_id = !empty($_GET["register_id"]) ? (int)trim($_GET["register_id"]) : $register_id ;
   
	$reg_info = get_register("", $register_id);
	$reg_info = $reg_info[0];

	// d($reg_info); die();
	$q = "SELECT a.register_id
			, b.name AS coursetype_name
			, c.name AS section_name
			, a.course_id
			, a.coursetype_id
			, a.section_id
		FROM register AS a
		LEFT JOIN coursetype AS b ON b.coursetype_id=a.coursetype_id
		LEFT JOIN section AS c ON c.section_id=a.section_id
		WHERE a.active='T'
			AND a.register_id={$register_id}
	";
	$rs = $db->rows($q);
	$coursetype_name = $rs["coursetype_name"];
	$section_name = $rs["section_name"];
	$coursetype_id = (int)$rs["coursetype_id"];
	$section_id = (int)$rs["section_id"];

	$course_ids = $rs["course_id"];
	$course_ids = explode(",", $course_ids);
	$course_id = $course_ids[0];

	// echo $course_id;
	// $course_id = 10;
/*
	$cos_info = get_course("", $course_id);
	$course_name = $cos_info[0]["title"];
*/	
	if ( $section_id==1 && $coursetype_id==2 ) {
		$course_name = "Examination Fee";
	}else{
		$course_name = "Training Fee";
	}//end else
	// echo $course_name."<br>";
	$course_name_len = strlen($course_name);
	// echo $course_name_len."<br>";
	if ( $course_name_len>50 ) {
		// $course_name = substr($course_name, 0, 49);
		$course_name = mb_substr($course_name,0,49, "UTF-8");
		// $course_name = iconv_substr($course_name, 0,49, "UTF-8");
	}//end if
	// echo $course_name."<br>";
	// $productDesc = $section_name." (".$coursetype_name.")";
	$course_name = iconv('utf-8', 'tis-620', $course_name);
	$course_name = convertToUTF8($course_name);
	$course_name = urlencode($course_name);
	$productDesc = $course_name;
	$ref1 = $productDesc;

	$orderId = $register_id;
	$member_id = $reg_info["member_id"];
	$purchaseAmt = $reg_info["course_price"] - $reg_info["course_discount"];
	$purchaseAmt = $purchaseAmt."00";
	$paymentMethod = $reg_info["pay_method"];

	$expire_date = $reg_info["expire_date"];
	$expire_date = explode(" ", $expire_date);
	$expire_date = $expire_date[0];
	$expire_date = strtotime($expire_date);
	$date_now = strtotime($date_now);

	$orderExpire = ($expire_date-$date_now)/(60*60*24);
	$orderExpire = $orderExpire*24*60;

	// d($reg_info); //die();
	// $orderId = rand ( 00001 , 99999 );
	// $purchaseAmt = rand ( 100 , 99999 );
	// $orderExpire = "10080";

	$args = array();
	$args["projectCode"] = $projectCode;
	$args["command"] = "RequestOrderTepsApi";
	$args["sid"] = $sid;
	$args["redirectUrl"] = $redirectUrl;
	$args["merchantId"] = $merchantId;
	$args["orderId"] = $orderId;
	$args["purchaseAmt"] = $purchaseAmt;
	$args["currency"] = $currency;
	$args["paymentMethod"] = $paymentMethod;
	$args["smsMobile"] = $smsMobile;
	$args["mobileNo"] = $mobileNo;
	$args["smsFlag"] = $smsFlag;
	$args["tokenKey"] = $tokenKey;
	$args["orderExpire"] = $orderExpire;
	$args["projectCode"] = $projectCode;
	$args["productDesc"] = $productDesc;
	$args["ref1"] = $ref1;
	$args["ref2"] = $ref2;
	$args["ref3"] = $ref3;
	$args["ref4"] = $ref4;
	$args["ref5"] = $ref5;
	$integrityStr = $sid.$merchantId.$orderId.$purchaseAmt.$SecretKey;
	$integrityStr = hash('sha256', $integrityStr);
	$args["integrityStr"] = $integrityStr;

	// d($args);  die();

	// $url = "https://sandbox.mpay.co.th/mpay-sandbox-api/InterfaceService?";

	/*
	$params = 'projectCode=TEPS&command=RequestOrderTepsApi&sid=dae67d342acacd094b8926f8165fc3cc6b17184b5e15dce72077de6db4b07a8d&redirectUrl=http://mpay-ati.numplus.com/info.php&merchantId=21474&orderId=5464&purchaseAmt=99900&currency=THB&paymentMethod=&productDesc=&smsMobile=&mobileNo=&smsFlag=N&tokenKey=&orderExpire=720&ref1=&ref2=&ref3=&ref4=&ref5=&integrityStr=513d1699b468acc877980602bfc94d7dc46d4b48b75333625557a4e9384f0444';
	*/
	$params = "";
	$str_txt = '';
	foreach ($args as $key => $v) {
		 $params .= $key . '='.$v.'&'; 
	}//end loop $v

	// echo $params; die();
	//echo $url_for_payment."?".$params;
	$str_txt = $url_for_payment."?".$params;
 	write_log("pay-now", $str_txt, "a", "../mpay/log/" );


 	// d($curl);
 	$url = $url_for_payment;
	$data = $args;

/*
	// use key 'http' even if you send the request to https://...
	$options = array(
	    'http' => array(
	        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
	        'method'  => 'POST',
	        'content' => http_build_query($data)
	    )
	);


	$options=array("ssl"=>array(
		"verify_peer"=>false,
		"verify_peer_name"=>false),
		"http"=>array(
			'timeout' => 60, 
			'user_agent' => 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.9) Gecko/20071025 Firefox/3.0.0.1'
		)
	);
*/



/*
	$context  = stream_context_create($options);
	$output = file_get_contents($url, false, $context);

	
	var_dump($context);
	echo "<hr>";
	var_dump($output);
	echo "<hr>";

	$xml=simplexml_load_string($output);
	var_dump($xml);
	echo "<hr>";
	d($xml);

    die();
*/

    
	$ch = curl_init(); 

	curl_setopt($ch,CURLOPT_URL,$url_for_payment);

	curl_setopt($ch,CURLOPT_POSTFIELDS,$params);

	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	curl_setopt($ch,CURLOPT_HEADER, false);

	curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 60);

	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

	curl_setopt($ch, CURLOPT_ENCODING, "UTF-8");

	// curl_setopt($ch, CURLOPT_SSLVERSION, 3);
	// curl_setopt($ch, CURLOPT_SSL_CIPHER_LIST, 'SSLv3');

	$output=curl_exec($ch);
	$error_msg = curl_error($ch);
	$info = curl_getinfo($ch);  

	curl_close($ch);

	$q = "SELECT payment_mpay_register_list_id 
		FROM payment_mpay_register_list 
		WHERE register_id={$register_id}
	";
	$payment_mpay_register_list_id = $db->data($q);

	$args = array();
	$args["table"] = "payment_mpay_register_list";
	$args["id"] = $payment_mpay_register_list_id;
	$args["curl_url_for_payment"] = base64_encode($url_for_payment);
	$args["curl_params"] = base64_encode($params);
	$args["rectime"] = $datetime_now;

	$db->set($args);
	unset($args);
	
	// if ( !$output ) {
	// 	echo date("Y-m-d H:i:s");
	// 	echo "<hr>";
	// 	echo "Status: "; var_dump($output); echo "เกิดข้อผิดพลาดในการเชื่อมต่อกับ mPAY กรุณาแจ้งผู้ดูแลระบบ";
	// 	echo "<hr>";
	// 	echo "ERROR msg: "; var_dump($error_msg);
	// 	die();
	// }//end if

	?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" type="text/css" />
	<link rel="stylesheet" href="bootstrap/css/bootstrap.css" type="text/css" />
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script> 
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<style>
@font-face {
	font-family: 'circularregular';
	src: url('font/circular-webfont.eot');
	src: url('font/circular-webfont.eot?#iefix') format('embedded-opentype'),  url('font/circular-webfont.woff2') format('woff2'),  url('font/circular-webfont.woff') format('woff'),  url('font/circular-webfont.ttf') format('truetype'),  url('font/circular-webfont.svg#circularregular') format('svg');
	font-weight: normal;
	font-style: normal;
}

body {
margin-top: 100px;
font-family: "circularregular";	
}
	.billinfo {
    padding: 20px;
    padding-top: 20px;
    padding-bottom: 20px;
    background-color: #FFF;
    border: 1px solid #dcdcdc;
    border-radius: 10px;
    font-size: 24px;
   }

 .img-responsive{
   	display: block;
	max-width: 100px;
	height: auto;
	margin: auto;
   }

.img-mpay{
   	display: block;
	/**height: 500px; **/
	width: 1000px;
	height: auto;
	margin: auto;
	border: 1px solid #000;
   }

   .buttonb{
	/*background: linear-gradient(to bottom, #6580b5 0%, #6580b5 56%, #6580b5 56%, #4264a1 56%, #4264a1 100%);*/
	background: linear-gradient(to bottom, #abc22b 0%, #a3ba29 56%, #93ad28 56%, #8da729 56%, #84a12a 100%);
	margin-top: 35px;
	margin-bottom:20px;
	float:right;
	border-radius:5px;
	color:#FFF;
	padding:10px;
	font-size:20px;
	}

</style>

<!-- 
	<script>
	function myFunction() {
	    alert("Page is loaded");
	}
	</script>
	 -->
</head>
<body>

<form action='' method="POST">
 <div class="container">
 	 <div class="row">
	    <div class="col-sm-12">
		   <img class="img-responsive" src="img001.png" alt="Another alt text">
		</div>
	      <!-- <div class="col-sm-12" style="text-align: center;"> -->
	            <!-- <h3>MPay ขอขอบคุณ</h3> -->
	        <!-- </div>  -->
	</div>
	<br>
	<br>
<?php
	if ( !$output ) {
?>
    <div class="billinfo">
	    <div class="control-group">
	      <label class="control-label"><?php echo date("Y-m-d H:i:s");?></label>
	      <div class="form-group">
	        <p><?php echo "เกิดข้อผิดพลาดในการเชื่อมต่อกับ mPAY กรุณาแจ้งผู้ดูแลระบบ";?></p>
	        <p><?php //echo "Status: ".var_dump($output);?></p>
	        <p><?php echo "ERROR msg: "; var_dump($error_msg);?></p>
	      </div>
	      <!-- Button -->
	      <div class="form-group">
	      	<div class="buttonb"> 
	      		<a href="#" onclick="goToindex();" style="color: #fff;">กลับหน้าแรก</a>
	      	</div>
	      </div>
	      <!--<hr>
	         <label class="control-label">วิธีการชำระ</label>
	      <div class="form-group">
	        <p>สามารถไปที่ เมนูข้อมูลส่วนตัว เมนูย่อยประวัติการสมัครและผลการอบรม/สอบ เพื่อทำการคลิกเลือกใบเสร็จเพื่อทำการชำระย้อนหลังได้ค่ะ</p> 
	        <br>
		   <img class="img-mpay" src="mpay_pay.jpg" alt="Another alt text">
	      </div> -->
	   	</div>
  	</div>

<script>
	function goToindex(){
		window.open('../index.php?p=main','_self');
	}
</script>

<?php		
	die();
	}//end if
?>
 <!-- Button -->
<!--  
 	<div class="buttonb"> 
   	กลับหน้าแรก
	</div>
-->
</form>
</body>
</html>


	<?php
	
	// d($db);
	// read xml output

	if ( !empty($output) ) {
		// $xml=simplexml_load_string($output) or die("Error: Cannot create object");    
		$xml=simplexml_load_string($output) or die("error");    
/*
		// print output;
		echo "<br>";
		echo 'status:'.$xml->status . "<br>";
		echo 'respCode:'.$xml->respCode . "<br>";
		echo 'respDesc:'.$xml->respDesc . "<br>";
		echo 'saleId:'.$xml->saleId. "<br>";
		echo 'endPointUrl:'.urldecode($xml->endPointUrl). "<br>";
*/		
	}//end if
	
	$send_status = 'error';
	if($output && $xml->respCode == "0000" ){

		$send_status = 'success';
	}
	elseif ($output && $xml->respCode !="0000" ) {
		$error_msg = $output;	
	}

	$str_txt = "register_id={$register_id}|".$params."|send_status={$send_status}|"."error_msg={$error_msg}|";
 	write_log("pay-now", $str_txt, "a", "../mpay/log/" );

	// die();

	$q = "SELECT payment_mpay_register_list_id 
		FROM payment_mpay_register_list 
		WHERE register_id={$register_id}
	";
	$payment_mpay_register_list_id = $db->data($q);
	// d($db);
	if ( $output && $xml->respCode == "0000" ) {
		$xml_end_point_url = urldecode($xml->endPointUrl);

		$args = array();
		$args["table"] = "payment_mpay_register_list";
		$args["id"] = $payment_mpay_register_list_id;
		$args["xml_status"] = $xml->status;
		$args["xml_resp_code"] = $xml->respCode;
		$args["xml_resp_desc"] = $xml->respDesc;
		$args["xml_sale_id"] = $xml->saleId;
		$args["xml_end_point_url"] = $xml_end_point_url;
		$args["rectime"] = $datetime_now;

		$db->set($args);
		// var_dump($db->set($args, true, true));
		// d($args); die();

		$_SESSION["login"]["mpay"]["register_id"] = $register_id;
		$_SESSION["login"]["mpay"]["payment_mpay_register_list_id"] = $payment_mpay_register_list_id;
		unset($args);

		header( "Location: {$xml_end_point_url}" );
	}else{
		$args = array();
		$args["table"] = "payment_mpay_register_list";
		$args["id"] = $payment_mpay_register_list_id;
		$args["remark"] = $error_msg;
		$args["rectime"] = $datetime_now;

		$db->set($args);
		unset($args);

		echo date("Y-m-d H:i:s");
		echo "<hr>";
		echo "Status: "; var_dump($output);
		echo "<hr>";
		echo "ERROR msg: "; var_dump($error_msg);
	}//end else

}//end if

?>

<?php 
/*
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	<a href="<?php echo urldecode($xml->endPointUrl);?>" target="_blank">Link</a>
</body>
</html>
*/
?>