<?php
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/course.php";
include_once "pay-status-sendmail-func.php";
include("lib/nusoap.php");
include("lib/config.php");
global $db;
$datetime_now = date('Y-m-d H:i:s');
$debug = true;
$debug = false;

$result_status = false;

function write_log_in_local($file_name="", $msg="", $write_mode="a"){
	$str = $msg;	
	$date_now = date('Y-m-d H:i:s');
	$date_log = date('Y-m-d');
	$log_name = "./log/".$file_name."_".$date_log.".log";
	$file = fopen($log_name, $write_mode);
	$str_txt = $date_now."|".$str."\r\n";
	fwrite($file, $str_txt);
	fclose($file);
}//end func

function convert_amount($amount=""){
	if (empty($amount)) { 
		$amount = 0;
		return $amount; 
	}//end if
	$satang = substr($amount, -2);
	$bath = substr($amount, 0, -2);
	$amount = $bath.".".$satang;
	return $amount;
}//end func

if ( !empty($_POST) ) {
// if ( !empty($_GET) ) {
	$str_txt = "";
	$params = "";
	foreach ($_POST as $key => $v) {
	// foreach ($_GET as $key => $v) {
		// $data = trim($v[$key]);
		$data = trim($v);
		$params .= "{$key}={$data}&";
		$str_txt .= "{$key}={$data}|";
	}//end loop $v
	//write_log_in_local("api-socket-ctrl-POST", $str_txt);
	// write_log_in_local("api-socket-ctrl", $str_txt);
//}//end if

// d($_SERVER);
// d($_SERVER); die();

$REQUEST_METHOD = trim($_SERVER["REQUEST_METHOD"]);
$QUERY_STRING = $_SERVER["QUERY_STRING"];

// write_log_in_local("pay-now", $str_txt);
$data = NULL;
// $params = array();
//if ( $REQUEST_METHOD == "POST" ) {
// if ( $REQUEST_METHOD == "GET" ) {
	// die("Not access !!");
	// $params = $QUERY_STRING;
	$params = trim($params, "&");
	unset($QUERY_STRING);
	$curl_params = $params;
	$params = explode("&", $params);
	$str_txt = "";

	// d($params);
	$a_data = array();
	
	if ( !empty($params) ) {
		
		foreach ($params as $key => $v) {
			$tmp = "";
			$tmp = trim($v);
			$tmp = explode("=", $tmp);

			$key_name = trim($tmp[0]);
			$data = trim($tmp[1]);

			$str_txt .= "{$key_name}={$data}|";
			// d($tmp);
			// echo $tmp."<br>";
			// echo $v."<br>";

			$a_data[$key_name] = $data;
		}//end loop $v

		write_log_in_local("api-socket-ctrl", $str_txt);

	}//end if


	/*
	demo data
	status=S|respCode=0000|respDesc=Success|tranId=4854083145|saleId=663558|orderId=228116|currency=THB|exchangeRate=100000|purchaseAmt=1000|amount=1000|incCustomerFee=0|excCustomerFee=1500|paymentStatus=SUCCESS|paymentCode=0080937403|orderExpireDate=20180517095745|custEmail=|shipName=|shipAddress=|shipProvince=|shipZip=|shipCountry=THA|remark1=|remark2=|integrity=b7698b54d72593011d66d6e6403e97430d1dabeca6faf23a15cd255dee508289|custo
	merId=null|creditCardNo=null|

	
	[status] => S
    [respCode] => 0000
    [respDesc] => Success
    [tranId] => null
    [saleId] => 663558
    [orderId] => 228116
    [currency] => THB
    [exchangeRate] => 100000
    [purchaseAmt] => 1000
    [amount] => 1000
    [incCustomerFee] => 0
    [excCustomerFee] => 1500
    [paymentStatus] => PENDING
    [paymentCode] => 0080937403
    [orderExpireDate] => 20180517095745
    [custEmail] => 
    [shipName] => 
    [shipAddress] => 
    [shipProvince] => 
    [shipZip] => 
    [shipCountry] => THA
    [remark1] => 
    [remark2] => 
    [integrity] => b7698b54d72593011d66d6e6403e97430d1dabeca6faf23a15cd255dee508289
    [customerId] => null
	

	*/

	$xml_result_status = strtolower($a_data["status"]);
	$xml_result_code = $a_data["respCode"];
	$xml_result_payment_status = strtolower($a_data["paymentStatus"]);

	if ( !empty($a_data) ) {
		
		$register_id = (int)$a_data["orderId"];
		$xml_sale_id = (int)$a_data["saleId"];

		$q = "SELECT payment_mpay_register_list_id 
			FROM payment_mpay_register_list
			WHERE active='T'
				AND register_id={$register_id}
				AND xml_sale_id={$xml_sale_id}
		";
		$payment_mpay_register_list_id = $db->data($q);



		$args = array();
		$args["table"] = "payment_mpay_register_list";
		if ( !empty($payment_mpay_register_list_id) ) {
			$args["id"] = (int)$payment_mpay_register_list_id;
		}//end if
		// $args["xml_status"] = $a_data["status"];
		// $args["xml_resp_code"] = $a_data["respCode"];
		// $args["xml_resp_desc"] = $a_data["respDesc"];
		$args["xml_result_status"] = $a_data["status"];
		$args["xml_result_code"] = $a_data["respCode"];
		$args["xml_result_desc"] = $a_data["respDesc"];
		$args["xml_result_payment_status"] = $a_data["paymentStatus"];
		$args["xml_payment_code"] = $a_data["paymentCode"];
		$args["xml_order_expire_date"] = $a_data["orderExpireDate"];
		$args["xml_amount"] = convert_amount($a_data["amount"]);
		$args["xml_inc_customer_fee"] = convert_amount($a_data["incCustomerFee"]);
		$args["xml_exc_customer_fee"] = convert_amount($a_data["excCustomerFee"]);
		$args["xml_purchase_amt"] = convert_amount($a_data["purchaseAmt"]);
		$args["curl_params"] = base64_encode($curl_params);
		$args["rectime"] = $datetime_now;
		
		// d($args);
		$db->set($args);


		if ( $xml_result_status=="s" && $xml_result_code=="0000" && $xml_result_payment_status=="success" ) {
			
			// d($a_data);

			$register_id = (!empty($register_id)) ? $register_id : (int)$a_data["orderId"];

			$arr_tmp = array();
			$q = "SELECT coursetype_id, section_id from register where register_id=$register_id";
			$v = $db->rows($q);
				$section_id = (int)$v["section_id"];
				$coursetype_id = (int)$v["coursetype_id"];
			$arr_tmp = rundocno($coursetype_id, $section_id);

			$args = array();
			$args["table"] = "register";
			if ( !empty($register_id) ) {
				$args["id"] = (int)$register_id;
			}//end if
			$args["pay_status"] = 3;
			$args["pay_date"] = $datetime_now;
			$args["pay_price"] = convert_amount($a_data["purchaseAmt"]);

			$fee = !empty($a_data["excCustomerFee"]) ? $a_data["excCustomerFee"] : $a_data["incCustomerFee"];

			// $args["pay_diff"] = convert_amount($fee);

			$args["runyear"] = $arr_tmp["runyear"];
			$args["runno"] = $arr_tmp["runno"];
			$args["doc_prefix"] = $arr_tmp["doc_prefix"];
			$args["docno"] = $arr_tmp["docno"];

			$db->set($args);
			// d($args);

		    $result_status = true;


			$this_file = basename($_SERVER["SCRIPT_FILENAME"], '.php');
			// var_dump($this_file);
		    register_log_save($register_id, 'update' ,$this_file, false);
		    // var_dump($rs); die();


		    $server_name = trim($_SERVER["SERVER_NAME"]);

		    if ( $server_name=="localhost" ) {
		    	$url = $server_name."/ati-ic/backoffice/check-course-elearning.php";
		    }else{
		    	$url = $server_name.'/backoffice/check-course-elearning.php';
		    }//end else


		    // $url = "http://localhost/ati-ic/backoffice/check-course-elearning.php";
		    $postData = "register_id={$register_id}&pay_status=3&pay_type=mpay";

		    $ch = curl_init();  
			curl_setopt($ch,CURLOPT_URL,$url);
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
			curl_setopt($ch,CURLOPT_HEADER, false); 
			curl_setopt($ch, CURLOPT_POST, count($postData));
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);    
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

			$output=curl_exec($ch);
			$error_msg = curl_error($ch);
			$info = curl_getinfo($ch);  
			curl_close($ch);




		    $array_register = array();
		    $array_register[$register_id] = $register_id;
		    pay_status_sendmail_func($array_register); 


		    
		}//end if



	}//end if



}//end if


?>

<script>
/*	
	$(document).ready(function(){
	    var register_id = "<?php //echo $register_id; ?>";
	    var result_status = "<?php //echo $result_status; ?>";
	    // alert(result_status);
	    if ( result_status==true ) {
	    	// alert(result_status);
			$.ajax({
		    	url: "../backoffice/check-course-elearning.php", 
		    	type: 'POST',
				data: {register_id: register_id
					, pay_status: 3
					, pay_type: 'mpay'
				},
		    	success: function(data){
	            	
		        }
		    });//end ajax
	    }//end if
	   
	});
*/
</script>

<?php /*
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ระบบบริจาค</title>
</head>
<body>
	<form name="sendform" method="post" action="<?php echo $url_for_el; ?>">
	<!-- <form name="sendform" method="post" action="test.php"> -->
		<input type="hidden" id=register_id name=register_id value="<?php echo $register_id;?>">
		<input type="hidden" id=pay_status name=pay_status value="3">
		<input type="hidden" id=pay_type name=pay_type value="mpay">

	</form>
	<!-- <button onclick="document.sendform.submit();">Submit</button> -->
</body>
</html>

<script type="text/javascript">

	$(document).ready(function(){
		// alert(555);
		document.sendform.submit();
	});
</script>		

*/?>