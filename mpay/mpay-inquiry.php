<?php
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/course.php";
include_once "pay-status-sendmail-func.php";
include("lib/nusoap.php");
include("lib/config.php");
global $db;

$debug = true;
$debug = false;

$datetime_now = date('Y-m-d H:i:s');
$date_now = date('Y-m-d');


// function write_log($file_name="", $msg="", $write_mode="a"){
// 	$str = $msg;	
// 	$date_now = date('Y-m-d H:i:s');
// 	$date_log = date('Y-m-d');
// 	$log_name = "./log/".$file_name."_".$date_log.".log";
// 	$file = fopen($log_name, $write_mode);
// 	$str_txt = $date_now."|".$str."\r\n";
// 	fwrite($file, $str_txt);
// 	fclose($file);
// }//end func

// d($_SESSION);

$member_id = $_SESSION["login"]["info"]["member_id"];
$register_id = $_SESSION["login"]["mpay"]["register_id"];
$payment_mpay_register_list_id = $_SESSION["login"]["mpay"]["payment_mpay_register_list_id"];

//for debug
//179643 179659 47 63
if ( $debug ) {	
	if ( !empty($register_id) ) {
		echo "!! Open mode maintenance !!";
		die();
	}//end if
	$register_id = 179645;
	$payment_mpay_register_list_id = 103;
}//end if

$result_status = false;

if ( $register_id && $payment_mpay_register_list_id ) {
	$reg_info = get_register("", $register_id);
	$reg_info = $reg_info[0];

	// d($reg_info); die();

	$orderId = $register_id;
	$member_id = $reg_info["member_id"];
	$purchaseAmt = $reg_info["course_price"] - $reg_info["course_discount"];
	$purchaseAmt = $purchaseAmt."00";

	$q = "SELECT xml_sale_id FROM payment_mpay_register_list WHERE payment_mpay_register_list_id={$payment_mpay_register_list_id}";
	$saleId = $db->data($q);

	$args = array();
	$args["projectCode"] = $projectCode;
	$args["command"] = "InquiryApi";
	$args["merchantId"] = $merchantId;
	$args["orderId"] = $orderId;
	$args["purchaseAmt"] = $purchaseAmt;
	$args["saleId"] = $saleId;


	$postData = '';

   //$params = 'projectCode=TEPS&command=InquiryApi&merchantId=6813&orderId=JJ005&purchaseAmt=100000&saleId=194";' ;

   $url = $url_for_payment;

   //create name value pairs seperated by &

   foreach($args as $k => $v){ 
      $postData .= $k . '='.$v.'&'; 
   }
   $postData = rtrim($postData, '&');
 	
 	// echo $postData;

    $ch = curl_init();  

 

    curl_setopt($ch,CURLOPT_URL,$url);

    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

    curl_setopt($ch,CURLOPT_HEADER, false); 

    curl_setopt($ch, CURLOPT_POST, count($postData));

    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);    

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

 
    $output=curl_exec($ch);
	$error_msg = curl_error($ch);
	$info = curl_getinfo($ch);  


    curl_close($ch);

	if ( !$output ) {
		echo date("Y-m-d H:i:s");
		echo "<hr>";
		echo "Status: "; var_dump($output); echo "เกิดข้อผิดพลาดในการเชื่อมต่อกับ mPAY กรุณาแจ้งผู้ดูแลระบบ";
		echo "<hr>";
		echo "ERROR msg: "; var_dump($error_msg);
		die();
	}//end if

	$xml=simplexml_load_string($output) or die("error");
	// d($xml);

 	$process_status = 'error';
 	$connection_status = 'success';

	if ( $xml->respCode == "0000" ) {

		$process_status = 'success';
	}
	if(!empty($error_msg)){
		$connection_status = 'error';
	}

	$str_txt = "register_id={$register_id}|".$postData."|process_status={$process_status}|"."connection_status={$connection_status}|"."error_msg={$error_msg}|";
 	write_log("mpay-inquiry", $str_txt, "a", "../mpay/log/" );

	// echo $xml->paymentStatus;
	// echo $xml->respCode;
	if ( $xml->respCode == "0000" ) {

		$paymentStatus = trim($xml->paymentStatus);
		if ( $paymentStatus=="SUCCESS" ) {
			// $amount = $xml->amount;
			$amount = substr($xml->amount, 0, -2);
			$amount = !empty($amount) ? $amount : 0;
			$excCustomerFee = substr($xml->excCustomerFee, 0, -2);
			$excCustomerFee = !empty($excCustomerFee) ? $excCustomerFee : 0;
			$purchaseAmt = substr($xml->purchaseAmt, 0, -2);
			$purchaseAmt = !empty($purchaseAmt) ? $purchaseAmt : 0;

			$args = array();
			$args["table"] = "register";
			$args["id"] = $register_id;
			$args["pay_date"] = $datetime_now;
			$args["pay_price"] = $amount;
			$args["pay_status"] = 3;

			$docNo = $reg_info["docno"];
 			if($docNo==""){
				$q = "select coursetype_id, section_id from register where register_id=$register_id";
				$v = $db->rows($q);
				$section_id = (int)$v["section_id"];
				$coursetype_id = (int)$v["coursetype_id"];
				$t = rundocno($coursetype_id, $section_id);
				$args = array_merge($args, $t);	
			}
			
			$db->set($args);

			// d($args);

			$args = array();
			$args["table"] = "payment_mpay_register_list";
			$args["id"] = $payment_mpay_register_list_id;
			$args["xml_result_code"] = $xml->respCode;
			$args["xml_result_status"] = $xml->status;
			$args["xml_result_desc"] = $xml->respDesc;
			$args["xml_result_payment_status"] = $xml->paymentStatus;
			$args["xml_credit_card_no"] = $xml->creditCardNo;
			$args["xml_order_expire_date"] = $xml->orderExpireDate;
			$args["xml_amount"] = $amount;
			$args["xml_exc_customer_fee"] = $excCustomerFee;
			$args["xml_purchase_amt"] = $purchaseAmt;
			$args["xml_payment_code"] = $xml->paymentCode;
			$db->set($args);

			// d($args);

			$this_file = basename($_SERVER["SCRIPT_FILENAME"], '.php');
			// var_dump($this_file);
		    register_log_save($register_id, 'update' ,$this_file, false);
		    // var_dump($rs); die();

		    $array_register[$register_id] = $register_id;
		    pay_status_sendmail_func($array_register); 

			// d($args);
			$result_status = true;
		}//end if
		

	}else{

	}//end else

    // var_dump($output);
    // return $output;
}//end if
// echo $result_status;
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" type="text/css" />
	<link rel="stylesheet" href="bootstrap/css/bootstrap.css" type="text/css" />
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script> 
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<style>
@font-face {
	font-family: 'circularregular';
	src: url('font/circular-webfont.eot');
	src: url('font/circular-webfont.eot?#iefix') format('embedded-opentype'),  url('font/circular-webfont.woff2') format('woff2'),  url('font/circular-webfont.woff') format('woff'),  url('font/circular-webfont.ttf') format('truetype'),  url('font/circular-webfont.svg#circularregular') format('svg');
	font-weight: normal;
	font-style: normal;
}

body {
margin-top: 100px;
font-family: "circularregular";	
}
	.billinfo {
    padding: 20px;
    padding-top: 20px;
    padding-bottom: 20px;
    background-color: #FFF;
    border: 1px solid #dcdcdc;
    border-radius: 10px;
    font-size: 24px;
   }

 .img-responsive{
   	display: block;
	max-width: 100px;
	height: auto;
	margin: auto;
   }

   .buttonb{
	/*background: linear-gradient(to bottom, #6580b5 0%, #6580b5 56%, #6580b5 56%, #4264a1 56%, #4264a1 100%);*/
	background: linear-gradient(to bottom, #abc22b 0%, #a3ba29 56%, #93ad28 56%, #8da729 56%, #84a12a 100%);
	margin-top: 35px;
	margin-bottom:20px;
	float:right;
	border-radius:5px;
	color:#FFF;
	padding:10px;
	font-size:20px;
	}



</style>

<!-- 
	<script>
	function myFunction() {
	    alert("Page is loaded");
	}
	</script>
	 -->
</head>
<body>

<form action='' method="POST">
 <div class="container">
 	 <div class="row">
	    <div class="col-sm-12">
		   <img class="img-responsive" src="img001.png" alt="Another alt text">
		</div>
	      <!-- <div class="col-sm-12" style="text-align: center;"> -->
	            <!-- <h3>MPay ขอขอบคุณ</h3> -->
	        <!-- </div>  -->
	</div>
	<br>
	<br>
<?php
	if ( $result_status ) {
?>
    <div class="billinfo">
	    <div class="control-group">
	      <label class="control-label"><?php echo "สวัสดีคุณ ".$reg_info["fname"]." ".$reg_info["lname"];?></label>
	      <div class="form-group">
	        <p>ขอขอบคุณที่คุณเลือกใช้บริการ mPay เราขอยืนยันว่าคำสั่งซื้อหมายเลข <?php echo $xml->paymentCode; ?> ของคุณได้รับการชำระเงินเรียบร้อย</p>
	      </div>
	      <hr>
	        <label class="control-label"><?php echo "Hi ".$reg_info["fname"]." ".$reg_info["lname"];?></label>
	      <div class="form-group">
	        <p>Thank you for choosing mPay! We are pleased to inform you that your order <?php echo $xml->paymentCode; ?> has been success</p>
	      </div>
	      <!-- Button -->
	      <div class="form-group">
	      	<div class="buttonb"> 
	      		<a href="#" onclick="goToindex();" style="color: #fff;">กลับหน้าแรก</a>
	      	</div>
	      </div>
	   	</div>
  	</div>
<?php
	}else{
?>

	<div class="billinfo">
	    <div class="control-group">
	      <div class="form-group">
	        <span class="red">ไม่พบคำสั่งซื้อของท่าน กรุณาติดต่อผู้ดูแลระบบ</span>
	        <br>
	        <span class="red">
			Phone :0-2264-0909 ต่อ 202 - 207 /
			ในเวลาทำการ
			จันทร์ - ศุกร์ เวลา 8.30 น. - 17:00 น.
			</span>
			<br>
			<span class="red">
			Fax : 0-2661-8504
			</span>
			<br>
			<span class="red">
			Email : Examination@ati-asco.org
			</span>
	      </div>
	      <hr>
	      <div class="form-group">
	        <span class="red">Process Error, Please contact administrator</span>
	       <br>
	       <span class="red">
			Phone :0-2264-0909 ext. 202 - 207 / 
			MON - FRI เวลา 8.30 AM - 17:00 PM
			</span>
			<br>
			<span class="red">
			Fax : 0-2661-8504
			</span>
			<br>
			<span class="red">
			Email : Examination@ati-asco.org
			</span>
	      </div>
	      <!-- Button -->
	      <div class="form-group">
	      	<div class="buttonb"> 
	      		<a href="#" onclick="goToindex();" style="color: #fff;">กลับหน้าแรก</a>
	      	</div>
	      </div>
	  </div>
  	</div>

<?php		
	}//end else
?>
 <!-- Button -->
<!--  
 	<div class="buttonb"> 
   	กลับหน้าแรก
	</div>
-->
</form>
</body>
</html>

<script>
	$(document).ready(function(){
	    var register_id = "<?php echo $register_id; ?>";
	    var result_status = "<?php echo $result_status; ?>";
	    // alert(result_status);
	    if ( result_status==true ) {
	    	// alert(result_status);
			$.ajax({
		    	url: "../backoffice/check-course-elearning.php", 
		    	type: 'POST',
				data: {register_id: register_id
					, pay_status: 3
					, pay_type: 'mpay'
				},
		    	success: function(data){
	            	
		        }
		    });//end ajax
	    }//end if
	   
	});

	function goToindex(){
    window.open('../index.php?p=main','_self');
  }

</script>