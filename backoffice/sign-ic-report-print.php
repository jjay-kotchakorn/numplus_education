<?php
include_once "./share/authen.php";
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
global $db;
require_once dirname(__FILE__) . '/PHPExcel.php';
$info = get_course_detail(" AND a.course_detail_id={$_GET["course_detail_id"]}");
$course_info = get_course(" AND a.course_id={$_GET["course_id"]}");
$sort_by = $_GET["sort_by"];

//d($info);
//d($_GET);

if($info){
	$info = $info[0];
	$course_info = $course_info[0];
	$course_detail_id = $info["course_detail_id"];
	$course_id = $course_info["course_id"];
	$fileName ="";

	$q = "SELECT a.register_id , a.course_id
		  FROM register_course_detail a 
		  INNER JOIN register b ON a.register_id=b.register_id AND b.pay_status IN (3,5,6) 		  		
		  WHERE a.course_detail_id=$course_detail_id AND a.active='T'";
    $r = $db->get($q);

    $ids = array();
    $register_id = array();
    foreach ($r as $key => $v) {
    	array_push($register_id, $v["register_id"]);
    	array_push($ids, $v["course_id"]);
    }

    //move
    $ids = array();
	$q = " SELECT register_id 
		FROM register_course_detail 
		WHERE active='T'  AND course_detail_id=$course_detail_id";
	$get_all = $db->get($q);
	if($get_all){
		foreach ($get_all as $key => $value) {
			$ids[] = $value["register_id"];
		}
	}
	$q = " SELECT register_id 
		FROM register 
		WHERE active='T' AND course_detail_id='$course_detail_id'";
	$get_register_all = $db->get($q);
	if($get_register_all){
		foreach ($get_register_all as $key => $value) {
			$ids[] = $value["register_id"];
		}
	}

	$t = array_unique($ids);
	$con_ids = implode(",", $t);

	$ord_by = "order by a.no asc";

	$q = "SELECT a.no,
			 a.title,
			 a.fname,
			 a.lname,
			 a.cid,
			 a.receipt_id,
			 a.course_id,
			 b.email1,
			 b.tel_home,
			 b.tel_mobile,
			 b.tel_office,
			 b.org_type,
			 b.org_name,
			 b.org_lv
		FROM register a 
		INNER JOIN member b ON a.member_id=b.member_id
		WHERE a.register_id IN ($con_ids) AND a.pay_status IN (3,5,6) AND a.active='T' 
		$ord_by
	";
	$r = $db->get($q);
?>

<!DOCTYPE html>
<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>รายชื่อผู้สอบ <?php echo $course_info["title"]; ?></title>
<link href="css/printform-style.css" rel="stylesheet" type="text/css" media="all" />
<style>
	@media print{
		body{
			padding: 0px;
		}
		#btPrint{
			display: none;
		}
	}
	.rpt-sign{
		table-layout: fixed;
		width: 1199;
		/*width: 100%;*/
		word-wrap:break-word;
	}
	.rpt-sign td {
	  white-space: initial;
	}
</style>
<body>

	<div class="form-landscape">
	<?php 
		if($sort_by == 'name'){
	?>
				<?php 
				if($r){
					$i = 0;
					foreach ($r as $key => $value) {
						$i++;
						$cut_row = $i%13; //set record per page
					if ( $i==1 || $cut_row==1 ) {
				?>

				<!-- <table class="td-center"> -->
				<table class="rpt-sign">
					<col width=10>
					<col width=20>
					<col width=40>
					<col width=40>
					<col width=36>
					<col width=60>
					<col width=60>
					<col width=30>
					<col width=30>
					<col width=30>
					<col width=40>
					<col width=40>
					<col width=40>
					<col width=40>
					<tbody>
						<tr>
							<td colspan="14" class="row-title">
								<div class="title" style="font-size: 26px">รายชื่อผู้สอบ <?php echo $course_info["title"]; ?></div>
								<div class="date-info" style="font-size: 22px">วันที่ <?php echo revert_date($info["date"])."&nbsp;เวลา ".$info["time"]; ?> น.</div>
								<div class="detail-info" style="font-size: 22px"><?php echo $info['address_detail']." ".$info['address'] ?></div>
							</td>
						</tr>
					</tbody>
					<tbody>

						<tr>
							<td class=""><span class="center" style="font-size: 22px">เลขที่<br>นั่ง</span></td>
							<td class=""><span class="center" style="font-size: 22px"><br>คำนำหน้า</span></td>
							<td class=""><span class="center" style="font-size: 22px"><br>ชื่อ</span></td>
							<td class=""><span class="center" style="font-size: 22px"><br>สกุล</span></td>
							<td class=""><span class="center" style="font-size: 22px">เลขที่บัตร<br>ประชาชน</span></td>
							<td class=""><span class="center" style="font-size: 22px"><br>หน่วยงาน</span></td>
							<td class=""><span class="center" style="font-size: 22px"><br>อีเมล์</span></td>
							<td class=""><span class="center" style="font-size: 22px">โทรศัพท์<br>บ้าน</span></td>
							<td class=""><span class="center" style="font-size: 22px">โทรศัพท์<br>ที่ทำงาน</span></td>
							<td class=""><span class="center" style="font-size: 22px">โทรศัพท์<br>มือถือ</span></td>
							<td class=""><span class="center" style="font-size: 22px"><br>ลายมือชื่อเข้าสอบ</span></td>
							<td class=""><span class="center" style="font-size: 22px"><br>ลายมือชื่อรับผลสอบ</span></td>
							<td class=""><span class="center" style="font-size: 22px">หมายเหตุ/ลายมือชื่อรับใบเสร็จ</span></td>
							<td class=""><span class="center" style="font-size: 22px">ยินยอมเปิดเผยข้อมูลให้กับ</span></td>
						</tr>
					</tbody>
				<?php
					}
				?>
				<tbody>
				<?php 
						$org_name = null;
						if ($value["org_type"]) {
							$id = $value["org_type"];
							if ($id==0 || empty($id)) {
								$org_name = "-";
							}elseif($id==5){
								$org_name = $value["org_lv"];
							}else{
								$q = "SELECT name 
									FROM receipt 
									WHERE receipt_id = {$value["org_name"]}";
								$org_name = $db->get($q);
								$org_name = $org_name[0]["name"];
							}
						}// end if
					?>			
						<tr>
							<!-- <td><span class="center"><?php echo $i; ?></td> -->
							<td><span class="center"><?php echo $value["no"]; ?></td>
							<td><span class="left"><?php echo $value["title"]; ?></td>
							<td><span class="left"><?php echo $value["fname"]; ?></td>
							<td><span class="left"><?php echo $value["lname"]; ?></td>
							<td><span class="center"><?php echo $value["cid"]; ?></td>
							<td><span class="left"><?php echo $org_name; ?></td>
							<td><span class="left"><?php echo $value["email1"]; ?></td>
							<td><span class="center"><?php echo $value["tel_home"]; ?></td>
							<td><span class="center"><?php echo $value["tel_mobile"]; ?></td>
							<td><span class="center"><?php echo $value["tel_office"]; ?></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<?php 
						if ( $cut_row==1 && $i!=1 ) {
						?>
							<br>
							<div style="page-break-after: always"></div>
						<?php
						}//END IF
					}//end loop	
				?>
				</tbody>
				<?php 
				} //end if
				?>
			</table>
	<?php
		} //end if sort by name
		else{

			$q = "SELECT distinct a.course_id
				FROM register a 
				WHERE a.register_id IN ($con_ids) AND a.pay_status IN (3,5,6) AND a.active='T' $ord_by
			";
			//var_dump($q);	
			$cos_ids = $db->get($q);

			foreach ($cos_ids as $key => $v) {
				$cos_id = intval($v["course_id"]);

				$q = "SELECT a.register_id,
						 a.no,
						 a.title,
						 a.fname,
						 a.lname,
						 a.cid,
						 a.receipt_id,
						 a.course_id,
						 a.course_detail_id,
						 m.email1,
						 m.tel_home,
						 m.tel_mobile,
						 m.tel_office,
						 m.org_type,
						 m.org_name,
						 m.org_lv
				FROM register a  
				LEFT JOIN member m ON a.member_id=m.member_id
				WHERE course_id = $cos_id AND a.register_id IN ($con_ids) AND a.pay_status IN (3,5,6) AND a.active='T'
				$ord_by
				";
				$member_info = $db->get($q);

				$c = "SELECT c.course_id,
						c.code,
						c.course_order,
						c.title,
						c.set_time,
						c.code_project,
						d.date,
						d.time,
						d.address_detail,
						d.address
				FROM course c
				LEFT JOIN course_detail d ON d.course_id = c.course_id 
				WHERE c.course_id = $cos_id AND c.active='T'
				";
				$cos_info = $db->get($c);
				$cos_info = $cos_info[0];

				//$cos_info["address_detail"] = null;
				if ($cos_info["address_detail"]=="" || $cos_info["address_detail"]==NULL) {
					$cos_dt_info = get_course_detail("", $_GET["course_detail_id"]);
					$cos_dt_info = $cos_dt_info[0];
					$cos_date = $cos_dt_info["date"];
					$cos_time = $cos_dt_info["time"];
					$cos_addrs = $cos_dt_info['address_detail']." ".$cos_dt_info['address'];
				}else{
					$cos_date = $cos_info["date"];
					$cos_time = $cos_info["time"];
					$cos_addrs = $cos_info['address_detail']." ".$cos_info['address'];
				}

/*
				<div class="page-header">
					<div class="title">รายชื่อผู้สอบ <?php echo $cos_info["title"]; ?></div>
					<div class="date-info">วันที่ <?php echo revert_date($cos_date)."&nbsp;เวลา ".$cos_time; ?> น.</div>
					<div class="detail-info"><?php echo $cos_addrs; ?></div>
				</div> 
*/
						$i=0;
						foreach ($member_info as $key => $value): 
							$i++;
							$cut_row = $i%13; //set record per page
							if ( $i==1 || $cut_row==1 ) {

					?>	
					<!-- <table class="td-center"> -->
					<table class="rpt-sign">
						<col width=10>
						<col width=20>
						<col width=40>
						<col width=40>
						<col width=36>
						<col width=60>
						<col width=60>
						<col width=30>
						<col width=30>
						<col width=30>
						<col width=40>
						<col width=40>
						<col width=40>
						<col width=40>
						<tbody>
							<tr>
								<td colspan="14" class="row-title">
									<div class="title" style="font-size: 26px">รายชื่อผู้สอบ <?php echo $cos_info["title"]; ?></div>
									<div class="date-info" style="font-size: 22px">วันที่ <?php echo revert_date($cos_date)."&nbsp;เวลา ".$cos_time; ?> น.</div>
									<div class="detail-info" style="font-size: 22px"><?php echo $cos_addrs; ?></div>
								</td>
							</tr>
						</tbody>

						<tbody>
							<tr>
								<td ><span class="center">เลขที่<br>นั่ง</span></td>
								<td width="1%"><span class="center" style="font-size: 22px">คำนำหน้า</span></td>
								<td width="7%"><span class="center" style="font-size: 22px"><br>ชื่อ</span></td>
								<td width="8%"><span class="center" style="font-size: 22px"><br>สกุล</span></td>
								<td width="5%"><span class="center" style="font-size: 22px">เลขที่บัตร<br>ประชาชน</span></td>
								<td width="18%"><span class="center" style="font-size: 22px"><br>หน่วยงาน</span></td>
								<td width="5%"><span class="center" style="font-size: 22px"><br>อีเมล์</span></td>
								<td width="5%"><span class="center" style="font-size: 22px">โทรศัพท์<br>บ้าน</span></td>
								<td width="5%"><span class="center" style="font-size: 22px">โทรศัพท์<br>ที่ทำงาน</span></td>
								<td width="5%"><span class="center" style="font-size: 22px">โทรศัพท์<br>มือถือ</span></td>
								<td width="10%"><span class="center" style="font-size: 22px"><br>ลายมือชื่อเข้าสอบ</span></td>
								<td width="10%"><span class="center" style="font-size: 22px"><br>ลายมือชื่อรับผลสอบ</span></td>
								<td width="10%"><span class="center" style="font-size: 22px">หมายเหตุ/ลายมือชื่อรับใบเสร็จ</span></td>
								<td width="10%"><span class="center" style="font-size: 22px">ยินยอมเปิดเผยข้อมูลให้กับ</span></td>
							</tr>
						</tbody>

						<tbody>
						<?php
							}//end if show header

							$org_name = null;
							if ($value["org_type"]) {
								$id = $value["org_type"];
								if ($id==0 || empty($id)) {
									$org_name = "-";
								}elseif($id==5){
									$org_name = $value["org_lv"];
								}else{
									$q = "SELECT name 
										FROM receipt 
										WHERE receipt_id = {$value["org_name"]}";
									$org_name = $db->get($q);
									$org_name = $org_name[0]["name"];
								}
							}// end if
						?>	
							<tr>
								<!-- <td><span class="center"><?php echo $value["register_id"]; ?></td> -->
								<td><span class="center"><?php echo $value["no"]; ?></td>
								<td><span class="left"><?php echo $value["title"]; ?></td>
								<td><span class="left"><?php echo $value["fname"]; ?></td>
								<td><span class="left"><?php echo $value["lname"]; ?></td>
								<td><span class="center"><?php echo $value["cid"]; ?></td>
								<td><span class="left"><?php echo $org_name; ?></td>
								<td><span class="left"><?php echo $value["email1"]; ?></td>
								<td><span class="center"><?php echo $value["tel_home"]; ?></td>
								<td><span class="center"><?php echo $value["tel_mobile"]; ?></td>
								<td><span class="center"><?php echo $value["tel_office"]; ?></td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
						<?php 
							if ( $cut_row==1 && $i!=1 ) {
						?>
								<br>
								<div style="page-break-after: always"></div>
						<?php
							}//end if
						endforeach 
						?>
						</tbody>

					</table>
				<br>
				<div style="page-break-after: always"></div>
	<?php
			} //end loop cod_id
		} // end else
	?>

	</div>

</body>
<script type="text/javascript">
function printPage(){
	   window.print();
	   setTimeout(" parent.$.fancybox.close()",1000);
	}
</script>
</html>
<?php } 
?>