<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
global $db;
global $SECTIONID;

$error = $_SESSION["error"]["msg"];
unset($_SESSION["error"]["msg"]);
$success = $_SESSION["success"]["msg"];
unset($_SESSION["success"]["msg"]);

$course_id = $_GET["course_id"];
$section = datatype(" and a.active='T'", "section", true);
$coursetype = datatype(" and a.active='T'", "coursetype", true);
$q = "select  title from course where course_id=$course_id";
$r = $db->rows($q);
$str = "";
if($r){
	$str = $r["title"];
	$info = get_course("", $course_id);
	if($info) $info = $info[0];
	$cos_name = $info["title"];
	$info["title"] .= " ".$info["detail"];
}else{
	$str = "เพิ่มหลักสูตร";
}
?>
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="row">
				<div class="col-md-12">           
					<div class="block-flat">
						<div class="header">              
							<ol class="breadcrumb">
								<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>');">หน้าหลัก</a></li>
								<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>&type=select-course');">เลือกหลักสูตร</a></li>
								<li class="active">หน้ายอดผู้สมัคร</li>
							</ol>
						</div>
						<div class="content">
							<form id="frmMain" name="frmMain" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="">
								<input type="hidden" name="course_id" id="course_id" value="<?php echo $course_id; ?>">
								
								<div class="col-sm-12">
									<div class="form-group row">
										<label class="col-sm-1 control-label" style="padding-right:10px;" >ชื่อหลักสูตร <span class="red" style="display:inline-block; position:absolute;"> &nbsp;*</span></label>
										<div class="col-sm-5">
											<div class="input-text">
												<?php echo $info["title"]; ?>
											</div>	
											
										</div> 
										<label class="col-sm-2 control-label">ประเภทใบอนุญาต/คุณวุฒิ </label>
										<div class="col-sm-3">
											<div class="input-text">
												<?php echo $info["section_name"]; ?>
											</div>
										</div>                
										<label class="col-sm-2 control-label">ประเภทหลักสูตร</label>
										<div class="col-sm-2">
											<div class="input-text">
												<?php echo $info["coursetype_name"]; ?>
											</div>
										</div>
									</div>
									<?php 
										$start = date("Y-m-01");
										$stop = date('Y-m-t',strtotime('today'));
										$dateStart = ($_POST["date_start"]) ? thai_to_timestamp($_POST["date_start"]) :  $start;
										$dateStop =  ($_POST["date_stop"]) ? thai_to_timestamp($_POST["date_stop"]) : $stop;
										if ($dateStart || $dateStop) {
										    if (!$dateStart && $dateStop)
										        $dateStart = $dateStop;
										    if (!$dateStop && $dateStart)
										        $dateStop = $dateStart;
										    $t = $dateStart;
										    if ($dateStart > $dateStop) {
										        $dateStart = $dateStop;
										        $dateStop = $t;
										    }
										}
									 ?>
								<div class="form-group row"> 
									<label class="col-sm-1 control-label"  >วันที่เริ่ม</label>
									<div class="col-sm-2"  >
										<input class="form-control" name="date_start" value="<?php echo revert_date($dateStart); ?>" id="date_start" onblur="reCall();" placeholder="วันที่เริ่ม" type="text">
									</div>                                          
									<label class="col-sm-1 control-label"  >วันที่สิ้นสุด</label>
									<div class="col-sm-2" >
										<input class="form-control" name="date_stop" id="date_stop" value="<?php echo revert_date($dateStop); ?>" onblur="reCall();" placeholder="วันที่สิ้นสุด" type="text">
									</div>
									<label class="col-sm-1 control-label">PUB / INH</label>
									<div class="col-sm-2">
										<select name="check_inh" id="check_inh" class="form-control">
											<option selected="selected"  value="">--เลือก--</option>
											<option value="pub">PUB</option>
											<option value="inh">INH</option>
										</select>
									</div>   
									<button class="btn" id="filter_date"> <i class="fa fa-search"></i></button>
								</div> 

									<div class="form-group row" <?php if(!$course_id) echo 'style="display:none;"'; ?>>										
										<table class="table table-striped" id="tbList">
											<thead>
												<tr class="alert alert-success" style="font-weight:bold;">
													<!-- <td width="3%">ลำดับ</td> -->
													<td width="8%" class="center">วัน</td>
													<td width="7%" class="center">เวลา</td>
													<td width="3%" class="center">ALL</td>
													<td width="7%" class="center">ชื่อโครงการ</td>
													<td width="15%" class="center"><?php echo ($info["section_id"]==1 && $info["coursetype_id"]==2) ? "สถานที่" : "หลักสูตร";?></td>
													<td width="8%" class="center">จังหวัด</td>
													<td width="6%" class="center">เปิดรับ</td>
													<td width="6%" class="center">ปิดรับ</td>
													<td width="3%" class="center">จอง</td>
													<td width="3%" class="center">สำรอง</td>
													<td width="3%" class="center">สมัคร</td>
													<td width="3%" class="center">ว่าง</td>
													<td width="3%" class="center">Max</td>
													<td width="6%" class="center">สถานะ</td>
													<td width="20%" class="center">รายละเอียด</td>
												</tr>
											</thead>
											<tbody>
												<?php 
												$select_year = date("Y");
												$select_month = date("m");
												$sWhere = ($dateStart && $dateStop) ? " and a.date>='$dateStart' and a.date<='$dateStop'" : "";
												if($_POST["check_inh"]=="pub") $sWhere .= " and a.inhouse='F'";
												if($_POST["check_inh"]=="inh") $sWhere .= " and a.inhouse='T'";												
												$r = get_course_detail(" and a.active='T' and a.course_id=$course_id $sWhere");
												// d($r);
												if($r){
													$runNo = 1;
													$sum_jong = 0; 
													$sum_book = 0; 
													$sum_reg = 0; 
													$sum_rem = 0; 
													$sum_max = 0; 
													$sum_usebook = 0;
													$tmp_cos_dt_ids = "";
													foreach($r as $k=>$v){
														$course_detail_id = $v["course_detail_id"];
														$tmp_cos_dt_ids .= $course_detail_id.",";
														$use = $v["sit_all"];//$use = get_use_chair($course_detail_id);
														$chair_all = $v['chair_all'];
														$book = $v["book"];
														$usebook = 0;
														if($book>0) {
															$usebook += $v["book_all"];
															//$usebook += get_use_book($course_detail_id);
														}
														$sit_all = $chair_all - $book - $use + $usebook;
														if($sit_all<0) $sit_all = 0;
														$status = get_course_status_name($sit_all, $v["status"], $v["coursetype_id"]);
														if($v["date"]<date("Y-m-d") or $v["end_regis"]<date("Y-m-d")){
															$status = "<span style='color:red;'>ปิดรับสมัคร</span>";
														}
														$jong = $v["regis_all"];
														//$jong = get_use_regis($course_detail_id, "1");
														//$reg = get_use_regis($course_detail_id, "3,5,6");
														$reg = $use - $jong;
														$sum_jong += $jong; 
														$sum_book += $book;
														$sum_usebook += $usebook; 
														$sum_reg += $reg; 
														$sum_rem = 0; 
														$sum_max = 0;	

														$cos_dt_info = get_course_detail("", $v["course_detail_id"]);
														$cos_dt_info = $cos_dt_info[0];
														$cos_info = get_course("", $cos_dt_info["course_id"]);
														$cos_info = $cos_info[0];
													
														if ($v["code_project"]) {
															$cd_proj = $v["code_project"];
															$cos_dt_id = $v["course_detail_id"];
														}else{
															$cd_proj = $cos_dt_info["code_project"];
															$cos_dt_id = $cos_dt_info["course_detail_id"];
														}
														if ( empty($cd_proj) ) {
															$cd_proj = "เพิ่มชื่อโครงการ";
														}
														?>
														<tr style="cursor:pointer;">
															<!-- <td class="center"><?php //echo $runNo; ?></td> -->
															<!-- <td class="center"><strong style="font-size:15px;"><?php //echo $v["day"]; ?>. </strong> <?php //echo revert_date($v["date"]); ?></td> -->
															<td  class="center"><?php echo $v["day"].". "; ?><?php echo revert_date($v["date"]); ?></td>
															<td  class="center"><?php echo $v["time"]; ?></td>
															<td  class="center"><?php echo ($v["inhouse"]=="T") ? 'IH' : 'WR'; ?></td>
															<!-- <td  class="center"><?php //echo $v["code_project"]; ?></td> -->
															<td  class="center">
																<a class="" data-toggle="tooltip" data-original-title="อัพเดทชื่อโครงการ" href="#" 
																	onclick="update_project_code(<?php echo $cos_dt_id; ?>);">
																	<?php echo $cd_proj;?>
																</a>
															</td>
															<td  class="center"><?php echo ($info["section_id"]==1  && $info["coursetype_id"]==2) ? $v["address_detail"] : $cos_info["title"]; ?></td>
															<td  class="center"><?php echo $v["address"]; ?></td>													
															<td  class="center"><?php echo revert_date($v["open_regis"]); ?></td>
															<td  class="center"><?php echo revert_date($v["end_regis"]); ?></td>
															<td  class="center"><?php echo $jong; ?></td>													
															<td  class="center"><?php echo $usebook; ?> (<?php echo (int) $book; ?>)</td>													
															<td  class="center"><?php echo $reg; ?></td>													
															<td  class="center"><?php echo $sit_all; ?></td>													
															<td  class="center"><?php echo $chair_all; ?> </td>													
															<td  class="center"><?php echo $status; ?></td>													
															<td  class="center">
																<a class="btn btn-small " data-toggle="tooltip" data-original-title="ดูข้อมูลการสมัคร" href="#" onclick="add_to_info(<?php echo $v["course_detail_id"].",".$cos_info["course_id"]; ?>);"><i class="fa  fa-list-alt"></i></a>
																<a class="btn btn-small " data-toggle="tooltip" data-original-title="ดูข้อมูลผู้มีสิทธิ์" href="#" onclick="add_to_rule(<?php echo $v["course_detail_id"]; ?>);"><i class="fa fa-edit"></i></a>
																<a class="btn btn-small " data-toggle="tooltip" data-original-title="ดูผลกิจกรรม" href="#" onclick="add_to_result(<?php echo $v["course_detail_id"]; ?>);"><i class="fa fa-bar-chart-o"></i></a>
																<a class="btn btn-small " data-toggle="tooltip" data-original-title="แก้ไขรอบกิจกรรม" href="#" onclick="edit_course_detail(<?php echo $v["course_detail_id"]; ?>);"><i class="fa fa-book"></i></a>
															</td>
														</tr>
														<?php
														$runNo++;
													} 	
												}
												?>
												<?php 
													$sWhere = ($dateStart && $dateStop) ? " and b.date>='$dateStart' and b.date<='$dateStop'" : "";
													if($_POST["check_inh"]=="pub") $sWhere .= " and b.inhouse='F'";
													if($_POST["check_inh"]=="inh") $sWhere .= " and b.inhouse='T'";														
													$con = "and a.course_id={$course_id} $sWhere";
												    $r = get_course_detail_list($con);
												    // d($r);
													if($r){
														$runNo = 1; 
														foreach($r as $k=>$v){
																$course_detail_id = $v["course_detail_id"];
																$use = $v["sit_all"];//$use = get_use_chair($course_detail_id);
																$chair_all = $v['chair_all'];
																$book = $v["book"];
																$usebook = 0;
																if($book>0) {
																	$usebook += $v["book_all"];
																	//$usebook += get_use_book($course_detail_id);
																}
																$sit_all = $chair_all - $book - $use + $usebook;
																if($sit_all<0) $sit_all = 0;
																$status = get_course_status_name($sit_all, $v["status"], $v["coursetype_id"]);
																if($v["date"]<date("Y-m-d") or $v["end_regis"]<date("Y-m-d")){
																	$status = "<span style='color:red;'>ปิดรับสมัคร</span>";
																}
																$jong = $v["regis_all"];
																//$jong = get_use_regis($course_detail_id, "1");
																//$reg = get_use_regis($course_detail_id, "3,5,6");																
																$reg = $use - $jong;
																$sum_jong += $jong; 
																$sum_book += $book;
																$sum_usebook += $usebook; 
																$sum_reg += $reg; 
																$sum_rem = 0; 
																$sum_max = 0;	
			
																$cos_dt_info = get_course_detail("", $v["course_detail_id"]);
																$cos_dt_info = $cos_dt_info[0];
																$cos_info = get_course("", $cos_dt_info["course_id"]);
																$cos_info = $cos_info[0];
																
																if ($v["code_project"]) {
																	$cd_proj = $v["code_project"];
																	$cos_dt_id = $v["course_detail_id"];
																}else{
																	$cd_proj = $cos_dt_info["code_project"];
																	$cos_dt_id = $cos_dt_info["course_detail_id"];
																}
																if ( empty($cd_proj) ) {
																	$cd_proj = "เพิ่มชื่อโครงการ";
																}
															?>
															<tr style="cursor:pointer;">
																<!-- <td class="center"><?php //echo $runNo; ?></td> -->
																<!-- <td class="center"><strong style="font-size:15px;"><?php //echo $v["day"]; ?>. </strong> <?php //echo revert_date($v["date"]); ?></td> -->
																<td class="center"><?php echo $v["day"]; ?>. <?php echo revert_date($v["date"]); ?></td>
																<td class="center"><?php echo $v["time"]; ?></td>
																<td class="center"><?php echo ($v["inhouse"]=="T") ? 'IH' : 'WR'; ?></td>
																<!-- <td  class="center"><?php //echo $v["code_project"]; ?></td> -->
																<td  class="center">
																	<a class="" data-toggle="tooltip" data-original-title="อัพเดทชื่อโครงการ" href="#" 
																		onclick="update_project_code(<?php echo $cos_dt_id; ?>);">
																		<?php echo $cd_proj;?>
																	</a>
																</td>
																<td  class="center"><?php echo ($info["section_id"]==1  && $info["coursetype_id"]==2) ? $v["address_detail"] : $cos_info["title"]; ?></td>
																<td  class="center"><?php echo $v["address"]; ?></td>														
																<td  class="center"><?php echo revert_date($v["open_regis"]); ?></td>
																<td  class="center"><?php echo revert_date($v["end_regis"]); ?></td>
																<td  class="center"><?php echo $jong; ?></td>													
																<td  class="center"><?php echo $usebook; ?> (<?php echo (int) $book; ?>)</td>													
																<td  class="center"><?php echo $reg; ?></td>													
																<td  class="center"><?php echo $sit_all; ?></td>														
																<td  class="center"><?php echo $chair_all; ?> </td>													
																<td  class="center"><?php echo $status; ?></td>													
																<td  class="center">
																	<a class="btn btn-small " data-toggle="tooltip" data-original-title="ดูข้อมูลการสมัคร" href="#" onclick="add_to_info(<?php echo $v["course_detail_id"].",".$cos_info["course_id"]; ?>);"><i class="fa  fa-list-alt"></i></a>
																	<a class="btn btn-small " data-toggle="tooltip" data-original-title="ดูข้อมูลผู้มีสิทธิ์" href="#" onclick="add_to_rule(<?php echo $v["course_detail_id"]; ?>);"><i class="fa fa-edit"></i></a>
																	<a class="btn btn-small " data-toggle="tooltip" data-original-title="ดูผลกิจกรรม" href="#" onclick="add_to_result(<?php echo $v["course_detail_id"]; ?>);"><i class="fa fa-bar-chart-o"></i></a>
																	<a class="btn btn-small " data-toggle="tooltip" data-original-title="แก้ไขรอบกิจกรรม" href="#" onclick="edit_course_detail(<?php echo $v["course_detail_id"]; ?>);"><i class="fa fa-book"></i></a>
																</td>
															</tr>
															<?php
															$runNo++;
														} 	
													}
												 ?>
												<?php 
													$sec_action = str_course_type($info["coursetype_id"]);
													$coursetype_id = $info["coursetype_id"];
													$section_id = $info["section_id"];
													
													// echo $section_id." | ".$sec_action;

													$x = get_select_course($section_id, $sec_action);
													$sWhere = ($dateStart && $dateStop) ? " and a.date>='$dateStart' and a.date<='$dateStop'" : "";
													if($_POST["check_inh"]=="pub") $sWhere .= " and a.inhouse='F'";
													if($_POST["check_inh"]=="inh") $sWhere .= " and a.inhouse='T'";														
													$tmp_cos_dt_ids = trim($tmp_cos_dt_ids, ",");
													$where_not_in = !empty($tmp_cos_dt_ids) ? " and a.course_detail_id NOT IN ({$tmp_cos_dt_ids})" : "";
													$con = " and a.active='T' and a.coursetype_id={$coursetype_id} and a.section_id={$section_id} $sWhere {$where_not_in}";
												    $r = get_course_detail($con);

												    // d($r);

													if($r && $x==4){
														//$runNo = 1; 
														foreach($r as $k=>$v){
																$course_detail_id = $v["course_detail_id"];
																$use = $v["sit_all"];//$use = get_use_chair($course_detail_id);
																$chair_all = $v['chair_all'];
																$book = $v["book"];
																$usebook = 0;
																if($book>0) {
																	$usebook += $v["book_all"];
																	//$usebook += get_use_book($course_detail_id);
																}
																$sit_all = $chair_all - $book - $use + $usebook;
																if($sit_all<0) $sit_all = 0;
																$status = get_course_status_name($sit_all, $v["status"], $v["coursetype_id"]);
																if($v["date"]<date("Y-m-d") or $v["end_regis"]<date("Y-m-d")){
																	$status = "<span style='color:red;'>ปิดรับสมัคร</span>";
																}
																$jong = $v["regis_all"];
																//$jong = get_use_regis($course_detail_id, "1");
																//$reg = get_use_regis($course_detail_id, "3,5,6");														
																$reg = $use - $jong;
																$sum_jong += $jong; 
																$sum_book += $book;
																$sum_usebook += $usebook; 
																$sum_reg += $reg; 
																$sum_rem = 0; 
																$sum_max = 0;		

																$cos_dt_info = get_course_detail("", $v["course_detail_id"]);
																$cos_dt_info = $cos_dt_info[0];
																$cos_info = get_course("", $cos_dt_info["course_id"]);
																$cos_info = $cos_info[0];
																
																if ($info["section_id"]==1 && $info["coursetype_id"]==2) {
																	$cos_info["course_id"] = $course_id;
																}
																
																if ($v["code_project"]) {
																	$cd_proj = $v["code_project"];
																	$cos_dt_id = $v["course_detail_id"];
																}else{
																	$cd_proj = $cos_dt_info["code_project"];
																	$cos_dt_id = $cos_dt_info["course_detail_id"];
																}
																if ( empty($cd_proj) ) {
																	$cd_proj = "เพิ่มชื่อโครงการ";
																}
															?>
															<tr style="cursor:pointer;">
																<!-- <td class="center"><?php //echo $runNo; ?></td> -->
																<td class="center"><strong style="font-size:15px;"><?php echo $v["day"]; ?>. </strong> <?php echo revert_date($v["date"]); ?></td>
																<td  class="center"><?php echo $v["time"]; ?></td>
																<td  class="center"><?php echo ($v["inhouse"]=="T") ? 'IH' : 'WR'; ?></td>
																<!-- <td  class="center"><?php //echo $v["code_project"]; ?></td> -->
																<td  class="center">
																	<a class="" data-toggle="tooltip" data-original-title="อัพเดทชื่อโครงการ" href="#" 
																		onclick="update_project_code(<?php echo $cos_dt_id; ?>);">
																		<?php echo $cd_proj;?>
																	</a>
																</td>
																<td  class="center"><?php echo ($info["section_id"]==1  && $info["coursetype_id"]==2) ? $v["address_detail"] : $cos_info["course_id"]; ?></td>
																<td  class="center"><?php echo $v["address"]; ?></td>	
																<td  class="center"><?php echo revert_date($v["open_regis"]); ?></td>
																<td  class="center"><?php echo revert_date($v["end_regis"]); ?></td>
																<td  class="center"><?php echo $jong; ?></td>													
																<td  class="center"><?php echo $usebook; ?> (<?php echo (int) $book; ?>)</td>													
																<td  class="center"><?php echo $reg; ?></td>													
																<td  class="center"><?php echo $sit_all; ?></td>													
																<td  class="center"><?php echo $chair_all; ?> </td>													
																<td  class="center"><?php echo $status; ?></td>													
																<td  class="center">
																	<a class="btn btn-small " data-toggle="tooltip" 
																		data-original-title="ดูข้อมูลการสมัคร" href="#" 
																		onclick="add_to_info(<?php echo $v["course_detail_id"].",".$cos_info["course_id"]; ?>);"><i class="fa  fa-list-alt"></i></a>
																	<a class="btn btn-small " data-toggle="tooltip" data-original-title="ดูข้อมูลผู้มีสิทธิ์" href="#" onclick="add_to_rule(<?php echo $v["course_detail_id"]; ?>);"><i class="fa fa-edit"></i></a>
																	<a class="btn btn-small " data-toggle="tooltip" data-original-title="ดูผลกิจกรรม" href="#" onclick="add_to_result(<?php echo $v["course_detail_id"]; ?>);"><i class="fa fa-bar-chart-o"></i></a>
																	<a class="btn btn-small " data-toggle="tooltip" data-original-title="แก้ไขรอบกิจกรรม" href="#" onclick="edit_course_detail(<?php echo $v["course_detail_id"]; ?>);"><i class="fa fa-book"></i></a>
																</td>
															</tr>
															<?php
															$runNo++;
														} 	
													}
												 ?>
												 <tr style=" background-color:#fff;text-align:center;">
												 	<td colspan="8" style="background-color:#fff;border-left:0px;border-bottom:0px;"></td>
												 	<td><?php echo $sum_jong; ?></td>
												 	<td><?php echo $sum_usebook, ' (',$sum_book, ')'; ?></td>
												 	<td><?php echo $sum_reg; ?></td>
												 	<td colspan="4"style="background-color:#fff;border-right:0px;border-bottom:0px;"></td>
												 </tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="clear"></div>
							<div class="form-group row" style="padding-left:10px;">
								<div class="col-sm-12">
									<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p'] ?>');">กลับสู่หน้าหลัก</button>
								</div>
							</div>
						</form>
					</div>
				</div>

			</div>
		</div>

	</div>
</div> 
</div>
<?php include_once ('inc/js-script.php'); ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$("#filter_date").click(function(event) {
			/* Act on the event */
			$("#frmMain").submit();
		});
	    $("#date_start").datepicker({language:'th-th',format:'dd-mm-yyyy'});
	    $("#date_stop").datepicker({language:'th-th',format:'dd-mm-yyyy'});		
	});
	function add_to_rule(id){
	    var url = "index.php?p=<?php echo $_GET["p"];?>&course_detail_id="+id+"&type=register-approve&course_id=<?php echo $course_id; ?>";
	   /*redirect(url);*/
	   window.open(url,'_blank');
	}
	function add_to_receipt(id){
	    var url = "index.php?p=<?php echo $_GET["p"];?>&course_detail_id="+id+"&type=register-receipt&course_id=<?php echo $course_id; ?>";
	   /*redirect(url);*/
	   window.open(url,'_blank');
	}
	function add_to_info(id, cos_id){
		//console.log(id+" "+cos_id);
	   	// var url = "index.php?p=<?php echo $_GET["p"];?>&course_detail_id="+id+"&type=register-info&course_id=<?php echo $course_id; ?>";
	    var url = "index.php?p=<?php echo $_GET["p"];?>&course_detail_id="+id+"&type=register-info&course_id="+cos_id;
	   	/*redirect(url);*/
	   	window.open(url,'_blank');
	}
	function add_to_result(id){
	    var url = "index.php?p=<?php echo $_GET["p"];?>&course_detail_id="+id+"&type=register-result&course_id=<?php echo $course_id; ?>";
	   /*redirect(url);*/
	   window.open(url,'_blank');
	}
	function edit_course_detail(id){
		var url = "index.php?p=course-detail&course_detail_id="+id+"&type=detail&flag=detail";
	    //var url = "index.php?p=register&course_detail_id="+id+"&type=walk-in&course_id=<?php echo $course_id; ?>";
	    window.open(url,'_blank');
	}

	function update_project_code(id){
		//alert(id);
		var course_id = "<?php echo $course_id; ?>";
		var cd_proj_new = prompt("เปลี่ยนชื่อโครงการใหม่", "");
		var url = "index.php?p=course-detail&course_detail_id="+id+"&type=detail&flag=code_project&code_project="+cd_proj_new;
		if (cd_proj_new != "") {
	        $.ajax( {
				"type": "GET",
				"async": false, 
				"url": url,
				"data": {course_detail_id:id
					, type:"detail"
					, flag:"code_project"
					, code_project:cd_proj_new}, 
				"success": function(data){	
					//alert(data);			 
					window.location.reload(true);
				}
			});
	    } 
	}

</script>
<style>
	#set_time-error{
		display: none !important;
	}
</style>