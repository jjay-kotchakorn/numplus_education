<?php
include_once "./share/authen.php";
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
global $db, $SECTIONID, $COURSETYPEID, $EMPID;

$date_now = date('Y-m-d H:i:s');
$date = date('Y-m-d');
$date_start = trim($_GET["date_start"]);
$date_stop = trim($_GET["date_stop"]);
$pay = trim($_GET["pay"]);
$coursetype_id = trim($_GET["coursetype_id"]);
$section_id = trim($_GET["section_id"]);
$code_project = trim($_GET["code_project"]);
// d($_GET);die();

$date_start_conv = thai_to_timestamp($date_start);
$date_stop_conv = thai_to_timestamp($date_stop);
// echo $date_start."/".$date_stop;
$cond_cos = "";
$coursetype_name = "ทุกประเภท";
$section_name = "ทุกประเภท";
if ( !empty($coursetype_id) ) {
	$cond_cos .= " AND r.coursetype_id={$coursetype_id}";
	//get coursetype_name
	$q="SELECT name FROM coursetype WHERE active='T' AND coursetype_id={$coursetype_id}";
	$rs = $db->get($q);
	$coursetype_name = $rs[0]["name"];
}//end if

if ( !empty($section_id) ) {
	$cond_cos .= " AND r.section_id={$section_id}";
	//get section_name
	$q="SELECT name FROM section WHERE active='T' AND section_id={$section_id}";
	$rs = $db->get($q);
	$section_name = $rs[0]["name"];	
}//end if

$q = "UPDATE payment_compare_all_list
	SET active = 'F'
	WHERE payment_type='{$pay}'
		AND payment_date BETWEEN '{$date_start_conv}' AND '{$date_stop_conv}' 
";
// echo $q;
$db->query($q);

//filter cond payment_type
$con_pay="";
switch ($pay) {
	case 'money':
		$con_pay .= " AND p.payment_type='money'";
		$pay_txt = "เงินสด";

		$q = "SELECT r.register_id
					, r.course_id
					, r.pay_price
					, r.pay_date
					, b.register_course_detail_course_detail_id
					, b.course_detail_id
				FROM register AS r inner join view_register_course_detail b on r.register_id=b.view_register_course_detail_id
				WHERE r.active='T' AND r.auto_del='F' AND r.pay_status=3 
					AND (r.pay='walkin' or r.pay='importfile' or r.pay='at_ati')
					AND r.pay_date BETWEEN '{$date_start_conv} 00:00:00' AND '{$date_stop_conv} 23:59:59'
					$cond_cos
				ORDER BY r.pay_date DESC	
		";
		$reg_info = $db->get($q);	
/*		echo $q;
		d($reg_info);die();*/

		foreach ($reg_info as $key => $v) {
			$arg = array();
			$register_id = $v["register_id"];
			$q = "SELECT p.payment_compare_all_list_id
					FROM payment_compare_all_list AS p
					WHERE p.active='T' 
						AND p.payment_type='{$pay}'
						AND p.register_id={$register_id}
			";	
			$payment_compare_all_list_id = $db->data($q);
			// d($rs_data);die();
			// d($rs_data);
			if ( !empty( $payment_compare_all_list_id ) ) {
				$arg["id"] = $payment_compare_all_list_id;
				$arg["date_update"] = "{$date_now}";
			}else{
				$arg["date_create"] = "{$date_now}";
			}//end else

			$t = $v["register_course_detail_course_detail_id"];
			$arg["table"] = "payment_compare_all_list";
			$arg["register_id"] = $register_id;
			$arg["course_id"] = $v["course_id"];
			$arg["course_detail_id"] = ($t!="") ? $t : $v["course_detail_id"];
			$payment_date = explode(" ", $v["pay_date"]);
			$payment_date = $payment_date[0];
			$arg["payment_date"] = $payment_date;
			$arg["payment_type"] = "{$pay}";
			$arg["amount"] = (float)$v["pay_price"];
			$arg["check_result"] = 'Y';
			$arg["recby_id"] = (int)$EMPID;
			$arg["active"] = 'T';
			// d($arg);
			$rs_add = $db->set($arg);
			// d($arg);
			// die();
		}//end loop $v

		break;
	case 'paysbuy':
		$con_pay .= " AND p.payment_type='paysbuy'";
		$pay_txt = "Paysbuy";

		$q = "SELECT r.register_id
					, r.course_id
					, r.pay_price
					, r.pay_date
					, b.register_course_detail_course_detail_id
					, b.course_detail_id
				FROM register AS r 
				INNER JOIN view_register_course_detail b ON r.register_id=b.view_register_course_detail_id
				WHERE r.active='T' AND r.auto_del='F' AND r.pay_status=3 
					AND (r.pay='paysbuy')
					AND r.pay_date BETWEEN '{$date_start_conv} 00:00:00' AND '{$date_stop_conv} 23:59:59'
				ORDER BY r.pay_date DESC	
		";
		$reg_info = $db->get($q);	
		
		// d($reg_info);die();
		//echo $q;
		foreach ($reg_info as $key => $v) {
			$arg = array();
			$register_id = $v["register_id"];
			$q = "SELECT p.payment_compare_all_list_id
					FROM payment_compare_all_list AS p
					WHERE p.active='T' 
						AND p.payment_type='{$pay}'
						AND p.register_id={$register_id}
			";	
			$payment_compare_all_list_id = $db->data($q);
			// d($rs_data);die();
			// d($rs_data);
			if ( !empty( $payment_compare_all_list_id ) ) {
				$arg["id"] = $payment_compare_all_list_id;
				$arg["date_update"] = "{$date_now}";
			}else{
				$arg["date_create"] = "{$date_now}";
			}//end else
			$t = $v["register_course_detail_course_detail_id"];

			$arg["table"] = "payment_compare_all_list";
			$arg["register_id"] = $register_id;
			$arg["course_detail_id"] = ($t!="") ? $t : $v["course_detail_id"];
			$arg["course_id"] = $v["course_id"];
			$payment_date = explode(" ", $v["pay_date"]);
			$payment_date = $payment_date[0];
			$arg["payment_date"] = $payment_date;
			$arg["payment_type"] = "{$pay}";
			$arg["amount"] = (float)$v["pay_price"];
			$arg["check_result"] = 'Y';
			$arg["recby_id"] = (int)$EMPID;
			$arg["active"] = 'T';
			// d($arg);
			$rs_add = $db->set($arg);
			// d($arg);
			// die();
		}//end loop $v
		
		break;
	case 'bbl':
		$con_pay .= " AND p.payment_type='bank' AND p.bank_code='002'";
		$pay_txt = "BBL";
		break;
	case 'kbank':
		$con_pay .= " AND p.payment_type='bank' AND p.bank_code='004'";
		$pay_txt = "KBANK";
		break;
	case 'mpay':
		$con_pay .= " AND p.payment_type='mpay'";
		$pay_txt = "mPAY";

		$q = "SELECT r.register_id
					, r.course_id
					, r.pay_price
					, r.pay_date
					, b.register_course_detail_course_detail_id
					, b.course_detail_id
				FROM register AS r inner join view_register_course_detail b on r.register_id=b.view_register_course_detail_id
				WHERE r.active='T' AND r.auto_del='F' AND r.pay_status IN (3, 9) 
					AND r.pay='mpay'
					AND r.pay_date BETWEEN '{$date_start_conv} 00:00:00' AND '{$date_stop_conv} 23:59:59'
				ORDER BY r.pay_date DESC	
		";
		$reg_info = $db->get($q);	

		// echo $q;
		// d($reg_info);
/*		
		foreach ($reg_info as $key => $v) {
			$register_id = $v["register_id"];

			$q = "SELECT register_credit_note_id FROM register_credit_note WHERE ref_new_register_id={$register_id}";
			$register_credit_note_id = $db->data($q);
			if ( !empty($register_credit_note_id) ) {
				unset($reg_info[$key]);
				continue;
			}//end if
		}//end lkoop $v
*/
		// d($reg_info);

		$arr_tmp = array();
		foreach ($reg_info as $key => $v) {
			$arr_tmp[$key] = $v;
			$register_id = $v["register_id"];
			$pay_price = $reg_info["pay_price"];

			$q = "SELECT a.payment_mpay_register_list_id
					, a.payment_options_mpay_id 
					, b.name AS payment_options_mpay_name
					, b.charge
					, b.price_unit_id
					, c.name AS price_unit_name
				FROM payment_mpay_register_list AS a
				LEFT JOIN payment_options_mpay AS b ON b.payment_options_mpay_id=a.payment_options_mpay_id
				LEFT JOIN price_unit AS c ON c.price_unit_id=b.price_unit_id
				WHERE a.active='T'
					AND a.register_id={$register_id}
			";
			// echo $q.";";

			$rs = $db->rows($q);
			
			// d($v);
			//d($rs);

			if ( !empty($rs) ) {
				$charge = $rs["charge"];
				$price_unit_id = $rs["price_unit_id"];
				$payment_mpay_register_list_id = $rs["payment_mpay_register_list_id"];
				
				if ( !empty($charge) && !empty($price_unit_id) ) {
					
					/*
					0.321
					9.679
					money_format('%.2n', 12345.67);
					*/

					// echo $v["pay_price"]."|";
					if ( $price_unit_id==2 ) {
						$pay_price = $v["pay_price"];
						$charge = $pay_price*($charge/100);
						// echo $charge."<hr>";
						$charge = number_format($charge, 2, ".", "");
					}else if ( $price_unit_id==1 ) {
						$charge = number_format($charge, 2, ".", "");
					}//end else if

					$arg = array();
					$arg["table"] = "payment_mpay_register_list";
					$arg["id"] = $payment_mpay_register_list_id;
					$arg["charge"] = $charge;
					$db->set($arg);

				}//end if
				$arr_tmp[$key]["fee"] = !empty($charge) ? $charge : 0;
			}//end if
			// die();
		}//end loop $v
		// d($arr_tmp);
		$reg_info = $arr_tmp;
		// d($reg_info);
		// die();



		foreach ($reg_info as $key => $v) {
			// d($v); die();
			$arg = array();
			$register_id = $v["register_id"];
			$pay_status = $v["pay_status"];

			$q = "SELECT register_credit_note_id 
				FROM register_credit_note 
				WHERE ref_new_register_id={$register_id}
			";
			$ref_new_register_id = $db->data($q);
			if ( !empty($ref_new_register_id) ) {
				continue;
			}//end if


			$q = "SELECT p.payment_compare_all_list_id
					FROM payment_compare_all_list AS p
					WHERE p.active='T' 
						AND p.payment_type='{$pay}'
						AND p.register_id={$register_id}
			";	
			$payment_compare_all_list_id = $db->data($q);
			// d($rs_data);die();
			// d($rs_data);
			if ( !empty( $payment_compare_all_list_id ) ) {
				$arg["id"] = $payment_compare_all_list_id;
				$arg["date_update"] = "{$date_now}";
			}else{
				$arg["date_create"] = "{$date_now}";
			}//end else

			$t = $v["register_course_detail_course_detail_id"];
			$arg["table"] = "payment_compare_all_list";
			$arg["register_id"] = $register_id;
			$arg["course_id"] = $v["course_id"];
			$arg["course_detail_id"] = ($t!="") ? $t : $v["course_detail_id"];
			$payment_date = explode(" ", $v["pay_date"]);
			$payment_date = $payment_date[0];
			$arg["payment_date"] = $payment_date;
			$arg["fee"] = (float)$v["fee"];
			$arg["payment_type"] = "{$pay}";
			$arg["amount"] = (float)$v["pay_price"];
			$arg["check_result"] = 'Y';
			$arg["recby_id"] = (int)$EMPID;
			$arg["active"] = 'T';
			// d($arg);
			$rs_add = $db->set($arg);
			// $db->set($arg, true, true);
			// echo ";<br>";
			// die();
		}//end loop $v

		break;	
}//end sw

?>


<!DOCTYPE html>
<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>สรุปรายการสอบและอบรม</title>
<link href="css/printform-style.css" rel="stylesheet" type="text/css" media="all" />
<style>
	@media print{
		body{
			padding: 0px;
			margin: 0px;
		}
		#btPrint{
			display: none;
		}
	}
	.rpt-print{
		table-layout: fixed;
		/*width: 1199;*/
		/*width: 100%;*/
		word-wrap:break-word;
	}
</style>
<!-- <body> -->
<body onload="receipt_tax_export()">
	<!-- <div class="form-portrait"> -->
	<div class="form-landscape">
		<div>
			<table class="no-border">
				<tbody>
					<tr>
						<td><span class="center font-weight-bold"><?php echo "รายงานสรุปยอดจากรายการ{$coursetype_name}<br>ประเภทใบอนุญาต/คุณวุฒิ {$section_name}";?></span></td>
					</tr>
					<tr>
						<td><span class="center"><?php echo "วันที่ {$date_start} ถึง {$date_stop}";?></span></td>
					</tr>
				</tbody>
			</table>
			<table class="td-center rpt-print" width=1200 style='table-layout:fixed'>
				<!-- <col width=20> -->

				<col width=40>
				<col width=70>
				<col width=120>
				<col width=40>
				<col width=40>

				<col width=40>
				<col width=40>
				<col width=40>
				<!-- <col width=40> -->
				<col width=40>

				<col width=40>
				<col width=40>

				<thead>
					<tr>
						<td colspan="11"><span class="left font-weight-bold">ช่องทาง <?php echo $pay_txt;?></span></td>
					</tr>
					<tr>
						<!-- <td ><span class="center">ลำดับ</span></td>	 -->				
						<td ><span class="center">วันที่</span></td>
						<td ><span class="center">Project code</span></td>
						<td ><span class="center">รายวิชา</span></td>
						<td ><span class="center">จำนวน</span></td>
						<td ><span class="center">ก่อน VAT</span></td>

						<td ><span class="center">VAT</span></td>
						<td ><span class="center">จำนวนเงิน</span></td>
						<td ><span class="center">รวม</span></td>
						<!-- <td ><span class="center">ส่วนลด</span></td> -->
						<td ><span class="center">หัก 3.21%</span></td>

						<td ><span class="center">รวมเป็นเงิน</span></td>
						<td ><span class="center">ช่องทางชำระ</span></td>
					</tr>
				</thead>
			
<?php

$sql = "SELECT DISTINCT(p.payment_date) AS payment_date
		FROM payment_compare_all_list AS p
		INNER JOIN register AS r ON p.register_id=r.register_id
		WHERE p.active='T' AND r.active='T'
			AND p.check_result='Y' 
			AND p.payment_date BETWEEN '{$date_start_conv}' AND '{$date_stop_conv}'
			{$con_pay}
			{$cond_cos}
		ORDER BY p.payment_date ASC		
";


$rs = $db->get($sql);
// d($rs);
// die();

if ( !empty($code_project) ) {
	$code_project =  strtoupper($code_project);
	$cond_cos .= " AND UPPER(d.code_project) LIKE '{$code_project}%'";
}

$ttl_registed = 0;
$ttl_course_price_before_vat = 0;
$ttl_course_price_vat = 0;
$sum_course_price = 0;
$ttl_sum_course_price = 0;
$ttl_sum_fee = 0;
$ttl_sum_amount = 0;
// $runno=0;
//get data for built rpt
foreach ($rs as $key => $c) {
	// $runno++;
	$payment_date = $c["payment_date"];
	$sql = "SELECT COUNT(p.payment_compare_all_list_id) AS cnt_reg_id
				, p.payment_date
				, p.course_id
				, SUM(p.amount) AS ttl_amount
				-- , SUM(r.pay_diff) AS ttl_fee
				, SUM(p.fee) AS ttl_fee
				, r.course_detail_id
				, d.code_project
				, r.course_price
				, r.course_discount
				, r.course_discount
				, r.pay_method
				, m.name AS mpay_type_name
			FROM payment_compare_all_list AS p
			INNER JOIN register AS r ON p.register_id=r.register_id
			INNER JOIN course_detail AS d ON p.course_detail_id=d.course_detail_id
			LEFT JOIN payment_options_mpay AS m ON m.payment_options_mpay_id=r.pay_method
			WHERE p.active='T' 			
				AND p.check_result='Y' 		
				AND p.payment_date='{$payment_date}'
				{$con_pay}
				{$cond_cos}				
			GROUP BY r.pay_method, d.code_project, p.course_id
			ORDER BY d.code_project ASC				 	
	";
	//-- AND (p.course_id<>'' OR p.course_id IS NOT NULL) 
	$rs = $db->get($sql);

	// echo $sql;
	// d($rs);
	// die();

	foreach ($rs as $key => $v) {
		$cos_dt_id="";
		$course_ids = trim($v["course_id"]);
		if ( empty($course_ids) ) {continue;}
		$course_ids = explode(",", $course_ids);
		$count_course_ids = count($course_ids);
		// echo "cnt cos ids = ".$count_course_ids."<br>";

		$payment_date = explode("-", $v["payment_date"]);
		$payment_date_yyyymmdd = "{$payment_date[0]}{$payment_date[1]}{$payment_date[2]}";

		$course_title = "";
		$course_code_project = "";
		$course_price = 0;
		$course_discount = 0;
		$ttl_course_price = 0;
		$course_price_before_vat = 0;
		$course_price_vat = 0;

		if ( $count_course_ids>1 ) {
			// d($course_ids);

			foreach ($course_ids as $key => $id) {
				$id = trim($id);
				// echo $d."<br>";
				$info_course = get_course("", $id);
				$info_course = $info_course[0];

				if ( !empty($info_course["code_project"])&&isset($info_course["code_project"]) ) { //chk cos refresher 
					if( $coursetype_id==4 || $coursetype_id=='4' ){
						$course_title .= '-<hr>';
					}else{
						$course_title .= $info_course["title"]."<hr>";
					}//end else
				}elseif ( empty($info_course["code_project"]) && ($coursetype_id!=4 || $coursetype_id!='4') ) {
					$course_title .= $info_course["title"]."<hr>";
				}//end elseif

			}//end loop $d
			$course_price = ($v["course_price"]-$v["course_discount"]);
			$ttl_course_price = $course_price*$v["cnt_reg_id"];
			//$t = ',1033:5535,1034:5536';


		}else{
			$info_course = get_course("", $course_ids[0]);
			$info_course = $info_course[0];
			// d($info_course);

			$t = trim($v["course_detail_id"], ",");
			$x = explode(":", $t);
			$cos_dt_id = (empty($x[1])) ? $v["course_detail_id"] : $x[1];

			$course_price = $info_course["price"]-$info_course["discount"];
			$ttl_course_price = $course_price*$v["cnt_reg_id"];
			
			if ( !empty($info_course["code_project"])&&isset($info_course["code_project"]) ) { //cos refresher 
				if( $coursetype_id==4 || $coursetype_id=='4' ){
					$course_title .= '-<hr>';
				}else{
					$course_title .= $info_course["title"]."<hr>";
				}//end else
			}elseif ( empty($info_course["code_project"]) && ($coursetype_id!=4 || $coursetype_id!='4') ) {
				$course_title .= $info_course["title"]."<hr>";
			}//end elseif

		}//end else

		$course_code_project = trim($v["code_project"], "");
		$course_title = trim($course_title, "<hr>");

		$course_price_before_vat =  $course_price-($course_price*0.06542);
		$course_price_vat = $course_price-$course_price_before_vat;
		$ttl_fee = (!empty($v["ttl_fee"])) ? $v["ttl_fee"] : 0;
		
		//chk course angry bird
		if ( $ttl_course_price != $v["ttl_amount"] ) {
			// $course_discount = $ttl_course_price-$v["ttl_amount"];
			$course_price = $v["ttl_amount"]/$v["cnt_reg_id"];
			$course_price_before_vat =  $course_price-($course_price*0.06542);
			$course_price_vat = $course_price-$course_price_before_vat;
			$ttl_course_price = $v["ttl_amount"];

		}//end if
		
		$ttl_amount = $v["ttl_amount"] - (float)$ttl_fee;

		$cos_price = number_format($course_price, 2, ".", "");
		$before_vat = $course_price_before_vat;
		$vat = $course_price_vat;

		$vat = number_format($vat, 2, ".", "");
		$before_vat = number_format($before_vat, 2, ".", "");
		$sum = number_format($before_vat+$vat, 2, ".", "");

		if ( $cos_price != $sum ) { 
			$before_vat = $cos_price-$vat;
			$sum = number_format($before_vat+$vat, 2, ".", "");
			$course_price_before_vat = $before_vat;
			$course_price_vat = $vat;
			// echo "cos_price = $cos_price | before_vat = $before_vat | vat = $vat | sum = $sum <br>";
		}//end if
?>

				<tbody>
					<tr>
						<!-- <td ><span class="center"><?php //echo $runno;?></span></td> -->
						<td ><span class="center"><?php echo $payment_date_yyyymmdd;?></span></td>
						<td ><span class="center"><?php echo $course_code_project;?></span></td>
						<td ><span class="left"><?php echo $course_title;?></span></td>
						<td ><span class="center"><?php echo $v["cnt_reg_id"];?></span></td>
						<td ><span class="right"><?php echo number_format($course_price_before_vat, 2, '.', ',');?></span></td>
						<td ><span class="right"><?php echo number_format($course_price_vat, 2, '.', ',');?></span></td>
						<td ><span class="right"><?php echo number_format($course_price, 2, '.', ',');?></span></td>
						<td ><span class="right"><?php echo number_format($ttl_course_price, 2, '.', ',');?></span></td>
						<td ><span class="right"><?php echo number_format($ttl_fee, 2, '.', ',');?></span></td>
						<td ><span class="right"><?php echo number_format($ttl_amount, 2, '.', ',');?></span></td>
						<td ><span class="center"><?php echo $v["mpay_type_name"];?></span></td>						
					</tr>
				</tbody>

<?php
	$ttl_registed = $ttl_registed+$v["cnt_reg_id"];
	$ttl_course_price_before_vat = $ttl_course_price_before_vat + $course_price_before_vat;
	$ttl_course_price_vat = $ttl_course_price_vat+$course_price_vat;
	$sum_course_price = $sum_course_price+$course_price;
	$ttl_sum_course_price = $ttl_sum_course_price+$ttl_course_price;
	$ttl_sum_fee = $ttl_sum_fee+$ttl_fee;
	$ttl_sum_amount = $ttl_sum_amount+$ttl_amount;
	}//end loop $v
	
}//end loop $c
// die();
?>
				<tfoot>
					<tr>
						<td colspan="3" rowspan="" headers="" class="center">รวม</td>
						<td colspan="" rowspan="" headers="" class="center"><?php echo number_format($ttl_registed);?></td>
						<td colspan="" rowspan="" headers="" class="right"><?php echo number_format($ttl_course_price_before_vat, 2 ,".", ",");?></td>
						<td colspan="" rowspan="" headers="" class="right"><?php echo number_format($ttl_course_price_vat, 2, ".", ",");?></td>
						<td colspan="" rowspan="" headers="" class="right"><?php echo number_format($sum_course_price, 2, ".", ",");?></td>
						<td colspan="" rowspan="" headers="" class="right"><?php echo number_format($ttl_sum_course_price, 2, ".", ",");?></td>
						<td colspan="" rowspan="" headers="" class="right"><?php echo number_format($ttl_sum_fee, 2, ".", ",");?></td>
						<td colspan="" rowspan="" headers="" class="right"><?php echo number_format($ttl_sum_amount, 2, ".", ",");?></td>
						<td colspan="" rowspan="" headers=""></td>
					</tr>
				</tfoot>

			</table>
		</div>
		<br>
	</div>

</html>	

<script type="text/javascript">
function printPage(){
   window.print();
   setTimeout(" parent.$.fancybox.close()",1000);
}

function receipt_tax_export(){
	var btn_confirm = confirm("หากต้องการ Export เป็นไฟล์ Excel ให้เลือก \"OK\"");
	if (btn_confirm == true) {
		var date_start = "<?php echo $date_start_conv; ?>";
		var date_stop = "<?php echo $date_stop_conv; ?>";
		var coursetype_id = "<?php echo $coursetype_id; ?>";
		var section_id = "<?php echo $section_id; ?>";
		var code_project = "<?php echo $code_project; ?>";
		var pay = "<?php echo $pay; ?>";
		url = "./report/report-coursetype-summary-cost-export-mpay.php?date_start="+date_start+"&date_stop="+date_stop+"&coursetype_id="+coursetype_id+"&pay="+pay+"&section_id="+section_id+"&code_project="+code_project;
		// window.open(url,'_self');
		window.open(url,'_blank');
	} else {
	}
}		
</script>