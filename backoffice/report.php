<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
global $db;

if($_GET["type"]=="select-course-detail"){
	include_once ('report-select-course-detail.php');
}else if($_GET["type"]=="register-info"){
	include_once ('report-register-info.php');
}else if($_GET["type"]=="register-approve"){
	include_once ('report-register-approve.php');
}else if($_GET["type"]=="register-result"){
	include_once ('report-register-result.php');
}else if($_GET["type"]=="register-result-import"){
	include_once ('report-register-result-import.php');
}else if($_GET["type"]=="register-receipt"){
	include_once ('report-register-receipt.php');
}else if($_GET["type"]=="receipt-address"){
	include_once 'register-edit-receipt-address.php';
}else if($_GET["type"]=="receipt-cancel"){
	include_once 'report-receipt-cancel-list.php';
}else if($_GET["type"]=="payment-compare-list"){
	include_once 'payment-compare-list.php';
}else if($_GET["type"]=="payment-compare-bank"){
	include_once 'report-payment-compare-bank.php';
}else if($_GET["type"]=="payment-compare-money"){
	include_once 'report-payment-compare-money.php';
}else if($_GET["type"]=="payment-compare-paysbuy"){
	include_once 'report-payment-compare-paysbuy.php';
}else if($_GET["type"]=="coursetype-summary-cost"){
	include_once 'coursetype-summary-cost.php';
}else if($_GET["type"]=="coursetype-summary-cost-rpt"){
	include_once 'report-coursetype-summary-cost.php';
}else if($_GET["type"]=="coursetype-summary-cost-rpt-wr"){
	include_once 'report-coursetype-summary-cost-wr.php';
}else if($_GET["type"]=="coursetype-summary-cost-rpt-mpay"){
	include_once 'report-coursetype-summary-cost-mpay.php';
}else if($_GET["type"]=="receipt-name"){
	// include_once 'register-edit-receipt-name.php';
	include_once 'register-edit-receipt-name2.php';
}else if($_GET["type"]=="credit-note"){
	include_once 'register-credit-note.php';
}else if($_GET["type"]=="payment-compare-mpay"){
	include_once 'report-payment-compare-mpay.php';
}

else{
	include_once 'report-select-course.php';
}


?>
