<?php
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";

global $db;

if($_POST){
	$args = array();
	$args["table"] = "course_discount";
	if($_POST["course_discount_id"])
	   $args["id"] = $_POST["course_discount_id"];
	$args["code"] = $_POST["code"];
	$args["coursetype_id"] = (int)$_POST["coursetype_id"];
	$args["section_id"] = (int)$_POST["section_id"];
	$args["name"] = $_POST["name"];
	$args["discount"] = (float)$_POST["discount"];
	$args["date_start"] = thai_to_timestamp($_POST["date_start"]);
	$args["date_stop"] = thai_to_timestamp($_POST["date_stop"]);
	$args["active"] = $_POST["active"];
	$args["recby_id"] = (int)$EMPID;
	$args["rectime"] = date("Y-m-d H:i:s");
	
   $ret = $db->set($args);
   $course_discount_id = $args["id"] ? $args["id"] : $ret;

}
$args = array();
$args["p"] = "course";
$args["course_discount_id"] = $course_discount_id;
$args["type"] = "discount-detail";
redirect_url($args);
?>