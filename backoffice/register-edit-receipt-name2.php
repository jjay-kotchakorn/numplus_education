<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
include_once "./share/member.php";
global $db;
global $SECTIONID;
$apay = datatype(" and a.active='T'", "pay_status", true);

$educationlevel = datatype(" and a.active='T'", "educationlevel", true);
$province = datatype(" and a.active='T'", "province", true);
$receipttype = datatype(" and a.active='T'", "receipttype", true);
$university = datatype_university(true);

$register_id = $_GET["register_id"];
if(!$register_id) $register_id = $_POST["register_id"];
// echo $register_id;
$section = datatype(" and a.active='T'", "section", true);
$v = get_register("", $register_id);

$credit_note_date = revert_date(date('Y-m-d'));
$credit_note_date_tt = date('H:i');

$str = "";
if(!empty($v)){
	$v = $v[0];
	$info = $v;
	
	// d($info);
	
	$str = "ใบเสร็จเลขที่ ".$v["docno"];
	$register_id = $v["register_id"];
	$coursetype_id = $v["coursetype_id"];
	$section_id = $v["section_id"];

	$remark_new = "ออกใบเสร็จใหม่จากเดิม ".$str;

	if($info["slip_type"]=="individuals"){
		$slip_type_text = "ออกในนามบุคคลธรรมดา";
		$chk_receipt_type_indy = 'checked="checked"';	
		$chk_receipt_type_corp = "";
	} 
	if($info["slip_type"]=="corparation"){
		$slip_type_text = "ออกในนามนิติบุคคล (สำหรับเบิกค่าใช้จ่าย)";
		$chk_receipt_type_corp = 'checked="checked"';
		$chk_receipt_type_indy = "";
	} 
/*
	$q = "select name from receipttype where receipttype_id={$info["receipttype_id"]}";
	$receipttype_name = $db->data($q);
	$q = "select name from district where district_id={$info["receipt_district_id"]}";
	$district_name = $db->data($q);
	$q = "select name from amphur where amphur_id={$info["receipt_amphur_id"]}";
	$amphur_name = $db->data($q);
	$q = "select name from province where province_id={$info["receipt_province_id"]}";
	$province_name = $db->data($q);
	$q = "select register_id from register a where a.active='T' and  a.course_id='$course_id'";
	$id = $db->data($q);
	$total_price = $price - $discount;
	$t = convert_mktime($info["rectime"]) + (2 * 24 * 60 * 60);
	*/
}//end if	

?>
<link rel="stylesheet" type="text/css" href="js/jquery.nanoscroller/nanoscroller.css" />
<link href="js/jquery.icheck/skins/square/blue.css" rel="stylesheet">

<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="col-sm-12">
				<div class="content block-flat ">
					<div class="header">              
						<ol class="breadcrumb">
							<li><a href="#" onClick="clearPage('report&type=register-receipt');">หน้าหลัก</a></li>
							<li class="active"><?php echo $str; ?></li>
						</ol>
					</div>
					<form id="frmMain" action="update-receipt-name.php" method="POST" accept-charset="utf-8">
						
						<input type="hidden" name="register_id" id="register_id" value="<?php echo $register_id; ?>">

						<div class="header">							
							<h4><i class="fa fa-pencil"></i>&nbsp;แก้ไขข้อมูลสำหรับออกใบเสร็จใบใหม่</h4>
												
							<div class="form-group row">
								<label class="col-sm-2 control-label" style="padding-right:0px; text-align: center;">วันที่ใช้แสดงบนใบเสร็จ</label>
								<div class="col-sm-1" style="padding-left:0px; padding-right:10px;">
									<input class="form-control" name="receipt_date" value="<?php echo $credit_note_date; ?>" id="receipt_date" placeholder="เลือกวันที่" type="text">
								</div>   
								<div class="col-sm-1" style="padding-left:0px; padding-right:10px;">
									<input class="form-control" name="receipt_date_tt" id="receipt_date_tt" value="<?php echo $credit_note_date_tt;?>" placeholder="เวลา" type="text" style="width:50px; display:inline-block;">
								</div>
								<span class="red">( วันที่ทำรายการ )</span>                            
							</div>			

							<div class="form-group row">
								<label class="col-sm-2 control-label" style="padding-right:0px; text-align: center;">หมายเหตุ</label>
								<div class="col-sm-7" style="padding-left:0px; padding-right:0px;">
									<textarea id="remark" name="remark" class="form-control" placeholder="หมายเหตุ"><?php echo $remark_new ?></textarea>
								</div>		
							</div>		
						</div>

					

						<h4><i class="fa fa-money"></i>&nbsp;ได้รับเงินจาก</h4>	
						<div class="form-group row">	
									
							<div class="col-sm-12">			
<!--  												
								<label class="radio-inline" onclick="receipt_check('old')" style="padding-left:10px;padding-top:0px"> 
									<div aria-disabled="false" aria-checked="false" style="position: relative;" class="iradio_square-blue">
										<input style="position: absolute; opacity: 0;" name="use_slip" class="icheck required" type="radio" value="old">
											<ins style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;" class="iCheck-helper">
											</ins>
									</div> ใช้ข้อมูลเดิม <span class="red">*</span> 
								</label> 
 -->
								<label class="radio-inline" onclick="receipt_check('new')" style="padding-left:10px;padding-top:0px; visibility: hidden;"> 
									<div aria-disabled="false" aria-checked="false" style="position: relative; visibility: hidden;" class="iradio_square-blue">
										<input style="position: absolute; opacity: 0;" name="use_slip" class="icheck required" type="radio" value="new" checked="checked">
											<ins style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;" class="iCheck-helper">
											</ins>
									</div> เปลี่ยนแปลงข้อมูลใหม่ <span class="red">*</span> 
								</label> 
							</div>
							
						</div>



						<div id="receipt_address">					
							
						
							<div class="form-group row">	
										
								<div class="col-sm-12">								
									<label class="radio-inline"  onclick="corparation_click(this)" style="padding-left:10px;padding-top:0px"> 
										<div aria-disabled="false" aria-checked="false" style="position: relative;" class="iradio_square-blue">
											<input style="position: absolute; opacity: 0;" value="individuals" name="slip_type" class="icheck required" type="radio" <?php echo $chk_receipt_type_indy;?> >
												<ins style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;" class="iCheck-helper">
												</ins>
										</div> ออกในนามบุคคลธรรมดา <span class="red">*</span> 
									</label> 

									<label class="radio-inline" onclick="corparation_click(this)" style="padding-left:10px;padding-top:0px"> 
										<div aria-disabled="false" aria-checked="false" style="position: relative;" class="iradio_square-blue">
											<input style="position: absolute; opacity: 0;" value="corparation" name="slip_type" class="icheck required" type="radio" <?php echo $chk_receipt_type_corp;?>>
												<ins style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;" class="iCheck-helper">
												</ins>
										</div> ออกในนามนิติบุคคล (สำหรับเบิกค่าใช้จ่าย) <span class="red">*</span> 
									</label> 
								</div>

							</div>


							<div id="corparationinfo" style="display: none;">										
								<div style="display:inline-block;margin-left:15px;" class="form-group row">
									<?php foreach ($receipttype as $key => $value) {
										$click = "getDropDown('#receipt_id', '{$value["receipttype_id"]}', 'receipt', 'data/receipt.php')";
										echo '<label class="radio-inline" onclick="'.$click.'" style="padding-left:10px;padding-top:0px"> <div aria-disabled="false" aria-checked="false" style="position: relative;" class="iradio_square-blue"><input style="position: absolute; opacity: 0;" value="'.$value["receipttype_id"].'" name="receipttype_id"  class="icheck" type="radio"><ins style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;" class="iCheck-helper"></ins></div>&nbsp;&nbsp;'.$value["name"].'</label>';
									} ?>
									<label class="radio-inline" style="padding-left:10px;padding-top:0px">
										<input  maxlength="200" class="form-control" name="receipttype_text" id="receipttype_text" type="text">
									</label>
								</div>	
							</div>

							<div  id="section_corparation_receipt" style="display: none;"class="form-group row">
								<label class="control-label col-sm-2">ออกใบเสร็จในนาม</label>
								<label class="radio-inline col-sm-3" style="padding-left:10px;padding-top:0px">
									<select name="receipt_id" class="form-control" id="receipt_id" onchange="receipt_info(this.value);">
										<option value="">กรุณาเลือกจากรายการที่ปรากฎ</option>
									</select>											
								</label>
								<label class="control-label col-sm-1">สาขา </label>									
								<label class="radio-inline col-sm-2" style="padding-left:10px;padding-top:0px">
									<input  name="branch" class="form-control required" id="branch" type="text">											
								</label>
							</div>

							<div id="section_individuals_receipt" class="form-group row" style="display: none;">																
							<label class="control-label col-sm-2">ออกใบเสร็จในนาม</label>
							<div class="col-sm-1" style="padding-right:0px;">
								<select name="receipt_title" id="receipt_title" class="regis_select form-control">
									<option value="นาย">นาย</option>
									<option value="นาง">นาง</option>
									<option value="นางสาว">นางสาว</option>
									<option value="อื่นๆ">อื่นๆ</option>
								</select> </div>
								<label class="radio-inline col-sm-2" style="padding-left:10px;padding-top:0px">				                			                
									<input class="form-control" id="receipt_title_text" name="receipt_title_text" placeholder="คำนำหน้า" type="text" style="max-width:150px; display:none;">
								</label>												                
								<label class="col-sm-1 control-label">ชื่อ</label>
								<div class="col-sm-2">
									<input class="form-control required" name="receipt_fname" id="receipt_fname" placeholder="ชื่อ" type="text">
								</div>			                			                
								<label class="col-sm-1 control-label">นามสกุล</label>
								<div class="col-sm-2">
									<input class="form-control required " name="receipt_lname" id="receipt_lname" placeholder="นามสกุล" type="text">
								</div>

							</div>


							<div class="form-group row">

								<label class="control-label col-sm-2" style="font-size:12px;">เลขที่บัตรประจำตัวผู้เสียภาษี <span class="red">*</span></label>									
								<label class="radio-inline col-sm-3" style="padding-left:10px;padding-top:0px">
									<input class="form-control required" name="taxno" id="taxno" maxlength="13" type="text"> 											
								</label>
							</div>
							<div class="form-group row"> 	
								<label class="col-sm-2 control-label">บ้านเลขที่  <span class="red">*</span></label>
								<div class="col-sm-2">
									<input class="form-control required" name="receipt_no" id="receipt_no" placeholder="บ้านเลขที่" type="text">
								</div>			                
								<label class="col-sm-2 control-label">หมู่บ้าน / คอนโด / อาคาร  </label>
								<div class="col-sm-2">
									<input class="form-control" name="receipt_gno" id="receipt_gno" placeholder="หมู่บ้าน / คอนโด / อาคาร" type="text">
								</div>
								<label class="col-sm-2 control-label">หมู่ที่ </label>
								<div class="col-sm-2">
									<input class="form-control " name="receipt_moo" id="receipt_moo" placeholder="หมู่ที่" type="text">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 control-label">ซอย</label>
								<div class="col-sm-2">
									<input class="form-control " name="receipt_soi" id="receipt_soi" placeholder="ซอย" type="text">
								</div>
								<label class="col-sm-2 control-label">ถนน </label>
								<div class="col-sm-2">
									<input class="form-control " name="receipt_road" id="receipt_road" placeholder="ถนน " type="text">
								</div>
							</div>							  		
							<div class="form-group row">
								<label class="col-sm-2 control-label">จังหวัด  <span class="red">*</span></label>
								<div class="col-sm-2">
									<select name="receipt_province_id" id="receipt_province_id" class="form-control required" onchange="getDropDown('#receipt_amphur_id', this.value, 'amphur', 'data/province.php')">
										<option value="">---- เลือก ----</option>
										<?php foreach ($province as $key => $value) {
											$id = $value['province_id'];
											$name = $value['name'];
											echo  "<option value='$id'>$name</option>";
										} ?>

									</select>
								</div>
								<label class="col-sm-2 control-label">อำเภอ  <span class="red">*</span></label>
								<div class="col-sm-2">
									<select name="receipt_amphur_id"  id="receipt_amphur_id" class="form-control required" onchange="getDropDown('#receipt_district_id', this.value, 'district', 'data/province.php')">
										<option value="">---- เลือก ----</option>								
									</select>
								</div>
								<label class="col-sm-2 control-label">ตำบล/แขวง  <span class="red">*</span></label>
								<div class="col-sm-2">
									<select name="receipt_district_id" id="receipt_district_id" class="form-control required">
										<option value="">---- เลือก ----</option>								
									</select>
								</div>
							</div>
							<div class="form-group row">								
								<label class="col-sm-2 control-label">รหัสไปรษณีย์  <span class="red">*</span></label>
								<div class="col-sm-2">
									<input  id="receipt_postcode" class="form-control required" name="receipt_postcode" maxlength="6" type="text">
								</div>
							</div>

 						</div>	
					</form>
					<div class="clear"></div>
					<br>
					<div class="filters">     
						<div class="btn-group pull-right">
<?php /* 
													<a class="btn btn-small " href="#" onclick="add_to_walkin(<?php echo $course_detail_id; ?>);"><i class="fa fa-user"></i> รับสมัคร Walk-in</a>
													<a class="btn btn-small " href="#" onclick="add_to_book(<?php echo $course_detail_id; ?>);" style="margin-left:30px; margin-right:30px;"><i class="fa fa-tags"></i> สำรองที่นั่ง</a>
													<a class="btn btn-small " href="#" onclick="add_to_import(<?php echo $course_detail_id; ?>);"><i class="fa  fa-list-alt"></i> import การสมัคร </a> 
*/ ?>							 						
						</div>

						
						
						<div class="form-group row" style="padding-left:10px;">
							<div class="col-sm-12">
								<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p'] ?>&type=register-receipt');">กลับสู่หน้าหลัก</button>
								<button type="button" id="bt_save" class="btn btn-primary" onClick="ckSave()">บันทึกข้อมูล</button>									
							</div>
						</div>



					</div>
					<div class="clear"></div>
				</div>
			</div>

		</div>
	</div> 
</div>
<?php include ('inc/js-script.php') ?>

<script type="text/javascript">
	$(document).ready(function() {
		var fv = $("#frmMain").validate();
		var member_id = "<?php echo $info["member_id"]?>";
		if(member_id) viewInfo(member_id);
		$("#birthdate").datepicker({language:'th-th',format:'dd-mm-yyyy'});
/*		$('input').iCheck({
			checkboxClass: 'icheckbox_square-blue checkbox',
			radioClass: 'iradio_square-blue'
		});*/
		$("#receipt_date").datepicker({language:'th-th',format:'dd-mm-yyyy' });

		$('#cid').keypress(function(event){ 
			var keycode = (event.keyCode ? event.keyCode : event.which);
			if(keycode == '13'){
				getCode();
			}
		});
/*
		var slip_type = "<?php //echo $info["slip_type"]?>";
		if ( slip_type=="individuals" ) {
			$("#corparationinfo").hide();
			$("#section_corparation_receipt").hide();
			$("#section_individuals_receipt").show();
			var select = $("#receipt_id");
			var options = select.attr('options');
			$('option', select).remove();
			$(select).append('<option value="">---- เลือก ----</option>');
			// $("#taxno").val('');
			$("#branch").val('');
		}else if( slip_type=="corparation" ){
			$("#corparationinfo").show();
			$("#section_corparation_receipt").show();
			$("#section_individuals_receipt").hide();
			$("#taxno").val('');
			$("#branch").val('');
		}//end else if
	*/
		// $("#receipt_address").hide();
		$("#receipt_address").show();


		//check alert
		$.ajax({
			url: 'data/ajax-check-session.php',
			data: {
				session_name: 'update_register_receipt_name',
				session_type: 'alert',
				destroy: true,
			},
			type: 'GET',
			dataType: 'json',
			success: function(data){
				// console.log(data);
				// location.reload();

				if(data.status=='error'){
					$.gritter.removeAll({
						after_close: function(){
						$.gritter.add({
							position: 'center',
							title: 'Error',
							text: data.msg,
							class_name: 'danger'
						});
						}
					});
				}

				if(data.status=='success'){
					$.gritter.removeAll({
						after_close: function(){
						$.gritter.add({
							position: 'center',
							title: 'success',
							text: data.msg,
							class_name: 'success'
						});
						}
					});
				}
			}// success
		});	 //end ajax

	});//end ready
 

	$(function(){
		$("#title_th").change(function(){
			if($(this).val() == 'นาย'){ 
				$("#title_en").val("Mr");
				$("#title_th_text").hide();
				$("#title_en_text").hide();
				if($("#title_th_text").hasClass('required'))  $("#title_th_text").removeClass('required');
				if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
				$("#gender").val("M");
			}
			else if($(this).val() == 'นาง'){ 
				$("#title_en").val("Mrs");
				$("#title_th_text").hide();
				$("#title_en_text").hide();
				if($("#title_th_text").hasClass('required'))  $("#title_th_text").removeClass('required');
				if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
				$("#gender").val("F");
			}
			else if($(this).val() == 'นางสาว'){ 
				$("#title_en").val("Ms");
				$("#title_th_text").hide();
				$("#title_en_text").hide();
				if($("#title_th_text").hasClass('required'))  $("#title_th_text").removeClass('required');
				if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
				$("#gender").val("F");
			}
			else if($(this).val() == 'อื่นๆ'){ 
				$("#title_en").val("Other");
				$("#title_th_text").show();
				$("#title_en_text").show();
				$("#title_th_text").addClass('required');
				$("#title_en_text").addClass('required');
				$("#gender").val("");
			}
		});

		$("#title_en").change(function(){
			if($(this).val() == 'Mr'){ 
				$("#title_en_text").hide();
				if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
				$("#gender").val("M");
			}
			else if($(this).val() == 'Mrs'){ 
				$("#title_en_text").hide();
				if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
				$("#gender").val("F");
			}
			else if($(this).val() == 'Ms'){ 
				$("#title_en_text").hide();
				if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
				$("#gender").val("F");
			}
			else if($(this).val() == 'Other'){ 
				$("#title_en_text").show();
				$("#title_en_text").addClass('required');
				$("#gender").val("");
			}
		});
		$("#receipt_title").change(function(){
			if($(this).val() == 'นาย'){ 
				$("#receipt_title_text").hide();
				if($("#receipt_title_text").hasClass('required'))  $("#receipt_title_text").removeClass('required');
			}
			else if($(this).val() == 'นาง'){ 
				$("#receipt_title_text").hide();
				if($("#receipt_title_text").hasClass('required'))  $("#receipt_title_text").removeClass('required');
			}
			else if($(this).val() == 'นางสาว'){ 
				$("#receipt_title_text").hide();
				if($("#receipt_title_text").hasClass('required'))  $("#receipt_title_text").removeClass('required');
			}
			else if($(this).val() == 'อื่นๆ'){ 
				$("#receipt_title_text").show();
				$("#receipt_title_text").addClass('required');
			}
		});
		$(".radio-inline").click(function(){
			$(this).find($("input[type=radio][name='slip_type']")).change(function(){   
				var t = $(this).val();
				if(t == 'corparation'){ 
					$("#taxno").addClass('required');				
				}else{ 
					if($("#taxno").hasClass('required'))  $("#taxno").removeClass('required');
				}
			});
		});
		$('input[name=show_pass]').on('ifChecked', function(event){
			$("#password, #password2").attr("type", "text");
		});			
		$('input[name=show_pass]').on('ifUnchecked', function(event){
			$("#password, #password2").attr("type", "password");
		});	

		$("input[name='receipttype_id']").on('ifChanged', function(event){
			if($(this).val() == '5'){ 
				$("#receipttype_text").addClass('required');
				$("#section_corparation_receipt").hide();
			}else{ 
				if($("#receipttype_text").hasClass('required'))  $("#receipttype_text").removeClass('required');
				$("#section_corparation_receipt").show();
			}
			$("#taxno").val('');
			$("#branch").val('');
			checkboxaddress_reset();
		});
		$('input[name=usebook]').on('ifChecked', function(event){
			$(this).val("T");
		});		
		$('input[name=usebook]').on('ifUnchecked', function(event){
			$(this).val("F");
			//checkboxaddress_reset();
		});

});



function viewInfo(member_id){
	if(typeof member_id=="undefined") return;
	getmemberInfo(member_id);
}

function getmemberInfo(id){
	if(typeof id=="undefined") return;
	var url = "data/memberlist.php";
	var param = "member_id="+id+"&single=T";
	$.ajax( {
		"dataType":'json', 
		"type": "POST",
		"async": false, 
		"url": url,
		"data": param, 
		"success": function(data){	
			$.each(data, function(index, array){    	     
				console.log(array);
				$("#taxno").val(array.taxno);
				$("#branch").val(array.branch);
				$("#receipt_no").val(array.no);
				$("#receipt_gno").val(array.gno);
				$("#receipt_moo").val(array.moo);
				$("#receipt_soi").val(array.soi);
				$("#receipt_road").val(array.road);
				$("#receipt_postcode").val(array.postcode);

				// $("#receipt_title").val(array.receipt_title);
				$("#receipt_title").val(array.receipt_title);
				$("#receipt_fname").val(array.receipt_fname);
				$("#receipt_lname").val(array.receipt_lname);
				$("#receipt_title_text").val(array.receipt_title_text);

				if ( array.slip_type == "individuals" ) {
					$("#taxno").val(array.cid);
				}//end if

				var t = array.slip_type;
				// alert(t);
				corparation_info(t);
				$("select[name=receipt_province_id] option").filter(function() {
					return $(this).val() == array.province_id; 
				}).attr('selected', true);	
				var select = $("#receipt_amphur_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.receipt_amphur_id+'"> '+array.receipt_amphur_name+' </option>');	
				var select = $("#receipt_district_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.receipt_district_id+'"> '+array.receipt_district_name+' </option>');				
				var select = $("#amphur_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.amphur_id+'"> '+array.amphur_name+' </option>');	
				var select = $("#district_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.district_id+'"> '+array.district_name+' </option>');
				var select = $("#receipt_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.receipt_id+'"> '+array.receipt_name+' </option>');
				setVal("#frmMain", array);
				var select = $("#org_name");
				$('option', select).remove();
				$(select).append('<option value="'+array.org_name+'"> '+array.org_name_text+' </option>'); 
				if(!array.org_name_text){
					array.org_name_text = '-';
				}
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-blue checkbox',
					radioClass: 'iradio_square-blue'
				});
				if($("#title_th").val() == 'อื่นๆ'){ 
					$("#title_th").val(array.title_th_text);
				}	
				if($("#receipt_title").val() == 'อื่นๆ'){ 
					$("#receipt_title_text").show();
					$("#receipt_title_text").val(array.receipt_title_text);
				}	
					

			});	

			// console.log(array);			 
}
});
}
function corparation_click(tag){
	$(tag).find('input').iCheck('check');
	var desp = $(tag).find('input').val();
	corparation_info(desp);
}
function corparation_info(desp){
	if(desp=="corparation") {
		$("#corparationinfo").show();
		$("#section_corparation_receipt").show();
		$("#section_individuals_receipt").hide();
		// $("#taxno").val('');
		$("#branch").val('');
	}else{
		$("#corparationinfo").hide();
		$("#section_corparation_receipt").hide();
		$("#section_individuals_receipt").show();
		var select = $("#receipt_id");
		var options = select.attr('options');
		$('option', select).remove();
		$(select).append('<option value="">---- เลือก ----</option>');
		// $("#taxno").val('');
		$("#branch").val('');		
	}
	//checkboxaddress_reset();
}

function receipt_info(id){
	if(typeof id=="undefined") return;
	var url = "data/receipt.php";
	var param = "name=receipt_detail&value="+id;
	$.ajax( {
		"dataType":'json', 
		"type": "GET",
		"async": false, 
		"url": url,
		"data": param, 
		"success": function(data){
			// console.log(data);	
			$.each(data, function(index, array){		
				// console.log(array);	 				 	
				$("#taxno").val(array.taxno);
				$("#branch").val(array.branch);
				$("#receipt_no").val(array.no);
				$("#receipt_gno").val(array.gno);
				$("#receipt_moo").val(array.moo);
				$("#receipt_soi").val(array.soi);
				$("#receipt_road").val(array.road);
				$("#receipt_postcode").val(array.postcode);
				$("#receipt_province_id").val(array.province_id);
				$("#receipt_amphur_id").val(array.amphur_id);
				$("#receipt_district_id").val(array.district_id);
				$("select[name=receipt_province_id] option").filter(function() {
					return $(this).val() == array.province_id; 
				}).attr('selected', true);		
				var select = $("#receipt_amphur_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.amphur_id+'"> '+array.amphur_name+' </option>');	
				var select = $("#receipt_district_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.district_id+'"> '+array.district_name+' </option>');					
			});				 
		}
	});
}

function ckSave(){
	var t = confirm("ยืนยันแก้ไขข้อมูลการออกใบเสร็จ");
	if(!t) return false;
	onCkForm("#frmMain");
	$("#frmMain").submit();
}

function checkboxaddress(){
	$("#receipt_title").val($("#title_th").val());
	if($("#receipt_title").val() == 'อื่นๆ'){ 
		$("#receipt_title_text").show();
		$("#receipt_title_text").addClass('required');
		$("#receipt_title_text").val($("#title_th_text").val());
	}else{
		$("#receipt_title_text").hide();
		$("#receipt_title_text").removeClass('required');
		$("#receipt_title_text").val("");
	}
	$("#receipt_fname").val($("#fname_th").val());
	$("#receipt_lname").val($("#lname_th").val());    

	$("#taxno").val($("#cid").val());
	$("#receipt_no").val($("#no").val());
	$("#receipt_gno").val($("#gno").val());
	$("#receipt_moo").val($("#moo").val());
	$("#receipt_soi").val($("#soi").val());
	$("#receipt_road").val($("#road").val());

	$('#receipt_province_id').val($('#province_id').val());

	var select = $("#receipt_amphur_id");
	$('option', select).remove();
	$(select).append('<option value="'+$("#amphur_id").val()+'"> '+ $("#amphur_id").find(":selected").text()+' </option>'); 

	var select = $("#receipt_district_id");
	$('option', select).remove();
	$(select).append('<option value="'+$("#district_id").val()+'"> '+$("#district_id").find(":selected").text()+' </option>');  

	$("#receipt_postcode").val($("#postcode").val());

}

function checkboxaddress_reset()
{
	$("#receipt_title").val("");
	$("#receipt_title_text").hide();
	$("#receipt_title_text").removeClass('required');
	$("#receipt_title_text").val("");
	$("#receipt_fname").val("");
	$("#receipt_lname").val("");    

	$("#taxno").val("");
	$("#receipt_no").val("");
	$("#receipt_gno").val("");
	$("#receipt_moo").val("");
	$("#receipt_soi").val("");
	$("#receipt_road").val("");

	$('#receipt_province_id').val("");

	var select = $("#receipt_amphur_id");
	$('option', select).remove();
	$(select).append('<option value="">---- เลือก ----</option>');

	var select = $("#receipt_district_id");
	$('option', select).remove();
	$(select).append('<option value="">---- เลือก ----</option>');

	$("#receipt_postcode").val("");
}
function change_select_university()
{
	var id = $("#select_university").val().split("-");
	$("#grd_ugrp1").val(id[0]);
	$("#grd_uid1").val(id[1]);
	$("#grd_uname1").val($("#select_university option:selected").text());
}
function getCode(){
	var url = "data/member-info.php";
	var param = "cid="+$('#cid').val();
	$.ajax( {
		"dataType":'json', 
		"type": "POST",
		"async": false, 
		"url": url,
		"data": param, 
		"success": function(data){	
			if(data==""){
				msgError("ไม่พบข้อมูลของ "+$('#cid').val());
			}
			$.each(data, function(index, array){                
				$("#taxno").val(array.taxno);
				$("#branch").val(array.branch);
				$("#receipt_no").val(array.no);
				$("#receipt_gno").val(array.gno);
				$("#receipt_moo").val(array.moo);
				$("#receipt_soi").val(array.soi);
				$("#receipt_road").val(array.road);
				$("#receipt_postcode").val(array.postcode);
				var t = array.slip_type;
				corparation_info(t);
				$("select[name=receipt_province_id] option").filter(function() {
					return $(this).val() == array.province_id; 
				}).attr('selected', true);	
				var select = $("#receipt_amphur_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.receipt_amphur_id+'"> '+array.receipt_amphur_name+' </option>');	
				var select = $("#receipt_district_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.receipt_district_id+'"> '+array.receipt_district_name+' </option>');				
				var select = $("#amphur_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.amphur_id+'"> '+array.amphur_name+' </option>');	
				var select = $("#district_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.district_id+'"> '+array.district_name+' </option>');
				var select = $("#receipt_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.receipt_id+'"> '+array.receipt_name+' </option>');
				setVal("#frmMain", array);
				var select = $("#org_name");
				$('option', select).remove();
				$(select).append('<option value="'+array.org_name+'"> '+array.org_name_text+' </option>'); 
				if(!array.org_name_text){
					array.org_name_text = '-';
				}
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-blue checkbox',
					radioClass: 'iradio_square-blue'
				});			
			});				 
		}
	});
}


function receipt_check(use_slip){
	// var use_slip = $("#use_slip").val();
	// alert(use_slip);
	if ( use_slip=="new" ) {
		$("#receipt_address").show();
	}else if ( use_slip=="old" ) {
		$("#receipt_address").hide();
	}//end if

}//end func

</script>