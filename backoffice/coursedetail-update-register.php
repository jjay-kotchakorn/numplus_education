<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
include_once "./share/member.php";
require_once dirname(__FILE__) . '/PHPExcel.php';
global $db;
	?>

<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">	
		<div class="cl-mcont">
			<div class="row">
				<div class="col-md-12">
					<div class="block-flat">
						<div class="header">							
							<h3>อัพเดทจำนวนผู้มัคร</h3>
						</div>
						<?php 
							$start = date("Y-m-01");
							$stop = date('Y-m-t',strtotime('today'));
							$dateStart = ($_POST["date_start"]) ? thai_to_timestamp($_POST["date_start"]) :  $start;
							$dateStop =  ($_POST["date_stop"]) ? thai_to_timestamp($_POST["date_stop"]) : $stop;
							if ($dateStart || $dateStop) {
							    if (!$dateStart && $dateStop)
							        $dateStart = $dateStop;
							    if (!$dateStop && $dateStart)
							        $dateStop = $dateStart;
							    $t = $dateStart;
							    if ($dateStart > $dateStop) {
							        $dateStart = $dateStop;
							        $dateStop = $t;
							    }
							}
						 ?>							
						<div class="content">
							<form action="" method="post" id="frmMain">							
								<div class="form-group row">
									<label class="col-sm-2 control-label"  style=" padding-right:0px;">วันที่</label>
									<div class="col-sm-2"  style="padding-left:0px; padding-right:0px;">
										<input class="form-control" name="date_start" id="date_start" value="<?php echo revert_date($dateStart); ?>"  placeholder="Date Start" type="text">
									</div>                                          
									<label class="col-sm-1 control-label"  style=" text-align:center;"> - </label>
									<div class="col-sm-2"  style="padding-left:0px; padding-right:0px;">
										<input class="form-control" name="date_stop" id="date_stop" value="<?php echo revert_date($dateStop); ?>" placeholder="Date Stop" type="text">
									</div>
								 	<label class="col-sm-5 control-label">
										<a  class="btn btn-info" onclick="ckSave();"><i class="fa fa-search">&nbsp;เลือก</i></a>&nbsp;&nbsp;
										<a id="bt-sync" style="<?php echo $style ; ?>" class="btn btn-primary pull-right" onclick="update_product();"><i class="fa fa-refresh">&nbsp;&nbsp;อัพเดท</i></a>
									</label>  
								</div>
							</form>
							<div class="progress progress-striped active" id="progressouter">
								<div class="progress-bar progress-bar-success" id="progress">0%</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div id="update-msg">
										
									</div>
								</div>
							</div>	
										<table class="table table-striped" id="tbEmp">
											<thead>
												<tr class="alert alert-success" style="font-weight:bold;">
												    <td width="3%">ลำดับ</td> 
													<td width="8%" class="center">วัน</td>
													<td width="7%" class="center">เวลา</td>
													<td width="3%" class="center">ALL</td>
													<td width="15%" class="center"><?php echo "สถานที่";?></td>
													<td width="8%" class="center">จังหวัด</td>
													<td width="6%" class="center">เปิดรับ</td>
													<td width="6%" class="center">ปิดรับ</td>
													<td width="3%" class="center">จอง</td>
													<td width="3%" class="center">สำรอง</td>
													<td width="3%" class="center">สมัคร</td>
													<td width="3%" class="center">ว่าง</td>
													<td width="3%" class="center">Max</td>
													<td width="6%" class="center">สถานะ</td>
												</tr>
											</thead>
											<tbody>
												<?php 
												$select_year = date("Y");
												$select_month = date("m");
												$sWhere = ($dateStart && $dateStop) ? " and a.date>='$dateStart' and a.date<='$dateStop'" : "";										
												$r = get_course_detail(" and a.active='T' $sWhere");
												$pList = array();	
												if($r){
													$runNo = 1;
													$sum_jong = 0; 
													$sum_book = 0; 
													$sum_reg = 0; 
													$sum_rem = 0; 
													$sum_max = 0; 
													$sum_usebook = 0;
													foreach($r as $k=>$v){
														$course_detail_id = $v["course_detail_id"];
														$pList[] = $course_detail_id;
														$use = $v["sit_all"];//$use = get_use_chair($course_detail_id);
														$chair_all = $v['chair_all'];
														$book = $v["book"];
														$usebook = 0;
														if($book>0) {
															$usebook += $v["book_all"];
															//$usebook += get_use_book($course_detail_id);
														}
														$sit_all = $chair_all - $book - $use;
														if($sit_all<0) $sit_all = 0;
														$status = get_course_status_name($sit_all, $v["status"], $v["coursetype_id"]);
														if($v["date"]<date("Y-m-d") or $v["end_regis"]<date("Y-m-d")){
															$status = "<span style='color:red;'>ปิดรับสมัคร</span>";
														}
														$jong = $v["regis_all"];
														//$jong = get_use_regis($course_detail_id, "1");
														//$reg = get_use_regis($course_detail_id, "3,5,6");
														$reg = $use - $jong;
														$sum_jong += $jong; 
														$sum_book += $book;
														$sum_usebook += $usebook; 
														$sum_reg += $reg; 
														$sum_rem = 0; 
														$sum_max = 0;	
														?>
														<tr style="cursor:pointer;">
															<td class="center"><?php echo $runNo; ?></td>
															<!-- <td class="center"><strong style="font-size:15px;"><?php //echo $v["day"]; ?>. </strong> <?php //echo revert_date($v["date"]); ?></td> -->
															<td class="center"><?php echo $v["day"]; ?>. <?php echo revert_date($v["date"]); ?></td>
															<td  class="center"><?php echo $v["time"]; ?></td>
															<td  class="center"><?php echo ($v["inhouse"]=="T") ? 'IH' : 'WR'; ?></td>
															<td  class="center"><?php echo $v["address_detail"]; ?></td>
															<td  class="center"><?php echo $v["address"]; ?></td>													
															<td  class="center"><?php echo revert_date($v["open_regis"]); ?></td>
															<td  class="center"><?php echo revert_date($v["end_regis"]); ?></td>
															<td  class="center"><?php echo $jong; ?></td>													
															<td  class="center"><?php echo $usebook; ?> (<?php echo (int) $book; ?>)</td>													
															<td  class="center"><?php echo $reg; ?></td>													
															<td  class="center"><?php echo $sit_all; ?></td>													
															<td  class="center"><?php echo $chair_all; ?> </td>													
															<td  class="center"><?php echo $status; ?></td>													
														</tr>
														<?php
														$runNo++;
													} 	
												}
												?>
								</tbody>
							</table>
							<div class="clear"></div>
						</div>
					</div>				
				</div>
			</div>	
		</div>
	</div> 
</div>

<?php include ('inc/js-script.php') ?>
<script>
$(document).ready(function() {
	$("#date_start").datepicker({language:'th-th',format:'dd-mm-yyyy'});
	$("#date_stop").datepicker({language:'th-th',format:'dd-mm-yyyy'});	
	oTable = $("#tbEmp").dataTable({
		"sDom": 'T<"clear">lfrtip',
		"oLanguage": {
			"sInfoEmpty": "",
			"sInfoFiltered": ""
		},
		"oTableTools": {
			"aButtons":  ""
		},
		"bProcessing": true,
		"sPaginationType": "full_numbers",
		"aaSorting": [[ 0, "asc" ]],
		'iDisplayLength': 15,
		"aLengthMenu" : [10, 15, 25, 50, 100 , 1000]
	});

});
function reCall(){
	oTable.fnClearTable( 0 );
	oTable.fnDraw();
}
function ckSave(){
	$("#frmMain").submit();
}

function update_product(){
	var t = confirm("ยืนยันอัพเดท ข้อมูล");
	if(!t) return;
	var pList = <?php echo json_encode($pList); ?>;
	var all = pList.length;
/*	$("#progress").css('width','0%');
	$("#progress").html('0%');	*/ 
/*	$('#model-sync').modal('show');	 */
	   var i = 1;
	   if(all>0){
	   	 $("#tbEmp_wrapper").hide();
	   }
	   $.each(pList, function(index, val) {
			$.ajax( {
				"type": "GET",
				"async": false, 
				"url": "data/single-course-details-register-update.php",
				"data": "id="+val, 
				"success": function(data){
					/*console.log(data);	*/
					var p = (i / all) * 100;
					$("#progress").html(p.toFixed(0)+'%');
					$("#progress").css('width',p.toFixed(0)+'%');
					$("#update-msg").append(i+". "+data);				 
				}
			});		    
		 	i++;
	    });	
	    $("#update-msg").append("Done !");
}	  

</script>