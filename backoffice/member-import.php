<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
include_once "./share/member.php";
require_once dirname(__FILE__) . '/PHPExcel.php';
global $db;
global $SECTIONID, $EMPID, $RIGHTTYPEID;
$apay = datatype(" and a.active='T' and a.pay_status_id in (3,5,6) ", "pay_status", true);

$educationlevel = datatype(" and a.active='T'", "educationlevel", true);
$province = datatype(" and a.active='T'", "province", true);
$receipttype = datatype(" and a.active='T'", "receipttype", true);
$university = datatype_university(true);

$error = $_SESSION["error"]["msg"];
unset($_SESSION["error"]["msg"]);
$success = $_SESSION["success"]["msg"];
unset($_SESSION["success"]["msg"]);

$preview = false;

//$fileName = $_FILES['excelFile']['tmp_name'];

if (!empty($_FILES) && $_POST) {
     
	$uploaddir = 'uploads/'; 
	$file = $uploaddir . basename($_FILES['excelFile']['name']); 
	$name_photo=($_FILES['excelFile']['name']);
	$size=$_FILES['excelFile']['size'];
	if($size>10485760000)
	{
		echo "error file size > 100 MB";
		unlink($_FILES['excelFile']['tmp_name']);
		exit;
	}

	if (move_uploaded_file($_FILES['excelFile']['tmp_name'], $file)) { 
		$fileName = $file;
	    //echo "success"; 
	} else {
		echo "error ".$_FILES['excelFile']['error']." --- ".$_FILES['excelFile']['tmp_name']." %%% ".$file."($size)";
	}  
}
	
if(isset($fileName)) $preview = true;

?>

<link rel="stylesheet" type="text/css" href="js/jquery.nanoscroller/nanoscroller.css" />
<link href="js/jquery.icheck/skins/square/blue.css" rel="stylesheet">
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="row">

				<div class="col-md-12">           
					<div class="block-flat">
						<div class="header">              
							<ol class="breadcrumb">
								<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>');">หน้าหลัก</a></li>
								<li class="active">นำเข้าข้อมูลสมาชิก</li>
							</ol>
						</div>
						<div class="content">
							<?php if($preview===true){ 
								
								$args = array();
								$args[] = "title";
								$args[] = "fname";
								$args[] = "lname";
								$args[] = "cid";
								$args[] = "nation"; 
								$args[] = "slip_type";
								$args[] = "email1"; 
								$args[] = "receipttype_id";
								$args[] = "receipttype_text";
								$args[] = "receipt_id";
								$args[] = "taxno";
								$args[] = "receipt_title";
								$args[] = "receipt_fname";
								$args[] = "receipt_lname";
								$args[] = "receipt_no";
								$args[] = "receipt_gno";
								$args[] = "receipt_moo";
								$args[] = "receipt_soi";
								$args[] = "receipt_road";
								$args[] = "receipt_district_id";
								$args[] = "receipt_amphur_id";
								$args[] = "receipt_province_id";	
								$args[] = "receipt_postcode";  
								$data = array();
								$headBar = "";								
								//error_reporting(E_ALL);
								//ini_set('display_errors','off');

								$objPHPExcel = PHPExcel_IOFactory::load($fileName);
								$temp = array();
								foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
									$worksheetTitle     = $worksheet->getTitle();
													      $highestRow         = $worksheet->getHighestRow(); // e.g. 10
													      $highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
													      $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
													      $nrColumns = ord($highestColumn) - 64;
													      //echo"<div class='box-header '><h2><i class='icon-edit'></i>&nbsp;";
													      $headBar .= "นำเข้าไฟล์ ".$worksheetTitle." ";
													      $headBar .= $nrColumns . ' คอลัมน์ (A-' . $highestColumn . ') ';
													      $headBar .= ' จำนวน ' . ($highestRow-1) . ' เร็คคอร์ด.';
													      //echo "</h2></div>";
													       //echo '<table class="table table-striped">';
													      for ($row = 2; $row <= $highestRow; ++ $row) {
													          //echo '<tr>';
													      	$tr_temp = "<tr>";
													      	$t = array();
													      	for ($col = 0; $col < $highestColumnIndex; ++ $col) {
													      		$cell = $worksheet->getCellByColumnAndRow($col, $row);
													      		$val = $cell->getValue();

													      		$dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
													             //echo '<td>' .$val.'</td>';
													             $tr_temp .= '<td>' .$val.'</td>';
													      		$t[$args[$col]] = $val;
													      	}
													      	$data[] = $t;
													         //echo '</tr>';
													         $tr_temp .= '</tr>';
													         $temp[] = $tr_temp;
													      }
													       //echo '</table>';
													  }
													  if($data){
													  	$count = 0;
													  	$err = 0;
													  	$success = 0;
													  	//$db->begin();
													  	$array_error = array();
													  	$array_success = array();
 														ini_set('max_input_time', 600);
 														ini_set("memory_limit", "400M");
													  	foreach ($data as $key => $field) {													  		
													  		$count++;	
													  		if($count%30==0){
													  			$db = new MySQLDB;
													  		}						
													  		$field["title"] = trim($field["title"]);						  		
													  		if(trim($field["title"])==""){
													  			$err++;
													  			$tr_error_msg = '';
													  			$tr_error_msg = 'คำนำหน้า เป็นค่าว่าง';
													  			$tr_error = '<tr class="alert-danger"><td colspan="24">'.alert_danger($tr_error_msg).'</td></tr>';													  			
													  			$array_error[$key] = $temp[$key].$tr_error;
													  			continue;
													  		}				
													  		$field["fname"] = trim($field["fname"]);								  		
													  		if(trim($field["fname"])==""){
													  			$err++;
													  			$tr_error_msg = '';
													  			$tr_error_msg = 'ชื่อ เป็นค่าว่าง';
													  			$tr_error = '<tr class="alert-danger"><td colspan="24">'.alert_danger($tr_error_msg).'</td></tr>';													  			
													  			$array_error[$key] = $temp[$key].$tr_error;
													  			continue;
													  		}	
													  		$field["lname"] = trim($field["lname"]);											  		
													  		if(trim($field["lname"])==""){
													  			$err++;
													  			$tr_error_msg = '';
													  			$tr_error_msg = 'นามสกุล เป็นค่าว่าง';
													  			$tr_error = '<tr class="alert-danger"><td colspan="24">'.alert_danger($tr_error_msg).'</td></tr>';													  			
													  			$array_error[$key] = $temp[$key].$tr_error;
													  			continue;
													  		}

													  		$perfect_name = false;
													  		if ( preg_match("/^[a-zA-Zก-๊. ]{1,}$/", trim($field["title"]) )
													  			&& preg_match("/^[a-zA-Zก-๊. ]{1,}$/", trim($field["fname"]) ) 
													  			&& preg_match("/^[a-zA-Zก-๊. ]{1,}$/", trim($field["lname"]) ) 
													  			) {
													  			$perfect_name = true;
													  		}
													  		if (!$perfect_name) {
													  			$err++;
													  			$tr_error_msg = '';
													  			$tr_error_msg = 'ตรวจพบอักขระพิเศษใน คำนำหน้าชื่อ, ชื่อ, นามสกุล';
													  			$tr_error = '<tr class="alert-danger"><td colspan="24">'.alert_danger($tr_error_msg).'</td></tr>';													  			
													  			$array_error[$key] = $temp[$key].$tr_error;
													  			continue;
													  		}

													  		if(trim($field["cid"])==""){
													  			$err++;
													  			$tr_error_msg = '';
													  			$tr_error_msg = 'ไม่มีเลขที่บัตรประชาชน';
													  			$tr_error = '<tr class="alert-danger"><td colspan="24">'.alert_danger($tr_error_msg).'</td></tr>';													  			
													  			$array_error[$key] = $temp[$key].$tr_error;
													  			continue;
													  		}
													  		$cid = trim($field["cid"]);
													  		$q = "select member_id from member where cid='$cid'";
													  		$member_id = $db->data($q);													  		
													  		if($field["nation"]=="T" || $field["nation"]==""){
													  			$ck = check_cart_id($cid);
													  			//var_dump($ck);
													  			if(!$ck){
														  			$err++;
														  			$tr_error_msg = '';
														  			$tr_error_msg = 'เลขที่บัตรประชาชน ไม่ถูกต้อง';
														  			$tr_error = '<tr class="alert-danger"><td colspan="24">'.alert_danger($tr_error_msg).'</td></tr>';
														  			//echo "<hr> เลขที่บัตรประชาชน ไม่ถูกต้อง : ID".$cid;
														  			$array_error[$key] = $temp[$key].$tr_error;
														  			continue;	
													  			}
													  		}
													  		if($field["slip_type"]=="1"){
													  			$field["slip_type"] = "individuals";
													  		}else if($field["slip_type"]=="2"){
													  			$field["slip_type"] = "corparation";
													  		}else{
													  			$err++;
													  			//echo "<hr>ไม่มีข้อมูล ประเภทการออกใบเสร็จ ID : ".$cid;
													  			$tr_error_msg = '';
													  			$tr_error_msg = "ไม่มีข้อมูล ประเภทการออกใบเสร็จ";
													  			$tr_error = '<tr class="alert-danger"><td colspan="24">'.alert_danger($tr_error_msg).'</td></tr>';														  			
													  			$array_error[$key] = $temp[$key].$tr_error;
													  			continue;													  			
													  		}
													  		$email = $field["email1"];
													  		if (filter_var($email, FILTER_VALIDATE_EMAIL)) {													  			
													  		}else{
													  			$err++;
													  			//echo "<hr>ไม่มีข้อมูล ประเภทการออกใบเสร็จ ID : ".$cid;
													  			$tr_error_msg = '';
													  			$tr_error_msg = (trim($email)=="") ? "Email เป็นค่าว่าง" : "รูปแบบ Email ไม่ถูกต้อง";
													  			$tr_error = '<tr class="alert-danger"><td colspan="24">'.alert_danger($tr_error_msg).'</td></tr>';														  			
													  			$array_error[$key] = $temp[$key].$tr_error;
													  			continue;													  			
													  		}													  		
												  			$receipt_id = (int)$field["receipt_id"];
												  			if($receipt_id){
												  				$aReceipt = get_receipt("", $receipt_id);
																$receipt_info = $aReceipt[0];
																if($receipt_info){																		
													  				/*stert*/
																	$field["receipt_no"] = $receipt_info["no"];
																	$field["receipt_gno"] = $receipt_info["gno"];
																	$field["receipt_moo"] = $receipt_info["moo"];
																	$field["receipt_soi"] = $receipt_info["soi"];
																	$field["receipt_road"] = $receipt_info["road"];
																	$field["receipt_district_id"] = $receipt_info["district_id"];
																	$field["receipt_amphur_id"] = $receipt_info["amphur_id"];
																	$field["receipt_province_id"] = $receipt_info["province_id"];
																	$field["receipt_postcode"] = $receipt_info["postcode"];
																	$field["taxno"] = $receipt_info["taxno"];
													  				/*stop*/
																}
												  				
												  			}
													  		if($member_id){											  					
													  			//echo "<hr> รหัสประจำตัวประชาชน ".$cid." มีข้อมูลในระบบแล้ว";
													  			$err++;
													  			$tr_error_msg = '';
													  			$tr_error_msg = "รหัสประจำตัวประชาชนมีข้อมูลในระบบแล้ว";
													  			$tr_error = '<tr class="alert-danger"><td colspan="24">'.alert_danger($tr_error_msg).'</td></tr>';														  			
													  			$array_error[$key] = $temp[$key].$tr_error;
													  			continue;													  			
													  		}else{
													  			if($field["title"]=="นาง" || $field["title"]=="นาย" || $field["title"]=="นางสาว"){
													  				$field["title_th"] = $field["title"];													  				
													  			}else{
													  				$field["title_th"] = "อื่นๆ";
													  			}													  			
														  		$args = array();
																$args["table"] = "member";
																$args['username'] = $cid;
																$args['cid'] = $cid;
																$args['password'] = $cid;
																$args['title_th'] = $field["title_th"];	
																if($field["title_th"]=="อื่นๆ")															
																	$args['title_th_text'] = $field["title"];																
																$args['fname_th']  = $field["fname"];
																$args['lname_th']  = $field["lname"];
																$args['email1']  = $field["email1"];
																$args['email2']  = $field["email2"];
																$args['reg_date']  =  date("Y-m-d H:i:s");
																$args['last_update']  =  date("Y-m-d H:i:s");
																$args['slip_type']  = $field["slip_type"];
																$args['receipt_id']   = (int)$field["receipt_id"];
																$args['receipttype_id']   = (int)$field["receipttype_id"];
																$args['receipt_title'] = $field["receipt_title"];
																$args['receipt_title_text'] = $field["receipt_title_text"];
																$args['receipt_fname']  = $field["receipt_fname"];
																$args['receipt_lname']  = $field["receipt_lname"];
																$args['receipt_no']  = $field["receipt_no"];
																$args['receipt_gno']  = $field["receipt_gno"];
																$args['receipt_moo']  = $field["receipt_moo"];
																$args['receipt_soi']  = $field["receipt_soi"];
																$args['receipt_road']  = $field["receipt_road"];
																$args['receipt_district_id']   = (int) $field["receipt_district_id"];
																$args['receipt_amphur_id']   = (int) $field["receipt_amphur_id"];
																$args['receipt_province_id']   = (int) $field["receipt_province_id"];
																$args['receipt_postcode']   = (int)$field["receipt_postcode"];
																$args['nation']   = $field["nation"];
																$args['taxno']   = $field["taxno"];
																$ret = $db->set($args);
																if(!$ret){
																	//echo "<hr> สมัครสมาชิกไม่ได้ ID : ".$cid;
														  			$tr_error_msg = '';
														  			$tr_error_msg = " สมัครสมาชิกไม่ได้ mysql error";
														  			$tr_error = '<tr class="alert-danger"><td colspan="24">'.alert_danger($tr_error_msg).'</td></tr>';														  			
																	//print_r($args);
																	$err++;
														  			$array_error[$key] = $temp[$key].$tr_error;	
																	continue;
																}else{
																	$success++;
																	$array_success[$key] = $temp[$key];
																}																
													  		}		
															$member_id = ($member_id) ? $member_id : $ret; 
													  	}

													  	if($err>0){
													  		//$db->rollback();
													  	}else{
													  		//$db->rollback();
													  		//$db->commit();

													  	}
													  	//print_r($result);
													  }
													  ?>
													  <div class="">
													  	<div class="content">
													  	<?php // print_r($_FILES); ?>
													  	<div class="">
													  			<div class="row" style="margin-top:0px;">
													  				<?php if($err>0){ ?>
													  				<div class="alert alert-danger">
													  					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
													  					<i class="fa fa-times-circle sign"></i><strong>Error!</strong> จำนวนที่ไม่สามารถนำเข้าข้อมูลได้ &nbsp;</strong> <?php echo $err;?> &nbsp;เร็คคอร์ด
													  				</div>
													  				<table class="table">
													  				<thead>												  					
														  				<tr>
														  					<th>คำนำหน้า</th>
														  					<th>ชื่อ</th>
														  					<th>นามสกุล</th>
														  					<th>เลขที่บัตรประชาชน </th>
														  					<th>สัญชาติไทย</th>														  					
														  					<th>ประเภทการออกใบเสร็จ</th>
														  					<th>อีเมล 1</th>
														  					<th>ID ประเภทบริษัท นิติบุคคล</th>
														  					<th>อื่นๆ</th>
														  					<th>ID บริษัทนิติบุคคล</th>
														  					<th>เลขที่บัตรประจำตัวผู้เสียภาษี</th>
														  					<th>คำนำหน้า</th>
														  					<th>ชื่อ</th>
														  					<th>นามสกุล</th>
														  					<th>บ้านเลขที่</th>
														  					<th>หมู่บ้าน</th>
														  					<th>หมู่ที่</th>
														  					<th>ซอย</th>
														  					<th>ถนน</th>
														  					<th>ID จังหวัด</th>
														  					<th>ID อำเภอ</th>
														  					<th>ID ตำบล/แขวง</th>
														  					<th>รหัสไปรษณีย์</th>
														  					<th>จำนวนเงิน</th>
														  				</tr>
													  				</thead>
													  					<?php if(count($array_error)>0){
													  							foreach ($array_error as $key => $value) {
													  								echo $value;
													  							}
													  					} ?>

													  				</table>
													  				<?php } ?>
													  				<?php if($success>0){ ?>
													  				<br>
													  				<div class="alert alert-success">
													  					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
													  					<i class="fa fa-check sign"></i><strong>Success!</strong> จำนวนที่นำเข้าข้อมูลได้ &nbsp;</strong> <?php echo $success;?> &nbsp;เร็คคอร์ด
													  				</div>
													  				
														  				<table class="table">

													  				<thead>												  					
														  				<tr>
														  					<th>คำนำหน้า</th>
														  					<th>ชื่อ</th>
														  					<th>นามสกุล</th>
														  					<th>เลขที่บัตรประชาชน </th>
														  					<th>สัญชาติไทย</th>														  					
														  					<th>ประเภทการออกใบเสร็จ</th>
														  					<th>อีเมล 1</th>
														  					<th>ID ประเภทบริษัท นิติบุคคล</th>
														  					<th>อื่นๆ</th>
														  					<th>ID บริษัทนิติบุคคล</th>
														  					<th>เลขที่บัตรประจำตัวผู้เสียภาษี</th>
														  					<th>คำนำหน้า</th>
														  					<th>ชื่อ</th>
														  					<th>นามสกุล</th>
														  					<th>บ้านเลขที่</th>
														  					<th>หมู่บ้าน</th>
														  					<th>หมู่ที่</th>
														  					<th>ซอย</th>
														  					<th>ถนน</th>
														  					<th>ID จังหวัด</th>
														  					<th>ID อำเภอ</th>
														  					<th>ID ตำบล/แขวง</th>
														  					<th>รหัสไปรษณีย์</th>
														  					<th>จำนวนเงิน</th>
														  				</tr>
													  				</thead>
													  					<?php if(count($array_success)>0){
													  							foreach ($array_success as $key => $value) {
													  								echo $value;
													  							}
													  					} ?>

													  				</table>
													  				<?php } ?>
													  			</div>
													  			
													  		</div>
													  	</div>
														<div class="clear"></div>
														<div class="form-group row" style="padding-left:10px;">
															<div class="col-sm-12">
																<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p'] ?>');">กลับสู่หน้าหลัก</button>																																			
															<button type="button" class="btn btn-primary" onClick="clearPage('<?php echo $_GET['p'] ?>&type=member-import')">นำเข้าข้อมูลใหม่</button>
															</div>
														</div>
													  <?php

							}else{ ?>
							<form id="frmMain" name="frmMain" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="">
								<input type="hidden" name="upload" value="T">
								<div class="col-sm-12">
									<div class="row">
										<div class="col-md-12" >
											<div class="form-group row">
														<label class="col-sm-2 control-label">เลือกไฟล์</label>
												<div class="col-sm-3">
													<div class="input-text">
														 <input type="file" name="excelFile" class="required " data-no-uniform="true">
													</div>
												</div> 
												<div class="col-md-2">
													<a href="documents/register-import.xlsx" class="btn"><i class="fa fa-download"></i> ดาวน์โหลด ตัวอย่างไฟล์</a>
												</div>
											</div> 
											</div>
										</div>
									</div>
								</div>
								<div class="clear"></div>
								<div class="form-group row" style="padding-left:10px;">
									<div class="col-sm-12">
										<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p'] ?>');">กลับสู่หน้าหลัก</button>
										<button type="button" id="bt_save" class="btn btn-primary" onClick="ckSave()">Import & Submit</button>																			
									</div>
								</div>
							</form>
							<?php } ?>
						</div>
					</div>

				</div>
			</div>

		</div>
	</div> 
</div>

<form id="frmDel" name="frmDel" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="">
	<input type="hidden" name="clearAll">
</form>
<?php include_once ('inc/js-script.php'); ?>
<script type="text/javascript">
function ckSave(id){
	var t = confirm("ยืนยันนำเข้าข้อมูลสมาชิก");
	if(!t) return false;
	$("#frmMain").submit();
}
function clear_import_data(){
	var t = confirm("ยืนยันการล้างข้อมูล");
	if(!t) return false;
	$("input[name=clearAll]").val("T");
	$("#frmDel").submit();
}
</script>
<style>
	#set_time-error{
		display: none !important;
	}
	#pay_status-error, #slip_type-error{
		width: 400px;
		margin-top: 21px;
	}
</style>