<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
include_once "./share/member.php";
global $db;
global $SECTIONID;
$apay = datatype(" and a.active='T'", "pay_status", true);

$educationlevel = datatype(" and a.active='T'", "educationlevel", true);
$province = datatype(" and a.active='T'", "province", true);
$receipttype = datatype(" and a.active='T'", "receipttype", true);
$university = datatype_university(true);

$error = $_SESSION["error"]["msg"];
unset($_SESSION["error"]["msg"]);
$success = $_SESSION["success"]["msg"];
unset($_SESSION["success"]["msg"]);

$course_id = $_GET["course_id"];
$course_detail_id = $_GET["course_detail_id"];
$section = datatype(" and a.active='T'", "section", true);
$coursetype = datatype(" and a.active='T'", "coursetype", true);
$q = "select  day, date, time , address, address_detail from course_detail where course_detail_id=$course_detail_id";
$r = $db->rows($q);
$str = "";
if($r){
	$str = $r["day"]." ".revert_date($r["date"])." ".$r["time"]." ".$r["address_detail"]." ".$r["address"];
	$info = get_course("", $course_id);
	if($info) $info = $info[0];
}else{
	$str = "เพิ่มหลักสูตร";
}
$dtail = get_course_detail("", $course_detail_id);
$dtail = $dtail[0];
?>
<link rel="stylesheet" type="text/css" href="js/jquery.nanoscroller/nanoscroller.css" />
<link href="js/jquery.icheck/skins/square/blue.css" rel="stylesheet">

<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="col-sm-12">
				<div class="content block-flat ">
						<div class="header">              
							<ol class="breadcrumb">
								<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>');">หน้าหลัก</a></li>
								<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>&type=select-course');">เลือกหลักสูตร</a></li>
								<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>&course_id=<?php echo $course_id; ?>&type=select-course-detail');">หน้ายอดผู้สมัคร</a></li>
								<li class="active">ข้อมูลการสมัคร</li>
							</ol>
						</div>
						<div class="header">	
							<div class="form-group row">
								<label class="col-sm-6" style="padding-left:15px;" >ประเภทใบอนุญาต/คุณวุฒิ : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $info["section_name"]; ?></label>
								<label class="col-sm-6" style="padding-left:15px;" >ประเภทหลักสูตร : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $info["coursetype_name"]; ?></label>
								<label class="col-sm-12" style="padding-left:15px;" >สถานที่ : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $dtail["address_detail"]." ".$dtail["address"]; ?></label>
								<label class="col-sm-2" style="padding-left:15px;" >วันที่ : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $dtail["date"]; ?></label>
								<label class="col-sm-9" style="padding-left:15px;" >เวลา : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $dtail["time"]; ?></label>
							</div>	
							<div class="form-group row">
								<label class="col-sm-1 control-label" style=" padding-right:0px;">สถานะ<br>การชำระเงิน</label>
								<div class="col-sm-2">
									<select name="sStatus" id="sStatus" class="form-control" onchange="reCall();">
										<option value="">---- เลือก ----</option>
										<?php foreach ($apay as $key => $value) {
											$id = $value['pay_status_id'];
											$s = "";
											if($info["pay_status"]==$id){
												$s = "selected";
											}
											$name = $value['name'];
											echo  "<option value='$id' {$s}>$name</option>";
										} ?>
									</select>
								</div>
								<?php if($i=="xxdfee"){ ?>
									<label class="col-sm-1 control-label">PUB / INH </label>
									<div class="col-sm-1">
										<select name="pub" class="form-control" id="pub" class="required">
											<option value="">ALL</option>
											<option selected="selected" value="T">PUB</option>
											<option value="F">INH</option>
										</select>
									</div>
									<?php } ?>
								<label class="col-sm-1 control-label"  style=" padding-right:0px;">วันที่เริ่ม</label>
								<div class="col-sm-2"  style="padding-left:0px; padding-right:0px;">
									<!-- <input class="form-control" name="date_start" value="<?php echo revert_date(date("Y-m-d")); ?>" id="date_start" onblur="reCall();" placeholder="วันที่เริ่ม" type="text"> -->
									<input class="form-control" name="date_start" value="<?php echo ""; ?>" id="date_start" onblur="reCall();" placeholder="วันที่เริ่ม" type="text">
								</div>                                          
								<label class="col-sm-1 control-label"  style=" padding-right:0px;">วันที่สิ้นสุด</label>
								<div class="col-sm-2"  style="padding-left:0px; padding-right:0px;">
									<input class="form-control" name="date_stop" id="date_stop" onblur="reCall();" placeholder="วันที่สิ้นสุด" type="text">
								</div> 
								<label class="col-sm-1 control-label">
									<a href="#" class="btn" onclick="reCall();"><i class="fa fa-search">&nbsp;</i></a>									
								</label>  
								<label class="col-sm-1 control-label">								
									<a href="#" class="btn" onclick="reCall();"><i class="fa fa-refresh">&nbsp;</i> Refresh</a>&nbsp;&nbsp;									
								</label>                                            
							</div>
							
						</div>
					<table id="tbCourse" class="table" style="width:100%">
						  <thead>
							  <tr>
								  <th style="text-align:center" width="5%">ลำดับ</th>
								  <th width="13%">ชื่อ-นามสกุล</th>
								  <th style="text-align:center" width="18%">หลักสูตร</th>
								  <th style="text-align:center" width="10%">ลงทะเบียน</th>
								  <th style="text-align:center" width="10%">หมดเขต</th>
								  <th style="text-align:center" width="10%">วันที่ชำระเงิน</th>
								  <th style="text-align:center" width="8%">สถานะ</th>
								  <th style="text-align:center" width="8%">หมายเหตุ</th>
								  <th style="text-align:center" width="7%">ช่องทาง<br>การสมัคร</th>
								  <th style="text-align:center" width="7%">Print ใบเสร็จ</th>
							  </tr>
						  </thead>   
						<tbody>
						</tbody>
					</table>
					<div class="clear"></div>
					<br>
					<div class="filters">     
						<div class="btn-group pull-right">
							<a class="btn btn-small " href="#" onclick="add_to_walkin(<?php echo $course_detail_id; ?>);"><i class="fa fa-user"></i> รับสมัคร Walk-in</a>
							<a class="btn btn-small " href="#" onclick="add_to_book(<?php echo $course_detail_id; ?>);" style="margin-left:30px; margin-right:30px;"><i class="fa fa-tags"></i> สำรองที่นั่ง</a>
							<a class="btn btn-small " href="#" onclick="add_to_import(<?php echo $course_detail_id; ?>);"><i class="fa  fa-list-alt"></i> import การสมัคร </a> 
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</div>

		</div>
	</div> 
</div>
<?php include ('inc/js-script.php') ?>

<script type="text/javascript">
$(document).ready(function() {
	var get_type = "<?php echo $_GET["type"]; ?>";
	if(get_type=="childlist" || get_type=="course_order") $("#add").hide();
	var oTable;
	listItem();	
    $("#date_start").datepicker({language:'th-th',format:'dd-mm-yyyy'});
    $("#date_stop").datepicker({language:'th-th',format:'dd-mm-yyyy'});
	$("#section_id").change(function(event) {
		var id = $(this).val();
		getDropDown('#coursetype_id', id, 'coursetype', 'data/coursetype.php')
	});  
});

function listItem(){
   var get_type = "<?php echo $_GET["type"]; ?>";
   var url = "data/report-registerlist.php";
   oTable = $("#tbCourse").dataTable({
	   "sDom": 'T<"clear">lfrtip',
	   "oLanguage": {
   	   "sInfoEmpty": "",
   		"sInfoFiltered": ""
						  },
		"oTableTools": {
			"aButtons":  ""
		},
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": url,
		"sPaginationType": "full_numbers",
		"aaSorting": [[ 0, "desc" ]],
		'iDisplayLength': 100,
		"fnServerData": function ( sSource, aoData, fnCallback ) {
/*			aoData.push({"name":"section_id","value":<?php echo (int) $info["section_id"]; ?>});			
			aoData.push({"name":"coursetype_id","value":<?php echo (int) $info["coursetype_id"]; ?>});	*/	
			aoData.push({"name":"date_start","value":$("#date_start").val()});			
			aoData.push({"name":"date_stop","value":$("#date_stop").val()});	
			/*aoData.push({"name":"pub","value":$("#pub").val()});*/	
			aoData.push({"name":"course_detail_id","value":<?php echo (int) $course_detail_id; ?>});			
			aoData.push({"name":"status","value":$("#sStatus").val()});			
			aoData.push({"name":"active","value": "T"});			
			aoData.push({"name":"type","value":get_type});	
			$.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});
		}
   }); 
}

function editInfo(id){
	if(typeof id=="undefined") return;
   var url = "index.php?p=<?php echo $_GET["p"];?>&register_id="+id+"&type=info";
   redirect(url);
}


function childlist(id){
	if(typeof id=="undefined") return;
   var url = "index.php?p=<?php echo $_GET["p"];?>&register_id="+id+"&type=childdetail";
   redirect(url);
}

function addnew(){
   var url = "index.php?p=<?php echo $_GET["p"];?>&type=select-course";
   redirect(url);
}

function reCall(){
	oTable.fnClearTable( 0 );
	oTable.fnDraw();
}

function selectMember(){
   var url = "member map";
   memberpop(url,"ret_member_select");	
}

function ret_member_select(id){
	for(var i in id){
		var ck = "";
		var data = id[i];
        $("#member_id").val(data["member_id"]);
        $("#membername").val(data.name_th);
        reCall();
	}
}

function addcourse_detail_other(){
   var url = "course_detail map";
   courseDetailData(url,"ret_detail_other");	
}

function ret_detail_other(id){
	for(var i in id){
		var ck = "";
		var data = id[i];
		$("#course_detail_id").val(data.course_detail_id);
		console.log(id);
		var str = data.date+" เวลา :  "+data.time+" สถานที่ : "+data.address_detail+" "+data.address+ " จำนวนที่นั่ง : "+data.chair_all;
		$("#course_detail_name").val(str);
		reCall();
	}
}

function clearSearch(){
 $("#course_detail_name").val("");
 $("#course_detail_id").val("");
 $("#member_id").val("");
 $("#membername").val("");
 reCall();
}
function add_to_walkin(id){
    var url = "index.php?p=register&course_detail_id="+id+"&type=walk-in&course_id=<?php echo $course_id; ?>";
    redirect(url);
    //window.open(url,'_blank');
}
function add_to_book(id){
	var url = "index.php?p=course-detail&course_detail_id="+id+"&type=detail&flag=detail";
    //var url = "index.php?p=register&course_detail_id="+id+"&type=walk-in&course_id=<?php echo $course_id; ?>";
    window.open(url,'_blank');
}

function add_to_import(id){
    var url = "index.php?p=register&course_detail_id="+id+"&type=register-import&course_id=<?php echo $course_id; ?>";
    window.open(url,'_blank');
}

function update_pay_status(){
	var t = confirm("update  สถานะ ชำระเงินแล้ว ? ");
	if(!t) return false;
    var i = 0;
    var str = "";
    $("#tbCourse tbody tr :checkbox").each(function() {
        if ($(this).is(":checked")) {
            var id = $(this).val();
            str += id+",";
        }
        i++;
    });
   
  if(str!=""){
	$.ajax({
		"type": "POST",
		"async": false, 
		"url": "data/register-status-update.php",
		"data": {'register_id': str}, 
		"success": function(data){	
			$.gritter.removeAll({
		        after_close: function(){
		          $.gritter.add({
		          	position: 'center',
			        title: 'Success',
			        text: data,
			        class_name: 'success'
			      });
		        }
		      });
 			reCall();				      							 
		}
	});
  }else{
    $("input[name=menu_order_"+id+"]").val(tmp);
  }

}
function printInfo(id){
	if(typeof id!="undefined"){

	}else{
	    var id = "";
	    $("#tbCourse tbody tr :checkbox").each(function() {
	        if ($(this).is(":checked")) {
	            var str = $(this).val();
	            id += str+",";
	        }
	    });
	}
   	var url = "receipt-print.php?register_id="+id;

   window.open(url,'_blank');
}
</script>