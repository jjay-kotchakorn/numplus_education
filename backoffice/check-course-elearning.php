<?php
include_once "./connection/connection.php";
include_once "./lib/lib.php";
include_once "./share/course.php";
include_once "./share/member.php";
require "./elerning/vendor/autoload.php";
global $db, $EMPID;
$debug = false;
// $debug = true;
use \Curl\Curl;

$datetime_now = date("Y-m-d H:i:s");
/*
$expire_date=get_expire_date(10, "day", $datetime_now, true); //ส่งค่าให้ฟังก์ชั่น วันที่ปัจจุบัน พร้อมจำนวนวัน
echo $expire_date;
die();
*/
function write_log_local_func($file_name="", $msg="", $write_mode="a"){
	$str = $msg;	
	$date_now = date('Y-m-d H:i:s');
	$date_log = date('Y-m-d');
	$log_name = "./log/".$file_name."_".$date_log.".log";
	$file = fopen($log_name, $write_mode);
	$str_txt = $date_now."|".$str."\r\n";
	fwrite($file, $str_txt);
	fclose($file);
}//end func

if ( $debug ) {
	$register_id = 179967;
	// echo $register_id;
	check_course_elearning($register_id);	
}//end if

if (!empty($_POST["register_id"])) {
	$register_id = trim($_POST["register_id"], ",");
	$pay_status = (int)$_POST["pay_status"];
	$pay_type = trim($_POST["pay_type"]);
	
	// d($_POST);
	if ( !empty($pay_status) && ($pay_status==3 || $pay_status==5 || $pay_status==6) ) {

		//write log
		$str_txt = "";
	    $str_txt .= "func=main|register_id={$register_id}|pay_status={$pay_status}";
	    write_log_local_func("check-course-elearning", $str_txt);
		
		$rs = check_course_elearning($register_id);

	}else if ( empty($pay_status) && !empty($pay_type) ) {

		if ( $pay_type=='import-file' ) {
			$register_ids = explode(",", $register_id);
			foreach ($register_ids as $key => $register_id) {
				$q = "SELECT pay_status FROM register WHERE active='T' AND register_id={$register_id}";
				$pay_status = $db->data($q);
				//write log
				$str_txt = "";
			    $str_txt .= "func=main_import-file|register_id={$register_id}|pay_status={$pay_status}";
			    write_log_local_func("check-course-elearning", $str_txt);
				
				$rs = check_course_elearning($register_id);
			}//end loop $register_id
		}elseif ( $pay_type=='walk-in' ) {
			$q = "SELECT pay_status FROM register WHERE active='T' AND register_id={$register_id}";
			$pay_status = $db->data($q);
			//write log
			$str_txt = "";
		    $str_txt .= "func=main_walk-in|register_id={$register_id}|pay_status={$pay_status}";
		    write_log_local_func("check-course-elearning", $str_txt);
			
			$rs = check_course_elearning($register_id);
		}elseif ( $pay_type=='reconcile' ) {
			$register_ids = explode(",", $register_id);

			if ( count($register_ids)>0 ) {
				foreach ($register_ids as $key => $register_id) {
					$q = "SELECT pay_status FROM register WHERE active='T' AND register_id={$register_id}";
					$pay_status = $db->data($q);
					//write log
					$str_txt = "";
				    $str_txt .= "func=main_Reconcile|register_id={$register_id}|pay_status={$pay_status}";
				    write_log_local_func("check-course-elearning", $str_txt);
					
					$rs = check_course_elearning($register_id);
				}//end loop $register_id
			}//end if
		}//end elseif		
	}//end else if

}elseif ( !empty($_POST["register_data_list_id"]) && $_POST["type"]=='send-again' ) {
	$register_data_list_ids = trim($_POST["register_data_list_id"]);
	$q = "SELECT register_data_list_id,
			ref_elerning_site_vender_id,
			register_id,
			course_id,
			course_detail_id,
			member_id,
			cid,
			title,
			fname,
			lname,
			code_course_wr,
			code_course_elearning,
			send_date,
			receive_date,
			expire_date,
			result_date,
			send_status,
			receive_status,
			result_id,
			rectime,
			active,
			ref_uuid,
			email,
			birthdate,
			remark
		FROM register_data_list
		WHERE active='T' AND register_data_list_id={$register_data_list_ids};
	";
	$info = $db->rows($q);

	$value = array();
	$x = $arrayName = array('order_id' => $info["register_id"] );
	$value = $info + $x;
	$rs = send_request_http("register", $value);
	// d($ret);die();

}else{
	return;
}//end elseif


function check_course_elearning($register_id=""){
	if(!$register_id) return;
	// echo $register_id;die();
	// var_dump($register_id);die();
	// d($register_id);die();
	$arr = get_request_elearning($register_id);
	
	foreach ($arr as $key => $value) {
		// d($value); die();
		$cnt_v = count($value);
		if ( $cnt_v>1 ) {
			foreach ($value as $key => $value) {
				// d($value);
				$ret = send_request_http("register", $value);
			}//end loop $d
		}else{
			$ret = send_request_http("register", $value[0]);
		}//end else
		// d($value); die();
		// die();
	}//end loop $value
}//end func



function get_request_elearning($register_id=""){
	// var_dump($register_id);die();
	global $db;
	$err = '';
	$result = array();
	if(!$register_id) return;
	$info = get_register("", $register_id);
	// d($info);die();
	// if(isset($info) && $info[0]){
	if( !empty($info) ){
		$info = $info[0];
		// d($info);die();

		if ( (empty($info["pay_date"]) || $info["pay_date"]=="" || $info["pay_date"]=="0000-00-00 00:00:00") 
			&& ($info["pay_status"]==5 || $info["pay_status"]==6 || $info["pay_status"]==3) 
		) {
			$info["pay_date"] = $info["date"];
		}//end if

		$course_detail_id = $info["course_detail_id"];
		$course_id = $info["course_id"];
		$q = "SELECT view_register_course_detail_id,
				register_course_detail_course_detail_id,
				course_detail_id,
				code_project,
				code_project_single,
				register_course_detail_course_id,
				course_id
			FROM view_register_course_detail
			WHERE view_register_course_detail_id={$register_id}
		";

		// echo $q;die();

		$view_register = $db->get($q);

		// d($view_register);die();		

		$cos_dt_id = explode(":", $course_detail_id);
		// var_dump($cos_dt_id);die();
		if ( count($cos_dt_id)>1 ) {
		// if(strpos(':', $info["course_detail_id"])==1){
			// $course_detail_id = $view_register["register_course_detail_course_detail_id"];
			// $course_id = $view_register["register_course_detail_course_id"];

			foreach ($view_register as $key => $v) {
				$course_detail_id = $v["register_course_detail_course_detail_id"];
				$course_id = $v["register_course_detail_course_id"];

						// d($info);die();
				// d($view_register); die();
				$q = "SELECT elerning_map_course_detail_id,
						course_detail_id,
						course_id,
						transfer_status,
						expire_status,
						expire_date,
						num_expire_day,
						`code`,
						`name`,
						name_eng,
						active,
						recby_id,
						rectime,
						remark
					FROM elerning_map_course_detail 
					WHERE course_detail_id={$course_detail_id} 
						AND course_id={$course_id} 
						AND transfer_status='T'
				";
				// echo $q;die();
				$map_course_detail = $db->rows($q);
				$map_course_detail_info = serialize($map_course_detail);
				// d($map_course_detail);die();

				if(!$map_course_detail){
					return;
				}
				$member_id = $info["member_id"];
				$member_info = view_member('', $member_id);
				$member_info = $member_info[0];

				$course_info = get_course("", $course_id);
				$course_info = $course_info[0];
				$expire_time = "";
				switch ( (int)$map_course_detail["expire_status"] ) {
					case 1:
						$expire_time = convert_mktime($map_course_detail["expire_date"]);
					break;
					case 2:
						$num = (int)$map_course_detail["num_expire_day"];
						//$expire_time = convert_mktime($info["pay_date"])+($num*24*60*60);
						$expire_time = get_expire_date($num, "day", $info["pay_date"]);
						$expire_time = strtotime($expire_time);

						// echo "num_day=".$map_course_detail["num_expire_day"]."<br>";
						// echo "pay_date=".$info["pay_date"]."<br>";
						// echo "expire_time=".$expire_time."<br>";
						// echo "expire_date=".date("d/m/Y", $expire_time)."<br>";
						// echo "expire_date_conv=".date("Y-m-d", $expire_time)."<br>";
						// die();
					break;
					case 3:
						$expire_time = 'unlimit';
					break;
			    }//end sw
			    // $expire_date = ($expire_time=='unlimit') ? '31/12/9999' : date("d/m/Y", $expire_time);
			    // echo $expire_date; die();
			    if ( $expire_time=='unlimit' ) {
			    	$expire_date = '31/12/9999';
			    }else{
			    	$expire_date = date("Y-m-d 00:00:00", $expire_time);
			    	$expire_date = revert_date($expire_date);    	
			    	// $expire_date = str_replace("-", "/", $expire_date);
			    }//end else
			    // echo $expire_date; die();

				$args = array();
				$args["order_id"] = $info["register_id"];
				$args["course_detail_id"] = $course_detail_id;
				$args["course_id"] = $course_id;
				$args["cid"] = $info["cid"];
				$args["title"] = $info["title"];
				$args["fname"] = $info["fname"];
				$args["lname"] = $info["lname"];
				$args["member_id"] = $info["member_id"];
				$args["expire_date"] = $expire_date;
				$args["birthdate"] = revert_date($member_info["birthdate"]);
				$args["email"] = $member_info["email1"];
				// $args["course_code"] = $course_info["code"];
				$args["code_project"] = $course_info["code"];
				$args["elerning_map_course_detail_id"] = (int)$map_course_detail["elerning_map_course_detail_id"];
				$args["map_course_detail_info"] = $map_course_detail_info;
				$args["code_project_course_detail"] = ($view_register["code_project_single"]) ? $view_register["code_project_single"]: $view_register["code_project"];

				// d($args);die();
				$q = "SELECT elerning_map_course_id,
						course_id,
						`code`,
						`name`,
						name_eng,
						active,
						recby_id,
						rectime,
						remark,
						course_detail_id 
					FROM elerning_map_course 
					WHERE course_id={$course_id} 
						AND name!='' 
						AND course_detail_id={$course_detail_id}
				";
				// echo $q;die();
				$list = $db->get($q);
				if(!$list){
					$msg = 'ไม่มีการบันทึก รหัสหลักสูตรของระบบ E-Learning กรุณาตรวจสอบ ที่หลักสูตร course_id='.$course_id;
					$err = 'error code:777';
					echo $err."|".$msg;
					die();
				}
				// d($list);die();
				$req = array();
				foreach ($list as $k => $v) {
					$t = $args;
					$t["code_course_wr"] = $v["name"];
					array_push($req, $t);		
				}//end loop $v
				$result[] = $req;

			}//end loop $v
		
		}else{
			// d($info);
			$q = "SELECT elerning_map_course_detail_id,
					course_detail_id,
					course_id,
					transfer_status,
					expire_status,
					expire_date,
					num_expire_day,
					`code`,
					`name`,
					name_eng,
					active,
					recby_id,
					rectime,
					remark
				FROM elerning_map_course_detail 
				WHERE course_detail_id={$course_detail_id} 
					AND course_id={$course_id} 
					AND transfer_status='T'
			";
			// echo $q;die();
			$map_course_detail = $db->rows($q);
			$map_course_detail_info = serialize($map_course_detail);
			// echo $q;die();
			// d($map_course_detail);die();
			if(!$map_course_detail){
				return;
			}
			$member_id = $info["member_id"];
			$member_info = view_member('', $member_id);
			$member_info = $member_info[0];

			$course_info = get_course("", $course_id);
			$course_info = $course_info[0];
			$expire_time = "";
			switch ($map_course_detail["expire_status"]) {
				case 1:
					$expire_time = convert_mktime($map_course_detail["expire_date"]);
				break;
				case 2:
					$num = (int)$map_course_detail["num_expire_day"];

					// $expire_time = convert_mktime($info["pay_date"])+($num*24*60*60);
					// $expire_time = get_expire_date($num, "day", $info["pay_date"]);
					$expire_time = get_expire_date($num, "day", $info["pay_date"]);
					$expire_time = strtotime($expire_time);
				break;
				case 3:
					$expire_time = 'unlimit';
				break;
		    }
		    // $expire_date = ($expire_time=='unlimit') ? '31/12/9999' : date("d/m/Y", $expire_time);
	    	if ( $expire_time=='unlimit' ) {
		    	$expire_date = '31/12/9999';
		    }else{
		    	$expire_date = date("Y-m-d 00:00:00", $expire_time);
		    	// echo $expire_date; die();
		    	$expire_date = revert_date($expire_date);
		    	// $expire_date = str_replace("-", "/", $expire_date);
		    }//end else
		    // echo $expire_time."<hr>";
		    // echo $expire_date."<hr>"; die();

			$args = array();

			$args["order_id"] = $info["register_id"];
			$args["course_detail_id"] = $course_detail_id;
			$args["course_id"] = $course_id;
			$args["cid"] = $info["cid"];
			$args["title"] = $info["title"];
			$args["fname"] = $info["fname"];
			$args["lname"] = $info["lname"];
			$args["member_id"] = $info["member_id"];
			$args["expire_date"] = $expire_date;
			$args["birthdate"] = revert_date($member_info["birthdate"]);
			$args["email"] = $member_info["email1"];
			// $args["course_code"] = $course_info["code"];
			$args["code_project"] = $course_info["code"];
			$args["elerning_map_course_detail_id"] = (int)$map_course_detail["elerning_map_course_detail_id"];
			$args["map_course_detail_info"] = $map_course_detail_info;
			$args["code_project_course_detail"] = ($view_register["code_project_single"]) ? $view_register["code_project_single"]: $view_register["code_project"];

			// d($args);die();
			$q = "SELECT elerning_map_course_id,
					course_id,
					`code`,
					`name`,
					name_eng,
					active,
					recby_id,
					rectime,
					remark,
					course_detail_id 
				FROM elerning_map_course 
				WHERE course_id={$course_id} 
					AND name!='' 
					AND course_detail_id={$course_detail_id}
			";
			// echo $q;die();
			$list = $db->get($q);
			if( empty($list) ){
				$msg = 'ไม่มีการบันทึก รหัสหลักสูตรของระบบ E-Learning กรุณาตรวจสอบ ที่หลักสูตร course_id='.$course_id;
				$err = 'error code:777';
				echo $err."|".$msg;
				die();
			}
			// d($list);die();
			$req = array();
			foreach ($list as $k => $v) {
				$t = $args;
				$t["code_course_wr"] = $v["name"];
				array_push($req, $t);		
			}//end loop $v
			$result[] = $req;
		}//end else

		// d($result);die();
		// return $req;
		return $result;
	}//end if
}//end func



function send_request_http($type_request="", $value=array()){
	global $db;
	if(!$value || !$type_request) return;
	$url_site_wr = get_config("elearning-url-site-wr");
	$url_site_elearning = get_config("elearning-url-site-elearning");
	$url = $url_site_elearning;
/*
	d($value);
	die();
*/
	$api_user = "ati_user";
	$api_key = "ati7214";
	$date_now = date('Y-m-d H:i:s');

	if ( $type_request=="register" ) {		
		$args = array();
		$args["table"] = "register_data_list";	
		if ( !empty($value["register_data_list_id"]) ) {
			$args["id"] = (int)$value["register_data_list_id"];	
			$args["birthdate"] = $value["birthdate"];
			// $expire_date = $value["expire_date"];
			$args["expire_date"] = $value["expire_date"];
			$args["ref_uuid"] = $value["ref_uuid"];
			$args["send_status"] = 'T';
			$args["remark"] = 'มีการส่งข้อมูลครั้งล่าสุดวันที่: '.$date_now;
		}else{
			$ref_uuid = ref_uuid();
			$ref_uuid = $value["order_id"]."-".$ref_uuid;			
			$args["ref_uuid"] = $ref_uuid;
			$args["birthdate"] = thai_to_timestamp($value["birthdate"]);
			$expire_date = $value["expire_date"]=='31/12/9999' ? '9999-12-31 23:59:59' : thai_to_timestamp($value["expire_date"]) ;
			// $expire_date = $value["expire_date"];
			$args["expire_date"] = $expire_date;
			$args["send_date"] = $date_now;
		}//end else
		$args["register_id"] = (int)$value["order_id"];	
		$args["member_id"] = (int)$value["member_id"];
		$args["cid"] = $value["cid"];
		$args["title"] = $value["title"];
		$args["fname"] = $value["fname"];
		$args["lname"] = $value["lname"];		
		$args["course_id"] = $value["course_id"];
		$args["course_detail_id"] = $value["course_detail_id"];
		$args["rectime"] = $date_now;
		// $args["birthdate"] = thai_to_timestamp($value["birthdate"]);
		$args["email"] = $value["email"];
		$args["code_project"] = $value["code_project"];
		$args["code_course_wr"] = $value["code_course_wr"];
		$args["elerning_map_course_detail_id"] = $value["elerning_map_course_detail_id"];
		$args["map_course_detail_info"] = $value["map_course_detail_info"];
		
		// d($args); die();
		
		// echo $db->set($args, true, true);die();
		
		$ref_elerning_site_ati_id = $db->set($args);
		$ref_elerning_site_ati_id = !empty($ref_elerning_site_ati_id) ? $ref_elerning_site_ati_id : $args["id"];

		// echo "value_exp_date=".$value["expire_date"];
		// echo "value_exp_date_conv=".thai_to_timestamp($value["expire_date"]);
		// die();	

		// $birthdate = '1986-02-07';
		$birthdate = $args["birthdate"];
		$birthdate = revert_date($birthdate);
		$birthdate = str_replace('-', '/', $birthdate);
		// echo $birthdate;
		if ( $expire_date == '0000-00-00 00:00:00' ) {
		 	$expire_date = '31-12-9999';
		}else{
			$expire_date = revert_date($expire_date);
		}
		$expire_date = str_replace('-', '/', $expire_date);	
/*
		echo $birthdate."<hr>";
		echo $expire_date."<hr>";
		d($args); die();
*/
		// $url = "testdfnslrg.com";

		$curl = new Curl();
		$curl->post($url."/".$type_request."/", array(
		    //'username' => 'ati_user',
		    'register_id' => $args["register_id"],
		    'member_id' => $args["member_id"],
		    'cid' => $args["cid"],
		    'title' => $args["title"],
		    'fname' => $args["fname"],
		    'lname' => $args["lname"],
		    'course_id' => $args["course_id"],
		    'course_detail_id' => $args["course_detail_id"],
		    'ref_elerning_site_ati_id' => $ref_elerning_site_ati_id,
		    'ref_uuid' => $args["ref_uuid"],
		    'birthdate' => $birthdate,
		    'code_project' => $value["code_project"],
		    'expire_date' => $expire_date,
		    'email' => $args["email"],
		    'code_course_wr' => $args["code_course_wr"],
		    'api_user' => $api_user,
		    'api_key' => $api_key,
		    'send_date' => $date_now
		));

		$str_txt = "";
		$str_txt .= "func=send_request_http|"; 
	    foreach ($args as $key => $v) {
	    	$str_txt .= "{$key}={$v}|";
	    }//end loop $v

		$send_status = 'error';
		if ($curl->error) {
		    $remark = 'Error: ' . $curl->errorCode . ': ' . $curl->errorMessage . "\n";
		    echo $remark;
		    $q = "UPDATE register_data_list 
		    	SET send_status='F'
		    		, rectime='{$date_now}' 
		    		, remark='{$remark}'
		    	WHERE register_data_list_id={$ref_elerning_site_ati_id}
		    ";
		    // echo $q;
		    $db->query($q);

		    $str_txt .= "send_status={$send_status}|remark={$remark}"; 
		    write_log_local_func("check-course-elearning", $str_txt);

		    return $curl->errorMessage;
		} elseif ($curl->response) {
			$send_status = 'success';
		    // echo 'Data server received via POST:' . "\n";
		    // var_dump($curl->response->form);die();
		    // var_dump($curl->response);	die();
			// var_dump($curl->response);    
			$str_txt .= "send_status={$send_status}|"; 
			write_log_local_func("check-course-elearning", $str_txt);

		    return $curl->response;		    
		}//end else

		$curl->close();

	}//end if

	// $value = $args;
	// $value = $args;
	// $value = $ref_elerning_site_ati_id;
	// return $value;
}//end func



function ref_uuid()
{
	return sprintf(
		'%02x%02x%02x%02x-%02x%02x-4%x%02x-%x%x%02x-%02x%02x%02x%02x%02x%02x',
		mt_rand(0, 255),
		mt_rand(0, 255),
		mt_rand(0, 255),
		mt_rand(0, 255),
		mt_rand(0, 255),
		mt_rand(0, 255),
		mt_rand(0, 15),
		mt_rand(0, 255),
		mt_rand(8, 11),
		mt_rand(0, 15),
		mt_rand(0, 255),
		mt_rand(0, 255),
		mt_rand(0, 255),
		mt_rand(0, 255),
		mt_rand(0, 255),
		mt_rand(0, 255),
		mt_rand(0, 255)
	);
}//end func



?>