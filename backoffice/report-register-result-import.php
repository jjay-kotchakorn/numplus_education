	<?php
	include_once "./lib/lib.php";
	include_once "./connection/connection.php";
	include_once "./share/datatype.php";
	include_once "./share/course.php";
	include_once "./share/member.php";
	require_once dirname(__FILE__) . '/PHPExcel.php';
	global $db, $EMPID;
	global $SECTIONID;
	$apay = datatype(" and a.active='T' and a.pay_status_id in (3,5,6) ", "pay_status", true);

	$educationlevel = datatype(" and a.active='T'", "educationlevel", true);
	$province = datatype(" and a.active='T'", "province", true);
	$receipttype = datatype(" and a.active='T'", "receipttype", true);
	$university = datatype_university(true);

	$error = $_SESSION["error"]["msg"];
	unset($_SESSION["error"]["msg"]);
	$success = $_SESSION["success"]["msg"];
	unset($_SESSION["success"]["msg"]);

	$course_id = $_GET["course_id"];
	$course_detail_id = $_GET["course_detail_id"];
	$section = datatype(" and a.active='T'", "section", true);
	$coursetype = datatype(" and a.active='T'", "coursetype", true);
	$q = "select  day, date, time , address, address_detail from course_detail where course_detail_id=$course_detail_id";
	$r = $db->rows($q);
	$str = "";
	if($r){
		$str = $r["day"]." ".revert_date($r["date"])." ".$r["time"]." ".$r["address_detail"]." ".$r["address"];
		$info = get_course("", $course_id);
		if($info) $info = $info[0];
	}else{
		$str = "เพิ่มหลักสูตร";
	}

	$preview = false;

	//$fileName = $_FILES['excelFile']['tmp_name'];

	if (!empty($_FILES) && $_POST) {
		$file = $_FILES['excelFile']['tmp_name'];
		$es_exam_data = file_get_contents($file); 		
	}

	if(isset($es_exam_data)) $preview = true;
	?>
	<link rel="stylesheet" type="text/css" href="js/jquery.nanoscroller/nanoscroller.css" />
	<link href="js/jquery.icheck/skins/square/blue.css" rel="stylesheet">
	<div id="cl-wrapper">
		<div class="container-fluid" id="pcont">
			<div class="cl-mcont">
				<div class="row">
					<div class="col-md-12">           
						<div class="block-flat">
							<div class="header">              
								<ol class="breadcrumb">
									<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>');">หน้าหลัก</a></li>
									<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>&type=select-course');">เลือกหลักสูตร</a></li>
									<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>&course_id=<?php echo $course_id; ?>&type=select-course-detail');">เลือกสถานที่</a></li>
									<li class="active">import ผลการเข้าร่วม</li>
								</ol>
							</div>
							<div class="content">

								<?php if($preview===true){ 
									$arr = array();
									if($es_exam_data !="") { 
										$return_string = 2000;
										$exam = explode("<brk1>",$es_exam_data); 
										$n_exam = sizeof($exam);
										if($n_exam<=0) $return_string = 1002;	
										for($i=0;$i<$n_exam;$i++) {
											$exam_data = explode("<brk2>",$exam[$i]);	
											$arr[$i] = $exam_data;	
											$exid = $exam_data[0]; 
											if(!$exid || $exid=="") continue;
											$reg_no = $exam_data[1]; 
											$reg_id = (int)$exam_data[2]; 
											$reg_title = $exam_data[3]; 
											$reg_fname = $exam_data[4]; 
											$reg_lname = $exam_data[5]; 
											$reg_cid = $exam_data[6]; 
											$reg_result = $exam_data[7]; 
											$reg_result_date = $exam_data[8];
											if($reg_id>0){
												$q = "select register_id from register where register_id={$reg_id}";
												$register_id = $db->data($q);
											} 
											if(!$register_id){
												$q = "select course_detail_id from course_detail where ref_exam_id like '%$exid' and ref_exam_id is not null";
												$course_detail_id = $db->data($q);
												if($course_detail_id){
													$q = "select register_id from register where course_detail_id={$course_detail_id} and cid='$reg_cid'";
													$register_id = $db->data($q);
												}
											}
											if($register_id){
/*												
												$args = array();
												$args["table"] = "register";
												$args["id"] = $register_id;
												$args["result"] = $reg_result;
												$args["result_date"] = $reg_result_date;
												$args["rectime"] = date("Y-m-d H:i:s");
												$ret = $db->set($args);
*/
												$q = "select register_course_detail_id from register_course_detail where course_detail_id='{$course_detail_id}' and course_id='{$course_id}' and register_id = '{$register_id}'";
												//echo $q;
												$ids = $db->data($q);
												if($ids>0){
													$args = array();
													$args["table"] = "register_course_detail";
													$args["id"] = $ids;
													if($reg_result>0 || $te_results>0)
														//$args["result_date"] = date("Y-m-d H:i:s");
														$args["result_date"] = $reg_result_date;
													else{
														$args["result_date"] = "NULL";
													}
													$args["register_result_id"] = $reg_result;
													$args["result_by"] = $EMPID;
													$args["recby_id"] = $EMPID;
													$args["rectime"] = date("Y-m-d H:i:s");
													$ret = $db->set($args);	
												}else{
													$q = "SELECT * FROM register WHERE active='T'
															AND pay_status IN (3,5,6)
															AND register_id=$register_id
													";
													$rs = $db->rows($q);
													$cos_dt_ids = explode(":", $rs["course_detail_id"]);
													if ( count($cos_dt_ids)==2 ) {
														$course_id = $rs["course_id"];
														$course_detail_id = $cos_dt_ids[1];
														$q = "select register_course_detail_id from register_course_detail where course_detail_id='{$course_detail_id}' and course_id='{$course_id}' and register_id = '{$register_id}'";
														//echo $q;
														$ids = $db->data($q);
														if($ids>0){
															$args = array();
															$args["table"] = "register_course_detail";
															$args["id"] = $ids;
															if($reg_result>0 || $te_results>0)
																//$args["result_date"] = date("Y-m-d H:i:s");
																$args["result_date"] = $reg_result_date;
															else{
																$args["result_date"] = "NULL";
															}
															$args["register_result_id"] = $reg_result;
															$args["result_by"] = $EMPID;
															$args["recby_id"] = $EMPID;
															$args["rectime"] = date("Y-m-d H:i:s");
															$ret = $db->set($args);	
														}//end if
													}else{				

														$args = array();
														$args["table"] = "register";
														$args["id"] = $register_id;
														if($reg_result>0 || $te_results>0)
															//$args["result_date"] = date("Y-m-d H:i:s");
															$args["result_date"] = $reg_result_date;
														else{
															$args["result_date"] = "NULL";
														}
														$args["result"] = $reg_result;
														$args["te_results"] = $reg_result;
														$args["result_by"] = $EMPID;
														$args["recby_id"] = $EMPID;
														$args["rectime"] = date("Y-m-d H:i:s");
														$ret = $db->set($args);	
																		
													}//end else
												}//end else

												$register_id = $args["id"] ? $args["id"] : $ret;

												?>
												<div class="alert alert-success col-md-12">
													<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
													<i class="fa fa-check sign"></i><strong>Success!</strong> นำเข้าข้อมูลข้อมูรหัสประจำตัวประชาชน <?php echo $reg_cid; ?> เลขที่นั่งสอบ <?php echo $reg_no; ?> [register_id=<?php echo $reg_id ?>, result=<?php echo $reg_result; ?>, result_date=<?php echo $reg_result_date; ?>] เรียบร้อยแล้ว
												</div>	
												<?php
											}else{
												$return_string = 1003;
												?>
									  				<div class="alert alert-danger">
									  					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
									  					<i class="fa fa-times-circle sign"></i><strong>Error!</strong> ไม่สามารถนำเข้าข้อมูลของ รหัสประจำตัวประชาชน <?php echo $reg_cid; ?>  เลขที่นั่งสอบ <?php echo $reg_no; ?> [register_id=<?php echo $reg_id ?>, result=<?php echo $reg_result; ?>, result_date=<?php echo $reg_result_date; ?>]ได้ กรุณา ตรวจสอบอีกครั้ง
									  				</div>
												<?php
											}                                          
										} 

									} 						
								}else{ ?>
								<form id="frmMain" name="frmMain" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="">
									<input type="hidden" name="course_id" id="course_id" value="<?php echo $course_id; ?>">
									<input type="hidden" name="course_detail_id" id="course_detail_id" value="<?php echo $course_detail_id; ?>">
									<input type="hidden" name="coursetype_id" id="coursetype_id" value="<?php echo $info["coursetype_id"]; ?>">
									<input type="hidden" name="section_id" id="section_id" value="<?php echo $info["section_id"]; ?>">
									<div class="row">
										<div class="col-md-12" >
											<div class="form-group row">
														<label class="col-sm-2 control-label">เลือกไฟล์</label>
												<div class="col-sm-3">
													<div class="input-text">
														 <input type="file" name="excelFile" class="required " data-no-uniform="true">
													</div>
												</div> 
											</div> 
											</div>
										</div>
									<div class="clear"></div>
									<div class="form-group row" style="padding-left:10px;">
										<div class="col-sm-12">
											<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p'] ?>');">กลับสู่หน้าหลัก</button>
											<button type="button" id="bt_save" class="btn btn-primary" onClick="ckSave()">Import & Submit</button>																			
										</div>
									</div>
								</form>
								<?php } ?>
							</div>
						</div>

					</div>
				</div>

			</div>
		</div> 
	</div>
	<?php include_once ('inc/js-script.php'); ?>
	<script type="text/javascript">
	function ckSave(id){
		var t = confirm("ยืนยันการimport ผลการเข้าร่วม");
		if(!t) return false;
		var checkboxes = $("#frmMain").find(':checkbox');
		onCkForm("#frmMain");
		$("#frmMain").submit();
	}

	</script>
	<style>
		#set_time-error{
			display: none !important;
		}
		#pay_status-error, #slip_type-error{
			width: 400px;
			margin-top: 21px;
		}
	</style>