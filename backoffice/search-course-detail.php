<?php
include_once "./share/authen.php";
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/menu.php";
include_once "./share/datatype.php";

$table = $_GET["table"] ? $_GET["table"] : "course_detail";
$checkBox = $_GET["ckBox"] ? "T" : "";
$retFunc = $_GET["retFunc"] ? $_GET["retFunc"] : "retInfo";
$con = $_GET["con"] ? trim($_GET["con"]) : "";
global $db;

?>
<!DOCTYPE html>
<html lang="en">
<?php include ('inc/header.php'); ?>
<body>

        <div class="box-content">                    
            <div class="alert search-header" style="margin-bottom:0px;">
                <div class="form-group row">
                    <div class="col-md-12">                        
                    <button onClick="javascript:parent.$.fancybox.close();" class="close" type="button">×</button>
                            &nbsp;&nbsp;วันที่&nbsp;&nbsp;<input class="form-control focused" id="sDateStart" onKeyUp="disp();" style="width:150px;display:inline-block;" type="text" value="">
                            &nbsp;&nbsp;ถึงวันที่&nbsp;&nbsp;<input class="form-control focused" id="sDateStop" onKeyUp="disp();" style="width:150px;display:inline-block;" type="text" value="">                
                            &nbsp;&nbsp;ค้นหา&nbsp;&nbsp;<input class="form-control focused" id="sSearch" onKeyUp="disp();" style="width:150px;display:inline-block;" type="text" value="">                
                                        
                            <a href="#" class="btn btn-rad btn-info" onClick="disp();"><i class="fa fa-search"></i></a>&nbsp;&nbsp;
                            <button onClick="getKey();" id="ok" class="btn btn-rad btn-success" type="button" title="คลิกเพื่อเลือกรายการ">เลือก/OK</button>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12">                        
                            <input id="scourse_id" class="" style="width:100%;" onchange="disp();" name="optionvalue" type="hidden" data-placeholder="ค้นหาหลักสูตร" />
                    </div>
                </div>
            </div>
            <div style="overflow-y:scroll; height:100%; vertical-align:top;"> 
                <table width="100%" class="table">
                    <thead>
                        <tr class="alert alert-success" style="font-weight:bold;">
                            <td width="7%"><input data-no-uniform="true" type="checkbox" onClick="ckAll(this)" id="ckAll">&nbsp;</td>
                            <td width="13%" class="center">วัน</td>
                            <td width="12%" class="center">เวลา</td>
                            <td width="30%" class="center">ข้อมูลสถานที่</td>
                            <td width="10%" class="center">สถานที่</td>
                            <td width="8%" class="center">จำนวนที่นั่ง</td>
                            <td width="10%" class="center">เปิดรับสมัคร</td>
                            <td width="10%" class="center">ปิดรับสมัคร</td>                                    
                        </tr>
                    </thead>   
                </table>
            </div>
            <div style="overflow-y:scroll; height:210px; vertical-align:top; margin-top:-6px;">                      
                <table class="table table-striped" id="tbList">
                    <thead>
                        <tr style="cursor:pointer; " valign="middle" onClick="singleKey(this);">
                            <td width="7%"><input type="hidden" name="course_detail_id" id="course_detail_id"></td>
                            <td width="13%" class="center"><span id="date"></span></td>
                            <td width="12%" class="center" id="time"></td>
                            <td width="30%" class="center" id="address_detail"></td>                                                   
                            <td width="10%" class="center" id="address"></td>
                            <td width="8%" class="center" id="chair_all"></td>
                            <td width="10%" class="center" id="open_regis"></td>
                            <td width="10%" class="center" id="end_regis"></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr style="cursor:pointer;" onClick="singleKey(this);">
                            <td width="7%"><input type="hidden" name="course_detail_id" id="course_detail_id">
                                &nbsp;</td>
                            <td width="13%" class="center"><span id="date"></span></td>
                            <td width="12%" class="center" id="time"></td>
                            <td width="30%" class="center" id="address_detail"></td>                                                   
                            <td width="10%" class="center" id="address"></td>
                            <td width="8%" class="center" id="chair_all"></td>
                            <td width="10%" class="center" id="open_regis"></td>
                            <td width="10%" class="center" id="end_regis"></td>                                 
                        </tr>
                        <tr style="cursor:pointer;">
                            <td width="7%">
                                <input type="hidden" name="course_detail_id" id="course_detail_id">
                                <input type="checkbox" id="ckBox">&nbsp;&nbsp;</td>
                            <td width="13%" class="center"><span id="date"></span></td>
                            <td width="12%" class="center" id="time"></td>
                            <td width="30%" class="center" id="address_detail"></td>                                                   
                            <td width="10%" class="center" id="address"></td>
                            <td width="8%" class="center" id="chair_all"></td>
                            <td width="10%" class="center" id="open_regis"></td>
                            <td width="10%" class="center" id="end_regis"></td>                                  
                        </tr>
                    </tbody>
                </table>   
            </div>                      
        </div>
		

<?php   include_once ('inc/js-script.php'); ?>
    <?php include ('inc/footer.php') ?>
<script type="text/javascript">
        var checkBox = "<?php echo $checkBox ?>";
        var mapField = "<?php echo $map; ?>";
        var con = "<?php echo $con; ?>";
        if (checkBox == "T") {
            var trList = $("#tbList tbody tr:eq(1)").clone();
        } else {
            $("#ok").hide();
            var trList = $("#tbList tbody tr:eq(0)").clone();
            $("#ckAll").hide();
        }

        (checkBox == "T") ? delCkRow("#tbList", "1") : delRow("#tbList");

        $(document).ready(function() {
            $("#sDateStart").datepicker({language:'th-th',format:'dd-mm-yyyy'});
            $("#sDateStop").datepicker({language:'th-th',format:'dd-mm-yyyy'});           
            disp();

            $('#scourse_id').select2({
                minimumInputLength: 3,
                ajax: {
                  url: "data/course-optionlist.php",
                  dataType: 'json',
                  data: function (term, page) {
                    return {
                      q: term
                    };
                  },
                  results: function (data, page) {
                    return { results: data };
                  }
                }
              });
        });

        function disp() {
            (checkBox == "T") ? delCkRow("#tbList") : delRow("#tbList");
            var url = "data/course-detail-search.php";
            var param = "";
            param = param + "&date_start=" + $("#sDateStart").val();
            param = param + "&date_stop=" + $("#sDateStop").val();
            param = param + "&search=" + $("#sSearch").val();
            param = param + "&course_id=" + $("#scourse_id").val();
            param = param + "&othercon=" + con;
            if ($("#ck").is(":checked")) {
                param = param + "&all=T";
            }
            $.ajax({
                "dataType": 'json',
                "type": "POST",
                "url": url,
                "data": param,
                "success": function(data) {
                    (checkBox == "T") ? delCkRow("#tbList") : delRow("#tbList");
                    $.each(data, function(index, array) {
                        var ck = "";
                        $("#tbList tbody tr :input").each(function() {
                            var t = $(this).val();
                            var ckId = array.course_detail_id;
                            if (ckId == t) {
                                ck = "1";
                            }
                        });
                        if (ck == "1")
                            return;
                        addTrLine("#tbList", trList, array);
                    });
                }
            });
        }


        function singleKey(tag) {
            var ret = new Array();
            var i = 0;
            ret[i] = new Array();
            ret[i]["course_detail_id"] = $(tag).find('#course_detail_id').val();
            ret[i]["date"] = $(tag).find('#date').text();
            ret[i]["time"] = $(tag).find('#time').text();
            ret[i]["address_detail"] = $(tag).find('#address_detail').text();
            ret[i]["address"] = $(tag).find('#address').text();
            ret[i]["chair_all"] = $(tag).find('#chair_all').text();
            ret[i]["open_regis"] = $(tag).find('#open_regis').text();
            ret[i]["end_regis"] = $(tag).find('#end_regis').text();
            parent.<?php echo $retFunc; ?>(ret);
            parent.$.fancybox.close();
        }

        function getKey() {
            var ret = new Array();
            var i = 0;
            $("#tbList tbody tr :checkbox").each(function() {
                if ($(this).is(":checked")) {
                    var tag = $(this).parent().parent();
                    ret[i] = new Array();
                    ret[i]["course_detail_id"] = $(tag).find('#course_detail_id').val();
                    ret[i]["date"] = $(tag).find('#date').text();
                    ret[i]["time"] = $(tag).find('#time').text();
                    ret[i]["address_detail"] = $(tag).find('#address_detail').text();
                    ret[i]["address"] = $(tag).find('#address').text();
                    ret[i]["chair_all"] = $(tag).find('#chair_all').text();
                    ret[i]["open_regis"] = $(tag).find('#open_regis').text();
                    ret[i]["end_regis"] = $(tag).find('#end_regis').text();
                }
                i++;
            });
            parent.<?php echo $retFunc; ?>(ret);
            parent.$.fancybox.close();
        }

        function ckAll(tag) {
            $("#tbList tbody tr :checkbox").each(function() {
                if ($(tag).is(":checked")) {
                    $(this).attr('checked', true);
                } else {
                    $(this).attr('checked', false);
                }
            });
        }

    </script>   
</body>
</html>