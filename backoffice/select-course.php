<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
include_once "./share/section.php";
global $db;

$section = get_section(" and a.active='T'");
$coursetype = datatype(" and a.active='T'", "coursetype", true);
$select_course = datatype(" and a.active='T'", "select_course", true);
$str = "ตั้งค่าเลือกวิชา";

$q = "select data_value from config where name='select_course_setting'";
$array_data = $db->data($q);

if(!$array_data){
	$array_data = array();
}else{
	$array_data = json_decode($array_data, true);
}

?>

<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="row">
					<div class="col-md-12">           
					<div class="block-flat">
						<div class="header">              
						<ol class="breadcrumb">
							<li class="active"><?php echo $str; ?></li>
						</ol>
						</div>
						<div class="content">
						<?php //print_r($array_data); ?>
							<form id="frmMain" name="frmMain" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="update-select-course.php">
								<div class="form-group row" >										
										<?php foreach ($section as $key => $value) {
											$section_id = $value['section_id'];
											$name = $value['name'];											
										 ?>
										<h4><?php echo $name; ?></h4>
										<input type="hidden" name="section_id[<?php echo $section_id; ?>]" value="<?php echo $section_id; ?>">
										<table class="table table-striped" id="tbList">
											<thead>
						                        <tr class="alert alert-success" style="font-weight:bold;">
						                            <td width="25%" class="center">อบรม</td>
						                            <td width="25%" class="center">สอบ</td>
						                            <td width="25%" class="center">อบรม+สอบ</td>
						                            <td width="25%" class="center">อบรมต่ออายุ</td>
						                        </tr>
						                    </thead>
											<tbody>
												<tr>
						                            <td width="25%" class="center">
						                            	<?php 
						                            	if($value["aob_rom"]=="have"){ ?>
															<select name="aob_rom[<?php echo $section_id; ?>]" id="aob_rom_<?php echo $section_id; ?>" class="form-control">
																<option value="">---- เลือก ----</option>
																<?php foreach ($select_course as $k => $v) {
																	$id = $v['select_course_id'];
																	$name = $v['name'];
																	$selected = ($array_data[$section_id]["aob_rom"]==$id) ? "selected='selected'" : "";
																	echo  "<option {$selected} value='$id'>$name</option>";
																} ?>

															</select>
						                            	<?php } 
						                            	?>
						                            </td>
						                            <td width="25%" class="center">
						                            	<?php 
						                            	if($value["sorb"]=="have"){ ?>
															<select name="sorb[<?php echo $section_id; ?>]" id="sorb_<?php echo $section_id; ?>" class="form-control">
																<option value="">---- เลือก ----</option>
																<?php foreach ($select_course as $k => $v) {
																	$id = $v['select_course_id'];
																	$name = $v['name'];
																	$selected = ($array_data[$section_id]["sorb"]==$id) ? "selected='selected'" : "";
																	echo  "<option {$selected} value='$id'>$name</option>";
																} ?>

															</select>
						                            	<?php } 
						                            	?>
						                            </td>
						                            <td width="25%" class="center">
						                            	<?php 
						                            	if($value["aob_rom_sorb"]=="have"){ ?>
															<select name="aob_rom_sorb[<?php echo $section_id; ?>]" id="aob_rom_sorb_<?php echo $section_id; ?>" class="form-control">
																<option value="">---- เลือก ----</option>
																<?php foreach ($select_course as $k => $v) {
																	$id = $v['select_course_id'];
																	$name = $v['name'];
																	$selected = ($array_data[$section_id]["aob_rom_sorb"]==$id) ? "selected='selected'" : "";
																	echo  "<option {$selected} value='$id'>$name</option>";
																} ?>

															</select>
						                            	<?php } 
						                            	?>
						                            </td>
						                            <td width="25%" class="center">
						                            	<?php 
						                            	if($value["aob_rom_tor_ar_yu"]=="have"){ ?>
															<select name="aob_rom_tor_ar_yu[<?php echo $section_id; ?>]" id="aob_rom_tor_ar_yu_<?php echo $section_id; ?>" class="form-control">
																<option value="">---- เลือก ----</option>
																<?php foreach ($select_course as $k => $v) {
																	$id = $v['select_course_id'];
																	$name = $v['name'];
																	$selected = ($array_data[$section_id]["aob_rom_tor_ar_yu"]==$id) ? "selected='selected'" : "";
																	echo  "<option {$selected} value='$id'>$name</option>";
																} ?>

															</select>
						                            	<?php } 
						                            	?>
						                            </td>
						                        </tr>
											</tbody>
										</table>
									<?php } ?>
									</div>
								</div>
							</div>
							<div class="clear"></div>
							<div class="form-group row" style="padding-left:10px;">
								<div class="col-sm-12">
									<button type="button" class="btn btn-primary" onClick="ckSave()">Save changes</button>
									<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p'] ?>');">Cancel</button>
								</div>
							</div>
						</form>
						</div>
					</div>
					
					</div>
				</div>

		</div>
	</div> 
</div>
<?php include_once ('inc/js-script.php'); ?>
<script type="text/javascript">
	$(document).ready(function() {
	 $("#frmMain").validate();
	 var course_id = "<?php echo $_GET["course_id"]?>";
	 if(course_id) viewInfo(course_id);
	 
 });
 function viewInfo(course_id){
	 if(typeof course_id=="undefined") return;
	 getnewsInfo(course_id);
}

function getnewsInfo(id){
	if(typeof id=="undefined") return;
	var url = "data/courselist.php";
	var param = "course_id="+id+"&single=T";
	dataUrl(url, param,"#frmMain");
}

function addcourse_detail(){
   var course_id = "<?php echo $_GET["course_id"]?>";
   var url = "index.php?p=<?php echo $_GET["p"];?>&type=detail&course_id="+course_id;
   redirect(url);
}

function editcourse_detail(id){
	if(typeof id=="undefined") return;
	var course_id = "<?php echo $_GET["course_id"]?>";
	var url = "index.php?p=<?php echo $_GET["p"];?>&course_detail_id="+id+"&type=detail&course_id="+course_id;
	redirect(url);
}

function ckSave(id){
	onCkForm("#frmMain");
	$("#frmMain").submit();
} 
</script>