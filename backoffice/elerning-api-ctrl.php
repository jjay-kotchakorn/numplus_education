<?php
error_reporting(0);
header('Access-Control-Allow-Origin: *');
include_once "./connection/connection.php";
include_once "./lib/lib.php";
global $db;
$date_now = date('Y-m-d H:i:s');

function write_log_in_local($file_name="", $msg="", $write_mode="a"){
	$str_txt = "";
	$str = $msg;	
	$date_now = date('Y-m-d H:i:s');
	$date_log = date('Y-m-d');
	$log_name = "./log/".$file_name."_".$date_log.".log";
	$file = fopen($log_name, $write_mode);
	$str_txt = $date_now."|".$str."\r\n";
	fwrite($file, $str_txt);
	fclose($file);
}//end func

function check_auth($api_key="", $api_user=""){
	if ( $api_key=="ati7214" && $api_user=="ati_user" ) {
		return true;
	}else{
		return false;
	}//end else
}//end func

$request_method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : '';
$request_url = !empty($_SERVER["PATH_INFO"]) ? $_SERVER["PATH_INFO"] : '';
if (!array_key_exists('CONTENT_TYPE', $_SERVER) && array_key_exists('HTTP_CONTENT_TYPE', $_SERVER)) {
    $_SERVER['CONTENT_TYPE'] = $_SERVER['HTTP_CONTENT_TYPE'];
}
$content_type = isset($_SERVER['CONTENT_TYPE']) ? $_SERVER['CONTENT_TYPE'] : '';
// $data_values = $_GET;
if ($request_method === 'POST') {
    $data_values = $_POST;
    $auth = check_auth($data_values["api_key"], $data_values["api_user"]);
    if ( $request_url == "/register/" && $auth ) {

    	$q = "SELECT elerning_site_vender_id 
    		FROM elerning_site_vender 
    		WHERE active='T' 
    			AND register_id={$data_values["register_id"]} 
    			AND member_id={$data_values["member_id"]}
    	";
    	$elerning_site_vender_id = $db->data($q);

    	$args = array();
		$args["table"] = "elerning_site_vender";
		
		if( $elerning_site_vender_id ){
		   // $args["id"] = $elerning_site_vender_id;
		   // $args["result_id"] = 0;
		}//end if

		$args["ref_elerning_site_ati_id"] = $data_values["ref_elerning_site_ati_id"];
		$args["register_id"] = $data_values["register_id"];
		$args["member_id"] = $data_values["member_id"];
		$args["cid"] = $data_values["cid"];
		$args["title"] = $data_values["title"];
		$args["fname"] = $data_values["fname"];
		$args["lname"] = $data_values["lname"];
		$args["course_id"] = $data_values["course_id"];
		$args["course_detail_id"] = $data_values["course_detail_id"];
		$args["receive_date"] = $data_values["send_date"];
		$args["ref_uuid"] = $data_values["ref_uuid"];
		$args["birthdate"] = $data_values["birthdate"];
		$args["expire_date"] = $data_values["expire_date"];
		$args["email"] = $data_values["email"];
		$args["code_course_wr"] = $data_values["code_course_wr"];
		$args["rectime"] = $date_now;
		// d($args);
		$ref_elerning_site_vender_id = $db->set($args);
    	// echo "ref_elerning_site_vender_id=".$ref_elerning_site_vender_id;
		$str_txt = "";
	    foreach ($args as $key => $v) {
	    	$str_txt .= "{$key}={$v}|";
	    }//end loop $v
	    write_log_in_local("elearning-api-ctrl-register", $str_txt);

    }elseif ( $request_url == "/result/" ) {
    	$status = "error";
    	// $auth = check_auth($data_values["api_key"], $data_values["api_user"]);

    	$str_txt = "";
		foreach ($data_values as $key => $v) {
	    	$str_txt .= "{$key}={$v}|";
	    }//end loop $v
	    $str_txt .= "system_type=data_from_EL";
	    write_log_in_local("elearning-api-ctrl-result", $str_txt);

    	$args = array();
		$args["table"] = "register_data_list";
		$args["id"] = (int)$data_values["register_data_list_id"];
		$args["ref_elerning_site_vender_id"] = (int)$data_values["ref_elerning_site_vender_id"];
		$args["receive_date"] = $data_values["send_date"];
		

		//$data_values["type"] | 0=training, 1=exam
		$args["coursetype_el"] = $data_values["type"];

		if ( $data_values["type"]==1 ) {
			$args["result_id"] = (int)$data_values["result"];
			$args["result_date"] = $data_values["result_date"];
		}else if( $data_values["type"]==0 ) {
			$args["te_result_id"] = (int)$data_values["result"];
			$args["te_result_date"] = $data_values["result_date"];
		}//end else

		$args["receive_status"] = 'T';
		$args["rectime"] = $date_now;

		if ( $args["id"]==0 || $args["ref_elerning_site_vender_id"]==0 
				|| ($args["result_id"]==0 && $args["te_result_id"]==0) ) {

			if ( $args["id"]==0 ) {
				echo "register_data_list_id is not empty, ";
			}
			if ( $args["ref_elerning_site_vender_id"]==0 ) {
				echo "ref_elerning_site_vender_id is not empty, ";
			}
			if ( $args["result_id"]==0 && $args["te_result_id"]==0 ) {
				echo "result is not empty, ";
			}
			
		}else{
			$rs = $db->set($args);

			//update result to TB register or register_course_detail
			$register_data_list_id = (int)$args["id"];
			$q = "SELECT
					a.register_data_list_id,
					a.ref_elerning_site_vender_id,
					a.register_id,
					a.course_id,
					a.course_detail_id,
					a.member_id,
					a.cid,
					a.title,
					a.fname,
					a.lname,
					a.code_course_wr,
					a.code_project,
					a.code_course_elearning,
					a.send_date,
					a.receive_date,
					a.expire_date,
					a.result_date,
					a.te_result_date,
					a.send_status,
					a.receive_status,
					a.result_id,
					a.te_result_id,
					a.rectime,
					a.active,
					a.ref_uuid,
					a.email,
					a.birthdate,
					a.remark
				FROM register_data_list AS a
				WHERE a.active='T' 
					AND a.register_data_list_id={$register_data_list_id}

			";
			$rs = $db->get($q);
			$rs = $rs[0];
			$register_id = (int)$rs["register_id"];
			$result_id = (int)$rs["result_id"];
			$result_date = $rs["result_date"];
			$te_result_id = (int)$rs["te_result_id"];
			$te_result_date = $rs["te_result_date"];			
			$course_id = $rs["course_id"];
			$course_detail_id = $rs["course_detail_id"];
/*
			if ( empty($data_values["type"]) ) {

			}else if( $data_values["type"]==0 ) {

			}else if( $data_values["type"]==1 ) {

			}//end else
*/
			if ( !empty($register_id) ) {

				// chk credit note
				$q = "SELECT
						register_credit_note_id,
						`code`,
						`name`,
						name_eng,
						active,
						ref_register_id,
						ref_new_register_id,
						runyear,
						runno,
						doc_prefix,
						docno,
						credit_note_date,
						ref_receipt_docno,
						new_receipt_docno,
						ref_pay_price,
						ref_pay_date,
						credit_note_price,
						diff_price,
						recby_id,
						rectime,
						remark,
						reason
					FROM register_credit_note 
					WHERE active='T'
						AND ref_register_id={$register_id}
				";
				$register_credit_note_info = $db->rows($q);


				if ( !empty($register_credit_note_info) ) {
					$ref_new_register_id = $register_credit_note_info["ref_new_register_id"];
					$remark = $register_credit_note_info["remark"]."|ref_register_id=".$register_credit_note_info["ref_register_id"];

					$register_id = $ref_new_register_id;

					$args = array();
					$args["table"] = "register_data_list";
					$args["id"] = $register_data_list_id;
					$args["rectime"] = date("Y-m-d H:i:s");
					$args["remark"] = $remark;
					$db->set($args);

				}//end if
				

				$q = "select register_course_detail_id 
					from register_course_detail 
					where course_detail_id='{$course_detail_id}' 
						and course_id='{$course_id}' 
						and register_id = '{$register_id}'
				";
				//echo $q;
				$register_course_detail_id = $db->data($q);
				if($register_course_detail_id>0){
					$args = array();
					$args["table"] = "register_course_detail";
					$args["id"] = $register_course_detail_id;
					if($result_id>0 || $te_result_id>0){
						//$args["result_date"] = date("Y-m-d H:i:s");
						if ( $data_values["type"]==1 ) {
							$args["register_result_id"] = $result_id;
							$args["result_date"] = $result_date;
							$args["result_from"] = "EL";
						}else if( $data_values["type"]==0 ) {
							$args["register_te_result_id"] = $te_result_id;
							$args["te_result_date"] = $te_result_date;
							$args["te_result_from"] = "EL";
						}//end else
					}else{
						$args["result_date"] = "0000-00-00 00:00:00";
					}//end else

					// $args["register_result_id"] = $result_id;
					// $args["recby_id"] = $EMPID;
					// $args["remark"] = "result from e-learning";
					$args["rectime"] = date("Y-m-d H:i:s");
					$db->set($args);

					$args = array();	
					$args["table"] = "register_data_list";
					$args["id"] = $register_data_list_id;
					$args["register_course_detail_id"] = $register_course_detail_id;
					$db->set($args);
					
				}else{
					$q = "SELECT * FROM register WHERE active='T'
							AND pay_status IN (3,5,6)
							AND register_id=$register_id
					";
					$rs = $db->rows($q);
					$cos_dt_ids = explode(":", $rs["course_detail_id"]);
					if ( count($cos_dt_ids)==2 ) {
						$course_id = $rs["course_id"];
						$course_detail_id = $cos_dt_ids[1];
						$q = "select register_course_detail_id 
							from register_course_detail 
							where course_detail_id='{$course_detail_id}' 
								and course_id='{$course_id}' 
								and register_id = '{$register_id}'
						";
						//echo $q;
						$register_course_detail_id = $db->data($q);
						if($register_course_detail_id>0){
							$args = array();
							$args["table"] = "register_course_detail";
							$args["id"] = $register_course_detail_id;
							if($result_id>0 || $te_results>0){
								if ( $data_values["type"]==1 ) {
									$args["register_result_id"] = $result_id;
									$args["result_date"] = $result_date;
									$args["result_from"] = "EL";
								}else if( $data_values["type"]==0 ) {
									$args["register_te_result_id"] = $te_result_id;
									$args["te_result_date"] = $te_result_date;
									$args["te_result_from"] = "EL";
								}//end else
							}else{
								$args["result_date"] = "0000-00-00 00:00:00";
							}
							// $args["register_result_id"] = $result_id;
							// $args["result_by"] = $EMPID;
							// $args["recby_id"] = $EMPID;
							$args["rectime"] = date("Y-m-d H:i:s");
							$db->set($args);	
						}//end if
					}else{				
						$q = "SELECT remark FROM register WHERE register_id={$register_id}";	
						$rs = $db->data($q);		
						$remark = $rs["remark"];

						$args = array();
						$args["table"] = "register";
						$args["id"] = $register_id;
						if($result_id>0 || $te_results>0)
							//$args["result_date"] = date("Y-m-d H:i:s");
							$args["result_date"] = $result_date;
						else{
							$args["result_date"] = "0000-00-00 00:00:00";
						}
						$args["result"] = $result_id;
						$args["te_results"] = $result_id;
						// $args["result_by"] = $EMPID;
						// $args["recby_id"] = $EMPID;
						$args["remark"] = $remark." #result from e-learning";
						$args["rectime"] = date("Y-m-d H:i:s");
						// $db->set($args);	
										
					}//end else
				}//end else

			}//end if

			$status = "success";
		}//end else
		echo "ref_elerning_site_vender_id=".$args["ref_elerning_site_vender_id"]." has update {$status}";

		$str_txt = "";
		foreach ($args as $key => $v) {
	    	$str_txt .= "{$key}={$v}|";
	    }//end loop $v
	    $str_txt .= "status={$status}|system_type=update_in_WR";
	    write_log_in_local("elearning-api-ctrl-result", $str_txt);
    }
    // d($data_values);

} elseif ($request_method === 'PUT') {
    if (strpos($content_type, 'application/x-www-form-urlencoded') === 0) {
        parse_str($http_raw_post_data, $_PUT);
        $data_values = $_PUT;
    }
} elseif ($request_method === 'PATCH') {
    if (strpos($content_type, 'application/x-www-form-urlencoded') === 0) {
        parse_str($http_raw_post_data, $_PATCH);
        $data_values = $_PATCH;
    }
} elseif ($request_method === 'DELETE') {
    if (strpos($content_type, 'application/x-www-form-urlencoded') === 0) {
        parse_str($http_raw_post_data, $_DELETE);
        $data_values = $_DELETE;
    }
} elseif ($request_method === 'GET') {
	$data_values = $_GET;
/*	
	if ( $request_url=="/product/list/" ) {
		$q="SELECT * FROM course";
		$rs = $db->get($q);
		// return d($_SERVER);die();
		// $rs = json_encode($rs);
		// return json_encode($rs);
		// return var_dump($rs);
		echo json_encode($rs);
		// d($rs);
	}
*/
	if ( $request_url=="/get-result/" ) {
		die();
		$ref_elerning_site_ati_id = (int)$data_values["order_id"];
		$ref_uuid = $data_values["ref_uuid"];
		
		$q="SELECT result_id FROM elerning_site_vender WHERE ref_elerning_site_ati_id={$ref_elerning_site_ati_id} AND ref_uuid='{$ref_uuid}'";
		$rs = $db->data($q);

		d($rs);
		// echo json_encode($rs);
	}
}//end else if

?>