<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
global $db, $SECTIONID, $COURSETYPEID;
$id = $_GET["course_detail_id"];
if($_GET["type"]=="detail"){
	include_once ('coursedetail.php');
}else{

$con_section_id = ($SECTIONID>0) ? " and a.section_id={$SECTIONID}" : "";
$con_coursetype_id = ($COURSETYPEID>0) ? " and a.coursetype_id={$COURSETYPEID}" : "";
$section = datatype(" and a.active='T' {$con_section_id}", "section", true);
if($con_coursetype_id)
	$coursetype = datatype(" and a.active='T' {$con_coursetype_id}", "coursetype", true);
?>
	<div id="cl-wrapper">
		<div class="container-fluid" id="pcont">
			<div class="cl-mcont">
				<div class="col-sm-12">
					<div class="content block-flat ">
						<div class="page-head">
							<button class="btn btn-success btn-small pull-right" onclick="addnew()" style="margin-top:10px;"><i class="fa fa-plus"></i> เพิ่มสถานที่</button>
							<h3><i class="fa fa-list"></i> &nbsp;วันเวลา-สถานที่</h3>
						</div>
						<div class="header">
							<div class="form-group row">
								<label class="col-sm-2 control-label">วันที่  <span class="red">*</span></label>
								<div class="col-sm-2">
									<input class="form-control" name="date_start" id="date_start" onblur="reCall();" placeholder="วันที่" type="text">
								</div>                                          
								<label class="col-sm-1 control-label">ถึงวันที่  <span class="red">*</span></label>
								<div class="col-sm-2">
									<input class="form-control" name="date_stop" id="date_stop" onblur="reCall();" placeholder="ถึงวันที่" type="text">
								</div>  							
								<label class="col-sm-2 control-label">ประเภทใบอนุญาต/คุณวุฒิ <span class="red">*</span></label>
								<div class="col-sm-3">
									<select name="section_id" id="section_id" class="form-control" onchange="reCall();">
										<option value="">---- เลือก ----</option>
										<?php foreach ($section as $key => $value) {
											$id = $value['section_id'];
											if($SECTIONID>0 && $SECTIONID!=$id) continue;
											if($SECTIONID>0 && $SECTIONID==$id){
												$select = "selected";
											}else{
												$select = "";
											}
											$name = $value['name'];
											echo  "<option {$select} value='$id'>$name</option>";
										} ?>

									</select>
								</div>                
							</div>
							<div class="form-group row">
								<label class="col-sm-2 control-label">ประเภทหลักสูตร<span class="red">*</span></label>
								<div class="col-sm-2">
									<select name="coursetype_id" id="coursetype_id" class="form-control" onchange="reCall();">
										<option value="">---- เลือก ----</option>
										<?php foreach ($coursetype as $key => $value) {
											$id = $value['coursetype_id'];
											if($COURSETYPEID>0 && $COURSETYPEID==$id){
												$select = "selected";
											}else{
												$select = "";
											}
											$name = $value['name'];
											echo  "<option {$select} value='$id'>$name</option>";
										} ?>

									</select>
								</div>
								<label class="col-sm-1 control-label">สถานะ</label>
								<div class="col-sm-2">
									<select name="active" id="active" class="form-control" onchange="reCall();">
										<option selected="selected" value="T">แสดง/สมัครได้</option>
										<option value="I"> แสดง/สมัครไม่ได้</option>
										<option value="C">ไม่แสดง/สมัครได้</option>
										<option value="F">ไม่แสดง/สมัครไม่ได้</option>
									</select>
								</div>                                           
								<label class="col-sm-1 control-label"> <a href="#" class="btn btn-rad btn-info" onClick="reCall();"><i class="fa fa-search"></i></a></label>
							</div> 
						</div>
						<table id="tbCourse" class="table" style="width:100%">
							  <thead>
		                        <tr class="alert alert-success" style="font-weight:bold;">
		                            <td width="5%">ลำดับ</td>
		                            <td width="10%" class="center">วัน</td>
		                            <td width="10%" class="center">เวลา</td>
		                            <td width="10%" class="center">สถานที่</td>
		                            <td width="20%" class="center">ข้อมูลสถานที่สอบ/อบรม</td>
		                            <td width="10%" class="center">เปิดรับสมัคร</td>
		                            <td width="10%" class="center">ปิดรับสมัคร</td>
		                            <td width="5%" class="center">สถานะ</td>
		                            <td width="5%" class="center">จัดการ</td>
		                        </tr>
							  </thead>   
							<tbody>
							</tbody>
						</table>
						<div class="clear"></div>
					</div>
				</div>

			</div>
		</div> 
	</div>
	<?php include ('inc/js-script.php') ?>

	<script type="text/javascript">
	$(document).ready(function() {
        $("#date_start").datepicker({language:'th-th',format:'dd-mm-yyyy'});
        $("#date_stop").datepicker({language:'th-th',format:'dd-mm-yyyy'});  
		var oTable;
		listItem();	
		$("#section_id").change(function(event) {
			var id = $(this).val();
			getDropDown('#coursetype_id', id, 'coursetype', 'data/coursetype.php')
		}); 
	});

	function listItem(){
	   var url = "data/coursedetaillist.php";
	   oTable = $("#tbCourse").dataTable({
	   	   "iDisplayLength": 100,
		   "sDom": 'T<"clear">lfrtip',
		   "oLanguage": {
	   	   "sInfoEmpty": "",
	   		"sInfoFiltered": ""
							  },
			"oTableTools": {
				"aButtons":  ""
			},
			"bProcessing": true,
			"bServerSide": true,
			"sAjaxSource": url,
			"sPaginationType": "full_numbers",
			"aaSorting": [[ 0, "desc" ]],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				aoData.push({"name":"section_id","value":$("#section_id").val()});			
				aoData.push({"name":"coursetype_id","value":$("#coursetype_id").val()});			
				aoData.push({"name":"date_start","value":$("#date_start").val()});			
				aoData.push({"name":"date_stop","value":$("#date_stop").val()});			
				aoData.push({"name":"active","value":$("#active").val()});			
				$.ajax( {
					"dataType": 'json', 
					"type": "POST", 
					"url": sSource, 
					"data": aoData, 
					"success": fnCallback
				});
			}
	   }); 
	}

	function editInfo(id){
		if(typeof id=="undefined") return;
	   var url = "index.php?p=<?php echo $_GET["p"];?>&course_detail_id="+id+"&type=detail&flag=detail";
	   redirect(url);
	}
	function addnew(){
	   var url = "index.php?p=<?php echo $_GET["p"];?>&type=detail&flag=detail";
	   //redirect(url);
	   window.open(url, '_blank');
	}

	function reCall(){
		oTable.fnClearTable( 0 );
		oTable.fnDraw();
	}

	</script>
<?php } ?>