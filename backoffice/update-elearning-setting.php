<?php
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";
global $db;

$url_site_wr = $_POST["url_site_wr"];
$url_site_elearning = $_POST["url_site_elearning"];

if ( !empty($_POST) ) {
	$db->query("update config set data_value='{$url_site_wr}' where name='elearning-url-site-wr'");
	$db->query("update config set data_value='{$url_site_elearning}' where name='elearning-url-site-elearning'");		
}//end if

$_SESSION["msg"]["success"] = "บันทึกสำเร็จ";

header('Location: index.php?p=elearning-setting');
?>