<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
global $db, $RIGHTTYPEID, $EMPID;
$apay = datatype(" and a.active='T' and a.pay_status_id in (3,5,6) ", "pay_status", true);

$educationlevel = datatype(" and a.active='T'", "educationlevel", true);
$province = datatype(" and a.active='T'", "province", true);
$receipttype = datatype(" and a.active='T'", "receipttype", true);
$university = datatype_university(true);

$error = $_SESSION["error"]["msg"];
unset($_SESSION["error"]["msg"]);
$success = $_SESSION["success"]["msg"];
unset($_SESSION["success"]["msg"]);

$register_id = $_GET["register_id"];
if(!$register_id) $register_id = $_POST["register_id"];
echo $register_id;
$section = datatype(" and a.active='T'", "section", true);
$v = get_register("", $register_id);

$apay = datatype(" and a.active='T'", "pay_status", true);
$pde = get_config("paystatus_display_edit");
$paystatus_display_edit = array();
if($pde!=""){
	$paystatus_display_edit = explode(",", $pde);
}
$arr_pay = array();
foreach ($apay as $key => $value) {
	$arr_pay[$value["pay_status_id"]] = $value["name"];
}
$str = "";
if($v){
	$v = $v[0];
	$info = $v;
	$str = "ใบเสร็จเลขที่ ".$v["docno"];
	$register_id = $v["register_id"];
	$coursetype_id = $v["coursetype_id"];
	$section_id = $v["section_id"];
	if($info["slip_type"]=="individuals") $slip_type_text = "ออกในนามบุคคลธรรมดา";
	if($info["slip_type"]=="corparation") $slip_type_text = "ออกในนามนิติบุคคล (สำหรับเบิกค่าใช้จ่าย)";
	$q = "select name from receipttype where receipttype_id={$info["receipttype_id"]}";
	$receipttype_name = $db->data($q);
	$q = "select name from district where district_id={$info["receipt_district_id"]}";
	$district_name = $db->data($q);
	$q = "select name from amphur where amphur_id={$info["receipt_amphur_id"]}";
	$amphur_name = $db->data($q);
	$q = "select name from province where province_id={$info["receipt_province_id"]}";
	$province_name = $db->data($q);
	$q = "select register_id from register a where a.active='T' and  a.course_id='$course_id'";
	$id = $db->data($q);
	$total_price = $price - $discount;
	$t = convert_mktime($info["rectime"]) + (2 * 24 * 60 * 60);
}				
if($_POST["flag"]=="edit-receipt"){
	$db->begin();
	$q = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'register_log'";
	$column_name = $db->get($q);
	$register_log_data = array("table"=>'register_log');
	foreach ($column_name as $key => $field) {
		$field_name = $field["COLUMN_NAME"];
		if($info[$field_name]=="") continue;
		$register_log_data[$field_name] = $info[$field_name];
	} 
	unset($register_log_data["register_log_id"]);
/*	echo "<pre>";
	print_r($register_log_data);
	echo "</pre>";*/
	$id = $db->set($register_log_data);
	$args = array();
	$args["table"] = "register";
	$args["id"] = $register_id;
	$args["title"] = $_POST["title_th"];
	$args["fname"] = $_POST["fname_th"];
	$args["lname"] = $_POST["lname_th"];
	$args["slip_type"] = $_POST["slip_type"];
	$args["receipttype_id"] =  (int)$_POST["receipttype_id"];
	$args["receipttype_text"] = $_POST["receipttype_text"];
	$args["receipt_id"] = (int)$_POST["receipt_id"];
	$args["taxno"] = $_POST["taxno"];
	$args["receipt_title"] = $_POST["receipt_title"];
	if($args["receipt_title"]=="อื่นๆ"){
		$args["receipt_title"] = $_POST["receipt_title_text"];
	}
	$args["receipt_fname"] = $_POST["receipt_fname"];
	$args["receipt_lname"] = $_POST["receipt_lname"];
	$args["receipt_no"] = $_POST["receipt_no"];
	$args["receipt_gno"] = $_POST["receipt_gno"];
	$args["receipt_moo"] = $_POST["receipt_moo"];
	$args["receipt_soi"] = $_POST["receipt_soi"];
	$args["receipt_road"] = $_POST["receipt_road"];
	$args["receipt_province_id"] = (int)$_POST["receipt_province_id"];
	$args["receipt_amphur_id"] = (int)$_POST["receipt_amphur_id"];
	$args["receipt_district_id"] = (int)$_POST["receipt_district_id"];
	$args["receipt_postcode"] = $_POST["receipt_postcode"];
	$args["recby_id"] = (int)$EMPID;
	$args["rectime"] = date("Y-m-d H:i:s");
	$section_id = (int)$v["section_id"];
	$coursetype_id = (int)$v["coursetype_id"];
	//$t = rundocno($coursetype_id, $section_id);
	//$args = array_merge($args, $t);
	// $db->set($args);
	$update_success = true;	
	if(!$id){
		$db->rollback();
	}else{		
		$db->commit();
		$v = get_register("", $register_id);
		$str = "";
		if($v){
			$v = $v[0];
			$info = $v;
			$str = "เลขที่ใบเสร็จ ".$v["docno"];
			$register_id = $v["register_id"];
			$coursetype_id = $v["coursetype_id"];
			$section_id = $v["section_id"];
			if($info["slip_type"]=="individuals") $slip_type_text = "ออกในนามบุคคลธรรมดา";
			if($info["slip_type"]=="corparation") $slip_type_text = "ออกในนามนิติบุคคล (สำหรับเบิกค่าใช้จ่าย)";
			$q = "select name from receipttype where receipttype_id={$info["receipttype_id"]}";
			$receipttype_name = $db->data($q);
			$q = "select name from district where district_id={$info["receipt_district_id"]}";
			$district_name = $db->data($q);
			$q = "select name from amphur where amphur_id={$info["receipt_amphur_id"]}";
			$amphur_name = $db->data($q);
			$q = "select name from province where province_id={$info["receipt_province_id"]}";
			$province_name = $db->data($q);
			$q = "select register_id from register a where a.active='T' and  a.course_id='$course_id'";
			$id = $db->data($q);
			$total_price = $price - $discount;
			$t = convert_mktime($info["rectime"]) + (2 * 24 * 60 * 60);			
		}
	}

}


?>
<link rel="stylesheet" type="text/css" href="js/jquery.nanoscroller/nanoscroller.css" />
<link href="js/jquery.icheck/skins/square/blue.css" rel="stylesheet">

<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="row">
				<div class="col-md-12">           
					<div class="block-flat">
						<div class="header">              
							<ol class="breadcrumb">
								<li><a href="#" onClick="clearPage('report&type=register-receipt');">หน้าหลัก</a></li>
								<li class="active"><?php echo $str; ?></li>
							</ol>
						</div>
						<div class="content">
							<?php if($update_success===true){ ?>
								<div class="alert alert-success">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
									<i class="fa fa-save"></i>&nbsp;<strong>แก้ไขใบเสร็จเรียบร้อย!</strong> <?php echo $str; ?>
								</div>
							<?php }else{ ?>
								<!-- <div class="alert alert-warning">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
									<i class="fa fa-warning sign"></i><strong>คำเตือน!</strong> ทุกครั้งที่มี แก้ไขข้อมูลที่อยู่การออกใบเสร็จ ระบบจะทำการ run เลขที่ใบเสร็จ ใหม่ เลขที่ใบเสร็จเดิมจะถูกยกเลิก
								</div> -->
							<?php } ?>

							<form id="frmMain" name="frmMain" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="">


								<input type="hidden" name="register_id" id="register_id" value="<?php echo $info["register_id"]; ?>">								
								<input type="hidden" name="flag" id="flag" value="edit-receipt">								
								<div class="col-sm-12">
									<div class="row">
										<div class="col-md-12" >
											<input type="hidden" name="member_id" id="member_id" value="<?php echo $member_id; ?>">
											<div class="form-group row">
												<div class="col-sm-12">
<!-- 
													<label class="col-sm-2 control-label">เลขที่บัตรประชาชน <span class="red">*</span></label>
													<div class="col-sm-2">
														<input class="form-control required cart_id" name="cid" id="cid" placeholder="รหัสประจำตัวประชาชน" type="text" readonly="readonly">
													</div>
													 -->
													<label class="col-sm-2 control-label">เลขที่ใบเสร็จปัจจุบัน <span class="red">*</span></label>
													<div class="col-sm-2">
														<input class="form-control" type="text" readonly="readonly" value="<?php echo $info["docno"] ?>">
													</div>
													<label class="col-sm-4 control-label"><span class="red" style="font-size:  16px;">( เลขที่ใบเสร็จใหม่จะถูกออกให้หลังจากการกดบันทึกข้อมูล )</span></label>
												</div>
											</div>

<?php 
/*
?>											
											<div class="form-group row">
												<label class="control-label col-sm-1">คำนำหน้า  <span class="red">*</span></label>
												<label class="col-sm-2"> 
													<input class="form-control required" name="title_th" id="title_th"   type="text" readonly="readonly">  
												</label> 
												<label class="col-sm-1">				                			                
													<input class="form-control" id="title_th_text" name="title_th_text" placeholder="คำนำหน้า" type="text" style="max-width:100px;display: none;">
												</label>

												<label class="col-sm-1 control-label">ชื่อ <span class="red">*</span></label>
												<div class="col-sm-3">
													<input class="form-control required" name="fname_th" id="fname_th" placeholder="ชื่อ" type="text" readonly="readonly">
												</div>			                			                
												<label class="col-sm-1 control-label">นามสกุล <span class="red">*</span></label>
												<div class="col-sm-3">
													<input class="form-control required " name="lname_th" id="lname_th" placeholder="นามสกุล" type="text" readonly="readonly">
												</div>
											</div>


											<div class="no-top">
												<?php if($info["slip_type"]){?>
												<div class="header">
													<h4>ข้อมูลสำหรับแสดงบนใบเสร็จรับเงิน</h4>
												</div>	
												<div class="form-group row">	                			                
													<label class="col-sm-1 control-label">ออกใบเสร็จ</label>
													<div class="col-sm-2"><?php echo $slip_type_text; ?></div>		                			                
													<label class="col-sm-2 control-label">ออกใบเสร็จในนาม</label>
													<div class="col-sm-3">
														<?php 
														$receipt_id = $info["receipt_id"];
														if($receipttype_name!="" && $info["slip_type"]=="corparation"){
															if($info["receipttype_id"]==5){
																$receipttype_name = "";
																$name = $info["receipttype_text"];
															}else{
																$name = $db->data("select name from receipt where receipt_id=$receipt_id");
															}
															echo $receipttype_name." ".$name;
														}else{
															echo $info["receipt_title"]." ".$info["receipt_fname"]."&nbsp&nbsp&nbsp&nbsp".$info["receipt_lname"]; 
														}
														?>

													</div>
													<label class="col-sm-2 control-label">เลขที่ประจำตัวผู้เสียภาษี</label>
													<div class="col-sm-2"><?php echo $info["taxno"]; ?></div>			                			                
												</div>
												<div class="form-group row">
													<label class="col-sm-1 control-label">บ้านเลขที่</label>
													<div class="col-sm-2"><?php echo $info["receipt_no"]; ?></div>
													<label class="col-sm-2 control-label">หมู่บ้าน / คอนโด / อาคาร</label>
													<div class="col-sm-2"><?php echo $info["receipt_gno"]; ?></div>
													<label class="col-sm-1 control-label">หมู่ที่</label>
													<div class="col-sm-1"><?php echo $info["receipt_moo"]; ?></div>
													<label class="col-sm-1 control-label">ซอย</label>
													<div class="col-sm-2"><?php echo $info["receipt_soi"]; ?></div>
												</div>
												<div class="form-group row">
													<label class="col-sm-1 control-label">ถนน</label>
													<div class="col-sm-2"><?php echo $info["receipt_road"]; ?></div>
													<label class="col-sm-2 control-label">ตำบล / แขวง</label>
													<div class="col-sm-2"><?php echo $district_name; ?></div>
													<label class="col-sm-1 control-label">อำเภอ / เขต</label>
													<div class="col-sm-2"><?php echo $amphur_name; ?></div>
												</div>
												<div class="form-group row">
													<label class="col-sm-1 control-label">จังหวัด</label>
													<div class="col-sm-2"><?php echo $province_name; ?></div>
													<label class="col-sm-2 control-label">รหัสไปรษณีย์</label>
													<div class="col-sm-2"><?php echo $info["receipt_postcode"]; ?></div>
												</div>
												<?php } ?>													
											</div>
<?php 
*/
?>

											<div class="header">							
												<h4><i class="fa fa-pencil"></i>&nbsp;แก้ไขข้อมูลสำหรับออกใบเสร็จใบใหม่</h4>
											</div><br>

											<div class="form-group row">
												<label class="control-label col-sm-2">วันที่แสดงบนใบเสร็จ</label>
												<div class="col-sm-2">
													<input class="form-control " name="start_date" id="start_date"  placeholder="วันที่บนใบเสร็จ" type="text">
												</div>	
											</div>

											<div class="form-group row">						
												<div class="col-sm-12">								
													<label class="radio-inline"  onclick="corparation_click(this)" style="padding-left:10px;padding-top:0px"> <div aria-disabled="false" aria-checked="false" style="position: relative;" class="iradio_square-blue"><input style="position: absolute; opacity: 0;" value="individuals" name="slip_type" class="icheck required" type="radio"><ins style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;" class="iCheck-helper"></ins></div> ออกในนามบุคคลธรรมดา <span class="red">*</span> </label> 
													<label class="radio-inline"  onclick="corparation_click(this)" style="padding-left:10px;padding-top:0px"> <div aria-disabled="false" aria-checked="false" style="position: relative;" class="iradio_square-blue"><input style="position: absolute; opacity: 0;" value="corparation" name="slip_type" class="icheck required" type="radio"><ins style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;" class="iCheck-helper"></ins></div> ออกในนามนิติบุคคล (สำหรับเบิกค่าใช้จ่าย) <span class="red">*</span> </label> 
												</div>
											</div>

											<div id="corparationinfo" style="display: none;">										
												<div style="display:inline-block;margin-left:15px;" class="form-group row">
													<?php foreach ($receipttype as $key => $value) {
														$click = "getDropDown('#receipt_id', '{$value["receipttype_id"]}', 'receipt', 'data/receipt.php')";
														echo '<label class="radio-inline" onclick="'.$click.'" style="padding-left:10px;padding-top:0px"> <div aria-disabled="false" aria-checked="false" style="position: relative;" class="iradio_square-blue"><input style="position: absolute; opacity: 0;" value="'.$value["receipttype_id"].'" name="receipttype_id"  class="icheck" type="radio"><ins style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;" class="iCheck-helper"></ins></div>&nbsp;&nbsp;'.$value["name"].'</label>';
													} ?>
													<label class="radio-inline" style="padding-left:10px;padding-top:0px">
														<input  maxlength="200" class="form-control" name="receipttype_text" id="receipttype_text" type="text">
													</label>
												</div>	
											</div>							

											<div  id="section_corparation_receipt" style="display: none;"class="form-group row">
												<label class="control-label col-sm-2">ออกใบเสร็จในนาม</label>
												<label class="radio-inline col-sm-3" style="padding-left:10px;padding-top:0px">
													<select name="receipt_id" class="form-control" id="receipt_id" onchange="receipt_info(this.value);">
														<option value="">กรุณาเลือกจากรายการที่ปรากฎ</option>
													</select>											
												</label>
												<label class="control-label col-sm-1">สาขา </label>									
												<label class="radio-inline col-sm-2" style="padding-left:10px;padding-top:0px">
													<input  name="branch" class="form-control required" id="branch" type="text">											
												</label>
											</div>
											<div id="section_individuals_receipt" class="form-group row" style="display: none;">																
												<label class="control-label col-sm-2">ออกใบเสร็จในนาม</label>
												<div class="col-sm-1" style="padding-right:0px;">
													<select name="receipt_title" id="receipt_title" class="regis_select form-control">
														<option value="นาย">นาย</option>
														<option value="นาง">นาง</option>
														<option value="นางสาว">นางสาว</option>
														<option value="อื่นๆ">อื่นๆ</option>
													</select> </div>
													<label class="radio-inline col-sm-2" style="padding-left:10px;padding-top:0px">				                			                
														<input class="form-control" id="receipt_title_text" name="receipt_title_text" placeholder="คำนำหน้า" type="text" style="max-width:150px; display:none;">
													</label>												                
													<label class="col-sm-1 control-label">ชื่อ</label>
													<div class="col-sm-2">
														<input class="form-control required" name="receipt_fname" id="receipt_fname" placeholder="ชื่อ" type="text">
													</div>			                			                
													<label class="col-sm-1 control-label">นามสกุล</label>
													<div class="col-sm-2">
														<input class="form-control required " name="receipt_lname" id="receipt_lname" placeholder="นามสกุล" type="text">
													</div>

												</div>
												<div class="form-group row">

													<label class="control-label col-sm-2" style="font-size:12px;">เลขที่บัตรประจำตัวผู้เสียภาษี <span class="red">*</span></label>									
													<label class="radio-inline col-sm-3" style="padding-left:10px;padding-top:0px">
														<input class="form-control required" name="taxno" id="taxno" maxlength="13" type="text"> 											
													</label>
												</div>
												<div class="form-group row"> 	
													<label class="col-sm-2 control-label">บ้านเลขที่  <span class="red">*</span></label>
													<div class="col-sm-2">
														<input class="form-control required" name="receipt_no" id="receipt_no" placeholder="บ้านเลขที่" type="text">
													</div>			                
													<label class="col-sm-2 control-label">หมู่บ้าน / คอนโด / อาคาร  </label>
													<div class="col-sm-2">
														<input class="form-control" name="receipt_gno" id="receipt_gno" placeholder="หมู่บ้าน / คอนโด / อาคาร" type="text">
													</div>
													<label class="col-sm-2 control-label">หมู่ที่ </label>
													<div class="col-sm-2">
														<input class="form-control " name="receipt_moo" id="receipt_moo" placeholder="หมู่ที่" type="text">
													</div>
												</div>
												<div class="form-group row">
													<label class="col-sm-2 control-label">ซอย</label>
													<div class="col-sm-2">
														<input class="form-control " name="receipt_soi" id="receipt_soi" placeholder="ซอย" type="text">
													</div>
													<label class="col-sm-2 control-label">ถนน </label>
													<div class="col-sm-2">
														<input class="form-control " name="receipt_road" id="receipt_road" placeholder="ถนน " type="text">
													</div>
												</div>							  		
												<div class="form-group row">
													<label class="col-sm-2 control-label">จังหวัด  <span class="red">*</span></label>
													<div class="col-sm-2">
														<select name="receipt_province_id" id="receipt_province_id" class="form-control required" onchange="getDropDown('#receipt_amphur_id', this.value, 'amphur', 'data/province.php')">
															<option value="">---- เลือก ----</option>
															<?php foreach ($province as $key => $value) {
																$id = $value['province_id'];
																$name = $value['name'];
																echo  "<option value='$id'>$name</option>";
															} ?>

														</select>
													</div>
													<label class="col-sm-2 control-label">อำเภอ  <span class="red">*</span></label>
													<div class="col-sm-2">
														<select name="receipt_amphur_id"  id="receipt_amphur_id" class="form-control required" onchange="getDropDown('#receipt_district_id', this.value, 'district', 'data/province.php')">
															<option value="">---- เลือก ----</option>								
														</select>
													</div>
													<label class="col-sm-2 control-label">ตำบล/แขวง  <span class="red">*</span></label>
													<div class="col-sm-2">
														<select name="receipt_district_id" id="receipt_district_id" class="form-control required">
															<option value="">---- เลือก ----</option>								
														</select>
													</div>
												</div>
												<div class="form-group row">								
													<label class="col-sm-2 control-label">รหัสไปรษณีย์  <span class="red">*</span></label>
													<div class="col-sm-2">
														<input  id="receipt_postcode" class="form-control required" name="receipt_postcode" maxlength="6" type="text">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="clear"></div>
								<div class="form-group row" style="padding-left:10px;">
									<div class="col-sm-12">
										<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p'] ?>');">กลับสู่หน้าหลัก</button>
										<button type="button" id="bt_save" class="btn btn-primary" onClick="ckSave()">บันทึกข้อมูล</button>																			
									</div>
								</div>
							</form>
						</div>
					</div>

				</div>
			</div>

		</div>
	</div> 
</div>
<?php include_once ('inc/js-script.php'); ?>


<script type="text/javascript">
	$(document).ready(function() {
		var fv = $("#frmMain").validate();
		var member_id = "<?php echo $info["member_id"]?>";
		if(member_id) viewInfo(member_id);
		$("#birthdate").datepicker({language:'th-th',format:'dd-mm-yyyy'});
/*		$('input').iCheck({
			checkboxClass: 'icheckbox_square-blue checkbox',
			radioClass: 'iradio_square-blue'
		});*/
		$("#start_date").datepicker({language:'th-th',format:'dd-mm-yyyy' });

		$('#cid').keypress(function(event){ 
			var keycode = (event.keyCode ? event.keyCode : event.which);
			if(keycode == '13'){
				getCode();
			}

		});


	});
 

		$(function(){
			$("#title_th").change(function(){
				if($(this).val() == 'นาย'){ 
					$("#title_en").val("Mr");
					$("#title_th_text").hide();
					$("#title_en_text").hide();
					if($("#title_th_text").hasClass('required'))  $("#title_th_text").removeClass('required');
					if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
					$("#gender").val("M");
				}
				else if($(this).val() == 'นาง'){ 
					$("#title_en").val("Mrs");
					$("#title_th_text").hide();
					$("#title_en_text").hide();
					if($("#title_th_text").hasClass('required'))  $("#title_th_text").removeClass('required');
					if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
					$("#gender").val("F");
				}
				else if($(this).val() == 'นางสาว'){ 
					$("#title_en").val("Ms");
					$("#title_th_text").hide();
					$("#title_en_text").hide();
					if($("#title_th_text").hasClass('required'))  $("#title_th_text").removeClass('required');
					if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
					$("#gender").val("F");
				}
				else if($(this).val() == 'อื่นๆ'){ 
					$("#title_en").val("Other");
					$("#title_th_text").show();
					$("#title_en_text").show();
					$("#title_th_text").addClass('required');
					$("#title_en_text").addClass('required');
					$("#gender").val("");
				}
			});

$("#title_en").change(function(){
	if($(this).val() == 'Mr'){ 
		$("#title_en_text").hide();
		if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
		$("#gender").val("M");
	}
	else if($(this).val() == 'Mrs'){ 
		$("#title_en_text").hide();
		if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
		$("#gender").val("F");
	}
	else if($(this).val() == 'Ms'){ 
		$("#title_en_text").hide();
		if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
		$("#gender").val("F");
	}
	else if($(this).val() == 'Other'){ 
		$("#title_en_text").show();
		$("#title_en_text").addClass('required');
		$("#gender").val("");
	}
});
$("#receipt_title").change(function(){
	if($(this).val() == 'นาย'){ 
		$("#receipt_title_text").hide();
		if($("#receipt_title_text").hasClass('required'))  $("#receipt_title_text").removeClass('required');
	}
	else if($(this).val() == 'นาง'){ 
		$("#receipt_title_text").hide();
		if($("#receipt_title_text").hasClass('required'))  $("#receipt_title_text").removeClass('required');
	}
	else if($(this).val() == 'นางสาว'){ 
		$("#receipt_title_text").hide();
		if($("#receipt_title_text").hasClass('required'))  $("#receipt_title_text").removeClass('required');
	}
	else if($(this).val() == 'อื่นๆ'){ 
		$("#receipt_title_text").show();
		$("#receipt_title_text").addClass('required');
	}
});
$(".radio-inline").click(function(){
	$(this).find($("input[type=radio][name='slip_type']")).change(function(){   
		var t = $(this).val();
		if(t == 'corparation'){ 
			$("#taxno").addClass('required');				
		}else{ 
			if($("#taxno").hasClass('required'))  $("#taxno").removeClass('required');
		}
	});
});
$('input[name=show_pass]').on('ifChecked', function(event){
	$("#password, #password2").attr("type", "text");
});			
$('input[name=show_pass]').on('ifUnchecked', function(event){
	$("#password, #password2").attr("type", "password");
});	

$("input[name='receipttype_id']").on('ifChanged', function(event){
	if($(this).val() == '5'){ 
		$("#receipttype_text").addClass('required');
		$("#section_corparation_receipt").hide();
	}else{ 
		if($("#receipttype_text").hasClass('required'))  $("#receipttype_text").removeClass('required');
		$("#section_corparation_receipt").show();
	}
	$("#taxno").val('');
	$("#branch").val('');
	//checkboxaddress_reset();
});
$('input[name=usebook]').on('ifChecked', function(event){
	$(this).val("T");
});		
$('input[name=usebook]').on('ifUnchecked', function(event){
	$(this).val("F");
	//checkboxaddress_reset();
});

});



function viewInfo(member_id){
	if(typeof member_id=="undefined") return;
	getmemberInfo(member_id);
}

function getmemberInfo(id){
	if(typeof id=="undefined") return;
	var url = "data/memberlist.php";
	var param = "member_id="+id+"&single=T";
	$.ajax( {
		"dataType":'json', 
		"type": "POST",
		"async": false, 
		"url": url,
		"data": param, 
		"success": function(data){	
			$.each(data, function(index, array){                
				$("#taxno").val(array.taxno);
				$("#branch").val(array.branch);
				$("#receipt_no").val(array.no);
				$("#receipt_gno").val(array.gno);
				$("#receipt_moo").val(array.moo);
				$("#receipt_soi").val(array.soi);
				$("#receipt_road").val(array.road);
				$("#receipt_postcode").val(array.postcode);
				var t = array.slip_type;
				corparation_info(t);
				$("select[name=receipt_province_id] option").filter(function() {
					return $(this).val() == array.province_id; 
				}).attr('selected', true);	
				var select = $("#receipt_amphur_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.receipt_amphur_id+'"> '+array.receipt_amphur_name+' </option>');	
				var select = $("#receipt_district_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.receipt_district_id+'"> '+array.receipt_district_name+' </option>');				
				var select = $("#amphur_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.amphur_id+'"> '+array.amphur_name+' </option>');	
				var select = $("#district_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.district_id+'"> '+array.district_name+' </option>');
				var select = $("#receipt_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.receipt_id+'"> '+array.receipt_name+' </option>');
				setVal("#frmMain", array);
				var select = $("#org_name");
				$('option', select).remove();
				$(select).append('<option value="'+array.org_name+'"> '+array.org_name_text+' </option>'); 
				if(!array.org_name_text){
					array.org_name_text = '-';
				}
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-blue checkbox',
					radioClass: 'iradio_square-blue'
				});
				if($("#title_th").val() == 'อื่นๆ'){ 
					$("#title_th").val(array.title_th_text);
				}	
				if($("#receipt_title").val() == 'อื่นๆ'){ 
					$("#receipt_title_text").show();
					$("#receipt_title_text").val(array.receipt_title_text);
				}				
			});	

			console.log(array);			 
}
});
}
function corparation_click(tag){
	$(tag).find('input').iCheck('check');
	var desp = $(tag).find('input').val();
	corparation_info(desp);
}
function corparation_info(desp){
	if(desp=="corparation") {
		$("#corparationinfo").show();
		$("#section_corparation_receipt").show();
		$("#section_individuals_receipt").hide();
		$("#taxno").val('');
		$("#branch").val('');
	}else{
		$("#corparationinfo").hide();
		$("#section_corparation_receipt").hide();
		$("#section_individuals_receipt").show();
		var select = $("#receipt_id");
		var options = select.attr('options');
		$('option', select).remove();
		$(select).append('<option value="">---- เลือก ----</option>');
		$("#taxno").val('');
		$("#branch").val('');
	}
	//checkboxaddress_reset();
}

function receipt_info(id){
	if(typeof id=="undefined") return;
	var url = "data/receipt.php";
	var param = "name=receipt_detail&value="+id;
	$.ajax( {
		"dataType":'json', 
		"type": "GET",
		"async": false, 
		"url": url,
		"data": param, 
		"success": function(data){
			console.log(data);	
			$.each(data, function(index, array){			 				 	
				$("#taxno").val(array.taxno);
				$("#branch").val(array.branch);
				$("#receipt_no").val(array.no);
				$("#receipt_gno").val(array.gno);
				$("#receipt_moo").val(array.moo);
				$("#receipt_soi").val(array.soi);
				$("#receipt_road").val(array.road);
				$("#receipt_postcode").val(array.postcode);
				$("select[name=receipt_province_id] option").filter(function() {
					return $(this).val() == array.province_id; 
				}).attr('selected', true);		
				var select = $("#receipt_amphur_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.amphur_id+'"> '+array.amphur_name+' </option>');	
				var select = $("#receipt_district_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.district_id+'"> '+array.district_name+' </option>');					
			});				 
		}
	});
}
function removeImg(){
	var defaultImg = "images/no-avatar-male.jpg";
	if($('input[name=tmpimg]').val()=="")return;
	var t = confirm("ลบรูปภาพ");
	if(!t) return;
	$('#img').attr('src', defaultImg);
	$('input[name=delimg]').val("T");	
	ckSave();
}
function ckSave(){
	var t = confirm("ยืนยันแก้ไขข้อมูลที่อยู่การออกใบเสร็จ");
	if(!t) return false;
	onCkForm("#frmMain");
	$("#frmMain").submit();
}

function checkboxaddress(){

	$("#receipt_title").val($("#title_th").val());
	if($("#receipt_title").val() == 'อื่นๆ'){ 
		$("#receipt_title_text").show();
		$("#receipt_title_text").addClass('required');
		$("#receipt_title_text").val($("#title_th_text").val());
	}
	else
	{
		$("#receipt_title_text").hide();
		$("#receipt_title_text").removeClass('required');
		$("#receipt_title_text").val("");
	}
	$("#receipt_fname").val($("#fname_th").val());
	$("#receipt_lname").val($("#lname_th").val());    

	$("#taxno").val($("#cid").val());
	$("#receipt_no").val($("#no").val());
	$("#receipt_gno").val($("#gno").val());
	$("#receipt_moo").val($("#moo").val());
	$("#receipt_soi").val($("#soi").val());
	$("#receipt_road").val($("#road").val());

	$('#receipt_province_id').val($('#province_id').val());

	var select = $("#receipt_amphur_id");
	$('option', select).remove();
	$(select).append('<option value="'+$("#amphur_id").val()+'"> '+ $("#amphur_id").find(":selected").text()+' </option>'); 

	var select = $("#receipt_district_id");
	$('option', select).remove();
	$(select).append('<option value="'+$("#district_id").val()+'"> '+$("#district_id").find(":selected").text()+' </option>');  

	$("#receipt_postcode").val($("#postcode").val());

}

function checkboxaddress_reset()
{
	$("#receipt_title").val("");
	$("#receipt_title_text").hide();
	$("#receipt_title_text").removeClass('required');
	$("#receipt_title_text").val("");
	$("#receipt_fname").val("");
	$("#receipt_lname").val("");    

	$("#taxno").val("");
	$("#receipt_no").val("");
	$("#receipt_gno").val("");
	$("#receipt_moo").val("");
	$("#receipt_soi").val("");
	$("#receipt_road").val("");

	$('#receipt_province_id').val("");

	var select = $("#receipt_amphur_id");
	$('option', select).remove();
	$(select).append('<option value="">---- เลือก ----</option>');

	var select = $("#receipt_district_id");
	$('option', select).remove();
	$(select).append('<option value="">---- เลือก ----</option>');

	$("#receipt_postcode").val("");
}
function change_select_university()
{
	var id = $("#select_university").val().split("-");
	$("#grd_ugrp1").val(id[0]);
	$("#grd_uid1").val(id[1]);
	$("#grd_uname1").val($("#select_university option:selected").text());
}
function getCode(){
	var url = "data/member-info.php";
	var param = "cid="+$('#cid').val();
	$.ajax( {
		"dataType":'json', 
		"type": "POST",
		"async": false, 
		"url": url,
		"data": param, 
		"success": function(data){	
			if(data==""){
				msgError("ไม่พบข้อมูลของ "+$('#cid').val());
			}
			$.each(data, function(index, array){                
				$("#taxno").val(array.taxno);
				$("#branch").val(array.branch);
				$("#receipt_no").val(array.no);
				$("#receipt_gno").val(array.gno);
				$("#receipt_moo").val(array.moo);
				$("#receipt_soi").val(array.soi);
				$("#receipt_road").val(array.road);
				$("#receipt_postcode").val(array.postcode);
				var t = array.slip_type;
				corparation_info(t);
				$("select[name=receipt_province_id] option").filter(function() {
					return $(this).val() == array.province_id; 
				}).attr('selected', true);	
				var select = $("#receipt_amphur_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.receipt_amphur_id+'"> '+array.receipt_amphur_name+' </option>');	
				var select = $("#receipt_district_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.receipt_district_id+'"> '+array.receipt_district_name+' </option>');				
				var select = $("#amphur_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.amphur_id+'"> '+array.amphur_name+' </option>');	
				var select = $("#district_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.district_id+'"> '+array.district_name+' </option>');
				var select = $("#receipt_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.receipt_id+'"> '+array.receipt_name+' </option>');
				setVal("#frmMain", array);
				var select = $("#org_name");
				$('option', select).remove();
				$(select).append('<option value="'+array.org_name+'"> '+array.org_name_text+' </option>'); 
				if(!array.org_name_text){
					array.org_name_text = '-';
				}
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-blue checkbox',
					radioClass: 'iradio_square-blue'
				});			
			});				 
		}
	});
}
function ckdup_register(id){
	if(typeof id=="undefined") return;
	var member_id = "<?php echo $member_id;?>";
	var url = "data/ckdup-register.php";
	var param = "course_detail_id="+id+"&member_id="+member_id+"&format=<?php echo $format; ?>";
	$.ajax({
			"dataType":'json', 
			"type": "POST",
			"async": false, 
			"url": url,
			"data": param, 
			"success": function(data){  
         		var ck = 0;
         		$.each(data, function(index, array){
         			ck++;
         		});
         		if(ck>0){
         			msgError("");
         		}
			}
	});
}
</script>
<style>
	#set_time-error{
		display: none !important;
	}
	#pay_status-error, #slip_type-error{
		width: 400px;
		margin-top: 21px;
	}
	.no-top .control-label{
		padding-top: 0px !important;
	}
</style>