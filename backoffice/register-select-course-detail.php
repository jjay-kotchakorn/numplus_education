<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
global $db;
global $SECTIONID;

$error = $_SESSION["error"]["msg"];
unset($_SESSION["error"]["msg"]);
$success = $_SESSION["success"]["msg"];
unset($_SESSION["success"]["msg"]);

$course_id = $_GET["course_id"];
$section = datatype(" and a.active='T'", "section", true);
$coursetype = datatype(" and a.active='T'", "coursetype", true);
$q = "select  title from course where course_id=$course_id";
$r = $db->rows($q);
$str = "";
if($r){
	$str = $r["title"];
	$info = get_course("", $course_id);
	if($info) $info = $info[0];
}else{
	$str = "เพิ่มหลักสูตร";
}
?>
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="row">
				<div class="col-md-12">           
					<div class="block-flat">
						<div class="header">              
							<ol class="breadcrumb">
								<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>');">หน้าหลัก</a></li>
								<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>&type=select-course');">เลือกหลักสูตร</a></li>
								<li class="active">เลือกสถานที่</li>
							</ol>
						</div>
						<div class="content">
							<form id="frmMain" name="frmMain" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="">
								<input type="hidden" name="course_id" id="course_id" value="<?php echo $course_id; ?>">
								
								<div class="col-sm-12">
									<div class="form-group row">
									<label class="col-sm-1 control-label" style="width:118px;">รหัสหลักสูตร </label>
										<div class="col-sm-2" style="width:150px;">
											<div class="input-text">
												<?php echo $info["code"]; ?>
											</div>
										</div>
										<label class="col-sm-2 control-label">ประเภทใบอนุญาต/คุณวุฒิ </label>
										<div class="col-sm-3">
											<div class="input-text">
												<?php echo $info["section_name"]; ?>
											</div>
										</div>                
										<label class="col-sm-2 control-label">ประเภทหลักสูตร</label>
										<div class="col-sm-2">
											<div class="input-text">
												<?php echo $info["coursetype_name"]; ?>
											</div>
										</div>
									</div>                        
									<div class="form-group row"> 
										<label class="col-sm-1 control-label" style="padding-right:10px;" >ชื่อหลักสูตร <span class="red" style="display:inline-block; position:absolute;"> &nbsp;*</span></label>
										<div class="col-sm-5">
											<div class="input-text">
												<?php echo $info["title"]; ?>
											</div>	
											<?php echo $info["detail"]; ?>
										</div>                               
										<label class="col-sm-1 control-label">ระยะเวลา</label>
										<div class="col-sm-1">
											<div class="input-text">
												<?php echo $info["set_time"]; ?>&nbsp;<?php echo $info["unit_time"]; ?>
											</div>
										</div>                                          
										<label class="col-sm-1 control-label" style="padding-left: 5px; padding-right: 5px;">ราคา (บาท) </label>
										<div class="col-sm-1">
											<div class="input-text">
												<?php echo set_comma($info["price"]); ?>
											</div>	
										</div> 
										<div class="col-sm-1">
											<div class="input-text" > <strong>
												<?php echo $info["inhouse"]=="T" ? "inhouse" : ""; ?>
												</strong>
											</div>	
										</div>
									</div>
									<?php 
										$start = date("Y-m-01");
										$stop = date('Y-m-t',strtotime('today'));
										$dateStart = ($_POST["date_start"]) ? thai_to_timestamp($_POST["date_start"]) :  $start;
										$dateStop =  ($_POST["date_stop"]) ? thai_to_timestamp($_POST["date_stop"]) : $stop;
										if ($dateStart || $dateStop) {
										    if (!$dateStart && $dateStop)
										        $dateStart = $dateStop;
										    if (!$dateStop && $dateStart)
										        $dateStop = $dateStart;
										    $t = $dateStart;
										    if ($dateStart > $dateStop) {
										        $dateStart = $dateStop;
										        $dateStop = $t;
										    }
										}
									 ?>
								<div class="form-group row"> 
									<label class="col-sm-1 control-label"  >วันที่เริ่ม</label>
									<div class="col-sm-2"  >
										<input class="form-control" name="date_start" value="<?php echo revert_date($dateStart); ?>" id="date_start" onblur="reCall();" placeholder="วันที่เริ่ม" type="text">
									</div>                                          
									<label class="col-sm-1 control-label"  >วันที่สิ้นสุด</label>
									<div class="col-sm-2" >
										<input class="form-control" name="date_stop" id="date_stop" value="<?php echo revert_date($dateStop); ?>" onblur="reCall();" placeholder="วันที่สิ้นสุด" type="text">
									</div>  
									<label class="col-sm-1 control-label">PUB / INH</label>
									<div class="col-sm-2">
										<select name="check_inh" id="check_inh" class="form-control">
											<option selected="selected"  value="">--เลือก--</option>
											<option value="pub">PUB</option>
											<option value="inh">INH</option>
										</select>
									</div>   
									<button class="btn" id="filter_date"> <i class="fa fa-search"></i></button>
								</div> 
									<div class="form-group row" <?php if(!$course_id) echo 'style="display:none;"'; ?>>
										<h4><i class="fa  fa-clock-o"></i> &nbsp;ข้อมูลเวลา/สถานที่</h4>
										<table class="table table-striped" id="tbList">
											<thead>
												<tr class="alert alert-success" style="font-weight:bold;">
													<td width="3%">ลำดับ</td>
													<td width="8%" class="center">วัน</td>
													<td width="8%" class="center">เวลา</td>
													<td width="8%" class="center">สถานที่</td>
													<td width="15%" class="center">ข้อมูลสถานที่สอบ/อบรม</td>
													<td width="7%" class="center">ที่นั่งคงเหลือ (จอง)</td>
													<td width="5%" class="center">ที่นั่งทั้งหมด</td>
													<td width="6%" class="center">สถานะ</td>
													<td width="6%" class="center">เปิดรับสมัคร</td>
													<td width="6%" class="center">ปิดรับสมัคร</td>
													<td width="6%" class="center"> In House</td>
													<td width="12%" class="center">ลงทะเบียน</td>
												</tr>
											</thead>
											<tbody>
												<?php 
												$select_year = date("Y");
												$select_month = date("m");
												$sWhere = ($dateStart && $dateStop) ? " and a.date>='$dateStart' and a.date<='$dateStop'" : "";
												if($_POST["check_inh"]=="pub") $sWhere .= " and a.inhouse='F'";
												if($_POST["check_inh"]=="inh") $sWhere .= " and a.inhouse='T'";
												$r = get_course_detail(" and a.active='T' and a.course_id=$course_id $sWhere");

												if($r){
													$runNo = 1; 
													foreach($r as $k=>$v){
														$course_detail_id = $v["course_detail_id"];
														$use = 0;
														//$use = get_use_chair($course_detail_id);
														$chair_all = $v['chair_all'];
														$book = $v["book"];
														$usebook = 0;
														if($book>0) {
															$usebook += $v["book_all"];
															//$usebook += get_use_book($course_detail_id);
														}
														$sit_all = $chair_all - $book - $use;
														$status = get_course_status_name($sit_all, $v["status"], $v["coursetype_id"]);
														if($v["date"]<date("Y-m-d")){
															$status = "ปิดรับสมัคร";
														}
														?>
														<tr style="cursor:pointer;">
															<td class="center"><?php echo $runNo; ?></td>
															<td class="center"><strong style="font-size:15px;"><?php echo $v["day"]; ?></strong> <?php echo revert_date($v["date"]); ?></td>
															<td class="center"><?php echo $v["time"]; ?></td>
															<td class="center"><?php echo $v["address_detail"]; ?></td>
															<td class="center"><?php echo $v["address"]; ?></td>													
															<td class="center"><?php echo $sit_all; ?> (<?php echo (int) $book - $usebook; ?>) </td>													
															<td class="center"><?php echo $chair_all-$book ," (", $book; ?>)</td>													
															<td class="center"><?php echo $status; ?></td>													
															<td class="center"><?php echo revert_date($v["open_regis"]); ?></td>
															<td class="center"><?php echo revert_date($v["end_regis"]); ?></td>
															<td class="center"><?php echo ($v["inhouse"]=="T") ? '<i class="fa fa-check-square-o"></i>' : ''; ?></td>
															<td class="center">
																<a class="btn btn-small " data-toggle="tooltip" data-original-title="สมัครแบบด่วน (ครั้งละคน)" href="#" onclick="add_to_walkin(<?php echo $v["course_detail_id"]; ?>);"><i class="fa fa-user"></i></a>
																<a class="btn btn-small " data-toggle="tooltip" data-original-title="สมัครแบบเป็นกลุ่ม (Import)" href="#" onclick="add_to_import(<?php echo $v["course_detail_id"]; ?>);"><i class="fa  fa-list-alt"></i></a>
															</td>
														</tr>
														<?php
														$runNo++;
													} 	
												}
												?>
												<?php 
													$sWhere = ($dateStart && $dateStop) ? " and b.date>='$dateStart' and b.date<='$dateStop'" : "";
													if($_POST["check_inh"]=="pub") $sWhere .= " and b.inhouse='F'";
													if($_POST["check_inh"]=="inh") $sWhere .= " and b.inhouse='T'";													
													$con = "and a.course_id={$course_id} $sWhere";
												    $r = get_course_detail_list($con);

													if($r){
														$runNo = 1; 
														foreach($r as $k=>$v){
																$course_detail_id = $v["course_detail_id"];
																$use = 0;
																//$use = get_use_chair($course_detail_id);
																$chair_all = $v['chair_all'];
																$book = $v["book"];
																$usebook = 0;
																if($book>0) {
																	$usebook += $v["book_all"];
																	//$usebook += get_use_book($course_detail_id);
																}
																$sit_all = $chair_all - $book - $use;
																$status = get_course_status_name($sit_all, $v["status"], $v["coursetype_id"]);
																if($v["date"]<date("Y-m-d")){
																	$status = "ปิดรับสมัคร";
																}															
															?>
															<tr style="cursor:pointer;">
																<td class="center"><?php echo $runNo; ?></td>
																<td class="center"><strong style="font-size:15px;"><?php echo $v["day"]; ?></strong> <?php echo revert_date($v["date"]); ?></td>
																<td class="center"><?php echo $v["time"]; ?></td>
																<td class="center"><?php echo $v["address_detail"]; ?></td>
																<td class="center"><?php echo $v["address"]; ?></td>													
																<td class="center"><?php echo $sit_all; ?> (<?php echo (int) $book - $usebook; ?>) </td>													
																<td class="center"><?php echo $chair_all-$book ," (", $book; ?>)</td>														
																<td class="center"><?php echo $status; ?></td>													
																<td class="center"><?php echo revert_date($v["open_regis"]); ?></td>
																<td class="center"><?php echo revert_date($v["end_regis"]); ?></td>
																<td class="center"><?php echo ($v["inhouse"]=="T") ? '<i class="fa fa-check-square-o"></i>' : ''; ?></td>
																<td class="center">
																	<a class="btn btn-small " data-toggle="tooltip" data-original-title="สมัครแบบด่วน (ครั้งละคน)" href="#" onclick="add_to_walkin(<?php echo $v["course_detail_id"]; ?>);"><i class="fa fa-user"></i></a>
																	<a class="btn btn-small " data-toggle="tooltip" data-original-title="สมัครแบบเป็นกลุ่ม (Import)" href="#" onclick="add_to_import(<?php echo $v["course_detail_id"]; ?>);"><i class="fa  fa-list-alt"></i></a>
																</td>
															</tr>
															<?php
															$runNo++;
														} 	
													}
												 ?>
												<?php 
													$sec_action = str_course_type($info["coursetype_id"]);
													$coursetype_id = $info["coursetype_id"];
													$section_id = $info["section_id"];
													$x = get_select_course($section_id, $sec_action);
													
													$sWhere = ($dateStart && $dateStop) ? " and a.date>='$dateStart' and a.date<='$dateStop'" : "";
													if($_POST["check_inh"]=="pub") $sWhere .= " and a.inhouse='F'";
													if($_POST["check_inh"]=="inh") $sWhere .= " and a.inhouse='T'";													
													$con = " and a.active='T' and a.coursetype_id={$coursetype_id} and a.section_id={$section_id} $sWhere";
												    $r = get_course_detail($con);
													if($r && $x==4){
														//$runNo = 1; 
														foreach($r as $k=>$v){
																$course_detail_id = $v["course_detail_id"];
																$use = 0;
																//$use = get_use_chair($course_detail_id);
																$chair_all = $v['chair_all'];
																$book = $v["book"];
																$usebook = 0;
																if($book>0) {
																	$usebook += $v["book_all"];
																	//$usebook += get_use_book($course_detail_id);
																}
																$sit_all = $chair_all - $book - $use;
																$status = get_course_status_name($sit_all, $v["status"], $v["coursetype_id"]);
																if($v["date"]<date("Y-m-d")){
																	$status = "ปิดรับสมัคร";
																}															
															?>
															<tr style="cursor:pointer;">
																<td class="center"><?php echo $runNo; ?></td>
																<td class="center"><strong style="font-size:15px;"><?php echo $v["day"]."."; ?></strong> <?php echo revert_date($v["date"]); ?></td>
																<td class="center"><?php echo $v["time"]; ?></td>
																<td class="center"><?php echo $v["address_detail"]; ?></td>
																<td class="center"><?php echo $v["address"]; ?></td>
																<td class="center"><?php echo $sit_all; ?> (<?php echo (int) $book - $usebook; ?>) </td>													
																<td class="center"><?php echo $chair_all-$book ," (", $book; ?>)</td>														
																<td class="center"><?php echo $status; ?></td>													
																<td class="center"><?php echo revert_date($v["open_regis"]); ?></td>
																<td class="center"><?php echo revert_date($v["end_regis"]); ?></td>
																<td class="center"><?php echo ($v["inhouse"]=="T") ? '<i class="fa fa-check-square-o"></i>' : ''; ?></td>
																<td class="center">
																	<a class="btn btn-small " data-toggle="tooltip" data-original-title="สมัครแบบด่วน (ครั้งละคน)" href="#" onclick="add_to_walkin(<?php echo $v["course_detail_id"]; ?>);"><i class="fa fa-user"></i></a>
																	<a class="btn btn-small " data-toggle="tooltip" data-original-title="สมัครแบบเป็นกลุ่ม (Import)" href="#" onclick="add_to_import(<?php echo $v["course_detail_id"]; ?>);"><i class="fa  fa-list-alt"></i></a>
																</td>
															</tr>
															<?php
															$runNo++;
														} 	
													}
												 ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="clear"></div>
							<div class="form-group row" style="padding-left:10px;">
								<div class="col-sm-12">
									<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p'] ?>');">กลับสู่หน้าหลัก</button>
								</div>
							</div>
						</form>
					</div>
				</div>

			</div>
		</div>

	</div>
</div> 
</div>
<?php include_once ('inc/js-script.php'); ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$("#filter_date").click(function(event) {
			/* Act on the event */
			$("#frmMain").submit();
		});
	    $("#date_start").datepicker({language:'th-th',format:'dd-mm-yyyy'});
	    $("#date_stop").datepicker({language:'th-th',format:'dd-mm-yyyy'});		
	});
	function add_to_walkin(id){
	    var url = "index.php?p=<?php echo $_GET["p"];?>&course_detail_id="+id+"&type=walk-in&course_id=<?php echo $course_id; ?>";
	   redirect(url);
	}
	function add_to_import(id){
	    var url = "index.php?p=<?php echo $_GET["p"];?>&course_detail_id="+id+"&type=register-import&course_id=<?php echo $course_id; ?>";
	   redirect(url);
	}

</script>
<style>
	#set_time-error{
		display: none !important;
	}
</style>