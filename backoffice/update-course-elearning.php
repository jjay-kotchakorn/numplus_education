<?php
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";
global $db;


$type = $_POST["type"];
$course_id = $_POST["current_course_id"];
$course_detail_id = $_POST["course_detail_id"];
$arr = array();
if($type=="course_detail"){
	$args = array();
	$args["table"] = "elerning_map_course_detail";
	$q = "select elerning_map_course_detail_id from elerning_map_course_detail where course_id={$course_id} and course_detail_id={$course_detail_id}";
	$ids = $db->data($q);
	if($ids){
		$args["id"] = $ids;
	}else{
		$args["course_id"] = $_POST["current_course_id"];
		$args["course_detail_id"] = $_POST["course_detail_id"];
	}	
	$args["transfer_status"] = ($_POST["transfer_status"]) ? $_POST["transfer_status"] : "F";
	$args["expire_status"] = $_POST["expire_status"];
	$exp = "";
	if($args["expire_status"]==1){
		$args["expire_date"] = thai_to_timestamp($_POST["expire_date"]);
		$exp = $_POST["expire_date"];
	}else{
		$args["expire_date"] = "00-00-0000 00:00:00";
	}
	if($args["expire_status"]==2){
		$args["num_expire_day"] = $_POST["num_expire_day"] - 0;
		$exp = "<a title='นับจากวันที่ชำระเงิน / นับจากวันที่วางบิล / ยกเว้นค่าธรรมเนียม'>".$_POST["num_expire_day"]." วัน</a>";
	}else{
		$args["num_expire_day"] = "" - 0;
	}
	if($args["expire_status"]==3){
		$exp = " ไม่มีวันหมดอายุ";
	}

	$args["remark"] = $db->escape($exp);
	$args["recby_id"] = (int)$EMPID;
	$args["rectime"] = date("Y-m-d H:i:s");
	$ret = $db->set($args);
	$arr = array('transfer_status'=> $args["transfer_status"], 'exp'=> $exp);

}else if($type=="course" && $_POST["course_detail_id"]){
	$msg = '0';
	if(is_array($_POST["name"])){
		foreach($_POST["name"] as $index=>$v){
			$args = array();
			$args["table"] = "elerning_map_course";
			$code = $_POST["code"][$index];
			$q = "select elerning_map_course_id from elerning_map_course where course_id={$_POST["course_idx"]} and code={$code} and course_detail_id={$_POST["course_detail_id"]}";
			$ids = $db->data($q);
			if($ids){
				$args["id"] = $ids;
			}else{
				$args["course_id"] = $_POST["course_idx"];
				$args["course_detail_id"] = $_POST["course_detail_id"];
				$args["code"] = $code;
				if($v=="") continue;
			}		
			$args["name"] = $v;			
			$db->set($args);
			$msg = '1';
		}	
		$arr = array("msg"=>$msg);
	}
}
echo json_encode($arr);
?>