<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
global $db, $SECTIONID, $COURSETYPEID;

$apay = datatype(" and a.active='T'", "pay_status", true);
$con_section_id = ($SECTIONID>0) ? " and a.section_id={$SECTIONID}" : "";
$con_coursetype_id = ($COURSETYPEID>0) ? " and a.coursetype_id={$COURSETYPEID}" : "";
$section = datatype(" and a.active='T' {$con_section_id}", "section", true);
if($con_coursetype_id)
	$coursetype = datatype(" and a.active='T' {$con_coursetype_id}", "coursetype", true);

$educationlevel = datatype(" and a.active='T'", "educationlevel", true);
$province = datatype(" and a.active='T'", "province", true);
$receipttype = datatype(" and a.active='T'", "receipttype", true);
$university = datatype_university(true);
?>
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="col-sm-12">
				<div class="content block-flat ">
					<h3><i class="fa fa-list"></i> &nbsp;ประวัติการรับส่งข้อมูลระหว่างระบบ WR และระบบ E-Learning</h3>
					<div class="page-head">
						<div class="form-group row">
							<label class="col-sm-2 control-label">ประเภทใบอนุญาต/คุณวุฒิ</label>
							<div class="col-sm-3">
								<select name="section_id" id="section_id" class="form-control" onchange="reCall();">
									<option value="">---- เลือก ----</option>
									<?php foreach ($section as $key => $value) {
										$id = $value['section_id'];
										if($SECTIONID>0 && $SECTIONID!=$id) continue;
										if($SECTIONID>0 && $SECTIONID==$id){
											$select = "selected";
										}else{
											$select = "";
										}
										$name = $value['name'];
										echo  "<option {$select} value='$id'>$name</option>";
									} ?>

								</select>
							</div>                
							<label class="col-sm-2 control-label">ประเภทหลักสูตร</label>
							<div class="col-sm-2">
								<select name="coursetype_id" id="coursetype_id" class="form-control" onchange="reCall();">
									<option value="">---- เลือก ----</option>
									<?php foreach ($coursetype as $key => $value) {
										$id = $value['coursetype_id'];
										if($COURSETYPEID>0 && $COURSETYPEID==$id){
											$select = "selected";
										}else{
											$select = "";
										}
										$name = $value['name'];
										echo  "<option {$select} value='$id'>$name</option>";
									} ?>

								</select>
							</div>
<!-- 							<div class="col-sm-1">
								<label class="col-sm-1 control-label">
									<a onclick="clearSearch();" id="ok" class="btn btn-warning" 
										type="button"><i class="fa fa fa-exchange">&nbsp;</i>&nbsp;Clear</a>
								</label> 
							</div> -->
						</div>

						<!-- <div class="form-group row">
							<label class="col-sm-1 control-label">เวลา/สถานที่</label>
							<div class="col-sm-5">
								<div class="input-group">
									<input class="form-control" type="text" id="course_detail_name" readonly="true">
									<input name="course_detail_id" type="hidden" id="course_detail_id" value="" />
									<span class="input-group-btn">
										<button class="btn btn-primary" type="button" onclick="addcourse_detail_other();">...</button>
									</span>
								</div>				
							</div> 
							<label class="col-sm-1 control-label">ชื่อ-นามสกุล</label>
							<div class="col-sm-3">
								<div class="input-group">
									<input type="text" class="form-control" id="membername" name="membername" readonly="true" value="">
									<input name="member_id" type="hidden" id="member_id" value="" />
									<span class="input-group-btn">
										<button class="btn btn-primary" type="button" onClick="selectMember();">...</button>
									</span>
								</div>
							</div>
							<div class="col-sm-1">
								<label class="col-sm-1 control-label">
									<a onclick="clearSearch();" id="ok" class="btn btn-warning" 
										type="button"><i class="fa fa fa-exchange">&nbsp;</i>&nbsp;Clear</a>
								</label> 
							</div>
						</div>
                        -->


						
						<div class="form-group row">	
							<label class="col-sm-1 control-label"  style=" padding-right:0px;">วันที่มีการส่งข้อมูล</label>
							<div class="col-sm-1"  style="padding-left:0px; padding-right:0px;">
								<input class="form-control" name="date_start" value="<?php echo revert_date(date("Y-m-d")); ?>" id="date_start" onblur="reCall();" placeholder="วันที่เริ่ม" type="text">
							</div>                                          
							<label class="col-sm-1 control-label"  style=" padding-right:0px;">วันที่สิ้นสุด</label>
							<div class="col-sm-1"  style="padding-left:0px; padding-right:0px;">
								<input class="form-control" name="date_stop" id="date_stop" onblur="reCall();" placeholder="วันที่สิ้นสุด" type="text">
							</div> 
 							
							<label class="col-sm-1 control-label"  style=" padding-right:0px;"></label>
							<label class="col-sm-1 control-label"  style=" padding-right:0px;">วันที่มีการรับข้อมูล</label>
							<div class="col-sm-1"  style="padding-left:0px; padding-right:0px;">
								<input class="form-control" name="date_start_result" value="" id="date_start_result" onblur="reCall();" placeholder="วันที่เริ่ม" type="text">
							</div>  
							<label class="col-sm-1 control-label"  style=" padding-right:0px;">วันที่สิ้นสุด</label>
							<div class="col-sm-1"  style="padding-left:0px; padding-right:0px;">
								<input class="form-control" name="date_stop_result" id="date_stop_result" onblur="reCall();" placeholder="วันที่สิ้นสุด" type="text">
							</div>


							<label class="col-sm-2 control-label">
								<a href="#" class="btn" onclick="reCall();"><i class="fa fa-search">&nbsp;</i></a>&nbsp;&nbsp;
								<!-- <button onclick="clearSearch();" id="ok" class="btn btn-success" type="button">Clear</button> -->
							</label>                                            
						</div>

						<div class="form-group row">
							<label class="col-sm-1 control-label" style=" padding-right:0px;">สถานะการส่งข้อมูล</label>
							<div class="col-sm-2">
								<select name="send_status" id="send_status" class="form-control" onchange="reCall();">
									<option selected="selected" value="all">แสดงทั้งหมด</option>
									<option value="T">สำเร็จ</option>
									<option value="F">ไม่สำเร็จ</option>
								</select>
							</div> 
							<label class="col-sm-1 control-label" style=" padding-right:0px;">สถานะการรับข้อมูล</label>
							<div class="col-sm-2">
								<select name="receive_status" id="receive_status" class="form-control" onchange="reCall();">
									<option selected="selected" value="all">แสดงทั้งหมด</option>
									<option value="T">สำเร็จ</option>
									<option value="F">รอรับข้อมูล</option>
								</select>
							</div> 
							<label class="col-sm-1 control-label" style=" padding-right:0px;">การแสดงผล</label>
							<div class="col-sm-2">
								<select name="order_by_date" id="order_by_date" class="form-control" onchange="reCall();">
									<option selected="selected" value="all">แสดงทั้งหมด</option>
									<option value="send_date">วันที่ WR ส่งข้อมูล</option>
									<option value="receive_date">วันที่ EL ส่งข้อมูลกลับมา</option>
								</select>
							</div> 
						</div>

					</div>
					<table id="tbEmp" class="table" style="width:100%">
						  <thead>
							  <tr>
								  <th width="4%">ลำดับ <input type="checkbox" id="checkAll" name="checkAll" style="float:right;"></th>							
								  <th width="2%">คำนำหน้า</th>
								  <th width="8%">ชื่อ</th>
								  <th width="8%">นามสกุล</th>
								  <th width="5%">รหัสประจำตัวประชาชน</th>
								  
								  <th width="3%">member ID</th>
								  <th width="3%">register ID</th>
								  <th width="3%">Course ID (WR)</th>
								  <th width="7%">Course code (WR)</th>
								  <th width="3%">Course detail ID (WR)</th>
								  
								  <th width="7%">Course code(E-learning)</th>
								  <th width="5%">วันหมดอายุ</th>
								  <th width="5%">วันที่ WR ส่งข้อมูล</th>
								  <th width="5%">วันที่ EL ส่งข้อมูลกลับมา</th>
								  <th width="7%">ผลกิจกรรม (สอบ)</th>
								  <th width="7%">ผลกิจกรรม (อบรม)</th>
								  
								  <th width="5%">วันที่บันทึกผลกิจกรรม (สอบ)</th>
								  <th width="5%">วันที่บันทึกผลกิจกรรม (อบรม)</th>
								  <th width="7%">สถานะการส่งข้อมูล</th>
								  <th width="7%">สถานะการรับข้อมูล</th>
								  <th width="5%">หมายเหตุ</th>
								  <!-- <th width="5%">ขอผลกิจกรรม</th> -->
							  </tr>
						  </thead>   
						<tbody>
						</tbody>
					</table>
					<br><br>



					<div class="form-group row" style="padding-left:10px;">
						<div class="col-sm-12">

							<button type="button" class="btn btn-success pull-left" onClick="send_to_elearning()">ส่งค่าไป elearning</button>										
						</div>
					</div>
	



				</div>
			</div>

		</div>
	</div> 
</div>
<?php include ('inc/js-script.php') ?>

<script type="text/javascript">
$(document).ready(function() {
	var oTable;
	$("#frmMain").validate();
	listItem();
	$("#date_start").datepicker({language:'th-th',format:'dd-mm-yyyy'});
    $("#date_stop").datepicker({language:'th-th',format:'dd-mm-yyyy'});
    $("#date_start_result").datepicker({language:'th-th',format:'dd-mm-yyyy'});
    $("#date_stop_result").datepicker({language:'th-th',format:'dd-mm-yyyy'});
    $("#section_id").change(function(event) {
		var id = $(this).val();
		getDropDown('#coursetype_id', id, 'coursetype', 'data/coursetype.php')
	});

    $( "#checkAll" ).hide();
    $('#send_status').change(function() {
	    // alert($('#send_status').val());
	    if ( $("#send_status").val()=='F' ) {
	    	$( "#checkAll" ).show();
	    }else{
	    	$( "#checkAll" ).hide();
	    }//end else

	});


});

$("#checkAll").click(function(){
		$("#tbEmp tbody td :checkbox").prop('checked', $(this).prop("checked"));
});

function listItem(){
   var url = "data/elernning-register-log-list.php";
   oTable = $("#tbEmp").dataTable({
	   "sDom": 'T<"clear">lfrtip',
	   "oLanguage": {
   	   "sInfoEmpty": "",
   		"sInfoFiltered": ""
						  },
		"oTableTools": {
			"aButtons":  ""
		},
		"bSort": false ,
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": url,
		"sPaginationType": "full_numbers",
		"aaSorting": [[ 0, "desc" ]],
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			aoData.push({"name":"date_start","value":$("#date_start").val()});			
			aoData.push({"name":"date_stop","value":$("#date_stop").val()});
			aoData.push({"name":"date_start_result","value":$("#date_start_result").val()});			
			aoData.push({"name":"section_id","value":$("#section_id").val()});	
			aoData.push({"name":"coursetype_id","value":$("#coursetype_id").val()});	
					
			aoData.push({"name":"date_stop_result","value":$("#date_stop_result").val()});
			aoData.push({"name":"send_status","value":$("#send_status").val()});
			aoData.push({"name":"receive_status","value":$("#receive_status").val()});
			aoData.push({"name":"order_by_date","value":$("#order_by_date").val()});
			aoData.push({"name":"member_id","value":$("#member_id").val()});			
			aoData.push({"name":"course_detail_id","value":$("#course_detail_id").val()});
			$.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});
		}
   }); 
}

function editInfo(id){
	if(typeof id=="undefined") return;
   var url = "index.php?p=<?php echo $_GET["p"];?>&emp_id="+id+"&type=info";
   redirect(url);
}
function addnew(){
   var url = "index.php?p=<?php echo $_GET["p"];?>&type=info";
   redirect(url);
}

function reCall(){
	oTable.fnClearTable( 0 );
	oTable.fnDraw();
}

function clearSearch(){
 $("#course_detail_name").val("");
 $("#course_detail_id").val("");
 $("#member_id").val("");
 $("#membername").val("");
 $("#section_id").val("");
 $("#coursetype_id").val("");
 $("#date_start").val("");
 $("#date_stop").val("");
 $("#date_start_result").val("");
 $("#date_stop_result").val("");
 $("#receive_status").val("all");
 $("#send_status").val("all");
 $("#order_by_date").val("all");


 reCall();
}


function selectMember(){
   var url = "member map";
   memberpop(url,"ret_member_select");	
}

function ret_member_select(id){
	for(var i in id){
		var ck = "";
		var data = id[i];
        $("#member_id").val(data["member_id"]);
        $("#membername").val(data.name_th);
        reCall();
	}
}

function addcourse_detail_other(){
   var url = "course_detail map";
   courseDetailData(url,"ret_detail_other");	
}

function ret_detail_other(id){
	for(var i in id){
		var ck = "";
		var data = id[i];
		$("#course_detail_id").val(data.course_detail_id);
		console.log(id);
		var str = data.date+" เวลา :  "+data.time+" สถานที่ : "+data.address_detail+" "+data.address+ " จำนวนที่นั่ง : "+data.chair_all;
		$("#course_detail_name").val(str);
		reCall();
	}
}

function get_result(ref_id=""){
	// console.log(ref_id);
	$.ajax({
		url: 'elerning-site-ati-ctrl.php',
		type: 'POST',
		// dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
		data: {ref_id: ref_id, type: 'get-result'},
	})

	.done(function() {
		console.log("success");
		$.gritter.removeAll({
	        after_close: function(){
	          $.gritter.add({
	          	position: 'center',
		        title: 'success',
		        text: 'ส่งคำขอผลกิจกกรรมเรียบร้อย',
		        class_name: 'success'
		      });
	        }
	    });
	    reCall();
	})
	.fail(function() {
		console.log("error");
		$.gritter.removeAll({
	        after_close: function(){
	          $.gritter.add({
	          	position: 'center',
		        title: 'Error',
		        text: 'ส่งคำขอผลกิจกกรรมไม่สำเร็จ',
		        class_name: 'danger'
		      });
	        }
	     });
	})
	.always(function() {
		console.log("complete");
	});
	
}//end func

function send_to_elearning(){
	//alert(555);
	var id = "";
	var ids = "";
    $("#tbEmp tbody tr :checkbox").each(function() {
        if ($(this).is(":checked")) {
            var id = $(this).val();
            ids += id+",";

            $.ajax({
				url: 'check-course-elearning.php',
				type: 'POST',
				async: false, 
				// dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
				data: {register_data_list_id: id, pay_status: 3, type: 'send-again'},
			})

        }
    });
    // console.log(id);
    reCall();
	if(ids==""){
		alert("ยังไม่ได้เลือกรายการ ? ");
		return;
	}else{

	}//end else

}//end func

</script>