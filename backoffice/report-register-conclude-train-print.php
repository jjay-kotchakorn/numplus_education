<?php
include_once "./share/authen.php";
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
global $db;
require_once dirname(__FILE__) . '/PHPExcel.php';
$info = get_course_detail(" and a.course_detail_id={$_GET["course_detail_id"]}");
$course_info = get_course(" and a.course_id={$_GET["course_id"]}");
if($info){
	$info = $info[0];
	$course_info = $course_info[0];
	$course_detail_id = $info["course_detail_id"];
	$course_id = $course_info["course_id"];
	$fileName ="";
	$apay = datatype(" and a.active='T'", "pay_status", true);
	$arr_pay = array();
	foreach ($apay as $key => $value) {
		$arr_pay[$value["pay_status_id"]] = $value["name"];
	}
?>

<!DOCTYPE html>
<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>รายชื่อผู้สอบ <?php echo $course_info["title"]; ?></title>
<link href="css/printform-style.css" rel="stylesheet" type="text/css" media="all" />
<style>
	@media print{
		body{
			padding: 0px;

		}
		#btPrint{
			display: none;
		}
	}
</style>
<body>

	<div class="form-landscape">
		<div class="page-header">
			<div>สรุปผลการ <?php echo $course_info["coursetype_name"]; ?> หลักสูตร<?php echo $course_info["title"]; ?></div>
			<div>วันที่ <?php echo revert_date($info["date"])."&nbsp;เวลา ".$info["time"]; ?> น.</div>
			<div><?php echo $info['address_detail']." ".$info['address'] ?></div>
		</div>
		<table class="td-center">
			<thead>
				<tr>
					<td width="160"><span class="center">เลขที่บัตร</span></td>
					<td width="60"><span class="center">คำนำหน้า</span></td>
					<td width="160"><span class="center">ชื่อ</span></td>
					<td width="160"><span class="center">นามสกุล</span></td>
					<td width="160"><span class="center">สถานะการชำระเงิน</span></td>
					<td width="160"><span class="center">จำนวนเงิน</span></td>
					<td width="160"><span class="center">วิธีการชำระเงิน</span></td>
					<td><span class="center">วันที่ชำระเงิน</span></td>
				</tr>
			</thead>
			<?php 

			$ids = array();
			$q = " select register_id from register_course_detail where active='T'  and course_detail_id=$course_detail_id";
			$get_all = $db->get($q);
			if($get_all){
				foreach ($get_all as $key => $value) {
					$ids[] = $value["register_id"];
				}
			}
			$q = " select register_id from register where active='T'  and course_detail_id='$course_detail_id'";
			$get_register_all = $db->get($q);
			if($get_register_all){
				foreach ($get_register_all as $key => $value) {
					$ids[] = $value["register_id"];
				}
			}
			$t = array_unique($ids);
			$con_ids = implode(",", $t);
			$q = "select a.title,
						 a.fname,
						 a.lname,
						 a.cid,
						 a.branch,
						 b.email1,
						 b.tel_home,
						 b.tel_mobile,
						 b.tel_office,
						 a.pay_status,
						 a.course_price,
						 a.course_discount,
						 a.pay_date,
						 a.pay
			from register a inner join member b on a.member_id=b.member_id
			where a.register_id in ($con_ids) and a.pay_status in (3,5,6) and a.active='T' 
			order by a.no asc";
			$r = $db->get($q);
			if($r){
			?>
			<tbody>
				<?php foreach ($r as $key => $value): ?>
					<?php 
						if($value["pay"]=="at_ati"){
							$pay_type = "ชำระเงินสดผ่าน ATI";
						}else if($value["pay"]=="paysbuy"){
							$pay_type = "ชำระเงินช่องทางอื่นๆ (paysbuy)";
							$print_style = "visibility:hidden";
						}else if($value["pay"]=="bill_payment"){
							$pay_type = "ชำระเงินผ่าน Bill-payment";
						}else if($value["pay"]=="at_asco"){
							$pay_type = "เช็ค/เงินโอน";
						}else if($value["pay"]=="walkin"){
							if($info["pay_status"]==5 || $info["pay_status"]==6)
								$pay_type = $arr_pay[$info["pay_status"]];
							else	
								$pay_type = "ชำระเงินสดผ่าน ATI";
						}else if($value["pay"]=="importfile"){
							if($info["pay_status"]==5 || $info["pay_status"]==6)
								$pay_type = $arr_pay[$info["pay_status"]];
							else	
								$pay_type = "ชำระเงินสดผ่าน ATI";
						}
					 ?>
					<tr>
						<td><span class="center"><?php echo $value["cid"]; ?></span></td>
						<td><span class="left"><?php echo $value["title"]; ?></span></td>
						<td><span class="left"><?php echo $value["fname"]; ?></span></td>
						<td><span class="left"><?php echo $value["lname"]; ?></span></td>
						<td><span class="center"><?php echo $arr_pay[$value["pay_status"]]; ?></span></td>
						<td><span class="right"><?php echo set_comma($value["course_price"]-$value["course_discount"]) ?></span></td>
						<td><span class="center"><?php echo $pay_type ; ?></span></td>
						<td><span class="center"><?php echo revert_date($value["pay_date"], true); ?></span></td>
					</tr>			
					<?php 
						$i++;
					endforeach ?>
			</tbody>
			<?php 
			} ?>
		</table>
	</div>

</body>
<script type="text/javascript">
function printPage(){
	   window.print();
	   setTimeout(" parent.$.fancybox.close()",1000);
	}
</script>
</html>
<?php } 
?>