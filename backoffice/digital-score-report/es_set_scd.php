<?php
//place this before any script you want to calculate time
$time_start = microtime(true);
include_once "../lib/lib.php";
include_once "../connection/connection.php";

include ("inc.my.cipher.php");
include ("ini.api.key.php");
global $db;

$datetime_now = date("Y-m-d H:i:s");

function return_time_format($number){
	//$number = "09:30:00";
	// $txt = str_replace(":", ".", $number);
	// return substr($txt, 0, 5);
	return substr($number, 0, 5);
}//end func


//Make sure that it is a POST request.
if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') != 0)
{
	header("HTTP/1.0 500");
	$r="{ \"response_code\":\"1001\" , \'response_message\":\"Request method must be POST\" }";
	throw new Exception( $r );
}
 
//Make sure that the content type of the POST request has been set to application/json
$contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : ''; //If Condition is true ? Then value X : Otherwise value Y
if(strcasecmp($contentType, 'application/json') != 0)
{
	header("HTTP/1.0 500");
	$r="{ \"response_code\":\"1002\" , \'response_message\":\"Content type must be: application/json\" }";
	throw new Exception( $r );
}
 
//Receive the RAW post data.
$content = trim(file_get_contents("php://input"));
 
$str_txt = ""; 
$str_txt .= $content."\r\n";
write_log("es_set_scd", $str_txt, "a", "./log/" );

//Attempt to decode the incoming RAW post data from JSON.
$decoded = json_decode($content, true);

// d($decoded); die();
 
//If json_decode failed, the JSON is invalid.
if(!is_array($decoded))
{
	header("HTTP/1.0 500");
	$r="{ \"response_code\":\"1003\" , \'response_message\":\"Received content contained invalid JSON\" }";
	throw new Exception( $r );
}

//Process the JSON.
$data = isset($decoded['data']) ? trim($decoded['data']) : '';
$signature = isset($decoded['signature']) ? trim($decoded['signature']) : '';

if($data=="" || $signature=="")
{
	header("HTTP/1.0 500");
	$r="{ \"result_code\" : \"3001\" , \"result_message\" : \"Request : Invalid parameter(s)\" } ";
}
else
{
	// - - - ini private key data - - -
	$es_public_xml_file=$ini_es_public;
	$es_public_xml=file_get_contents( $es_public_xml_file );
	
	// - - - AES Decrypt  - - -
	$aes1 = new myAES();
	$aes1->loadkeyFromXML( $es_public_xml , true );
	$data1=$aes1->decrypt( $data , true );
	
	$rqd=json_decode( $data1 , true );
	$sz=sizeof( $rqd );
	// - - - Json Request Parameter Name - - -
	$jsname[1]="scd";
	$jsname[2]="scddata";

	$sz_jsn=sizeof( $jsname );
	if($sz==$sz_jsn)
	{
		$jsn_err=0;
		for($i=1;$i<=$sz_jsn;$i++){ 
			if( isset($rqd[$jsname[$i]])){ 

			}else{ 
				$jsn_err=$jsn_err+1; 
			} 
		}//end loop $i
		
		if( $jsn_err==0 )
		{
			// - - - begin write scd data to file - - -
			$fn="temp/scd/".$rqd[$jsname[1]]."_".date("YmdHis").".scd";
			$f1=@fopen( $fn , 'w' );
			@fwrite( $f1 , $data1 );
			@fclose( $f1 );

			// - - - begin insert data to DB - - -
			//                   Coding
			


			$scddata = array();
			$scddata = $rqd["scddata"];
			// d($scddata); die();
			if ( count($scddata)>0 ) {
				
				foreach ($scddata as $key => $v) {			
					$str_txt = ""; 
					// d($v); die();
					if (!empty($v)) {
						$exam_data = $v;
						foreach ($v as $key => $d) {
							$str_txt .= "$key=$d|";		
						}//end loop $d
						
						$exam_id = trim($exam_data["exam_id"]); 
						$title = trim($exam_data["title"]); 
						$examtype = $exam_data["examtype"]; 
						$reg_max = $exam_data["reg_max"]; 
						$pv = (int)$exam_data["pv"]; 
						$place = trim($exam_data["place"]); 
						$set_time = $exam_data["examtype"]; 
						$time1 = $exam_data["time1"]; 
						$time2 = $exam_data["time2"]; 
						$status = $exam_data["status"]; 
						// $inhouse = $exam_data["inhouse"]; 
						$regtype = $exam_data["regtype"]; 
						$start_reg = $exam_data["start_reg"]; 
						$expire_reg = $exam_data["expire_reg"];

						$address = $title;
						


						$date = substr($exam_id, 1, 8); //12561093004
						$year = substr($date, 0, 4);
						$year_en = substr($date, 0, 4)-543;
						$month = substr($date, 4, 6);
						$month = substr($month, 0, 2);
						$day = substr($date, 6, 8);
						$date = $year."-".$month."-".$day;
						$date = thai_to_timestamp($date);
						$t = convert_mktime($date." 00:00:00");
						$date_n = call_day($day, $month, $year_en);
						$date_en = $year_en."-".$month."-".$day;

						$inhouse = !empty($exam_data["inhouse"]) ? trim($exam_data["inhouse"]) : '1';
						//switch inhouse
						switch ($inhouse) {
							case '1':
								$inhouse = 'F';
								break;
							case '2':
								$inhouse = 'T';
								break;
							default:
								# code...
								break;
						}//end sw


						$active = 'T';	
						//switch status
						switch ($status) {
							case '0':
								$status = "ปิดรับสมัครชั่วคราว";
								break;
							case '1':
								$status = "เปิดรับสมัคร";
								break;
							case '2':
								$status = "รอบสอบเต็ม";
								break;
							case '3':
								$status = "ยกเลิกรอบสอบ";
								$active = 'F';
								break;
							default:
								$status = "ยกเลิกรอบสอบ";
								$active = 'F';
								break;
						}//end sw
						
						if ( ($datetime_now < $start_reg)&&($status == "เปิดรับสมัคร") ) {
							$status = "ปิดรับสมัครชั่วคราว";
						}//end if

						$q = "SELECT course_detail_id 
							FROM course_detail 
							WHERE 1
								-- AND active='T' 
								AND ( ref_exam_id is not null OR ref_exam_id <> '' )
								AND ref_exam_id LIKE '{$exam_id}'
						";
						$course_detail_id = $db->data($q);

						
						$args = array();
						$args["table"] = "course_detail";
						if( !empty($course_detail_id) ){
							$mode = 'update';//
							$args["id"] = (int)$course_detail_id;			
						}else{
							$mode = 'add';//
							$args["ref_exam_id"] = $exam_id;			
							$args["datetime_add"] = $datetime_now;
						}
						$args["date"] = $year_en."-".$month."-".$day;
						$args["time"] = return_time_format($time1)."-".return_time_format($time2);
						// $args["time"] = $time1."-".$time2;

						$time = explode("-", $args["time"]);
						$time_phase1 = trim($time[0]);
						$time_phase2 = trim($time[1]);
						$date_phase1 = "{$args["date"]} {$time_phase1}:00";
						$date_phase2 = "{$args["date"]} {$time_phase2}:00";
						$args["date_phase1"] = $date_phase1;
						$args["date_phase2"] = $date_phase2;

						$args["code_project"] = "";
						//$args["day"] = $thai_day[$date_n];
						$args["day"] = $date_n;
						$args["address"] = trim($address);
						$args["coursetype_id"] = 2;
						$args["section_id"] = 1;
						// $args["address"] = iconv('TIS-620', 'UTF-8', $address);
						// $args["address_detail"] = iconv('TIS-620', 'UTF-8', $place);
						$args["address_detail"] = $place;			
						$args["chair_all"] = (int)$reg_max;
						$args["open_regis"] = $start_reg;
						$args["end_regis"] = $expire_reg;
						//$args["status"] = "เปิดรับสมัคร";
						$args["status"] = $status;
						//$args["date_other"] = $_POST["date_other"];
						$args["active"] = $active;
						$args["inhouse"] = $inhouse;
						// $args["recby_id"] = (int)$EMPID;
						$args["rectime"] = date("Y-m-d H:i:s");
						$args["datetime_update"] = $datetime_now;

						//check data before insert
						$tmp_txt = "";
						foreach ($args as $key => $a) {
							$tmp_txt .= "$key=$a|";		
						}//end loop $d
						$tmp_txt .= "\n";
						write_log("es_set_scd-insert", $tmp_txt, "a", "./log/" );

						$rs = $db->set($args);

						$course_detail_id = !empty($rs) ? $rs : $course_detail_id;
						// d($args); die();
						$str_txt = "course_detail_id={$course_detail_id}|".$str_txt;
					}//end if
					$str_txt .= "\n";
					write_log("es_set_scd", $str_txt, "a", "./log/" );
				}//end loop $v
			}else{

			}//end else


			// - - - end insert data to DB - - -




			$r="{ \"result_code\" : \"2001\" , \"result_message\" : \"Success\" } ";
		}
		else
		{
			header("HTTP/1.0 500");
			$r="{ \"result_code\" : \"3003\" , \"result_message\" : \"Request : Invalid index name\" } ";
		}
	}
	else
	{
		header("HTTP/1.0 500");
		$r="{ \"result_code\" : \"3002\" , \"result_message\" : \"Request : Invalid index size : [".$sz." / ".$sz_jsn."'] ";
	}
}

// Display Script End time
$time_end = microtime(true);
/*
	//dividing with 60 will give the execution time in minutes other wise seconds
	$execution_time = ($time_end - $time_start)/60;
	//execution time of the script
	echo '<b>Total Execution Time:</b> '.$execution_time.' Mins';
*/
$execution_time = ($time_end - $time_start);
//execution time of the script
// echo 'Total Execution Time: '.$execution_time.' Sec';

echo $r;
?>