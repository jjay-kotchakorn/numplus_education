<?php
	include ("inc.my.cipher.php");
	include ("ini.api.key.php");

	//Make sure that it is a POST request.
	if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') != 0)
	{
		header("HTTP/1.0 500");
		$r="{ \"response_code\":\"1001\" , \'response_message\":\"Request method must be POST\" }";
		throw new Exception( $r );
	}
	 
	//Make sure that the content type of the POST request has been set to application/json
	$contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : ''; //If Condition is true ? Then value X : Otherwise value Y
	if(strcasecmp($contentType, 'application/json') != 0)
	{
		header("HTTP/1.0 500");
		$r="{ \"response_code\":\"1002\" , \'response_message\":\"Content type must be: application/json\" }";
		throw new Exception( $r );
	}
	 
	//Receive the RAW post data.
	$content = trim(file_get_contents("php://input"));
	 
	//Attempt to decode the incoming RAW post data from JSON.
	$decoded = json_decode($content, true);
	 
	//If json_decode failed, the JSON is invalid.
	if(!is_array($decoded))
	{
		header("HTTP/1.0 500");
		$r="{ \"response_code\":\"1003\" , \'response_message\":\"Received content contained invalid JSON\" }";
		throw new Exception( $r );
	}

	//Process the JSON.
	$data = isset($decoded['data']) ? trim($decoded['data']) : '';
	$signature = isset($decoded['signature']) ? trim($decoded['signature']) : '';

	if($data=="" || $signature=="")
	{ 
		$r="{ \"result_code\" : \"3001\" , \"result_message\" : \"Request : Invalid parameter(s)\" } ";
	}
	else
	{
		// - - - ini private key data - - -
		$es_public_xml_file=$ini_es_public;
		$es_public_xml=file_get_contents( $es_public_xml_file );
		
		// - - - AES Decrypt  - - -
		$aes1 = new myAES();
		$aes1->loadkeyFromXML( $es_public_xml , true );
		$data1=$aes1->decrypt( $data , true );
		
		$rqd=json_decode($data1 , true);
		$sz=sizeof($rqd);
		// - - - Json Request Parameter Name - - -
		$jsname[1]="exam_id";

		$sz_jsn=sizeof($jsname);
		if($sz==$sz_jsn)
		{
			$jsn_err=0;
			for($i=1;$i<=$sz_jsn;$i++){ if( isset($rqd[$jsname[$i]])){ }else{ $jsn_err=$jsn_err+1; } }

			if( $jsn_err==0 )
			{
				// - - - begin select reg datat from data - - -

				// - - - create response data - - -
				// - - - begin sample reg datat - - -
				$rdata="{";
				$rdata=$rdata." \"exam_id\" : \"22561060302\" , ";
				$rdata=$rdata." \"reg_data\" :  [ ";
				
				$rdata=$rdata."{";
				$rdata=$rdata."\"reg_no\" : \"1\",";
				$rdata=$rdata."\"reg_id\" : \"1\",";
				$rdata=$rdata."\"reg_title\" : \"".iconv("tis-620", "utf-8","���")."\",";
				$rdata=$rdata."\"reg_fname\" : \"".iconv("tis-620", "utf-8","������")."\",";
				$rdata=$rdata."\"reg_lname\" : \"".iconv("tis-620", "utf-8","AAAAA")."\",";
				$rdata=$rdata."\"reg_cid\" : \"1000000000001\",";
				$rdata=$rdata."\"reg_date\" : \"2018-05-01 12:50:00\",";
				$rdata=$rdata."\"reg_by\" : \"1\",";
				$rdata=$rdata."\"reg_status\" : \"1\",";
				$rdata=$rdata."\"reg_exam_date\" : \"2018-05-05\",";
				$rdata=$rdata."\"reg_course_id\" : \"99\",";
				$rdata=$rdata."\"reg_course_code\" : \"P1\" ";
				$rdata=$rdata."}";

				$rdata=$rdata." , ";

				$rdata=$rdata."{";
				$rdata=$rdata."\"reg_no\" : \"2\",";
				$rdata=$rdata."\"reg_id\" : \"2\",";
				$rdata=$rdata."\"reg_title\" : \"".iconv("tis-620", "utf-8","���")."\",";
				$rdata=$rdata."\"reg_fname\" : \"".iconv("tis-620", "utf-8","������")."\",";
				$rdata=$rdata."\"reg_lname\" : \"".iconv("tis-620", "utf-8","BBBBB")."\",";
				$rdata=$rdata."\"reg_cid\" : \"1000000000002\",";
				$rdata=$rdata."\"reg_date\" : \"2018-05-01 12:50:00\",";
				$rdata=$rdata."\"reg_by\" : \"1\",";
				$rdata=$rdata."\"reg_status\" : \"1\",";
				$rdata=$rdata."\"reg_exam_date\" : \"2018-05-05\",";
				$rdata=$rdata."\"reg_course_id\" : \"99\",";
				$rdata=$rdata."\"reg_course_code\" : \"P1\" ";
				$rdata=$rdata."}";

				$rdata=$rdata."] ";

				$rdata=$rdata."} ";
				// - - - end sample reg datat - - -
				
				// - - - begin crypt response data - - -
				// - - - ini private key data - - -
				$wr_private_xml_file=$ini_wr_private;
				$wr_private_xml=file_get_contents( $wr_private_xml_file );
				
				$r_data="";
				$r_signature="";

				$aes2 = new myAES();
				$aes2->loadkeyFromXML( $wr_private_xml , true );
				$r_data=$aes2->encrypt( $rdata , true );
				
				$r_signature=$ini_sig;
				// - - - end crypt response data - - -

				$res_data="{ \"data\" : \"".$r_data."\" , \"signature\" : \"".$r_signature."\" }";
				$r="{ \"result_code\" : \"2001\" , \"result_message\" : ".$res_data." } ";

				// - - - end select reg datat from data - - -
			}
			else
			{
				header("HTTP/1.0 500");
				$r="{ \"result_code\" : \"3003\" , \"result_message\" : \"Request : Invalid index name\" } ";
			}
		}
		else
		{
			header("HTTP/1.0 500");
			$r="{ \"result_code\" : \"3002\" , \"result_message\" : \"Request : Invalid index size : [".$sz." / ".$sz_jsn."']\" } ";
		}
	}
	echo $r;
?>