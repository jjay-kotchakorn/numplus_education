<?php
//place this before any script you want to calculate time
$time_start = microtime(true);
include_once "../lib/lib.php";
include_once "../connection/connection.php";

include ("inc.my.cipher.php");
include ("ini.api.key.php");
global $db;

//Make sure that it is a POST request.
if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') != 0)
{
	header("HTTP/1.0 500");
	$r="{ \"response_code\":\"1001\" , \'response_message\":\"Request method must be POST\" }";
	throw new Exception( $r );
}
 
//Make sure that the content type of the POST request has been set to application/json
$contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : ''; //If Condition is true ? Then value X : Otherwise value Y
if(strcasecmp($contentType, 'application/json') != 0)
{
	header("HTTP/1.0 500");
	$r="{ \"response_code\":\"1002\" , \'response_message\":\"Content type must be: application/json\" }";
	throw new Exception( $r );
}
 
//Receive the RAW post data.
$content = trim(file_get_contents("php://input"));
 
$str_txt = ""; 
$str_txt .= $content."\r\n";
write_log("es_set_scd_res", $str_txt, "a", "./log/" );


//Attempt to decode the incoming RAW post data from JSON.
$decoded = json_decode($content, true);

 
//If json_decode failed, the JSON is invalid.
if(!is_array($decoded))
{
	header("HTTP/1.0 500");
	$r="{ \"response_code\":\"1003\" , \'response_message\":\"Received content contained invalid JSON\" }";
	throw new Exception( $r );
}else{
	unset($content);
}//end else

//Process the JSON.
$data = isset($decoded['data']) ? trim($decoded['data']) : '';
$signature = isset($decoded['signature']) ? trim($decoded['signature']) : '';

if($data=="" || $signature=="")
{ 
	header("HTTP/1.0 500");
	$r="{ \"result_code\" : \"3001\" , \"result_message\" : \"Request : Invalid parameter(s)\" } ";
}
else
{
	// - - - ini private key data - - -
	$es_public_xml_file=$ini_es_public;
	$es_public_xml=file_get_contents( $es_public_xml_file );
	
	// - - - Chk Signature Method - - -
	// - - - AES Decrypt  - - -
	$aes1 = new myAES();
	$aes1->loadkeyFromXML( $es_public_xml , true );
	$data1=$aes1->decrypt( $data , true );
	
	$rqd=json_decode( $data1 , true );
	$sz=sizeof( $rqd );
	// - - - Json Request Parameter Name - - -
	$jsname[1]="exam_id";
	$jsname[2]="reg_cert_data";
	$jsname[3]="reg_data";

	// d($rqd); 
	// die();

	$sz_jsn=sizeof( $jsname );
	if($sz==$sz_jsn)
	{
		$jsn_err=0;
		for($i=1;$i<=$sz_jsn;$i++){ 
			if( isset($rqd[$jsname[$i]]) ){ 

			}else{ 
				$jsn_err=$jsn_err+1; 
			} 
		}//end loop $i
		
		if( $jsn_err==0 )
		{
			$fn="temp/res/".$rqd[$jsname[1]]."_".date("YmdHis").".res";
			$f1=fopen( $fn , 'w' );
			fwrite( $f1 , $data1 );
			fclose( $f1 );

			/*$fzip="temp/res/".$rqd[$jsname[1]]."_".date("YmdHis").".zip";
			$f1=fopen( $fzip , 'w' );
			fwrite( $f1 , base64_decode( $rqd[$jsname[2]]) );
			fclose( $f1 );*/

			$cert_file=$ini_es_url.$rqd[$jsname[2]];
			$cert_data=file_get_contents( $cert_file );
			$fzip="temp/res/".$rqd[$jsname[1]]."_".date("YmdHis").".zip";
			$f1=@fopen( $fzip , 'w' );
			@fwrite( $f1 , $cert_data );
			@fclose( $f1 );




			// - - - begin insert data to DB - - -

			$reg_data = $rqd[$jsname[3]];
			// d($reg_data); die();

			// - - - end insert data to DB - - -




			$r="{ \"result_code\" : \"2001\" , \"result_message\" : \"Success\" } ";
		}
		else
		{
			header("HTTP/1.0 500");
			$r="{ \"result_code\" : \"3003\" , \"result_message\" : \"Request : Invalid index name\" } ";
		}
	}
	else
	{
		header("HTTP/1.0 500");
		$r="{ \"result_code\" : \"3002\" , \"result_message\" : \"Request : Invalid index size : [".$sz." / ".$sz_jsn."'] ";
	}
}

// Display Script End time
$time_end = microtime(true);
/*
	//dividing with 60 will give the execution time in minutes other wise seconds
	$execution_time = ($time_end - $time_start)/60;
	//execution time of the script
	echo '<b>Total Execution Time:</b> '.$execution_time.' Mins';
*/
$execution_time = ($time_end - $time_start);
//execution time of the script
// echo 'Total Execution Time: '.$execution_time.' Sec';

echo $r;
?>