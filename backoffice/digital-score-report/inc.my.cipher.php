<?php
	// - - - begin inc phpseclib - - -
	set_include_path(get_include_path() . PATH_SEPARATOR . 'phpseclib');
	include('Crypt/AES.php');
	include('Crypt/RSA.php');
	include('Crypt/Random.php');
	include('Math/BigInteger.php');
	// - - - end inc phpseclib - - -

	// - - - begin class myB64 - - -
	class myB64
	{
		public static function encode( $str , $url_enc=true )
		{
			// +/= must convert to -_. (URL Post)
			$str=base64_encode( $str );
			//if($url_enc==true){ $str=str_replace( array('+','/','='), array('-','_','.'), $str ); }
			return $str;
		}
		
		public static function decode( $str , $url_enc=true )
		{
			// -_. must convert to +/= (URL Post)
			//if($url_enc==true){ $str=str_replace( array('-','_','.'), array('+','/','='), $str ); }
			$str=base64_decode( $str );
			return $str;
		}
	}
	// - - - end class myB64 - - -

	// - - - begin class myAES - - -
	class myAES
	{
		var $key="";
		var $keylength=0;

		function myAES()
		{
			$this->key="";
		}
		
		function loadKey( $key , $b64_decode=false )
		{
			if($b64_decode==true){ $key=myB64::decode($key); }
			$this->key=$key;
			$this->keylength=strlen($this->key)*8;
		}

		function loadKeyFromXML( $x , $b64_decode=false )
		{
			$xml="<KEYS>".$x."</KEYS>";
			$xml_obj=simplexml_load_string($xml);
			$k=trim($xml_obj->AESKeyValue);
			if($b64_decode==true){ $k=myB64::decode($k); }
			$this->key=$k;
			$this->keylength=strlen($this->key)*8;
		}

		function encrypt( $str , $b64_encode=false )
		{
			$aes = new Crypt_AES(CRYPT_AES_MODE_ECB);
			$aes->setKeyLength( $this->keylength );
			$aes->setKey( $this->key );
			$r=$aes->encrypt( $str );
			if( $b64_encode==true ){ $r=myB64::encode( $r ); }
			return $r;
		}

		function decrypt( $str , $b64_decode=false )
		{
			$aes = new Crypt_AES(CRYPT_AES_MODE_ECB);
			$aes->setKeyLength( $this->keylength );
			$aes->setKey( $this->key );
			if( $b64_decode==true ){ $str=myB64::decode( $str ); }
			$r=$aes->decrypt( $str );
			return $r;
		}
	}
	// - - - end class myAES - - -

	class myRSA
	{
		var $key="";
		
		function myRSA()
		{
			$this->key="";
		}

		function loadKeyFromXML( $x )
		{
			$this->key=$x;
		}
		
		function sign( $str )
		{
			$rsa = new Crypt_RSA();
			$rsa->setSignatureMode(CRYPT_RSA_SIGNATURE_PKCS1);
			$rsa->setPrivateKeyFormat(CRYPT_RSA_PRIVATE_FORMAT_XML);
			$rsa->loadKey( $this->key );
			$rsa->setHash('sha512');

			$dataenc=mb_convert_encoding($str, 'UTF-16LE');
			$dataenc=bin2hex( $dataenc );
			$dataenc=pack('H*', $dataenc);
            $dataToSign = new Math_BigInteger($dataenc, 256);

			$sig=$rsa->sign( $dataToSign->toBytes() );
			$sig=myB64::encode( $sig );
			return $sig;
		}

		function verify( $str , $sig_b64 )
		{
			$sig=myB64::decode( $sig_b64 );
			$rsa = new Crypt_RSA();
			$rsa->setSignatureMode(CRYPT_RSA_SIGNATURE_PKCS1);
			$rsa->setPublicKeyFormat(CRYPT_RSA_PUBLIC_FORMAT_XML);
			$rsa->loadKey( $this->key );
			$rsa->setHash('sha512');

			$dataenc=mb_convert_encoding($str , 'UTF-16LE');
			$dataenc=bin2hex( $dataenc );
			$dataenc=pack('H*', $dataenc);
			$dataToVerify = new Math_BigInteger($dataenc, 256);
			$verify_status=$rsa->verify( $dataToVerify->toBytes(), $sig );
			return $verify_status;
		}
	}
?>
