<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
global $db;

?>
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="col-sm-12">
				<div class="content block-flat ">
					<div class="page-head">
						<h3><i class="fa fa-list"></i> &nbsp;จำลองการลงผล E-learning ไปยัง WR</h3>
						<div class="form-group row">

							<label class="col-sm-1 control-label"  style=" padding-right:0px;">วันที่เริ่ม</label>
							<div class="col-sm-1"  style="padding-left:0px; padding-right:0px;">
								<input class="form-control" name="date_start" value="<?php echo revert_date(date("Y-m-d")); ?>" id="date_start" onblur="reCall();" placeholder="วันที่เริ่ม" type="text">
							</div>                                          
							<label class="col-sm-1 control-label"  style=" padding-right:0px;">วันที่สิ้นสุด</label>
							<div class="col-sm-1"  style="padding-left:0px; padding-right:0px;">
								<input class="form-control" name="date_stop" id="date_stop" onblur="reCall();" placeholder="วันที่สิ้นสุด" type="text">
							</div> 
							
							<label class="col-sm-2 control-label">
								<a href="#" class="btn" onclick="reCall();"><i class="fa fa-search">&nbsp;</i></a>&nbsp;&nbsp;
								<!-- <button onclick="clearSearch();" id="ok" class="btn btn-success" type="button">Clear</button> -->
							</label>                                            
						</div>
					</div>
					<table id="tbEmp" class="table" style="width:100%">
						  <thead>
							  <tr>
								  <th width="5%">ลำดับ</th>
								  <th width="5%">Order ID</th>
								  <th width="10%">คำนำหน้า</th>
								  <th width="10%">ชื่อ</th>
								  <th width="10%">นามสกุล</th>
								  <th width="10%">รหัสประจำตัวประชาชน</th>
								  <th width="7%">member_id</th>
								  <th width="7%">Course ID (WR)</th>
								  <th width="7%">Course detail ID (WR)</th>
								  <th width="10%">วันที่ลงทะเบียน</th>
								  <th width="15%">ส่งผลการเรียนไปที่ WR</th>
							  </tr>
						  </thead>   
						<tbody>
						</tbody>
					</table>
					<br><br>
				</div>
			</div>

		</div>
	</div> 
</div>
<?php include ('inc/js-script.php') ?>

<script type="text/javascript">
$(document).ready(function() {
	var oTable;
	$("#frmMain").validate();
	$("#date_start").datepicker({language:'th-th',format:'dd-mm-yyyy'});
    $("#date_stop").datepicker({language:'th-th',format:'dd-mm-yyyy'});
	listItem();
});

function listItem(){
   var url = "data/elernning-site-vender-list.php";
   oTable = $("#tbEmp").dataTable({
	   "sDom": 'T<"clear">lfrtip',
	   "oLanguage": {
   	   "sInfoEmpty": "",
   		"sInfoFiltered": ""
						  },
		"oTableTools": {
			"aButtons":  ""
		},
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": url,
		"sPaginationType": "full_numbers",
		"aaSorting": [[ 0, "desc" ]],
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			// aoData.push({"name":"menu_id","value":$("#srchMenuId").val()});
			aoData.push({"name":"date_start","value":$("#date_start").val()});			
			aoData.push({"name":"date_stop","value":$("#date_stop").val()});
			$.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});
		}
   }); 
}

function editInfo(id){
	if(typeof id=="undefined") return;
   var url = "index.php?p=<?php echo $_GET["p"];?>&emp_id="+id+"&type=info";
   redirect(url);
}
function addnew(){
   var url = "index.php?p=<?php echo $_GET["p"];?>&type=info";
   redirect(url);
}

function reCall(){
	oTable.fnClearTable( 0 );
	oTable.fnDraw();
}

function send_result(order_id, result, ref_elerning_site_ati_id, ref_uuid){
	$.ajax({
		url: 'elerning-site-vender-ctrl.php',
		type: 'POST',
		// dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
		data: {order_id: order_id
				, type: 'result'
				, username: 'elerning'
				, result: result
				, ref_elerning_site_ati_id: ref_elerning_site_ati_id
				, ref_uuid: ref_uuid
			},
	})
	.done(function() {
		console.log("success");
		$.gritter.removeAll({
	        after_close: function(){
	          $.gritter.add({
	          	position: 'center',
		        title: 'success',
		        text: 'ส่งผลการเรียน E-Lerning สำเร็จ',
		        class_name: 'success'
		      });
	        }
	    });
	})
	.fail(function() {
		console.log("error");
		$.gritter.removeAll({
	        after_close: function(){
	          $.gritter.add({
	          	position: 'center',
		        title: 'Error',
		        text: 'ส่งผลการเรียน E-Lerning ไม่สำเร็จ',
		        class_name: 'danger'
		      });
	        }
	     });
	})
	.always(function() {
		console.log("complete");
	});
	
}

</script>