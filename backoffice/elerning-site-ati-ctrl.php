<?php
include_once "./connection/connection.php";
include_once "./lib/lib.php";
include_once "./share/course.php";
require "./elerning/vendor/autoload.php";
global $db;

use \Curl\Curl;

$url_site_wr = get_config("elearning-url-site-wr");
$url_site_elearning = get_config("elearning-url-site-elearning");

$url = $url_site_elearning;

$api_user = "ati_user";
$api_key = "ati7214";
$date_now = date('Y-m-d H:i:s');

function write_log($file_name="", $msg="", $write_mode="a"){
	$str = $msg;	
	$date_now = date('Y-m-d H:i:s');
	$date_log = date('Y-m-d');
	$log_name = "./log/".$file_name."-".$date_log.".log";
	$file = fopen($log_name, $write_mode);
	$str_txt = $date_now."|".$str."\r\n";
	fwrite($file, $str_txt);
	fclose($file);
}//end func

function ref_uuid()
{
	return sprintf(
		'%02x%02x%02x%02x-%02x%02x-4%x%02x-%x%x%02x-%02x%02x%02x%02x%02x%02x',
		mt_rand(0, 255),
		mt_rand(0, 255),
		mt_rand(0, 255),
		mt_rand(0, 255),
		mt_rand(0, 255),
		mt_rand(0, 255),
		mt_rand(0, 15),
		mt_rand(0, 255),
		mt_rand(8, 11),
		mt_rand(0, 15),
		mt_rand(0, 255),
		mt_rand(0, 255),
		mt_rand(0, 255),
		mt_rand(0, 255),
		mt_rand(0, 255),
		mt_rand(0, 255),
		mt_rand(0, 255)
	);
}//end func

if ( !empty($_POST) ) {
	// echo "555";
	$type = !empty($_POST["type"]) ? $_POST["type"] : "";
	$register_id = !empty($_POST["register_id"]) ? $_POST["register_id"] : "";
	$course_id = !empty($_POST["course_id"]) ? $_POST["course_id"] : "";
	$course_detail_id = !empty($_POST["course_detail_id"]) ? $_POST["course_detail_id"] : "";
	$username = !empty($_POST["username"]) ? $_POST["username"] : "";

	$cond = "";
	if ( !empty($course_id) && !empty($course_detail_id) ) {
		$cond .= " AND (v.register_course_detail_course_detail_id={$course_detail_id} OR v.course_detail_id={$course_detail_id})";
	}//end if

	if ( $type=="register" && !empty($register_id) ) {

		// $rs = get_register("", $register_id);
		$q = "SELECT
					v.view_register_course_detail_id,
					v.register_course_detail_course_detail_id,
					v.course_detail_id,
					v.code_project,
					v.code_project_single,
					a.register_id,
					a.`no`,
					a.runno,
					a.docno,
					a.section_id,
					a.coursetype_id,
					a.member_id,
					a.title,
					a.fname,
					a.lname,
					a.cid,
					a.slip_type,
					a.receipttype_id,
					a.receipttype_text,
					a.receipt_id,
					a.taxno,
					a.slip_name,
					a.slip_address1,
					a.slip_address2,
					a.date,
					a.register_by,
					a.ref1,
					a.ref2, 
					a.`status`,
					a.expire_date,
					a.exam_date,
					a.course_id,
					-- a.course_detail_id,
					a.course_price,
					a.course_discount,
					a.pay,
					a.pay_status,
					a.pay_date,
					a.pay_method,
					a.pay_yr,
					a.pay_mn,
					a.pay_id,
					a.pay_price,
					a.pay_diff,
					a.approve,
					a.approve_by,
					a.approve_date,
					a.register_mod,
					a.last_mod,
					a.last_mod_date,
					a.result,
					a.result_date,
					a.result_by,
					a.receipt_title,
					a.receipt_fname,
					a.receipt_lname,
					a.receipt_no,
					a.receipt_gno,
					a.receipt_moo,
					a.receipt_soi,
					a.receipt_road,
					a.receipt_district_id,
					a.receipt_amphur_id,
					a.receipt_province_id,
					a.receipt_postcode,
					a.active,
					a.recby_id,
					a.rectime,
					a.remark
				FROM view_register_course_detail AS v
				LEFT JOIN register AS a ON a.register_id=v.view_register_course_detail_id
				WHERE v.view_register_course_detail_id={$register_id}
					AND a.active='T'
					{$cond}
		";

		echo $q;die();

		$info = $db->get($q);
		// d($info);die();

		foreach ($info as $key => $rs) {
			// d($rs);die();
			// $rs = $rs[0];
			// $course_id = $rs["course_id"];
			// $course_detail_id = $rs["course_detail_id"];
			$course_detail_id = strpos($rs["course_detail_id"], ":") ? $rs["register_course_detail_course_detail_id"] : $rs["course_detail_id"];

			$q="SELECT course_id FROM course_detail WHERE course_detail_id={$course_detail_id}";
			$course_id = $db->data($q);

			$member_id = $rs["member_id"];
			$cid = $rs["cid"];
			$title = $rs["title"];
			$fname = $rs["fname"];
			$lname = $rs["lname"];
			$expire_date = !empty($rs["expire_date"]) ? $rs["expire_date"] : '000-00-00 00:00:00';
			// d($rs);
			$curl = new Curl();
			// d($curl);die();

			$ref_uuid = ref_uuid();
			$ref_uuid = $register_id."-".$ref_uuid;
			// var_dump($gen_ref_uuid);die();

			$q="SELECT register_data_list_id FROM register_data_list WHERE active='T' AND register_id={$register_id}";
			// echo $q;die();
			$register_data_list_id = $db->data($q);

			$q="SELECT email1, birthdate FROM member WHERE member_id={$member_id}";
			$member_info = $db->rows($q);
			$email = !empty($member_info["email1"]) ? $member_info["email1"] : '';
			$birthdate = !empty($member_info["birthdate"]) ? $member_info["birthdate"] : '0000-00-00';

			/*
			$birthdate = '1981-03-25';
			echo $birthdate."<br>";
			$birthdate = revert_date($birthdate);
			$birthdate = str_replace("-", "/", $birthdate);
			echo $birthdate."<br>";

			$expire_date = '2011-09-21 23:59:59';
			echo $expire_date."<br>";
			$expire_date = revert_date($expire_date);
			$expire_date = str_replace("-", "/", $expire_date);
			echo $expire_date."<br>";
			*/

			$args = array();
			$args["table"] = "register_data_list";
			
			if( !empty($register_data_list_id) ){
			   // $args["id"] = (int)$register_data_list_id;
			   // $args["receive_date"] = null;
			   // $args["result_id"] = null;
			   // $args["send_status"] = 'F';
			}//end if

			$args["register_id"] = (int)$register_id;	
			$args["member_id"] = (int)$member_id;
			$args["cid"] = $cid;
			$args["title"] = $title;
			$args["fname"] = $fname;
			$args["lname"] = $lname;		
			$args["course_id"] = $course_id;
			$args["course_detail_id"] = $course_detail_id;
			$args["send_date"] = $date_now;
			$args["rectime"] = $date_now;
			$args["ref_uuid"] = $ref_uuid;
			$args["birthdate"] = $birthdate;
			$args["email"] = $email;
			$args["expire_date"] = $expire_date;
			// d($args);die();

			$ref_elerning_site_ati_id = $db->set($args);
			// var_dump($db->set($args, true, true));die();

			$ref_elerning_site_ati_id = !empty($ref_elerning_site_ati_id) ? $ref_elerning_site_ati_id : $register_data_list_id;


			if ( $birthdate=='0000-00-00' ) {
				$birthdate = '00/00/0000';
			}else{
				$birthdate = revert_date($birthdate);
				$birthdate = str_replace("-", "/", $birthdate);
			}//end else		

			$curl->post($url."/".$type."/", array(
			    'username' => $username,
			    'register_id' => $register_id,
			    'member_id' => $member_id,
			    'cid' => $cid,
			    'title' => $title,
			    'fname' => $fname,
			    'lname' => $lname,
			    'course_id' => $course_id,
			    'course_detail_id' => $course_detail_id,
			    'ref_elerning_site_ati_id' => $ref_elerning_site_ati_id,
			    'ref_uuid' => $ref_uuid,
			    'birthdate' => $birthdate,
			    'expire_date' => $expire_date,
			    'email' => $email,
			    'api_user' => $api_user,
			    'api_key' => $api_key,
			    'send_date' => $date_now
			));

			if ($curl->error) {
			    echo 'Error: ' . $curl->errorCode . ': ' . $curl->errorMessage . "\n";
			    $q = "UPDATE register_data_list 
			    	SET send_status='F'
			    		, rectime='{$date_now}' 
			    	WHERE register_data_list_id={$ref_elerning_site_ati_id}
			    ";
			    // echo $q;
			    $db->query($q);
			} elseif ($curl->response) {
			    // echo 'Data server received via POST:' . "\n";
			    // var_dump($curl->response->form);
			    var_dump($curl->response);			    
			}//end else
			
			$curl->close();
		}//end loop $rs	

	}elseif ( $type=="get-result" && !empty($_POST["ref_id"]) ) {
		// d($_POST);

		$ref_id = trim($_POST["ref_id"]);
		$ref_id = explode(",", $ref_id);
		$order_id = (int)$ref_id[0];
		$ref_uuid = $ref_id[1];
		
		$curl = new Curl();
		$curl->get($url."/".$type."/", array(			
		    'order_id' => $order_id,
		    'ref_uuid' => $ref_uuid,
		    'api_user' => $api_user,
		    'api_key' => $api_key
		));

		$rs = trim($curl->response);
		$rs = str_replace("<pre>", "", $rs);
		$rs = str_replace("</pre>", "", $rs);
		// echo $rs;
		$result_id = (int)$rs;

		if ( $result_id!=0 || $result_id!=null || $result_id!=false ) {
			$q = "UPDATE register_data_list 
		    	SET result_id={$result_id}
		    		, receive_date='{$date_now}'
		    		, rectime='{$date_now}' 
		    		, receive_status='T' 
		    	WHERE register_data_list_id={$order_id}
		    ";
		    echo $q;
		    $db->query($q);		
		}else{
			continue;
		}

/*
		if ($curl->error) {
		    echo 'Error: ' . $curl->errorCode . ': ' . $curl->errorMessage . "\n";
		} elseif ($curl->response) {
		    // echo 'Data server received via POST:' . "\n";
		    // var_dump($curl->response->form);
		    var_dump($curl->response);			    
		}//end else
*/
		$curl->close();
	}//end elseif

}//end if

?>