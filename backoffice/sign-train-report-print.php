<?php
include_once "./share/authen.php";
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
global $db;
require_once dirname(__FILE__) . '/PHPExcel.php';
$info = get_course_detail(" AND a.course_detail_id={$_GET["course_detail_id"]}");
$course_info = get_course(" AND a.course_id={$_GET["course_id"]}");

$filter = $_GET["filter"];

if($info){
	$info = $info[0];
	$course_info = $course_info[0];
	$course_detail_id = $info["course_detail_id"];
	$course_id = $course_info["course_id"];
	$fileName ="";
?>

<!DOCTYPE html>
<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>รายชื่อผู้สอบ <?php echo $course_info["title"]; ?></title>
<link href="css/printform-style.css" rel="stylesheet" type="text/css" media="all" />
<style>
	@media print{
		body{
			padding: 0px;

		}
		#btPrint{
			display: none;
		}
	}
	.rpt-sign{
		table-layout: fixed;
		width: 1199;
		/*width: 100%;*/
		word-wrap:break-word;
	}
	.rpt-sign td {
	  white-space: initial;
	}
</style>
<body>

	<div class="form-landscape">

		<?php 
			$ids = array();
			$q = " SELECT register_id 
				FROM register_course_detail 
				WHERE active='T' AND course_detail_id=$course_detail_id";
			$get_all = $db->get($q);
			if($get_all){
				foreach ($get_all as $key => $value) {
					$ids[] = $value["register_id"];
				}
			}
			$q = " SELECT register_id FROM register WHERE active='T'  AND course_detail_id='$course_detail_id'";
			$get_register_all = $db->get($q);
			if($get_register_all){
				foreach ($get_register_all as $key => $value) {
					$ids[] = $value["register_id"];
				}
			}
			$t = array_unique($ids);
			$con_ids = implode(",", $t);
			$ord_by = "order by a.no asc";

			if ($filter == 'name') {
				$ord_by = "order by a.fname ASC;";
			}
			if ($filter == 'course') {
				$ord_by = "order by a.course_id ASC";
			}

			$q = "SELECT a.title,
						 a.no,
						 a.fname,
						 a.lname,
						 a.cid,
						 a.branch,
						 b.email1,
						 b.tel_home,
						 b.tel_mobile,
						 b.tel_office,
						 c.name as org_name_text
			FROM register a inner join member b on a.member_id=b.member_id
				left join receipt c on c.receipt_id=b.org_name
			WHERE a.register_id in ($con_ids) AND a.pay_status in (3,5,6) AND a.active='T' 
			$ord_by
			";
			$r = $db->get($q);
			if($r){
				$i = 0;
				foreach ($r as $key => $value): 
					$i++;
						$cut_row = $i%12; //set record per page
					if ( $i==1 || $cut_row==1 ) {
		?>
		<!-- <table class="td-center"> -->
		<table class="rpt-sign">
			<col width=20>
			<col width=35>
			<col width=80>
			<col width=80>
			<col width=70>
			<col width=120>
			<col width=55>
			<col width=130>
			<col width=50>
			<col width=50>
			<col width=50>
			<col width=50>
			<col width=50>
			<col width=50>
			<col width=70>
			
			<!-- <thead> -->
				<tr>
					<td colspan="14" class="row-title">
						<div style="font-size: 26px"><?php echo $course_info["title"]." หลักสูตร".$course_info["section_name"]; ?></div>
						<div style="font-size: 22px">วันที่ <?php echo revert_date($info["date"])."&nbsp;เวลา ".$info["time"]; ?> น.</div>
						<div style="font-size: 22px"><?php echo $info['address_detail']." ".$info['address'] ?></div>
					</td>
				</tr>
				<tr>
					<td><span class="center" style="font-size: 22px">เลขที่นั่ง</span></td>
					<td><span class="center" style="font-size: 22px">คำนำหน้า</span></td>
					<td><span class="center" style="font-size: 22px">ชื่อ</span></td>
					<td><span class="center" style="font-size: 22px">สกุล</span></td>
					<td><span class="center" style="font-size: 22px">เลขที่บัตรประชาชน</span></td>
					<td><span class="center" style="font-size: 22px">บริษัท</span></td>
					<td><span class="center" style="font-size: 22px">โทรศัพท์<br>มือถือ</span></td>
					<td><span class="center" style="font-size: 22px">E-mail</span></td>
					<td colspan="2">
						<div class="page-header">
							<div style="font-size: 22px">หลักสูตรที่ 1</div>
							<br>
							<div>
								<table class="no-border">
									<tbody>
										<tr>
											<td width="50%" style="font-size: 22px">เซ็นเข้า</td>
											<td width="50%" style="font-size: 22px">เซ็นออก</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</td>
					<td colspan="2">
						<div class="page-header">
							<div style="font-size: 22px">หลักสูตรที่ 2</div>
							<br>
							<div>
								<table class="no-border">
									<tbody>
										<tr>
											<td width="50%" style="font-size: 22px">เซ็นเข้า</td>
											<td width="50%" style="font-size: 22px">เซ็นออก</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</td>
					<td colspan="2">
						<div class="page-header">
							<div style="font-size: 22px">หลักสูตรที่ 3</div>
							<br>
							<div>
								<table class="no-border">
									<tbody>
										<tr>
											<td width="50%" style="font-size: 22px">เซ็นเข้า</td>
											<td width="50%" style="font-size: 22px">เซ็นออก</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</td>
					<td><span class="center" style="font-size: 22px">หมายเหตุ</span></td>
				</tr>
			<!-- </thead> -->	
			<?php
					}//end if show header
			?>	
			<!-- <tbody>	 -->	
				<tr>
					<td width="30"><span class="center"><?php echo $value["no"]; ?></span></td>
					<td width="50"><span class="left"><?php echo $value["title"]; ?></span></td>
					<td width="100"><span class="left"><?php echo $value["fname"]; ?></span></td>
					<td width="100"><span class="left"><?php echo $value["lname"]; ?></span></td>
					<td width="100"><span class="center"><?php echo $value["cid"]; ?></span></td>
					<td width="130"><span class="left"><?php echo $value["org_name_text"]; ?></span></td>
					<td width="90"><span class="center"><?php echo $value["tel_mobile"]; ?></span></td>
					<td width="150"><?php echo $value["email1"]; ?></td>
					<td width="60"></td>
					<td width="60"></td>
					<td width="60"></td>
					<td width="60"></td>
					<td width="60"></td>
					<td width="60"></td>
					<td></td>
				</tr>			
					<?php 
						if ( $cut_row==1 && $i!=1 ) {
						?>
							<br>
							<div style="page-break-after: always"></div>
						<?php
						}//END IF
					endforeach ?>
			</tbody>
			<?php 
			}//end if
			?>
		</table>
	</div>

</body>
<script type="text/javascript">
function printPage(){
	   window.print();
	   setTimeout(" parent.$.fancybox.close()",1000);
	}
</script>
</html>
<?php } 
?>