<?php
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";

global $db;

//var_dump($_POST);
$course_id = $_POST["course_id"];
$args["table"] = "course";
$args["parent_id"] = (int)$_POST["course_id"];
//$mode = $_POST["mode"];
$args["code"] = $_POST["code"];
$args["section_id"] = (int)$_POST["section_id"];
$args["coursetype_id"] = (int)$_POST["coursetype_id"];
$args["code_project"] = $_POST["code_project"];
$args["title"] = $_POST["title"];
$args["life_time"] = (int)$_POST["life_time"];
$args["detail"] = $_POST["detail"];
$args["set_time"] = $_POST["set_time"];
$args["unit_time"] = $_POST["unit_time"];
$args["price"] = (int)$_POST["price"];
$args["inhouse"] = $_POST["inhouse"];
$args["rereg_date"] = (int)$_POST["rereg_date"];
// $args["set_time_fa"] = ($_POST["set_time_fa"]) ? $_POST["set_time_fa"] : (int)0;
// $args["unit_time_fa"] = ($_POST["unit_time_fa"]) ? $_POST["unit_time_fa"] : NULL;
$args["date_start"] = ($_POST["date_start"]) ? $_POST["date_start"] : '0000-00-00 00:00:00';
$args["date_stop"] = ($_POST["date_stop"]) ? $_POST["date_stop"] : '0000-00-00 00:00:00';
$args["discount"] = ($_POST["discount"]) ? $_POST["discount"] : (int)0;
$args["status"] = (int)1;
$args["active"] = $_POST["active"];
$args["recby_id"] = (int)$EMPID;
$args["createtime"] = date("Y-m-d H:i:s");

// d($args);
// d($db->set($args, true, true));
// die();
$ret = $db->set($args);

/*
$sql = "INSERT INTO course (
	section_id, code, title, detail, set_time
	, life_time, price, rereg_date, discount, status
	, coursetype_id, parent_id, recby_id, date_start, date_stop
	, code_project, unit_time, set_time_fa, unit_time_fa, inhouse
	, createtime)
	VALUES ("; 
$sql .=	$args["section_id"].", '".$args["code"]."', '".$args["title"]."', '".$args["detail"]."', '".$args["set_time"]."', '";
$sql .=	$args["life_time"]."', ".$args["price"].", ".$args["rereg_date"].", ".$args["discount"].", ".$args["status"].", ";
$sql .=	$args["coursetype_id"].", ".$args["parent_id"].", ".$args["recby_id"].", '".$args["date_start"]."', '".$args["date_stop"]."', '";
$sql .=	$args["code_project"]."', '".$args["unit_time"]."', '".$args["set_time_fa"]."', '".$args["unit_time_fa"]."', '".$args["inhouse"]."', '";
$sql .= $args["createtime"];
$sql .=	"')";


$ret = $db->set_update($sql);
*/
/*var_dump($ret);
var_dump($sql);*/
if ($ret){
	$_SESSION["success"]["msg"] = "บันทึกข้อมูลเรียบร้อยแล้ว";
	$args = array();
	$args["p"] = "course";
	$args["type"] = "childdetail";
	$args["course_id"] = $course_id;
	//var_dump($args);
	redirect_url($args);
}else{
	$_SESSION["error"]["msg"] = "ไม่สามารถบันทึกข้อมูลได้";
	$args = array();
	$args["p"] = "course";
	$args["type"] = "childdetail";
	$args["course_id"] = $course_id;
	redirect_url($args);
}

?>
