<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
global $db, $SECTIONID, $COURSETYPEID;

$con_section_id = ($SECTIONID>0) ? " and a.section_id={$SECTIONID}" : "";
$con_coursetype_id = ($COURSETYPEID>0) ? " and a.coursetype_id={$COURSETYPEID}" : "";
$section = datatype(" and a.active='T' {$con_section_id}", "section", true);
if($con_coursetype_id)
	$coursetype = datatype(" and a.active='T' {$con_coursetype_id}", "coursetype", true);
$type = $_GET["type"];
$str = ($type=="childlist") ? "หลักสูตรย่อย" : "หลักสูตร/วิชา";
if($type=="course_order") $str = "จัดเรียง หลักสูตร/วิชา";

?>
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="col-sm-12">
				<div class="content block-flat ">
					<div class="page-head">
						<button id="add" class="btn btn-success btn-small pull-right" onclick="addnew()" style="margin-top:10px;"><i class="fa fa-plus"></i> เพิ่มหลักสูตร</button>
						<h3><i class="fa fa-list"></i> &nbsp; <?php echo $str; ?></h3>
					</div>
						<div class="header">
							<div class="form-group row">
								<label class="col-sm-2 control-label">ประเภทใบอนุญาต/คุณวุฒิ</label>
								<div class="col-sm-3">
									<select name="section_id" id="section_id" class="form-control" onchange="reCall();">
										<option value="">---- เลือก ----</option>
										<?php foreach ($section as $key => $value) {
											$id = $value['section_id'];
											if($SECTIONID>0 && $SECTIONID!=$id) continue;
											if($SECTIONID>0 && $SECTIONID==$id){
												$select = "selected";
											}else{
												$select = "";
											}
											$name = $value['name'];
											echo  "<option {$select} value='$id'>$name</option>";
										} ?>

									</select>
								</div>                
								<label class="col-sm-2 control-label">ประเภทหลักสูตร</label>
								<div class="col-sm-2">
									<select name="coursetype_id" id="coursetype_id" class="form-control" onchange="reCall();">
										<option value="">---- เลือก ----</option>
										<?php foreach ($coursetype as $key => $value) {
											$id = $value['coursetype_id'];
											if($COURSETYPEID>0 && $COURSETYPEID==$id){
												$select = "selected";
											}else{
												$select = "";
											}
											$name = $value['name'];
											echo  "<option {$select} value='$id'>$name</option>";
										} ?>

									</select>
								</div>
								<label class="col-sm-1 control-label">สถานะ</label>
								<div class="col-sm-2">
									<select name="active" id="active" class="form-control" onchange="reCall();">
										<option selected="selected" value="T">แสดง</option>
										<option value="F">ไม่แสดง</option>
									</select>
								</div>                                           
							</div> 
							<?php if($type!="childlist"): ?>
							<div class="form-group row">
								<label class="col-sm-2 control-label"  style=" padding-right:0px;">วันที่เริ่มหลักสูตร</label>
								<div class="col-sm-2"  style="padding-left:0px; padding-right:0px;">
									<input class="form-control" name="date_start" id="date_start" onblur="reCall();" placeholder="วันที่เริ่มหลักสูตร" type="text">
								</div>                                          
								<label class="col-sm-2 control-label"  style=" padding-right:0px;">วันที่สิ้นสุดหลักสูตร</label>
								<div class="col-sm-2"  style="padding-left:0px; padding-right:0px;">
									<input class="form-control" name="date_stop" id="date_stop" onblur="reCall();" placeholder="วันที่สิ้นสุดหลักสูตร" type="text">
								</div> 
								<label class="col-sm-1 control-label">การแสดงผล</label>
								<div class="col-sm-2">
									<select name="childlist" id="childlist" class="form-control" onchange="reCall();">
										<option selected="selected" value="all">แสดงทั้งหมด</option>
										<option value="parent">หลักสูตรหลัก</option>
										<option value="child">หลักสูตรย่อย</option>
									</select>
								</div>
								<label class="col-sm-1 control-label">
									<a href="#" class="btn" onclick="reCall();"><i class="fa fa-search">&nbsp;</i></a>									
								</label>   								
							</div>
							<?php endif; ?> 
						</div>
					<table id="tbCourse" class="table" style="width:100%">
						  <thead>
							  <tr>
								  <th width="5%" class="center">ลำดับ</th>
								  <th width="8%">รหัส</th>
								  <th width="<?php echo (!$_GET["type"]) ? '30%' : '40%' ?>">ชื่อหลักสูตร</th>
								  <th width="17%">ประเภทใบอนุญาต/คุณวุฒิ</th>
								  <th width="13%">ประเภทหลักสูตร</th>
								  <th width="13%">IH/PB</th>
								  <th width="<?php echo (!$_GET["type"]) ? '20%' : '14%' ?>">

								  	<?php if($_GET["type"]=="course_order") { ?> 
										<a class="btn" onclick="saveAll();"><i class="fa fa-save">&nbsp;</i> บันทึกทั้งหมด</a>									
								  	<?php }else{ ?>
								  		Manage
								  	<?php } ?>
								  </th>
							  </tr>
						  </thead>   
						<tbody>
						</tbody>
					</table>
					<div class="clear"></div>
				</div>
			</div>

		</div>
	</div> 
</div>
<?php include ('inc/js-script.php') ?>

<script type="text/javascript">
$(document).ready(function() {
	var get_type = "<?php echo $_GET["type"]; ?>";
	if(get_type=="childlist" || get_type=="course_order") $("#add").hide();
	var oTable;
	listItem();
	$("#section_id").change(function(event) {
		var id = $(this).val();
		getDropDown('#coursetype_id', id, 'coursetype', 'data/coursetype.php')
	}); 
    $("#date_start").datepicker({language:'th-th',format:'dd-mm-yyyy'});
    $("#date_stop").datepicker({language:'th-th',format:'dd-mm-yyyy'});
 	
});

function listItem(){
   var get_type = "<?php echo $_GET["type"]; ?>";
   var url = "data/courselist.php";
   oTable = $("#tbCourse").dataTable({
   	   "iDisplayLength": 100,
	   "sDom": 'T<"clear">lfrtip',
	   "oLanguage": {
   	   "sInfoEmpty": "",
   		"sInfoFiltered": ""
						  },
		"oTableTools": {
			"aButtons":  ""
		},
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": url,
		"sPaginationType": "full_numbers",
		"aaSorting": [[ 0, "desc" ]],
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			aoData.push({"name":"section_id","value":$("#section_id").val()});			
			aoData.push({"name":"coursetype_id","value":$("#coursetype_id").val()});			
			aoData.push({"name":"active","value":$("#active").val()});			
			aoData.push({"name":"type","value":get_type});
			aoData.push({"name":"date_start","value":$("#date_start").val()});			
			aoData.push({"name":"date_stop","value":$("#date_stop").val()});				
			if(get_type!="childlist"){
				aoData.push({"name":"childlist","value":$("#childlist").val()});	
			}			
			$.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});
		}
   }); 
}

function editInfo(id){
	if(typeof id=="undefined") return;
   var url = "index.php?p=<?php echo $_GET["p"];?>&course_id="+id+"&type=info";
   redirect(url);
}


function childlist(id){
	if(typeof id=="undefined") return;
   var url = "index.php?p=<?php echo $_GET["p"];?>&course_id="+id+"&type=childdetail";
   redirect(url);
}

function addnew(){
   var url = "index.php?p=<?php echo $_GET["p"];?>&type=info";
   redirect(url);
}

function reCall(){
	oTable.fnClearTable( 0 );
	oTable.fnDraw();
}
function saveAll(){
	var c = 0;
	var s = 0;
	$("#tbCourse tbody tr td .form-control").each(function(){
		  var tag = $(this).attr("name");
		  id = tag.replace("menu_order_", "")-0;
		  var mixed_var = $("input[name=menu_order_"+id+"]").val();
		  var tmp = $("input[name=menu_order_"+id+"]").attr("data");
		  var whitespace =
		    " \n\r\t\f\x0b\xa0\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u200b\u2028\u2029\u3000";
		  var ck = (typeof mixed_var === 'number' || (typeof mixed_var === 'string' && whitespace.indexOf(mixed_var.slice(-1)) === -
		    1)) && mixed_var !== '' && !isNaN(mixed_var);
		  if(ck){
			$.ajax({
				"type": "POST",
				"async": true, 
				"url": "data/course-order-update.php",
				"data": {'course_id': id, 'data_value': mixed_var}, 
				"success": function(data){
					s++;
					reCall();
				}
			});
		  }
		  c++;
	});
	reCall();
	$.gritter.removeAll({
        after_close: function(){
          $.gritter.add({
          	position: 'center',
	        title: 'Success',
	        //text: data,
	        text: 'Save all',
	        class_name: 'success'
	      });
        }
    });
}

function update_order(id){
  var mixed_var = $("input[name=menu_order_"+id+"]").val();
  var tmp = $("input[name=menu_order_"+id+"]").attr("data");
  var whitespace =
    " \n\r\t\f\x0b\xa0\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u200b\u2028\u2029\u3000";
  var ck = (typeof mixed_var === 'number' || (typeof mixed_var === 'string' && whitespace.indexOf(mixed_var.slice(-1)) === -
    1)) && mixed_var !== '' && !isNaN(mixed_var);
  if(ck){
	$.ajax({
		"type": "POST",
		"async": false, 
		"url": "data/course-order-update.php",
		"data": {'course_id': id, 'data_value': mixed_var}, 
		"success": function(data){
			console.log(data);	
			$.gritter.removeAll({
		        after_close: function(){
		          $.gritter.add({
		          	position: 'center',
			        title: 'Success',
			        text: data,
			        class_name: 'success'
			      });
		        }
		      });
 			reCall();				      							 
		}
	});
  }else{
    $("input[name=menu_order_"+id+"]").val(tmp);
  }

}

</script>
<style>
	#tbCourse tbody td:nth-child(1){
		text-align: center;
	}
</style>