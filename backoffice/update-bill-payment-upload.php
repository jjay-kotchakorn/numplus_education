<?php
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";
include('share/class.upload.php');
global $db;
if($_POST){
	$args = array();
	$args["table"] = "bill_payment_upload";
	if($_POST["bill_payment_upload_id"])
	   $args["id"] = $_POST["bill_payment_upload_id"];
	else
		$args["name"] = revert_date(date("Y-m-d H:i:s"));
		
	$args["code"] = $_POST["code"];
	$args["remark"] = $_POST["remark"];
	$args["active"] = ($_POST["active"]) ? $_POST["active"] : "T";
	$args["recby_id"] = (int)$EMPID;
	$args["rectime"] = date("Y-m-d H:i:s");
	
   $ret = $db->set($args);
   $bill_payment_upload_id = $args["id"] ? $args["id"] : $ret;

}
$args = array();
$args["p"] = "bill-payment-upload";
$args["bill_payment_upload_id"] = $bill_payment_upload_id;
$args["type"] = "info";
redirect_url($args);
?>