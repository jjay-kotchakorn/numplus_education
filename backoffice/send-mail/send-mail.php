<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
global $db;
$date_now = date('Y-m-d H:i:s');

$debug = true;
$debug = false;


function sendmail_func($send_to_email="", $send_to_email_cc="", $subject="", $msg="", $run_in_file=""){
    if(empty($send_to_email)){
        $status = array('send_status' => 'F'
                    , 'error_msg' => 'send_to_email is not empty!!'
                );
        return $status;
    }//end if 
    global $db;
    $html = $msg;
    /**
     * This example shows making an SMTP connection with authentication.
     */

    //SMTP needs accurate times, and the PHP time zone MUST be set
    //This should be done in your php.ini, but this is how to do it if you don't have access to that
    //date_default_timezone_set('Etc/UTC');

    include_once  "./PHPMailer-master/PHPMailerAutoload.php";

    //Create a new PHPMailer instance
    $mail = new PHPMailer;
    //Tell PHPMailer to use SMTP
    $mail->isSMTP();
    //Enable SMTP debugging
    // 0 = off (for production use)
    // 1 = client messages
    // 2 = client and server messages
    //$mail->SMTPDebug = 2; 
    //Ask for HTML-friendly debug output

    //$email = $_POST['email'];


    //$message = "เรียน คุณ ".$data["fname"]." ".$data["lname"];
    
    $mail->CharSet = "utf-8";
    $mail->ContentType = "text/html";

    $mail->Debugoutput = 'html'; 
    //Set the hostname of the mail server
    $mail->Host = "mail.ati-asco.org"; 
    //Set the SMTP port number - likely to be 25, 465 or 587
    $mail->Port = 25; 
    //Whether to use SMTP authentication
    $mail->SMTPAuth = true; 
    //Username to use for SMTP authentication
    $mail->Username = "register@ati-asco.org"; 
    //Password to use for SMTP authentication
    $mail->Password = "Ati_1234"; //Set who the message is to be sent from
    $mail->setFrom('register@ati-asco.org', 'ASCO Training Institute (ATI)'); 
    //Set an alternative reply-to address
    // $mail->addReplyTo('thictyouth@numplus.in.th', 'Koom2'); 
    //Set who the message is to be sent to
    $mail->addAddress($send_to_email, 'You'); 
    $send_to_email_cc = trim($send_to_email_cc);
    if( !empty($send_to_email_cc) ){
        //Set Sent to CC
        $send_mail_cc = explode(",", $send_to_email_cc);
        $mail_cc = "";
        foreach ($send_mail_cc as $key => $cc) {
            $mail_cc = trim($cc);
            if ( !empty($mail_cc) ) {
                $mail->addCC($mail_cc);
            }//end if
        }//end loop $cc
        // $mail->addCC($send_to_email_cc); 
    }
    // $mail->AddCC('Examination@ati-asco.org');
    // $mail->AddCC('reg_training@ati-asco.org');
    //Set the subject line
    $mail->Subject = $subject;
    //$mail->Subject = 'แจ้งข้อมูลการลงทะเบียน';   
    //Read an HTML message body from an external file, convert referenced images to embedded,
    //convert HTML into a basic plain-text alternative body
    $mail->msgHTML($html); 

    //Replace the plain text body with one created manually
    // $mail->AltBody = 'This is a Automatic Mail'; 
    //Attach an image file
    /*echo $html;*/

    //send the message, check for errors

    $rs = $mail->send();
    $msg_error = "";
    $send_status = "";
    if (!$rs) {
        //echo "UPDATE ERROR";
        $send_status = "error";
        // $_SESSION["reister_info_sendmain"]["error"] = " มีข้อผิดพลาดเกิดขึ้น กรุณาติดต่อผู้ดูแลระบบ";
        $msg_error = $mail->ErrorInfo;
        $status = array('send_status' => 'F'
                    , 'error_msg' => $msg_error
                );
    } else {
        $send_status = "success";
        //echo "UPDATE SUCCESSFULLY";
        // $_SESSION["reister_info_sendmain"]["msg"] = "ระบบได้ส่ง รหัสผ่านไปยัง Email ของท่านเรียบร้อยแล้ว";
        $status = array('send_status' => 'T'
                    , 'error_msg' => ''
                );
    }//end else


/*	print_r($_SESSION["reister_info_sendmain"]);
    print_r($mail);
    echo $html;*/

    // $str_txt = "";    
    // $str_txt .= "|send_to_email={$send_to_email}|send_to_email_cc={$send_to_email_cc}";
    // $str_txt .= "|send_status={$send_status}|error_msg={$msg_error}";
    // $str_txt .= "|run_in_file={$run_in_file}";
    // $str_txt .= "|subject={$subject}|msg={$html}";
    // write_log("send-email", $str_txt, "a", "../send-mail/log/" );

    return $status;            

}//end func


?>