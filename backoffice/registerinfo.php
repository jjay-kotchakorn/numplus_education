<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
//include "./data/update-pay-status-log.php";

global $db, $RIGHTTYPEID, $EMPID;
$register_id = $_GET["register_id"];
$msg = $_GET["msg"];
$msg_txt = $_GET["text"];
if ($msg=="success") {
	$success = true;
	$error = "";
}elseif($msg=="error"){
	$error = true;
	$success = "";
}
$section = datatype(" and a.active='T'", "section", true);
$v = get_register("", $register_id);
$apay = datatype(" and a.active='T'", "pay_status", true);

$pde = get_config("paystatus_display_edit");
$paystatus_display_edit = array();
if($pde!=""){
	$paystatus_display_edit = explode(",", $pde);
}

$rpde = get_config("righttype_paystatus_display_edit");
$righttype_paystatus_display_edit = array();
if($rpde!=""){
	$righttype_paystatus_display_edit = explode(",", $rpde);
}


$arr_pay = array();
foreach ($apay as $key => $value) {
	$arr_pay[$value["pay_status_id"]] = $value["name"];
}
$str = "";
if($v){
	$v = $v[0];
	$result = $v["result"];
	$te_results = $v["te_results"];
	$info = $v;
	$ckresult = ($v["result"]!="") ? "T" : "F";
	$str = "เลขที่ ".$v["register_id"];
	$register_id = $v["register_id"];
	$sCourse_id = $v["course_id"];
	$list_course = $v["course_id"];
	$list_course_detail = $v["course_detail_id"];
	$register_pay_status = $v["pay_status"];
	$single = false;
	if(strrpos($list_course, ",")!==false){
		
	}else{
		$single = true;
	}
	$con = " and a.course_id in ($sCourse_id)";
	$r = get_course($con);
	$title = "";
	$parent_id = 0;
	$price = 0;
	$array_course = array();
	$array_course_detail = array();
	if($r){
		foreach ($r as $key => $row) {
			$course_id = $row['course_id'];
			$array_course[$course_id] = $row;
			$section_id = $row['section_id'];
			$code = $row['code'];
			$title .= $row['title'].", ";
			$set_time = $row['set_time'];
			$life_time = $row['life_time'];
			$status = $row['status']; 
			$parent_id = (int)$row['parent_id'];

		}
	}
	$q = "select a.course_detail_id, a.course_id from register_course_detail a where a.register_id={$register_id} and a.course_id in ($sCourse_id)";
	$cd = $db->get($q);
	$parent_title = "";
	if($parent_id>0){
		$q = "select title from course where active='T' and course_id=$parent_id";
		$t = $db->data($q);
		$parent_title = "<strong>{$t}</strong><br>";	
	}
	$title = trim($title, ", ");
	$price = $v["course_price"];
	$discount = $v["course_discount"];
	if($cd){    
		$display_date = "";
		$display_time = "";
		$display_address = "";
		foreach ($cd as $kk => $vv) {
			$row = get_course_detail(" and a.course_detail_id={$vv["course_detail_id"]}");
			if($row) $row = $row[0];
			$course_detail_id = $row['course_detail_id'];
			$course_id = $vv['course_id'];
			$array_course_detail[$course_id][$course_detail_id] = $row;
			$day = $row['day'];
			$date = $row['date'];
			$time = $row['time'];
			$display_date .= $day." ".revert_date($date).", ";
			$display_time .= $time.", ";
			$display_address .= $row['address_detail']." ".$row['address'].", ";
		}

	}else{
		$row = get_course_detail(" and a.course_detail_id={$v["course_detail_id"]}");
		if($row) $row = $row[0];
		$course_detail_id = $row['course_detail_id'];
		$course_id = $v['course_id'];
		$array_course_detail[$course_id][$course_detail_id] = $row;
		$day = $row['day'];
		$date = $row['date'];
		$time = $row['time'];
		$display_date .= $day." ".revert_date($date).", ";
		$display_time .= $time.", ";
		$display_address .= $row['address']." ".$row['address_detail'].", ";
	}

	$display_date = trim($display_date, ", ");
	$display_time = trim($display_time, ", ");
	$display_address = trim($display_address, ", ");
	$id = $v["register_id"];
	$title = str_replace(", ", "<br>",trim($title, ", "));
	$display_date = str_replace(", ", "<br>",trim($display_date, ", "));
	$display_time = str_replace(", ", "<br>",trim($display_time, ", "));
	$display_address = str_replace(", ", "<br>",trim($display_address, ", "));
	$content = "";
	$pay_type = "";
/*	
	if($v["pay"]=="at_ati"){
		$pay_type = "ชำระเงินสดผ่าน ATI";
	}else if($v["pay"]=="paysbuy"){
		$pay_type = "ชำระเงินช่องทางอื่นๆ (paysbuy)";
    }else if($v["pay"]=="mpay"){
		$pay_type = "ชำระเงินช่องทางอื่นๆ (mpay)";
	}else if($v["pay"]=="bill_payment"){
		$pay_type = "ชำระเงินผ่าน Bill-payment";
	}else if($v["pay"]=="at_asco"){
		$pay_type = "เช็ค/เงินโอน";
	}else if($v["pay"]=="walkin"){
		if($info["pay_status"]==5 || $info["pay_status"]==6)
			$pay_type = $arr_pay[$info["pay_status"]];
		else	
			$pay_type = "ชำระเงินสดผ่าน ATI";
	}else if($v["pay"]=="importfile"){
		if($info["pay_status"]==5 || $info["pay_status"]==6)
			$pay_type = $arr_pay[$info["pay_status"]];
		else	
			$pay_type = "ชำระเงินสดผ่าน ATI";
	}else if ($v["pay"]=="free") {
		$pay_type = "-";
	}
*/
	$pay_type_name = $v["pay"];
	$q = "SELECT name FROM pay_type WHERE code='{$pay_type_name}'";
	$pay_type = $db->data($q);

	if($pay_type_name=="walkin"){
		if($info["pay_status"]==5 || $info["pay_status"]==6)
			$pay_type = $arr_pay[$info["pay_status"]];
		// else	
		// 	$pay_type = "ชำระเงินสดผ่าน ATI";
	}else if($pay_type_name=="importfile"){
		if($info["pay_status"]==5 || $info["pay_status"]==6)
			$pay_type = $arr_pay[$info["pay_status"]];
		// else	
		// 	$pay_type = "ชำระเงินสดผ่าน ATI";
	}//end else

	if ($pay_type_name!="bill_payment") {
		$info["ref1"] = "";
		$info["ref2"] = "";
	}//end if

	$pay = $v["pay_id"];
	$status = (int)$v["pay_status"];
	$pay_date = $v["pay_date"];
	$d = explode(" ", revert_date($pay_date, true));
	//$pay_date_dd = ($d[0]) ? $d[0] : "";
	if ($v["pay_date"] == "0000-00-00 00:00:00") {
		//$d[0] = "dd-mm-yyyy";
		$d[0] = "-";
		$d[1] = "-";
	}
	$pay_date_dd = $d[0];
	$pay_date_tt = $d[1];
	
	$pay_date = ($pay_date && $pay_date!="") ? revert_date($pay_date, false) : '';
	$status_pay="";
/*	
	switch ($status) {
		case 1:
			$status_pay="รอการชำระเงิน";
			break;
		case 3:
			$status_pay="ชำระเงินแล้ว";
			break;
		case 6:
			$status_pay="ยกเว้นค่าธรรมเนียม";
			break;
	}//end sw
*/
	$q = "SELECT name FROM pay_status WHERE pay_status_id={$status}";
	$status_pay = $db->data($q);

	$td = "";
	if($info["slip_type"]=="individuals") $slip_type_text = "ออกในนามบุคคลธรรมดา";
	if($info["slip_type"]=="corparation") $slip_type_text = "ออกในนามนิติบุคคล (สำหรับเบิกค่าใช้จ่าย)";
	$q = "select name from receipttype where receipttype_id={$info["receipttype_id"]}";
	$receipttype_name = $db->data($q);
	$q = "select name from district where district_id={$info["receipt_district_id"]}";
	$district_name = $db->data($q);
	$q = "select name from amphur where amphur_id={$info["receipt_amphur_id"]}";
	$amphur_name = $db->data($q);
	$q = "select name from province where province_id={$info["receipt_province_id"]}";
	$province_name = $db->data($q);
	$q = "select register_id from register a where a.active='T' and  a.course_id='$course_id'";
	$id = $db->data($q);
	$total_price = $price - $discount;
	$t = convert_mktime($info["rectime"]) + (2 * 24 * 60 * 60);
	//$pay_date = $info["expire_date"];

}//end if


$q = "SELECT
		r.register_log_id,
		r.register_id,
		r.`no`,
		r.member_id,
		r.title,
		r.fname,
		r.lname,
		r.cid,
		r.slip_type,
		r.receipttype_id,
		r.receipttype_text,
		r.receipt_id,
		r.taxno,
		r.slip_name,
		r.slip_address1,
		r.slip_address2,
		r.date,
		r.register_by,
		r.ref1,
		r.ref2,
		r.`status`,
		r.expire_date,
		r.exam_date,
		r.course_id,
		r.course_detail_id,
		r.course_price,
		r.course_discount,
		r.pay,
		r.pay_date,
		r.pay_method,
		r.pay_yr,
		r.pay_mn,
		r.pay_id,
		r.pay_price,
		r.pay_diff,
		r.approve,
		r.approve_by,
		r.approve_date,
		r.register_mod,
		r.last_mod,
		r.last_mod_date,
		r.result,
		r.result_date,
		r.result_by,
		r.receipt_title,
		r.receipt_fname,
		r.receipt_lname,
		r.receipt_no,
		r.receipt_gno,
		r.receipt_moo,
		r.receipt_soi,
		r.receipt_road,
		r.receipt_district_id,
		r.receipt_amphur_id,
		r.receipt_province_id,
		r.receipt_postcode,
		r.active,
		r.recby_id,
		r.rectime,
		r.remark,
		r.coursetype_id,
		r.section_id,
		r.pay_status,
		r.branch,
		r.runno,
		r.docno,
		r.runyear,
		r.auto_del,
		r.usebook,
		p.name AS pay_status_name,
		CONCAT(e.prefix, e.fname, ' ', e.lname) AS emp_name
	FROM register_log AS r
	LEFT JOIN pay_status AS p ON p.pay_status_id=r.pay_status
	LEFT JOIN emp AS e ON e.emp_id=r.recby_id
	WHERE r.active='T'
		AND r.register_id={$register_id}
	ORDER BY rectime DESC
";

$register_log = $db->get($q);

// echo "<br><br><br><br><br><br><br><br>";
// echo $q;
// d($register_log);

$q="SELECT
		r.register_id,
		r.`no`,
		r.member_id,
		r.title,
		r.fname,
		r.lname,
		r.cid,
		r.slip_type,
		r.receipttype_id,
		r.receipttype_text,
		r.receipt_id,
		r.taxno,
		r.slip_name,
		r.slip_address1,
		r.slip_address2,
		r.date,
		r.register_by,
		r.ref1,
		r.ref2,
		r.`status`,
		r.expire_date,
		r.exam_date,
		r.course_id,
		r.course_detail_id,
		r.course_price,
		r.course_discount,
		r.pay,
		r.pay_date,
		r.pay_method,
		r.pay_yr,
		r.pay_mn,
		r.pay_id,
		r.pay_price,
		r.pay_diff,
		r.approve,
		r.approve_by,
		r.approve_date,
		r.register_mod,
		r.last_mod,
		r.last_mod_date,
		r.result,
		r.result_date,
		r.result_by,
		r.receipt_title,
		r.receipt_fname,
		r.receipt_lname,
		r.receipt_no,
		r.receipt_gno,
		r.receipt_moo,
		r.receipt_soi,
		r.receipt_road,
		r.receipt_district_id,
		r.receipt_amphur_id,
		r.receipt_province_id,
		r.receipt_postcode,
		r.active,
		r.recby_id,
		r.rectime,
		r.remark,
		r.coursetype_id,
		r.section_id,
		r.pay_status,
		r.branch,
		r.runno,
		r.docno,
		r.runyear,
		r.auto_del,
		r.usebook,
		r.te_results,
		r.register_no,
		r.doc_prefix,
		r.ref_exam_id,
		p.name AS pay_status_name,
		CONCAT(e.prefix, e.fname, ' ', e.lname) AS emp_name
	FROM register AS r
	LEFT JOIN pay_status AS p ON p.pay_status_id=r.pay_status
	LEFT JOIN emp AS e ON e.emp_id=r.recby_id
	WHERE r.active='T' AND r.register_id={$register_id}
";
$register = $db->get($q);
$register_log = array_merge($register, $register_log);
// d($register_log);

?>
<link rel="stylesheet" type="text/css" href="js/bootstrap.summernote/dist/summernote.css" />
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="row">
				  <div class="col-md-12">			      
					<div class="block-flat">
					  <div class="header">							
					  	<ol class="breadcrumb">
							<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>');">หน้าหลัก</a></li>
							<li class="active"><?php echo $str; ?></li>
						</ol>
					  </div>
					  <div class="content">
						<table class="table table-striped">
							<tbody class="no-border-x">
							<?php 
								if($array_course){
									$runNo = 1; 
									foreach($array_course as $k=>$row){
										$course_id = $row['course_id'];
										$code = $row['code'];
										$title = $row['title'];
									?>
	                        
				                        <tr>
				                            <td width="5%" colspan='6' style=" font-size:16px;">หลักสูตร <?php echo "[{$code}] {$title}"; ?></td>	                            
				                        </tr>
										<tr style="font-weight:bold;">
											<td width="10%" class="center">วัน </td>
											<td width="10%" class="center">เวลา</td>
											<td width="10%" class="center">สถานที่</td>
											<td width="25%" class="center">ข้อมูลสถานที่สอบ/อบรม</td>
											<td width="10%" class="center">เปิดรับสมัคร</td>
											<td width="10%" class="center">ปิดรับสมัคร</td>
										</tr>	
				                       	<?php 
				                       		if($array_course_detail[$course_id]) { 
				                       			foreach ($array_course_detail[$course_id] as $k => $v) { ?>
												<tr style="cursor:pointer;">													
													<td class="center"><strong style="font-size:15px;"><?php echo $v["day"]; ?></strong> <?php echo revert_date($v["date"]); ?></td>
													<td  class="center"><?php echo $v["time"]; ?></td>
													<td  class="center"><?php echo $v["address"]; ?></td>
													<td  class="center"><?php echo $v["address_detail"]; ?></td>													
													<td  class="center"><?php echo revert_date($v["open_regis"]); ?></td>
													<td  class="center"><?php echo revert_date($v["end_regis"]); ?></td>
												</tr>
										<?php 	} 
											}
										?>
									<?php
										$runNo++;
									} 	
								}
							 ?>
							</tbody>
						</table>
						<div class="header">							
							<h4>ข้อมูลผู้สมัคร</h4>
						</div>
						<div class="form-group row">	                			                
							<label class="col-sm-2 control-label">เลขที่บัตรประจำตัว 13 หลัก</label>
							<div class="col-sm-2"><?php echo $info["cid"]; ?></div>		                			                
							<label class="col-sm-2 control-label">คำนำหน้า - ชื่อ - นามสกุล</label>
							<div class="col-sm-2"><?php echo $info["title"]; ?> &nbsp;<?php echo $info["fname"]; ?>&nbsp;&nbsp;<?php echo $info["lname"]; ?></div>
							<label class="col-sm-2 control-label">วันที่สมัคร</label>
							<div class="col-sm-2"><?php echo revert_date($info['date']); ?></div>
						</div>
						<?php 

						if($info["slip_type"]){?>
							<div class="header">
								<h4>ข้อมูลสำหรับแสดงบนใบเสร็จรับเงิน</h4>
							</div>	
							<div class="form-group row">	                			                
								<label class="col-sm-1 control-label">ออกใบเสร็จ</label>
								<div class="col-sm-2"><?php echo $slip_type_text; ?></div>		                			                
								<label class="col-sm-2 control-label">ออกใบเสร็จในนาม</label>
								<div class="col-sm-3">
								<?php 
									$receipt_id = $info["receipt_id"];
									if($receipttype_name!="" && $info["slip_type"]=="corparation"){
										if($info["receipttype_id"]==5){
											$receipttype_name = "";
											$name = $info["receipttype_text"];
										}else{
											$name = $db->data("select name from receipt where receipt_id=$receipt_id");
										}
										echo $receipttype_name." ".$name;
									}else{
										echo $info["receipt_title"]." ".$info["receipt_fname"]."&nbsp&nbsp&nbsp&nbsp".$info["receipt_lname"]; 
									}
								 ?>

								</div>
								<label class="col-sm-2 control-label">เลขที่ประจำตัวผู้เสียภาษี</label>
								<div class="col-sm-2"><?php echo $info["taxno"]; ?></div>			                			                
							</div>
							<div class="form-group row">
								<label class="col-sm-1 control-label">บ้านเลขที่</label>
								<div class="col-sm-2"><?php echo $info["receipt_no"]; ?></div>
								<label class="col-sm-2 control-label">หมู่บ้าน / คอนโด / อาคาร</label>
								<div class="col-sm-2"><?php echo $info["receipt_gno"]; ?></div>
								<label class="col-sm-1 control-label">หมู่ที่</label>
								<div class="col-sm-1"><?php echo $info["receipt_moo"]; ?></div>
								<label class="col-sm-1 control-label">ซอย</label>
								<div class="col-sm-2"><?php echo $info["receipt_soi"]; ?></div>
							</div>
							<div class="form-group row">
								<label class="col-sm-1 control-label">ถนน</label>
								<div class="col-sm-2"><?php echo $info["receipt_road"]; ?></div>
								<label class="col-sm-1 control-label">ตำบล / แขวง</label>
								<div class="col-sm-2"><?php echo $district_name; ?></div>
								<label class="col-sm-1 control-label">อำเภอ / เขต</label>
								<div class="col-sm-2"><?php echo $amphur_name; ?></div>
							</div>
							<div class="form-group row">
								<label class="col-sm-1 control-label">จังหวัด</label>
								<div class="col-sm-2"><?php echo $province_name; ?></div>
								<label class="col-sm-2 control-label">รหัสไปรษณีย์</label>
								<div class="col-sm-2"><?php echo $info["receipt_postcode"]; ?></div>
							</div>
						<?php } ?>	
						<div class="header">							
							<h4>ข้อมูลการชำระเงิน</h4>
						</div>
						<div class="form-group row">	                			                
							<label class="col-sm-2 control-label">รูปแบบการชำระเงิน</label>
							<div class="col-sm-2"><?php echo $pay_type; ?></div>		                			                
							<label class="col-sm-2 control-label">สถานะการชำระ</label>
							<div class="col-sm-2"><?php echo $status_pay; ?></div>
						</div>
						<div class="form-group row">	                			                
							<label class="col-sm-2 control-label">ref 1</label>
							<div class="col-sm-2"><?php echo $info["ref1"]; ?></div>		                			                
							<label class="col-sm-2 control-label">ref 2</label>
							<div class="col-sm-2"><?php echo $info["ref2"]; ?></div>
						</div>
						<div class="form-group row">	                			                
							<label class="col-sm-2 control-label">ราคา (บาท)</label>
							<div class="col-sm-2"><?php echo set_comma($info["course_price"])?> </div>		                			                
							<label class="col-sm-2 control-label">ส่วนลด (บาท)</label>
							<div class="col-sm-2"><?php echo set_comma((int)$info["course_discount"]); ?> </div>
						</div>										
						  <form id="frmMain" name="frmMain" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="update-register.php">
						  <input type="hidden" name="register_id" id="register_id" value="<?php echo $register_id; ?>">
						  <input type="hidden" name="type" id="type" value="<?php echo "update"; ?>">
						  <div class="col-sm-12">		                
								<div class="form-group row">	                			                
								<label class="col-sm-1 control-label">วันที่ชำระเงิน</label>
								<div class="col-sm-3">
								  <input class="form-control" name="pay_date_dd" id="pay_date_dd" value="<?php echo $pay_date_dd; ?>"   placeholder="วันที่ชำระ" type="text" style="width:130px; display:inline-block;">
								  <input class="form-control" name="pay_date_tt" id="pay_date_tt" value="<?php echo $pay_date_tt; ?>"   placeholder="เวลา" type="text" style="width:50px; display:inline-block;">
								</div>		                			                
								<label class="col-sm-2 control-label">จำนวนเงินที่ชำระ </label>
								<div class="col-sm-2">
								  <input class="form-control required number" name="pay_price" value="<?php echo set_comma((int)$info["pay_price"]); ?>" id="pay_price" required="" placeholder="จำนวนเงินที่ชำระ" type="text">
								</div>			                			                
								<label class="col-sm-2 control-label">เลขที่ใบเสร็จ</label>
								<div class="col-sm-2">
								  <input class="form-control" name="docno" value="<?php echo $info["docno"]; ?>" id="docno" placeholder="เลขที่ใบเสร็จ" type="text">
								</div>		                			                
							  </div>				                
								<div class="form-group row">               			                
								<label class="col-sm-2 control-label">สถานะการชำระเงิน</label>
								<div class="col-sm-2">
									<select name="pay_status" id="pay_status" class="form-control required">
										<option value="">---- เลือก ----</option>
										<?php foreach ($apay as $key => $value) {
											$id = $value['pay_status_id'];

											if($id==9 || $id==7) continue;

											$s = "";
											if($info["pay_status"]==$id){
												$s = "selected";
											}
											$name = $value['name'];
											echo  "<option value='$id' {$s}>$name</option>";
										} ?>
				
									</select>
								</div>  
								<label class="col-sm-1 control-label">หมายเหตุ</label>
								<div class="col-sm-7">
									<textarea id="remark" name="remark" class="form-control" placeholder="หมายเหตุ"><?php echo $info["remark"] ?></textarea>
								</div>
								        			                
							  </div>
						  </div>
						  <div class="clear"></div>
						  <div class="form-group row" style="padding-left:10px;">
								<div class="col-sm-12">
									<?php if(in_array($register_pay_status, $paystatus_display_edit) && in_array($RIGHTTYPEID, $righttype_paystatus_display_edit) && $ckresult=="T") {
											$style = "display:none;";
										}?>
									<button type="button" class="btn btn-primary" onClick="ckSave()" style="<?php echo $style; ?>">Save changes</button>
									<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p'] ?>');">Cancel</button>
								</div>
						  </div>


						<br>
						<div class="container" style="background-color: #e9fff6"> 
							<div class="form-group row"">
								<div class="col-sm-12">

									<table class="table table-hover table-striped table-bordered">
										<caption style="font-size: 20px; margin-bottom: 15px;">เก็บประวัติสถานะการชำระเงิน</caption>
										<thead>
											<tr>
												<th class="center" style="font-size: 16px;">ลำดับ</th>
												<th class="center" style="font-size: 16px;">วันที่บันทึกข้อมูล</th>																	
												<th class="center" style="font-size: 16px;">สถานะการชำระ</th>
												<th class="center" style="font-size: 16px;">จำนวนเงิน</th>												
												<th class="center" style="font-size: 16px;">วันที่ชำระเงิน</th>
												<th class="center" style="font-size: 16px;">เลขที่ใบเสร็จ</th>	
												<th class="center" style="font-size: 16px;">ผู้บันทึก(เจ้าหน้าที่)</th>
												<th class="center" style="font-size: 16px;">หมายเหตุ</th>
											</tr>
										</thead>
										<tbody>
											
										<?php
											// d($register_log);
											// $runno = 0;
											$runno = 0;
											$tmp_log = array();
											foreach ($register_log as $key => $v) {
												// $runno++;												
												if ( empty($tmp_log) ) {
													$tmp_log = $v;	
												}else{
													if ( $tmp_log["register_id"]==$v["register_id"] ) {
														if ( $tmp_log["rectime"]==$v["rectime"] ) {
															$tmp_log = array();
															continue;
														}//end if					
													}//end if
												}//end else	
												$runno++;	
												$style_tr = ($runno%2==0) ? 'info' : '';
										?>

											<tr class="<?php echo $style_tr;?>">
												<td class="center"><?php echo $runno; ?></td>
												<td class="center"><?php echo $v["rectime"]; ?></td>
												
												<td class="center"><?php echo $v["pay_status_name"]; ?></td>
												<td class="center"><?php echo set_comma($v["pay_price"]); ?></td>
												<td class="center"><?php echo $v["pay_date"]; ?></td>
												<td class="center"><?php echo $v["docno"]; ?></td>
												<td><?php echo $v["emp_name"]; ?></td>
												<td class="left"><?php echo $v["remark"]; ?></td>
											</tr>
										<?php
											}//end loop $v
										?>

										</tbody>
									</table>

								</div>
						  	</div>



						</div>

						</form>
					  </div>
					</div>
					
				  </div>
				</div>
				
				
		

		</div>
	</div> 
</div>
<?php include_once ('inc/js-script.php'); ?>
<style>
	.form-group{
		border-bottom: 1px dashed #EFEFEF;
	}
</style>
<script type="text/javascript">
$(document).ready(function() {
   $("#frmMain").validate();
   var nowDate = new Date();
   var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);	
    $("#pay_date_dd").datepicker({language:'th-th',format:'dd-mm-yyyy' });
   var register_id = "<?php echo $_GET["register_id"]?>";
   if(register_id) viewInfo(register_id);
   

   	 // show succes
	 ///////////////////////////////////////////////
	 var error = "<?php echo $error ?>";
	 var msg_txt = "<?php echo $msg_txt ?>";
     if(error!=""){
		$.gritter.removeAll({
	        after_close: function(){
	          $.gritter.add({
	          	position: 'center',
		        title: 'Error',
		        text: 'บันทึกไม่สำเร็จ '+msg_txt,
		        class_name: 'danger'
		      });
	        }
	     });
     }
     var success = "<?php echo $success ?>";
     if(success!=""){
		$.gritter.removeAll({
	        after_close: function(){
	          $.gritter.add({
	          	position: 'center',
		        title: 'success',
		        text: 'บันทึกเรียบร้อย',
		        class_name: 'success'
		      });
	        }
	    });
	 }
	 /////////////////////////////////////////////

    $('#pay_status').on('change', function() {
		var get_pay_status = "<?php echo $status; ?>";
	  	var chg_pay_status = $("#pay_status").val();
	  	var pay_price = "<?php echo $info["pay_price"]?>";
	  	var pay_total_price = "<?php echo set_comma($info["course_price"]-$info["course_discount"])?>";

	  	if ( (get_pay_status!=chg_pay_status) && (chg_pay_status=='2' || chg_pay_status=='3') ) {
	  		//alert(course_discount);
	  		if ( pay_price==0 || pay_price=='0' || pay_price==0.00 || pay_price=='0.00') {
	  			$("#pay_price").val(pay_total_price);
	  		}
	  	}else if( (get_pay_status!=chg_pay_status) && (chg_pay_status=='8') ){
	  		$("#pay_price").val(0);
	  	}else if( (get_pay_status!=chg_pay_status) && (chg_pay_status!='8') ){
	  		$("#pay_price").val(pay_total_price);
	  	}else{
	  		$("#pay_price").val(pay_price);
	  	}
    })

    var RIGHTTYPEID = "<?php echo $RIGHTTYPEID;?>";
    var result = "<?php echo $result;?>";
    var te_results = "<?php echo $te_results;?>";
    // alert(result);
    //console.log(result+"-->"+te_results+"-->"+RIGHTTYPEID);
    $("#pay_status").prop("disabled", false);
 /*   
    if ( (RIGHTTYPEID!=1 || RIGHTTYPEID==4) && ( (result!="" || result!=null)||(te_results!="" || te_results!=null) ) ) {
    	$("#pay_status").prop("disabled", true);
    };
*/
	var register_pay_status = "<?php echo $register_pay_status ?>";
	var arr_pde = <?php echo json_encode($paystatus_display_edit); ?>;
	var ck_pde = jQuery.inArray( register_pay_status, arr_pde );

	var arr_rpde = <?php echo json_encode($righttype_paystatus_display_edit); ?>;
	var ck_rpde = jQuery.inArray( RIGHTTYPEID, arr_rpde );
	var ckresult = "<?php echo $ckresult; ?>";
	console.log(ckresult);
	if(ck_rpde!= -1 && ck_pde != -1 && ckresult=="T"){
			$("#pay_status").prop("disabled", true);

	}else{
		
		$("#pay_status").prop("disabled", false);
	}
/*	if( (result!="" || result!=null) && (te_results!="" || te_results!=null) ){
		//filter role
		if( RIGHTTYPEID!=1 && RIGHTTYPEID!=4 ){
			//console.log("55555555555");
		}//end if
	}else if( (te_results=="" || te_results==null) ){
	}//end else if*/
 });

function viewInfo(register_id){
   if(typeof register_id=="undefined") return;
   getregisterInfo(register_id);
}

function getregisterInfo(id){
	if(typeof id=="undefined") return;
	var url = "data/registerlist.php";
	var param = "register_id="+id+"&single=T";
	dataUrl(url, param,"#frmMain");
}


function ckSave(id){

  // send status pay for sendmail
  var register_id = "<?php echo $register_id ?>";
  var get_pay_status = "<?php echo $status; ?>";
  var chg_pay_status = $("#pay_status").val();
  var pay_date_dd = "<?php echo $pay_date_dd ?>";
  var pay_date_tt = "<?php echo $pay_date_tt ?>";
  //console.log(get_pay_status+" -> "+chg_pay_status);
  //console.log('id: '+register_id);
  // console.log('pay_date_dd: '+pay_date_dd+'|pay_date_tt: '+pay_date_tt);
  	if ( chg_pay_status=='9' ) {
		if ( get_pay_status=='3' ) {
	  		onCkForm("#frmMain");
	  		$("#frmMain").submit();
		}else{
	  		alert("ต้องชำระเงินก่อนจึงจะเปลี่ยนเป็นสถานะ ยกเลิก (Credit note) ได้");
	  		return;			
		}//end else
  	}else{
	  	onCkForm("#frmMain");
	  	$("#frmMain").submit();
  	}//end else	

  if ( (get_pay_status!=chg_pay_status) && (chg_pay_status=='2' || chg_pay_status=='3' || chg_pay_status=='5' || chg_pay_status=='6') ) {
  	//console.log("555");
  	//setTimeout("alert('send mail');",5000);
  	
	  $.ajax({
			"type": "POST",
			"async": false, 
			"url": "data/pay-status-sendmail.php",
			"data": {'register_id': register_id, 'pay_status': chg_pay_status}, 
			"success": function(data){	
				//console.log(data);			      							 
			}
		});
	  $.ajax({
			"type": "POST",
			"async": false, 
			"url": "check-course-elearning.php",
			"data": {'register_id': register_id, 'pay_status': chg_pay_status}, 
			"success": function(data){	
				//console.log(data);			      							 
			}
		});  	

  }//end if

}	
</script>