<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
include_once "./share/member.php";
require_once dirname(__FILE__) . '/PHPExcel.php';
global $db;
global $SECTIONID, $EMPID, $RIGHTTYPEID;

if($_POST["del_id"]>0){	
	$upload_signature_id = $_POST["del_id"];
	$q = "select a.name, a.url, a.upload_signature_id from upload_signature a where a.upload_signature_id=".$upload_signature_id;
	$r = $db->rows($q);
	if($r){		
		$args = array();
		$args["table"] = "upload_signature";
		$args["id"] = $upload_signature_id;
		$args["url"] = "";
		$args["recby_id"] = (int)$EMPID;
		$args["rectime"] = date("Y-m-d H:i:s");		
	    $db->set($args);
		unlink($r["url"]);
	}
}

if (!empty($_FILES) && $_POST) {
	$uploaddir = 'uploads-signature/'; 
	$error_msg = array();
	$success_msg = array();
	foreach ($_FILES['excelFile']['tmp_name'] as $key => $part) {
		$file = $uploaddir . basename($_FILES['excelFile']['name'][$key]);
		$size = $_FILES['excelFile']['size'][$key];
		if($size>10485760000){
			echo "error file size > 100 MB";
			unlink($_FILES['excelFile']['tmp_name'][$key]);
			continue;
		}
		if (move_uploaded_file($part, $file)) { 
			$fileName = $file;
		    $success_msg[$key] = "success"; 
				$args = array();
				$args["table"] = "upload_signature";
				$args["id"] = $key;
				$args["url"] = $file;
				$args["active"] = "T";
				$args["recby_id"] = (int)$EMPID;
				$args["rectime"] = date("Y-m-d H:i:s");		
			    $db->set($args);		    
		} else {
			$error_msg[$key] =  "error ".$_FILES['excelFile']['error']." --- ".$_FILES['excelFile']['tmp_name'][$key]." %%% ".$file."($size)";
		}  		
	}
}

?>

<link rel="stylesheet" type="text/css" href="js/jquery.nanoscroller/nanoscroller.css" />
<link href="js/jquery.icheck/skins/square/blue.css" rel="stylesheet">
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="row">

				<div class="col-md-12">           
					<div class="block-flat">
						<div class="header">              
							<ol class="breadcrumb">
								<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>');">หน้าหลัก</a></li>
								<li class="active">อัพโหลดรูปลายเซนต์</li>
							</ol>
						</div>
						<?php 
							$q = "select * from upload_signature where active='T'";
							$r = $db->get($q);

						 ?>

						<form id="frmMain" name="frmMain" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="">
							<table class="table table-striped" id="tbList">
								<thead>
									<tr style="cursor:pointer; " valign="middle" onClick="singleKey(this);">
										<td width="7%">ลำดัย</td>                            
										<td width="13%" class="center" id="cid">รหัส</td>
										<td width="50%" class="center" id="name_th">รายการ</td>
										<td width="30%" class="center" id="name_eng">รูปภาพ</td>
										<td width="30%" class="center" id="name_eng">จัดการ</td>
									</tr>
								</thead>
								<tbody>
									<?php if($r){
											$runno = 1;
											foreach ($r as $k => $v) {
												$id = $v["upload_signature_id"];												
												?>
												<tr style="cursor:pointer;">
													<td width="7%"><input type="hidden" name="upload_signature_id" id="upload_signature_id"><?php echo $runno; ?></td>                            
													<td width="13%" class="center"><?php echo $v["code"]; ?></td>
													<td width="50%" class="center"><?php echo $v["name"]; ?></td>
													<td width="30%" class="center">
														<?php if($v["url"]==""): ?>
															<input type="file" name="excelFile[<?php echo $id ?>]" class="required " data-no-uniform="true">
														<?php else: ?>
															<img src="<?php echo $v["url"]; ?>" alt="" height="80">
														<?php endif; ?>
													</td> 
													<td class="text-center"><?php if($v["url"]!=""): ?><a href="#" onClick="del_img(<?php echo $id ?>)" class="label label-danger"><i class="fa fa-times"></i></a><?php endif; ?></td>                                
												</tr><?php
												
												$runno++;
											}
										} 
										?>
								</tbody>
							</table>  
						</form>					

						<div class="clear"></div>
						<div class="form-group row" style="padding-left:10px;">
							<div class="col-sm-12">
							<button type="button" id="bt_save" class="btn btn-primary pull-right" onClick="ckSave()">Upload</button>																			
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>

	</div>
</div> 

<form id="frmDel" name="frmDel" class="form-horizontal group-border-dashed"  method="post" action="">
	<input type="hidden" name="del_id">
</form>
<?php include_once ('inc/js-script.php'); ?>
<script type="text/javascript">
	function ckSave(id){
		var t = confirm("ยืนยันการอัพโหลดไฟล์");
		if(!t) return false;
		$("#frmMain").submit();
	}
	function del_img(id){
		var t = confirm("ยืนยันลยไฟล์");
		if(!t) return false;		
		if(typeof id=="undefined") return;
		$("input[name=del_id]").val(id);
		$("#frmDel").submit();
	}
</script>
<style>
	#set_time-error{
		display: none !important;
	}
	#pay_status-error, #slip_type-error{
		width: 400px;
		margin-top: 21px;
	}
</style>