<?php
include_once "./share/authen.php";
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
global $db;
require_once dirname(__FILE__) . '/PHPExcel.php';

?>
<!DOCTYPE html>
<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>วุฒิบัตร IC,IBA </title>
<link href="css/printform-style.css" rel="stylesheet" type="text/css" media="all" />
<style>
	@media print{
		body{
			padding: 0px;
			margin: 0px;
		}
		#btPrint{
			display: none;
		}
	}
	.page-header {
		text-align: center;
		height: 600px;
		display: block;
		position: relative;
	}

</style>
<body>
<?php 
$id = $_GET["register_id"];
$idss = explode(",", $id);

foreach ($idss as $key => $id) {
	// AND (result=1 || te_results=1)
	$qry = "SELECT register_id FROM register WHERE register_id=$id AND (result=1 || te_results=1)";
	$res = $db->get($qry);
	$res = $res[0];
	if ($res != NULL) {
		$res_id = $res["register_id"];
		$ids[] = $res_id;
	}
	
}

//if(!$id) die();
$page = 1;
$all_page = count($ids); 

foreach ($ids as $key => $register_id) {

	if($register_id){
		$section = datatype(" and a.active='T'", "section", true);
		$v = get_register("", $register_id);
		$apay = datatype(" and a.active='T'", "pay_status", true);
		$pde = get_config("paystatus_display_edit");
		$paystatus_display_edit = array();
		if($pde!=""){
			$paystatus_display_edit = explode(",", $pde);
		}
		$arr_pay = array();
		foreach ($apay as $key => $value) {
			$arr_pay[$value["pay_status_id"]] = $value["name"];
		}
		$str = "";
		if($v){
			$v = $v[0];
			$info = $v;
			$str = "เลขที่ ".$v["register_id"];
			$register_id = $v["register_id"];
			$sCourse_id = $v["course_id"];
			$list_course = $v["course_id"];
			$list_course_detail = $v["course_detail_id"];
			$register_pay_status = $v["pay_status"];
			$single = false;
			if(strrpos($list_course, ",")!==false){
				
			}else{
				$single = true;
			}
			$con = " and a.course_id in ($sCourse_id)";
			$r = get_course($con);
			$title = "";
			$parent_id = 0;
			$price = 0;
			$array_course = array();
			$array_course_detail = array();
			if($r){
				foreach ($r as $key => $row) {
					$course_id = $row['course_id'];
					$array_course[$course_id] = $row;
					$section_id = $row['section_id'];
					$code = $row['code'];
					$title .= $row['title'].", ";
					$set_time = $row['set_time'];
					$life_time = $row['life_time'];
					$status = $row['status']; 
					$parent_id = (int)$row['parent_id'];

				}
			}
			$q = "select a.course_detail_id, a.course_id 
				from register_course_detail a where a.register_id={$register_id} and a.course_id in ($sCourse_id)";
			$cd = $db->get($q);
			$parent_title = "";
			if($parent_id>0){
				$q = "select title from course where active='T' and course_id=$parent_id";
				$t = $db->data($q);
				$parent_title = "<strong>{$t}</strong><br>";	
			}
			$title = trim($title, ", ");
			$price = $v["course_price"];
			$discount = $v["course_discount"];
			if($cd){    
				$display_date = "";
				$display_time = "";
				$display_address = "";
				foreach ($cd as $kk => $vv) {
					$row = get_course_detail(" and a.course_detail_id={$vv["course_detail_id"]}");
					if($row) $row = $row[0];
					$course_detail_id = $row['course_detail_id'];
					$course_id = $vv['course_id'];
					$array_course_detail[$course_id][$course_detail_id] = $row;
					$day = $row['day'];
					$date = $row['date'];
					$time = $row['time'];
					$display_date .= $day." ".revert_date($date).", ";
					$display_time .= $time.", ";
					$display_address .= $row['address_detail']." ".$row['address'].", ";
				}

			}else{
				$row = get_course_detail(" and a.course_detail_id={$v["course_detail_id"]}");
				if($row) $row = $row[0];
				$course_detail_id = $row['course_detail_id'];
				$course_id = $v['course_id'];
				$array_course_detail[$course_id][$course_detail_id] = $row;
				$day = $row['day'];
				$date = $row['date'];
				$time = $row['time'];
				$display_date .= $day." ".revert_date($date).", ";
				$display_time .= $time.", ";
				$display_address .= $row['address']." ".$row['address_detail'].", ";
			}

			$display_date = trim($display_date, ", ");
			$display_time = trim($display_time, ", ");
			$display_address = trim($display_address, ", ");
			$id = $v["register_id"];
			$title = str_replace(", ", "<br>",trim($title, ", "));
			$display_date = str_replace(", ", "<br>",trim($display_date, ", "));
			$display_time = str_replace(", ", "<br>",trim($display_time, ", "));
			$display_address = str_replace(", ", "<br>",trim($display_address, ", "));
			$content = "";
			$pay_type = "";
			$print_style = "";
			if($v["pay"]=="at_ati"){
				$pay_type = "ชำระเงินสดผ่าน ATI";
			}else if($v["pay"]=="paysbuy"){
				$pay_type = "ชำระเงินช่องทางอื่นๆ (paysbuy)";
				$print_style = "visibility:hidden";
			}else if($v["pay"]=="bill_payment"){
				$pay_type = "ชำระเงินผ่าน Bill-payment";
			}else if($v["pay"]=="at_asco"){
				$pay_type = "เช็ค/เงินโอน";
			}else if($v["pay"]=="walkin"){
				if($info["pay_status"]==5 || $info["pay_status"]==6)
					$pay_type = $arr_pay[$info["pay_status"]];
				else	
					$pay_type = "ชำระเงินสดผ่าน ATI";
			}else if($v["pay"]=="importfile"){
				if($info["pay_status"]==5 || $info["pay_status"]==6)
					$pay_type = $arr_pay[$info["pay_status"]];
				else	
					$pay_type = "ชำระเงินสดผ่าน ATI";
			}
			$pay = $v["pay_id"];
			$status = $v["pay_status"];
			$pay_date = $v["pay_date"];
			$pay_date = ($pay_date && $pay_date!="") ? revert_date($pay_date, false) : '';
			$status_pay = ($status==3) ? "ชำระเงินแล้ว" : "รอการชำระเงิน";
			$td = "";
			if($info["slip_type"]=="individuals") $slip_type_text = "ออกในนามบุคคลธรรมดา";
			if($info["slip_type"]=="corparation") $slip_type_text = "ออกในนามนิติบุคคล (สำหรับเบิกค่าใช้จ่าย)";
			$q = "select name from receipttype where receipttype_id={$info["receipttype_id"]}";
			$receipttype_name = $db->data($q);
			$q = "select name from district where district_id={$info["receipt_district_id"]}";
			$district_name = $db->data($q);
			$q = "select name from amphur where amphur_id={$info["receipt_amphur_id"]}";
			$amphur_name = $db->data($q);
			$q = "select name from province where province_id={$info["receipt_province_id"]}";
			$province_name = $db->data($q);
			$q = "select register_id from register a where a.active='T' and  a.course_id='$course_id'";
			$id = $db->data($q);
			$total_price = $price - $discount;
			$t = convert_mktime($info["rectime"]) + (2 * 24 * 60 * 60);
			//$pay_date = $info["expire_date"];
			$receipt_full = $info["receipt_title"].$info["receipt_fname"]." ".$info["receipt_lname"];
			$address = $info["receipt_no"];
			if($info["receipt_gno"]!="") 
				$address .= " ".$info["receipt_gno"];
			if($info["receipt_moo"]!="") 
				$address .= " ".$info["receipt_moo"];
			if($info["receipt_soi"]!="") 
				$address .= " ".$info["receipt_soi"];
			if($info["receipt_road"]!="") 
				$address .= " ".$info["receipt_road"];
			$address .= "<br>";
	        $address .= $info["receipt_district_name"];
	        $address .= $info["receipt_amphur_name"];
	        $address .= $info["receipt_province_name"];
	        $address .= $info["receipt_postcode"];

		}
?>
	<!-- <div class="form-portrait"> -->
	<div class="form-landscape">
		<div class="page-header">
			<div><h2>&nbsp;</h2></div>
			<div class="form-logo"><img src="images/logo1.png" alt="" /></div>
			<div><h3>วุฒิบัตรฉบับนี้ให้ไว้เพื่อแสดงว่า</h3></div>
			<div><h3 class="font-weight-bold"><?php echo $info["title"]; ?><?php echo $info["fname"]; ?>&nbsp;&nbsp;<?php echo $info["lname"]; ?></h3></div>
			<div><h3>ได้รับการอบรมครบถ้วนตามหลักสูตร</h3></div>
			<div><h2>&nbsp;</h2></div>
			<div>
				<?php 
					if($array_course){
						$runNo = 1;
						foreach($array_course as $k=>$row){
							$course_id = $row['course_id'];
							$code = $row['code'];
							$title = $row['title'];
							$course_price = $info['course_price'] - $info["course_discount"];
							$t = $array_course_detail[$course_id];
							foreach ($t as $key => $v) {}
						?>
							<h2 class="font-weight-bold"><?php echo "{$title}"; ?></h2>
	                       	<?php	
							?>
						<?php
							$runNo++;
						} 	
					}
				 ?>
				
			</div>
			<div><h2>&nbsp;</h2></div>
			<div><h4>ณ วันที่ <?php echo date_thai(convert_mktime($info["result_date"]), true); ?></h4></div>
			<div><h5><?php echo get_signature(1, 50, 50); ?></h5></div>
			<div><h4>(นายภาคภูมิ ภาย์วิศาลย)</h4></div>
			<div><h4>รองเลขาธิการ</h4></div>
			<div><h4>สมาคมบริษัทหลักทรัพย์ไทย</h4></div>
		</div>
	</div>
	<?php if($page!=$all_page) { ?>
		<!-- <br style="page-break-before: always; "> -->
		<div style="page-break-after: always"></div>
	<?php } ?>
<?php 
	$page++;
} 
}
?>

</body>
<script type="text/javascript">
function printPage(){
	   window.print();
	   setTimeout(" parent.$.fancybox.close()",1000);
	}
</script>
</html>