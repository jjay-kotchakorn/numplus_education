<?php
include_once "./share/authen.php";
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/menu.php";
include_once "./share/datatype.php";
global $db;

$table = $_GET["table"] ? $_GET["table"] : "course";
$checkBox = $_GET["ckBox"] ? "T" : "";
$retFunc = $_GET["retFunc"] ? $_GET["retFunc"] : "retInfo";
$con = $_GET["con"] ? trim($_GET["con"]) : "";
global $db;

?>
<!DOCTYPE html>
<html lang="en">
<?php include ('inc/header.php'); ?>
<body>

        <div class="box-content"> 
                 
            <div class="alert search-header" style="padding:10px;">
                <button onClick="javascript:parent.$.fancybox.close();" class="close" type="button">×</button>
                &nbsp;&nbsp;<span style="font-size: 13px;line-height: 13px;width: 120px;display: inline-block;">รหัสประจำตัวประชาชน</span>&nbsp;&nbsp;                        
                <input class="form-control focused" id="sCid" onKeyUp="disp();" style="width:140px;display:inline-block;" type="text" value=""> 
                &nbsp;&nbsp;
                <span style="font-size: 13px;line-height: 13px;width:100px;display: inline-block;">ชื่อ-นามสกุล</span>&nbsp;&nbsp;
                <input class="form-control focused" id="sSearch" onKeyUp="disp();" style="width:140px;display:inline-block;" type="text" value="">               
                &nbsp;&nbsp;
                <select name="sActive" id="sActive" class="form-control" onchange="disp();" style="width:150px;display:inline-block;">
                    <option selected="selected" value="T">แสดง</option>
                    <option value="F">ไม่แสดง</option>
                </select>&nbsp;&nbsp;                            
                <a href="#" class="btn btn-rad btn-info" onClick="disp();"><i class="fa fa-search"></i></a>&nbsp;&nbsp;
                <button onClick="getKey();" id="ok" class="btn btn-rad btn-success" type="button" title="คลิกเพื่อเลือกรายการ">เลือก/OK</button>
            </div>
            <div style="overflow-y:scroll; height:100%; vertical-align:top;"> 
                <table width="100%" class="table">
                    <thead>
                        <tr class="alert alert-success" style="font-weight:bold;">
                            <td width="7%"><input data-no-uniform="true" type="checkbox" onClick="ckAll(this)" id="ckAll">&nbsp;</td>
                            <td width="35%" class="center">ชื่อ-นามสกุล</td>
                            <td width="35%" class="center">ชื่อ-นามสกุล ภาษาอังกฤษ</td>
                            <td width="25%" class="center">รหัสประจำตัวประชาชน</td>                                  
                        </tr>
                    </thead>   
                </table>
            </div>
            <div style="overflow-y:scroll; height:285px; vertical-align:top; margin-top:-6px;">                      
                <table class="table table-striped" id="tbList">
                    <thead>
                        <tr style="cursor:pointer; " valign="middle" onClick="singleKey(this);">
                            <td width="7%"><input type="hidden" name="member_id" id="member_id">-</td>                            
                            <td width="35%" class="center" id="name_th">&nbsp;</td>
                            <td width="35%" class="center" id="name_eng">&nbsp;</td>
                            <td width="25%" class="center" id="cid">&nbsp;</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr style="cursor:pointer;" onClick="singleKey(this);">
                            <td width="7%"><input type="hidden" name="member_id" id="member_id">
                                &nbsp;</td>                            
                            <td width="35%" class="center" id="name_th">&nbsp;</td>
                            <td width="35%" class="center" id="name_eng">&nbsp;</td>
                            <td width="25%" class="center" id="cid">&nbsp;</td>                                
                        </tr>
                        <tr style="cursor:pointer;">
                            <td width="7%">
                                <input type="hidden" name="member_id" id="member_id">
                                <input type="checkbox" id="ckBox">&nbsp;&nbsp;</td>                            
                            <td width="35%" class="center" id="name_th">&nbsp;</td>
                            <td width="35%" class="center" id="name_eng">&nbsp;</td>
                            <td width="25%" class="center" id="cid">&nbsp;</td>                                 
                        </tr>
                    </tbody>
                </table>   
            </div>                      
        </div>
        

<?php   include_once ('inc/js-script.php'); ?>
    <?php include ('inc/footer.php') ?>
<script type="text/javascript">
        var checkBox = "<?php echo $checkBox ?>";
        var mapField = "<?php echo $map; ?>";
        var con = "<?php echo $con; ?>";
        if (checkBox == "T") {
            var trList = $("#tbList tbody tr:eq(1)").clone();
        } else {
            $("#ok").hide();
            var trList = $("#tbList tbody tr:eq(0)").clone();
            $("#ckAll").hide();
        }

        (checkBox == "T") ? delCkRow("#tbList", "1") : delRow("#tbList");

        $(document).ready(function() {
         
            disp();
        });

        function disp() {
            (checkBox == "T") ? delCkRow("#tbList") : delRow("#tbList");
            var url = "data/member-search.php";
            var param = "";
            param = param + "&active=" + $("#sActive").val();
            param = param + "&othercon=" + con;
            param = param + "&search=" + $("#sSearch").val();
            param = param + "&cid=" + $("#sCid").val();
            if ($("#ck").is(":checked")) {
                param = param + "&all=T";
            }
            $.ajax({
                "dataType": 'json',
                "type": "POST",
                "url": url,
                "data": param,
                "success": function(data) {
                    (checkBox == "T") ? delCkRow("#tbList") : delRow("#tbList");
                    $.each(data, function(index, array) {
                        var ck = "";
                        $("#tbList tbody tr :input").each(function() {
                            var t = $(this).val();
                            var ckId = array.member_id;
                            if (ckId == t) {
                                ck = "1";
                            }
                        });
                        if (ck == "1")
                            return;
                        addTrLine("#tbList", trList, array);
                    });
                }
            });
        }


        function singleKey(tag) {
            var ret = new Array();
            var i = 0;
            ret[i] = new Array();
            ret[i]["member_id"] = $(tag).find('#member_id').val();
            ret[i]["name_th"] = $(tag).find('#name_th').text();
            ret[i]["name_eng"] = $(tag).find('#name_eng').text();
            ret[i]["cid"] = $(tag).find('#cid').text();
            parent.<?php echo $retFunc; ?>(ret);
            parent.$.fancybox.close();
        }

        function getKey() {
            var ret = new Array();
            var i = 0;
            $("#tbList tbody tr :checkbox").each(function() {
                if ($(this).is(":checked")) {
                    var tag = $(this).parent().parent();
                    ret[i] = new Array();
                    ret[i]["member_id"] = $(tag).find('#member_id').val();
                    ret[i]["name_th"] = $(tag).find('#name_th').text();
                    ret[i]["name_eng"] = $(tag).find('#name_eng').text();
                    ret[i]["cid"] = $(tag).find('#cid').text();
                }
                i++;
            });
            parent.<?php echo $retFunc; ?>(ret);
            parent.$.fancybox.close();
        }

        function ckAll(tag) {
            $("#tbList tbody tr :checkbox").each(function() {
                if ($(tag).is(":checked")) {
                    $(this).attr('checked', true);
                } else {
                    $(this).attr('checked', false);
                }
            });
        }
    </script>   
</body>
</html>