<?php
include_once "./share/authen.php";
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/course.php";
include_once "./share/datatype.php";
global $db, $EMPID;
/** PHPExcel */
require_once "./report/Classes/PHPExcel.php";
/** PHPExcel_IOFactory - Reader */
include "./report/Classes/PHPExcel/IOFactory.php";
// echo "string";die();
$date_now = date('Y-m-d H:i:s');
$date = date('Y-m-d');
// var_dump($_GET);die();
$pay_date = $_GET["pay_date_chk"];
$pay_date_chk = thai_to_timestamp($pay_date);
$pay_type_chk = $_GET["pay_type_chk"];
$pay_type_txt = ($pay_type_chk=='paysbuy') ? 'paysbuy' : 'mpay';


?>


<!DOCTYPE html>
<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ผลการตรวจข้อมูลการชำระเงิน</title>
<link href="css/printform-style.css" rel="stylesheet" type="text/css" media="all" />
<style>
	@media print{
		body{
			padding: 0px;
			margin: 0px;
		}
		#btPrint{
			display: none;
		}
	}
	.rpt-print{
		table-layout: fixed;
		/*width: 1199;*/
		/*width: 100%;*/
		word-wrap:break-word;
	}
</style>
<!-- <body> -->
<body onload="receipt_tax_export()">
	<!-- <div class="form-portrait"> -->
	<div class="form-landscape">
		<div>
			<table class="no-border">
				<tbody>
					<tr>
						<td><span class="center font-weight-bold"><?php echo "ผลการตรวจข้อมูลการชำระเงิน";?></span></td>
					</tr>
					<tr>
						<td><span class="center"><?php echo "ช่องทางการชำระ : {$pay_type_txt} / วันที่ชำระ : {$pay_date}";?></span></td>
					</tr>
				</tbody>
			</table>

			<table class="td-center rpt-print" width=1200 style='table-layout:fixed'>
				<col width=30>

				<col width=65>
				<col width=55>
				<col width=100>
				<col width=120>
				<col width=80>
				<col width=70>

				<col width=55>
				<col width=70>
				<col width=80>
				<col width=70>
				<col width=40>				
				<col width=75>

				<col width=75>
				<col width=35>
				<thead>
					<tr>
						<td colspan="7">
							<span class="center font-weight-bold">ข้อมูลใบเสร็จรับเงินจาก WR</span>
						</td>
						<td colspan="7">
							<span class="center font-weight-bold">ข้อมูลการชำระจาก <?php echo $pay_type_txt?></span>
						</td>
						<td rowspan="2">
							<span class="center font-weight-bold">ผลการตรวจ</span>
						</td>

					</tr>

					<tr>
						<td ><span class="center">ลำดับ</span></td>					
						<td ><span class="center">เลขที่ใบเสร็จ</span></td>
						<td ><span class="center">วันที่ชำระ</span></td>
						<td ><span class="center">ชื่อผู้สมัคร</span></td>
						<td ><span class="center">หลักสูตร</span></td>
						<td ><span class="center">Project code</span></td>
						<td ><span class="center">ราคา</span></td>

						<td ><span class="right-line-center">วันที่ชำระ</span></td>
						<td ><span class="right-line-center">สถานะการชำระ</span></td>
						<td ><span class="center">Send/Receive</span></td>
						<td ><span class="center">ยอดชำระ</span></td>
						<td ><span class="center">Invoice No.</span></td>
						<td ><span class="center">Subject</span></td>
						<td ><span class="center">Buyer Card Name</span></td>
						<!-- <td ><span class="center">Y / N</span></td> -->
					</tr>
				</thead>
<?php
				$sql = "SELECT r.register_id
							, r.course_id
							, r.docno
							, r.pay_date
							, CONCAT(r.title,'',r.fname,' ',r.lname) AS cus_name
							, r.course_price
							, r.course_discount
							, r.course_detail_id
							, p.payment_mpay_register_list_id
							, p.register_id
							, p.payment_options_mpay_id
							, p.`code`
							, p.active
							, p.recby_id
							, p.rectime
							, p.remark
							, p.xml_status
							, p.xml_resp_code
							, p.xml_resp_desc
							, p.xml_sale_id AS invoice_no
							, p.xml_end_point_url
							, p.xml_result_code
							, p.xml_result_status
							, p.xml_result_desc
							, p.xml_result_payment_status AS payment_status
							, p.xml_credit_card_no AS send_receive
							, p.xml_order_expire_date
							, p.xml_amount
							, p.xml_inc_customer_fee
							, p.xml_exc_customer_fee
							, p.xml_purchase_amt AS amount
							, p.xml_payment_code
							, p.charge
							, '-' AS buyer_card_name
							-- , p.curl_url_for_payment
							-- , p.curl_params		
						FROM register AS r
						LEFT JOIN payment_mpay_register_list AS p ON p.register_id = r.register_id
						WHERE r.active='T' AND r.pay_status IN (3,9)
							AND r.pay='mpay'
							AND ( r.pay_date LIKE '{$pay_date_chk}%' ) 
							
						GROUP BY r.register_id
						-- ORDER BY p.payment_time DESC
				";
				$rs = $db->get($sql);

				// d($rs);
				foreach ($rs as $key => $v) {
					$register_id = $v["register_id"];

					$q = "SELECT register_credit_note_id FROM register_credit_note WHERE ref_new_register_id={$register_id}";
					$register_credit_note_id = $db->data($q);
					if ( !empty($register_credit_note_id) ) {
						unset($rs[$key]);
						continue;
					}//end if
				}//end loop $v

				// echo $sql;
				// d($rs);			
				// die();

			$runno=0;	
			$ttl_amount=0;
			$ttl_course_price=0;
			$ttl_cus_from_wr=0;
			$ttl_cus_from_bank=0;
			foreach ($rs as $key => $v) {
				$runno++;		
				if( !empty($v["register_id"]) ) $ttl_cus_from_wr++;
				if( !empty($v["payment_mpay_register_list_id"]) ) $ttl_cus_from_bank++;
				$amount = $v["amount"];
				// $amount = number_format($v["amount"]);
				$payment_date = explode(' ', $v["payment_date"]);
				$payment_date = $payment_date[0];
				$pay_date = explode(' ', $v["pay_date"]);
				$pay_date = $pay_date[0];
				$course_price = $v["course_price"]-$v["course_discount"];
				// $course_price = number_format($course_price);
				$status_chk = ($amount-$course_price == 0) ? $status_chk='&#10003' : $status_chk='&#x2718;';
				
				// get course name, project code				
				$sql = "SELECT a.course_id
							, a.course_detail_id
							, b.code_project
						FROM register_course_detail a 
						LEFT JOIN  course_detail b ON a.course_detail_id=b.course_detail_id
						WHERE a.active='T' AND a.register_id={$v["register_id"]}	
				";

				$rs = $db->get($sql);
				$course_name = "";
				$project_code = "";			
				if($rs){					
					foreach ($rs as $key => $c) {
						$info_cos = get_course("", $c["course_id"]);
						$info_cos = $info_cos[0];
						if ( !empty($info_cos) ) {
							$course_name .= $info_cos["title"]."<br><hr>";
							if ( !empty($c["code_project"]) ) {
								$project_code .= $c["code_project"];
							}else{
								$project_code .= "-";
							}
							$project_code .= "<br><hr>";
						}// end if
					}// end loop $c
				}else{
					$course_id = $v["course_id"];
					$info_cos = get_course("", $v["course_id"]);
					$info_cos = $info_cos[0];
					if ( !empty($info_cos) ) {
						$course_name .= $info_cos["title"];
					}// end if					
				}
				if(strpos(':', $v["course_detail_id"])==0 && !$project_code){
					$q = "select code_project from course_detail where course_detail_id={$v["course_detail_id"]}";
					$project_code  = $db->data($q);
				}
				$course_name = trim($course_name, "<hr>");
				$project_code = trim($project_code, "<hr>");
			
				//insert data to TB payment_compare_all_list
				$arg = array();
				$arg["table"] = "payment_compare_all_list";
				$arg["payment_compare_file_list_id"] = $v["payment_compare_file_list_id"];
				$arg["table_payment_compare_ref_id"] = $v["payment_mpay_register_list_id"];
				$arg["register_id"] = $v["register_id"];
				$arg["course_id"] = $v["course_id"];
				$arg["payment_date"] = $payment_date;
				$arg["payment_type"] = "{$pay_type_chk}";
				$arg["amount"] = (float)$v["amount"];
				$arg["fee"] = (float)$v["fee"];
				$arg["check_result"] = 'Y';
				$arg["recby_id"] = (int)$EMPID;
				$arg["date_create"] = "{$date_now}";
				$arg["pay_date_chk"] = "{$pay_date_chk}";
				// d($arg);
				$rs_add = $db->set($arg);
?>				
				<tbody>
					<td ><span class="center"><?php echo $runno;?></span></td>
					<td ><span class="center"><?php echo $v["docno"];?></span></td>
					<td ><span class="center"><?php echo revert_date($pay_date);?></span></td>
					<td ><span class="left"><?php echo $v["cus_name"];?></span></td>
					<td ><span class="left"><?php echo $course_name;?></span></td>
					<td ><span class="center"><?php echo $project_code;?></span></td>
					<td ><span class="right"><?php echo number_format($course_price, 2, '.', ',');?></span></td>
					
					<td ><span class="center"><?php echo revert_date($payment_date);?></span></td>
					<td ><span class="center"><?php echo $v["payment_status"];?></span></td>
					<td ><span class="center"><?php echo $v["send_receive"];?></span></td>
					<td ><span class="right"><?php echo number_format($amount,2, '.', ',');?></span></td>
					<td ><span class="center"><?php echo $v["invoice_no"];?></span></td>
					<td ><span class="left"><?php echo $v["subject"];?></span></td>
					<td ><span class="center"><?php echo $v["buyer_card_name"];?></span></td>
					<td ><span class="center font-weight-bold"><?php echo $status_chk;?></span></td>
				</tbody>
<?php
				$ttl_amount = $ttl_amount+$amount;
				$ttl_course_price = $ttl_course_price+$course_price;
			}// end loop v	

				//chk status
				$txt_status_n = "";
				if ( ($ttl_amount==$ttl_course_price) && ($ttl_cus_from_wr==$ttl_cus_from_bank) ) {
					$chk_status = 'Y';
					$bg_color = "bgcolor=\"#00FF00\"";
				}else{
					$chk_status = 'N';
					$bg_color = "bgcolor=\"#FF0000\"";
					$txt_status_n = "หมายเหตุ :  ";
					if ( $ttl_amount!=$ttl_course_price ) {
						$txt_status_n .= "จำนวนเงินทั้งหมดจากระบบ WR และจำนวนเงินทั้งหมดที่ชำระผ่าน {$pay_type_txt} ไม่ตรงกัน, ";
					}
					if ( $ttl_cus_from_wr!=$ttl_cus_from_bank ) {
						$txt_status_n .= "จำนวนผู้ชำระเงินจากระบบ WR และจำนวนผู้ชำระเงินจาก {$pay_type_txt} ไม่ตรงกัน, ";
					}
					$txt_status_n = trim($txt_status_n, ", ");
					//rs incorrect
					$sql = "DELETE FROM payment_compare_all_list
						WHERE active='T' 
						AND pay_date_chk='{$pay_date_chk}' 
						AND payment_type='{$pay_type_chk}'
					";	
					// echo $sql;		
					$db->query($sql);
				}

				$ttl_amount = number_format($ttl_amount,2, '.', ',');	
				$ttl_course_price = number_format($ttl_course_price,2, '.', ',');
?>
				<tbody>
					<tr>
						<td colspan="6"><span class="center">จำนวนเงินทั้งหมดจากระบบ WR</span></td>
						<td colspan=""><span class="right"><?php echo $ttl_course_price;?></span></td>				
						<td colspan="6"><span class="center"><?php echo "จำนวนเงินทั้งหมดที่ชำระผ่าน {$pay_type_txt}"?></span></td>
						<td colspan=""><span class="right"><?php echo $ttl_amount;?></span></td>
						<td rowspan="2" <?php echo $bg_color;?>><span class="center font-weight-bold"><br><?php echo $chk_status;?></span></td>
					
					</tr>
					<tr>
						<td colspan="6"><span class="center">จำนวนผู้ชำระเงินจากระบบ WR</span></td>
						<td colspan=""><span class="center"><?php echo number_format($ttl_cus_from_wr);?></span></td>
						<td colspan="6"><span class="center"><?php echo "จำนวนผู้ชำระเงินจาก {$pay_type_txt}"?></span></td>
						<td colspan=""><span class="center"><?php echo number_format($ttl_cus_from_bank);?></span></td>
					</tr>
				</tbody>

			</table>
		</div>
		<?php echo $txt_status_n; ?>
		<br>
	</div>

</html>	

<script type="text/javascript">
function printPage(){
   window.print();
   setTimeout(" parent.$.fancybox.close()",1000);
}
function receipt_tax_export(){
	var btn_confirm = confirm("หากต้องการ Export เป็นไฟล์ Excel ให้เลือก \"OK\"");
	if (btn_confirm == true) {
		var pay_date_chk = '<?php echo $pay_date_chk; ?>';
		var pay_type_chk = '<?php echo $pay_type_chk; ?>';
		url = "./report/report-payment-compare-mpay-export.php?pay_date_chk="+pay_date_chk+"&pay_type_chk="+pay_type_chk;
		// window.open(url,'_self');
		window.open(url,'_blank');
	} else {
	}
}
</script>
