<?php 
$msg = $_SESSION["login"]["msg"];
unset($_SESSION["login"]["msg"]);
?>
<div id="cl-wrapper" class="login-container">
	<div class="middle-login">
		<div class="block-flat">
			<div class="header">							
				<h3 class="text-center"><img class="logo-img" src="images/logo.png" alt="logo"/></h3>
			</div>
			<div>
				<form style="margin-bottom: 0px !important;" class="form-horizontal" method="post" action="update-login.php">
					<div class="content">
						<h4 class="title">Login Access</h4>
							<div class="form-group">
								<div class="col-sm-12">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-user"></i></span>
										<input type="text" placeholder="Username" id="username" name="username" class="form-control">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-lock"></i></span>
										<input type="password" placeholder="Password" id="password" name="password" class="form-control">
									</div>
								</div>
							</div>
							
					</div>
					<div class="foot">
						<!-- <button class="btn btn-default" data-dismiss="modal" type="button">Register</button> -->
						<button class="btn btn-primary" data-dismiss="modal" type="submit">Log me in</button>
					</div>
				</form>
			</div>
		</div>
	</div> 
</div>

<?php include ('inc/js-script.php') ?>

<script language="javascript" type="text/javascript">
	$(document).ready(function(){
		$("#form_login").validate();
	});
	var msg = "<?php echo $msg?>";
	if(msg!=""){
		var options =  $.parseJSON('{"text":"<p>'+msg+'</p>","layout":"center","type":"error"}');
		noty(options); 
	}
</script>