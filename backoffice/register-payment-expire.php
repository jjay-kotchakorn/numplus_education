<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
include_once "./share/section.php";
global $db;

$section = get_section(" and a.active='T'");
$coursetype = datatype(" and a.active='T'", "coursetype", true);
$payment_options = datatype(" and a.active='T'", "payment_options", true);
$str = "ตั้งค่าตัวเลือกในการชำระเงิน";

$q = "select data_value from config where name='payment_options_setting'";
$array_data = $db->data($q);

if(!$array_data){
	$array_data = array();
}else{
	$array_data = json_decode($array_data, true);
}

?>

<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="row">
					<div class="col-md-12">           
					<div class="block-flat">
						<div class="header">              
						<ol class="breadcrumb">
							<li class="active"><?php echo $str; ?></li>
						</ol>
						</div>
						<div class="content">
						<?php //print_r($array_data); ?>
							<form id="frmMain" name="frmMain" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="update-register-payment-expire.php">
								<div class="form-group row" >										
										<?php foreach ($section as $key => $value) {
											$section_id = $value['section_id'];
											$name = $value['name'];											
										 ?>
										<h4><?php echo $name; ?></h4>
										<input type="hidden" name="section_id[<?php echo $section_id; ?>]" value="<?php echo $section_id; ?>">
										<table class="table table-striped" id="tbList">
											<thead>
						                        <tr class="alert alert-success" style="font-weight:bold;">
						                            <td width="25%" class="center">อบรม</td>
						                            <td width="25%" class="center">สอบ</td>
						                            <td width="25%" class="center">อบรม+สอบ</td>
						                            <td width="25%" class="center">อบรมต่ออายุ</td>
						                        </tr>
						                    </thead>
											<tbody>
												<tr>
						                            <td width="25%" class="center">
						                            	<?php 
						                            	if($value["aob_rom"]=="have"){ ?>
																<?php foreach ($payment_options as $k => $v) {
																	$id = $v['payment_options_id'];
																	$name = $v['name'];
																	$selected = ($array_data[$section_id]["aob_rom"]==$id) ? "selected='selected'" : "";
																	$data = $array_data[$section_id]["aob_rom"][$id];
																	//print_r($data);
																	$checked = ($data["ck"]=="T") ? "checked" : "";
																	$day = $data["day"];
																	//  "<option {$selected} value='$id'>$name</option>";
																	?>
									                            	<div class="row">
									                            		<label class="col-sm-6 control-label"><?php echo $name; ?></label>
									                            		<div class="col-sm-6">
									                            			<div class="input-group">
									                            				<span class="input-group-addon">
									                            					<input type="checkbox" <?php echo $checked; ?> name="aob_rom[<?php echo $section_id; ?>][<?php echo $id; ?>][ck]">
									                            				</span>
									                            				<input type="text" name="aob_rom[<?php echo $section_id; ?>][<?php echo $id; ?>][day]" class="form-control" value="<?php echo $day; ?>">
									                            			</div>
									                            		</div>
									                            	</div>

																	<?php
																} ?>

						                            	<?php } 
						                            	?>
						                            </td>
						                            <td width="25%" class="center">
						                            	<?php 
						                            	if($value["sorb"]=="have"){ ?>
																<?php foreach ($payment_options as $k => $v) {
																	$id = $v['payment_options_id'];
																	$name = $v['name'];
																	$selected = ($array_data[$section_id]["sorb"]==$id) ? "selected='selected'" : "";
																	$data = $array_data[$section_id]["sorb"][$id];
																	$checked = ($data["ck"]=="T") ? "checked" : "";
																	$day = $data["day"];
																	// "<option {$selected} value='$id'>$name</option>";
																 ?>
									                            	<div class="row">
									                            		<label class="col-sm-6 control-label"><?php echo $name; ?></label>
									                            		<div class="col-sm-6">
									                            			<div class="input-group">
									                            				<span class="input-group-addon">
									                            					<input type="checkbox"  <?php echo $checked; ?> name="sorb[<?php echo $section_id; ?>][<?php echo $id; ?>][ck]">
									                            				</span>
									                            				<input type="text" name="sorb[<?php echo $section_id; ?>][<?php echo $id; ?>][day]" class="form-control" value="<?php echo $day; ?>">
									                            			</div>
									                            		</div>
									                            	</div>
															
						                            	<?php 
						                            	}
						                            } 
						                            	?>
						                            </td>
						                            <td width="25%" class="center">
						                            	<?php 
						                            	if($value["aob_rom_sorb"]=="have"){ ?>
																<?php foreach ($payment_options as $k => $v) {
																	$id = $v['payment_options_id'];
																	$name = $v['name'];
																	$selected = ($array_data[$section_id]["aob_rom_sorb"]==$id) ? "selected='selected'" : "";
																	$data = $array_data[$section_id]["aob_rom_sorb"][$id];
																	$checked = ($data["ck"]=="T") ? "checked" : "";
																	$day = $data["day"];																	
																	//echo  "<option {$selected} value='$id'>$name</option>";
																?>
									                            	<div class="row">
									                            		<label class="col-sm-6 control-label"><?php echo $name; ?></label>
									                            		<div class="col-sm-6">
									                            			<div class="input-group">
									                            				<span class="input-group-addon">
									                            					<input type="checkbox"  <?php echo $checked; ?> name="aob_rom_sorb[<?php echo $section_id; ?>][<?php echo $id; ?>][ck]">
									                            				</span>
									                            				<input type="text" name="aob_rom_sorb[<?php echo $section_id; ?>][<?php echo $id; ?>][day]" class="form-control" value="<?php echo $day; ?>">
									                            			</div>
									                            		</div>
									                            	</div>
															
						                            	<?php
						                            			}
						                            	 } 
						                            	?>
						                            </td>
						                            <td width="25%" class="center">
						                            	<?php 
						                            	if($value["aob_rom_tor_ar_yu"]=="have"){ ?>
																<?php foreach ($payment_options as $k => $v) {
																	$id = $v['payment_options_id'];
																	$name = $v['name'];
																	$selected = ($array_data[$section_id]["aob_rom_tor_ar_yu"]==$id) ? "selected='selected'" : "";
																	$data = $array_data[$section_id]["aob_rom_tor_ar_yu"][$id];
																	$checked = ($data["ck"]=="T") ? "checked" : "";
																	$day = $data["day"];																	
																	//echo  "<option {$selected} value='$id'>$name</option>";
																 ?>
									                            	<div class="row">
									                            		<label class="col-sm-6 control-label"><?php echo $name; ?></label>
									                            		<div class="col-sm-6">
									                            			<div class="input-group">
									                            				<span class="input-group-addon">
									                            					<input type="checkbox"  <?php echo $checked; ?> name="aob_rom_tor_ar_yu[<?php echo $section_id; ?>][<?php echo $id; ?>][ck]">
									                            				</span>
									                            				<input type="text" name="aob_rom_tor_ar_yu[<?php echo $section_id; ?>][<?php echo $id; ?>][day]" class="form-control" value="<?php echo $day; ?>">
									                            			</div>
									                            		</div>
									                            	</div>
															
						                            	<?php 
						                            		}
						                            	} 
						                            	?>
						                            </td>
						                        </tr>
											</tbody>
										</table>
									<?php } ?>
									</div>
								</div>
							</div>
							<div class="clear"></div>
							<div class="form-group row" style="padding-left:10px;">
								<div class="col-sm-12">
									<button type="button" class="btn btn-primary" onClick="ckSave()">Save changes</button>
									<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p'] ?>');">Cancel</button>
								</div>
							</div>
						</form>
						</div>
					</div>
					
					</div>
				</div>

		</div>
	</div> 
</div>
<?php include_once ('inc/js-script.php'); ?>
<script type="text/javascript">
	$(document).ready(function() {
	 $("#frmMain").validate();
	 var course_id = "<?php echo $_GET["course_id"]?>";
	 if(course_id) viewInfo(course_id);
	 
 });
 function viewInfo(course_id){
	 if(typeof course_id=="undefined") return;
	 getnewsInfo(course_id);
}

function getnewsInfo(id){
	if(typeof id=="undefined") return;
	var url = "data/courselist.php";
	var param = "course_id="+id+"&single=T";
	dataUrl(url, param,"#frmMain");
}

function addcourse_detail(){
   var course_id = "<?php echo $_GET["course_id"]?>";
   var url = "index.php?p=<?php echo $_GET["p"];?>&type=detail&course_id="+course_id;
   redirect(url);
}

function editcourse_detail(id){
	if(typeof id=="undefined") return;
	var course_id = "<?php echo $_GET["course_id"]?>";
	var url = "index.php?p=<?php echo $_GET["p"];?>&course_detail_id="+id+"&type=detail&course_id="+course_id;
	redirect(url);
}

function ckSave(id){
	onCkForm("#frmMain");
	$("#frmMain").submit();
} 
</script>