<?php 
function view_bill_payment_upload($con="", $bill_payment_upload_id="", $order=false){
   if($con=="" && $bill_payment_upload_id=="") return array();
   global $db;   
   $con_bill_payment_upload_id = $bill_payment_upload_id ? " and a.bill_payment_upload_id=$bill_payment_upload_id" : "";
   $con = $bill_payment_upload_id ? "" : $con;
   $con_orders = ($order==true) ? " a.bill_payment_upload_id " : " a.bill_payment_upload_id desc";
    $q = "select 
            a.bill_payment_upload_id,
            a.code,
            a.name,
            a.name_eng,
            a.active,
            a.recby_id,
            a.rectime,
            a.remark,
            CONCAT(b.prefix,b.fname,' ',b.lname) as recby_name
    from  bill_payment_upload a inner join emp b on a.recby_id=b.emp_id
    where a.active!='' $con $con_bill_payment_upload_id
    order by  $con_orders
    limit 400";
   $r = $db->get($q);   
   return $r;
}
?>
