<?php 
function get_section($con="", $section_id="", $order=false){
   if($con=="" && $section_id=="") return array();
   global $db;   
   $con_section_id = $section_id ? " and a.section_id=$section_id" : "";
   $con = $section_id ? "" : $con;
   $con_orders = ($order==true) ? " a.section_id " : " a.section_id desc";
   $q = "select a.section_id,
   				a.code,
				a.name,
				a.name_eng,
				a.aob_rom,
				a.sorb,
				a.aob_rom_sorb,
				a.aob_rom_tor_ar_yu,
				a.datetime_create,
				a.last_update,
				a.status,
				a.news,
				a.ati_news,
				a.title,
				a.detail,
				a.active,
				a.recby_id,
				a.rectime,
				a.remark
		 from section a             
		 where a.active!='' $con $con_section_id
		 order by $con_orders
		 limit 1000";
   $r = $db->get($q);   
   return $r;
}
?>
