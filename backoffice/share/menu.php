<?php 
function get_menu(){
	if(isset($_SESSION["menuinfo"])){
		return $_SESSION["menuinfo"];
	}else{
		global $db;
		if($db){
			$q = "select a.menu_id, a.code, a.name, a.name_eng, a.active, a.useclass, a.action, a.url
						,a.recby_id, a.rectime, a.active
				  from menu a 
				  where a.active='T' $con
				  order by a.code, a.menu_id";
			$rs = $db->get($q);
			return $_SESSION["menuinfo"] = (is_array($rs)) ? $rs : "";
		}
   }
}
function get_menulist($menu_id){
	if(isset($_SESSION["menulistinfo"][$menu_id])){
		return $_SESSION["menulistinfo"][$menu_id];
	}else{
	   global $db;
	   if($db){
			$q = "select a.menulist_id, a.code, a.name, a.name_eng, a.active, a.url,
						 a.recby_id, a.rectime, a.url, a.menu_id, a.short, a.action,
						 b.name as menu_name, b.name_eng as menu_name_eng, a.useclass
				from menulist a inner join menu b on a.menu_id=b.menu_id
				where a.active='T' and a.menu_id=$menu_id
				order by a.code, a.menulist_id";
			$rs = $db->get($q);
		   return $_SESSION["menulistinfo"][$menu_id] = (is_array($rs)) ? $rs : "";
	   }
	}
}
function get_menuvisible($righttype_id="", $type=""){
	global $db;
	if(!$type || !$righttype_id) return array();
	$con_menu_id = $type=="menu" ? " and a.menu_id is not null" : "";
	$con_menulist_id = $type=="menulist" ? " and a.menulist_id is not null" : "";
	$con_righttype_id = $righttype_id ? " and a.righttype_id=$righttype_id" : "";
	$q = "select a.menu_id, a.menulist_id, a.menuvisible_id, a.recby_id
			      ,b.name as righttypename
			      ,c.name as menuname
			      ,d.name as menulistname
			      ,e.prefix+' '+e.fname+' '+e.lname as empname
		   from menuvisible a inner join righttype b on a.righttype_id=b.righttype_id
			     left join menu c on c.menu_id=a.menu_id
			     left join menulist d on d.menulist_id=a.menulist_id
			     left join emp e on e.emp_id=a.recby_id
			where a.active='T' $con_menu_id $con_menulist_id $con_righttype_id
			order by a.menuvisible_id";
	$r = $db->get($q);
	return $r;
}

function check_righttype($righttype_id="", $menu_id="", $menulist_id="", $part=""){
   global $db;
   if(!$righttype_id) return false;
   $con_menu_id = $menu_id ? " and a.menu_id=$menu_id" : "";
   $con_menulist_id = $menulist_id ? " and a.menulist_id=$menulist_id" : "";
   $con_righttype_id = $righttype_id ? " and a.righttype_id=$righttype_id" : "";
   $con_part = $part ? " and d.url='$part'" : "";
   $q = "select a.menuvisible_id
         from menuvisible a inner join righttype b on a.righttype_id=b.righttype_id
              left join menu c on c.menu_id=a.menu_id and c.active='T'
              left join menulist d on d.menulist_id=a.menulist_id and d.active='T'
         where a.active='T' $con_menu_id $con_menulist_id $con_righttype_id $con_part
         order by a.menuvisible_id";
   $r = $db->data($q);
   if($r)return true;
   return false;
}
?>