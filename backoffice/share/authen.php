<?php
if (!isset($_SESSION)) {//initialize the session
  session_start();
}
header ('Content-type: text/html; charset=utf-8');
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_COMPILE_ERROR);
date_default_timezone_set('Asia/Bangkok');

$is_login = $_SESSION["login"]["isLogin"];
$is_admin_login = $_SESSION["login"]["isAdminLogin"];
if(isset($_SESSION["login"]["info"])){
   $LOGIN_INFO = $_SESSION["login"]["info"];
   $USERNAME = $LOGIN_INFO["username"];
   $EMPID = $LOGIN_INFO["emp_id"];
   $RIGHTTYPEID = $LOGIN_INFO["righttype_id"];
   $RIGTHTYPENAME = $LOGIN_INFO["righttype_name"];
   $ISADMIN = ($RIGHTTYPEID==1) ? "T" : "F";
   $SECTIONID = (int)$LOGIN_INFO["section_id"];
   $COURSETYPEID = (int)$LOGIN_INFO["coursetype_id"];
}
if(isset($_SESSION["emp"])){
	$EMPINFO = $_SESSION["emp"]["empInfo"];
	$EMPNAME = $EMPINFO["prefix"].$EMPINFO["fname"]." ".$EMPINFO["lname"];
	$EMPCODE = $EMPINFO["code"];
  if(!$EMPINFO["img"])
         $EMPINFO["img"] = "images/no-avatar-male.jpg";
  $EMPIMG  =  $EMPINFO["img"];
}
if(isset($LOGIN_INFO["member_id"]) && !$EMPID){
  $_SESSION["login"]["isLogin"] = false;
  session_destroy();
  //to fully log out a visitor we need to clear the session varialbles
  header("Location:index.php?p=default");
}
?>
