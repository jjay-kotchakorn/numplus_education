<?php 
function view_member($con="", $member_id="", $order=false, $limit=100){
   if($con=="" && $member_id=="") return array();
   global $db;   
   $con_member_id = $member_id ? " and a.member_id=$member_id" : "";
   $con = $member_id ? "" : $con;
   $con_orders = ($order==true) ? " a.member_id " : " a.member_id desc";
   $q = "select a.member_id,
            a.username,
            a.password,
            a.activate_code,
            a.actvate_date,
            a.title_th,
            a.title_th_text,
            a.fname_th,
            a.lname_th,
            a.title_en,
            a.title_en_text,
            a.fname_en,
            a.lname_en,
            a.email1,
            a.email2,
            a.slip_type,
            a.slip_name,
            a.slip_address1,
            a.slip_address2,
            a.get_news1,
            a.cid,
            a.cid_type,
            a.reg_date,
            a.reg_type,
            a.reg_by,
            a.status,
            a.comment,
            a.last_update,
            a.gender,
            a.birthdate,
            a.no,
            a.gno,
            a.moo,
            a.soi,
            a.road,
            a.district_id,
            a.amphur_id,
            a.province_id,
            a.postcode,
            a.tel_home,
            a.tel_mobile,
            a.tel_office,
            a.tel_other,
            a.tel_office_ext,
            a.receipt_id,
            a.receipttype_id,
            a.receipttype_text,
            a.receipt_title,
            a.receipt_title_text,
            a.receipt_fname,
            a.receipt_lname,
            a.receipt_no,
            a.receipt_gno,
            a.receipt_moo,
            a.receipt_soi,
            a.receipt_road,
            a.receipt_district_id,
            a.receipt_amphur_id,
            a.receipt_province_id,
            a.receipt_postcode,
            a.receipt_tel_home,
            a.receipt_tel_mobile,
            a.receipt_tel_office,
            a.receipt_tel_other,
            a.org_type,
            a.org_type as orgtype_id,
            a.org_name,
            a.org_lv,
            a.org_lv as orgtype_text,
            a.org_position,
            a.grd_lv1,
            a.grd_yr1,
            a.grd_ugrp1,
            a.grd_uid1,
            a.grd_uname1,
            a.grd_major1,
            a.active,
            a.recby_id,
            a.rectime,
            a.activelogin,
            h.branch,
            a.taxno,
            a.grd_other,
            a.nation,
            b.name as district_name,
            c.name as amphur_name,
            d.name as province_name,            
            e.name as receipt_district_name,
            f.name as receipt_amphur_name,
            g.name as receipt_province_name,
            h.name as receipt_name,
            i.name as org_name_text
            from  member a left join district b on b.district_id=a.district_id
                left join amphur c on c.amphur_id=a.amphur_id
                left join province d on d.province_id=a.province_id        
                left join district e on e.district_id=a.receipt_district_id
                left join amphur f on f.amphur_id=a.receipt_amphur_id
                left join province g on g.province_id=a.receipt_province_id
                left join receipt h on h.receipt_id=a.receipt_id
                left join receipt i on i.receipt_id=a.org_name
            where a.active!='' $con $con_member_id
            order by $con_orders
            limit $limit";
           $r = $db->get($q);   
     return $r;
}

function get_receipt($con="", $receipt_id="", $order=false){
   if($con=="" && $receipt_id=="") return array();
   global $db;   
   $con_receipt_id = $receipt_id ? " and a.receipt_id=$receipt_id" : "";
   $con = $receipt_id ? "" : $con;
   $con_orders = ($order==true) ? " a.receipt_id " : " a.receipt_id desc";
    $q = "select a.receipt_id,
         a.code,
         a.name,
         a.name_eng,
         a.receipttype_id,
         a.no,
         a.gno,
         a.moo,
         a.soi,
         a.road,
         a.district_id,
         a.amphur_id,
         a.province_id,
         a.postcode,
         a.address_other,
         a.taxno,
         a.branch,
         a.active,
         a.recby_id,
         a.rectime,
         a.remark,
         c.name as province_name,
         d.name as district_name,
         e.name as amphur_name
         from receipt a left join receipttype b on a.receipttype_id=b.receipttype_id
         left join province c on c.province_id=a.province_id
         left join district d on d.district_id=a.district_id
         left join amphur e on e.amphur_id=a.amphur_id
        where a.active!='' $con $con_receipt_id
    order by $con_orders
    limit 400";
   $r = $db->get($q);   
   return $r;
}

function check_block_list($cid, $exd, $exm, $exy) {
    /*
    Request 
             $cid = เลขที่บัตรประจ าตัวประชาชน หรือ หนังสือเดินทาง 
             $exd = วันที่สอบ 1-31 
             $exm = เดือนสอบ 1-12 
             $exy = ค.ศ. YYYY 

    Response (ถ้า connect ได้) 
             1000:0000-00-00 : connect ได้ ข้อมูลไม่ถูกต้อง (จ านวนข้อมูล) 
             1001:0000-00-00 : connect ได้ ข้อมูลไม่ถูกต้อง (เลขที่บัตร) 
             1002:0000-00-00 : connect ได้ ข้อมูลไม่ถูกต้อง (วันที่) 
             2000:0000-00-00 : invalid user or password 
             3000:0000-00-00 : ไม่อยู่ในรายการห้ามสอบ 
             4000:YYYY-MM-DD : อยู่ในรายการห้ามสอบ ถึงวันที่ YYYY-MM-DD  */  

    $cusr = "ati";
    $cpwd = "ati@793";
    $rpcurl = "http://27.254.77.161/bs/blrpc4.php";

    $request = $cid.",".$cusr.",".$cpwd.",".$exd.",".$exm.",".$exy;
    $ch  =  curl_init();    
    $url = $rpcurl;
    curl_setopt($ch, CURLOPT_URL, $url);
    $data  =  array('request'  => $request);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    
    $r = curl_exec($ch);
    curl_setopt($ch, CURLOPT_POST, false);
    curl_close($ch);
    $r = trim($r);
    return $r;
}
?>