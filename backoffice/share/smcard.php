<?php 
function get_smcard_report_list($con="", $smcard_report_list_id="", $order=false, $debug=false){
   if($con=="" && $smcard_report_list_id=="") return array();
   global $db;   
   $con = $smcard_report_list_id ? "" : $con;
   $con_orders = ($order==true) ? " a.smcard_report_list_id ASC" : " a.smcard_report_list_id DESC";
   $q = "SELECT
			a.smcard_report_list_id,
			a.register_id,
			a.register_course_detail_id,
			a.course_id,
			a.course_detail_id,
			a.coursetype_id,
			a.section_id,
			a.member_id,
			a.cid,
			a.emp_id,
			a.smcard_report_id,
			a.smcard_stamp_type_id,
			a.datetime_stamp,
			a.time_start,
			a.time_stop,
			a.active,
			a.recby_id,
			a.rectime,
			a.remark
		FROM smcard_report_list AS a   
		WHERE a.active!='' $con
		ORDER BY $con_orders
		LIMIT 1000";
   if ( $debug==false ) {
   		$r = $db->get($q);   
   }else{
   		$r = $q;
   }//end else
   
   return $r;
}//end func

?>
