<?php 
include_once "./share/authen.php";
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/menu.php";

$pay_date = $_GET["pay_date"];
$pay_type = $_GET["pay_type"];
?>
<!DOCTYPE html>
<html lang="en">
<?php include ('inc/header.php'); ?>
<body>
	<?php
	include_once "./share/datatype.php";
	global $db;
	//$bill_payment_upload_id = $_GET["bill_payment_upload_id"];	
	?>
	<link rel="stylesheet" type="text/css" href="js/bootstrap.summernote/dist/summernote.css" />
	<div id="cl-wrapper">
		<div class="container-fluid" id="pcont">
			<div class="cl-mcont" style="height:400px;">
				
				<div class="col-md-12">
					<div class="content">
						<div class="cl-mcont">
							<form action="payment-compare-upload-process.php" class="dropzone dz-clickable" id="my-awesome-dropzone">
								<input type="hidden" name="pay_date" id="pay_date" value="<?php echo $pay_date;?>">
								<input type="hidden" name="pay_type" id="pay_type" value="<?php echo $pay_type;?>">
								<!-- <input type="hidden" name="bill_payment_upload_id" id="bill_payment_upload_id" value="<?php echo $bill_payment_upload_id; ?>"> -->
								<div class="dz-default dz-message"><span>ลากไฟล์มาไว้ที่นี่</span></div>
							</form>
						</div>						
					    <a class="label label-default pull-right" href="#" onclick="ckClose();">ปิดหน้านี้</a>		
					</div>
				</div>		

			</div>
		</div> 
	</div>
	<?php 		  	
	include_once ('inc/js-script.php');
	include ('inc/footer.php') ?>
</body>

<script type="text/javascript" src="js/bootstrap.summernote/dist/summernote.min.js"></script>
<script type="text/javascript" src="js/dropzone/dropzone.js"></script>
<script type="text/javascript">

function ckClose(){
	window.parent.$.fancybox.close();
}

</script>
</html>
<?php $db->disconnect(); ?>