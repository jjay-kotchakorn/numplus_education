<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
global $db;

// d($_SESSION);
// $url_site_wr = get_config("elearning-url-site-wr");
// $url_site_elearning = get_config("elearning-url-site-elearning");

$_GET['p']="index.php";

if ( !empty($_SESSION["msg"]["success"]) ) {
	$msg_success = true;
	unset($_SESSION["msg"]["success"]);
}

$q="SELECT config_id, name, data_value FROM config WHERE name='elearning-url-site-wr' OR name='elearning-url-site-elearning'";
$rs = $db->get($q);

// d($rs);
foreach ($rs as $key => $v) {
	if ( $v["name"]=='elearning-url-site-wr' ) {
		$url_site_wr = $v["data_value"];
	}
	if ( $v["name"]=='elearning-url-site-elearning' ) {
		$url_site_elearning = $v["data_value"];
	}

}//end loop $v

?>
<link rel="stylesheet" type="text/css" href="js/bootstrap.summernote/dist/summernote.css" />
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="row">
				  <div class="col-md-12">			      
					<div class="block-flat">
					  <div class="header">							
					  	<ol class="breadcrumb">
							<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>');">หน้าหลัก</a></li>
							<li class="active">E-learning setting</li>
						</ol>
					  </div>
					  <div class="content">
						  <form id="frmMain" name="frmMain" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="update-elearning-setting.php">	
						  <input type="hidden" name="p" value="elearning-setting" placeholder="">					
						  <div class="col-sm-12">		
						  	<div class="form-group row">
						  		<span style="font-size: 20px;" class="left red">* ใช้สำหรับทดสอบการเชื่อมต่อระหว่างระบบ WR และ ระบบ E-learning ด้วย API ผ่านทาง URL</span>
						  	</div>	 

							<div class="form-group row">	                			                
								<label class="col-sm-2 control-label">URL ของระบบ WR</label>
								<div class="col-sm-7">
								  <input class="form-control" name="url_site_wr" id="url_site_wr" required="" placeholder="" type="text" value="<?php echo $url_site_wr;?>">
								</div>			                			                
								
							</div>				                
							<div class="form-group row">	                			                
								<label class="col-sm-2 control-label">URL ของระบบ E-learning</label>
								<div class="col-sm-7">
								  <input class="form-control" name="url_site_elearning" id="url_site_elearning"  placeholder="" type="text" required="" value="<?php echo $url_site_elearning;?>">
								</div>	
<!-- 
									<label class="col-sm-2 control-label">สถานะ</label>
									<div class="col-sm-2">
										<select name="active" id="active" class="form-control required">
										  <option selected="selected" value="T">แสดง</option>
										  <option value="F">ไม่แสดง</option>
										</select>
									</div>
									 -->
							</div>

							<div class="form-group row">	                			                
								<label class="col-sm-2 control-label">URL สำหรับส่งผลกิจกรรมมาที่ระบบ WR</label>
								<span class="red" style="font-size: 14px;"><?php echo $url_site_wr."/result/"; ?></span>
<!-- 								
							<div class="col-sm-7">
							  <input class="form-control" name="url_site_wr" id="url_site_wr" required="" placeholder="" type="text" value="<?php echo $url_site_wr;?>">
							</div>			                			                
								 -->							
							</div>
<!-- 							
<div class="form-group row">
  <label class="col-sm-1 control-label">รายละเอียด</label>
  <div class="clear"></div><br>
  	<div class="col-sm-12">
  		<textarea id="detail" class="summernote" name="detail" placeholder="Description"></textarea>
  	</div>
</div>
 -->
						  </div>
						  <div class="clear"></div>
						  <div class="form-group row" style="padding-left:10px;">
								<div class="col-sm-12">
									<button type="button" class="btn btn-primary" onClick="ckSave()">Save changes</button>
									<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p'] ?>');">Cancel</button>
								</div>
						  </div>
						</form>
					  </div>
					</div>
					
				  </div>

				</div>


				<div class="container" style="background-color: #b9eddb">
				  	<form action="" method="post" accept-charset="utf-8">
				  		<div class="form-group row">	                			                
							<label class="col-sm-4 control-label">URL สำหรับทดสอบส่งค่าไปยัง Server ปลายทาง</label>
							<div class="col-sm-7">
							  <input class="form-control" name="url_test_api" id="url_test_api" required="" placeholder="" type="text" value="">
							</div>			                			                
							
						</div>
				  		<div class="form-group row" style="padding-left:10px;">
								<div class="col-sm-12">
									<button type="button" class="btn btn-primary" onClick="send_test_api()">ทดสอบ</button>
								</div>
						  </div>			  		
				  	</form>
				  </div>


		</div>
	</div> 
</div>
<?php include_once ('inc/js-script.php'); ?>

<script type="text/javascript" src="js/bootstrap.summernote/dist/summernote.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
   $("#frmMain").validate();

   // var news_id = "<?php //echo $_GET["news_id"]?>";
   var msg_success = "<?php echo $msg_success;?>";
   if (msg_success) {
   	$.gritter.removeAll({
        after_close: function(){
          $.gritter.add({
          	position: 'center',
	        title: 'success',
	        text: 'บันทึกสำเร็จ',
	        class_name: 'success'
	      });
        }
    });
   }else{

   }//end else
        
 });
   


function ckSave(id){
  onCkForm("#frmMain");
 var tmp = $('#detail').code();
 $('#detail').val(tmp);
  $("#frmMain").submit();
}


function send_test_api(){
	var url = $("#url_test_api").val();
	$.ajax({
		"type": "POST",
		"async": false, 
		"url": "test-curl.php",
		"data": {'url': url}, 
		"success": function(data){	
			// console.log(data);
			alert(data);			      							 
		}
	});
}//end func

</script>