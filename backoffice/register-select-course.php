<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
global $db, $SECTIONID, $COURSETYPEID;
$con_section_id = ($SECTIONID>0) ? " and a.section_id={$SECTIONID}" : "";
$con_coursetype_id = ($COURSETYPEID>0) ? " and a.coursetype_id={$COURSETYPEID}" : "";
$section = datatype(" and a.active='T' {$con_section_id}", "section", true);
$coursetype = datatype(" and a.active='T' {$con_coursetype_id}", "coursetype", true);
$type = $_GET["type"];
$str = "เลือกหลักสูตร/วิชา";
?>
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="col-sm-12">
				<div class="content block-flat ">
						<div class="header">						
						<ol class="breadcrumb">
							<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>');">หน้าหลัก</a></li>							
							<li class="active"><?php echo $str; ?></li>
						</ol>
						<br>
							<div class="form-group row">
								<label class="col-sm-2 control-label">ประเภทใบอนุญาต/คุณวุฒิ</label>
								<div class="col-sm-3">
									<select name="section_id" id="section_id" class="form-control" onchange="reCall();">
										<option value="">---- เลือก ----</option>
										<?php foreach ($section as $key => $value) {
											$id = $value['section_id'];
											if($SECTIONID>0 && $SECTIONID!=$id) continue;
											if($SECTIONID>0 && $SECTIONID==$id){
												$select = "selected";
											}else{
												$select = "";
											}
											$name = $value['name'];
											echo  "<option {$select} value='$id'>$name</option>";
										} ?>

									</select>
								</div>                
								<label class="col-sm-2 control-label">ประเภทหลักสูตร</label>
								<div class="col-sm-2">
									<select name="coursetype_id" id="coursetype_id" class="form-control" onchange="reCall();">
										<option value="">---- เลือก ----</option>
										<?php foreach ($coursetype as $key => $value) {
											$id = $value['coursetype_id'];
											if($COURSETYPEID>0 && $COURSETYPEID==$id){
												$select = "selected";
											}else{
												$select = "";
											}
											$name = $value['name'];
											echo  "<option {$select} value='$id'>$name</option>";
										} ?>

									</select>
								</div>
								<label class="col-sm-1 control-label">สถานะ</label>
								<div class="col-sm-2">
									<select name="active" id="active" class="form-control" onchange="reCall();">
										<option selected="selected" value="T">แสดง</option>
										<option value="F">ไม่แสดง</option>
									</select>
								</div>                                           
							</div> 
							<?php if($type!="childlist"): ?>
							<div class="form-group row">
								<label class="col-sm-2 control-label">การแสดงผล</label>
								<div class="col-sm-3">
									<select name="childlist" id="childlist" class="form-control" onchange="reCall();">
										<option selected="selected" value="all">แสดงทั้งหมด</option>
										<option value="parent">หลักสูตรหลัก</option>
										<option value="child">หลักสูตรย่อย</option>
									</select>
								</div>                                           
							</div>
							<?php endif; ?> 
						</div>
					<table id="tbCourse" class="table" style="width:100%">
						  <thead>
							  <tr>
								  <th width="8%">ลำดับ</th>
								  <th width="8%">รหัส</th>
								  <th width="40%">ชื่อหลักสูตร</th>
								  <th width="17%">ประเภทใบอนุญาต/คุณวุฒิ</th>
								  <th width="13%">วันที่</th>
								  <th width="13%">ประเภทหลักสูตร</th>
								  <th width="14%">Manage</th>
							  </tr>
						  </thead>   
						<tbody>
						</tbody>
					</table>
					<div class="clear"></div>
				</div>
			</div>

		</div>
	</div> 
</div>
<?php include ('inc/js-script.php') ?>

<script type="text/javascript">
$(document).ready(function() {
	var get_type = "<?php echo $_GET["type"]; ?>";
	if(get_type=="childlist" || get_type=="course_order") $("#add").hide();
	var oTable;
	listItem();	
	$("#section_id").change(function(event) {
		var id = $(this).val();
		getDropDown('#coursetype_id', id, 'coursetype', 'data/coursetype.php')
	}); 
});

function listItem(){
   var get_type = "<?php echo $_GET["type"]; ?>";
   var url = "data/courselist.php";
   oTable = $("#tbCourse").dataTable({
	   "sDom": 'T<"clear">lfrtip',
	   "oLanguage": {
   	   "sInfoEmpty": "",
   		"sInfoFiltered": ""
						  },
		"oTableTools": {
			"aButtons":  ""
		},
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": url,
		"sPaginationType": "full_numbers",
		"aaSorting": [[ 0, "desc" ]],
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			aoData.push({"name":"section_id","value":$("#section_id").val()});			
			aoData.push({"name":"coursetype_id","value":$("#coursetype_id").val()});			
			aoData.push({"name":"active","value":$("#active").val()});			
			aoData.push({"name":"type","value":get_type});
			if(get_type!="childlist"){
				aoData.push({"name":"childlist","value":$("#childlist").val()});	
			}			
			$.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});
		}
   }); 
}
function select_course(id){
    var url = "index.php?p=<?php echo $_GET["p"];?>&course_id="+id+"&type=select-course-detail";
   redirect(url);
}

function reCall(){
	oTable.fnClearTable( 0 );
	oTable.fnDraw();
}


</script>