<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
include_once "./share/member.php";
global $db, $SECTIONID, $COURSETYPEID;
$apay = datatype(" and a.active='T'", "pay_status", true);
$con_section_id = ($SECTIONID>0) ? " and a.section_id={$SECTIONID}" : "";
$con_coursetype_id = ($COURSETYPEID>0) ? " and a.coursetype_id={$COURSETYPEID}" : "";
$section = datatype(" and a.active='T' {$con_section_id}", "section", true);
if($con_coursetype_id)
	$coursetype = datatype(" and a.active='T' {$con_coursetype_id}", "coursetype", true);

$educationlevel = datatype(" and a.active='T'", "educationlevel", true);
$province = datatype(" and a.active='T'", "province", true);
$receipttype = datatype(" and a.active='T'", "receipttype", true);
$university = datatype_university(true);

$error = $_SESSION["error"]["msg"];
unset($_SESSION["error"]["msg"]);
$success = $_SESSION["success"]["msg"];
unset($_SESSION["success"]["msg"]);
?>
<link rel="stylesheet" type="text/css" href="js/jquery.nanoscroller/nanoscroller.css" />
<link href="js/jquery.icheck/skins/square/blue.css" rel="stylesheet">

<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="col-sm-12">
				<div class="content block-flat ">
						<div class="header">              
							<ol class="breadcrumb">
								<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>');">หน้าหลัก</a></li>
								<li class="active">หน้าใบเสร็จรับเงินที่ถูกยกเลิก</li>
							</ol>
						</div>
										
						<div class="header">
<!-- 							<div class="form-group row">
								<label class="col-sm-2 control-label">ประเภทใบอนุญาต/คุณวุฒิ</label>
								<div class="col-sm-3">
									<select name="section_id" id="section_id" class="form-control" onchange="reCall();">
										<option value="">---- เลือก ----</option>
										<?php foreach ($section as $key => $value) {
											$id = $value['section_id'];
											if($SECTIONID>0 && $SECTIONID!=$id) continue;
											if($SECTIONID>0 && $SECTIONID==$id){
												$select = "selected";
											}else{
												$select = "";
											}
											$name = $value['name'];
											echo  "<option {$select} value='$id'>$name</option>";
										} ?>

									</select>
								</div>                
								<label class="col-sm-2 control-label">ประเภทหลักสูตร</label>
								<div class="col-sm-2">
									<select name="coursetype_id" id="coursetype_id" class="form-control" onchange="reCall();">
										<option value="">---- เลือก ----</option>
										<?php foreach ($coursetype as $key => $value) {
											$id = $value['coursetype_id'];
											if($COURSETYPEID>0 && $COURSETYPEID==$id){
												$select = "selected";
											}else{
												$select = "";
											}
											$name = $value['name'];
											echo  "<option {$select} value='$id'>$name</option>";
										} ?>

									</select>
								</div>
								<label class="col-sm-1 control-label" style=" padding-right:0px;">ช่องทางชำระ</label>
								<div class="col-sm-2">
									<select name="sPay" id="sPay" class="form-control" onchange="reCall();">
										<option selected="selected" value="">แสดงทั้งหมด</option>
										<option value="paysbuy">paysbuy</option>
										<option value="bill_payment">Bill-payment</option>
										<option value="at_asco">เช็ค/เงินโอน</option>
										<option value="at_ati">ชำระเงินสดผ่าน ATI</option>
									</select>
								</div>                                          
							</div> 							
 -->
							<div class="form-group row">

								<label class="col-sm-1 control-label"  style=" padding-right:0px;">วันที่ลงทะเบียน</label>
								<div class="col-sm-2"  style="padding-left:0px; padding-right:0px;">
									<input class="form-control" name="date_start" id="date_start" onblur="reCall();" placeholder="วันที่ลงทะเบียน" type="text">
								</div>                                          
								<label class="col-sm-1 control-label"  style=" padding-right:0px;"> ถึงวันที่ </label>
								<div class="col-sm-3"  style="padding-left:0px; padding-right:0px;">
									<input class="form-control" name="date_stop" id="date_stop" onblur="reCall();" placeholder=" ถึงวันที่ " type="text">
								</div> 

                                <label class="col-sm-1 control-label" style=" padding-right:0px;">ช่องทางชำระ</label>
								<div class="col-sm-2">
									<select name="sPay" id="sPay" class="form-control" onchange="reCall();">
										<option selected="selected" value="">แสดงทั้งหมด</option>
										<option value="paysbuy">paysbuy</option>
										<option value="bill_payment">Bill-payment</option>
										<option value="at_asco">เช็ค/เงินโอน</option>
										<option value="at_ati">ชำระเงินสดผ่าน ATI</option>
										<option value="mpay">mPAY</option>
									</select>
								</div>        

								<label class="col-sm-1 control-label">
									<a href="#" class="btn" onclick="reCall();"><i class="fa fa-search">&nbsp;</i></a>&nbsp;
									<!-- <button onclick="clearSearch();" id="ok" class="btn btn-success" type="button">Clear</button> -->
								</label>  								   
							</div>
							
						</div>
					<form id="frmMain" name="frmMain" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="">
					<table id="tbCourse" class="table" style="width:100%">
						  <thead>
							  <tr>
								  <th style="text-align:center" width="5%">ลำดับ</th>
								  <th style="text-align:left" width="10%">เลขที่ใบเสร็จ</th>
								  <th style="text-align:left" width="10%">รหัสโครงการ</th>
								  <th width="13%">ชื่อ-นามสกุล</th>
								  <th style="text-align:center" width="">ออกใบเสร็จในนาม</th>
								  <th style="text-align:center" width="11%">วันที่ลงทะเบียน</th>
								  <th style="text-align:center" width="12%">ช่องทางชำระ</th>						
							  </tr>
						  </thead>   
						<tbody>
						</tbody>
					</table>
					</form>
					<div class="clear"></div>
					<br>
					<div class="filters">         
						<div class="btn-group pull-right">
							<a class="btn btn-small " href="#" onclick="printtax();"><i class="fa fa-print"></i> สรุปยอดซื้อ-ขายหลักสูตรของใบเสร็จที่ถูกยกเลิก</a>
						</div>    
					</div>
					<div class="clear"></div>
				</div>
			</div>

		</div>
	</div> 
</div>
<form id="print_form" action="" method="post">
	<input type="hidden" id="register_ids" name="register_id">;		
</form>
<?php include ('inc/js-script.php') ?>

<script type="text/javascript">
$(document).ready(function() {
	var get_type = "<?php echo $_GET["type"]; ?>";
	if(get_type=="childlist" || get_type=="course_order") $("#add").hide();
	var oTable;
	listItem();	
    $("#date_start").datepicker({language:'th-th',format:'dd-mm-yyyy'});
    $("#date_stop").datepicker({language:'th-th',format:'dd-mm-yyyy'});    
	$("#section_id").change(function(event) {
		var id = $(this).val();
		getDropDown('#coursetype_id', id, 'coursetype', 'data/coursetype.php')
	});

});
$("#checkAll").click(function(){
 		$("#tbCourse tbody td :checkbox").prop('checked', $(this).prop("checked"));
	}); 
function listItem(){
   var get_type = "<?php echo $_GET["type"]; ?>";
   var url = "data/report-receipt-cancel-list.php";
   oTable = $("#tbCourse").dataTable({
	   "sDom": 'T<"clear">lfrtip',
	   "oLanguage": {
   	   "sInfoEmpty": "",
   		"sInfoFiltered": ""
						  },
		"oTableTools": {
			"aButtons":  [	
/*			{
				"sExtends": "xls",
				"sButtonText": "Export ข้อมูลผู้มีสิทธิ์"
			}*/
			]
		},
		 "bSort": false ,
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": url,
		"sPaginationType": "full_numbers",
		"aaSorting": [[ 0, "desc" ]],
		'iDisplayLength': 50,
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			// aoData.push({"name":"section_id","value":$("#section_id").val()});			
			// aoData.push({"name":"coursetype_id","value":$("#coursetype_id").val()});			
			// aoData.push({"name":"member_id","value":$("#member_id").val()});			
			// aoData.push({"name":"course_detail_id","value":$("#course_detail_id").val()});						
			aoData.push({"name":"date_start","value":$("#date_start").val()});			
			aoData.push({"name":"date_stop","value":$("#date_stop").val()});			
			//aoData.push({"name":"status","value":"3,5,6"});			
			aoData.push({"name":"pay_by","value":$("#sPay").val()});			
			//aoData.push({"name":"active","value": "T"});			
			aoData.push({"name":"type","value":"register-receipt"});
			aoData.push({"name":"pay_status","value":8});		
			$.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});
			//console.log(aoData);
		}
   });
   /*$('#tbCourse_filter input').unbind();
   $('#tbCourse_filter input').bind('keyup', function(e) {
       if(e.keyCode == 13) {
        oTable.fnFilter(this.value);  
        console.log(this.value); 
    }
   }); */
}

function reCall(){
	oTable.fnClearTable( 0 );
	oTable.fnDraw();
}

//send data to receipt-tax.php
function printtax(){
	var sec_id = $("#section_id").val();
	var cos_type_id = $("#coursetype_id").val();
	var pay_by = $("#sPay").val();
	//var cos_dtail = $("#course_detail_name").val();
	//var name = $("#membername").val();
	var date_start = $("#date_start").val();
	var date_stop = $("#date_stop").val();

   if ( (date_start == '' || date_start == null) || (date_stop == '' || date_stop == null) ) {
   	alert("กรุณาเลือกช่วงเวลา"); 
	location.href = "index.php?p=report&type=receipt-cancel#";
   }else{
   	var url = "report-receipt-cancel.php?pay_by="+pay_by+"&date_start="+date_start+"&date_stop="+date_stop;
   	window.open(url,'_blank');
   };
}

</script>