<?php 
/*
if (! isset($_SERVER['HTTPS']) or $_SERVER['HTTPS'] == 'off' ) {
    $redirect_url = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    header("Location: $redirect_url");
    exit();
}
*/
include_once "./share/authen.php";
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/menu.php";
?>
<!DOCTYPE html>
<html lang="en">
<?php include ('inc/header.php'); ?>
<body>
	<?php if($is_login) include ('inc/top-menu.php'); ?>
	<?php

	if($is_login){
		$args = array();
		foreach ($menu_access as $key => $value) {
			$t = explode("&", $key);
			$args[$t[0]] = $value;
		}
		//$args = $menu_access;
		$args['main'] = 'dashboard.php';
	}else{
		$args = array(
			'default' => 'login.php',
			'forget-password' => 'forgot-password.php',
			'import' => 'import.php'
			);
	}

	$call_page = isset($_GET['p']) ? $_GET['p'] : "";
	if(!$call_page || !($args[$call_page])) 
		$call_page = ($is_login) ? "main" : "default";


	?>
	<?php 
		  if(file_exists($args[$call_page])) 
		  	render_pages($args[$call_page]); 
		  else 
		  	include_once ('inc/js-script.php');
	?>
	<?php include ('inc/footer.php') ?>
</body>
</html>
<?php $db->disconnect(); ?>