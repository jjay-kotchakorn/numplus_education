<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./send-mail/send-mail.php";

//d($_POST); die();

function write_log_in_local($file_name="", $msg="", $write_mode="a"){
	$str = $msg;	
	$date_now = date('Y-m-d H:i:s');
	$date_log = date('Y-m-d');
	$log_name = "./log/".$file_name."_".$date_log.".log";
	$file = fopen($log_name, $write_mode);
	$str_txt = $date_now."|".$str."\r\n";
	fwrite($file, $str_txt);
	fclose($file);
}//end func


if (!empty($_POST)){

$display_date = date("Y-m-d H:i:s");
$name = $_POST["name"];
// $sent_to_email = $_POST["email"];
$sent_to_email = "";
$phone = $_POST["phone"];
$email = $_POST["email"];
$display_subject ="ติดต่อเรื่อง ".$_POST["display_subject"];
$display_message = $_POST["display_message"];

}

// $dear = "เรียน คุณ ";
// $toppic = $dear.$name;

//$message = "" ;

$message = "เรียน คุณ".$_POST["fname_th"]." ".$_POST["lname_th"]."<br/> ";
$html = '
		<table class="deviceWidth" align="left" border="0" cellpadding="0" cellspacing="0" width="800"> 
			<tbody>
				<tr>
					<td class="center" style="font-size: 12px; color: #687074; font-weight: bold; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 10px 0px; " bgcolor="#ffffff">
						'.$message.'
						การสมัครสมาชิกของคุณเรียบร้อยแล้ว 
					</td>

				</tr>
				<tr>
					<td class="center" style="font-size: 12px; color: #687074; font-weight: bold; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 10px 0px 0px; " bgcolor="#ffffff">
						ท่านสามารถเข้าสู่ระบบ โดยใช้ข้อมูลข้างต้นโดย <a style="text-decoration: none; color: #27d7e7;" href="https://register.ati-asco.org/index.php">คลิกที่นี่</a> 
					</td>
				</tr>
				<tr>
					<td class="center" style="font-size: 12px; color: #687074; font-weight: bold; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 10px 0px; " bgcolor="#ffffff">
						สถาบันฝึกอบรม สมาคมบริษัทหลักทรัพย์ (ATI)<br>
						ส่วนฝึกอบรมติดต่อ: Training@ati-asco.org โทร 02-264-0909 ต่อ 223<br>
						ส่วนทดสอบผู้แนะนำการลงทุนฯติดต่อ: Examination@ati-asco.org โทร 02-264-0909 ต่อ 203-207<br>
						ชมรมวาณิชธนกิจ (IB Club) ส่วนฝึกอบรมและทดสอบติดต่อ: fatraining@asco.or.th โทร 02-264-0909 ต่อ 124,125<br>
					</td>
				</tr>
			</tbody>
		</table>
';	


$resp = sendmail_func( $sent_to_email , "", $display_subject, $html);


$date = date('Y-m-d');
$str_txt = "";
$str_txt .= "name={$name}|detail_mail={$html}";
write_log_in_local("smcard-send-mail-{$date}", $str_txt);



// $url = "../contact.php";
// // echo $url; die();
// header( "location: $url" );

?>
