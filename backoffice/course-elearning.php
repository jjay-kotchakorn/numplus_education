<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
global $db, $SECTIONID, $COURSETYPEID;

$error = $_SESSION["error"]["msg"];
unset($_SESSION["error"]["msg"]);
$success = $_SESSION["success"]["msg"];
unset($_SESSION["success"]["msg"]);

$course_id = $_GET["course_id"];

$q = "select  * from course where course_id=$course_id";
$single_course = $db->rows($q);
$str = "";
if($single_course){
	$inhouse = $single_course["inhouse"];
	$str = $single_course["title"];
}else{
	$str = "เพิ่มหลักสูตร";
}
$all_course = array();
?>
<link rel="stylesheet" type="text/css" href="js/bootstrap.summernote/dist/summernote.css" />
<link rel="stylesheet" type="text/css" href="js/jquery.nanoscroller/nanoscroller.css" />
<link href="js/jquery.icheck/skins/square/blue.css" rel="stylesheet">
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="row">
				<div class="col-md-12">           
					<div class="block-flat">
						<div class="header">              
							<ol class="breadcrumb">
								<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>&type=elearning', true);">หน้าหลัก</a></li>
								<li class="active"><?php echo $str; ?></li>
							</ol>
						</div>

						<?php 
							$stop = date('Y-m-t',strtotime('today'));
							$start = date("Y-m-01");
							$dateStart = ($_POST["date_start"]) ? thai_to_timestamp($_POST["date_start"]) :  $start;
							$dateStop =  ($_POST["date_stop"]) ? thai_to_timestamp($_POST["date_stop"]) : $stop;
							if ($dateStart || $dateStop) {
							    if (!$dateStart && $dateStop)
							        $dateStart = $dateStop;
							    if (!$dateStop && $dateStart)
							        $dateStop = $dateStart;
							    $t = $dateStart;
							    if ($dateStart > $dateStop) {
							        $dateStart = $dateStop;
							        $dateStop = $t;
							    }
							}
						 ?>	
						 <div class="content">	
							 <form id="frmSearch" name="frmSearch" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="">					 
								 <div class="form-group alert-info" style="padding: 30px;"> 
								 	<label class="col-sm-1 control-label"  >วันที่เริ่ม</label>
								 	<div class="col-sm-2"  >
								 		<input class="form-control" name="date_start" value="<?php echo revert_date($dateStart); ?>" id="date_start"  placeholder="วันที่เริ่ม" type="text">
								 	</div>                                          
								 	<label class="col-sm-1 control-label"  >วันที่สิ้นสุด</label>
								 	<div class="col-sm-2" >
								 		<input class="form-control" name="date_stop" id="date_stop" value="<?php echo revert_date($dateStop); ?>"  placeholder="วันที่สิ้นสุด" type="text">
								 	</div>
								 	<label class="col-sm-1 control-label">PUB / INH</label>
								 	<div class="col-sm-2">
								 		<select name="check_inh" id="check_inh" class="form-control">
								 			<option selected="selected"  value="">--เลือก--</option>
								 			<option value="pub">PUB</option>
								 			<option value="inh">INH</option>
								 		</select>
								 	</div>   
								 	<button class="btn" id="filter_date"> <i class="fa fa-search"></i></button>
								 </div> 
							 </form>
						 </div>						
							<div class="content" id="section-contents">
								<div class="col-sm-12">                   
									<div class="form-group row"> 
										<label class="col-sm-1 control-label" style="width:118px;text-align: left; font-size: 16px; font-weight:normal;">รหัสหลักสูตร</label>
										<div class="col-sm-2" style="width:150px;">
											<input class="form-control" id="code" name="code" required="" placeholder="รหัสหลักสูตร" type="text">
										</div>
										<label class="col-sm-9 control-label" style="padding-right:10px; text-align: left; font-size: 16px; font-weight:normal;" ><span id="title"></span> [ <span id="section_name"></span> ]  [ <span id="coursetype_name"></span>]</label> 
										<div class="clear"></div>
										<hr><?php 
											$q = "select * from course where parent_id={$course_id}";
											$children_course = $db->get($q);
											$show = "T";
											if(!empty($children_course) && count($children_course)>0){
											}else{
												$q = "select * from course where course_id={$course_id}";
												$children_course = $db->get($q);
												$show = "F";
											}
											//$runNo = 1;
											if(!empty($children_course) && count($children_course)>0){
												foreach ($children_course as $kc => $vc) { 
														$course_id = $vc["course_id"];
														$all_course[$course_id] = $course_id;
														$is_ic = ($vc["section_id"]==1 && $vc["coursetype_id"]==2) ? true : false;
														if($show=="T"){
													?>
													<label class="col-sm-1 control-label" style="width:118px;text-align: left; font-size: 16px; font-weight:normal;">รหัสหลักสูตร</label>
													<div class="col-sm-2" style="width:150px;"><input class="form-control" value="<?php echo $vc["code"]; ?>" type="text"></div>
													<label class="col-sm-12 control-label" style="margin-top:10px;padding-right:10px; text-align: left; font-size: 16px; font-weight:normal;" ><?php echo $vc["title"]; ?></label>
													<div class="clear"></div>
													<br>
													<?php } ?>	
													 <div class="col-md-4" id="get_data_sections_<?php echo $course_id;  ?>">
													 	<div class="course_part_wraper">
															<?php // echo gen_elearning_input_line($vc); ?>
													 	</div>
														<form id="frm_data_sections_<?php echo $course_id;  ?>">															
															 <div class="header">
															 	<h5></h5>
															 	<div class="course_detail_data_<?php echo $course_id;  ?>"></div>
															 	<input type="hidden" name="current_course_detail_id" id="current_course_detail_id">
															 	<input type="hidden" name="current_course_id" id="current_course_id" value="<?php echo $course_id;  ?>">
															 </div>
															 <div class="course_settings">
															 	<table class="table-striped">
															 		<tr>
															 			<td colspan="2" class="text-right"> <input type="checkbox" name="transfer_status" value="T"></td>
															 			<td colspan="5"> ส่งค่าไปยังระบบ E-Learning</td>
															 		</tr>
															 		<tr>
															 			<td colspan="2"  class="text-right"> <input type="radio" name="expire_status" class="date" id="expire_status1" value="1"></td>
															 			<td colspan="5"> หมดอายุวันที่&nbsp;&nbsp;<input name="expire_date" id="expire_date<?php echo $course_id; ?>" value="" type="text" style="width:100px;margin-left: 5px;"></td>
															 		</tr>
															 		<tr>
															 			<td colspan="2"  class="text-right"> <input type="radio" name="expire_status" id="expire_status2" value="2"></td>
															 			<td colspan="5"> หมดอายุ&nbsp;<input value="" type="text" id="num_expire_day" name="num_expire_day" style="width:50px;margin-left: 30px;margin-right: 2px;"> วัน นับจากวันที่ชำระเงิน / วันที่วางบิล / ยกเว้นค่าธรรมเนียม</td>
															 		</tr>
															 		<tr>
															 			<td colspan="2"  class="text-right"> <input type="radio" name="expire_status" id="expire_status3" value="3"></td>
															 			<td colspan="5"> ไม่มีวันหมดอายุ</td>
															 		</tr>
															 	</table>
															 	<div class="footer" style="padding-top:20px; margin-left: -3px;">
																	<button type="button" class="btn btn-primary" onClick="ckSave(<?php echo $course_id;  ?>)">บันทึกรายการเดียว</button>
																	<button type="button" class="btn btn-success" onClick="ckSave(<?php echo $course_id;  ?>, 'ckbox')">บันทึกหลายรายการ</button>
																	<?php /*																	<button type="button" class="btn btn-success pull-right" onClick="ckSaveCourse(<?php echo $course_id;  ?>)">บันทีกรหัสหลักสูตร</button>
																	*/ ?>
															 		
															 	</div>
															 </div>
														</form>
													 </div>
													 <div class="col-md-8">					
															<table class="table table-striped" id="tbList<?php echo $course_id ?>">
																<thead>
																	<tr class="alert alert-success" style="font-weight:bold;">
																		<td width="5%" class="text-center"><input type="checkbox"  onClick="checkAll(this, <?php echo $course_id; ?>)">&nbsp;&nbsp;&nbsp;&nbsp;</td>
																		<td width="11%" class="center">วัน</td>
																		<td width="9%" class="center">เวลา</td>
																		<td width="8%" class="center">สถานที่</td>
																		<td width="20%" class="center">ข้อมูลสถานที่สอบ/อบรม</td>
																		<td width="8%" class="center">เปิดรับสมัคร</td>
																		<td width="8%" class="center">ปิดรับสมัคร</td>
																		<td width="3%" class="center"><a href="#" title=" ส่งค่าไปยังระบบ E-Learning">สถานะ</a></td>
																		<td width="10%" class="center">วันหมดอายุ</td>
																		<td width="5%" class="center">เลือก</td>
																	</tr>
																</thead>
																<tbody>
																	<?php 
																	$select_year = date("Y");
																	$select_month = date("m");
																	$sWhere = ($dateStart && $dateStop) ? " and a.date>='$dateStart' and a.date<='$dateStop'" : "";
																	if($_POST["check_inh"]=="pub") $sWhere .= " and a.inhouse='F'";
																	if($_POST["check_inh"]=="inh") $sWhere .= " and a.inhouse='T'";	
																	$con = ($is_ic) ? " and a.section_id=1 and coursetype_id=2 " : " and a.course_id=$course_id";
																	$r = get_course_detail(" and a.active='T' $con  $sWhere ", "", true);
																	$runNo = 1; 
																	if($r){
																		foreach($r as $k=>$v){
																			$course_detail_data = $runNo;
																			$course_detail_data .= ". วัน ";
																			$course_detail_data .= $v["day"];
																			$course_detail_data .= " ";
																			$course_detail_data .= revert_date($v["date"]);
																			$course_detail_data .= " ";
																			$course_detail_data .=  "เวลา ".$v["time"];
																			$course_detail_data .= " ";
																			$course_detail_data .=  $v["address"];
																			$course_detail_data .= " ";
																			$course_detail_data .=  $v["address_detail"];
																			$q = "select * from elerning_map_course_detail where course_id={$course_id} and course_detail_id={$v["course_detail_id"]}";
																			$array_data = $db->rows($q);
																			$t = '';
																			$txt_exp = '';
																			if($array_data){
																				$txt_exp = $array_data["remark"];
																				if($array_data["transfer_status"]=="T"){
																					$t = '<span class="alert-success"><i class="fa fa-check"></i></span>';
																				}else{
																					$t = '<span class="alert-danger"><i class="fa fa-times "></i></span>';
																				}
																			}
																		    ?>
																			<tr style="cursor:pointer;" class="tr-subheader" id="tr<?php echo $course_id ?>_<?php echo $v["course_detail_id"] ?>" valign="middle">
																				<td class="center">
																					<input type="checkbox" id="ckBox" value="" style="float:none;cursor: pointer;"> 
																					<?php echo $runNo; ?>
																					<input type="hidden" name="course_detail_data_<?php echo $v["course_detail_id"] ?>" id="course_detail_data" value='<?php echo $course_detail_data; ?>'>
																					<input type="hidden" name="course_detail_id" id="course_detail_id" value="<?php echo $v["course_detail_id"] ?>">
																				</td>
																				<td class="center"><strong style="font-size:15px;"><?php echo $v["day"]."."; ?></strong> <?php echo revert_date($v["date"]); ?></td>
																				<td  class="center"><?php echo $v["time"]; ?></td>
																				<td  class="center"><?php echo $v["address"]; ?></td>
																				<td  class="center"><?php echo $v["address_detail"]; ?></td>													
																				<td  class="center"><?php echo revert_date($v["open_regis"]); ?></td>
																				<td  class="center"><?php echo revert_date($v["end_regis"]); ?></td>
																				<td class="text-center" id="transfer_status"><?php echo $t; ?></td>
																				<td id="exp" class="text-center" ><?php echo $txt_exp; ?></td>					
																				<td style="text-align: center;" onClick="singleKey(this, <?php echo $course_id; ?>);"><i class="fa fa-file-o"></i></td>					
																			</tr>
																			<?php
																			$runNo++;
																		} 	
																	}
																	$sWhere = ($dateStart && $dateStop) ? " and b.date>='$dateStart' and b.date<='$dateStop'" : "";
																	if($_POST["check_inh"]=="pub") $sWhere .= " and b.inhouse='F'";
																	if($_POST["check_inh"]=="inh") $sWhere .= " and b.inhouse='T'";														

																	$con = "and a.course_id={$course_id} $sWhere";
																	
																    $r = get_course_detail_list($con,  true);
																    if($r && !$is_ic){
																		//$runNo = 1; 
																		foreach($r as $k=>$v){
																			$course_detail_data = $runNo;
																			$course_detail_data .= ". วัน ";
																			$course_detail_data .= $v["day"];
																			$course_detail_data .= " ";
																			$course_detail_data .= revert_date($v["date"]);
																			$course_detail_data .= " ";
																			$course_detail_data .=  "เวลา ".$v["time"];
																			$course_detail_data .= " ";
																			$course_detail_data .=  $v["address"];
																			$course_detail_data .= " ";
																			$course_detail_data .=  $v["address_detail"];
																			$q = "select * from elerning_map_course_detail where course_id={$course_id} and course_detail_id={$v["course_detail_id"]}";
																			$array_data = $db->rows($q);
																			$t = '';
																			$txt_exp = '';
																			if($array_data){
																				$txt_exp = $array_data["remark"];
																				if($array_data["transfer_status"]=="T"){
																					$t = '<span class="alert-success"><i class="fa fa-check"></i></span>';
																				}else{
																					$t = '<span class="alert-danger"><i class="fa fa-times "></i></span>';
																				}
																			}
																		    ?>
																			<tr style="cursor:pointer;" class="tr-subheader" id="tr<?php echo $course_id ?>_<?php echo $v["course_detail_id"] ?>" valign="middle">
																				<td class="center">
																					<input type="checkbox" id="ckBox" value="" style="float:none;cursor: pointer;"> 
																					<?php echo $runNo; ?>
																					<input type="hidden" name="course_detail_data_<?php echo $v["course_detail_id"] ?>" id="course_detail_data" value='<?php echo $course_detail_data; ?>'>
																					<input type="hidden" name="course_detail_id" id="course_detail_id" value="<?php echo $v["course_detail_id"] ?>">
																				</td>
																				<td class="center"><strong style="font-size:15px;"><?php echo $v["day"]."."; ?></strong> <?php echo revert_date($v["date"]); ?></td>
																				<td  class="center"><?php echo $v["time"]; ?></td>
																				<td  class="center"><?php echo $v["address"]; ?></td>
																				<td  class="center"><?php echo $v["address_detail"]; ?></td>													
																				<td  class="center"><?php echo revert_date($v["open_regis"]); ?></td>
																				<td  class="center"><?php echo revert_date($v["end_regis"]); ?></td>
																				<td class="text-center" id="transfer_status"><?php echo $t; ?></td>
																				<td id="exp" class="text-center" ><?php echo $txt_exp; ?></td>					
																				<td style="text-align: center;" onClick="singleKey(this, <?php echo $course_id; ?>);"><i class="fa fa-file-o"></i></td>					
																			</tr>
																			<?php
																			$runNo++;
																		} 	
																	}
																	?>
																</tbody>
															</table>
															<br>
													 </div> 
													 <div class="clear"></div><hr>
													 <?php
												}
											}else{ ?>

											<?php  } ?>   
									</div> 
								</div>
							</div>
							<div class="clear"></div>
					</div>
				</div>

			</div>
		</div>

	</div>
</div> 
</div>
<?php include_once ('inc/js-script.php'); ?>
<script type="text/javascript" src="js/bootstrap.summernote/dist/summernote.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		var nowDate = new Date();
		var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);		
		$("#date_start").datepicker({language:'th-th',format:'dd-mm-yyyy'/*,startDate: today*/ });
		$("#date_stop").datepicker({
			language:'th-th',
		 	format:'dd-mm-yyyy'/*,
		 	startDate: today */
		 });
		<?php 
		if($all_course>0){
			foreach ($all_course as $key => $value) {
				?>
				$("#expire_date"+<?php echo $value ?>).datepicker({
					language:'th-th',
				 	format:'dd-mm-yyyy'/*,
				 	startDate: today */
				 });
				<?php
			}
		}

		 ?>		
		var course_id = "<?php echo $_GET["course_id"]?>";
		if(course_id) viewInfo(course_id);		
		$("#filter_date").click(function(event) {
			/* Act on the event */
			$("#frmSearch").submit();
		});
	});


    function checkAll(tag, course_id) {
        $("#tbList"+course_id+" tbody tr :checkbox").each(function() {
            if ($(tag).is(":checked")) {
                $(this).prop('checked', true);
            } else {
                $(this).prop('checked', false);
            }
        });
    }

    function singleKey(tag, course_id) {
        var ids = $(tag).parent().find('#course_detail_id').val();
        var data = $(tag).parent().find('#course_detail_data').val();
        $("#get_data_sections_"+course_id).find('h5').html(data);
        $("#get_data_sections_"+course_id).find('#current_course_detail_id').val(ids);
        disp(ids, course_id) ;
    }
	function viewInfo(course_id){
		if(typeof course_id=="undefined") return;
		getnewsInfo(course_id);
	}
    function disp(ids, course_id) {
        var url = "data/map-course-detail.php";
        var param = "";
        param = param + "&course_id="+course_id;
        param = param + "&course_detail_id="+ids;
		disp_course_elearning(ids, course_id);
        $.ajax({
            "dataType": 'json',
            "type": "POST",
            "url": url,
            "data": param,
            "success": function(data) {
            	if(data){            		
		             var ck = (data.transfer_status=="T") ? true : false;
		             var exp = data.expire_status;
		             $("#frm_data_sections_"+course_id).find("input[name=transfer_status]").prop('checked', ck);
		             if(exp==1){
		            	 $("#frm_data_sections_"+course_id).find("input[name=expire_date]").val(data.expire_datex);
		             }else if(exp==2){
		             	$("#frm_data_sections_"+course_id).find("input[name=num_expire_day]").val(data.num_expire_day);
		            	 $("#frm_data_sections_"+course_id).find("input[name=expire_date]").val("");
		             }else if(exp==3){
		            	 $("#frm_data_sections_"+course_id).find("input[name=expire_date]").val("");
		             }
		            $("#frm_data_sections_"+course_id).find("#expire_status"+exp).prop('checked', true);
            	}
            }
        });
	    
    }
    function disp_course_elearning(course_detail_id, course_id) {
        var url = "data/course-elearning-list.php";
        var param = "";
        param = param + "&course_id="+course_id;
        param = param + "&course_detail_id="+course_detail_id;

        $.ajax({
            "type": "POST",
            "url": url,
            "data": param,
            "success": function(data) {
	             $("#get_data_sections_"+course_id).find(".course_part_wraper").html(data);
            }
        });
    }
	function getnewsInfo(id){
		if(typeof id=="undefined") return;
		var url = "data/courselist.php";
		var param = "course_id="+id+"&single=T";
		dataUrl(url, param,"#section-contents");
	}

	function ckSave(id, ckbox){
		var url = "update-course-elearning.php";
        var param = "";
        param = $("#frm_data_sections_"+id).serialize();
        param = param + "&type=course_detail";

		if(typeof ckbox!="undefined"){
			$("#tbList"+id+" tbody > tr").each(function(index, el) {
				var ck =  $(this).find(":checkbox").is(":checked");
				if(ck){
					var course_detail_id = $(this).find('#course_detail_id').val();
		            param = param + "&course_detail_id="+course_detail_id;
			        $.ajax({
			            "dataType": 'json',
			            "type": "POST",
			            "url": url,
			            "data": param,
			            "success": function(data) {
			            	console.log(data);
			            	var t = '';
			            	if(data.transfer_status=="T"){
			            		t = '<span class="alert-success"><i class="fa fa-check"></i></span>';
			            	}else{
			            		t = '<span class="alert-danger"><i class="fa fa-times "></i></span>';
			            	}
			            	$("#tr"+id+"_"+course_detail_id).find('#transfer_status').html(t);
			            	$("#tr"+id+"_"+course_detail_id).find('#exp').html(data.exp);
			            	ckSaveCourse(id, course_detail_id);
			            }
			        });					
				}
			});
		}else{
	        var current_course_detail_id = $("#get_data_sections_"+id).find('#current_course_detail_id').val() - 0;
	        if(current_course_detail_id<=0){
	        	alert('กรุณาเลือก ข้อมูลสถานที่สอบ/อบรม');
	        	return;
	        }	
			var course_detail_id = current_course_detail_id;
            param = param + "&course_detail_id="+course_detail_id;
	        $.ajax({
	            "dataType": 'json',
	            "type": "POST",
	            "url": url,
	            "data": param,
	            "success": function(data) {
	            	console.log(data);
	            	var t = '';
	            	if(data.transfer_status=="T"){
	            		t = '<span class="alert-success"><i class="fa fa-check"></i></span>';
	            	}else{
	            		t = '<span class="alert-danger"><i class="fa fa-times "></i></span>';
	            	}
	            	$("#tr"+id+"_"+course_detail_id).find('#transfer_status').html(t);
	            	$("#tr"+id+"_"+course_detail_id).find('#exp').html(data.exp);
	            	ckSaveCourse(id, course_detail_id);
	            }
	        });		        		
		}
		//msgSuccess('บันทึกข้อมูลเรียบร้อยแล้ว');
	} 

	function ckSaveCourse(course_id, course_detail_id){
		var url = "update-course-elearning.php";
        var param = "";
        param = $("#frmMain"+course_id).serialize();
        param = param + "&type=course";
        param = param + "&course_detail_id="+course_detail_id;
        $.ajax({
            "dataType": 'json',
            "type": "POST",
            "url": url,
            "data": param,
            "success": function(data) {
            	if(data.msg=="1"){
            		msgSuccess('บันทึกรหัสหลักสูตรของระบบ E-Learning เรียบร้อยแล้ว');
            	}else{
            		msgError('ไม่สามารถบันทึกข้อมูลรหัสหลักสูตรของระบบ E-Learning  กรุณาติดต่อผู้ดูแลระบบ')
            	}
            }
        });	       
	} 
</script>
<style>
	#set_time-error{
		display: none !important;
	}
	table tbody td {
		padding: 4px 8px !important;
		font-size: 12px;
		line-height: 25px !important;
	}
	table tr{
		height: 40px;
	}
	.tr-subheader td {
		font-size: 13px;
		line-height: 30px !important;
		background: #D9EDF7 !important
	}
	.datepicker table tfoot{
		display: none;
	}
	#transfer_status > span{
		background: none !important;
	}
</style>