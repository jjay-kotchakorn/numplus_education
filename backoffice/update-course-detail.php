<?php
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";

global $db;
if($_POST){
	$args = array();
	$date = $_POST["date"];
	$thai_day = array(1=>"จ",2=>"อ",3=>"พ",4=>"พฤ",5=>"ศ",6=>"ส",7=>"อา");
	$date = thai_to_timestamp($date);
	$t = convert_mktime($date." 00:00:00");
	$date_n = date("N", $t);
	$args["table"] = "course_detail";
	if($_POST["course_detail_id"]){
	   $args["id"] = $_POST["course_detail_id"];
	}
	else{
	   $args["datetime_add"] = date("Y-m-d H:i:s");
	}
	$args["date"] = $date;
	$args["time"] = $_POST["time"];

	$time = explode("-", $args["time"]);
	$time_phase1 = trim($time[0]);
	$time_phase2 = trim($time[1]);
	$date_phase1 = "{$date} {$time_phase1}:00";
	$date_phase2 = "{$date} {$time_phase2}:00";
	$args["date_phase1"] = $date_phase1;
	$args["date_phase2"] = $date_phase2;

	$args["code_project"] = $_POST["code_project"];
	$args["day"] = $thai_day[$date_n];
	$args["address"] = $_POST["address"];
	$args["coursetype_id"] = (int)$_POST["coursetype_id"];
	$args["section_id"] = (int)$_POST["section_id"];
	$args["course_id"] = (int)$_POST["course_id"];
	$args["address_detail"] = $_POST["address_detail"];
	$args["chair_all"] = (int)$_POST["chair_all"];
	$args["book"] = (int)$_POST["book"];
	$args["open_regis"] = thai_to_timestamp($_POST["open_regis"]);
	$args["end_regis"] = thai_to_timestamp($_POST["end_regis"]);
	$args["date_start"] = thai_to_timestamp($_POST["date_start"]);
	$args["date_stop"] = thai_to_timestamp($_POST["date_stop"]);
	$args["discount"] = (float)$_POST["discount"];
	$args["status"] = $_POST["status"];
	$args["date_other"] = $_POST["date_other"];
	$args["inhouse"] = ($_POST["inhouse"]=="T") ? "T" : "F";
	$args["active"] = $_POST["active"];
	$args["remark"] = $_POST["remark"];
	if($args["status"]=="ยกเลิกรอบสอบ"){
		$args["active"] = "F"; 
	}else{
		$args["active"] = "T"; 
	}
	$args["recby_id"] = (int)$EMPID;
	$args["rectime"] = date("Y-m-d H:i:s");
	$args["datetime_update"] = date("Y-m-d H:i:s");
    $ret = $db->set($args);
    $course_detail_id = $args["id"] ? $args["id"] : $ret;

}

// show succes
$_SESSION["success"]["msg"] = "บันทึกข้อมูลเรียบร้อยแล้ว";

if($_POST["active"]=="F" && $_POST["flag"]!="detail"){
	$args = array();
	$args["p"] = "course";
	$args["course_id"] = (int)$_POST["course_id"];
	$args["type"] = "info";
	redirect_url($args);	
}else{
	$args = array();
	$args["p"] = ($_POST["flag"]=="detail") ? "course-detail" : "course";
	$args["course_detail_id"] = $course_detail_id;
	$args["flag"] = $_POST["flag"];
	if($_POST["flag"]!="detail")
		$args["course_id"] = (int)$_POST["course_id"];
	$args["flag"] = $_POST["flag"];	
	$args["type"] = "detail";
	redirect_url($args);
}
?>