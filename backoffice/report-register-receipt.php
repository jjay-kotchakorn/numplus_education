<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
include_once "./share/member.php";
global $db, $SECTIONID, $COURSETYPEID;
$apay = datatype(" and a.active='T'", "pay_status", true);
$con_section_id = ($SECTIONID>0) ? " and a.section_id={$SECTIONID}" : "";
$con_coursetype_id = ($COURSETYPEID>0) ? " and a.coursetype_id={$COURSETYPEID}" : "";
$section = datatype(" and a.active='T' {$con_section_id}", "section", true);
if($con_coursetype_id)
	$coursetype = datatype(" and a.active='T' {$con_coursetype_id}", "coursetype", true);

$educationlevel = datatype(" and a.active='T'", "educationlevel", true);
$province = datatype(" and a.active='T'", "province", true);
$receipttype = datatype(" and a.active='T'", "receipttype", true);
$university = datatype_university(true);

$error = $_SESSION["error"]["msg"];
unset($_SESSION["error"]["msg"]);
$success = $_SESSION["success"]["msg"];
unset($_SESSION["success"]["msg"]);
?>
<link rel="stylesheet" type="text/css" href="js/jquery.nanoscroller/nanoscroller.css" />
<link href="js/jquery.icheck/skins/square/blue.css" rel="stylesheet">

<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="col-sm-12">
				<div class="content block-flat ">
						<div class="header">              
							<ol class="breadcrumb">
								<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>');">หน้าหลัก</a></li>
								<li class="active">หน้าใบเสร็จรับเงิน</li>
							</ol>
						</div>
										
						<div class="header">
							<div class="form-group row">
								<label class="col-sm-2 control-label">ประเภทใบอนุญาต/คุณวุฒิ</label>
								<div class="col-sm-3">
									<select name="section_id" id="section_id" class="form-control" onchange="reCall();">
										<option value="">---- เลือก ----</option>
										<?php foreach ($section as $key => $value) {
											$id = $value['section_id'];
											if($SECTIONID>0 && $SECTIONID!=$id) continue;
											if($SECTIONID>0 && $SECTIONID==$id){
												$select = "selected";
											}else{
												$select = "";
											}
											$name = $value['name'];
											echo  "<option {$select} value='$id'>$name</option>";
										} ?>

									</select>
								</div>                
								<label class="col-sm-2 control-label">ประเภทหลักสูตร</label>
								<div class="col-sm-2">
									<select name="coursetype_id" id="coursetype_id" class="form-control" onchange="reCall();">
										<option value="">---- เลือก ----</option>
										<?php foreach ($coursetype as $key => $value) {
											$id = $value['coursetype_id'];
											if($COURSETYPEID>0 && $COURSETYPEID==$id){
												$select = "selected";
											}else{
												$select = "";
											}
											$name = $value['name'];
											echo  "<option {$select} value='$id'>$name</option>";
										} ?>

									</select>
								</div>
								<label class="col-sm-1 control-label" style=" padding-right:0px;">ช่องทางชำระ</label>
								<div class="col-sm-2">
									<select name="sPay" id="sPay" class="form-control" onchange="reCall();">
										<option selected="selected" value="">แสดงทั้งหมด</option>
										<option value="paysbuy">paysbuy</option>
										<option value="bill_payment">Bill-payment</option>
										<option value="at_asco">เช็ค/เงินโอน</option>
										<option value="at_ati">ชำระเงินสดผ่าน ATI</option>
										<option value="mpay">mPAY</option>
									</select>
								</div>                                          
							</div> 							
							<div class="form-group row">
								<label class="col-sm-1 control-label">เวลา/สถานที่</label>
								<div class="col-sm-5">
									<div class="input-group">
										<input class="form-control" type="text" id="course_detail_name" readonly="true">
										<input name="course_detail_id" type="hidden" id="course_detail_id" value="" />
										<span class="input-group-btn">
											<button class="btn btn-primary" type="button" onclick="addcourse_detail_other();">...</button>
										</span>
									</div>				
								</div>
								<label class="col-sm-1 control-label">ชื่อ-นามสกุล</label>
								<div class="col-sm-3">
									<div class="input-group">
										<input type="text" class="form-control" id="membername" name="membername" readonly="true" value="">
										<input name="member_id" type="hidden" id="member_id" value="" />
										<span class="input-group-btn">
											<button class="btn btn-primary" type="button" onClick="selectMember();">...</button>
										</span>
									</div>
								</div>
								<div class="col-sm-1">
									<label class="col-sm-1 control-label">
										<a onclick="clearSearch();" id="ok" class="btn btn-warning" 
											type="button"><i class="fa fa fa-exchange">&nbsp;</i>&nbsp;Clear</a>
									</label> 
								</div>
							</div>
							<div class="form-group row">
								<!-- <label class="col-sm-1 control-label">ชื่อ-นามสกุล</label>
								<div class="col-sm-3">
									<div class="input-group">
										<input type="text" class="form-control" id="membername" name="membername" readonly="true" value="">
										<input name="member_id" type="hidden" id="member_id" value="" />
										<span class="input-group-btn">
											<button class="btn btn-primary" type="button" onClick="selectMember();">...</button>
										</span>
									</div>
								</div> -->
								<label class="col-sm-1 control-label"  style=" padding-right:0px;">วันที่ชำระ</label>
								<div class="col-sm-2"  style="padding-left:0px; padding-right:0px;">
									<input class="form-control" name="date_start" id="date_start" onblur="reCall();" placeholder="วันที่ชำระ" type="text">
								</div>                                          
								<label class="col-sm-1 control-label"  style=" padding-right:0px;"> ถึงวันที่ </label>
								<div class="col-sm-3"  style="padding-left:0px; padding-right:0px;">
									<input class="form-control" name="date_stop" id="date_stop" onblur="reCall();" placeholder=" ถึงวันที่ " type="text">
								</div> 
								<label class="col-sm-1 control-label">
									<a href="#" class="btn" onclick="reCall();"><i class="fa fa-search">&nbsp;</i></a>&nbsp;
									<!-- <button onclick="clearSearch();" id="ok" class="btn btn-success" type="button">Clear</button> -->
								</label>  
								<label class="col-sm-1 control-label"  style=" padding-right:0px;"> สถานะใบเสร็จ </label>
								<div class="col-sm-3">
									<select name="rc_status" id="rc_status" class="form-control" onchange="">
										<option selected="selected"  value="success">ใบเสร็จ ( ใช้งาน )</option>
										<!-- <option value="cancel">ใบเสร็จ ( ที่ถูกคืนเงิน )</option> -->
										<option value="credit_note">ใบเสร็จที่ถูกยกเลิกแบบ (Credit note/ออกใบเสร็จใหม่)</option>
										<option value="success_and_credit_note">ใช้งาน + Credit note</option>

									</select>
								</div> 
                                        
							</div>
							
						</div>
					<form id="frmMain" name="frmMain" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="">
					<table id="tbCourse" class="table" style="width:100%">
						  <thead>
							  <tr>
								  <th style="text-align:center" width="5%">ลำดับ <input type="checkbox" id="checkAll" style="float:right;"></th>
								  <th style="text-align:left" width="5%">เลขที่ใบเสร็จ</th>
								  <th style="text-align:left" width="10%">รหัสโครงการ</th>
								  <th width="15%">ชื่อ-นามสกุล</th>
								  <th style="text-align:center" width="20">ออกใบเสร็จในนาม</th>
								  <th style="text-align:center" width="10%">วันที่ชำระเงิน</th>
								  <th style="text-align:center" width="10%">ช่องทางชำระ</th>			  
								  <th style="text-align:center" width="12%">แก้ไข</th>	  								  							  
								  <th style="text-align:center" width="15%">Print</th>								  								  							  
							  </tr>
						  </thead>   
						<tbody>
						</tbody>
					</table>
					</form>
					<div class="clear"></div>
					<hr>
					<div class="filters">         
						<div class="btn-group pull-right">
							<a class="btn btn-small " href="#" onclick="export_pdf();"><i class="fa fa-download"></i> Download PDF (สำเนา)</a>
						</div>											
						<div class="btn-group pull-right">
							<a class="btn btn-small " href="#" onclick="printInfo_no_copy();"><i class="fa fa-print"></i> ใบเสร็จตามรายการที่เลือก (ต้นฉบับ)</a>
						</div>
						<div class="btn-group pull-right">
							<a class="btn btn-small " href="#" onclick="printInfo();"><i class="fa fa-print"></i> ใบเสร็จตามรายการที่เลือก</a>
						</div>          
						<div class="btn-group pull-right">
							<a class="btn btn-small " href="#" onclick="printtax2();"><i class="fa fa-print"></i> สรุปยอดซื้อ-ขายหลักสูตร (ไม่มีเลขที่โครงการ)</a>
						</div>
						<div class="btn-group pull-right">
							<a class="btn btn-small " href="#" onclick="printtax();"><i class="fa fa-print"></i> สรุปยอดซื้อ-ขายหลักสูตร (มีเลขที่โครงการ)</a>
						</div>    
						<div class="btn-group pull-right">
							<a class="btn btn-small " href="#" onclick="rpt_sum_project();"><i class="fa fa-print"></i> สรุปยอดตามเลขที่โครงการ</a>
						</div> 
					</div>
<!-- 					
<div class="filters"> 
	<div class="btn-group pull-right">
		<a class="btn btn-small " href="#" onclick="export_pdf();"><i class="fa fa-download"></i> Download PDF</a>
	</div>
</div>
 -->
					<div class="clear"></div>
				</div>
			</div>

		</div>
	</div> 
</div>
<form id="print_form" action="" method="post">
	<input type="hidden" id="register_ids" name="register_id">;		
</form>
<?php include ('inc/js-script.php') ?>

<script type="text/javascript">
$(document).ready(function() {
	var get_type = "<?php echo $_GET["type"]; ?>";
	if(get_type=="childlist" || get_type=="course_order") $("#add").hide();
	var oTable;
	listItem();	
    $("#date_start").datepicker({language:'th-th',format:'dd-mm-yyyy'});
    $("#date_stop").datepicker({language:'th-th',format:'dd-mm-yyyy'});    
	$("#section_id").change(function(event) {
		var id = $(this).val();
		getDropDown('#coursetype_id', id, 'coursetype', 'data/coursetype.php')
	});

	$("#rc_status").change(function(event) {
		reCall();
	});

	//check alert
	$.ajax({
		url: 'data/ajax-check-session.php',
		data: {
			session_name: 'update_register_receipt_name',
			session_type: 'alert',
			destroy: true,
		},
		type: 'GET',
		dataType: 'json',
		success: function(data){
			// console.log(data);
			// location.reload();

			if(data.status=='error'){
				$.gritter.removeAll({
					after_close: function(){
					$.gritter.add({
						position: 'center',
						title: 'Error',
						text: data.msg,
						class_name: 'danger'
					});
					}
				});
			}

			if(data.status=='success'){
				$.gritter.removeAll({
					after_close: function(){
					$.gritter.add({
						position: 'center',
						title: 'success',
						text: data.msg,
						class_name: 'success'
					});
					}
				});
			}
		}// success
	});	 //end ajax

});
$("#checkAll").click(function(){
 		$("#tbCourse tbody td :checkbox").prop('checked', $(this).prop("checked"));
	}); 
function listItem(){
   var get_type = "<?php echo $_GET["type"]; ?>";
   var url = "data/report-receiptlist.php";
   oTable = $("#tbCourse").dataTable({
	   "sDom": 'T<"clear">lfrtip',
	   "oLanguage": {
   	   "sInfoEmpty": "",
   		"sInfoFiltered": ""
						  },
		"oTableTools": {
			"aButtons":  [	
/*			{
				"sExtends": "xls",
				"sButtonText": "Export ข้อมูลผู้มีสิทธิ์"
			}*/
			]
		},
		 "bSort": false ,
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": url,
		"sPaginationType": "full_numbers",
		"aaSorting": [[ 0, "desc" ]],
		'iDisplayLength': 50,
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			aoData.push({"name":"section_id","value":$("#section_id").val()});			
			aoData.push({"name":"coursetype_id","value":$("#coursetype_id").val()});			
			aoData.push({"name":"member_id","value":$("#member_id").val()});			
			aoData.push({"name":"course_detail_id","value":$("#course_detail_id").val()});						
			aoData.push({"name":"date_start","value":$("#date_start").val()});			
			aoData.push({"name":"date_stop","value":$("#date_stop").val()});			
			//aoData.push({"name":"status","value":"3,5,6"});			
			aoData.push({"name":"pay","value":$("#sPay").val()});			
			//aoData.push({"name":"active","value": "T"});			
			aoData.push({"name":"type","value":"register-receipt"});
			aoData.push({"name":"rc_status","value":$("#rc_status").val()});		
			$.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});
			//console.log(aoData);
		}
   });
   /*$('#tbCourse_filter input').unbind();
   $('#tbCourse_filter input').bind('keyup', function(e) {
       if(e.keyCode == 13) {
        oTable.fnFilter(this.value);  
        console.log(this.value); 
    }
   }); */
}

function editInfo(id){
   if(typeof id=="undefined") return;
   var url = "index.php?p=<?php echo $_GET["p"];?>&register_id="+id+"&type=receipt-address";
 	/*redirect(url);*/
 	window.open(url,'_blank');
}
function edit_receipt_name(id){
   if(typeof id=="undefined") return;
   var url = "index.php?p=<?php echo $_GET["p"];?>&register_id="+id+"&type=receipt-name";
 	/*redirect(url);*/
 	window.open(url,'_self');
}


function reCall(){
	oTable.fnClearTable( 0 );
	oTable.fnDraw();
}

function selectMember(){
   var url = "member map";
   memberpop(url,"ret_member_select");	
}

function ret_member_select(id){
	for(var i in id){
		var ck = "";
		var data = id[i];
        $("#member_id").val(data["member_id"]);
        $("#membername").val(data.name_th);
        reCall();
	}
}

function addcourse_detail_other(){
   var url = "course_detail map";
   courseDetailData(url,"ret_detail_other");	
}

function ret_detail_other(id){
	for(var i in id){
		var ck = "";
		var data = id[i];
		$("#course_detail_id").val(data.course_detail_id);
		console.log(id);
		var str = data.date+" เวลา :  "+data.time+" สถานที่ : "+data.address_detail+" "+data.address+ " จำนวนที่นั่ง : "+data.chair_all;
		$("#course_detail_name").val(str);
		reCall();
	}
}

function clearSearch(){
 $("#coursetype_id").val("");
 $("#section_id").val("");
 $("#member_id").val("");
 $("#membername").val("");
 $("#date_start").val("");
 $("#date_stop").val("");
 $("#sPay").val("");
 reCall();
}
function add_to_walkin(id){
    var url = "index.php?p=register&course_detail_id="+id+"&type=walk-in&course_id=<?php echo $course_id; ?>";
    window.open(url,'_blank');
}
function add_to_book(id){
	var url = "index.php?p=course-detail&course_detail_id="+id+"&type=detail&flag=detail";
    //var url = "index.php?p=register&course_detail_id="+id+"&type=walk-in&course_id=<?php echo $course_id; ?>";
    window.open(url,'_blank');
}

function update_register_no(){
	var t = confirm(" บันทึกเลขที่นั่ง ? ");
	if(!t) return false;
	var url = "data/report-update-no.php";
    var formData = $("#frmMain").serializeArray();
    $.post(url, formData).done(function (data) {
 		if(data.indexOf("UPDATE")!= -1){
 			msgSuccess(data);
 		}else{
 			msgError(data);
 		}
    });	
}
function printInfo(id){
	if(typeof id!="undefined"){

	}else{
	    var id = "";
	    $("#tbCourse tbody tr :checkbox").each(function() {
	        if ($(this).is(":checked")) {
	            var str = $(this).val();
	            id += str+",";
	        }
	    });
	}
	if(id==""){
		alert("ยังไม่ได้เลือกรายการ ? ");
		return;
	}
   	var url = "receipt-print.php";
   	$("#register_ids").val(id);
   	$("#print_form").attr("action", url);
   	$("#print_form").submit();
   	return;
   // window.open(url,'_blank');
}

function printInfo_no_copy(id){
	if(typeof id!="undefined"){

	}else{
	    var id = "";
	    $("#tbCourse tbody tr :checkbox").each(function() {
	        if ($(this).is(":checked")) {
	            var str = $(this).val();
	            id += str+",";
	        }
	    });
	}
	if(id==""){
		alert("ยังไม่ได้เลือกรายการ ? ");
		return;
	}
   	var url = "receipt-print-no-copy.php";
   	$("#register_ids").val(id);
   	$("#print_form").attr("action", url);
   	$("#print_form").submit();
   	return;
   // window.open(url,'_blank');
}

//send data to receipt-tax.php
function printtax(){
	var sec_id = $("#section_id").val();
	var cos_id = $("#coursetype_id").val();
	var pay_by = $("#sPay").val();
	var cos_dtail = $("#course_detail_name").val();
	var name = $("#membername").val();
	var date_start = $("#date_start").val();
	var date_stop = $("#date_stop").val();

   if ( (date_start == '' || date_start == null) || (date_stop == '' || date_stop == null) ) {
   	alert("กรุณาเลือกช่วงเวลา"); 
	location.href = "index.php?p=report&type=register-receipt#";
   }else{
   	var url = "receipt-tax.php?sec_id="+sec_id+"&cos_id="+cos_id+"&pay_by="+pay_by+"&cos_dtail="+cos_dtail+"&name="+name+"&date_start="+date_start+"&date_stop="+date_stop;
   	window.open(url,'_blank');
   };
}

function printtax2(){
	var sec_id = $("#section_id").val();
	var cos_id = $("#coursetype_id").val();
	var pay_by = $("#sPay").val();
	var cos_dtail = $("#course_detail_name").val();
	var name = $("#membername").val();
	var date_start = $("#date_start").val();
	var date_stop = $("#date_stop").val();

   if ( (date_start == '' || date_start == null) || (date_stop == '' || date_stop == null) ) {
   	alert("กรุณาเลือกช่วงเวลา"); 
	location.href = "index.php?p=report&type=register-receipt#";
   }else{
   	var url = "receipt-tax2.php?sec_id="+sec_id+"&cos_id="+cos_id+"&pay_by="+pay_by+"&cos_dtail="+cos_dtail+"&name="+name+"&date_start="+date_start+"&date_stop="+date_stop;
   	window.open(url,'_blank');
   };
}

function rpt_sum_project(){
	var sec_id = $("#section_id").val();
	var cos_id = $("#coursetype_id").val();
	var pay_by = $("#sPay").val();
	var cos_dtail = $("#course_detail_name").val();
	var name = $("#membername").val();
	var date_start = $("#date_start").val();
	var date_stop = $("#date_stop").val();

   if ( (date_start == '' || date_start == null) || (date_stop == '' || date_stop == null) ) {
   	alert("กรุณาเลือกช่วงเวลา"); 
	location.href = "index.php?p=report&type=register-receipt#";
   }else{
   	var url = "report-sum-project.php?sec_id="+sec_id+"&cos_id="+cos_id+"&pay_by="+pay_by+"&cos_dtail="+cos_dtail+"&name="+name+"&date_start="+date_start+"&date_stop="+date_stop;
   	window.open(url,'_blank');
   };
}

//export PDF
function export_pdf(){
	var sec_id = $("#section_id").val();
	var cos_id = $("#coursetype_id").val();
	var pay_by = $("#sPay").val();
	var rc_status = $("#rc_status").val();
	//var cos_dtail = $("#course_detail_name").val();
	//var name = $("#membername").val();
	var date_start = $("#date_start").val();
	var date_stop = $("#date_stop").val();

   if ( (date_start == '' || date_start == null) || (date_stop == '' || date_stop == null) ) {
   	alert("กรุณาเลือกช่วงเวลา"); 
	location.href = "index.php?p=report&type=register-receipt#";
   }else{
   	// alert("555");
   	var url = "./report/report-receipt-export-pdf.php?sec_id="+sec_id+"&cos_id="+cos_id+"&pay_by="+pay_by+"&date_start="+date_start+"&date_stop="+date_stop+"&rc_status="+rc_status;
   	// console.log(url);
   	window.open(url,'_blank');
   };
}


function update_pay_status(){
	var t = confirm("update  สถานะ ชำระเงินแล้ว ? ");
	if(!t) return false;
    var i = 0;
    var str = "";
    $("#tbCourse tbody tr :checkbox").each(function() {
        if ($(this).is(":checked")) {
            var id = $(this).val();
            str += id+",";
        }
        i++;
    });
   
  if(str!=""){
	$.ajax({
		"type": "POST",
		"async": false, 
		"url": "data/register-status-update.php",
		"data": {'register_id': str}, 
		"success": function(data){	
			$.gritter.removeAll({
		        after_close: function(){
		          $.gritter.add({
		          	position: 'center',
			        title: 'Success',
			        text: data,
			        class_name: 'success'
			      });
		        }
		      });
 			reCall();				      							 
		}
	});
  }else{
    $("input[name=menu_order_"+id+"]").val(tmp);
  }

}


function edit_credit_note(id){
    var url = "index.php?p=report&type=credit-note&register_id="+id;
    // var url = "index.php?p=report&type=credit-note";
    window.open(url,'_self');
}

function print_credit_note(id){
    var url = "receipt-print-credit-note.php?register_id="+id;
    // var url = "index.php?p=report&type=credit-note";
    window.open(url,'_blank');
}

</script>