﻿<?php
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";

global $db;

$q = "select course_detail_id, ref_exam_id from course_detail_migrate";
$r = $db->get($q);
$ids = array();
foreach ($r as $key => $v) {
	if(!$v["ref_exam_id"]) continue;
	$ids[$v["ref_exam_id"]] = $v["course_detail_id"];
}

$q = "select * from register_history LIMIT 0, 10000";
$r = $db->get($q);
$i = 0;

foreach ($r as $key => $value) {
	set_time_limit(600);
	$ref_id = $value["ref_exam_id"];
	$args = array();
	$args["table"] = "register_history";
	if($ref_id)
	   $args["id"] = array('key'=>'ref_exam_id', 'value'=>$ref_id);
	$args["course_detail_id"] = $ids[$ref_id];
    $ret = $db->set($args);
    $course_detail_id = $args["id"] ? $args["id"] : $ret;
    print_r($args);
  	echo $i++;
  	echo "<br>";
}
?>