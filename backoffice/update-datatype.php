<?php
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";

$datatype_id = $_POST["datatype_id"];
$table_name = trim($_POST["table_name"]);
$datatype_name = $_POST["datatype_name"];
$con_id = $_POST["con_id"];
$db->begin();
if ($datatype_id && $EMPID) {
    if (is_array($_POST["ckbox"])) {
        foreach ($_POST["ckbox"] as $key => $val) {
            $args = array();
            $args["table"] = $table_name;
            if ($_POST["table_id"][$key]) {
                $args["id"] = $_POST["table_id"][$key];
            } else {
                $args["recby_id"] = $EMPID;
                $args["recTime"] = date("Y-m-d H:i:s");
            }
            $args["code"] = $_POST["code"][$key];
            $args["name"] = $_POST["name"][$key];
            $args["name_eng"] = $_POST["name_eng"][$key];
            $args["remark"] = $_POST["remark"][$key];
            if (!$args["id"] && $val == "F")
                continue;
            if ($args["id"] && $val == "F") {
                $args["active"] = "F";
            }
            if (trim($args["code"]) == "" && trim($args["name"]) == "")
                continue;
            $ret = $db->set($args, true);
            if ($args["id"] && $ret !== false) {
                $args["recby_id"] = $EMPID;
                $args["rectime"] = date("Y-m-d H:i:s");
                $db->set($args);
            }
        }
    }
}
$db->commit();
unset($_SESSION["dataTypeInfo"][$table_name]);

$_SESSION["popup"]["msg"] = "บันทึกข้อมูลเรียบร้อยแล้ว";

$args = array();
$args["datatype_id"] = $datatype_id;
$args["datatype_name"] = $datatype_name;
$args["table_name"] = $table_name;
$args["con_id"] = $con_id;
redirect_url($args);
?>