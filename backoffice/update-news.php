<?php
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";
include('share/class.upload.php');
global $db;
if($_POST){
	$args = array();
	$args["table"] = "news";
	if($_POST["news_id"])
	   $args["id"] = $_POST["news_id"];
	$args["code"] = $_POST["code"];
	$args["newstype_id"] = (int)$_POST["newstype_id"];
	$args["section_id"] = (int)$_POST["section_id"];
	$args["name"] = $_POST["name"];
	$args["link_to"] = $_POST["link_to"];
	$args["detail"] = $_POST["detail"];
	$args["highlight"] = $_POST["highlight"];
	$args["active"] = $_POST["active"];
	$args["recby_id"] = (int)$EMPID;
	$args["rectime"] = date("Y-m-d H:i:s");
	
   $ret = $db->set($args);
   $news_id = $args["id"] ? $args["id"] : $ret;

}

if($_POST["news_id"]){
	$args = array();
	$args["p"] = "news";
	$args["news_id"] = $news_id;
	$args["type"] = "info";
	redirect_url($args);
}else{
	$args = array();
	$args["p"] = "news";
	$arr_clear_arg[] = "news_id";
	$arr_clear_arg[] = "type";
	redirect_url($args, false, $arr_clear_arg);
}
?>