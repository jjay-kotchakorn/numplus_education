<?php
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";
global $db;


$q = "select data_value from config where name='select_course_setting'";
$array_data = $db->data($q);

if(!$array_data){
	$array_data = array();
}else{
	$array_data = json_decode($array_data, true);
}

if(is_array($_POST["section_id"])){
	foreach($_POST["section_id"] as $index=>$v){

		$array_data[$index] = array(
				"aob_rom"           => $_POST["aob_rom"][$index],
				"sorb"              => $_POST["sorb"][$index],
				"aob_rom_sorb"      => $_POST["aob_rom_sorb"][$index],
				"aob_rom_tor_ar_yu" => $_POST["aob_rom_tor_ar_yu"][$index]);

	}
}

$encode = json_encode($array_data);
$db->query("update config set data_value='{$encode}' where name='select_course_setting'");	

$aArg = array();
redirect_url($aArg);
?>