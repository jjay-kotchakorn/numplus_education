<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/member.php";
global $db;
$educationlevel = datatype(" and a.active='T'", "educationlevel", true);
$province = datatype(" and a.active='T'", "province", true);
$receipttype = datatype(" and a.active='T'", "receipttype", true);
$university = datatype_university(true);
$receipt_id = $_GET["receipt_id"];
$q = "select name from receipt where receipt_id=$receipt_id";
$r = $db->rows($q);
$str = "";
if($r){
	$str = $r["name"];
}else{
	$str = "เพิ่มบริษัท";
}


/*echo "<pre>";
print_r($_SESSION["login"]["info"]);
echo "</pre>";*/


?>
<link rel="stylesheet" type="text/css" href="js/jquery.nanoscroller/nanoscroller.css" />
<link href="js/jquery.icheck/skins/square/blue.css" rel="stylesheet">
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="row">
				  <div class="col-md-12">			      
					<div class="block-flat">
						<div class="col-sm-12">
							  <div class="header">							
							  	<ol class="breadcrumb">
							  		<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>');">หน้าหลัก</a></li>
							  		<li class="active"><?php echo $str; ?></li>
							  	</ol>
							  </div>
							  <br>
						</div>
					  <div class="content">
						  <form id="frmMain" name="frmMain" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="update-receipt.php">
						  <input type="hidden" name="receipt_id" id="receipt_id" value="<?php echo $receipt_id; ?>">
						  <div class="col-sm-12">
								<div  id="section_corparation_receipt"class="form-group row">									
										<label class="control-label col-sm-1" style="padding-right:2px;">ประเภทบริษัท</label>
										<label class="radio-inline col-sm-11" style="padding-left:10px;padding-top:0px">
												<?php foreach ($receipttype as $key => $value) {
													if($value["receipttype_id"]=="5") continue;
													echo '<label class="radio-inline"  style="padding-left:10px;padding-top:0px"> <div aria-disabled="false" aria-checked="false" style="position: relative;" class="iradio_square-blue"><input style="position: absolute; opacity: 0;" value="'.$value["receipttype_id"].'" name="receipttype_id"  class="icheck" type="radio"><ins style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;" class="iCheck-helper"></ins></div>&nbsp;&nbsp;'.$value["name"].'</label>';
												} ?>										
										</label>

							</div>

							<div class="form-group row"> 	
										<label class="control-label col-sm-1">ชื่อบริษัท </label>									
										<label class="radio-inline col-sm-2" style="padding-left:10px;padding-top:0px">
											<input  name="name" class="form-control required" id="name" type="text">											
										</label>
										<label class="control-label col-sm-1">สาขา </label>									
										<label class="radio-inline col-sm-2" style="padding-left:10px;padding-top:0px">
											<input  name="branch" class="form-control required" id="branch" type="text">											
										</label>										
										<label class="control-label col-sm-2">เลขที่บัตรประจำตัวผู้เสียภาษี</label>									
										<label class="radio-inline col-sm-2" style="padding-left:10px;padding-top:0px">
											<input class="form-control" name="taxno" id="taxno" maxlength="13" type="text"> 											
										</label>	                
								</div>
								  <div class="form-group row"> 	
									<label class="col-sm-1 control-label">บ้านเลขที่</label>
									<div class="col-sm-1">
									  <input class="form-control" name="no" id="no" placeholder="บ้านเลขที่" type="text">
									</div>			                
									<label class="col-sm-1 control-label">หมู่บ้าน</label>
									<div class="col-sm-2">
									  <input class="form-control" name="gno" id="gno" placeholder="หมู่บ้าน / คอนโด / อาคาร" type="text">
									</div>
									<label class="col-sm-1 control-label">หมู่ที่</label>
									<div class="col-sm-1">
									  <input class="form-control" name="moo" id="moo" placeholder="บ้านเลขที่" type="text">
									</div>
									<label class="col-sm-1 control-label">ซอย</label>
									<div class="col-sm-1">
									  <input class="form-control" name="soi" id="soi" placeholder="ซอย" type="text">
									</div>
									<label class="col-sm-1 control-label">ถนน </label>
									<div class="col-sm-2">
									  <input class="form-control" name="road" id="road" placeholder="ถนน " type="text">
									</div>
							  </div>							  							  
							  <div class="form-group row">
								<label class="col-sm-1 control-label">จังหวัด</label>
								<div class="col-sm-2">
								    <select name="province_id" id="province_id" class="form-control required" 
								    	onchange="getDropDown('#amphur_id', this.value, 'amphur', 'data/province.php')" 
								    	onclick="getDropDown('#amphur_id', this.value, 'amphur', 'data/province.php'), get_pv()">
										<option value="">---- เลือก ----</option>
										<?php foreach ($province as $key => $value) {
											$id = $value['province_id'];
											$name = $value['name'];
											echo  "<option value='$id'>$name</option>";
										} ?>

									</select>
								</div>
								<label class="col-sm-1 control-label">อำเภอ</label>
								<div class="col-sm-2">
								<select name="amphur_id"  id="amphur_id" class="form-control required" 
									onchange="getDropDown('#district_id', this.value, 'district', 'data/province.php')" 
									onclick="getDropDown('#district_id', this.value, 'district', 'data/province.php')">
									<option value="">---- เลือก ----</option>							
								</select>
								</div>
								<label class="col-sm-1 control-label" style="padding-right:2px;">ตำบล/แขวง</label>
								<div class="col-sm-2">
									<select name="district_id" id="district_id" class="form-control required">
										<option value="">---- เลือก ----</option>								
									</select>
								</div>								
								<label class="col-sm-1 control-label" style="padding-right:2px;">รหัสไปรษณีย์</label>
								<div class="col-sm-1">
									<input  id="postcode" class="form-control required" name="postcode" maxlength="6" type="text">
								</div>
							  </div>
								 <div class="form-group">
									<label class="col-sm-1 control-label">สถานะ</label>
									<div class="col-sm-2">
										<select name="active" id="active" class="form-control required">
										  <option selected="selected" value="T">แสดง</option>
										  <option value="F">ไม่แสดง</option>
										</select>
									</div>								 	

								 </div>
						  </div>
						  <div class="clear"></div>						
						  <div class="form-group">
								<div class="col-md-12">
									<div class="form-group row" style="text-align:left;"><hr>										
										<button type="button" class="btn btn-primary" onClick="ckSave()">Save changes</button>
										<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p'] ?>');">Cancel</button>
									</div>
								</div>
						  </div>
						</form>
					  </div>
					</div>
					
				  </div>
				</div>

		</div>
	</div> 
</div>
<?php include_once ('inc/js-script.php'); ?>
<script type="text/javascript">
  $(document).ready(function() {
   $("#frmMain").validate();
   var receipt_id = "<?php echo $_GET["receipt_id"]?>";
   if(receipt_id) receipt_info(receipt_id);
   $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue checkbox',
        radioClass: 'iradio_square-blue'
      });
});
function ckSave(id){
	//increase();
/*	var e = document.getElementById('province_id');
	var province_id = e.options[e.selectedIndex].value;
	console.log(province_id);
*/
	var checkboxes = $("#frmMain").find(':checkbox');
	onCkForm("#frmMain");
	$("#frmMain").submit();
}
 function receipt_info(id){
	if(typeof id=="undefined") return;
	var url = "data/receiptlist.php";
	var param = "single=T&receipt_id="+id;
	$.ajax( {
		"dataType":'json', 
		"type": "POST",
		"async": false, 
		"url": url,
		"data": param, 
		"success": function(data){
			//console.log(data);	
			$.each(data, function(index, array){
				setVal("#frmMain", array);		
				//console.log(array);
				$("select[name=province_id] option").filter(function() {
					return $(this).val() == array.province_id; 
				}).attr('selected', true);	

				var select = $("#amphur_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.amphur_id+'"> '+array.amphur_name+' </option>');	

				var select = $("#district_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.district_id+'"> '+array.district_name+' </option>');	

				//window.onload = get_pv();
			});				 
		}
	});
}


function get_pv()
{
	var pv_id=document.getElementById('province_id').value;
    //var e = document.getElementById("province_id");
	//var pv_id = e.options[e.selectedIndex].value;
    //console.log(pv_id);
}

</script>