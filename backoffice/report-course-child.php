<?php 
include_once "./share/authen.php";
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
global $db;
require_once dirname(__FILE__) . '/PHPExcel.php';

$cos_master_id = $_GET["cos_id"];
/*echo "string";
d($_GET);*/
?>
<!-- <div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<h3 class="text-center"><?php echo $cos_master_id;?></h3>

		</div>
	</div> 
</div> -->
<?php //include ('inc/js-script.php') ?>

<?php
$cos_info = get_course("", $cos_master_id);
$cos_info = $cos_info[0];

$cos_type_id = $cos_info["coursetype_id"];
//var_dump($cos_type_id);
/*echo "<pre>";
print_r($cos_info);
echo "</pre>";
echo "";*/
$qt = "SELECT name
	FROM coursetype
	WHERE active='T' AND coursetype_id=$cos_type_id";
$cos_type = $db->get($qt);	
$cos_type = $cos_type[0]["name"];
//var_dump($cos_type);

$date = new DateTime('now', new DateTimeZone('Asia/Bangkok'));
$date_now =  $date->format('Y-m-d H:i:s');

$cos_dt_ids = array();
$course_info = get_course(" AND a.active='T' AND a.parent_id=$cos_master_id");
//var_dump($course_info);
//d($course_info);
if (!$course_info) {
	echo "ไม่มีหลักสูตรย่อย";
	return;
}

$tmp_ids = NULL;
foreach ($course_info as $key => $v) {
	$tmp_ids .= $v["course_id"].",";
}

$cos_ids = trim($tmp_ids, ",");
//d($cos_ids);

//$reg_info = get_register(" AND a.active='T' AND a.auto_del='F' AND a.course_id IN ($cos_ids)");
$reg_info = get_register(" AND a.active='T' AND a.auto_del='F' AND a.pay_status IN (3, 5, 6) AND a.course_id IN ($cos_ids)");
//d($reg_info);

// $db->query("TRUNCATE TABLE rpt_sum_course_group");
$db->query("DELETE FROM rpt_sum_course_group WHERE 1");
//die();
//insert data to TB rpt_sum_course_group
foreach ($reg_info as $key => $r) {
	$args = array();
	$args["table"] = "rpt_sum_course_group";
	$args["register_id"] = (int)$r["register_id"];
	$args["no"] = (int)$cos_master_id;
	$args["member_id"] = $r["member_id"];
	$args["cid"] = $r["cid"];
	$args["course_id"] = $r["course_id"];
	//$args["course_detail_id"] = $r["course_detail_id"];
	$args["pay_price"] = !empty($r["pay_price"]) ? $r["pay_price"] : 0;
	$args["pay_price"] = $r["pay_status"]==6 ? 0 : $args["pay_price"];
	$args["docno"] = $r["docno"];
	$args["pay_status"] = $r["pay_status"];
	$args["register_no"] = 'T';
	$args["rectime"] = $date_now;
	$args["active"] = 'F';
	$ret = $db->set($args);

	//echo "pay_status={$args["pay_status"]}|price={$args["pay_price"]}<br>";
	//$str_txt = "reg_id=".$r["register_id"]."|cid=".$r["cid"]."|ret=".$ret;
	/*$str_txt = "ret=".$ret;
	   ob_start();
	
		print_r($args);echo "\r\n";
	   $page = ob_get_contents();
	$date_now = date('Y-m-d H:i:s');
	//$file = fopen('./log/log_wr_result_data.log', 'a');
	$log_name = "z-rpt-cos-child.log";
	$file = fopen($log_name, 'a');
	$str_txt = $date_now."|".$str_txt."\r\n".$page;

	fwrite($file, $str_txt);
	fclose($file);*/
}
//die();

$qry = "SELECT member_id 
	FROM rpt_sum_course_group
	WHERE no={$cos_master_id} AND active='F'
	";
$member_ids = $db->get($qry);
foreach ($member_ids as $key => $md) {
	$memb_ids[] = $md["member_id"];
}
//$member_ids = trim($member_ids, ",");
// d($memb_ids);
// die();
$member_ids = array_unique($memb_ids);

// d($member_ids);
//die();

$run_no = 1;
foreach ($member_ids as $key => $m) {
	 //d($m);
	 //die();
	$args = array();
	$member_id = $m;
	//echo "$m\r\n";
	$qry = "SELECT register_id
		,member_id
		,cid
		,course_id
		,pay_price
		,pay_status
		,docno
	FROM rpt_sum_course_group	
	WHERE no={$cos_master_id} AND active='F' AND register_no='T' AND member_id={$member_id}
	";	
	$data = $db->get($qry);

	// d($data);
	// die();
	$pay_price = 0;
	$cos_ids = NULL;
	$docno = NULL;
	$str_cos_ids = NULL;
	if (count($data)>1) {

		//d($data);
		//die();
		foreach ($data as $key => $d) {
			if ($d["pay_status"]==5) {
				$pay_price = $pay_price + 0;
			}else{
				$pay_price = $pay_price + $d["pay_price"];
			}		

			$cos_ids .= $d["course_id"].",";
			$docno .= $d["docno"].",";
		}//end loop

		if ($d["pay_status"]==5) {
			$pay_price = $pay_price + 0;
		}else{
			$pay_price = $pay_price + $d["pay_price"];
		}

		$args["pay_price"] = $pay_price;

		$cos_ids = trim($cos_ids, ",");
		//$cos_ids = array_unique(array)
		$cos_ids = explode(",", $cos_ids);
		$cos_ids = array_unique($cos_ids);
		sort($cos_ids);
		foreach ($cos_ids as $key => $c) {
			$str_cos_ids .= $c.",";
		}
		$str_cos_ids = trim($str_cos_ids, ",");
		//var_dump($str_cos_ids);
		// d($cos_ids);
		$args["course_id"] = $str_cos_ids;
		// die();
		//echo $r["pay_price"];

		$docno = trim($docno, ",");
		$args["docno"] = $docno;
		// var_dump($docno);
		// die();

	}//end if
	else{
		$data = $data[0];
		$args["course_id"] = $data["course_id"];

		if ($data["pay_status"]==5) {
			$pay_price = $pay_price + 0;
		}else{
			$pay_price = $pay_price + $data["pay_price"];
		}

		$args["pay_price"] = $pay_price;
		$args["docno"] = $data["docno"];
	}
	//d($data);
	//die();
	
	$args["table"] = "rpt_sum_course_group";
	$args["register_id"] = (int)$run_no;
	$args["no"] = (int)$cos_master_id;
	$args["member_id"] = $member_id;
	$args["rectime"] = $date_now;
	$args["active"] = 'T';
	$ret = $db->set($args);

	// d($args);
	// die();
	$run_no++;
}//end loop


$qry_reg_ids = "SELECT register_id
	FROM rpt_sum_course_group
	WHERE no={$cos_master_id} AND active='T'
";
$reg_ids = $db->get($qry_reg_ids);
foreach ($reg_ids as $key => $r) {
	$tmp_reg_id .= $r["register_id"].",";
}
$reg_ids = trim($tmp_reg_id, ",");

$qry = "SELECT course_id
	FROM rpt_sum_course_group
	where active='T' AND register_id in ($reg_ids)
	GROUP BY course_id
	ORDER BY course_id ASC";
$cos_grp_ids = $db->get($qry);

$tmp_data = array();
$data = array();
foreach ($cos_grp_ids as $key => $c) {
	$tmp_data = NULL;
	$tmp_data["course_master_id"] = $cos_master_id;
	$tmp_data["course_id"] = $c["course_id"];

	$ar_cos_ids = explode(",", $c["course_id"]);
	$count_ar_cos_ids = count($ar_cos_ids);
	$tmp_data["course_total"] = $count_ar_cos_ids;

	
	$qry = "SELECT count(register_id) AS count_reg_id
		FROM rpt_sum_course_group
		WHERE active='T' AND course_id = '{$c["course_id"]}'
		";
	$count_reg_id = $db->get($qry);
	$count_reg_id = $count_reg_id[0]["count_reg_id"];

	$tmp_data["reg_id_total"] = $count_reg_id;

	$qry_reg_info = "SELECT register_id
				,no
				,member_id
				,course_id
				,pay_price
			FROM rpt_sum_course_group
			WHERE course_id='{$c["course_id"]}' AND active='T'
	";
	$reg_ids = $db->get($qry_reg_info);

	if (!$reg_ids) {
		continue;
	}
	foreach ($reg_ids as $key => $r) {
		$tmp_data["reg_ids"] .= $r["register_id"].",";

		//$tmp_data["member_ids"] .= $r["member_id"].",";
	}
	
	$tmp_data["reg_ids"] = trim($tmp_data["reg_ids"], ",");

	//$tmp_data["member_ids"] = trim($tmp_data["member_ids"], ",");
	$data[] = $tmp_data;

}// end loop for

$data_dup = false;
$q = "SELECT rpt_sum_course_id
	FROM rpt_sum_course
	WHERE active='T' AND course_master_id = $cos_master_id";
$chk_dup = $db->get($q);
//var_dump($chk_dup);

/*echo "<pre>";
print_r($chk_dup);
echo "</pre> <br>";*/
if ($chk_dup) {
	$data_dup = true;
	$q = "DELETE FROM rpt_sum_course
		WHERE active='T' AND course_master_id=$cos_master_id";
	$clear_dup = $db->query($q);
	//r_dump($clear_dup);
	if ($clear_dup) {
		$data_dup = false;
	}else{
		$data_dup = true;
	}
}else{
	$data_dup = false;
}
//d($data);
if (!$data_dup) {
	foreach ($data as $key => $d) {
		//echo $d[$key]." => ".$d["course_id"]."<br>";

		$args = array();
		$args["table"] = "rpt_sum_course";
		$args["course_master_id"] = (int)$cos_master_id;
		$args["course_id"] = $d["course_id"];
		$args["course_total"] = $d["course_total"];
		$args["reg_id_total"] = $d["reg_id_total"];
		$args["reg_ids"] = $d["reg_ids"];
		$args["member_ids"] = $d["member_ids"];
		$args["update_time"] = $date_now;
		$args["active"] = "T";

		$ret = $db->set($args);
		//var_dump($ret);
	}// end loop for
}// end if

// echo "<br><br>";
// echo "<pre>";
// print_r($data);
// echo "</pre> <br>";

$qr = "SELECT course_id
		, course_total
		, reg_id_total
		, reg_ids
	FROM rpt_sum_course
	WHERE active='T' AND course_master_id = $cos_master_id
	ORDER BY course_total DESC";
$info = $db->get($qr);

// d($info);
// die();

?>

<!DOCTYPE html>
<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>รายชื่อผู้เข้าร่วมกิจกรรม</title>
<link href="css/printform-style.css" rel="stylesheet" type="text/css" media="all" />
<style>
	@media print{
		body{
			padding: 0px;
			margin: 0px;
		}
		#btPrint{
			display: none;
		}
	}
	.rpt-print{
		table-layout: fixed;
		/*width: 1199;*/
		/*width: 100%;*/
		word-wrap:break-word;
	}
</style>
<body>
	<!-- <div class="form-portrait"> -->
	<div class="form-landscape">
		<div>
			<table class="no-border">
				<tbody>
					<tr>
						<td><span class="center font-weight-bold"><?php echo "รายชื่อผู้เข้า".$cos_type;?></span></td>
					</tr>
					<tr>
						<td><span class="center font-weight-bold"><?php echo $cos_info["title"];?></span></td>
					</tr>
					<tr>
						<td><span class="center"><?php echo "วันที่สร้างหลักสูตร ".revert_date($cos_info["rectime"]);?></span></td>
					</tr>
				</tbody>
			</table>
		</div>

		<div>
<?php
		$run_no = 1;
		foreach ($info as $key => $v) {
			$strt_header = true;
			if ( $v["reg_id_total"]==0 || empty($v["reg_id_total"]) ) {
				continue;
			}
			//echo $v["course_id"];
			$cos_child_ids = explode(",", $v["course_id"]);	
			//var_dump($cos_child_ids);
			$cos_child_code = "รหัสหลักสูตร ";
			foreach ($cos_child_ids as $key => $ch) {
				//var_dump($ch);
				$cos_dtail_info = get_course("", $ch);
				$cos_dtail_info = $cos_dtail_info[0];
				//var_dump($cos_dtail_info);
				$cos_child_code .= $cos_dtail_info["code"]." , ";
				
			}
			$cos_child_code = trim($cos_child_code, " , ");		
?>
			<!-- <div style="page-break-before: always;"></div> -->
<?php
			echo $cos_child_code."<br>";
			echo "จำนวนผู้สมัคร  ".$v["reg_id_total"]."  คน";	

			//$reg_ids = explode(",", $v["reg_ids"]);
			$reg_ids = $v["reg_ids"];

			$qry = "SELECT r.register_id 
						, r.docno
						, r.pay_price
						, r.pay_status
						, m.title_th AS title_th
						, m.fname_th AS fname_th
					    , m.lname_th AS lname_th
					    , m.cid
					    , m.member_id
					    , m.tel_mobile
					    , m.email1
					    , m.org_type
					    , m.org_name
					    , m.org_lv
					    , m.title_th_text
					FROM rpt_sum_course_group AS r
					LEFT JOIN member AS m ON m.member_id = r.member_id 
					WHERE  r.active='T' AND r.register_id IN ($reg_ids) 
					order by fname_th asc";

			mysql_query("SET character_set_results=utf8");
			// mysql_query("SET character_set_client=tis620");
			// mysql_query("SET character_set_connection=tis620");

			$data_info = $db->get($qry);

			//d($data_info);
			//die();

?>			
			<table class="td-center rpt-print" width=1200 style='table-layout:fixed'>
				 <col width=50>
				 <col width=80>
				 <col width=100>
				 <col width=100>
				 <col width=100>
				 <col width=170>
				 <col width=200>
				 <col width=100>
				 <col width=100>
				 <col width=100>

<?php
			foreach ($data_info as $key => $d) {
				$org_name = "";

				// $tis620 = iconv("utf-8", "tis-620", $message );
    			// $utf8 = iconv("tis-620", "utf-8", $tis620 );
    			// htmlentities($fname, ENT_QUOTES, 'UTF-8');

				if ($d['title_th']=='อื่นๆ') {
					$d["title"] = $d["title_th_text"];
				}elseif($d['title_th']!='อื่นๆ') {
					$d["title"] = $d['title_th'];
				}
				
				$d["fname"] = $d['fname_th'];
				$d["lname"] = $d['lname_th'];

				if ( empty($d['org_name']) ) {
					$org_name = $d['org_lv'];
				}else{
					$qry_org = "SELECT name
						FROM receipt
						WHERE receipt_id={$d['org_name']}";
					$org_name = $db->get($qry_org);
					$org_name = $org_name[0]['name'];
					//var_dump($org_name);
				}

				$pay_price = number_format( $d["pay_price"], 2, '.', ',' );
				if ($d["pay_price"]==0) {
					$pay_price = '-';
				}

				if ($strt_header) {					
?>
				<thead>
					<tr>
						<td ><span class="center">ลำดับที่</span></td>
						<td ><span class="right-line-center">คำนำหน้าชื่อ</span></td>
						<td ><span class="right-line-center">ชื่อ</span></td>
						<td ><span class="center">นามสกุล</span></td>
						<td ><span class="center">เลขที่บัตรประชาชน</span></td>
						<td ><span class="center">E-mail</span></td>
						<td ><span class="center">สังกัด</span></td>
						<td ><span class="center">โทรศัพท์</span></td>
						<td ><span class="center">ใบเสร็จ</span></td>
						<td ><span class="center">จำนวนเงิน</span></td>
					</tr>
				</thead>
<?php
				$strt_header = false;
				}// end if 	
						
?>
				<tbody>
					<tr>
						<td ><span class="center"><?php echo $run_no;?></span></td>
						<td ><span class="center"><?php echo $d["title"];?></span></td>
						<td ><span class="left"><?php echo $d["fname"];?></span></td>
						<td ><span class="left"><?php echo $d["lname"];?></span></td>
						<td ><span class="center"><?php echo $d["cid"];?></span></td>
						<td ><span class="left"><?php echo $d["email1"];?></span></td>
						<td ><span class="left"><?php echo $org_name;?></span></td>
						<td ><span class="center"><?php echo $d["tel_mobile"];?></span></td>
						<td ><span class="center"><?php echo $d["docno"];?></span></td>
						<td ><span class="right"><?php echo $pay_price;?></span></td>
					</tr>
				</tbody>
<?php
			$run_no++;
			}// end loop $id
?>
			</table>
<?php						
		}// end loop $v
?>
		<!-- <div style="page-break-before: always;"></div> -->
		</div>
	</div>

</body>
<script type="text/javascript">
function printPage(){
	   window.print();
	   setTimeout(" parent.$.fancybox.close()",1000);
	}
</script>
</html>