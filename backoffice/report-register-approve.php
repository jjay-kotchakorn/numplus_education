<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
include_once "./share/member.php";
global $db;
global $SECTIONID;
$apay = datatype(" and a.active='T'", "pay_status", true);

$educationlevel = datatype(" and a.active='T'", "educationlevel", true);
$province = datatype(" and a.active='T'", "province", true);
$receipttype = datatype(" and a.active='T'", "receipttype", true);
$university = datatype_university(true);

$error = $_SESSION["error"]["msg"];
unset($_SESSION["error"]["msg"]);
$success = $_SESSION["success"]["msg"];
unset($_SESSION["success"]["msg"]);

$course_id = $_GET["course_id"];
$course_detail_id = $_GET["course_detail_id"];
$section = datatype(" and a.active='T'", "section", true);
$coursetype = datatype(" and a.active='T'", "coursetype", true);
$q = "select  day, date, time , address, address_detail from course_detail where course_detail_id=$course_detail_id";
$r = $db->rows($q);
$str = "";

if($r){
	$str = $r["day"]." ".revert_date($r["date"])." ".$r["time"]." ".$r["address_detail"]." ".$r["address"];
	$info = get_course("", $course_id);
	if($info) $info = $info[0];
	$section_id = $info["section_id"];
	$coursetype_id = $info["coursetype_id"];
}else{
	$str = "เพิ่มหลักสูตร";
}

$dtail = get_course_detail("", $course_detail_id);
$dtail = $dtail[0];

?>
<link rel="stylesheet" type="text/css" href="js/jquery.nanoscroller/nanoscroller.css" />
<link href="js/jquery.icheck/skins/square/blue.css" rel="stylesheet">

<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="col-sm-12">
				<div class="content block-flat ">
						<div class="header">              
							<ol class="breadcrumb">
								<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>');">หน้าหลัก</a></li>
								<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>&type=select-course');">เลือกหลักสูตร</a></li>
								<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>&course_id=<?php echo $course_id; ?>&type=select-course-detail');">หน้ายอดผู้สมัคร</a></li>
								<li class="active">ผู้มีสิทธิ์เข้าร่วมกิจกรรม</li>
							</ol>
						</div>
				<?php
						/*echo "<pre>";
						print_r($info);
						echo "</pre>";
						echo "<pre>";
						print_r($dtail);
						echo "</pre>";*/
				?>				
						<div class="form-group row">
							<label class="col-sm-6" style="padding-left:15px;" >ประเภทใบอนุญาต/คุณวุฒิ : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $info["section_name"]; ?></label>
							<label class="col-sm-6" style="padding-left:15px;" >ประเภทหลักสูตร : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $info["coursetype_name"]; ?></label>
							<label class="col-sm-12" style="padding-left:15px;" >สถานที่ : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $dtail["address_detail"]." ".$dtail["address"]; ?></label>
							<label class="col-sm-2" style="padding-left:15px;" >วันที่ : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $dtail["date"]; ?></label>
							<label class="col-sm-7" style="padding-left:15px;" >เวลา : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $dtail["time"]; ?></label>
							<!-- <a href="#" class="btn" onclick="reCall();"><i class="fa fa-refresh">&nbsp;</i> Refresh</a>&nbsp;&nbsp; -->
							<!-- <label class="col-sm-1 control-label">เรียงลำดับ</label> -->
							<div class="col-sm-3">
								<select name="sort_ord" id="sort_ord" class="form-control" onchange="reCall()">
									<option selected="selected" value="reg_date">เรียงจาก วันที่ลงทะเบียนล่าสุด</option>
									<!-- <option value="reg_date">เรียงจาก วันที่ลงทะเบียนล่าสุด</option> -->
									<option value="pay_date">เรียงจาก วันที่ชำระเงินล่าสุด</option>
									<option value="no">เรียงจาก เลขที่นั่งสอบ</option>
									<option value="name">เรียงจาก ชื่อ</option>
									<option value="cos_id">เรียงจาก หลักสูตร</option>
									<option value="status">เรียงจาก สถานะการชำระเงิน</option>
								</select>
							</div> 
						</div>

					<form id="frmMain" name="frmMain" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="">
					<table id="tbCourse" class="table" style="width:100%">
						  <thead>
							  <tr>
								  <th style="text-align:center" width="5%">ลำดับ</th>
								  <th width="13%">ชื่อ-นามสกุล</th>
								  <th style="text-align:center" width="18%">หลักสูตร</th>
								  <th style="text-align:center" width="8%">ลงทะเบียน</th>
								  <th style="text-align:center" width="8%">หมดเขต</th>
								  <th style="text-align:center" width="8%">วันที่ชำระเงิน</th>
								  <th style="text-align:center" width="8%">สถานะ</th>								  								  							  
							  </tr>
						  </thead>   
						<tbody>
						</tbody>
					</table>
					</form>
					<div class="clear"></div>
					
					<br>
					<div class="filters">     
						<div class="btn-group pull-left">
							<a class="btn btn-small " href="#" onclick="update_register_no();"><i class="fa fa-save"></i> บันทึกเลขที่นั่ง</a>
						</div>

						<div class="">
							<select name="sort_by" id="sort_by" class="from-control" >
								<!-- <option value="0">---- เรียงลำดับ ----</option> -->	
								<option value="course">หลักสูตร</option>
								<option value="name">ชื่อ-นามสกุล</option>
							</select>
						</div>	

						<div class="btn-group pull-right">
							<a class="btn btn-small " href="#" onclick="sign_train_print();"><i class="fa fa-print"></i> Print ใบเซ็นชื่อ</a>
						</div>
						<?php
						if ($info["section_id"] == 1) {
						?>
							<div class="btn-group pull-right">
								<a class="btn btn-small " href="#" onclick="sign_report_list_examiner_print();"><i class="fa fa-print"></i> รายชื่อผู้เข้ารับการ<?php echo $info["coursetype_name"] ?>	</a>
							</div>
						<?php
						}
						?>
						<div class="btn-group pull-right">
							<a class="btn btn-small " href="#" onclick="sign_report_label_print();"><i class="fa fa-print"></i> Lable-Address</a>
						</div>     
					</div>
					<div class="clear"></div>
				</div>
			</div>

		</div>
	</div> 
</div>
<?php include ('inc/js-script.php') ?>

<script type="text/javascript">
$(document).ready(function() {
	var get_type = "<?php echo $_GET["type"]; ?>";
	if(get_type=="childlist" || get_type=="course_order") $("#add").hide();
	var oTable;
	listItem();
	$("#sort_by").change(function(event) {
		reCall();
	});  
	//alert(sort_by);
	//listItem();	
});

function listItem(){
   //var sort_by = $("#sort_by").val();
   var get_type = "<?php echo $_GET["type"]; ?>";
   var url = "data/report-registerlist.php";
   oTable = $("#tbCourse").dataTable({
	   "sDom": 'T<"clear">lfrtip',
	   "oLanguage": {
   	   "sInfoEmpty": "",
   		"sInfoFiltered": ""
						  },
		"oTableTools": {
			"aButtons":  [	
			]
		},
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": url,
		"sPaginationType": "full_numbers",
		"aaSorting": [[ 0, "desc" ]],
		'iDisplayLength': 100,
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			aoData.push({"name":"course_detail_id","value":<?php echo (int) $course_detail_id; ?>});			
			aoData.push({"name":"status","value":"3,5,6"});			
			aoData.push({"name":"active","value": "T"});			
			aoData.push({"name":"type","value":get_type});	
			aoData.push({"name":"sort_by","value":$("#sort_ord").val()});
			$.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});
		}
   }); 
}

function editInfo(id){
	if(typeof id=="undefined") return;
   var url = "index.php?p=<?php echo $_GET["p"];?>&register_id="+id+"&type=info";
   redirect(url);
}


function childlist(id){
	if(typeof id=="undefined") return;
   var url = "index.php?p=<?php echo $_GET["p"];?>&register_id="+id+"&type=childdetail";
   redirect(url);
}

function addnew(){
   var url = "index.php?p=<?php echo $_GET["p"];?>&type=select-course";
   redirect(url);
}

function reCall(){
	oTable.fnClearTable( 0 );
	oTable.fnDraw();
}

function selectMember(){
   var url = "member map";
   memberpop(url,"ret_member_select");	
}

function ret_member_select(id){
	for(var i in id){
		var ck = "";
		var data = id[i];
        $("#member_id").val(data["member_id"]);
        $("#membername").val(data.name_th);
        reCall();
	}
}

function addcourse_detail_other(){
   var url = "course_detail map";
   courseDetailData(url,"ret_detail_other");	
}

function ret_detail_other(id){
	for(var i in id){
		var ck = "";
		var data = id[i];
		$("#course_detail_id").val(data.course_detail_id);
		console.log(id);
		var str = data.date+" เวลา :  "+data.time+" สถานที่ : "+data.address_detail+" "+data.address+ " จำนวนที่นั่ง : "+data.chair_all;
		$("#course_detail_name").val(str);
		reCall();
	}
}

function clearSearch(){
 $("#course_detail_name").val("");
 $("#course_detail_id").val("");
 $("#member_id").val("");
 $("#membername").val("");
 reCall();
}
function add_to_walkin(id){
    var url = "index.php?p=register&course_detail_id="+id+"&type=walk-in&course_id=<?php echo $course_id; ?>";
    window.open(url,'_blank');
}
function add_to_book(id){
	var url = "index.php?p=course-detail&course_detail_id="+id+"&type=detail&flag=detail";
    //var url = "index.php?p=register&course_detail_id="+id+"&type=walk-in&course_id=<?php echo $course_id; ?>";
    window.open(url,'_blank');
}
function printinfo(id){
	var url = "index.php?p=course-detail&course_detail_id="+id+"&type=detail&flag=detail";
    //var url = "index.php?p=register&course_detail_id="+id+"&type=walk-in&course_id=<?php echo $course_id; ?>";
    window.open(url,'_blank');
}
function update_register_no(){
	var t = confirm(" บันทึกเลขที่นั่ง ? ");
	if(!t) return false;
	var url = "data/report-update-no.php";
    var formData = $("#frmMain").serializeArray();
    $.post(url, formData).done(function (data) {
 		if(data.indexOf("UPDATE")!= -1){
 			msgSuccess(data);
 		}else{
 			msgError(data);
 		}
    });	
}

//print train
function sign_train_print(){
	var coursetype_id = "<?php echo $coursetype_id ?>";
	var section_id = "<?php echo $section_id; ?>";
	var sort_by = $("#sort_by").val();

	if(section_id==1 && coursetype_id==2){
   		//var url = "sign-ic-report-print.php?course_detail_id=<?php echo $course_detail_id; ?>&type=info&course_id=<?php echo $course_id; ?>";
   		var url = "sign-ic-report-print.php?course_detail_id=<?php echo $course_detail_id; ?>&type=info&course_id=<?php echo $course_id; ?>&sort_by="+sort_by;
	}else{
   		//var url = "sign-train-report-print.php?course_detail_id=<?php echo $course_detail_id; ?>&type=info&course_id=<?php echo $course_id; ?>";
		var url = "sign-train-report-print.php?course_detail_id=<?php echo $course_detail_id; ?>&type=info&course_id=<?php echo $course_id; ?>&sort_by="+sort_by;
	}
	//console.log(url);
   window.open(url,'_blank');
}



function sign_report_conclude_print(){
   var url = "report-register-conclude-train-print.php?course_detail_id=<?php echo $course_detail_id; ?>&type=info&course_id=<?php echo $course_id; ?>";
   window.open(url,'_blank');
}
function sign_report_list_examiner_print(){
   var url = "report-register-list-examiner-print.php?course_detail_id=<?php echo $course_detail_id; ?>&type=info&course_id=<?php echo $course_id; ?>";
   window.open(url,'_blank');
}
function sign_report_label_print(){
   var url = "report-register-label-print.php?course_detail_id=<?php echo $course_detail_id; ?>&type=info&course_id=<?php echo $course_id; ?>";
   window.open(url,'_blank');
}
function update_pay_status(){
	var t = confirm("update  สถานะ ชำระเงินแล้ว ? ");
	if(!t) return false;
    var i = 0;
    var str = "";
    $("#tbCourse tbody tr :checkbox").each(function() {
        if ($(this).is(":checked")) {
            var id = $(this).val();
            str += id+",";
        }
        i++;
    });
   
  if(str!=""){
	$.ajax({
		"type": "POST",
		"async": false, 
		"url": "data/register-status-update.php",
		"data": {'register_id': str}, 
		"success": function(data){	
			$.gritter.removeAll({
		        after_close: function(){
		          $.gritter.add({
		          	position: 'center',
			        title: 'Success',
			        text: data,
			        class_name: 'success'
			      });
		        }
		      });
 			reCall();				      							 
		}
	});
  }else{
    $("input[name=menu_order_"+id+"]").val(tmp);
  }

}
</script>