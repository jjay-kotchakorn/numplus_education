<?php
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";
include('share/class.upload.php');
global $db;

$ds          = DIRECTORY_SEPARATOR;  //1
 
$storeFolder = 'uploads/';   //2
 
if (!empty($_FILES) && $_POST["bill_payment_upload_id"]) {
     
	$uploaddir = './uploads/'; 
	$file = $uploaddir . basename($_FILES['file']['name']); 
	$name_photo=($_FILES['file']['name']);
	$size=$_FILES['file']['size'];
	if($size>10485760000)
	{
		echo "error file size > 100 MB";
		unlink($_FILES['file']['tmp_name']);
		exit;
	}
	$q = "select count(bill_payment_upload_id) as c from bill_payment_upload_list where name='$name_photo' and active='T'";
	$ck = $db->data($q);
	if($ck>0){
		echo "error $name_photo";
		unlink($_FILES['file']['tmp_name']);
		exit();
	}else{

		if (move_uploaded_file($_FILES['file']['tmp_name'], $file)) { 
			if($_POST){
				$args = array();
				$args["table"] = "bill_payment_upload_list";
				$args["bill_payment_upload_id"] = $_POST["bill_payment_upload_id"];
				$args["url"] = $uploaddir;
				$args["name"] = $name_photo;
				$args["active"] = "T";
				$args["recby_id"] = (int)$EMPID;
				$args["rectime"] = date("Y-m-d H:i:s");		
			    $db->set($args);
			}
		    //echo "success"; 
		} else {
			//echo "error ".$_FILES['file']['error']." --- ".$_FILES['file']['tmp_name']." %%% ".$file."($size)";
		}  
	}
}else{
	$bill_payment_upload_list_id = $_POST["bill_payment_upload_list_id"];
	$q = "select a.name, a.url, a.bill_payment_upload_list_id from bill_payment_upload_list a where a.bill_payment_upload_list_id=".$bill_payment_upload_list_id;
	$r = $db->rows($q);
	if($r){		
		$q = "delete from bill_payment_upload_list where  bill_payment_upload_list_id=".$r["bill_payment_upload_list_id"];
		$db->query($q);
		unlink($r["url"].$r["name"]);

		//ref backoffice/index.php?p=report&type=coursetype-summary-cost
		$q = "DELETE FROM payment_compare_all_list WHERE payment_compare_file_list_id=".$r["bill_payment_upload_list_id"];
		$db->query($q);
	}
}
?>