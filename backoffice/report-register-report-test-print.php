<?php
include_once "./share/authen.php";
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
global $db;
require_once dirname(__FILE__) . '/PHPExcel.php';
$info = get_course_detail(" and a.course_detail_id={$_GET["course_detail_id"]}");
$course_info = get_course(" and a.course_id={$_GET["course_id"]}");
if($info){
	$info = $info[0];
	$course_info = $course_info[0];
	$course_detail_id = $info["course_detail_id"];
	$course_id = $course_info["course_id"];
	$fileName ="";
	$apay = datatype(" and a.active='T'", "pay_status", true);
	$arr_pay = array();
	foreach ($apay as $key => $value) {
		$arr_pay[$value["pay_status_id"]] = $value["name"];
	}
?>

<!DOCTYPE html>
<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>รายชื่อผู้สอบ <?php echo $course_info["title"]; ?></title>
<link href="css/printform-style.css" rel="stylesheet" type="text/css" media="all" />
<style>
	@media print{
		body{
			padding: 0px;

		}
		#btPrint{
			display: none;
		}
	}
</style>
<body>
	<div class="form-landscape">

		<div>
			<table class="td-center">
				<thead>
					<tr>
						<td width="120"><span class="center">รหัสบริษัท</span></td>
						<td width="120"><span class="center">งวดรายงาน (ค.ศ.)<br>(YYYYMMDD)</span></td>
						<td width="120"><span class="center">รหัสแบบรายงาน</span></td>
						<td width="120"><span class="center">จำนวนรายการ</span></td>
						<td class="td-no-border">&nbsp;</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><span class="center">&nbsp;</span></td>
						<td><span class="center"></span></td>
						<td><span class="center"></span></td>
						<td><span class="center"></span></td>
						<td class="td-no-border"></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div>
			<table class="td-center">
				<thead>
					<tr class="font-weight-bold">
						<td colspan="4"><span class="center">DETAIL SECTION</span></td>
						<td colspan="6"><span class="center">คะแนน Post Test</span></td>
						<td class="td-no-border"></td>
						<td class="td-no-border"></td>
						<td class="td-no-border"></td>
					</tr>
					<tr>
						<td><span class="center">รหัสหลักสูตร</span></td>
						<td><span class="center">วันที่อบรม (ค.ศ.)<br>(YYYYMMDD)</span></td>
						<td><span class="center">ประเภท<br>รหัสประจำตัว</span></td>
						<td><span class="center">รหัสประจำตัว</span></td>
						<td><span class="center">คะแนน<br>ที่ได้</span></td>
						<td><span class="center">คะแนนเต็ม</span></td>
						<td><span class="center">MAX.</span></td>
						<td><span class="center">MIN.</span></td>
						<td><span class="center">AVG.</span></td>
						<td><span class="center">S.D.</span></td>
						<td><span class="center">คำนำหน้า</span></td>
						<td><span class="center">ชื่อ</span></td>
						<td><span class="center">สกุล</span></td>
					</tr>
				</thead>
				<?php 

				$ids = array();
				$q = " select register_id from register_course_detail where active='T'  and course_detail_id=$course_detail_id";
				$get_all = $db->get($q);
				if($get_all){
					foreach ($get_all as $key => $value) {
						$ids[] = $value["register_id"];
					}
				}
				$q = " select register_id from register where active='T'  and course_detail_id='$course_detail_id'";
				$get_register_all = $db->get($q);
				if($get_register_all){
					foreach ($get_register_all as $key => $value) {
						$ids[] = $value["register_id"];
					}
				}
				$t = array_unique($ids);
				$con_ids = implode(",", $t);
				$q = "select a.title,
							 a.fname,
							 a.lname,
							 a.cid,
							 a.branch,
							 b.email1,
							 b.tel_home,
							 b.tel_mobile,
							 b.tel_office,
							 a.pay_status,
							 a.course_price,
							 a.course_discount,
							 a.pay_date,
							 a.pay,
							 b.org_position,
							 c.name as org,
							 d.name as result_name,
							 a.remark,
							 a.course_id,
							 a.register_id,
							 a.course_detail_id
				from register a inner join member b on a.member_id=b.member_id
					left join receipt c on c.receipt_id=b.org_name
					left join register_result d on d.register_result_id=a.result
				where a.register_id in ($con_ids) and a.pay_status in (3,5,6) and a.active='T' 
				order by a.no asc";
				$rs = $db->get($q);
				if($rs){
					$i = 1;
				?>
				<tbody>
					<?php foreach ($rs as $key => $v): 
						$register_id = $v["register_id"];
						$sCourse_id = $v["course_id"];
						$list_course = $v["course_id"];
						$list_course_detail = $v["course_detail_id"];
						$register_pay_status = $v["pay_status"];
						$single = false;
						if(strrpos($list_course, ",")!==false){
							
						}else{
							$single = true;
						}
						$con = " and a.course_id in ($sCourse_id)";
						$r = get_course($con);
						$title = "";
						$display_code = "";
						$parent_id = 0;
						$price = 0;
						$array_course = array();
						$array_course_detail = array();
						if($r){
							foreach ($r as $key => $row) {
								$course_id = $row['course_id'];
								$array_course[$course_id] = $row;
								$section_id = $row['section_id'];
								$display_code .= $row["code"];
								$title .= $row['title'].", ";
								$set_time = $row['set_time'];
								$life_time = $row['life_time'];
								$status = $row['status']; 
								$parent_id = (int)$row['parent_id'];

							}
						}
						
						$q = "select a.course_detail_id, a.course_id from register_course_detail a where a.register_id={$register_id} and a.course_id in ($sCourse_id)";
						$cd = $db->get($q);
						$parent_title = "";
						if($parent_id>0){
							$q = "select title from course where active='T' and course_id=$parent_id";
							$t = $db->data($q);
							$parent_title = "<strong>{$t}</strong><br>";	
						}
						$title = trim($title, ", ");
						$price = $v["course_price"];
						$discount = $v["course_discount"];
						$display_date = "";
						$display_time = "";
						$display_address = "";
						if($cd){    
							foreach ($cd as $kk => $vv) {
								$row = get_course_detail(" and a.active='T' and a.course_detail_id={$vv["course_detail_id"]}");
								if($row) $row = $row[0];
								$course_detail_id = $row['course_detail_id'];
								$course_id = $vv['course_id'];
								$array_course_detail[$course_id][$course_detail_id] = $row;
								$day = $row['day'];
								$date = $row['date'];
								$time = $row['time'];
								$display_date .= $date."<br> ".$time;
								$display_time .= $time.", ";
								$display_address .= $row['address_detail']." ".$row['address'].", ";
							}

						}else{
							$row = get_course_detail(" and a.active='T' and a.course_detail_id={$v["course_detail_id"]}");
							if($row) $row = $row[0];
							$course_detail_id = $row['course_detail_id'];
							$course_id = $v['course_id'];
							$array_course_detail[$course_id][$course_detail_id] = $row;
							$day = $row['day'];
							$date = $row['date'];
							$time = $row['time'];
							$display_date .= $date."<br> ".$time;
							$display_time .= $time.", ";
							$display_address .= $row['address']." ".$row['address_detail'].", ";
						}

						$display_date = trim($display_date, ", ");
						$display_time = trim($display_time, ", ");
						$display_address = trim($display_address, ", ");					
					?>
						<tr>
							<td width="120"><span class="center"><?php echo $display_code; ?></span></span></td>
							<td width="120"><span class="center"><?php echo $display_date; ?></span></td>
							<td width="120"><span class="center"><?php echo (strlen($v["cid"])==13) ? "บัตรประชาชน":"พาสปอร์ต"; ?></span></td>
							<td width="120"><span class="center"><?php echo $v["cid"]; ?></span></td>
							<td width="50"><span class="center"></span></td>
							<td width="50"><span class="center"></span></td>
							<td width="50"><span class="center"></span></td>
							<td width="50"><span class="center"></span></td>
							<td width="50"><span class="center"></span></td>
							<td width="50"><span class="center"></span></td>
							<td width="70"><span class="center"><?php echo $v["title"]; ?></span></td>
							<td width="170"><span class="center"><?php echo $v["fname"]; ?></span></td>
							<td><span class="center"><?php echo $v["lname"]; ?></span></td>
						</tr>			
						<?php 
							$i++;
						endforeach ?>
				</tbody>
				<?php 
				} ?>
			</table>
		</div>
	</div>
</body>
<script type="text/javascript">
function printPage(){
	   window.print();
	   setTimeout(" parent.$.fancybox.close()",1000);
	}
</script>
</html>
<?php } 
?>