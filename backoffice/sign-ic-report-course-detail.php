<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
global $db;
$id = $_GET["course_detail_id"];
if($_GET["type"]=="detail"){
	include_once ('coursedetail.php');
}else{
$section = datatype(" and a.active='T'", "section", true);
$coursetype = datatype(" and a.active='T'", "coursetype", true);
?>
	<div id="cl-wrapper">
		<div class="container-fluid" id="pcont">
			<div class="cl-mcont">
				<div class="col-sm-12">
					<div class="content block-flat ">
						<div class="page-head">
							
							<h3><i class="fa fa-list"></i> &nbsp;ใบเซ็นชื่อ IC สอบ</h3>
						</div>
						<div class="header">
							<div class="form-group row">
								<label class="col-sm-2 control-label">วันที่  <span class="red">*</span></label>
								<div class="col-sm-2">
									<input class="form-control" name="date_start" id="date_start" onblur="reCall();" placeholder="วันที่" type="text">
								</div>                                          
								<label class="col-sm-1 control-label">ถึงวันที่  <span class="red">*</span></label>
								<div class="col-sm-2">
									<input class="form-control" name="date_stop" id="date_stop" onblur="reCall();" placeholder="ถึงวันที่" type="text">
								</div>  							
								<label class="col-sm-1 control-label"> <a href="#" class="btn btn-rad btn-info" onClick="reCall();"><i class="fa fa-search"></i></a></label>               
							</div>
						</div>
						<table id="tbCourse" class="table" style="width:100%">
							  <thead>
		                        <tr class="alert alert-success" style="font-weight:bold;">
		                            <td width="5%">ลำดับ</td>
		                            <td width="10%" class="center">วัน</td>
		                            <td width="10%" class="center">เวลา</td>
		                            <td width="10%" class="center">สถานที่</td>
		                            <td width="20%" class="center">ข้อมูลสถานที่สอบ/อบรม</td>
		                            <td width="10%" class="center">เปิดรับสมัคร</td>
		                            <td width="10%" class="center">ปิดรับสมัคร</td>
		                            <td width="5%" class="center">สถานะ</td>
		                            <td width="5%" class="center">จัดการ</td>
		                        </tr>
							  </thead>   
							<tbody>
							</tbody>
						</table>
						<div class="clear"></div>
					</div>
				</div>

			</div>
		</div> 
	</div>
	<?php include ('inc/js-script.php') ?>

	<script type="text/javascript">
	$(document).ready(function() {
        $("#date_start").datepicker({language:'th-th',format:'dd-mm-yyyy'});
        $("#date_stop").datepicker({language:'th-th',format:'dd-mm-yyyy'});  
		var oTable;
		listItem();	
	});

	function listItem(){
	   var url = "data/coursedetaillist.php";
	   oTable = $("#tbCourse").dataTable({
		   "sDom": 'T<"clear">lfrtip',
		   "oLanguage": {
	   	   "sInfoEmpty": "",
	   		"sInfoFiltered": ""
							  },
			"oTableTools": {
				"aButtons":  ""
			},
			"bProcessing": true,
			"bServerSide": true,
			"sAjaxSource": url,
			"sPaginationType": "full_numbers",
			"aaSorting": [[ 0, "desc" ]],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				aoData.push({"name":"section_id","value":1});			
				aoData.push({"name":"coursetype_id","value":2});			
				aoData.push({"name":"date_start","value":$("#date_start").val()});			
				aoData.push({"name":"date_stop","value":$("#date_stop").val()});			
				aoData.push({"name":"active","value":'T'});			
				aoData.push({"name":"report","value":'ic_sob'});			
				$.ajax( {
					"dataType": 'json', 
					"type": "POST", 
					"url": sSource, 
					"data": aoData, 
					"success": fnCallback
				});
			}
	   }); 
	}

	function editInfo(id){
		if(typeof id=="undefined") return;
	   var url = "index.php?p=<?php echo $_GET["p"];?>&course_detail_id="+id+"&type=info";
	   redirect(url);
	}

	function reCall(){
		oTable.fnClearTable( 0 );
		oTable.fnDraw();
	}

	</script>
<?php } ?>