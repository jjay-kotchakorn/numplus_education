<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
global $db;
require_once dirname(__FILE__) . '/PHPExcel.php';
$info = get_course_detail(" and a.course_detail_id={$_GET["course_detail_id"]}");
$course_info = get_course(" and a.course_id={$_GET["course_id"]}");
if($info){
	$info = $info[0];
	$course_info = $course_info[0];
	$course_detail_id = $info["course_detail_id"];
	$course_id = $course_info["course_id"];
	$fileName ="";
?>
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="row">
				<div class="col-md-12">           
					<div class="block-flat">
						<div class="header">              
							<ol class="breadcrumb">
								<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>');">หน้าหลัก</a></li>
								<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>&type=select-course');">เลือกหลักสูตร</a></li>
								<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>&course_id=<?php echo $course_id; ?>&type=select-course-detail');">เลือกสถานที่</a></li>
								<li class="active">รายชื่อผู้สอบ <?php echo $course_info["title"]; ?></li>
								<li class="pull-right"><a href="#" onClick="exportToExcel();">Export</a></li>
							</ol>
						</div>
						<div class="content" id="export">							
							<h3 class="text-center">รายชื่อผู้สอบ <?php echo $course_info["title"]; ?></h3>
							<h4 class="text-center">วันที่ <?php echo revert_date($info["date"])."&nbsp;เวลา ".$info["time"]; ?> น.</h4>
							<h4 class="text-center"><?php echo $info['address_detail']." ".$info['address'] ?></h4>
							<div class="report">
								<table>
									<thead>
										<tr >
											<th>เลขที่นั่ง</th>
											<th>คำนำหน้า</th>
											<th>ชื่อ</th>
											<th>นามสกุล</th>
											<th>เลขที่บัตรประชาชน</th>
											<th>หน่วยงาน</th>
											<th>อีเมล์</th>
											<th>โทรศัพท์บ้าน</th>
											<th>โทรศัพท์ที่ทำงาน</th>
											<th>โทรศัพท์มือถือ</th>
											<th>ลายมือชื่อเข้าสอบ</th>
											<th>ลายมือรับผลสอบ</th>
											<th>หมายเหตุ<br>รายมือชื่อรับใบเสร็จ</th>
											<th>ยินยอมเปิด<br>เผยข้อมูลให้กับ</th>
										</tr>
									</thead>
									<?php 

									$ids = array();
									$q = " select register_id from register_course_detail where active='T' and course_id='$course_id' and course_detail_id=$course_detail_id";
									$get_all = $db->get($q);
									if($get_all){
										foreach ($get_all as $key => $value) {
											$ids[] = $value["register_id"];
										}
									}
									$q = " select register_id from register where active='T' and course_id='$course_id' and course_detail_id='$course_detail_id'";
									$get_register_all = $db->get($q);
									if($get_register_all){
										foreach ($get_register_all as $key => $value) {
											$ids[] = $value["register_id"];
										}
									}
									$t = array_unique($ids);
									$con_ids = implode(",", $t);
									$q = "select a.title,
												 a.fname,
												 a.lname,
												 a.cid,
												 a.branch,
												 b.email1,
												 b.tel_home,
												 b.tel_mobile,
												 b.tel_office
									from register a inner join member b on a.member_id=b.member_id
									where a.register_id in ($con_ids) and a.pay_status in (3,5,6) and a.active='T' ";
									$r = $db->get($q);
									if($r){
										$i = 1;
										$objPHPExcel = new PHPExcel();
										$objPHPExcel->setActiveSheetIndex(0);
										$rowCount = 2;
										$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
																	 ->setLastModifiedBy("Maarten Balliauw");

										$objPHPExcel->setActiveSheetIndex(0)
										            ->setCellValue('A1', 'เลขที่นั่ง')
										            ->setCellValue('B1', 'คำนำหน้า')
										            ->setCellValue('C1', 'ชื่อ')
										            ->setCellValue('D1', 'นามสกุล')
													->setCellValue('E1', 'เลขที่บัตรประชาชน')
													->setCellValue('F1', 'หน่วยงาน')
										            ->setCellValue('G1', 'อีเมล์')
										            ->setCellValue('H1', 'โทรศัพท์บ้าน')
										            ->setCellValue('I1', 'โทรศัพท์ที่ทำงาน')
										            ->setCellValue('K1', 'โทรศัพท์มือถือ')
										            ->setCellValue('K1', 'ลายมือชื่อเข้าสอบ')
										            ->setCellValue('L1', 'ลายมือรับผลสอบ')
										            ->setCellValue('M1', 'ยินยอมเปิดเผยข้อมูลให้กับ')
										            ->setCellValue('N1', 'หมายเหตุรายมือชื่อรับใบเสร็จ')
										            ->setCellValue('O1', 'ยินยอมเปิดเผยข้อมูลให้กับ');
									?>
									<tbody>
									<?php foreach ($r as $key => $value): 
										$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount,$i);
										$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount,  $value["title"]);
										$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount,  $value["fname"]);
										$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount,  $value["lname"]);
										$objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount,  $value["cid"]);
										$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount,  $value["branch"]);
										$objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount,  $value["email1"]);
										$objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount,  $value["tel_home"]);
										$objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount,  $value["tel_mobile"]);
										$objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount,  $value["tel_office"]);
										$rowCount++;
									?>					
										<tr>					
											<td><?php echo $i; ?></td>
											<td><?php echo $value["title"]; ?></td>
											<td><?php echo $value["fname"]; ?></td>
											<td><?php echo $value["lname"]; ?></td>
											<td><?php echo $value["cid"]; ?></td>
											<td><?php echo $value["branch"]; ?></td>
											<td><?php echo $value["email1"]; ?></td>
											<td><?php echo $value["tel_home"]; ?></td>
											<td><?php echo $value["tel_mobile"]; ?></td>
											<td><?php echo $value["tel_office"]; ?></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
											<?php 
											$i++;
										endforeach ?>
									</tbody>
									<?php 
										$strPath = realpath(basename(getenv($_SERVER["SCRIPT_NAME"])));
										@unlink($strPath."/report/sign-ic-report.xlsx");
										$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
										$objWriter->save($strPath.'/report/sign-ic-report.xlsx');
										$fileName = "report/sign-ic-report.xlsx";
									} ?>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div> 
</div>
<?php } 


?>
<form class="form-horizontal" id="frmMain" method="post" action="saveExcel.php">
	<input type="hidden" name="filename" id="filename" value="xx">
	<input type="hidden" name="dataHTML" id="dataHTML"></form>
<?php include ('inc/js-script.php') ?>
<script type="text/javascript">
	function exportToExcel(){
	   var url = "<?php echo $fileName; ?>";
	   if(!url) return;
	   window.open(url);
	}	
</script>