<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
global $db, $SECTIONID, $COURSETYPEID;

$error = $_SESSION["error"]["msg"];
unset($_SESSION["error"]["msg"]);
$success = $_SESSION["success"]["msg"];
unset($_SESSION["success"]["msg"]);


$course_id = $_GET["course_id"];
$flag = $_GET["flag"];
$course_detail_id = $_GET["course_detail_id"];
$code_project = $_GET["code_project"];

if ($course_detail_id && $code_project && $flag=="code_project") {
	$code_project = trim($code_project);
	$qry = "UPDATE course_detail
		SET code_project='$code_project'
		WHERE course_detail_id=$course_detail_id";
	$res = $db->set_update($qry);
	if ($res) {
		exit();
	}
}

$con_section_id = ($SECTIONID>0) ? " and a.section_id={$SECTIONID}" : "";
$con_coursetype_id = ($COURSETYPEID>0) ? " and a.coursetype_id={$COURSETYPEID}" : "";
$section = datatype(" and a.active='T' {$con_section_id}", "section", true);
if($course_detail_id || $con_coursetype_id)
	$coursetype = datatype(" and a.active='T' {$con_coursetype_id}", "coursetype", true);

$q = "select  title, coursetype_id, section_id from course where course_id=$course_id";
$course_info = $db->rows($q);
if($course_info){
	$course_name = $course_info["title"];
	$section_id = $course_info["section_id"];
	$coursetype_id = $course_info["coursetype_id"];
	if(!$course_detail_id){
		$SECTIONID = $section_id;
		$COURSETYPEID = $coursetype_id;
	}
}

$cos_dtail = get_course_detail("", $course_detail_id);
$cos_dtail = $cos_dtail[0];

//d($cos_dtail);

$q = "select  day, date, time  from course_detail where course_detail_id=$course_detail_id";
$r = $db->rows($q);
$str = "";
if($r){
	$str = $r["day"]." ".revert_date($r["date"])." ".$r["time"];
}else{
	$str = "เพิ่มรายการ";
}

?>
<link rel="stylesheet" type="text/css" href="js/bootstrap.summernote/dist/summernote.css" />
<link rel="stylesheet" type="text/css" href="js/jquery.nanoscroller/nanoscroller.css" />
<link href="js/jquery.icheck/skins/square/blue.css" rel="stylesheet">
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="row">
					<div class="col-md-12">           
					<div class="block-flat">						             
						<ol class="breadcrumb">
							<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>');">หน้าหลัก</a></li>
							<?php if($flag!="detail"): ?>
								<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>&type=info&course_id=<?php echo $course_id ?>');"><?php echo $course_name; ?></a></li>
							<?php endif ?>
							<li class="active"><?php echo $str; ?></li>
						</ol>				
						<div class="content">
							<form id="frmMain" name="frmMain" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="update-course-detail.php">
							<input type="hidden" name="course_id" id="course_id" value="<?php echo $course_id; ?>">
							<input type="hidden" name="flag" id="flag" value="<?php echo $flag; ?>">
							<input type="hidden" name="course_detail_id" id="course_detail_id" value="<?php echo $course_detail_id; ?>">
							<div class="form-group row">
								<label class="col-sm-2 control-label">รหัสโครงการ </label>
								<div class="col-sm-2" >
									<input class="form-control" id="code_project" name="code_project"  placeholder="รหัสโครงการ" type="text">
								</div> 
								<label class="col-sm-2 control-label">ประเภทใบอนุญาต/คุณวุฒิ <span class="red">*</span></label>
								<div class="col-sm-2">
									<select name="section_id" id="section_id" class="form-control required">
										<option value="">---- เลือก ----</option>
										<?php foreach ($section as $key => $value) {
											$id = $value['section_id'];
											if($SECTIONID>0 && $SECTIONID==$id){
												$select = "selected";
											}else{
												$select = "";
											}
											$name = $value['name'];
											echo  "<option {$select} value='$id'>$name</option>";
										} ?>

									</select>
								</div>                
								<label class="col-sm-2 control-label">ประเภทหลักสูตร<span class="red">*</span></label>
								<div class="col-sm-2">
									<select name="coursetype_id" id="coursetype_id" class="form-control required">
										<option value="">---- เลือก ----</option>
										<?php foreach ($coursetype as $key => $value) {
											$id = $value['coursetype_id'];
											if($COURSETYPEID>0 && $COURSETYPEID==$id){
												$select = "selected";
											}else{
												$select = "";
											}
											$name = $value['name'];
											echo  "<option {$select} value='$id'>$name</option>";
										} ?>

									</select>
								</div>
							</div>  							
							<div class="col-sm-12 row">
								<div class="form-group row">
									<label class="col-sm-2 control-label">วันที่สอบ/อบรม/อบรมต่ออายุ <span class="red" style="display:inline-block; position:absolute;"> &nbsp;*</span></label>
									<div class="col-sm-2">
										<input class="form-control required" id="date" name="date" required="" placeholder="วันที่สอบ/อบรม/อบรมต่ออายุ" type="text">
									</div>
									<label class="col-sm-2 control-label" style="padding-left: 5px; padding-right: 5px;">รายละเอียด</label>
									<div class="col-sm-6">
										<input class="form-control" name="date_other" id="date_other"  placeholder="รายละเอียด" type="text">
									</div> 
								</div>
								<div class="form-group row">
									<label class="col-sm-2 control-label">เวลา<span class="red">*</span></label>
									<div class="col-sm-2">
											<input class="form-control required" id="time" name="time" required="" placeholder="เวลา" type="text">
									</div>                
									<label class="col-sm-2 control-label">จังหวัด<span class="red">*</span></label>
									<div class="col-sm-2">
										<input class="form-control required" name="address" id="address" required="" placeholder="จังหวัด" type="text">
									</div>
									 <label class="checkbox-inline" style="padding-left:10px;padding-top:0px"> 
										 <div aria-disabled="false" aria-checked="false" style="position: relative;" 
										 	class="icheckbox_square-blue"><input style="position: absolute; opacity: 0;" 
										 	value="<?php echo $cos_dtail['inhouse'];?>" name="inhouse" id="inhouse" class="icheck" type="checkbox">
										 	<ins style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;" class="iCheck-helper"></ins>
										 </div>&nbsp;&nbsp;<a data-toggle="tooltip" href="#" 
										 	data-original-title="กรณีเป็นหลักสูตร In House  admin สามารถตั้งราคาของหลักสูตรได้เอง">In House</a>
									 </label>
								</div>                        
								<div class="form-group row">                                        
								<label class="col-sm-2 control-label">ข้อมูลสถานที่สอบ/อบรม  <span class="red">*</span></label>
								<div class="col-sm-10">
									<input class="form-control required" name="address_detail" id="address_detail" required="" placeholder="ข้อมูลสถานที่สอบ/อบรม" type="text">
								</div>
								</div>                        
								<div class="form-group row">
									<label class="col-sm-2 control-label">วันที่เพิ่มเติม (กรณีเป็นชุดวันที่)</label>
									<div class="col-sm-10">
										<textarea id="remark" class="summernote" name="remark" placeholder="Description"></textarea>
									</div>
								</div>
								<div class="form-group row">                                        
								<label class="col-sm-2 control-label">จำนวนรับสมัคร</label>
								<div class="col-sm-2">
									<input class="form-control number" name="chair_all" id="chair_all"  placeholder="จำนวนรับสมัคร" type="text">
								</div>                                          
								<label class="col-sm-2 control-label">วันที่เปิดรับสมัคร  <span class="red">*</span></label>
								<div class="col-sm-2">
									<input class="form-control" name="open_regis" id="open_regis"  placeholder="วันที่เปิดรับสมัคร" type="text">
								</div>                                          
								<label class="col-sm-2 control-label">วันที่ปิดรับสมัคร  <span class="red">*</span></label>
								<div class="col-sm-2">
									<input class="form-control" name="end_regis" id="end_regis"  placeholder="วันที่ปิดรับสมัคร" type="text">
								</div>                                          
								</div>
								<div class="form-group row">

								<label class="col-sm-2 control-label">ส่วนลด</label>
								<div class="col-sm-2">
									<input class="form-control number" name="discount" id="discount"  placeholder="ส่วนลด" type="text">
								</div>                                          
								<label class="col-sm-2 control-label">วันที่</label>
								<div class="col-sm-2">
									<input class="form-control" name="date_start" id="date_start"  placeholder="วันที่" type="text">
								</div>  
								<label class="col-sm-2 control-label">ถึงวันที่ </label>
								<div class="col-sm-2">
									<input class="form-control" name="date_stop" id="date_stop"  placeholder="ถึงวันที่" type="text">
								</div>
							</div>
								<div class="form-group row">                                            
									<!-- <label class="col-sm-1 control-label">แสดงที่ Front  <span class="red">*</span></label>
									<div class="col-sm-2">
										<select name="active" id="active" class="form-control required">
											<option selected="selected" value="T">แสดง</option>
											<option value="F">ไม่แสดง</option>
										</select>
									</div> -->                                           
									<label class="col-sm-2 control-label">สถานะการรับสมัคร  <span class="red">*</span></label>
									<div class="col-sm-2">
										<select name="status" id="status" class="form-control required">
											<option selected="selected" value="เปิดรับสมัคร">เปิดรับสมัคร</option>
											<option value="ปิดรับสมัคร">ปิดรับสมัคร</option>
											<option value="ปิดรับสมัครชั่วคราว">ปิดรับสมัครชั่วคราว</option>
											<option value="ยกเลิกรอบสอบ">ยกเลิกรอบสอบ</option>
										</select>
									</div>
  
								<label class="col-sm-2 control-label">การจองสำรองที่นั่ง </label>
								<div class="col-sm-2">
									<input class="form-control" name="book" id="book"  placeholder="การจองสำรองที่นั่ง" type="text">
								</div>
								</div>
							</div>
							</div>
							<div class="clear"></div><hr>
							<div class="form-group row" style="padding-left:60px;">
								<div class="col-sm-12">
									<button type="button" class="btn btn-primary" onClick="ckSave()">Save changes</button>
									<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p'] ?>');">Cancel</button>
								</div>
							</div>
						</form>
						</div>
					</div>
					
					</div>
				</div>

		</div>
	</div> 
</div>
<?php include_once ('inc/js-script.php'); ?>
<script type="text/javascript" src="js/bootstrap.summernote/dist/summernote.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
	var nowDate = new Date();
	var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);		
	 $("#frmMain").validate({
		  submitHandler: function(form) {
		    onCkForm("#frmMain");
		    form.submit();
		  }
		});
	 var course_detail_id = "<?php echo $_GET["course_detail_id"]?>";
	 if(course_detail_id) viewInfo(course_detail_id);
	 var flag = "<?php echo $_GET["flag"]; ?>";
	 $("#flag").val(flag);
	 // $("#date").datepicker({language:'th-th',format:'dd-mm-yyyy',startDate: today });
	 $("#date").datepicker({language:'th-th',format:'dd-mm-yyyy' });
	 $("#open_regis").datepicker({language:'th-th',format:'dd-mm-yyyy'});
	 $("#end_regis").datepicker({language:'th-th',format:'dd-mm-yyyy'});
	 $('#time').mask('99:99 - 99:99');
   $('#remark').summernote({
            height: 100
        }); 
     $("#open_regis, #end_regis").blur(function(event) {
		check_date();
     });
	/* $("#date_start").datepicker({language:'th-th',format:'dd-mm-yyyy',startDate: today });*/
	 $("#date_start").datepicker({language:'th-th',format:'dd-mm-yyyy'});
	 $("#date_stop").datepicker({
	 	language:'th-th',
	 	format:'dd-mm-yyyy'
	 });
   	 $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue checkbox',
        radioClass: 'iradio_square-blue'
      });
     $("#date_start, #date_stop").blur(function(event) {
     	var from=$("#date_start").datepicker('getDate');
		var to =$("#date_stop").datepicker('getDate');
		var ckto = strTrim($("#date_stop").val());
		var ckf = strTrim($("#date_start").val());
		var msg = "";
		if(ckto=="" || ckf=="") return false;		
		if(to<from){
			msgError("วันที่ส่วนลดไม่ถูกต้อง");
			$("#date_stop").val("");
		}
     });
		
		var course_name = "<?php echo $course_name;?>";
		if ( course_name!="" ) {
			var section_id = "<?php echo $section_id;?>";
			getDropDown('#coursetype_id', section_id, 'coursetype', 'data/coursetype.php');
		}else{
			var section_id = $("#section_id").val();
		}//end else

		$("#section_id").change(function(event) {
			var section_id = $(this).val();
			getDropDown('#coursetype_id', section_id, 'coursetype', 'data/coursetype.php');
		}); 


	 // show succes
	 ///////////////////////////////////////////////
	 var error = "<?php echo $error ?>";
     if(error!=""){
		$.gritter.removeAll({
	        after_close: function(){
	          $.gritter.add({
	          	position: 'center',
		        title: 'Error',
		        text: error,
		        class_name: 'danger'
		      });
	        }
	     });
     }
     var success = "<?php echo $success ?>";
     if(success!=""){
		$.gritter.removeAll({
	        after_close: function(){
	          $.gritter.add({
	          	position: 'center',
		        title: 'success',
		        text: success,
		        class_name: 'success'
		      });
	        }
	    });
	 }
	 /////////////////////////////////////////////	

 });
 function viewInfo(course_detail_id){
	 if(typeof course_detail_id=="undefined") return;
	 getInfo(course_detail_id);
}

function check_date(){
	var d = $("#date").datepicker('getDate');
	var from= $("#open_regis").datepicker('getDate');
	var to = $("#end_regis").datepicker('getDate');
	var ckto = strTrim($("#end_regis").val());
	var ckf = strTrim($("#open_regis").val());
	var msg = "";
	if(ckto=="" || ckf=="") return false;
/*
	if(from>=d){
		msg += "วันที่เปิดรับสมัคร ต้องก่อน วันที่สอบ/อบรม/อบรมต่ออายุ  ";
		$("#open_regis").val("");			
	}
*/
/*
	if(to>=d){
	msg += "วันที่ปิดรับสมัคร ต้องก่อน วันที่สอบ/อบรม/อบรมต่ออายุ";
	$("#end_regis").val("");
	}
	*/
	if(msg!=""){
		msgError(msg);
		return true;
	}
	return false;
}
function getInfo(id){
	if(typeof id=="undefined") return;
	var url = "data/courselist.php";
	var param = "course_detail_id="+id+"&single=detail";
	dataUrl(url, param,"#frmMain");
}


function ckSave(id){
	var t = check_date();
	if(t) return false;
	
 	var tmp = $('#remark').code();
 	$('#remark').val(tmp);

 	// var ih = $('#inhouse').val();
 	// console.log(ih);

	$("#frmMain").submit();

	if ($("#status").val() == 'ยกเลิกรอบสอบ') {
		//console.log($("#status").val());
		var course_detail_id = "<?php echo $course_detail_id;?>";
		console.log(course_detail_id);
		$.ajax({
			"type": "POST",
			"async": false, 
			"url": "data/cancel-course-sendmail.php",
			"data": {'course_detail_id': course_detail_id, 'flag': 'T'}, 
			"success": function(data){	
				console.log(data);			      							 
			}
		});
	};

} 
</script>