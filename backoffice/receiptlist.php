<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/member.php";
global $db;
$receipttype = datatype(" and a.active='T'", "receipttype", true);
?>
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="col-sm-12">
				<div class="content block-flat ">
					<div class="page-head">
						<button class="btn btn-success btn-small pull-right" onclick="addnew()" style="margin-top:10px;"><i class="fa fa-plus"></i> เพิ่มบริษัท</button>
						<h3><i class="fa fa-list"></i> &nbsp;ข้อมูลบริษัท</h3>
					</div>
					<div class="form-group row">
					<label class="col-sm-2 control-label">ประเภทบริษัท <span class="red">*</span></label>
						<div class="col-sm-3">
							<select name="receipttype_id" id="receipttype_id" class="form-control" onchange="reCall();">
								<option value="">---- เลือก ----</option>
								<?php foreach ($receipttype as $key => $value) {
									$id = $value['receipttype_id'];
									if($id==5) continue;
									$name = $value['name'];
									echo  "<option value='$id'>$name</option>";
								} ?>

							</select>
						</div>
								<label class="col-sm-1 control-label">สถานะ</label>
								<div class="col-sm-2">
									<select name="active" id="active" class="form-control" onchange="reCall();">
										<option selected="selected" value="T">แสดง</option>
										<option value="F">ไม่แสดง</option>
									</select>
								</div> 
					</div>
					<table id="tbreceipt" class="table" style="width:100%">
						  <thead>
							  <tr>
								  <th width="8%">ลำดับ</th>
								  <th width="8%">ID</th>
								  <th width="18%">เลขที่บัตรประจำตัวผู้เสียภาษี</th>
								  <th width="36%">รายการ</th>
								  <th width="15%">ประเภทใบอนุญาต/คุณวุฒิ</th>
								  <th width="10%">สาขา</th>
								  <th width="11%">Manage</th>
							  </tr>
						  </thead>   
						<tbody>
						</tbody>
					</table>
					<div class="clear"></div>
				</div>
			</div>

		</div>
	</div> 
</div>
<?php include ('inc/js-script.php') ?>

<script type="text/javascript">
$(document).ready(function() {
	var oTable;
	$("#frmMain").validate();
	listItem();	
});

function listItem(){
   var url = "data/receiptlist.php";
   oTable = $("#tbreceipt").dataTable({
	   "sDom": 'T<"clear">lfrtip',
	   "oLanguage": {
   	   "sInfoEmpty": "",
   		"sInfoFiltered": ""
						  },
		"oTableTools": {
			"aButtons":  ""
		},
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": url,
		"sPaginationType": "full_numbers",
		"aaSorting": [[ 0, "desc" ]],
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			aoData.push({"name":"receipttype_id","value":$("#receipttype_id").val()});
			aoData.push({"name":"active","value":$("#active").val()});	
			$.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});
		}
   }); 
}

function editInfo(id){
	if(typeof id=="undefined") return;
   var url = "index.php?p=<?php echo $_GET["p"];?>&receipt_id="+id+"&type=info";
   redirect(url);
}
function addnew(){
   var url = "index.php?p=<?php echo $_GET["p"];?>&type=info";
   redirect(url);
}

function reCall(){
	oTable.fnClearTable( 0 );
	oTable.fnDraw();
}

</script>