<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/member.php";
global $db;

$id = $_POST["id"];
$con = "";
if($_POST["othercon"]){
    $a = explode("-", $_POST["othercon"]);
    $con_id = trim($a[1],",");
    if($con_id){
       $con .= " and a.{$a[0]} in ($con_id)";
    }   
}

$con .= ($_POST["active"]) ? " and a.active='{$_POST["active"]}'" : "";
$search = $db->escape(trim($_POST["search"]));
$cid = $db->escape(trim($_POST["cid"]));
if($search){
    $con .=  " and (a.fname_th like '%$search%' or a.lname_th like '%$search%' or a.fname_en like '%$search%' or a.lname_en like '%$search%')";
}
if($cid){
    $con .= " and a.cid like '%$cid%'";
}
$array_data = array();
if($con){
    $r = view_member($con, "", false, 50);
	   foreach($r as $k=>$v){
          $v["name_th"] = $v["title_th"]."".$v["fname_th"]." ".$v["lname_th"];
          $v["name_eng"] = $v["title_en"]."".$v["fname_en"]." ".$v["lname_en"];
	      $array_data[] = $v;
	   }      
}

echo json_encode($array_data); ?>

