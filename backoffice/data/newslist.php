<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/news.php";
global $db, $RIGHTTYPEID, $SECTIONID;

$single_info = $_POST["single"];
if($single_info=="T"){
	$aData = array();
	$id = $_POST["news_id"];
	if($id){
	   $r = view_news("", $id);
	   foreach($r as $k=>$v){
	      $aData[] = $v;
	   }  
	}
}else{

function fnColumnToField( $i ){
	/* Note that column 0 is the details column */
	if ( $i == 0 ||$i == 1 )
		return "a.news_id";
	else if ( $i == 2 )
		return "a.code";
	else if ( $i == 3 )
		return "a.name";
	else if ( $i == 4 )
		return "a.section_id";
	else if ( $i == 5 )
		return "a.highlight";
}

$sLimit = "";
if (isset( $_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1' )
{
	$sLimit = "LIMIT ".(int)($_POST['iDisplayStart'] );
	$sLimit .= ", ".(int)( $_POST['iDisplayLength'] );
}


/* Ordering */
if(isset($_POST['iSortCol_0'])){
	$sOrder = "ORDER BY  ";
	for ( $i=0 ; $i<$db->escape( $_POST['iSortingCols'] ) ; $i++ ){
		$sOrder .= fnColumnToField($db->escape( $_POST['iSortCol_'.$i] ))."
                ".$db->escape( $_POST['sSortDir_'.$i] ) .", ";
	}
	$sOrder = substr_replace( $sOrder, "", -2 );
}
 
/* Filtering */
  $sWhere = "";
  $WHERE = "WHERE a.active!='' ";
  $sAND = "";
if($_POST['sSearch'] != ""){
   $sWhere = "a.code LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "a.name LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "b.name LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "a.highlight LIKE '%".$db->escape( $_POST['sSearch'] )."%'";
	$sAND = "AND ";
}
if($SECTIONID>0){
	if($RIGHTTYPEID==1 || $RIGHTTYPEID==4){

	}else{
		$sWhere .= " and a.section_id=$SECTIONID";
	}
}

/* Paging */
$sQuery = "SELECT a.news_id, a.code, a.name, a.section_id, a.highlight, b.name as section_name, a.active
           FROM news a left join section b on b.section_id=a.section_id
		   $WHERE $sAND $sWhere
		   $sOrder
		   $sLimit";

$rResult = $db->get($sQuery);
$a = array();
if(is_array($rResult)){
	$runNo = 1;
	foreach ($rResult as $r){
		$id = $r["news_id"]; 
	  $manage =  get_datatable_icon("edit", $id);
	  $active = ($r["active"]=="T") ? "active" : "nonActive";   
		$a[] = array($runNo
				      ,$r['code']
				      ,$r['name']
				      ,$r['section_name']
				      ,$r["highlight"]=="T" ? 'Enable' : 'Disable'
				      ,$r["active"]=="T" ? "แสดง" : "ไม่แสดง" 
				      ,$manage);
		$runNo++;
	}
}

$aData = array();
$sQuery = "SELECT COUNT(*) as total
			  FROM news a
			  $WHERE $sAND $sWhere";

$rs = $db->data($sQuery);
$iFilteredTotal = $rs;
 
$sQuery = "SELECT COUNT(*) as total
			  FROM news a";
$resultTotal = $db->data($sQuery);
$iTotal = $resultTotal;
						 
$aData["sEcho"] = intval($_POST['sEcho']);
$aData["iTotalRecords"] = $iTotal; 
$aData["iTotalDisplayRecords"] = $iFilteredTotal; 
$aData["aaData"] = $a; 

}

echo json_encode($aData);
?>
