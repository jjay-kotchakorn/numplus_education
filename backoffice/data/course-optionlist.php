<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/course.php";
global $db;

$search = $db->escape(trim($_GET["q"]));
$con = "";
if($search){
    $con .=  " and (a.code like '%$search%' or a.title like '%$search%' or a.detail like '%$search%')";
}

$array_data = array();
if($con){
    $r = get_course($con);
	   foreach($r as $k=>$v){

	      $array_data[] = array('id'=> $v["course_id"], "text"=>$v["title"]);
	   }      
}

echo json_encode($array_data); ?>

