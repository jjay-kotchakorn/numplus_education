<?php
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
global $db;
$datetime_now = date('Y-m-d H:i:s');

$str_txt = "";
$str_txt .= "test run cronjob-running-10min";
// write_log("cronjob-running-10min", $str_txt);

//unset($rs);
//include file for running in cron job
$q = "SELECT
			a.cronjob_qman_list_id,
			a.cronjob_qman_id,
			a.`code`,
			a.path_file,
			a.file_name,
			a.`name`,
			a.name_eng,
			a.active,
			a.recby_id,
			a.rectime,
			a.remark
	  FROM cronjob_qman_list a
	  WHERE a.active='T' 
	  	AND a.cronjob_qman_id=1
";
// echo $q;
$rs = $db->get($q);
// d($rs); die();
// include_once "con-register-elearning.php";
foreach ($rs as $key => $v) {
	// d($v);die();
	$file_name = trim($v["file_name"]);
	if ( empty($file_name) ){
		continue;
	}else{
		include_once "{$file_name}";

		$cron_name = $v["name"];
		$path_file = $v["path_file"];
		$cron_file_name = $v["file_name"];


		if ( $cron_name=="con-register-elearning" ) {
			/*
				1.upload file reconcile & map ref1, ref2
				2.run cronjob 10 min
			*/
			$register_ids = trim($v["remark"]);

			$register_ids = explode(",", $register_ids);
			
			// d($register_ids); die();

			if ( count($register_ids)>0 ) {


				$str_txt = "";
				$str_txt .= "{$file_name} run in cronjob-running-10min";
				write_log("cronjob-running-10min", $str_txt);


				foreach ($register_ids as $key => $register_id) {
					if ( !empty($register_id) ) {
						$q = "SELECT pay_status FROM register WHERE active='T' AND register_id={$register_id}";
						$pay_status = $db->data($q);
						//write log
						$str_txt = "";
						$str_txt .= "func=main_Reconcile|register_id={$register_id}|pay_status={$pay_status}";
						write_log("check-course-elearning", $str_txt);
						
						// $rs = check_course_elearning($register_id);
						$rs = check_course_elearning($register_id);
					}//end if
				}//end loop $register_id

				$args = array();
				$args["table"] = "cronjob_qman_list";
				$args["id"] = 1;
				$args["rectime"] = $datetime_now;
				$args["remark"] = "";
				$db->set($args);

			}//end if
			
		}else if ( $cron_name=="con-check-auto-del" ) {	

			$str_txt = "";
			$str_txt .= "{$file_name}|run in cronjob-running-10min";
			write_log("cronjob-running-10min", $str_txt);

			// include_once "con-check-auto-del.php";
			// die();
		}//end else if



	} //end else	
}//end loop $v







?>
