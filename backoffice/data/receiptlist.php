<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/member.php";
include_once "../share/datatype.php";
global $db;

$single_info = $_POST["single"];
if($single_info=="T"){
	$aData = array();
	$id = $_POST["receipt_id"];
	if($id){
 		$receipt = get_receipt('', $id);
        if($receipt>0){
        	foreach ($receipt as $key => $v) {
        		 $aData[] = $v;
        	}
        }
	}
}else{

function fnColumnToField( $i ){
	/* Note that column 0 is the details column */
	if ( $i == 0 ||$i == 1 )
		return "a.receipt_id";
	else if ( $i == 2 )
		return "a.code";
	else if ( $i == 3 )
		return "a.name";
	else if ( $i == 4 )
		return "a.receipttype_id";
	else if ( $i == 5 )
		return "a.taxno";
}

$sLimit = "";
if (isset( $_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1' )
{
	$sLimit = "LIMIT ".(int)($_POST['iDisplayStart'] );
	$sLimit .= ", ".(int)( $_POST['iDisplayLength'] );
}


/* Ordering */
if(isset($_POST['iSortCol_0'])){
	$sOrder = "ORDER BY  ";
	for ( $i=0 ; $i<$db->escape( $_POST['iSortingCols'] ) ; $i++ ){
		$sOrder .= fnColumnToField($db->escape( $_POST['iSortCol_'.$i] ))."
                ".$db->escape( $_POST['sSortDir_'.$i] ) .", ";
	}
	$sOrder = substr_replace( $sOrder, "", -2 );
}
 
/* Filtering */
  $sWhere = "";
  $WHERE = "WHERE a.active!='' ";
  $sAND = "";
if($_POST['sSearch'] != ""){
   $sWhere = "a.code LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "a.name LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "b.name LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "a.taxno LIKE '%".$db->escape( $_POST['sSearch'] )."%'";
	$sAND = "AND ";
}
$sWhere .= ($_POST["receipttype_id"]) ? " and a.receipttype_id={$_POST["receipttype_id"]}" : "";
$sWhere .= ($_POST["active"]) ? " and a.active='{$_POST["active"]}'" : "";
/* Paging */
$sQuery = "SELECT a.receipt_id, a.code, a.name, a.receipttype_id, a.taxno, b.name as receipttype_name, a.active, a.branch
           FROM receipt a left join receipttype b on b.receipttype_id=a.receipttype_id
		   $WHERE $sAND $sWhere
		   $sOrder
		   $sLimit";

$rResult = $db->get($sQuery);
$a = array();
if(is_array($rResult)){
	$runNo = 1;
	foreach ($rResult as $r){
		$id = $r["receipt_id"]; 
	  $manage =  get_datatable_icon("edit", $id);
	  $active = ($r["active"]=="T") ? "active" : "nonActive";   
		$a[] = array($runNo
					  ,$id
				      ,$r['taxno']
				      ,$r['name']
				      ,$r['receipttype_name']
				      ,$r["branch"]
				      ,$manage);
		$runNo++;
	}
}

$aData = array();
$sQuery = "SELECT COUNT(*) as total
			  FROM receipt a
			  $WHERE $sAND $sWhere";

$rs = $db->data($sQuery);
$iFilteredTotal = $rs;
 
$sQuery = "SELECT COUNT(*) as total
			  FROM receipt a";
$resultTotal = $db->data($sQuery);
$iTotal = $resultTotal;
						 
$aData["sEcho"] = intval($_POST['sEcho']);
$aData["iTotalRecords"] = $iTotal; 
$aData["iTotalDisplayRecords"] = $iFilteredTotal; 
$aData["aaData"] = $a; 

}

echo json_encode($aData);
?>
