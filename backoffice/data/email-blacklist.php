<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/email.php";
global $db, $RIGHTTYPEID, $SECTIONID;
$single_info = $_POST["single"]; 
//d($_POST); die();
if($single_info=="T"){
	$aData = array();
	$id = $_POST["email_blacklist_id"]; 
	if($id){
	   $r = view_email("", $id);
	   foreach($r as $k=>$v){
	      $aData[] = $v;
	   }  
	}
}else{

function fnColumnToField( $i ){
	/* Note that column 0 is the details column */
	if ( $i == 0 ||$i == 1 )
		return "a.email_blacklist_id";
	else if ( $i == 2 )
		return "a.code";
	else if ( $i == 3 )
		return "a.name";
	// else if ( $i == 4 )
	// 	return "a.section_id";
	// else if ( $i == 5 )
	// 	return "a.highlight";
}

$sLimit = "";
if (isset( $_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1' )
{
	$sLimit = "LIMIT ".(int)($_POST['iDisplayStart'] );
	$sLimit .= ", ".(int)( $_POST['iDisplayLength'] );
}


/* Ordering */
if(isset($_POST['iSortCol_0'])){
	$sOrder = "ORDER BY  ";
	for ( $i=0 ; $i<$db->escape( $_POST['iSortingCols'] ) ; $i++ ){
		$sOrder .= fnColumnToField($db->escape( $_POST['iSortCol_'.$i] ))."
                ".$db->escape( $_POST['sSortDir_'.$i] ) .", ";
	}
	$sOrder = substr_replace( $sOrder, "", -2 );
}
 
/* Filtering */
  $sWhere = "";
  $WHERE = "WHERE a.active!='' ";
  $sAND = "";
if($_POST['sSearch'] != ""){
   $sWhere = "a.code LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "a.name LIKE '%".$db->escape( $_POST['sSearch'] )."%'";
	$sAND = "AND ";
}
// if($SECTIONID>0){
// 	if($RIGHTTYPEID==1 || $RIGHTTYPEID==4){

// 	}else{
// 		$sWhere .= " and a.section_id=$SECTIONID";
// 	}
// }

/* Paging */
//left join section b on b.section_id=a.section_id
$sQuery = "SELECT a.email_blacklist_id, a.code, a.name, a.active
           FROM email_blacklist a 
   
		   $WHERE $sAND $sWhere
		   $sOrder
		   $sLimit";

$rResult = $db->get($sQuery);
$a = array();
if(is_array($rResult)){
	$runNo = 1;
	foreach ($rResult as $r){
		$id = $r["email_blacklist_id"]; 
	  $manage =  get_datatable_icon("edit", $id);
	  $active = ($r["active"]=="T") ? "active" : "nonActive";   
		$a[] = array($runNo
				      ,$r['code']
				      ,$r['name']
				      //,$r['section_name']
				      //,$r["highlight"]=="T" ? 'Enable' : 'Disable'
				      ,$r["active"]=="T" ? "ใช้งาน" : "ไม่ใช้งาน" 
				      ,$manage);
		$runNo++;
	}
}

$aData = array();
$sQuery = "SELECT COUNT(*) as total
			  FROM email_blacklist a
			  $WHERE $sAND $sWhere";

$rs = $db->data($sQuery);
$iFilteredTotal = $rs;
 
$sQuery = "SELECT COUNT(*) as total
			  FROM email_blacklist a";
$resultTotal = $db->data($sQuery);
$iTotal = $resultTotal;
						 
$aData["sEcho"] = intval($_POST['sEcho']);
$aData["iTotalRecords"] = $iTotal; 
$aData["iTotalDisplayRecords"] = $iFilteredTotal; 
$aData["aaData"] = $a; 

}

echo json_encode($aData);
?>
