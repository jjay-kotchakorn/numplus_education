<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/course.php";
global $db;

$single_info = $_POST["single"];
if($single_info=="detail"){
	$aData = array();
	$id = $_POST["course_detail_id"];
	if($id){
	   $r = get_course_detail("", $id);
	   foreach($r as $k=>$v){
	   	  $v["open_regis"] = revert_date($v["open_regis"]);
	   	  $v["end_regis"] = revert_date($v["end_regis"]);
	   	  $v["date"] = revert_date($v["date"]);
	      $aData[] = $v;
	   }  
	}
}else if($single_info=="T"){
	$aData = array();
	$id = $_POST["course_detail_id"];
	if($id){
	   $r = get_course("", $id);
	   foreach($r as $k=>$v){
	      $aData[] = $v;
	   }  
	}
}else{

function fnColumnToField( $i ){
	/* Note that column 0 is the details column */
	if ( $i == 0 ||$i == 1 )
		return "a.course_detail_id";
	else if ( $i == 2 )
		return "a.day";
	else if ( $i == 3 )
		return "a.date";
	else if ( $i == 4 )
		return "a.section_id";
	else if ( $i == 5 )
		return "a.coursetype_id";
}


$sLimit = "";
if (isset( $_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1' )
{
	$sLimit = "LIMIT ".(int)($_POST['iDisplayStart'] );
	$sLimit .= ", ".(int)( $_POST['iDisplayLength'] );
}

/* Ordering */
if(isset($_POST['iSortCol_0'])){
	$sOrder = "ORDER BY  ";
	for ( $i=0 ; $i<$db->escape( $_POST['iSortingCols'] ) ; $i++ ){
		$sOrder .= fnColumnToField($db->escape( $_POST['iSortCol_'.$i] ))."
                ".$db->escape( $_POST['sSortDir_'.$i] ) .", ";
	}
	$sOrder = substr_replace( $sOrder, "", -2 );
}
 
/* Filtering */
  $sWhere = "";
  $WHERE = "WHERE a.active!='' ";
  $sAND = "";
if($_POST['sSearch'] != ""){
	$kwrd = trim($_POST['sSearch']);
	if ( preg_match("/.[ก-๙ ]+.+/", $kwrd) ) {
		$sWhere = "(a.day LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "a.code_project LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "a.address LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "a.address_detail LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "b.name LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "c.name LIKE '%".$db->escape( $_POST['sSearch'] )."%')";
	}else{
		$sWhere = "(a.day LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "a.date LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "a.time LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "a.code_project LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "a.address LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "a.address_detail LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "b.name LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "c.name LIKE '%".$db->escape( $_POST['sSearch'] )."%')";
	}
   
	$sAND = "AND ";
}
$sWhere .= ($_POST["section_id"]) ? " and a.section_id={$_POST["section_id"]}" : "";
$sWhere .= ($_POST["coursetype_id"]) ? " and a.coursetype_id={$_POST["coursetype_id"]}" : "";

$ck_active = $_POST["active"];
$close_ids = array();
if($ck_active=="I"){
	$sWhere .= ($_POST["active"]) ? " and a.active='T' and a.inhouse='T'" : "";
}else if($ck_active=="C"){
	$today = date("Y-m-d 00:00:00");
	$sWhere .= ($_POST["active"]) ? " and a.active='T'" : "";
	$sWhere .= ($today) ? " and a.end_regis<='$today'" : "";
}else{
	$sWhere .= ($_POST["active"]) ? " and a.active='{$_POST["active"]}'" : "";
}
if($SECTIONID>0){
	$sWhere .= " and a.section_id=$SECTIONID";
}
$dateStart = ($_POST["date_start"]) ? thai_to_timestamp($_POST["date_start"]) :  date("Y-01-01");
$dateStop =  ($_POST["date_stop"]) ? thai_to_timestamp($_POST["date_stop"]) : date("Y-12-31");
if ($dateStart || $dateStop) {
    if (!$dateStart && $dateStop)
        $dateStart = $dateStop;
    if (!$dateStop && $dateStart)
        $dateStop = $dateStart;
    $t = $dateStart;
    if ($dateStart > $dateStop) {
        $dateStart = $dateStop;
        $dateStop = $t;
    }
}
$sWhere .= ($dateStart && $dateStop) ? " and a.date>='$dateStart' and a.date<='$dateStop'" : "";
/* Paging */
$sQuery = "SELECT a.course_detail_id, a.day, a.date, a.time, a.address, a.address_detail, a.section_id, b.name as section_name, a.active, a.coursetype_id
				 ,c.name as coursetype_name
				 ,a.open_regis
				 ,a.end_regis
           FROM course_detail a left join section b on b.section_id=a.section_id
           		left join coursetype c on c.coursetype_id=a.coursetype_id
		   $WHERE $sAND $sWhere
		   $sOrder
		   $sLimit";


		   //write log file to /log/
			/*$date_now = date('Y-m-d H:i:s');
			$date_log = date('Y-m-d');
			$log_name = "./log/cosdtaillist_".$date_log.".log";
	        $file = fopen($log_name, 'a');
			$str_txt = $date_now."\r\n".$sQuery."\r\n";
			fwrite($file, $str_txt);
			fclose($file);   */   


$rResult = $db->get($sQuery);
$a = array();
if(is_array($rResult)){
	$runNo = 1;
	$list_id = get_config('sectionDisplayButton');
	$array_ck = explode(",", $list_id);
	$displayButton = '<a class="btn btn-info" onClick="promotionInfo(\''.$id.'\')"><i class="fa  fa-bars"></i> </a>';
	foreach ($rResult as $r){
	  $id = $r["course_detail_id"];
	  $manage =  get_datatable_icon("edit", $id);
	  if($_POST["report"]=="ic_sob"){
	  	 $manage =  get_datatable_icon("report", $id);
	  }
	  $active = ($r["active"]=="T") ? "active" : "nonActive";   
	  $ck = $r["section_id"];
	  if(in_array($ck, $array_ck)) $manage = $manage; 
		$a[] = array($runNo
				      ,$r['day'].". ".revert_date($r["date"])
				      ,$r['time']
				      ,$r['address']
				      ,$r["address_detail"]
	   	        	  ,revert_date($r["open_regis"])
	   	  			  ,revert_date($r["end_regis"])
	   	  			  ,$active
				      ,$manage);
		$runNo++;
	}
}

$aData = array();
$sQuery = "SELECT COUNT(*) as total
			  FROM course_detail a
			  $WHERE $sAND $sWhere";

$rs = $db->data($sQuery);
$iFilteredTotal = $rs;
 
$sQuery = "SELECT COUNT(*) as total
			  FROM course_detail a";
$resultTotal = $db->data($sQuery);
$iTotal = $resultTotal;
						 
$aData["sEcho"] = intval($_POST['sEcho']);
$aData["iTotalRecords"] = $iTotal; 
$aData["iTotalDisplayRecords"] = $iFilteredTotal; 
$aData["aaData"] = $a; 

}

echo json_encode($aData);
?>
