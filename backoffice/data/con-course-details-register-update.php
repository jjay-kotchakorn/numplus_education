<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";

$date_now = date('Y-m-d H:i:s');
$start = date("Y-m-01");
//$stop = date('Y-m-t',strtotime('today'));
$stop =  date("Y-m-t", strtotime( date( "Y-m-d",strtotime( date("Y-m-d") )) . "+5 month" )) ;
$q = "select course_detail_id from course_detail where active='T' and date>='$start' and date<='$stop'";
$rs = $db->get($q);

// echo $q.";<br>";
// d($rs);die();

$c = count($rs);
$i = 0;

$cos_dt_ids = "";
$str_txt = "";
$str_q = "";

foreach ($rs as $key => $v) {
	$course_detail_id = $v["course_detail_id"];

	if ( $course_detail_id ) {
		$cos_dt_ids .= $course_detail_id.",";
		// continue;
	}

	//$q = "select get_course_detail_sum($course_detail_id)";
	$q = "select get_course_detail_sum($course_detail_id) as sum, get_course_detail_regis($course_detail_id) as regis, get_course_detail_usebook($course_detail_id) as book, {$course_detail_id} as course_detail_id
	";
	
	// echo $q.";<br>";

	$a =  $db->rows($q);
	$sum = $a["sum"];
	$regis = $a["regis"];
	$pay = $sum - $regis;
	$book = $a["book"];


	$q = "update course_detail 
			set sit_all={$sum}
			, regis_all={$regis}
			, book_all={$book} 
			, datetime_update='{$date_now}'
		where course_detail_id={$course_detail_id}";
	// echo $q.";<br>";	
	$db->query($q);
	// echo "no {$i}/{$c} update course_detail_id = {$course_detail_id} : ( sum : {$sum}, book : $book, regis : $regis) <hr>";
	// $str_q .= $q.";\r\n";

/*
	$str_txt = "no {$i}/{$c} update course_detail_id = {$course_detail_id} : ( sum : {$sum}, book : $book, regis : $regis)";
	$str_txt .= "\r\n";
	$str_txt .= "------------------------------------------------------------------------------";
	$str_txt .= "\r\n";

	//write log
	// $date_now = date("Y-m-d");
	$log_name = "./log/con-course-details-register-update.log";
	$file = fopen($log_name, 'a');
	$date_now = date('Y-m-d H:i:s');
	$str_txt = $date_now."|".$str_txt;
	fwrite($file, $str_txt);
	fclose($file);
*/

	$str_txt .= "no {$i}/{$c} update course_detail_id = {$course_detail_id} : ( sum : {$sum}, book : $book, regis : $regis)";
	$str_txt .= "\r\n";
	// $str_q .= $q.";";

	$i++;
}

$cos_dt_ids = trim($cos_dt_ids, ",");
// echo $cos_dt_ids;

	// $str_txt = "no {$i}/{$c} update course_detail_id = {$course_detail_id} : ( sum : {$sum}, book : $book, regis : $regis)";
	// $str_txt .= "\r\n";
	$str_txt .= "({$cos_dt_ids})";
	$str_txt .= "\r\n";
	$str_txt .= $str_q;
	$str_txt .= "\r\n";
	$str_txt .= "------------------------------------------------------------------------------";
	$str_txt .= "\r\n";

	//write log
	$date_log = date("Y-m-d");
	$date_now = date('Y-m-d H:i:s');
	$log_name = "./log/con-course-details-register-update_{$date_log}.log";
	$file = fopen($log_name, 'a');
	$str_txt = $date_now."|".$str_txt;
	fwrite($file, $str_txt);
	fclose($file);

?>