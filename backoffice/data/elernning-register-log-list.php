<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
global $db;
$single_info = $_POST["single"];
if($single_info=="T"){
/*
	$aData = array();
	$id = $_POST["emp_id"];
	if($id){
	   $r = emp_info("", $id);
	   foreach($r as $k=>$v){
	      $v["birthdate"] = revert_date($v["birthdate"]);
		  if(!$v["img"])
		     $v["img"] = "images/no-avatar-male.jpg";
		  $con = " and a.emp_id=$id";
		  $t = login_info($con);
		  if($t){
			foreach($t as $key=>$val){
				$v["activelogin"] = $val["active"];
				$v["righttype_id"] = $val["righttype_id"];
				$v["username"] = $val["username"];
				$v["password"] = $val["password"];
				$v["section_id"] = $val["section_id"];
				$v["coursetype_id"] = $val["coursetype_id"];
			}
		  }else{
				$v["activelogin"] = "";
				$v["righttype_id"] = "";
				$v["username"] = "";
				$v["password"] = "";  	
		  }
	      $aData[] = $v;
	   }  
	}
*/
	
}else{

$sLimit = "";
if (isset( $_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1' )
{
	$sLimit = "LIMIT ".(int)($_POST['iDisplayStart'] );
	$sLimit .= ", ".(int)( $_POST['iDisplayLength'] );
}

/* Filtering */
$WHERE = "WHERE a.active='T'";
$sWhere = "";
if($_POST['sSearch'] != ""){
   $sWhere .= " AND (a.fname LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "a.lname LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "a.cid LIKE '%".$db->escape( $_POST['sSearch'] )."%')";
	$sAND = "AND ";
}

$sWhere .= $_POST["send_status"]=="all" ? "" : " AND a.send_status='{$_POST["send_status"]}'";
$sWhere .= $_POST["receive_status"]=="all" ? "" : " AND a.receive_status='{$_POST["receive_status"]}'";

$sWhere .= empty($_POST["section_id"]) ? "" : " AND b.section_id={$_POST["section_id"]}";
$sWhere .= empty($_POST["coursetype_id"]) ? "" : " AND b.coursetype_id={$_POST["coursetype_id"]}";

$sWhere .= empty($_POST["member_id"]) ? "" : " AND a.member_id={$_POST["member_id"]}";
$sWhere .= empty($_POST["course_detail_id"]) ? "" : " AND a.course_detail_id='{$_POST["course_detail_id"]}'";

$dateStart = ($_POST["date_start"]) ? thai_to_timestamp($_POST["date_start"]) :  "";
$dateStop =  ($_POST["date_stop"]) ? thai_to_timestamp($_POST["date_stop"]) : "";
if ($dateStart || $dateStop) {
    if (!$dateStart && $dateStop)
        $dateStart = $dateStop;
    if (!$dateStop && $dateStart)
        $dateStop = $dateStart;
    $t = $dateStart;
    if ($dateStart > $dateStop) {
        $dateStart = $dateStop;
        $dateStop = $t;
    }
}

$dateStartResult = ($_POST["date_start_result"]) ? thai_to_timestamp($_POST["date_start_result"]) :  "";
$dateStopResult =  ($_POST["date_stop_result"]) ? thai_to_timestamp($_POST["date_stop_result"]) : "";
if ($dateStartResult || $dateStopResult) {
    if (!$dateStartResult && $dateStopResult)
        $dateStartResult = $dateStopResult;
    if (!$dateStopResult && $dateStartResult)
        $dateStopResult = $dateStartResult;
    $t = $dateStartResult;
    if ($dateStartResult > $dateStopResult) {
        $dateStartResult = $dateStopResult;
        $dateStopResult = $t;
    }
}
// d($_POST);die();
$sWhere .= ($dateStart && $dateStop) ? " AND (a.send_date>='$dateStart 00:00:00' AND a.send_date<='$dateStop 23:59:59')" : "";
$sWhere .= ($dateStartResult && $dateStopResult) ? " AND (a.receive_date>='$dateStartResult 00:00:00' AND a.receive_date<='$dateStopResult 23:59:59')" : "";
// $sOrder = "ORDER BY a.result_date DESC";
// $sOrder = "ORDER BY a.send_date DESC";
if ( !empty($_POST["order_by_date"]) ) {
	$order_by_date = $_POST["order_by_date"];
	switch ($order_by_date) {
		case 'all':
			$sOrder = "ORDER BY a.rectime DESC";
			break;
		case 'send_date':
			$sOrder = "ORDER BY a.send_date DESC";
			break;
		case 'receive_date':
			$sOrder = "ORDER BY a.receive_date DESC";
			break;

	}//end sw
}//end if
/* Paging */
$sQuery = "SELECT a.register_data_list_id,
				a.elerning_map_course_detail_id,
				a.ref_elerning_site_vender_id,
				a.register_id,
				a.register_course_detail_id,
				a.course_id,
				a.course_detail_id,
				a.member_id,
				a.cid,
				a.title,
				a.fname,
				a.lname,
				a.code_course_wr,
				a.code_project,
				a.code_course_elearning,
				a.send_date,
				a.receive_date,
				a.expire_date,
				a.send_status,
				a.receive_status,
				a.result_id,
				a.result_date,
				a.te_result_id,
				a.te_result_date,
				a.coursetype_el,
				a.coursetype_wr,
				a.rectime,
				a.active,
				a.ref_uuid,
				a.email,
				a.birthdate,
				a.remark,
				a.map_course_detail_info,
				b.section_id,
				b.coursetype_id 
			FROM register_data_list AS a
			LEFT JOIN course AS b ON a.course_id = b.course_id
			$WHERE $sWhere
			$sOrder
			$sLimit
		   ";
  //echo $sQuery;

$rResult = $db->get($sQuery);
$a = array();
if(is_array($rResult)){
	$runNo = 1;
	foreach ($rResult as $r){
		$id = $r["register_data_list_id"]; 
		$ref_uuid = $r["ref_uuid"]; 
	  // $manage =  get_datatable_icon("edit", $id);
		$ckBox = '<input type="checkbox" name="ckbox['.$id.']" id="ckbox-'.$id.'" value="'.$id.'" style="float:right;"><input type="hidden" name="register_id[]" id="'.$id.'" value="'.$id.'">';
		$manage = "";
		$manage .= '<a class="btn btn-primary" onClick="get_result(\''.$id.','.$ref_uuid.'\')"> <i class="fa fa-reply"></i></a>';
	  	$active = ($r["active"]=="T") ? "active" : "nonActive";   

	  	$register_course_detail_id = $r["register_course_detail_id"];
	  	$result_name = "";
	  	$te_result_name = "";

	  	$q = "SELECT
				a.register_course_detail_id,
				a.register_id,
				a.course_id,
				a.course_detail_id,
				a.active,
				a.recby_id,
				a.rectime,
				a.remark,
				a.register_result_id,
				a.result_date,
				a.result_by,
				a.result_from,
				a.register_te_result_id,
				a.te_result_date,
				a.te_result_by,
				a.te_result_from,
				b.name AS result_name,
				c.name AS te_result_name
			FROM register_course_detail AS a
			LEFT JOIN register_result b ON b.register_result_id=a.register_result_id
			LEFT JOIN register_result c ON c.register_result_id=a.register_te_result_id
			WHERE a.register_course_detail_id={$register_course_detail_id}
	  	";
	  	$info = $db->rows($q);
	  	//$register_result_id = $info["register_result_id"];
	  	//$register_te_result_id = $info["register_te_result_id"];
	  	$result_date = $info["result_date"];
	  	$te_result_date = $info["te_result_date"];
	  	$result_name = $info["result_name"];
	  	$te_result_name = $info["te_result_name"];

/*
	  	if ( empty($r["coursetype_el"]) || $r["coursetype_el"]==1 ) {
	  		$result_id = !empty($r["result_id"]) ? $r["result_id"] : 4;
	  		$result_date = $r["result_date"];
	  		$q = "SELECT name FROM register_result WHERE register_result_id={$result_id}";
	  		$result_name = $db->data($q);
	  	}else if( $r["coursetype_el"]==0 ){
	  		$te_result_id = !empty($r["te_result_id"]) ? $r["te_result_id"] : 4;
	  		$te_result_date = $r["te_result_date"];
	  		$q = "SELECT name FROM register_result WHERE register_result_id={$te_result_id}";
	  		$te_result_name = $db->data($q);  		
	  	}//end elseif
*/
	  	// $result = "";
	  	// $r["result_id"] = !empty($r["result_id"]) ? $r["result_id"] : 4;
	  	// $q = "";
/*	  	
	  	switch ( $r["result_id"] ) {
	  		case '0':
	  				$result = "รอผลการเรียน";
	  			break;
	  		case '1':
	  				$result = "ผ่าน";
	  			break;
	  		case '2':
	  				$result = "ไม่ผ่าน";
	  			break;
	  		
	  		default:
	  			
	  			break;
	  	}//end sw
*/
	  	// $send_status = $r["send_status"]=='T' ? "success" : "fail";
	  	if ( $r["send_status"]=='T' ) {
	  		$send_status = "สำเร็จ";	
	  		$ckBox = "";  		
	  	}else{
	  		$send_status = "ไม่สำเร็จ";
	  		$ckBox = $ckBox;
	  	}//end else
	  	$receive_status = $r["receive_status"]=='T' ? "สำเร็จ" : "รอรับข้อมูล";
	  	$code_project = !empty($r["code_project"]) ? $r["code_project"] : "-";
	  	$expire_date = explode(" ", $r["expire_date"]);
	  	$expire_date = $expire_date[0];

		$a[] = array($runNo." / ".$id." ".$ckBox
				      ,$r['title']
				      ,$r['fname']
				      ,$r['lname']
				      ,$r["cid"]
				      ,$r["member_id"]
				      ,$r["register_id"]
				      ,$r["course_id"]
				      ,$code_project
				      ,$r["course_detail_id"]
				      ,$r["code_course_wr"]
				      ,$expire_date
				      ,$r["send_date"]
				      ,$r["receive_date"]
				      ,$result_name
				      ,$te_result_name
				      ,$result_date
				      ,$te_result_date
				      ,$send_status
				      ,$receive_status
				      ,$r["remark"]
				      //,$manage
				      );
		$runNo++;
	}
}

$aData = array();
$sQuery = "SELECT COUNT(*) as total
			  FROM register_data_list AS a
			  $WHERE $sWhere";
// echo $sQuery;die();
$rs = $db->data($sQuery);
$iFilteredTotal = $rs;
 
$sQuery = "SELECT COUNT(*) as total
			  FROM register_data_list";
$resultTotal = $db->data($sQuery);
$iTotal = $resultTotal;
						 
$aData["sEcho"] = intval($_POST['sEcho']);
$aData["iTotalRecords"] = $iTotal; 
$aData["iTotalDisplayRecords"] = $iFilteredTotal; 
$aData["aaData"] = $a; 

}

echo json_encode($aData);
?>
