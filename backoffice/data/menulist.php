<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/menu.php";
global $db;
$single_info = $_POST["single"];
if($single_info=="T"){
	$aData = array();
	$id = $_POST["menulist_id"];
	if($id){
		$q = "select a.menulist_id, a.code, a.name, a.name_eng, a.active, a.useclass, a.action, a.url
				,a.recby_id, a.rectime, a.active, a.menu_id, b.name as menu_name
			from menulist  a inner join menu b on a.menu_id=b.menu_id
			where a.active!='' and a.menulist_id=$id";
		$aData = $db->get($q);
	}
}else{
  $aColumns = array( 'menulist_id','code','name','name_eng','active');
  /* Indexed column (used for fast and accurate table cardinality) */
  $sIndexColumn = "menulist_id";

  function fnColumnToField( $i ){
	/* Note that column 0 is the details column */
	if ( $i == 0)
	  return "a.menulist_id";
	else if ( $i == 1 )
	  return "a.code";
	else if ( $i == 2 )
	  return "a.name";
	else if ( $i == 3 )
	  return "a.name_eng";
	else if ( $i == 4 )
	  return "a.menu_id";
	else if ( $i == 5 )
	  return "a.active";
	else return "a.menulist_id";
  }

  $sLimit = "";
  if (isset( $_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1' )
  {
	$sLimit = "LIMIT ".(int)($_POST['iDisplayStart'] );
	$sLimit .= ", ".(int)( $_POST['iDisplayLength'] );
  }

  /* Ordering */
  if(isset($_POST['iSortCol_0'])){
	$sOrder = "ORDER BY  ";
	for ( $i=0 ; $i<$db->escape( $_POST['iSortingCols'] ) ; $i++ ){
	  $sOrder .= fnColumnToField($db->escape( $_POST['iSortCol_'.$i] ))."
				  ".$db->escape( $_POST['sSortDir_'.$i] ) .", ";
	}
	$sOrder = substr_replace( $sOrder, "", -2 );
  }
   
  /* Filtering */
  $sWhere = "";
  $WHERE = "WHERE a.active!='' ";
  if($_POST['sSearch'] != ""){
	 $sWhere = "a.code LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			"a.name LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			"a.name_eng LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			"a.active LIKE '%".$db->escape( $_POST['sSearch'] )."%'";
	$sAND = "AND ";
  }
  if($_POST["menu_id"]!="All"){
  	$sWhere .= " AND a.menu_id=".$_POST["menu_id"]." ";
  }
  /* Paging */
  $sQuery = "SELECT a.menulist_id, a.code, a.name, a.name_eng, a.active, b.name as menu_name, a.menu_id
		 FROM menulist a inner join menu b on a.menu_id=b.menu_id 
		 $WHERE $sWhere
		 $sOrder
		 $sLimit";

  $rResult = $db->get($sQuery);
  $a = array();
  if(is_array($rResult)){
	$runNo = 1;
	foreach ($rResult as $r){
	  $id = $r["menulist_id"]; 
	  $manage =  get_datatable_icon("edit", $id);
	  $active = ($r["active"]=="T") ? "active" : "nonActive";   
	  $a[] = array($runNo
				,$r['code']
				,$r['name']
				,$r['name_eng']
				,$r['menu_name']
				,$active
				,$manage);
	  $runNo++;
	}
  }

  $aData = array();
  $sQuery = "SELECT COUNT(*) as total
		  FROM menulist a
		  $WHERE $sWhere";
  $rs = $db->data($sQuery);
  $iFilteredTotal = $rs;
   
  $sQuery = "SELECT COUNT(*) as total
		  FROM menulist a";
  $resultTotal = $db->data($sQuery);
  $iTotal = $resultTotal;
			   
  $aData["sEcho"] = intval($_POST['sEcho']);
  $aData["iTotalRecords"] = $iTotal; 
  $aData["iTotalDisplayRecords"] = $iFilteredTotal; 
  $aData["aaData"] = $a; 
}

echo json_encode($aData);
?>
