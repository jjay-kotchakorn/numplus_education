<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/datatype.php";
include_once "../share/course.php";
global $db;

/*
[section_id] => 1
[coursetype_id] => 2
[course_detail_id] => 8450
[course_id] => 40
[status] => 3,5,6
[active] => T
[type] => register-result
*/

// d($_POST); die();

$apay = datatype(" and a.active='T'", "pay_status", true);
$arr_pay = array();
foreach ($apay as $key => $value) {
	$arr_pay[$value["pay_status_id"]] = $value["name"];
}
set_time_limit(600);
$single_info = $_POST["single"];
if($single_info=="T"){
	$aData = array();
	$id = $_POST["member_id"];
	if($id){
   $r = view_member("", $id);
    foreach ($r as $k => $v) {
    	$v["select_university"] = $v["grd_ugrp1"]."-".$v["grd_uid1"];
    	$v["birthdate"] = revert_date($v["birthdate"]);
    	$v["password2"] = $v["password"];     
	    $aData[] = $v;	   }  
	}
}else{

function fnColumnToField( $i ){
	/* Note that column 0 is the details column */
	if($_POST["type"]=="register-info"){

		if ( $i == 0 ||$i == 1 )
			return "a.register_id";
		else if ( $i == 2 )
			return "a.fname";
		else if ( $i == 4 )
			return "a.date";
		else if ( $i == 5 )
			return "a.expire_date";
		else if ( $i == 5 )
			return "a.pay_date";
		else
			return "a.register_id";
	}else{		
		if ( $i == 0 ||$i == 1 )
			return "a.register_id";
		else if ( $i == 2 )
			return "a.cid";
		else if ( $i == 3 )
			return "a.fname";
		else
			return "a.register_id";
	}
}

$page_type = $_POST["type"];


$sLimit = "";
if (isset( $_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1' )
{
	$sLimit = "LIMIT ".(int)($_POST['iDisplayStart'] );
	$sLimit .= ", ".(int)( $_POST['iDisplayLength'] );
}


/* Ordering */
if(isset($_POST['iSortCol_0'])){
	$sOrder = "ORDER BY  ";
	for ( $i=0 ; $i<$db->escape( $_POST['iSortingCols'] ) ; $i++ ){
		$sOrder .= fnColumnToField($db->escape( $_POST['iSortCol_'.$i] ))."
                ".$db->escape( $_POST['sSortDir_'.$i] ) .", ";
	}
	$sOrder = substr_replace( $sOrder, "", -2 );
}
 
/* Filtering */
$sWhere = "";
$WHERE = "WHERE a.active!='' and a.pay_status!=8 AND a.auto_del='F' ";
if($_POST['sSearch'] != ""){
   $sWhere = " and  (a.title LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "a.fname LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "a.lname LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "a.ref1 LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".			    
			    "a.ref2 LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".					    
			    "a.cid LIKE '%".$db->escape( $_POST['sSearch'] )."%' )";
	$sAND = "AND ";
	
}


$sWhere .= ($_POST["section_id"]) ? " and a.section_id={$_POST["section_id"]}" : "";
$sWhere .= ($_POST["member_id"]) ? " and a.member_id={$_POST["member_id"]}" : "";
$sWhere .= ($_POST["coursetype_id"]) ? " and a.coursetype_id={$_POST["coursetype_id"]}" : "";
$sWhere .= ($_POST["status"]) ? " and a.pay_status in ({$_POST["status"]})" : "";
$sWhere .= ($_POST["active"]) ? " and a.active='{$_POST["active"]}'" : " and a.active='T'";
$sWhere .= ($_POST["pub"]=="T") ? " and a.pay not in ('importfile', 'walkin')" : "";
$sWhere .= ($_POST["pub"]=="F") ? " and a.pay in ('importfile', 'walkin')" : "";
$register_ids = array();
$flag = 0;
if($_POST["course_detail_id"]){
	$cond = "";
/*	
	if($page_type=="register-result" && ($_POST["course_id"])>0){
		$course_id = $_POST["course_id"];
		$cond = " AND a.course_id={$course_id}";
	}//end if
*/


	$q = "select a.register_id from register a where a.active='T' and a.course_detail_id={$_POST["course_detail_id"]} $cond";
	$cd = $db->get($q);
	
	// echo $q; die();

	if($cd){
		foreach ($cd as $key => $value) {
			$register_ids[$value["register_id"]] = $value["register_id"];
		}
	}

	$q = "select a.course_detail_id, a.register_id from register_course_detail a where a.active='T' and a.course_detail_id={$_POST["course_detail_id"]} $cond";
	$cd = $db->get($q);	

	// echo $q; die();

	// d($_POST); die();
	
	if($cd){
		foreach ($cd as $key => $value) {
			$register_ids[$value["register_id"]] = $value["register_id"];
		}
	}
	// d($register_ids); die();
	if(count($register_ids)>0){
		$register_ids = implode(",", $register_ids);
		$register_ids = trim($register_ids, ",");		
		$sWhere .= " and a.register_id in ($register_ids)";
	}else{
		$flag = 1;
		$sWhere .= " and a.register_id in (0)";
	}//end else
	// echo $sWhere; die();
}//end if

$dateStart = ($_POST["date_start"]) ? thai_to_timestamp($_POST["date_start"]) :  "";
$dateStop =  ($_POST["date_stop"]) ? thai_to_timestamp($_POST["date_stop"]) : "";
if ($dateStart || $dateStop) {
    if (!$dateStart && $dateStop)
        $dateStart = $dateStop;
    if (!$dateStop && $dateStart)
        $dateStop = $dateStart;
    $t = $dateStart;
    if ($dateStart > $dateStop) {
        $dateStart = $dateStop;
        $dateStop = $t;
    }
}
$sWhere .= ($dateStart && $dateStop) ? " and a.date>='$dateStart' and a.date<='$dateStop'" : "";

$orderBy = "";
if ( !empty($_POST["sort_by"]) && $_POST["sort_by"]!="cos_id" ) {
	switch ($_POST["sort_by"]) {
/*
		case 'cos_id':
			$orderBy = "order by a.course_id, convert(a.fname using tis620) asc";
			break;
*/			
		case 'name':
			$orderBy = "order by convert(a.fname using tis620) asc";
			break;
		case 'pay_date':
			$orderBy = "order by a.pay_date desc";
			break;
		case 'status':
			$orderBy = "order by a.pay_status desc";
			break;
		case 'no':
			$orderBy = "order by a.no asc";
			break;		
		case 'reg_date':
			$orderBy = "order by a.date desc";
			break;
	}//end sw
}elseif ( empty($_POST["sort_by"]) ) {
	$orderBy = "order by a.course_id, convert(a.fname using tis620) asc";
}//end if

//filter by register-result
if ( $page_type=="register-result" || $_POST["sort_by"]=="cos_id" ) {
	//group course_id
	$sQuery = "SELECT DISTINCT(a.course_id)
		FROM register a LEFT JOIN district b ON b.district_id=a.receipt_amphur_id
	        LEFT JOIN amphur c ON c.amphur_id=a.receipt_amphur_id
	        LEFT JOIN province d ON d.province_id=a.receipt_province_id        
	        LEFT JOIN receipt e ON e.receipt_id=a.receipt_id
	        LEFT JOIN register_result f ON f.register_result_id=a.result
	        LEFT JOIN register_result g ON g.register_result_id=a.te_results
			$WHERE $sWhere
			$orderBy
			$sLimit";
			//$sOrder
			//order by a.course_id, a.fname asc
			//ORDER BY CONVERT (rName USING tis620)  ASC;
	// echo $sQuery; die();		
	$rResult = $db->get($sQuery);

	$arr_group_course_id = array();
	foreach ($rResult as $key => $v) {
		$course_id = $v["course_id"];
		$arr_group_course_id[$course_id] = NULL;
		// $arr_group_course_id[$course_id] = $course_id;
	}//end loop $v

	ksort($arr_group_course_id);
	// d($arr_group_course_id);
	// die();
}//end if

/* Paging */
$sQuery = "SELECT a.register_id,
			a.`no`,
			a.runno,
			a.docno,
			a.section_id,
			a.coursetype_id,
			a.member_id,
			a.title,
			a.fname,
			a.lname,
			a.cid,
			a.slip_type,
			a.receipttype_id,
			a.receipttype_text,
			a.receipt_id,
			a.taxno,
			a.slip_name,
			a.slip_address1,
			a.slip_address2,
			a.date,
			a.register_by,
			a.ref1,
			a.ref2,
			a.`status`,
			a.expire_date,
			a.exam_date,
			a.course_id,
			a.course_detail_id,
			a.course_price,
			a.course_discount,
			a.pay,
			a.pay_status,
			a.pay_date,
			a.pay_method,
			a.pay_yr,
			a.pay_mn,
			a.pay_id,
			a.pay_price,
			a.pay_diff,
			a.approve,
			a.approve_by,
			a.approve_date,
			a.register_mod,
			a.last_mod,
			a.last_mod_date,
			a.result,
			a.te_results,
			a.result_date,
			a.result_by,
			a.receipt_title,
			a.receipt_fname,
			a.receipt_lname,
			a.receipt_no,
			a.receipt_gno,
			a.receipt_moo,
			a.receipt_soi,
			a.receipt_road,
			a.receipt_district_id,
			a.receipt_amphur_id,
			a.receipt_province_id,
			a.receipt_postcode,
			a.active,
			a.recby_id,
			a.rectime,
			a.remark,
            b.name AS receipt_district_name,
            c.name AS receipt_amphur_name,
            d.name AS receipt_province_name,
            e.name AS receipt_name,
            f.name AS result_name,
            g.name AS te_results_name
	FROM register a LEFT JOIN district b ON b.district_id=a.receipt_amphur_id
        LEFT JOIN amphur c ON c.amphur_id=a.receipt_amphur_id
        LEFT JOIN province d ON d.province_id=a.receipt_province_id        
        LEFT JOIN receipt e ON e.receipt_id=a.receipt_id
        LEFT JOIN register_result f ON f.register_result_id=a.result
        LEFT JOIN register_result g ON g.register_result_id=a.te_results
		$WHERE $sWhere
		$orderBy
		$sLimit";
		//$sOrder
		//order by a.course_id, a.fname asc
		//ORDER BY CONVERT (rName USING tis620)  ASC;
// echo $sQuery; die();		
$rResult = $db->get($sQuery);

//filter by register-result
if ( $page_type=="register-result" || $_POST["sort_by"]=="cos_id" ) {
	// d($arr_group_course_id);
	foreach ($rResult as $key => $v) {
		$cos_id = $v["course_id"];
		$arr_group_course_id[$cos_id][] = $v;
	}//end loop $v

	// d($arr_group_course_id);

	$tmp = array();
	foreach ($arr_group_course_id as $key => $v) {
		$tmp = array_merge($tmp, $v);
		// $tmp = $tmp+$v;
	}//end loop $v
	$rResult = $tmp;
	// d($rResult);
}//end if

$a = array();
if(is_array($rResult)){
	$runNo = 1;
	foreach ($rResult as $v){
			$register_id = $v["register_id"];
			if($page_type=="register-result" && ($_POST["course_id"])>0){
				$sCourse_id = $_POST["course_id"];
			}else{
				$sCourse_id = $v["course_id"];
			}
			$con = " and a.course_id in ($sCourse_id)";
			$r = get_course($con);
			$title = "";
			$parent_id = 0;
			$price = 0;
			if($r){
				foreach ($r as $key => $row) {
					$course_id = $row['course_id'];
					$section_id = $row['section_id'];
					$code = $row['code'];
					$code_project = $row["code_project"];
					$title .= $row['title'].", ";
					$set_time = $row['set_time'];
					$life_time = $row['life_time'];
					$status = $row['status']; 
					$parent_id = (int)$row['parent_id'];
				}
			}
			
			$q = "select a.course_detail_id from register_course_detail a where a.register_id={$register_id} and a.course_id in ($sCourse_id)";
			$cd = $db->get($q);
			$parent_title = "";
			if($parent_id>0){
				$q = "select title from course where active='T' and course_id=$parent_id";
				$t = $db->data($q);
				$parent_title = "<strong>{$t}</strong><br>";	
			}

			$title = trim($title, ", ");
			$price = $v["course_price"];
			$discount = $v["course_discount"];

			if($cd){    
				$display_date = "";
				$display_time = "";
				$display_address = "";
				foreach ($cd as $kk => $vv) {
					$row = get_course_detail(" and a.course_detail_id={$vv["course_detail_id"]}");
					if($row) $row = $row[0];
					$course_detail_id = $row['course_detail_id'];
					$course_id = $row['course_id'];
					$day = $row['day'];
					$date = $row['date'];
					$time = $row['time'];
					$display_date .= $day." ".revert_date($date).", ";
					$display_time .= $time.", ";
					$display_address .= $row['address_detail']." ".$row['address'].", ";
				}

			}else{
				if($page_type=="register-result" && ($_POST["course_detail_id"])>0){
					$v["course_detail_id"] = $_POST["course_detail_id"];
				}				
				$display_date = "";
				$display_time = "";
				$display_address = "";
				$row = get_course_detail(" and a.course_detail_id={$v["course_detail_id"]}");
				if($row) $row = $row[0];
				$course_detail_id = $row['course_detail_id'];
				$course_id = $row['course_id'];
				$day = $row['day'];
				$date = $row['date'];
				$time = $row['time'];
				$display_date .= $day." ".revert_date($date).", ";
				$display_time .= $time.", ";
				$display_address .= $row['address']." ".$row['address_detail'].", ";
			}

			$display_date = trim($display_date, ", ");
			$display_time = trim($display_time, ", ");
			$display_address = trim($display_address, ", ");
			$id = $v["register_id"];
			$title = str_replace(", ", "<br>",trim($title, ", "));
			$display_date = str_replace(", ", "<br>",trim($display_date, ", "));
			$display_time = str_replace(", ", "<br>",trim($display_time, ", "));
			$display_address = str_replace(", ", "<br>",trim($display_address, ", "));
			$content = "";
			$pay_type = "";
			$print_style = "";

			$pay = $v["pay_id"];
			$status = $v["pay_status"];
			$ckBox = ($_POST["type"]=="register-approve") ? '<input type="text" name="no['.$id.']" id="no-'.$id.'" value="'.$v["no"].'" style="float:right; width:50px;"><input type="hidden" name="register_id[]" id="'.$id.'" value="'.$id.'">' : '';
			$ckBox = ($_POST["type"]=="register-receipt") ? '<input type="checkbox" name="ckbox['.$id.']" id="ckbox-'.$id.'" value="T" style="float:right;"><input type="hidden" name="register_id[]" id="'.$id.'" value="'.$id.'">' : $ckBox;
			$ckBox = ($_POST["type"]=="register-result") ? '<input type="checkbox" name="ckbox['.$id.']" id="ckbox-'.$id.'" value="'.$id.'" style="float:right;"><input type="hidden" name="register_id[]" id="'.$id.'" value="'.$id.'">' : $ckBox;
		$status_pay = $arr_pay[$v["pay_status"]];
	  	$print = '<a class="btn btn-info" onClick="printInfo(\''.$id.'\')"> <i class="fa fa-print"></i></a>';
	  	$acs = array(3);
	  	if(!in_array($status, $acs)){
	  		$print = "";
	  	}
		$pay_type_regis = "";
		if($v["pay"]=="walkin" || $v["pay"]=="importfile"){
			$pay_type_regis = "BACK";
		}else { 
			$pay_type_regis = "WEB";
		}
		if($v["pay"]=="at_ati"){
			$pay_type = "ชำระเงินสดผ่าน ATI";
		}else if($v["pay"]=="paysbuy"){
			$pay_type = "ชำระเงินช่องทางอื่นๆ (paysbuy)";
			$print_style = "visibility:hidden";
		}else if($v["pay"]=="mpay"){
			$pay_type = "ชำระเงินช่องทางอื่นๆ (mPAY)";
			$print_style = "visibility:hidden";
		}else if($v["pay"]=="bill_payment"){
			$pay_type = "ชำระเงินผ่าน Bill-payment";
		}else if($v["pay"]=="at_asco"){
			$pay_type = "เช็ค/เงินโอน";
		}else if($v["pay"]=="walkin"){
			if($v["pay_status"]==5 || $v["pay_status"]==6)
				$pay_type = $arr_pay[$v["pay_status"]];
			else	
				$pay_type = "ชำระเงินสดผ่าน ATI";
		}else if($v["pay"]=="importfile"){
			if($v["pay_status"]==5 || $v["pay_status"]==6)
				$pay_type = $arr_pay[$v["pay_status"]];
			else	
				$pay_type = "ชำระเงินสดผ่าน ATI";
		}
		if($v["pay_status"]==6){
			$print = "";
		}		
		$receipt_full = $v["receipt_title"].$v["receipt_fname"]." ".$v["receipt_lname"];
		$address = $v["receipt_no"];
		if($v["receipt_gno"]!="") 
			$address .= " ".$v["receipt_gno"];
		if($v["receipt_moo"]!="") 
			$address .= " ".$v["receipt_moo"];
		if($v["receipt_soi"]!="") 
			$address .= " ".$v["receipt_soi"];
		if($v["receipt_road"]!="") 
			$address .= " ".$v["receipt_road"];
        $address .= $v["receipt_district_name"];
        $address .= $v["receipt_amphur_name"];
        $address .= $v["receipt_province_name"];

        if($v["slip_type"]=="corparation"){
        	$receipt_full = $v["receipt_name"];
        }

        $str_address = $receipt_full."<br>";
        $str_address .= $address;
		if($_POST["type"]=="register-receipt"){

			$a[] = array($runNo." ".$ckBox
						  ,$v["docno"]
						  ,$code_project
					      ,$v['title']." ".$v['fname']." ".$v['lname']
					      //,$v['fname']." ".$v['lname']
					      ,$str_address
						  ,revert_date($v["pay_date"], true)
						  ,$pay_type
						  ,get_datatable_icon("edit", $id)
					      ,$print);
		}else if($page_type=="register-result"){
			$register_id = $v["register_id"];
			$course_detail_id = $_POST["course_detail_id"];
			$te_results_name = "";

			if($_POST["course_id"]> 0 && $_POST["course_detail_id"] > 0 && $v["register_id"]>0){

				
				$q = "SELECT a.register_course_detail_id
						,a.register_id
						,a.course_id
						,a.course_detail_id
						,a.active
						,a.recby_id
						,a.rectime
						,a.remark
						,a.register_result_id
						,a.result_date
						,a.result_by
						,a.result_from
						,a.register_te_result_id
						,a.te_result_date
						,a.te_result_by
						,a.te_result_from
						,b.name AS result_name 
						,c.name AS te_result_name 
						,a.result_date
						,a.te_result_date						
						,d.title
					FROM register_course_detail a 
					LEFT JOIN register_result b ON a.register_result_id=b.register_result_id
					LEFT JOIN register_result c ON a.register_te_result_id=c.register_result_id
					LEFT JOIN course d ON d.course_id=a.course_id
					WHERE a.register_id={$register_id} 
						-- AND a.course_id={$_POST["course_id"]} 
						AND a.course_detail_id={$course_detail_id}
				";

				// d($q); 

				$t = $db->rows($q); 
				// echo "$q \r\n UNION ALL \r\n";
				if(!empty($t)){
					$course_id = $_POST["course_id"];
					$course_info = get_course("", $course_id);
					$course_info = $course_info[0];
					$coursetype_id = $course_info["coursetype_id"];

					if ( $coursetype_id==3 ) {
						$v["result_name"] = $t["result_name"];
						$v["result_date"] = $t["result_date"];
						$v["te_result_name"] = $t["te_result_name"];
					}elseif ( $coursetype_id==1 ) {
						$v["result_name"] = $t["te_result_name"];	
						$v["result_date"] = $t["te_result_date"];						
					}elseif ( $coursetype_id==2 ) {
						$v["result_name"] = $t["result_name"];		
						$v["result_date"] = $t["result_date"];			
					}//end else if
										
					if ( !empty($title) ) {
						$title = $t["title"];						
					}else{

					}//end else					
				}else{
					$q = "SELECT course_id FROM register WHERE register_id={$register_id}";
					$course_id = $db->data($q); 

					$q = "SELECT title FROM course WHERE course_id={$course_id}";
					$title = $db->data($q);
				}//end else
			}//end if
			if($v["coursetype_id"]==3 && $v["section_id"]==2){  
				$a[] = array($runNo." ".$ckBox
							  ,$v["no"]
						      ,$v['title']." ".$v['fname']." ".$v['lname']
							  ,$title
							  ,$display_address
						      ,$v["te_result_name"]
						      ,$v["result_name"]
							  ,revert_date($v["result_date"], true));
			}else{
				$a[] = array($runNo." ".$ckBox
							  ,$v["no"]
						      ,$v['title']." ".$v['fname']." ".$v['lname']
							  ,$title
							  ,$display_address
						      ,$v["result_name"]
							  ,revert_date($v["result_date"], true));				
			}


		}else{
			$a[] = array($runNo." ".$ckBox
					      ,$v['title']." ".$v['fname']." ".$v['lname']
					      //,$v['fname']." ".$v['lname']
					      ,$title
						  ,revert_date($v["date"], true)
						  ,revert_date($v["expire_date"], true)
						  ,revert_date($v["pay_date"], true)
					      ,$status_pay
						  ,$v["remark"]
						  ,'<span style="text-align:center; width:100%;display:block;">'.$pay_type_regis.'</span>' 
					      ,$print);

		}
		$runNo++;
	}
}
// d($a); die();
$aData = array();
$sQuery = "SELECT COUNT(*) as total
			  FROM register a
			  $WHERE $sWhere";

$rs = $db->data($sQuery);
$iFilteredTotal = $rs;
 
$sQuery = "SELECT COUNT(*) as total
			  FROM register a";
$resultTotal = $db->data($sQuery);
$iTotal = $resultTotal;
						 
$aData["sEcho"] = intval($_POST['sEcho']);
$aData["iTotalRecords"] = $iTotal; 
$aData["iTotalDisplayRecords"] = $iFilteredTotal; 
$aData["aaData"] = $a; 

}

echo json_encode($aData);
?>
