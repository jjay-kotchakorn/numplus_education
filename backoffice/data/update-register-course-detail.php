<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/course.php";
include_once "../share/datatype.php";
global $db, $EMPID;

if (!empty($_POST["register_id"])) {
	$register_id = trim($_POST["register_id"], ",");
	$register_ids = explode(",", $register_id);
	$pay_type = $_POST["pay_type"];

	foreach ($register_ids as $key => $register_id) {
		$q = "SELECT course_id, course_detail_id FROM register WHERE register_id={$register_id}";
		$info = $db->rows($q);
		$course_id = $info["course_id"];
		$course_detail_id = $info["course_detail_id"];
		$args = array();
		$args["table"] = "register_course_detail";
		$args["register_id"] = $register_id;
		$args["course_id"] = $course_id;
		$args["course_detail_id"] = $course_detail_id;
		$args["rectime"] = date("Y-m-d H:i:s");
		$args["recby_id"] = $EMPID;
		$args["remark"] = "importfile";
		$register_course_detail_id = $db->set($args);	
	}//end loop $register_id

}//end if	


?>