<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/course.php";
global $db, $COURSETYPEID, $SECTIONID;

$single_info = $_POST["single"];
if($single_info=="detail"){
	$aData = array();
	$id = $_POST["course_detail_id"];
	if($id){
	   $r = get_course_detail("", $id);
	   foreach($r as $k=>$v){
	   	  $v["open_regis"] = revert_date($v["open_regis"]);
	   	  $v["end_regis"] = revert_date($v["end_regis"]);
		if($v["date_start"]!='0000-00-00 00:00:00')
		  $v["date_start"] = revert_date($v["date_start"]);
		else $v["date_start"] = "";
		if($v["date_stop"]!='0000-00-00 00:00:00')
		  $v["date_stop"] = revert_date($v["date_stop"]);
		else 
			$v["date_stop"]  = "";
	   	  $v["date"] = revert_date($v["date"]);
	      $aData[] = $v;
	   }  
	}
}else if($single_info=="T"){
	$aData = array();
	$id = $_POST["course_id"];
	if($id){
	   $r = get_course("", $id);
	   foreach($r as $k=>$v){
	   	$v["price"] = !empty($v["price"]) ? $v["price"] : '0';
	   	if($v["date_start"]!='0000-00-00 00:00:00')
	   	  $v["date_start"] = revert_date($v["date_start"]);
	   	else $v["date_start"] = "";
	   	if($v["date_stop"]!='0000-00-00 00:00:00')
	   	  $v["date_stop"] = revert_date($v["date_stop"]);
	   	else 
	   		$v["date_stop"]  = "";
	      $aData[] = $v;

	   }  
	}
}else{

function fnColumnToField( $i ){
	/* Note that column 0 is the details column */
	if ( $i == 0 ||$i == 1 )
		return "a.course_order";
	else if ( $i == 2 )
		return "a.code";
	else if ( $i == 3 )
		return "a.title";
	else if ( $i == 4 )
		return "a.section_id";
	else if ( $i == 5 )
		return "a.coursetype_id";
}


$sLimit = "";
if (isset( $_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1' )
{
	$sLimit = "LIMIT ".(int)($_POST['iDisplayStart'] );
	$sLimit .= ", ".(int)( $_POST['iDisplayLength'] );
}

/* Ordering */
if(isset($_POST['iSortCol_0'])){
	$sOrder = "ORDER BY  ";
	for ( $i=0 ; $i<$db->escape( $_POST['iSortingCols'] ) ; $i++ ){
		$sOrder .= fnColumnToField($db->escape( $_POST['iSortCol_'.$i] ))."
                ".$db->escape( $_POST['sSortDir_'.$i] ) .", ";
	}
	$sOrder = substr_replace( $sOrder, "", -2 );
}

$sOrder = "ORDER BY  a.course_order asc, a.course_id desc";

/* Filtering */
  $sWhere = "";
  $WHERE = "WHERE a.active!='' ";
  $sAND = "";
if($_POST['sSearch'] != ""){
   $sWhere = "a.code LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "a.title LIKE '%".$db->escape( $_POST['sSearch'] )."%'";
	$sAND = "AND ";
}
$sWhere .= ($_POST["section_id"]) ? " and a.section_id={$_POST["section_id"]}" : "";
$sWhere .= ($_POST["coursetype_id"]) ? " and a.coursetype_id={$_POST["coursetype_id"]}" : "";
$sWhere .= ($_POST["active"]) ? " and a.active='{$_POST["active"]}'" : "";
if($SECTIONID>0){
	$sWhere .= " and a.section_id=$SECTIONID";
}
if($COURSETYPEID>0){
	$sWhere .= " and a.coursetype_id=$COURSETYPEID";
}

$sWhere .= ($_POST["type"]=="childlist") ? " and (a.parent_id is null or a.parent_id <= 0)" : "";
if($_POST["type"]!="childlist"){
	$sWhere .= ($_POST["childlist"]=="parent") ? " and (a.parent_id is null or a.parent_id <= 0)" : "";
	$sWhere .= ($_POST["childlist"]=="child") ? " and (a.parent_id is not null or a.parent_id > 0)" : "";	
}

$dateStart = ($_POST["date_start"]) ? thai_to_timestamp($_POST["date_start"]) :  "";
$dateStop =  ($_POST["date_stop"]) ? thai_to_timestamp($_POST["date_stop"]) : "";
if ($dateStart || $dateStop) {
    if (!$dateStart && $dateStop)
        $dateStart = $dateStop;
    if (!$dateStop && $dateStart)
        $dateStop = $dateStart;
    $t = $dateStart;
    if ($dateStart > $dateStop) {
        $dateStart = $dateStop;
        $dateStop = $t;
    }
}
$sWhere .= ($dateStart && $dateStop) ? " and a.createtime>='$dateStart' and a.createtime<='$dateStop'" : "";
/* Paging */
$sQuery = "SELECT a.course_id
	, a.code
	, a.title
	, a.section_id
	, b.name as section_name
	, a.active
	, a.coursetype_id
	,c.name as coursetype_name
	, a.detail
	, a.course_order
	, a.parent_id
	, a.createtime
	, a.inhouse
   FROM course a 
   left join section b on b.section_id=a.section_id
   left join coursetype c on c.coursetype_id=a.coursetype_id
   $WHERE $sAND $sWhere
   $sOrder
   $sLimit";

$rResult = $db->get($sQuery);
$a = array();
if(is_array($rResult)){
	$runNo = 1;
	$list_id = get_config('sectionDisplayButton');
	$array_ck = explode(",", $list_id);
	
	foreach ($rResult as $r){
	  $id = $r["course_id"];
	  $displayButton = '<a class="btn btn-info" onClick="promotionInfo(\''.$id.'\')"><i class="fa  fa-bars"></i> </a>';
	  $childlistBt = '<a class="btn btn-info" onClick="childlist(\''.$id.'\')"><i class="fa  fa-bars"></i> เพิ่มหลักสูตรย่อย </a>';
	  $manage =  ($_POST["type"]=="childlist") ? $childlistBt : get_datatable_icon("edit", $id);
	  if($_POST["type"]=="course_order"){
	  	$manage = '<input class="form-control" type="text" name="menu_order_'.$id.'" data="'.$r["course_order"].'" value="'.$r["course_order"].'" style="width:50px; display:inline-block;"><a class="btn btn-info" onClick="update_order(\''.$id.'\')"><i class="fa fa-save"></i>บันทึก </a>';
	  }
	  if($_POST["type"]=="select-course"){
	  	 $manage = '<a class="btn btn-info" onClick="select_course(\''.$id.'\')"><i class="fa  fa-plus"></i> เลือกหลักสูตร </a>';
	  }else if(!$_POST["type"]){
	  	if($r["parent_id"]<=0){
	  		$manage .= '<a class="btn btn-info" onClick="childlist(\''.$id.'\')"><i class="fa  fa-bars"></i> เพิ่มหลักสูตรย่อย </a>';
	  	}
	  }
	  $active = ($r["active"]=="T") ? "active" : "nonActive";   
	  $ck = $r["section_id"];
	  if(in_array($ck, $array_ck)) $manage = $manage; 

	  switch ($r['inhouse']) {
	  	case 'T': $inhouse = "Inhouse";
	  		break;
	  	case 'F': $inhouse = "Public";
	  		break;
	  }

	  if($_POST["type"]=="select-course"){
		$a[] = array($runNo
				      ,$r['code']
				      ,$r['title']."<div>".$r["detail"]."</div>"
				      ,$r['section_name']
				      ,revert_date($r["createtime"], true)
				      ,$r["coursetype_name"]
				      ,$inhouse
				      ,$manage);	  	
	  }else{
		$a[] = array($runNo
				      ,$r['code']
				      ,$r['title']."<div>".$r["detail"]."</div>"
				      ,$r['section_name']
				      ,$r["coursetype_name"]
				      ,$inhouse
				      ,$manage);
	  }

		$runNo++;
	}
}

$aData = array();
$sQuery = "SELECT COUNT(*) as total
			  FROM course a
			  $WHERE $sAND $sWhere";

$rs = $db->data($sQuery);
$iFilteredTotal = $rs;
 
$sQuery = "SELECT COUNT(*) as total
			  FROM course a";
$resultTotal = $db->data($sQuery);
$iTotal = $resultTotal;
						 
$aData["sEcho"] = intval($_POST['sEcho']);
$aData["iTotalRecords"] = $iTotal; 
$aData["iTotalDisplayRecords"] = $iFilteredTotal; 
$aData["aaData"] = $a; 

}

echo json_encode($aData);
?>
