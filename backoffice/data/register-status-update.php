<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/course.php";
global $db, $EMPID;
$id = trim($_POST["register_id"],",");
$pay_status = $_POST["status"];
$date_now = date('Y-m-d H:i:s');
$remark = $db->escape($_POST["remark"]);
if($id){
	$array = explode(",", $id);
	if($array){
		foreach ($array as $key => $register_id) {

			$this_file = basename($_SERVER["SCRIPT_FILENAME"], '.php');
			register_log_save($register_id, 'update', $this_file);

			$args = array();
			$reg_info = get_register("", $register_id);
			$reg_info = $reg_info[0];
			if ( $pay_status==3 && $reg_info["pay"]=='bill_payment' ) {			
				// $remark .= "/{$date_now} เปลี่ยนสถานะการชำระจาก ชำระเงินเกินกำหนด เป็น ชำระเงินแล้ว"; 
				$sql = "SELECT b.bank_code
					FROM bill_payment_upload_list_details AS b
					WHERE b.register_id={$register_id}
				";
				// echo $sql;die();
				$bill_info = $db->rows($sql);
				if ( !empty($bill_info) ) {
					$bank_code = sprintf("%01d", $bill_info["bank_code"]);
					$args["pay_method"] = (int)$bank_code;
				}//end if
			}//end if

			// $args = array();
			$args["table"] = "register";
			$args["id"] = $register_id;
			$args["pay_date"] = $date_now;
			$args["pay_status"] = $pay_status;
			$args["remark"] = $remark;
			$args["recby_id"] = $EMPID;
			$args["rectime"] = date("Y-m-d H:i:s");
			if($pay_status==3){
				$q = "SELECT coursetype_id
						, section_id 
						, course_price
						, course_discount
					FROM register 
					WHERE register_id={$register_id}
				";
				$v = $db->rows($q);
				$section_id = (int)$v["section_id"];
				$coursetype_id = (int)$v["coursetype_id"];

				$course_price = $v["course_price"];
				$course_discount = $v["course_discount"];
				$args["pay_price"] = $course_price-$course_discount;

				$t = rundocno($coursetype_id, $section_id);
				$args = array_merge($args, $t);	
			}
			$db->set($args);		
/*
			//write log
		    $str = "reg_id=".$register_id."|cos_type=".$coursetype_id."|sec_id=".$section_id."|docno=".$args["docno"];
			$date_now = date('Y-m-d H:i:s');
			$date_log = date('Y-m');
			$log_name = "./log/update-register_and_register-status-update_".$date_log.".log";
			$file = fopen($log_name, 'a');
			$str_txt = $date_now."|".$str."\r\n";
			fwrite($file, $str_txt);
			fclose($file);
*/
			if($register_id){
				$rs = get_register("", $register_id);
				$data = $rs[0];
				$ctype_id = (int)$data["coursetype_id"];
				$sec_id = (int)$data["section_id"];
				if($register_id){
				  // $rs = get_register("", $register_id);
				  // $data = $rs[0];
				$q = "SELECT a.member_id, a.password, a.email1, a.email2, a.fname_th, a.cid, a.lname_th FROM member a WHERE a.member_id={$data["member_id"]} and a.active='T'";
				$rowM = $db->rows($q);
				$M_PWD = $rowM['password'];
				$M_EMAIL1 = $rowM['email1'];
				$M_EMAIL2 = $rowM['email2'];	  
				  $sCourse_id = $data["course_id"];
				  $con = " and a.course_id in ($sCourse_id)";
				  $r = get_course($con);
				  $title = "";
				  $parent_id = 0;
				  $price = 0;
				  foreach ($r as $key => $row) {
				    $course_id = $row['course_id'];
				    $section_id = $row['section_id'];
				    $code = $row['code'];
				    $title .= $row['title'].", ";
				    $set_time = $row['set_time'];
				    $life_time = $row['life_time'];
				    $status = $row['status']; 
				    $parent_id = (int)$row['parent_id'];
				  }
				  $q = "select a.course_detail_id from register_course_detail a where a.register_id={$register_id} and a.course_id in ($sCourse_id)";
				  $cd = $db->get($q);

				  $title = trim($title, ", ");
				  $price = $data["course_price"];
				  $discount = $data["course_discount"];
				  if($cd){    
				    $display_date = "";
				    $display_address = "";
				    foreach ($cd as $kk => $vv) {
				      $row = get_course_detail(" and a.course_detail_id={$vv["course_detail_id"]}");
				      if($row) $row = $row[0];
				      $course_detail_id = $row['course_detail_id'];
				      $course_id = $row['course_id'];
				      $day = $row['day'];
				      $date = $row['date'];
				      $time = $row['time'];

				      $date_other = $row['date_other'];
				      $date_desc = $row['date_desc'];

				      $display_date .= revert_date($date)."&nbsp; ".$time.", ";
				      $display_address .= $row['address_detail']." ".$row['address'].", ";
				    }

				  }else{
				      $row = get_course_detail(" and a.course_detail_id={$data["course_detail_id"]}");
				      if($row) $row = $row[0];
				      $course_detail_id = $row['course_detail_id'];
				      $course_id = $row['course_id'];
				      $day = $row['day'];
				      $date = $row['date'];
				      $time = $row['time'];

				      $date_other = $row['date_other'];
				      $date_desc = $row['date_desc'];

				      $display_date .= revert_date($date)."&nbsp;เวลา ".$time.", ";
				      $display_address .= $row['address']." ".$row['address_detail'].", ";
				  }

				  $display_date = trim($display_date, ", ");
				  $display_address = trim($display_address, ", ");
				  $slip_type_text = "";
				  if($data["slip_type"]=="individuals") $slip_type_text = "ออกในนามบุคคลธรรมดา";
				  if($data["slip_type"]=="corparation") $slip_type_text = "ออกในนามนิติบุคคล (สำหรับเบิกค่าใช้จ่าย)";
				  $q = "select name from receipttype where receipttype_id={$data["receipttype_id"]}";
				  $receipttype_name = $db->data($q);
				  $q = "select name from district where district_id={$data["receipt_district_id"]}";
				  $district_name = $db->data($q);
				  $q = "select name from amphur where amphur_id={$data["receipt_amphur_id"]}";
				  $amphur_name = $db->data($q);
				  $q = "select name from province where province_id={$data["receipt_province_id"]}";
				  $province_name = $db->data($q);
				  $q = "select register_id from register a where a.active='T' and  a.course_id='$course_id'";
				  $id = $db->data($q);
				  $total_price = $price - $discount;
				  $t = convert_mktime($data["rectime"]) + (2 * 24 * 60 * 60);
				  $pay_date = $data["expire_date"];

				  $receipt_id = $data["receipt_id"];
				  $slip_name = "";
				  $slip_address = $data["receipt_no"]." ".$data["receipt_gno"]." ".$data["receipt_moo"]." ".$data["receipt_soi"]." ";
				  $slip_address .= $data["receipt_road"]." ".$district_name." ".$amphur_name." ".$province_name." ".$data["receipt_postcode"];

				  if ($data["slip_type"] == 'individuals') {
				  	$slip_name = $data["receipt_fname"]." ".$data["receipt_lname"];
				  }elseif($data["slip_type"] == 'corparation'){
				  	$q = "select name from receipt a where a.receipt_id=$receipt_id";
				  	$slip_name = $db->data($q);
				  }			

				  $ttl_price = $data["course_price"]-$data["course_discount"];
				  /*$q = "select pay from register a where a.active='T' and  a.course_id='$course_id'";
				  $pay = $db->data($q);*/

				  	$pay = $data['pay'];
				  	//$line_pay = "";
					$payMsg = "";
					if($pay=="bill_payment"){
						$line_pay = "Bill Payment : ";
						$payMsg = 'ช่องทางชำระ Bill Payment :<br>					
								รหัสการลงทะเบียน<br>
								Ref 1 : '.$data["ref1"] .'<br>
								Ref 2 : '.$data["ref2"] .'<br>
								ช่องทางชำระ Paysbuy : -<br>
								ช่องทางชำระ mPAY : -<br>
								ช่องทางชำระโดยชำระเงินที่ ATI : -<br>
								ช่องทางชำระโดยชำระเงินที่ ASCO : -<br>';
					}else{
						//$payMsg = sprintf("%06d", $register_id);
						$pay_id = sprintf("%06d", $register_id);

							if($pay=='paysbuy'){
								//$line_pay = "Paysbuy : ";
								$payMsg = "ช่องทางชำระ Bill Payment : -<br>";
								$payMsg .= "ช่องทางชำระ Paysbuy : ".$pay_id."<br>";
								$payMsg .= "ช่องทางชำระ mPAY : -<br>";
								$payMsg .= "ช่องทางชำระโดยชำระเงินที่ ATI : -<br>";
								$payMsg .= "ช่องทางชำระโดยชำระเงินที่ ASCO : -<br>";
							}
							if($pay=='mpay'){
								//$line_pay = "mPAY : ";
								$payMsg = "ช่องทางชำระ Bill Payment : -<br>";
								$payMsg .= "ช่องทางชำระ Paysbuy : -<br>";
								$payMsg .= "ช่องทางชำระ mPAY : ".$pay_id."<br>";
								$payMsg .= "ช่องทางชำระโดยชำระเงินที่ ATI : -<br>";
								$payMsg .= "ช่องทางชำระโดยชำระเงินที่ ASCO : -<br>";
							}
							if($pay=='at_ati'){
								//$line_pay = "ATI : ";
								$payMsg = "ช่องทางชำระ Bill Payment : -<br>";
								$payMsg .= "ช่องทางชำระ Paysbuy : -<br>";
								$payMsg .= "ช่องทางชำระ mPAY : -<br>";
								$payMsg .= "ช่องทางชำระโดยชำระเงินที่ ATI : ".$pay_id."<br>";
								$payMsg .= "ช่องทางชำระโดยชำระเงินที่ ASCO : -<br>";
							}
							if($pay=='at_asco'){
								//$line_pay = "ASCO : ";
								$payMsg = "ช่องทางชำระ Bill Payment : -<br>";
								$payMsg .= "ช่องทางชำระ Paysbuy : -<br>";
								$payMsg .= "ช่องทางชำระ mPAY : -<br>";
								$payMsg .= "ช่องทางชำระโดยชำระเงินที่ ATI : -<br>";
								$payMsg .= "ช่องทางชำระโดยชำระเงินที่ ASCO : ".$pay_id."<br>";
							}	

					}

					$message = "เรียน คุณ ".$data["fname"]." ".$data["lname"];
					$subject = "";
					$header = "";
					$html = "";
					//pay status
					switch ($pay_status) {
						/*case '1':
							$subject .= "แจ้งข้อมูลการลงทะเบียน";
							$header .= "ทางเราได้รับแจ้งว่าท่านมีความประสงค์ที่จะลงทะเบียนกิจกรรมโดยมีรายละเอียดดังนี้";
							$html .= '
							<table class="deviceWidth" align="center" border="0" cellpadding="0" cellspacing="0" width="100%"> 
								<tbody>
									<tr>
										<td class="center" style="font-size: 12px; color: #687074; font-weight: bold; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 20px 10px; " bgcolor="#ffffff">
											'.$message.'
											<br/>
											'.$header.'<br><br>
											ข้อมูลการลงทะเบียน<br>
											หลักสูตร : '.$title.'<br>
											วันที่ : '.$display_date.'<br>
											วันที่เป็นชุด : '.$date_other.'<br>
											description : '.$date_desc.'<br>
											สถานที่ : '.$display_address.'<br>
											ลงทะเบียนเมื่อ : '.$data["rectime"].'<br><br>								
											'.$payMsg.'<br>
											สถานะการลงทะเบียน : ลงทะเบียนแล้ว รอการชำระเงิน<br><br>
											ขอความกรุณาท่านชำระค่าธรรมเนียมภายในวันที่กำหนด<br><br>
											สถาบันฝึกอบรม สมาคมบริษัทหลักทรัพย์ (ATI)<br>
											ส่วนฝึกอบรมติดต่อ:Training@ati-asco.org โทร 02-264-0909 ต่อ 223<br>
											ส่วนทดสอบผู้แนะนำการลงทุนฯติดต่อ: Examination@ati-asco.org โทร 02-264-0909 ต่อ 203-207<br>
											ชมรมวาณิชธนกิจติดต่อ: fatraining@asco.or.th โทร 02-264-0909 ต่อ 124,125<br>
										</td>
									</tr>		
								</tbody>
							</table>
							';
							break;*/
						case '2':
							$subject .= "แจ้งการชำระเงินเกินกำหนด";
							$header .= "ทางเราได้รับแจ้งว่าท่านได้ชำระค่าธรรมเนียมเกินกำหนด ให้ท่านติดต่อเจ้าหน้าที่ที่เกี่ยวข้องโดยด่วน เพื่อเป็นการยืนยันสิทธิ์ในกิจกรรมที่ท่านได้ลงทะเบียนไว้";
							$html .= '
							<table class="deviceWidth" align="center" border="0" cellpadding="0" cellspacing="0" width="100%"> 
								<tbody>
									<tr>
										<td class="center" style="font-size: 12px; color: #687074; font-weight: bold; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 20px 10px; " bgcolor="#ffffff">
											'.$message.'
											<br/>
											'.$header.'<br><br>
											ข้อมูลการลงทะเบียน<br>
											หลักสูตร : '.$title.'<br>
											วันที่ : '.$display_date.'<br>
											วันที่เป็นชุด : '.$date_other.'<br>
											description : '.$date_desc.'<br>
											สถานที่ : '.$display_address.'<br>
											ลงทะเบียนเมื่อ : '.$data["rectime"].'<br><br>								
											'.$payMsg.'<br>
											สถานะการลงทะเบียน : ชำระเงินเกินกำหนด<br><br>
											ข้อมูลเพื่อการออกใบเสร็จ<br><br>
											ค่าธรรมเนียม: 1000 บาท (รวมภาษีมูลค่าเพิ่ม 7%)<br>
											ออกใบเสร็จในนาม: ธ. กรุงไทย จำกัด (มหาชน)<br>
											สาขาที่ : สำนักงานใหญ่<br>
											เลขประจำตัวผู้เสียภาษี : 0107537000882<br>
											ที่อยู่ :35 ถ.สุขุมวิท แขวงคลองเตยเหนือ เขตวัฒนา กรุงเทพมหานคร 10110<br><br>
											สถาบันฝึกอบรม สมาคมบริษัทหลักทรัพย์ (ATI)<br>
											ส่วนฝึกอบรมติดต่อ:Training@ati-asco.org โทร 02-264-0909 ต่อ 223<br>
											ส่วนทดสอบผู้แนะนำการลงทุนฯติดต่อ: Examination@ati-asco.org โทร 02-264-0909 ต่อ 203-207<br>
											ชมรมวาณิชธนกิจติดต่อ: fatraining@asco.or.th โทร 02-264-0909 ต่อ 124,125<br>
										</td>
									</tr>		
								</tbody>
							</table>
							';
							break;
						case '3':
							$subject .= "ยืนยันการชำระค่าธรรมเนียม";
							$header .= "ทางเราได้รับค่าธรรมเนียม จากท่านเป็นที่เรียบร้อยแล้ว และขอยืนยันการลงทะเบียนของท่าน ตามรายละเอียดดังนี้:";
							$html .= '
							<table class="deviceWidth" align="center" border="0" cellpadding="0" cellspacing="0" width="100%"> 
								<tbody>
									<tr>
										<td class="center" style="font-size: 12px; color: #687074; font-weight: bold; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 20px 10px; " bgcolor="#ffffff">
											'.$message.'
											<br/>
											'.$header.'<br><br>
											ข้อมูลการลงทะเบียน<br>
											หลักสูตร : '.$title.'<br>
											วันที่ : '.$display_date.'<br>
											วันที่เป็นชุด : '.$date_other.'<br>
											description : '.$date_desc.'<br>
											สถานที่ : '.$display_address.'<br>
											ลงทะเบียนเมื่อ : '.$data["rectime"].'<br><br>								
											'.$payMsg.'<br>
											สถานะการลงทะเบียน : ชำระเงินแล้ว ยืนยันการลงทะเบียน<br><br>
											ข้อมูลเพื่อการออกใบเสร็จ<br><br>
											ค่าธรรมเนียม: '.$ttl_price.' บาท (รวมภาษีมูลค่าเพิ่ม 7%)<br>
											ออกใบเสร็จในนาม: '.$slip_name.'<br>
											เลขประจำตัวผู้เสียภาษี : '.$data["taxno"].'<br>
											ที่อยู่ :'.$slip_address.'<br><br>
											** สำหรับผู้ที่สมัครสอบ License ของ Paper ใดๆ กรุณาอ่านระเบียบการเข้าสอบโดยการ <a style="text-decoration: none; color: #27d7e7;" href="http://www.ati-asco.org/testing_howto_regis.php"> คลิกที่นี่</a><br><br>
											สถาบันฝึกอบรม สมาคมบริษัทหลักทรัพย์ (ATI)<br>
											ส่วนฝึกอบรมติดต่อ:Training@ati-asco.org โทร 02-264-0909 ต่อ 223<br>
											ส่วนทดสอบผู้แนะนำการลงทุนฯติดต่อ: Examination@ati-asco.org โทร 02-264-0909 ต่อ 203-207<br>
											ชมรมวาณิชธนกิจติดต่อ: fatraining@asco.or.th โทร 02-264-0909 ต่อ 124,125<br>
										</td>
									</tr>		
								</tbody>
							</table>
							';
							break;													
						default:
							
							break;
					}
					
				}

				/*var_dump($html);
				die();*/

				/**
				 * This example shows making an SMTP connection with authentication.
				 */

				//SMTP needs accurate times, and the PHP time zone MUST be set
				//This should be done in your php.ini, but this is how to do it if you don't have access to that
				//date_default_timezone_set('Etc/UTC');

				include_once  "../PHPMailer-master/PHPMailerAutoload.php";

				//Create a new PHPMailer instance
				$mail = new PHPMailer;
				//Tell PHPMailer to use SMTP
				$mail->isSMTP();
				//Enable SMTP debugging
				// 0 = off (for production use)
				// 1 = client messages
				// 2 = client and server messages
				// $mail->SMTPDebug = 1; 
				//Ask for HTML-friendly debug output

				//$email = $_POST['email'];


				//$message = "เรียน คุณ ".$data["fname"]." ".$data["lname"];
				
				$mail->CharSet = "utf-8";
				$mail->ContentType = "text/html";

				$mail->Debugoutput = 'html'; 
				//Set the hostname of the mail server
				$mail->Host = "mail.ati-asco.org"; 
				//Set the SMTP port number - likely to be 25, 465 or 587
				$mail->Port = 25; 
				//Whether to use SMTP authentication
				$mail->SMTPAuth = true; 
				//Username to use for SMTP authentication
				$mail->Username = "register@ati-asco.org"; 
				//Password to use for SMTP authentication
				$mail->Password = "Ati_1234"; //Set who the message is to be sent from
				$mail->setFrom('register@ati-asco.org', 'ASCO Training Institute (ATI)'); 
				//Set an alternative reply-to address
				// $mail->addReplyTo('thictyouth@numplus.in.th', 'Koom2'); 
				//Set who the message is to be sent to
				$mail->addAddress($M_EMAIL1, 'You'); 
				if($M_EMAIL2 != ""){
				//Set Sent to CC
					$mail->addCC($M_EMAIL2); 
				}
				// $mail->AddCC('Examination@ati-asco.org');
				// $mail->AddCC('reg_training@ati-asco.org');

				if ( $sec_id==1 && $ctype_id==2 ) {
					$mail->AddCC('Examination@ati-asco.org');
					$ccto = "Examination";
				}elseif ( ($sec_id==1 && $ctype_id!=2) || ($sec_id==2) ) {
					$mail->AddCC('reg_training@ati-asco.org');
					$ccto = "reg_training";
				}elseif ($sec_id==3) {
					$mail->AddCC('fatraining@asco.or.th');
					$ccto = "fatraining";
				}

				//Set the subject line
				$mail->Subject = $subject;
				//$mail->Subject = 'แจ้งข้อมูลการลงทะเบียน';   
				//Read an HTML message body from an external file, convert referenced images to embedded,
				//convert HTML into a basic plain-text alternative body
				$mail->msgHTML($html); 

				//Replace the plain text body with one created manually
				// $mail->AltBody = 'This is a Automatic Mail'; 
				//Attach an image file


				//send the message, check for errors
				unset($_SESSION["reister_info_sendmain"]);
				$rs = $mail->send();
				$msg_error = "";
				if (!$rs) {
				  	$_SESSION["reister_info_sendmain"]["error"] = " มีข้อผิดพลาดเกิดขึ้น กรุณาติดต่อผู้ดูแลระบบ";
				  	// $msg_error = $mail->ErrorInfo;
				} else {
				   //echo "UPDATE SUCCESSFULLY";
				   $_SESSION["reister_info_sendmain"]["msg"] = "ระบบได้ส่ง รหัสผ่านไปยัง Email ของท่านเรียบร้อยแล้ว";
				}
			/*	print_r($_SESSION["reister_info_sendmain"]);
				print_r($mail);
				echo $html;*/

				//write log
				$date_now = date('Y-m-d H:i:s');
				$date = date('Y-m-d');
				$str_txt = $date_now;
				$str_txt .= "|type=sendmail|register_id={$register_id}|rs={$rs}|msg_error={$msg_error}";
				$str_txt .= "\r\n";

				$file_log = "./log/register-status-update-{$date}.log";
				$file = fopen($file_log, 'a');
				fwrite($file, $str_txt);
				fclose($file);

			}
		}
		echo "UPDATE SUCCESSFULLY";
	}
}
die();
?>