<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/course.php";
global $db;

$course_id = $_POST["course_id"];
$array_data = array();
if($course_id){
	  $con= " and a.parent_id in ($course_id)";
	  $r = get_course($con, "", true);
	   foreach($r as $k=>$v){
	   	  $v["course_child_id"] = $v["course_id"];
	   	  $v["edit_info"] = get_datatable_icon("edit", $v["course_id"]);
	      $array_data[] = $v;
	   }      
}

echo json_encode($array_data); ?>

