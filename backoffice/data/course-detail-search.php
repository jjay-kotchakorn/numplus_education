<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/course.php";
global $db;
$search = $db->escape(trim($_POST["search"]));
$course_id = $_POST["course_id"];
$id = $_POST["id"];
$con = "";
if($_POST["othercon"]){
    $a = explode("-", $_POST["othercon"]);
    $con_id = trim($a[1],",");
    if($con_id){
       $con .= " and a.{$a[0]} in ($con_id)";
    }   
}
$dateStart = ($_POST["date_start"]) ? thai_to_timestamp($_POST["date_start"]) :  '';
$dateStop =  ($_POST["date_stop"]) ? thai_to_timestamp($_POST["date_stop"]) : '';
if ($dateStart || $dateStop) {
    if (!$dateStart && $dateStop)
        $dateStart = $dateStop;
    if (!$dateStop && $dateStart)
        $dateStop = $dateStart;
    $t = $dateStart;
    if ($dateStart > $dateStop) {
        $dateStart = $dateStop;
        $dateStop = $t;
    }
}
$con .= ($dateStart && $dateStop) ? " and a.date>='$dateStart' and a.date<='$dateStop'" : "";
if($search){
    $con .=  " and (a.address like '%$search%' or a.address_detail like '%$search%')";
}
$con .= ($course_id) ? " and a.course_id={$course_id}" : "";
$array_data = array();
if($con){
    $r = get_course_detail($con, "", false, 50);
      if($r){        
    	   foreach($r as $k=>$v){
    	   	  $v["open_regis"] = revert_date($v["open_regis"]);
    	   	  $v["end_regis"] = revert_date($v["end_regis"]);
    	   	  $v["date"] = $v["day"]." ".revert_date($v["date"]);
    	      $array_data[] = $v;
    	   }
      }
        if($course_id){
            $r = array();
            $con = "";
            $con = " and a.course_id={$course_id}";
            $r = get_course_detail_list($con);
            if($r){
               foreach($r as $k=>$v){
                  $v["open_regis"] = revert_date($v["open_regis"]);
                  $v["end_regis"] = revert_date($v["end_regis"]);
                  $v["date"] = revert_date($v["date"]);
                  $array_data[] = $v;
               }      
                
            }
        }      
}

echo json_encode($array_data); ?>

