<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/course.php";
global $db;

$course_id = $_POST["course_id"];
$array_data = array();
if($course_id){
	$con = "and a.course_id={$course_id}";
    $r = get_course_detail_list($con,  true);
	   foreach($r as $k=>$v){
	   	  $v["open_regis"] = revert_date($v["open_regis"]);
	   	  $v["end_regis"] = revert_date($v["end_regis"]);
	   	  $v["date"] = revert_date($v["date"]);
	      $array_data[] = $v;
	   }      
}

echo json_encode($array_data); ?>

