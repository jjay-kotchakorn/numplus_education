<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "overdue-sendmail.php";
include_once "../share/course.php";
global $db;

$q = "select register_id, expire_date, pay_status from register where active='T' and pay_status=1 and pay='bill_payment'";
$r = $db->get($q);

$q = "select a.effective_date from bill_payment_upload_list a order by bill_payment_upload_list_id desc";
$last_day_import = $db->data($q);
if($last_day_import){						
	$p_dd = substr($last_day_import, 0,2);
	$p_mm = substr($last_day_import, 2,2);
	$p_yy = substr($last_day_import, 4,4);
	$last_date = date("{$p_yy}-{$p_mm}-{$p_dd}");
	$start = convert_mktime($last_date." 23:59:59");
}
foreach ($r as $key => $data) {
	$register_id = $data["register_id"];	
	$exd = explode(" ", $data["expire_date"]);
	$expire_date = $exd[0];
	$stop = convert_mktime($expire_date." 23:59:59");
	$time = $stop - $start;
	$status = ($time>0) ? "T" : "F";
	if($status=="F"){
		$args = array();
		$args["table"] = "register";
		$args["id"] = $register_id;
		$args["pay_status"] = 4;
		$args["remark"] = ($_POST["use_click"]=="T") ? "อัพเดทข้อมูลโดยการกดปุ่มเตะที่นั่ง" : "auto update";
		$db->set($args);
		sendmail($register_id);

		$this_file = basename($_SERVER["SCRIPT_FILENAME"], '.php');
		register_log_save($register_id, 'update', $this_file, false);	
	}
	if($_POST["use_click"]=="T"){
	}else{	

		$msg = date("Y-m-d H:i:s", $start)." - ".date("Y-m-d H:i:s", $stop)." =  ".$status;
		write_log("con-register-overdue-bill-payment", $msg, "a", "./log/" ); 

		echo date("Y-m-d H:i:s", $start)." - ".date("Y-m-d H:i:s", $stop)." =  ".$status;
		echo "<hr>";
	}
}

if($_POST["use_click"]=="T"){
	?>
	<div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<i class="fa fa-check sign"></i><strong>Success!</strong> อัพเดทข้อมูลเรียบร้อยแล้ว!
	</div>

	<?php

}
?>