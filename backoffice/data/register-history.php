<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/datatype.php";
include_once "../share/course.php";
global $db;
$apay = datatype(" and a.active='T'", "pay_status", true);
$arr_pay = array();
$str_option = '';
$str_option_no_cancel = '';
foreach ($apay as $key => $value) {
	$arr_pay[$value["pay_status_id"]] = $value["name"];
	$name = $value['name'];
	$str_option .= '<option value="'.$value["pay_status_id"].'" >'.$name.'</option>';
	if($value["pay_status_id"]==8) continue;
	$str_option_no_cancel .= '<option value="'.$value["pay_status_id"].'" >'.$name.'</option>';
}
set_time_limit(600);
$single_info = $_POST["single"];
if($single_info=="T"){
	$aData = array();
	$id = $_POST["member_id"];
	if($id){
   $r = view_member("", $id);
    foreach ($r as $k => $v) {
    	$v["select_university"] = $v["grd_ugrp1"]."-".$v["grd_uid1"];
    	$v["birthdate"] = revert_date($v["birthdate"]);
    	$v["password2"] = $v["password"];     
	    $aData[] = $v;	   }  
	}
}else{


function fnColumnToField( $i ){
	/* Note that column 0 is the details column */
	if ( $i == 0 ||$i == 1 )
		return "a.register_id";
	else if ( $i == 2 )
		return "a.cid";
	else if ( $i == 3 )
		return "a.fname";
	else if ( $i == 4 )
		return "a.lname";
	else
		return "a.register_id";
}

$sLimit = "";
if (isset( $_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1' )
{
	$sLimit = "LIMIT ".(int)($_POST['iDisplayStart'] );
	$sLimit .= ", ".(int)( $_POST['iDisplayLength'] );
}


/* Ordering */
if(isset($_POST['iSortCol_0'])){
	$sOrder = "ORDER BY  ";
	for ( $i=0 ; $i<$db->escape( $_POST['iSortingCols'] ) ; $i++ ){
		$sOrder .= fnColumnToField($db->escape( $_POST['iSortCol_'.$i] ))."
                ".$db->escape( $_POST['sSortDir_'.$i] ) .", ";
	}
	$sOrder = substr_replace( $sOrder, "", -2 );
}
 
/* Filtering */
$sWhere = "";
$WHERE = "WHERE a.REGISTER_CID={$_POST["cid"]} ";



/* Paging */
$sQuery = "SELECT a.`﻿EXAMINATION_ID`,
				a.REGISTER_NO,
				a.REGISTER_ID,
				a.REGISTER_MEMBER_ID,
				a.REGISTER_TITLE,
				a.REGISTER_FNAME,
				a.REGISTER_LNAME,
				a.REGISTER_CID,
				a.REGISTER_SLIP,
				a.REGISTER_SLIP_NAME,
				a.REGISTER_SLIP_ADDRESS1,
				a.REGISTER_SLIP_ADDRESS2,
				a.REGISTER_DATE,
				a.REGISTER_BY,
				a.REGISTER_REF1,
				a.REGISTER_REF2,
				a.REGISTER_STATUS,
				a.REGISTER_EXPIRE_DATE,
				a.REGISTER_EXAM_DATE,
				a.REGISTER_COURSE_ID,
				a.REGISTER_COURSE_PRICE,
				a.REGISTER_PAY,
				a.REGISTER_PAY_DATE,
				a.REGISTER_PAY_METHOD,
				a.REGISTER_PAY_YR,
				a.REGISTER_PAY_MN,
				a.REGISTER_PAY_ID,
				a.REGISTER_PAY_PRICE,
				a.REGISTER_PAY_DIFF,
				a.REGISTER_APPROVE,
				a.REGISTER_APPROVE_BY,
				a.REGISTER_APPROVE_DATE,
				a.REGISTER_MOD,
				a.REGISTER__LAST_MOD,
				a.REGISTER__LAST_MOD_DATE,
				a.REGISTER__RESULT,
				a.REGISTER__RESULT_DATE,
				a.REGISTER__RESULT_BY,
				c.code,
				c.title,
				b.EXAMINATION_PLACE
		FROM register_history a left join examination b on a.﻿EXAMINATION_ID=b.EXAMINATION_ID
			 left join course_history c on a.REGISTER_COURSE_ID=c.course_id
		$WHERE $sWhere
		$sOrder
		$sLimit";
$rResult = $db->get($sQuery);
$a = array();
if(is_array($rResult)){
	$runNo = 1;
	foreach ($rResult as $v){
			$a[] = array($runNo
				,revert_date($v["REGISTER_DATE"])
				,$v["REGISTER_CID"]
				,$v['REGISTER_TITLE']." ".$v['REGISTER_FNAME']." ".$v['REGISTER_LNAME']
				,$v["EXAMINATION_PLACE"]
				,$display_address
				,$title
				,"[".$v["code"]."]".$v["title"]
				     // ,revert_date($v['date'])
				,($status==1) ? $select_option  : $status_pay
				,$manage);
		$runNo++;
	}
}

$aData = array();
$sQuery = "SELECT COUNT(*) as total
			  FROM register a
			  $WHERE $sWhere";

$rs = $db->data($sQuery);
$iFilteredTotal = $rs;
 
$sQuery = "SELECT COUNT(*) as total
			  FROM register a";
$resultTotal = $db->data($sQuery);
$iTotal = $resultTotal;
						 
$aData["sEcho"] = intval($_POST['sEcho']);
$aData["iTotalRecords"] = $iTotal; 
$aData["iTotalDisplayRecords"] = $iFilteredTotal; 
$aData["aaData"] = $a; 

}

echo json_encode($aData);
?>
