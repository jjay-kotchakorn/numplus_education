<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
// include_once "../share/course.php";
global $db, $EMPID;

if( !empty($_POST) ){
	
	$member_id = $_POST["member_id"];
	$course_detail_id = $_POST["course_detail_id"];
	$q = "SELECT register_id 
		FROM register a 
		WHERE a.active='T' 
			AND a.pay_status IN (1,3,5,6)
			AND a.course_detail_id LIKE '%{$course_detail_id}%' 
			AND a.member_id = {$member_id} 
	";
	$register_id = $db->data($q);

	if( !empty($register_id) ){
		$q = "SELECT auto_del FROM register WHERE register_id={$register_id} AND pay_status=1";
		$auto_del = $db->data($q);
		if ( $auto_del=='T' ) {

			$args = array();
			$args["table"] = "register";
			$args["id"] = $register_id;
			$args["active"] = 'F';
			$db->set($args);
			
			$q = "UPDATE register_course
				SET active = 'F'
				WHERE register_id={$register_id};
			";
			$db->query($q);

			$q = "UPDATE register_course_detail
				SET active = 'F'
				WHERE register_id={$register_id};
			";
			$db->query($q);

/*			
			$q = "delete from register where register_id=$register_id";
			$db->query($q);
			$q = "delete from register_course where register_id=$register_id";
			$db->query($q);
			$q = "delete from register_course_detail where register_id=$register_id";
			$db->query($q);
*/

			unset($register_id);
		}else{
			echo "4";
		}
	}
	
	if(!$register_id){
/*
		$course_id = $_POST["course_id"];
		$course_detail_id = $_POST["course_detail_id"];

		$cos_info = get_course("", $course_id);
		$cos_info = $cos_info[0];
		
		$cos_dt_info = get_course_detail("", $course_detail_id);
		$cos_dt_info = $cos_dt_info[0];

		$course_price = $_POST["price"];
		$course_discount = $_POST["discount"];
		$pay_price = $course_price - $course_discount;
*/
		$args["table"] = "register";
		$args["member_id"] = $_POST["member_id"];
		$args["cid"] = $_POST["cid"];
		$args["title"] = $_POST["title_th"];
		$args["title"] = $_POST["title_th"];
		$t = $db->data("select title_th_text from member where member_id={$member_id}");
		if($args["title"]=="อื่นๆ") $args["title"] = $t;	
		$args["fname"] = $_POST["fname_th"];
		$args["lname"] = $_POST["lname_th"];
		$args["slip_type"] = $_POST["slip_type"];
		$args["date"] = date("Y-m-d H:i:s");
		$args["pay_date"] = date("Y-m-d H:i:s");
		$args["pay_status"] = $_POST["pay_status"];
		$args["status"] =  $_POST["pay_status"];
		$args["pay"] = "walkin";
		$args["receipttype_id"] =  (int)$_POST["receipttype_id"];
		$args["receipttype_text"] = $_POST["receipttype_text"];
		$args["receipt_id"] = (int)$_POST["receipt_id"];
		$args["taxno"] = $_POST["taxno"];
		$args["receipt_title"] = $_POST["receipt_title"];
		$args["receipt_fname"] = $_POST["receipt_fname"];
		$args["receipt_lname"] = $_POST["receipt_lname"];
		$args["receipt_no"] = $_POST["receipt_no"];
		$args["receipt_gno"] = $_POST["receipt_gno"];
		$args["receipt_moo"] = $_POST["receipt_moo"];
		$args["receipt_soi"] = $_POST["receipt_soi"];
		$args["receipt_road"] = $_POST["receipt_road"];
		$args["receipt_province_id"] = (int)$_POST["receipt_province_id"];
		$args["receipt_amphur_id"] = (int)$_POST["receipt_amphur_id"];
		$args["receipt_district_id"] = (int)$_POST["receipt_district_id"];
		$args["receipt_postcode"] = $_POST["receipt_postcode"];
		$args["course_id"] = $_POST["course_id"];
		$args["course_detail_id"] = $_POST["course_detail_id"];


		$args["course_price"] = $_POST["price"];
		$args["course_discount"] = (int)$_POST["discount"];
		$args["pay_price"] =  (int) $_POST["price"] -(int)$_POST["discount"];


		$args["section_id"] = (int)$_POST["section_id"];
		$args["coursetype_id"] = (int)$_POST["coursetype_id"];
		$args["usebook"] = ($_POST["usebook"]=="T") ? "T" : "F";
		$args["rectime"] = date("Y-m-d H:i:s");
		if($args["pay_status"]==3){			
			$section_id = (int)$_POST["section_id"];
			$coursetype_id = (int)$_POST["coursetype_id"];
			$t = rundocno($coursetype_id, $section_id);
			$args = array_merge($args, $t);
		}
  		if((int)$section_id==3){
  			$db->query("update member set fa='T' where member_id={$_POST["member_id"]}");
  		}
		$register_id = $db->set($args);	

		//insert to TB register_course_detail
		if ( !empty($register_id) ) {
			$args = array();
			$args["table"] = "register_course_detail";
			$args["register_id"] = $register_id;
			$args["course_id"] = $_POST["course_id"];
			$args["course_detail_id"] = $_POST["course_detail_id"];
			$args["rectime"] = date("Y-m-d H:i:s");
			$args["recby_id"] = $EMPID;
			$args["remark"] = "walkin";
			$register_course_detail_id = $db->set($args);	
		}//end if

		echo $register_id;
	}
}

?>