<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/bill-payment-upload.php";
global $db;

$single_info = $_POST["single"];
if($single_info=="T"){
	$aData = array();
	$id = $_POST["bill_payment_upload_id"];
	if($id){
	   $r = view_bill_payment_upload("", $id);
	   foreach($r as $k=>$v){
	      $aData[] = $v;
	   }  
	}
}else{

function fnColumnToField( $i ){
	/* Note that column 0 is the details column */
	if ( $i == 0)
		return "a.bill_payment_upload_id";
	else if ( $i == 1 )
		return "a.name";
	else 
		return "a.bill_payment_upload_id";
}

$sLimit = "";
if (isset( $_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1' )
{
	$sLimit = "LIMIT ".(int)($_POST['iDisplayStart'] );
	$sLimit .= ", ".(int)( $_POST['iDisplayLength'] );
}


/* Ordering */
if(isset($_POST['iSortCol_0'])){
	$sOrder = "ORDER BY  ";
	for ( $i=0 ; $i<$db->escape( $_POST['iSortingCols'] ) ; $i++ ){
		$sOrder .= fnColumnToField($db->escape( $_POST['iSortCol_'.$i] ))."
                ".$db->escape( $_POST['sSortDir_'.$i] ) .", ";
	}
	$sOrder = substr_replace( $sOrder, "", -2 );
}
 
/* Filtering */
  $sWhere = "";
  $WHERE = "WHERE a.active!='' ";
  $sAND = "";
if($_POST['sSearch'] != ""){
   $sWhere = "a.name LIKE '%".$db->escape( $_POST['sSearch'] )."%'";
	$sAND = "AND ";
}
/* Paging */
$sQuery = "SELECT a.bill_payment_upload_id, a.code, a.name, a.active, a.remark, CONCAT(b.prefix,b.fname,' ',b.lname) as recby_name
           FROM bill_payment_upload a inner join emp b on a.recby_id=b.emp_id
		   $WHERE $sAND $sWhere
		   $sOrder
		   $sLimit";

$rResult = $db->get($sQuery);
$a = array();
if(is_array($rResult)){
	$runNo = 1;
	foreach ($rResult as $r){
		$id = $r["bill_payment_upload_id"]; 
	  $manage =  get_datatable_icon("edit", $id);
	  $active = ($r["active"]=="T") ? "active" : "nonActive";   
		$a[] = array($runNo
				      ,$r['name']
				      ,$r["recby_name"]
				      ,$r["remark"]
				      ,$manage);
		$runNo++;
	}
}

$aData = array();
$sQuery = "SELECT COUNT(*) as total
			  FROM bill_payment_upload a
			  $WHERE $sAND $sWhere";

$rs = $db->data($sQuery);
$iFilteredTotal = $rs;
 
$sQuery = "SELECT COUNT(*) as total
			  FROM bill_payment_upload a";
$resultTotal = $db->data($sQuery);
$iTotal = $resultTotal;
						 
$aData["sEcho"] = intval($_POST['sEcho']);
$aData["iTotalRecords"] = $iTotal; 
$aData["iTotalDisplayRecords"] = $iFilteredTotal; 
$aData["aaData"] = $a; 

}

echo json_encode($aData);
?>
