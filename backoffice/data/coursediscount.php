<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/course.php";
global $db;

$single_info = $_POST["single"];
if($single_info=="detail"){
	$aData = array();
	$id = $_POST["course_discount_id"];
	if($id){
	   $r = get_course_discount("", $id);
	   foreach($r as $k=>$v){
	   	if($v["date_start"]!='0000-00-00 00:00:00')
	   	  $v["date_start"] = revert_date($v["date_start"]);
	   	if($v["date_stop"]!='0000-00-00 00:00:00')
	   	  $v["date_stop"] = revert_date($v["date_stop"]);
	      $aData[] = $v;
	   }  
	}
}else{

function fnColumnToField( $i ){
	/* Note that column 0 is the details column */
	if ( $i == 0 ||$i == 1 )
		return "a.course_discount_id";
	else if ( $i == 2 )
		return "a.code";
	else if ( $i == 3 )
		return "a.name";
	else if ( $i == 4 )
		return "a.section_id";
	else if ( $i == 5 )
		return "a.coursetype_id";
	else return "a.course_discount_id";
}


$sLimit = "";
if (isset( $_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1' )
{
	$sLimit = "LIMIT ".(int)($_POST['iDisplayStart'] );
	$sLimit .= ", ".(int)( $_POST['iDisplayLength'] );
}

/* Ordering */
if(isset($_POST['iSortCol_0'])){
	$sOrder = "ORDER BY  ";
	for ( $i=0 ; $i<$db->escape( $_POST['iSortingCols'] ) ; $i++ ){
		$sOrder .= fnColumnToField($db->escape( $_POST['iSortCol_'.$i] ))."
                ".$db->escape( $_POST['sSortDir_'.$i] ) .", ";
	}
	$sOrder = substr_replace( $sOrder, "", -2 );
}
 
/* Filtering */
  $sWhere = "";
  $WHERE = "WHERE a.active!='' ";
  $sAND = "";
if($_POST['sSearch'] != ""){
   $sWhere = "a.code LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "a.name LIKE '%".$db->escape( $_POST['sSearch'] )."%'";
	$sAND = "AND ";
}
$sWhere .= ($_POST["section_id"]) ? " and a.section_id={$_POST["section_id"]}" : "";
$sWhere .= ($_POST["coursetype_id"]) ? " and a.coursetype_id={$_POST["coursetype_id"]}" : "";
$sWhere .= ($_POST["active"]) ? " and a.active='{$_POST["active"]}'" : "";
if($SECTIONID>0){
	$sWhere .= " and a.section_id=$SECTIONID";
}

/* Paging */
$sQuery = "SELECT a.course_discount_id, a.code, a.name, a.section_id, b.name as section_name, a.active, a.coursetype_id
				 ,c.name as coursetype_name, a.discount, a.detail, a.parent_id, a.date_start, a.date_stop
           FROM course_discount a left join section b on b.section_id=a.section_id
           		left join coursetype c on c.coursetype_id=a.coursetype_id
		   $WHERE $sAND $sWhere
		   $sOrder
		   $sLimit";

$rResult = $db->get($sQuery);
$a = array();
if(is_array($rResult)){
	$runNo = 1;
	$list_id = get_config('sectionDisplayButton');
	$array_ck = explode(",", $list_id);
	
	
	foreach ($rResult as $r){
	  $id = $r["course_discount_id"];
	  $displayButton = '<a class="btn btn-info" onClick="promotionInfo(\''.$id.'\')"><i class="fa  fa-bars"></i> </a>';
	  $childlistBt = '<a class="btn btn-info" onClick="childlist(\''.$id.'\')"><i class="fa  fa-bars"></i> เพิ่มหลักสูตรย่อย </a>';
	  $manage =  ($_POST["type"]=="childlist") ? $childlistBt : get_datatable_icon("edit", $id);
	  $active = ($r["active"]=="T") ? "active" : "nonActive";   
	  $ck = $r["section_id"];
	  if(in_array($ck, $array_ck)) $manage = $manage; 
		$a[] = array($runNo
				      ,$r['code']
				      ,$r['name']
				      ,set_comma($r['discount'])
				      ,revert_date($r['date_start'])
				      ,revert_date($r['date_stop'])
				      ,$r['section_name']
				      ,$r["coursetype_name"]
				      ,$manage);
		$runNo++;
	}
}

$aData = array();
$sQuery = "SELECT COUNT(*) as total
			  FROM course_discount a
			  $WHERE $sAND $sWhere";

$rs = $db->data($sQuery);
$iFilteredTotal = $rs;
 
$sQuery = "SELECT COUNT(*) as total
			  FROM course_discount a";
$resultTotal = $db->data($sQuery);
$iTotal = $resultTotal;
						 
$aData["sEcho"] = intval($_POST['sEcho']);
$aData["iTotalRecords"] = $iTotal; 
$aData["iTotalDisplayRecords"] = $iFilteredTotal; 
$aData["aaData"] = $a; 

}

echo json_encode($aData);
?>
