<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/course.php";
global $db;

$course_id = $_POST["course_id"];
$course_detail_id = $_POST["course_detail_id"];
$output = "";
if($course_id && $course_detail_id){
	$vc = get_course("", $course_id);
	$vc = $vc[0];
	$output =  gen_elearning_input_line($vc, $course_detail_id);
}
echo $output;
die();
?>
