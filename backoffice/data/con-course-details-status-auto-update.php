<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/course.php";

$curr_date = date('Y-m-d');
$qry = "SELECT course_detail_id
			, status
			, open_regis
			, end_regis
			, now() AS curr_date
		FROM course_detail
		WHERE active='T' AND inhouse='F' 
			AND open_regis BETWEEN '$curr_date 00:00:00' AND '$curr_date 23:59:59'
			AND ( status='ปิดรับสมัคร' OR status='ปิดรับสมัครชั่วคราว' )
			-- AND status='ยกเลิกรอบสอบ'
";

$cos_dt = $db->get($qry);	
//d($cos_dt);
$count = count($cos_dt);
$run_no = 0;

if ($cos_dt) {
	foreach ($cos_dt as $key => $v) {
		$cos_dt_id = $v["course_detail_id"];
		$date_now = date('Y-m-d H:i:s');
		$q = "UPDATE course_detail SET status='เปิดรับสมัคร', datetime_update='$date_now' WHERE course_detail_id=$cos_dt_id";
		$db->query($q);
		$run_no++;
		$str_txt = "{$run_no}/{$count} => Update status เปิดรับสมัคร course_detail_id={$cos_dt_id} => success\r\n";
		//echo "{$run_no}/{$count} => Update status เปิดรับสมัคร course_detail_id={$cos_dt_id} => success <hr>";
		echo $str_txt."<hr>";

		//write log
		$log_name = "./log/con-course-details-status-auto-update.log";
		$file = fopen($log_name, 'a');
		//$date_now = date('Y-m-d H:i:s');
		$str_txt = $date_now."|".$str_txt;
		fwrite($file, $str_txt);
		fclose($file);
	}//end loop
}//end if



?>