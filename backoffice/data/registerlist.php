<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/datatype.php";
include_once "../share/course.php";
global $db;
$apay = datatype(" and a.active='T'", "pay_status", true);
$arr_pay = array();
$str_option = '';
$str_option_no_cancel = '';
foreach ($apay as $key => $value) {
	$arr_pay[$value["pay_status_id"]] = $value["name"];
	$name = $value['name'];
	$str_option .= '<option value="'.$value["pay_status_id"].'" >'.$name.'</option>';
	if($value["pay_status_id"]==8) continue;
	if($value["pay_status_id"]==9) continue;
	if($value["pay_status_id"]==7) continue;
	if($value["pay_status_id"]==6) continue;
	if($value["pay_status_id"]==5) continue;
	if($value["pay_status_id"]==4) continue;
	if($value["pay_status_id"]==3) continue;
	if($value["pay_status_id"]==2) continue;
	$str_option_no_cancel .= '<option value="'.$value["pay_status_id"].'" >'.$name.'</option>';
}
set_time_limit(600);
$single_info = $_POST["single"];
if($single_info=="T"){
	$aData = array();
	$id = $_POST["member_id"];
	if($id){
   $r = view_member("", $id);
    foreach ($r as $k => $v) {
    	$v["select_university"] = $v["grd_ugrp1"]."-".$v["grd_uid1"];
    	$v["birthdate"] = revert_date($v["birthdate"]);
    	$v["password2"] = $v["password"];     
	    $aData[] = $v;	   }  
	}
}else{


function fnColumnToField( $i ){
	/* Note that column 0 is the details column */
	if ( $i == 0 ||$i == 1 )
		return "a.register_id";
	else if ( $i == 2 )
		return "a.cid";
	else if ( $i == 3 )
		return "a.fname";
	else if ( $i == 4 )
		return "a.lname";
	else
		return "a.register_id";
}

$sLimit = "";
if (isset( $_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1' )
{
	$sLimit = "LIMIT ".(int)($_POST['iDisplayStart'] );
	$sLimit .= ", ".(int)( $_POST['iDisplayLength'] );
}


/* Ordering */
if(isset($_POST['iSortCol_0'])){
	$sOrder = "ORDER BY  ";
	for ( $i=0 ; $i<$db->escape( $_POST['iSortingCols'] ) ; $i++ ){
		$sOrder .= fnColumnToField($db->escape( $_POST['iSortCol_'.$i] ))."
                ".$db->escape( $_POST['sSortDir_'.$i] ) .", ";
	}
	$sOrder = substr_replace( $sOrder, "", -2 );
}
 
/* Filtering */
$sWhere = "";
$WHERE = "WHERE a.active!='' AND a.auto_del='F' ";
if($_POST['sSearch'] != ""){
   $sWhere = " and  (a.title LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "a.fname LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "a.lname LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "a.ref1 LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".			    
			    "a.ref2 LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".	
			    "a.register_id LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".	
			    "a.cid LIKE '%".$db->escape( $_POST['sSearch'] )."%' )";
	$sAND = "AND ";
	
}

$str_active = "";
if ($_POST["active"]) {
	$str_active = "a.active='{$_POST["active"]}'";
	//$active = 'T';
}
if ($_POST["status"]==8) {
	//$active = 'F';
	// $str_active = "(a.active='T' OR a.active='F') AND (a.docno<>'' OR a.docno IS NULL)";
	$str_active = "(a.active='T' OR a.active='F')";
}

switch ($_POST["sPay"]) {
	case 'at_ati': 
		$pay_type = " AND (a.pay='at_ati' OR a.pay='walkin' OR a.pay='importfile')";
		break;
	default:
		$pay_type = " AND a.pay='{$_POST["sPay"]}'";
		break;
}

// $sWhere .= " and a.active='$active'";
$sWhere .= " and {$str_active}";
$sWhere .= ($_POST["coursetype_id"]) ? " and a.coursetype_id={$_POST["coursetype_id"]}" : "";
$sWhere .= ($_POST["section_id"]) ? " and a.section_id={$_POST["section_id"]}" : "";
$sWhere .= ($_POST["status"]) ? " and a.pay_status={$_POST["status"]}" : "";
$sWhere .= ($_POST["sPay"]) ? "$pay_type" : "";
//$sWhere .= ($_POST["active"] && $_POST["pay_status"]==8) ? " and a.active='{$_POST["active"]}'" : "";
//$sWhere .= ($_POST["active"]=='T' && $_POST["pay_status"]==8) ? " and a.active='F' and a.pay_status={$_POST["status"]}" : "";
//$sWhere .= ($_POST["active"]=='T') ? " and a.active='T' " : "";
$sWhere .= ($_POST["member_id"]) ? " and a.member_id={$_POST["member_id"]}" : "";

$register_ids = array();
$flag = 0;
if($_POST["course_detail_id"]){
	
	$q = "select a.register_id from register a where a.active='T' and a.course_detail_id={$_POST["course_detail_id"]}";

	$cd = $db->get($q);
	if($cd){
		foreach ($cd as $key => $value) {
			$register_ids[$value["register_id"]] = $value["register_id"];
		}
	}

	$q = "select a.course_detail_id, a.register_id from register_course_detail a where a.active='T' and a.course_detail_id={$_POST["course_detail_id"]}";
	$cd = $db->get($q);	
	if($cd){
		foreach ($cd as $key => $value) {
			$register_ids[$value["register_id"]] = $value["register_id"];
		}
	}
	/*print_r($register_ids);*/
	if(count($register_ids)>0){
		$register_ids = implode(",", $register_ids);
		$sWhere .= " and a.register_id in ($register_ids)";
	}else{
		$flag = 1;
		$sWhere .= " and a.register_id in (0)";
	}
}
$dateStart = ($_POST["date_start"]) ? thai_to_timestamp($_POST["date_start"]) :  "";
$dateStop =  ($_POST["date_stop"]) ? thai_to_timestamp($_POST["date_stop"]) : "";
if ($dateStart || $dateStop) {
    if (!$dateStart && $dateStop)
        $dateStart = $dateStop;
    if (!$dateStop && $dateStart)
        $dateStop = $dateStart;
    $t = $dateStart;
    if ($dateStart > $dateStop) {
        $dateStart = $dateStop;
        $dateStop = $t;
    }
}
$sWhere .= ($dateStart && $dateStop) ? " and a.date>='$dateStart 00:00:00' and a.date<='$dateStop 23:59:59'" : "";
/* Paging */
$sQuery = "SELECT a.register_id,
			a.`no`,
			a.member_id,
			a.title,
			a.fname,
			a.lname,
			a.cid,
			a.slip_type,
			a.receipttype_id,
			a.receipttype_text,
			a.receipt_id,
			a.taxno,
			a.slip_name,
			a.slip_address1,
			a.slip_address2,
			a.date,
			a.register_by,
			a.ref1,
			a.ref2,
			a.`status`,
			a.expire_date,
			a.exam_date,
			a.course_id,
			a.course_detail_id,
			a.course_price,
			a.course_discount,
			a.pay,
			a.pay_status,
			a.pay_date,
			a.pay_method,
			a.pay_yr,
			a.pay_mn,
			a.pay_id,
			a.pay_price,
			a.pay_diff,
			a.approve,
			a.approve_by,
			a.approve_date,
			a.register_mod,
			a.last_mod,
			a.last_mod_date,
			a.result,
			a.result_date,
			a.result_by,
			a.receipt_title,
			a.receipt_fname,
			a.receipt_lname,
			a.receipt_no,
			a.receipt_gno,
			a.receipt_moo,
			a.receipt_soi,
			a.receipt_road,
			a.receipt_district_id,
			a.receipt_amphur_id,
			a.receipt_province_id,
			a.receipt_postcode,
			a.active,
			a.recby_id,
			a.rectime,
			a.remark,
			a.te_results,
			a.docno,
			a.coursetype_id,
            b.name AS receipt_district_name,
            c.name AS receipt_amphur_name,
            d.name AS receipt_province_name,
            e.name AS receipt_name,
            f.name AS result_name
		FROM register a 
		LEFT JOIN district b ON b.district_id=a.receipt_amphur_id
        LEFT JOIN amphur c ON c.amphur_id=a.receipt_amphur_id
        LEFT JOIN province d ON d.province_id=a.receipt_province_id        
        LEFT JOIN receipt e ON e.receipt_id=a.receipt_id
        LEFT JOIN register_result f ON f.register_result_id=a.result
		$WHERE $sWhere
		$sOrder
		$sLimit
";

//echo $sQuery; die();
$rResult = $db->get($sQuery);
// d($rResult);die();
$a = array();
if(is_array($rResult)){
	$runNo = 1;
	foreach ($rResult as $v){
			$register_id = $v["register_id"];
			$sCourse_id = $v["course_id"];
			$con = " and a.course_id in ($sCourse_id)";
			$r = get_course($con);
			$title = "";
			$parent_id = 0;
			$price = 0;
			if($r){
				foreach ($r as $key => $row) {
					$course_id = $row['course_id'];
					$section_id = $row['section_id'];
					$code = $row['code'];
					$title .= $row['title'].", ";
					$set_time = $row['set_time'];
					$life_time = $row['life_time'];
					$status = $row['status']; 
					$parent_id = (int)$row['parent_id'];
				}
			}
			$q = "SELECT 
					a.register_course_detail_id,
					a.register_id,
					a.course_id,
					a.course_detail_id,
					a.active,
					a.recby_id,
					a.rectime,
					a.remark,
					a.register_result_id,
					a.result_date,
					a.result_by,
					b.name AS result_name
				FROM register_course_detail a 
				LEFT JOIN register_result b ON b.register_result_id=a.register_result_id
				WHERE a.active='T'
					AND a.register_id={$register_id} AND a.course_id IN ($sCourse_id)
			";
			//echo $q."<hr>";
			$cd = $db->get($q);
			// d($cd);
			$parent_title = "";
			if($parent_id>0){
				$q = "select title from course where active='T' and course_id=$parent_id";
				$t = $db->data($q);
				$parent_title = "<strong>{$t}</strong><br>";	
			}

			$title = trim($title, ", ");
			$price = $v["course_price"];
			$discount = $v["course_discount"];
			$coursetype_id = $v["coursetype_id"];
			// echo $coursetype_id;die();
			$display_result_name = "";
			$display_te_result_name = "";

			if($cd){    
				// d($cd);die();
				$display_date = "";
				$display_time = "";
				$display_address = "";
				foreach ($cd as $kk => $vv) {
					// d($vv);
					$row = get_course_detail(" and a.course_detail_id={$vv["course_detail_id"]}");
					if($row) $row = $row[0];
					$course_detail_id = $row['course_detail_id'];
					$course_id = $row['course_id'];
					$day = $row['day'];
					$date = $row['date'];
					$time = $row['time'];
					$display_date .= $day.". ".revert_date($date).", ";
					$display_time .= $time.", ";
					$display_address .= $row['address_detail']." ".$row['address'].", ";

					$result_name = !empty($vv["result_name"]) ? $vv["result_name"] : "-";
					if ( $coursetype_id==1 || $coursetype_id==4 ) {
						$display_te_result_name .= $vv["result_name"]."<br>";		
						$display_result_name .= "<br>";
					}elseif ( $coursetype_id==2 ) {
						$display_result_name .= $vv["result_name"]."<br>";		
						$display_te_result_name .= "<br>";				
					}//end else if
					
				}//end loop $vv
				// echo $display_te_result_name;
			}else{
				$display_date = "";
				$display_time = "";
				$display_address = "";
				$row = get_course_detail(" and a.course_detail_id={$v["course_detail_id"]}");
				if($row) $row = $row[0];
				$course_detail_id = $row['course_detail_id'];
				$course_id = $row['course_id'];
				$day = $row['day'];
				$date = $row['date'];
				$time = $row['time'];
				$display_date .= $day.". ".revert_date($date).", ";
				$display_time .= $time.", ";
				$display_address .= $row['address_detail']." ".$row['address'].", ";

				if ( $coursetype_id==1 || $coursetype_id==4 ) {
					$display_te_result_name .= $v["result_name"]."<br>";		
					$display_result_name .= "<br>";
				}elseif ( $coursetype_id==2 ) {
					$display_result_name .= $v["result_name"]."<br>";		
					$display_te_result_name .= "<br>";				
				}//end else if
			}

			$display_date = trim($display_date, ", ");
			$display_time = trim($display_time, ", ");
			$display_address = trim($display_address, ", ");
			$id = $v["register_id"];
			$title = str_replace(", ", "<br><br>",trim($title, ", "));
			$display_date = str_replace(", ", "<br><br>",trim($display_date, ", "));
			$display_time = str_replace(", ", "<br><br>",trim($display_time, ", "));
			$display_address = str_replace(", ", "<br><br>",trim($display_address, ", "));
			
			$display_te_result_name = str_replace("<br>", "<br><br>",trim($display_te_result_name, ", "));
			$display_result_name = str_replace("<br>", "<br><br>",trim($display_result_name, ", "));

			$content = "";
			$pay_type = "";
			$print_style = "";

			$pay = $v["pay_id"];
			$status = $v["pay_status"];
			$ckBox = ($status==1 && $_POST["type"]!="register-history") ? '<input type="checkbox" id="ckBox" value="'.$id.'" style="float:right;cursor: pointer;">' : '';
			$status_pay = $arr_pay[$v["pay_status"]];
			$manage =  get_datatable_icon("edit", $id, false);
			
			$print = '<a class="btn btn-warning" onClick="printInfo(\''.$id.'\')"> <i class="fa fa-print"></i></a>';
			// $print_credit_note = '<a class="btn btn-default" onClick="print_credit_note(\''.$id.'\')" title="Credit note"> <i class="fa fa-print"></i></a>';
			if ( $v["pay_status"]==8 && !empty($v["docno"]) ) {
				$manage .= ' '.$print;
			}//end if
			if ( $v["pay_status"]==9 && !empty($v["docno"]) ) {
				// $manage .= $print_credit_note;
				$manage = "";
			}//end if


			if($v["pay"]=="importfile" || $v["pay"]=="walkin"){
			}else{
				$str_option = $str_option_no_cancel;
			}
			if($status==1 && $_POST["type"]!="register-history"){
				// $manage .= get_datatable_icon("save", $id, false); readonly disabled
				$select_option = '<select disabled name="pay_status_'.$id.'" id="pay_status'.$id.'" class="form-control">';
				$select_option .= $str_option;									
				$select_option .= '</select>';


			}
			if($_POST["type"]=="register-history"){
				// echo "id=".$v["cid"]."|display_te_result_name={$display_te_result_name}|display_result_name={$display_result_name}\r\n";
				$q = "SELECT a.*
						, b.name AS result_name
						, c.name AS te_result_name
					FROM register_course_detail a 
					LEFT JOIN register_result b ON b.register_result_id=a.register_result_id
					LEFT JOIN register_result c ON c.register_result_id=a.register_te_result_id
					WHERE a.active='T'
						AND a.register_id={$register_id} AND a.course_id IN ($sCourse_id)
				";
				$reg_cd = $db->rows($q);
				// d($cd); die();
				if ( !empty($reg_cd["result_name"]) || !empty($reg_cd["te_result_name"]) ) {
					$display_result_name = "";
					$display_te_result_name = "";
					if ( !empty($reg_cd["result_name"]) ) {
						$display_result_name = $reg_cd["result_name"];
					}//end if
					if ( !empty($reg_cd["te_result_name"]) ) {
						$display_te_result_name = $reg_cd["te_result_name"];
					}//end if

				}//end if
				// $register_course_detail_id = $cd[0]["register_course_detail_id"];
				$a[] = array($runNo
					,$v["register_id"]
					,revert_date($v["date"], true)
					,$v["cid"]
					,$v['title']." ".$v['fname']." ".$v['lname']
					,$display_date
					,$display_time
					,$display_address
					,$title
					,$status_pay
					// ,$register_course_detail_id
					,$display_te_result_name
					,$display_result_name
				);
				// d($a); die();
			}else{
				$a[] = array($runNo
					,$v["register_id"]
					,revert_date($v["date"], true)
					,revert_date($v['expire_date'])
					,$v["cid"]
					,$v['title']." ".$v['fname']." ".$v['lname']
					,$display_date
					,$display_time
					,$display_address
					,$title
					,($status==1) ? $select_option  : $status_pay
					,$manage);
			}

		$runNo++;
	}
}

$aData = array();
$sQuery = "SELECT COUNT(*) as total
			  FROM register a
			  $WHERE $sWhere";

$rs = $db->data($sQuery);
$iFilteredTotal = $rs;
 
$sQuery = "SELECT COUNT(*) as total
			  FROM register a";
$resultTotal = $db->data($sQuery);
$iTotal = $resultTotal;
						 
$aData["sEcho"] = intval($_POST['sEcho']);
$aData["iTotalRecords"] = $iTotal; 
$aData["iTotalDisplayRecords"] = $iFilteredTotal; 
$aData["aaData"] = $a; 

}

echo json_encode($aData);
?>
