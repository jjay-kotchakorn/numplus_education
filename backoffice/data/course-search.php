<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/course.php";
global $db;

$id = $_POST["id"];
$con = "";
if($_POST["othercon"]){
    $a = explode("-", $_POST["othercon"]);
    $con_id = trim($a[1],",");
    if($con_id){
       $con .= " and a.{$a[0]} in ($con_id)";
    }   
}
$con .= ($_POST["section_id"]) ? " and a.section_id={$_POST["section_id"]}" : "";
$con .= ($_POST["coursetype_id"]) ? " and a.coursetype_id={$_POST["coursetype_id"]}" : "";
$con .= ($_POST["active"]) ? " and a.active='{$_POST["active"]}'" : "";
$con .= ($_POST["childlist"]=="parent") ? " and (a.parent_id is null or a.parent_id <= 0)" : "";
$con .= ($_POST["childlist"]=="childlist") ? " and (a.parent_id is not null or a.parent_id > 0)" : "";
$search = $db->escape(trim($_POST["search"]));
if($search){
    $con .=  " and (a.code like '%$search%' or a.title like '%$search%')";
}
$array_data = array();
if($con){
    $r = get_course($con, "", false, 50);
	   foreach($r as $k=>$v){
	   	  $v["open_regis"] = revert_date($v["open_regis"]);
	   	  $v["end_regis"] = revert_date($v["end_regis"]);
	   	  $v["date"] = $v["day"]." ".revert_date($v["date"]);
	      $array_data[] = $v;
	   }      
}

echo json_encode($array_data); ?>

