<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/course.php";
include_once "../share/datatype.php";
include_once "pay-status-sendmail-func.php";
global $db;

$datetime_now = date('Y-m-d H:i:s');

$pay_status = datatype(" and a.active='T'", "pay_status", true);
$b = datatype(" and a.active='T'", "bank", true);
$bank = array();
foreach ($b as $key => $value) {
	$bank[$value["code"]] = $value["name"];
}

$apay = array();
foreach ($pay_status as $key => $value) {
	$apay[$value["pay_status_id"]] = $value["name"];
}
$q = "select a.name, a.url, a.bill_payment_upload_list_id, a.update_header, a.update_footer from bill_payment_upload_list a where a.bill_payment_upload_id=".$_POST["bill_payment_upload_id"];
$r = $db->get($q);
$update_log = $_POST["update_log"];
if($update_log=="T"){
	$db->query("update bill_payment_upload set lock_edit='T' where bill_payment_upload_id={$_POST["bill_payment_upload_id"]}");
}else{
}

$q = "select a.bill_payment_upload_id as c from bill_payment_upload a where a.bill_payment_upload_id < ".$_POST["bill_payment_upload_id"]." and a.lock_edit='T' order by a.bill_payment_upload_id desc ";
$last_bill_payment_upload_id = $db->data($q);
if($last_bill_payment_upload_id){	
	$q = "select a.effective_date from bill_payment_upload_list a where a.bill_payment_upload_id=".$last_bill_payment_upload_id;
	$last_day_import = $db->data($q);
}

$array_check_date = array();
$ckBank_code = array();
$enable = true;
if($r){ 
	$array_file = array();
	foreach ($r as $read_k => $read_v) {
		$file = "../uploads/".$read_v["name"];
		$text = file($file);

		$array_data = array();
		foreach($text as $k=>$v){
			set_time_limit(600);
			$line = substr($v, 0,1);
			if($line=="H"){
				$record_type = substr($v,0,1);
				$sequence_number = substr($v,1,6);
				$bank_code = substr($v,7,3);
				$company_account = substr($v,10,10);
				$company_name = substr($v,20,40);
				$effective_date = substr($v,60,8);
				$service_code = substr($v,68,8);
				$filler = substr($v,76,180);
				$array_data["bill_payment_upload_list_id"] = $read_v["bill_payment_upload_list_id"];
				$array_data["H"]  = array(
					'record_type' => $record_type,
					'sequence_number' => $sequence_number,
					'bank_code' => $bank_code,
					'company_account' => $company_account,
					'company_name' => $company_name,
					'effective_date' => $effective_date,
					'service_code' => $service_code,
					'filler' => $filler
					);
					if($last_day_import){						
						$p_dd = substr($last_day_import, 0,2);
						$p_mm = substr($last_day_import, 2,2);
						$p_yy = substr($last_day_import, 4,4);
						$p_dd = date("{$p_dd}"+1);
						$payment_date = date("{$p_yy}-{$p_mm}-{$p_dd}");
					}else{
						$payment_date = date("Y-m-07");
					}

					$p_dd = substr($effective_date, 0,2);
					$p_mm = substr($effective_date, 2,2);
					$p_yy = substr($effective_date, 4,4);
					$expire_date = date("{$p_yy}-{$p_mm}-{$p_dd}");
					$full_payment_date = $payment_date." 00:00:00";
					$start = convert_mktime($full_payment_date);
					$stop = convert_mktime($expire_date." 00:00:00");
					$time = $stop - $start;
					$status = ($time!=0) ? 3 : 2;
					if($status==3){
						$enable = false;
						//echo "import ไม่ได้";
					}else{
						//echo "import ได้";
					}
					if(count($array_check_date)>0){
						if(!in_array($effective_date, $array_check_date)){
							$enable  = false;
						}
					}
					$array_check_date[$read_v["name"]] = $effective_date;
					$ckBank_code[$bank_code] = $bank_code;
					if($read_v["update_header"]=="F"){						
						$args = array();
						$args = array(
						'record_type' => $record_type,
						'sequence_number' => $sequence_number,
						'bank_code' => $bank_code,
						'company_account' => $company_account,
						'company_name' => $company_name,
						'effective_date' => $effective_date,
						'service_code' => $service_code,
						'update_header' => "T",
						);	
						$args["table"] = "bill_payment_upload_list";
						$args["id"] = $read_v["bill_payment_upload_list_id"];
					    $db->set($args);
					}

			}else if($line=="D"){
				$record_type = substr($v,0,1);
				$sequence_number = substr($v,1,6);
				$bank_code = substr($v,7,3);
				$company_account = substr($v,10,10);
				$payment_date = substr($v,20,8);
				$payment_time = substr($v,28,6);
				$customer_name = substr($v,34,50);
				$customer_no_ref1 = substr($v,84,20);
				$ref2 = substr($v,104,20);
				$ref3 = substr($v,124,20);
				$branch_no = substr($v,144,4);
				$teller_no = substr($v,148,4);
				$transaction_type = substr($v,152,1);
				$transaction_code = substr($v,153,3);
				$cheque_number = substr($v,156,7);
				$amount = substr($v,163,13);
				$cheque_bank_code = substr($v,176,3);
				$payee_fee_same_zone = substr($v,179,8);
				$payee_fee_diff_zone = substr($v,187,8);
				$filler = substr($v,195,51);
				$new_cheque_no = substr($v,246,10);

				$array_data["D"][] = array(
					'record_type' => $record_type,
					'sequence_number' => $sequence_number,
					'bank_code' => $bank_code,
					'company_account' => $company_account,
					'payment_date' => $payment_date,
					'payment_time' => $payment_time,
					'customer_name' => $customer_name,
					'customer_no_ref1' => $customer_no_ref1,
					'ref2' => $ref2,
					'ref3' => $ref3,
					'branch_no' => $branch_no,
					'teller_no' => $teller_no,
					'transaction_type' => $transaction_type,
					'transaction_code' => $transaction_code,
					'cheque_number' => $cheque_number,
					'amount' => $amount,
					'cheque_bank_code' => $cheque_bank_code,
					'payee_fee_same_zone' => $payee_fee_same_zone,
					'payee_fee_diff_zone' => $payee_fee_diff_zone,
					'filler' => $filler,
					'new_cheque_no' => $new_cheque_no,
					);

			}else if($line=="T"){
				$record_type = substr($v,0,1);
				$sequence_number = substr($v,1,6);
				$bank_code = substr($v,7,3);
				$company_account_number = substr($v,10,10);
				$total_debit_amount = substr($v,20,13);
				$total_debit_transaction = substr($v,33,6);
				$total_credit_amount = substr($v,39,13);
				$total_credit_transaction = substr($v,52,6);
				$filler = substr($v,58,198);

				$array_data["T"] = array(
					'record_type' => $record_type,
					'sequence_number' => $sequence_number,
					'bank_code' => $bank_code,
					'company_account_number' => $company_account_number,
					'total_debit_amount' => $total_debit_amount,
					'total_debit_transaction' => $total_debit_transaction,
					'total_credit_amount' => $total_credit_amount,
					'total_credit_transaction' => $total_credit_transaction,
					'filler' => $filler,
					);
					if($read_v["update_footer"]=="F"){						
						$args = array();
						$args =  array(
							'record_type' => $record_type,
							'sequence_number' => $sequence_number,
							'bank_code' => $bank_code,
							'company_account_number' => $company_account_number,
							'total_debit_amount' => $total_debit_amount,
							'total_debit_transaction' => $total_debit_transaction,
							'total_credit_amount' => $total_credit_amount,
							'total_credit_transaction' => $total_credit_transaction,
							'update_footer' => "T"
							);	
						$args["table"] = "bill_payment_upload_list";
						$args["id"] = $read_v["bill_payment_upload_list_id"];
					    $db->set($args);
					}
			}
		}
		$array_file[$read_v["name"]] = $array_data;
		
	}

	if(count($ckBank_code)<2){
		$enable = false;
	}
	if(!$enable){
		?>
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<i class="fa fa-times-circle sign"></i><strong>Error!</strong> วันที่ ธนาคาร <?php echo revert_date($full_payment_date); ?>ไม่ตรงกัน หรือ อัพโหลด ไฟล์ ไม่ครบทั้ง 2 ธนาคาร หรือ วันที่ของไฟล์ Import ไม่เป็นวันที่ต่อเนื่องจากครั้งล่าสุด <?php echo revert_date($expire_date); ?>
		</div>
			
		<?php
		die();
	}
	$runing = 1;
	$array_register = array();
	$array_error = array();
	if(count($array_file)>0){
		//$db->begin();
		foreach ($array_file as $kline => $vline) {

			$bill_payment_upload_list_id = $vline["bill_payment_upload_list_id"];
			$details = $vline["D"];
			if(count($details)<=0) continue;
		?>
			<div class="clear"></div>
				<div class="form-group row">
					<div class="col-sm-12">
						<label class="control-label"><?php echo "[ ".$vline["H"]["bank_code"]." ] - ". $bank[$vline["H"]["bank_code"]]; ?></label><div class="clear"></div>
						<div class="table-responsive">
							<table>
								<thead>
									<tr>
										<th>No</th>
										<th>Sequence No</th>
										<th>Bank Code</th>
										<th>Company Account</th>
										<th>Payment Date</th>
										<th>Payment Time</th>
										<th>Customer</th>
										<th>Ref1</th>
										<th>Ref2</th>
										<th>Amount</th>
										<th>MAP ID</th>
										<th>Status In File</th>
									</tr>
								</thead>
								<tbody class="no-border-y">
								<?php
/*								print_r($details);
								die();	*/								
								$no = 1;
								$line = 1;								
								foreach ($details as $kk => $data) {
										$line++;
										set_time_limit(600);
										$ref1 = trim($data["customer_no_ref1"]);
										$ref2 = trim($data["ref2"]);
										//$q = "select register_id, expire_date from register where active='T' and pay='bill_payment' and ref1='{$ref1}' and ref2='{$ref2}'";
										$q = "select register_id, expire_date, pay_status, coursetype_id, section_id, course_price, course_discount  from register where active='T' and ref1='{$ref1}' and ref2='{$ref2}'";
										$a = $db->rows($q);
										$register_id = $a["register_id"];
										$section_id = (int)$a["section_id"];
										$coursetype_id = (int)$a["coursetype_id"];
										if(!$register_id) {
											$runing = 0;
											$array_error[$vline["H"]["bank_code"]][$no] = '<div class="alert alert-danger"><button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button><i class="fa fa-warning sign"></i><strong>บรรทัดที่ '.($line).'</strong> ไม่มีข้อมูลของ REF1 : '.$ref1.' และ REF2 : '.$ref2.'</div>';
											continue;
											//break;
										}else{
											$array_register[$register_id] = $register_id;
										}
										$n = (int)$data['amount'];
										$amount = substr($n,0, strlen($n)-2);
										$stang = substr($n,-2, 2);
										$price = $a["course_price"]-(int)$a["course_discount"];
										if($amount!=$price){
											$runing = 0;
											$array_error[$vline["H"]["bank_code"]][$no] = '<div class="alert alert-danger"><button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button><i class="fa fa-warning sign"></i><strong>บรรทัดที่ '.($line).'</strong> ชำระไม่ครบตามจำนวน '.set_comma($price).' โดยชำระมา '.set_comma($amount).'</div>';
											continue;
											//break;											
										}
										if($stang>0) $amount = $amount.".".$stang;
										$p_dd = substr($data['payment_date'], 0,2);
										$p_mm = substr($data['payment_date'], 2,2);
										$p_yy = substr($data['payment_date'], 4,4);
										$payment_date = date("{$p_yy}-{$p_mm}-{$p_dd}");

										$t_hh = substr($data['payment_time'], 0, 2);
										$t_mm = substr($data['payment_time'], 2, 2);
										$t_ss = substr($data['payment_time'], 4, 2);
										$payment_time = $t_hh.":".$t_mm;
										$exd = explode(" ", $a["expire_date"]);
										$expire_date = $exd[0];
										//$expire_date = "2015-01-07";
										$full_payment_date = $payment_date." ".$payment_time.":".$t_ss;
										$start = convert_mktime($full_payment_date);
										$stop = convert_mktime($expire_date." 23:59:59");
										$time = $stop - $start;
										$status = ($time>0) ? 3 : 2;
										$args = array();
										if($update_log=="T" && $register_id && ($a["pay_status"]==1 || $a["pay_status"]==4)){
											$args["table"] = "register";
											$args["id"] = $register_id;
											$args["pay_method"] = $data['bank_code'];
											$args["pay_price"] = $amount; // จำนวนเงินที่ชำระ
											$args["pay_yr"] = $p_yy; // ค่าธรรมเนียม;
											$args["pay_mn"] = $p_mm; // ค่าธรรมเนียม;
											$args["pay_id"] = $data['sequence_number']; // 
											$args["pay_date"] = $full_payment_date; // 
											$args["pay_status"] = $status;
											if($a["pay_status"]==1){												
												$t = rundocno($coursetype_id, $section_id);
												$args = array_merge($args, $t); 
											}
											$db->set($args); 
										}
										if($register_id){
											// echo "888888888888888";die();
											$recode_data = array();
											$recode_data = $data;
											$recode_data["register_id"] = $register_id;
											$recode_data["bill_payment_upload_list_id"] = $bill_payment_upload_list_id;
											$recode_data["bill_payment_upload_id"] = $_POST["bill_payment_upload_id"];
											$tb_ref_id = update_bill_payment_upload_list_details($recode_data);
											update_payment_compare_all_list($recode_data, "", $tb_ref_id);
											// d($rs);die();

										}
									?>
									<tr class="tr-data">
										<td><?php echo $no; ?></td>
										<td><?php echo $data['sequence_number'] ?></td>
										<td><?php echo $data['bank_code'] ?></td>
										<td><?php echo $data['company_account'] ?></td>
										<td><?php echo revert_date($payment_date); ?></td>
										<td><?php echo $payment_time ?></td>
										<td><?php echo iconv("tis-620", "utf-8", $data['customer_name']) ; ?></td>
										<td><?php echo $data['customer_no_ref1'] ?></td>
										<td><?php echo $data['ref2'] ?></td>
										<td><?php echo set_comma($amount); ?></td>
										<td><?php echo $register_id;?>	<a href="#" onclick="registerInfo(<?php echo $register_id; ?>)">Detail</a></td>
										<td><?php echo $apay[$status]; ?></td>
										
									</tr>
								<?php
									$no++;
								 }	
								 ?>
								</tbody>
							</table>

						</div>
					</div>
				</div>
		<?php 
		}
			if($runing==0){
				//$db->rollback();
			}else{
				if(count($array_register)>0 && $update_log=="T"){
					pay_status_sendmail_func($array_register);		

					$register_ids = "";
					foreach ($array_register as $key => $v) {
						$register_ids .= $v.",";
					}//end loop $v
					$register_ids = trim($register_ids, ",");

					$args = array();
					$args["table"] = "cronjob_qman_list";
					$args["id"] = 1;
					$args["rectime"] = $datetime_now;
					$args["remark"] = $register_ids;
					$db->set($args);
					// var_dump( $db->set($args, true, true) );

				}//end if
			}//end else
			if(count($array_error)>0){
				?>
				<hr>
				<div class="form-group row">
					<div class="col-sm-12">				
					<?php
					foreach ($array_error as $key => $value) {
						?>
						<label class="control-label"><?php echo "[ ".$key." ] - ". $bank[$key]; ?></label><div class="clear"></div>
						<?php
						foreach ($value as $k => $v) {
							echo $v;
						}
					}
					?>
					</div>
				</div>
					<style>
						#update_bill_payment_map{
							display: none !important;
						}
					</style>
				<?php
			}
			if($update_log=="T"){
		?>
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<i class="fa fa-check sign"></i><strong>Success!</strong> อัพเดทข้อมูลเรียบร้อยแล้ว!
			</div>
			<style>
				#update_bill_payment_map{
					display: none !important;
				}
			</style>
		<?php
			}			
			//$db->commit();

		
	} 
}
?>
