<?php
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";

/*
$_SESSION["course_promotion_detail"]["alert"]["status"] = 'success';
$_SESSION["course_promotion_detail"]["alert"]["msg"] = "บันทึกรายการสำเร็จ";
*/

if ( !empty($_GET) ) {
	$session_name = trim($_GET["session_name"]);	
	$session_type = trim($_GET["session_type"]);	
	$destroy = $_GET["destroy"];	
	// $session_status = $_GET["session_status"];	
	// $session_msg = $_GET["session_msg"];

	if ( $session_type=="alert" ) {
		if ( !empty($_SESSION[$session_name]) ) {
			$status = $_SESSION[$session_name][$session_type]["status"];
			$msg = $_SESSION[$session_name][$session_type]["msg"];
			if ( $destroy ) {
				$_SESSION[$session_name]=NULL;
				unset($_SESSION[$session_name]);
			}//end if
			echo json_encode(array('status' => $status,'msg'=> $msg));
		}else{
			$msg = "ไม่พบ SESSION ที่ต้องการ";
			echo json_encode(array('status' => 'empty','msg'=> $msg));
		}//ens else
	}else if ( $session_type=="get_data" ) {
		if ( !empty($_SESSION[$session_name]) ) {
			$status = $_SESSION[$session_name][$session_type]["status"];
			$data = $_SESSION[$session_name][$session_type]["data"];
			if ( $destroy ) {
				$_SESSION[$session_name]=NULL;
				unset($_SESSION[$session_name]);
			}//end if
			echo json_encode(array('status' => $status,'data'=> $data));
		}else{
			$data = "";
			echo json_encode(array('status' => 'empty','data'=> $data));
		}//ens else
	}//end else if


}//end if
die();

?>