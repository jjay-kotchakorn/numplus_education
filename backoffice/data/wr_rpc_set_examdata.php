<?php
include_once "../share/authen.php";
include_once "../connection/connection.php";
include_once "../lib/lib.php";
global $db;

/*var_dump($_POST['es_exam_data']);
die();*/

function call_day($day, $month, $year_en){
	$first = 1; 
	$last = cal_days_in_month(CAL_GREGORIAN, date($month), date($year_en));
    $cur = strtotime(date($month) . "/" . $day . "/" . date($year_en));	 
    $cur_day = date("l", $cur);
    switch ($cur_day) {
    	case 'Sunday':
    		$day_th = 'อา';
    		break;
    	case 'Monday':
    		$day_th = 'จ';
    		break;
    	case 'Tuesday':
    		$day_th = 'อ';
    		break;
		case 'Wednesday':
    		$day_th = 'พ';
    		break;
		case 'Thursday':
    		$day_th = 'พฤ';
    		break;
    	case 'Friday':
    		$day_th = 'ศ';
    		break;
    	case 'Saturday':
    		$day_th = 'ส';
    		break;
    	
    	default:
    		# code...
    		break;
    }
	//var_dump($target_day);
	return $day_th;
}

$thai_day = array(1=>"จ",2=>"อ",3=>"พ",4=>"พฤ",5=>"ศ",6=>"ส",7=>"อา");

function return_time_format($number){
	//$number = "09:30:00";
	$txt = str_replace(":", ".", $number);
	return substr($txt, 0, 5);
}
/*$_POST['es_exam_data'] = "
1-2558-10-1-1<brk2><brk2>0<brk2>25<brk2>10<brk2>ห้องคอมพิวเตอร์ ชั้น 3 อาคารตลาดหลักทรัพย์ฯ<brk2>180<brk2>12:00:00<brk2>14:30:00<brk2>3<brk2>1<brk2>2015-07-13 00:01:00<brk2>2015-09-27 23:59:00<brk1>
1-2558-10-1-2<brk2><brk2>0<brk2>25<brk2>10<brk2>ห้องคอมพิวเตอร์ ชั้น 3 อาคารตลาดหลักทรัพย์ฯ<brk2>180<brk2>15:00:00<brk2>17:30:00<brk2>3<brk2>1<brk2>2015-07-19 08:00:00<brk2>2015-09-27 23:59:00<brk1>";
*/

if(isset($_POST['es_exam_data'])){ 
	$es_exam_data = $_POST['es_exam_data']; 
	//echo "if";
}else{ 
	$es_exam_data ="";
	//echo "else"; 
} 

if($es_exam_data != "") { 
	$return_string = 2000;
	$exam = explode("<brk1>",$es_exam_data); 
	$n_exam = sizeof($exam);
	if($n_exam<=0){
		$return_string = 1000;
	}

	//write log
	//unlink("wr_exam_data.log");
	$count_updt = 0;
	for ($i=0; $i < $n_exam; $i++) { 
		$exam_data = explode("<brk2>",$exam[$i]);
		$exid = $exam_data[0]; 		
		if(!$exid || $exid=="") {
			continue;
		}	
		
		$cos_id = substr($exid, 8, 11);//

		$title = $exam_data[1]; 
		$examtype = $exam_data[2]; 
		$reg_max = $exam_data[3]; 
		$pv = (int)$exam_data[4]; 
		$place = $exam_data[5]; 
		$set_time = $exam_data[6]; 
		$time1 = $exam_data[7]; 
		$time2 = $exam_data[8]; 
		$status = $exam_data[9]; 
		$inhouse = $exam_data[10]; 
		$start_reg = $exam_data[11]; 
		$expire_reg = $exam_data[12];
		
		$date = substr($exid, 1, 8); //25580507
		$year = substr($date, 0, 4);
		$year_en = substr($date, 0, 4)-543;
		$month = substr($date, 4, 6);
		$month = substr($month, 0, 2);
		$day = substr($date, 6, 8);
		$date = $year."-".$month."-".$day;
		$date = thai_to_timestamp($date);
		$t = convert_mktime($date." 00:00:00");
		/*$date_n = date("N", $t);
		$date_n = $date_n;*/
		$date_n = call_day($day, $month, $year_en);
		$date_en = $year_en."-".$month."-".$day;

		$q = "SELECT course_detail_id FROM course_detail WHERE ref_exam_id LIKE '%$exid' AND ref_exam_id is not null";
		$id = $db->data($q);
		// $q = "SELECT name FROM province WHERE code=$pv";
		// $address = $db->data($q);
		$address = $title;
		$active = 'T';

		//switch inhouse
		switch ($inhouse) {
			case '1':
				$inhouse = 'F';
				break;
			case '2':
				$inhouse = 'T';
				break;
			default:
				# code...
				break;
		}

		//switch status
		switch ($status) {
			case '0':
				$status = "ปิดรับสมัครชั่วคราว";
				break;
			case '1':
				$status = "เปิดรับสมัคร";
				break;
			case '2':
				$status = "รอบสอบเต็ม";
				break;
			case '3':
				$status = "ยกเลิกรอบสอบ";
				$active = 'F';
				break;
			default:
				$status = "ยกเลิกรอบสอบ";
				$active = 'F';
				break;
		}

		$cur_date = date("Y-m-d H:i:s");
		if ( ($cur_date < $start_reg)&&($status == "เปิดรับสมัคร") ) {
			$status = "ปิดรับสมัครชั่วคราว";
		}

		//echo $i."=".$status."\n\r";

		$args = array();
		$args["table"] = "course_detail";
		if($id){
			$mode = 'update';//
			$args["course_id"] = (int)$id;			
		}else{
			$mode = 'add';//
			$args["ref_exam_id"] = $exid;			
		}
		$args["datetime_add"] = $date_en;
		$args["date"] = $year_en."-".$month."-".$day;
		$args["time"] = return_time_format($time1)."-".return_time_format($time2);

		$time = explode("-", $args["time"]);
		$time_phase1 = trim($time[0]);
		$time_phase2 = trim($time[1]);
		$date_phase1 = "{$args["date"]} {$time_phase1}:00";
		$date_phase2 = "{$args["date"]} {$time_phase2}:00";
		$args["date_phase1"] = $date_phase1;
		$args["date_phase2"] = $date_phase2;

		$args["code_project"] = "";
		//$args["day"] = $thai_day[$date_n];
		$args["day"] = $date_n;
		$args["address"] = trim($address);
		$args["coursetype_id"] = 2;
		$args["section_id"] = 1;
		//$args["address_detail"] = $place;
		$args["address"] = iconv('TIS-620', 'UTF-8', $address);
		$args["address_detail"] = iconv('TIS-620', 'UTF-8', $place);
		$args["chair_all"] = (int)$reg_max;
		$args["open_regis"] = $start_reg;
		$args["end_regis"] = $expire_reg;
		//$args["status"] = "เปิดรับสมัคร";
		$args["status"] = $status;
		$args["date_other"] = $_POST["date_other"];
		$args["active"] = $active;
		$args["inhouse"] = $inhouse;
		$args["recby_id"] = (int)$EMPID;
		$args["rectime"] = date("Y-m-d H:i:s");
		$args["datetime_update"] = date("Y-m-d H:i:s");

		/*echo "<pre>";
		print_r($args);
		echo "</pre>";
		die();*/

		//$ret = $db->set($args);

		$date_col = 'date';
		switch ($mode) {
			case 'update':
				$sql = "UPDATE course_detail SET $date_col='".$args["date"]."', time='".$args["time"]."',";
				$sql .= " day='".$args["day"]."', coursetype_id=".$args["coursetype_id"].",";
				$sql .= " section_id=".$args["section_id"].",";
				$sql .= " address='".$args["address"]."', address_detail='".$args["address_detail"]."',";
				$sql .= " chair_all=".$args["chair_all"].", open_regis='".$args["open_regis"]."', end_regis='".$args["end_regis"]."',";
				$sql .= " status='".$args["status"]."', date_other='".$args["date_other"]."', active='".$args["active"]."',";
				$sql .= " recby_id=".$args["recby_id"].", datetime_update='".$args["datetime_update"]."',";
				$sql .= " datetime_add='".$args["datetime_add"]."', inhouse='".$args["inhouse"]."', active='".$args["active"]."',";
				$sql .= " date_phase1='".$args["date_phase1"]."', date_phase2='".$args["date_phase2"]."'";
				$sql .= " WHERE course_detail_id = ".$args["course_id"];
				break;

			case 'add':
				$sql = "INSERT INTO course_detail (ref_exam_id
							, datetime_add
							, $date_col
							, time
							, day
							, coursetype_id
							, section_id
							, address
							, address_detail
							, chair_all
							, open_regis
							, end_regis
							, status
							, date_other
							, active
							, recby_id
							, rectime
							, datetime_update
							, inhouse
							, date_phase1
							, date_phase2
							) 
						VALUES ('".$args["ref_exam_id"]."'
							, '".$args["datetime_add"]."'
							, '".$args["date"]."'
							, '".$args["time"]."'
							, '".$args["day"]."'
							, ".$args["coursetype_id"]."
							, ".$args["section_id"]."
							, '".$args["address"]."'
							, '".$args["address_detail"]."'
							, ".$args["chair_all"]."
							, '".$args["open_regis"]."'
							, '".$args["end_regis"]."'
							, '".$args["status"]."'
							, '".$args["date_other"]."'
							, '".$args["active"]."'
							, ".$args["recby_id"]."
							, '".$args["rectime"]."'
							, '".$args["datetime_update"]."'
							, '".$args["inhouse"]."'
							, '".$args["date_phase1"]."'
							, '".$args["date_phase2"]."'
						)";
				break;

			default:
				# code...
				break;
		}

		$ret = $db->set_update($sql);

		$qry_stat = 0;
		if ($ret) {
			$count_updt++;
			$qry_stat = 1;
		}

		/*$course_detail_id = $args["course_id"] ? $args["course_id"] : $ret;
		if(!$course_detail_id) {
			$return_string = 1111;         
		}*/

		//write log file --> /backoffice/data/log/log_wr_exam_data.log
		$str_txt = $mode." rs=".$qry_stat."|";
		$str_txt .= $args["course_id"]."|".$args["ref_exam_id"]."|".$args["date"]."|".$args["time"]."|";
		$str_txt .= $args["day"]."|";
		$str_txt .= $args["coursetype_id"]."".$args["section_id"]."|".$args["address_detail"]."|".$args["chair_all"]."|";
		$str_txt .= $args["open_regis"]."|".$args["end_regis"]."|".$args["status"]."|".$args["date_other"]."|";
		$str_txt .= $args["active"]."|".$args["recby_id"]."|".$args["inhouse"]."|".$args["address"];
		$str_txt .= "\r\n";
		//$str_txt .= $sql."\r\n";

		$log_name = "./log/wr_exam_data_".$year_en."-".$month.".log";
		$file = fopen($log_name, 'a');
		$date_now = date('Y-m-d H:i:s');
		$str_txt = $date_now."|".$str_txt;
		fwrite($file, $str_txt);
		fclose($file);

		/*echo "<pre>";
		print_r($rs_sql);
		echo "</pre>";*/

		//var_dump($rs_sql);
		/*var_dump($ret);
		die();*/		
	}

	if ($count_updt == $n_exam) { //set_update --> success all reccord
		$return_string = 2000;
	}
	if (($count_updt == 0) && ($n_exam == 0)) { //set_update --> no data input
		$return_string = 1000;
	}
	if (($count_updt == 0) && ($n_exam != 0)) { //set_update --> 0 reccord
		$return_string = 3000;
	}
	if (($count_updt != $n_exam)&&($count_updt != 0)) {
		$return_string = 1999; //set_update --> success some reccord
		//$return_string = 2000;
		//$return_string = "$count_updt : $n_exam";
	}

	/*echo "<pre>";
	print_r($rs_sql);
	echo "</pre>";*/
	//var_dump($rs_sql);
	//die();

} 

//var_dump($return_string);
print $return_string;
?> 