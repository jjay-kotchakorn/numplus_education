<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
global $db;

$bill_payment_upload_id = $_POST["bill_payment_upload_id"];
$q = "select lock_edit from bill_payment_upload where bill_payment_upload_id=$bill_payment_upload_id";
$lock_edit = $db->data($q);

$q = "select a.name, a.url, a.bill_payment_upload_list_id from bill_payment_upload_list a where a.bill_payment_upload_id=".$_POST["bill_payment_upload_id"];
$r = $db->get($q);
if($r){ 
	$runing = 1;
	?>
	<div class="table-responsive">
		<table class="table no-border hover" >
			<thead class="no-border">
				<tr>
					<th style="width:10%;">ลำดับ</th>
					<th style="width:80%;"><strong>ชื่อไฟล์</strong></th>
					<th style="width:10%;" class="text-center"><strong>จัดการ</strong></th>
				</tr>
			</thead>
			<tbody class="no-border-y" id="tb_file_map_list" >
			<?php foreach ($r as $k => $v) { ?>
			<tr>
				<td ><?php echo $runing; ?></td>
				<td ><a target="_black" href="<?php echo $v["url"].$v["name"]; ?>"><strong><?php echo $v["name"]; ?></strong></a> </td>
				<td class="text-center" <?php echo ($lock_edit=="F") ? '' : 'style="visibility:hidden;"'; ?>><a  style="cursor:pointer;"class="label label-danger"  onclick="delete_file(<?php echo $v["bill_payment_upload_list_id"]; ?>)"><i class="fa fa-times"></i></a></td>
			</tr>				

			<?php
				$runing++;
			 }	?>
			</tbody>
		</table>		
	</div>
<?php } ?>
