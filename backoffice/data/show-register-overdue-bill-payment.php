<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
global $db;
$bill_payment_upload_id = (int) $_POST["bill_payment_upload_id"];
if($bill_payment_upload_id>0) $con = " a.bill_payment_upload_id in ($bill_payment_upload_id)";
$q = "select register_id, expire_date, pay_status, cid, title, fname, lname, date from register where active='T' and pay_status=1 and pay='bill_payment'";
$r = $db->get($q);
if($r){
?>
			<div class="clear"></div>
				<div class="form-group row">
					<div class="col-sm-12">
						<label class="control-label">รายชื่อเตะที่นั่ง</label><div class="clear"></div>
						<table id="tbCourse" class="table" style="width:100%">
							  <thead>
								  <tr>
									  <th style="text-align:center" width="4%">ลำดับ</th>
									  <th style="text-align:center" width="4%">ID</th>
									  <th style="text-align:center" width="10%">วันที่สมัคร</th>
									  <th style="text-align:center" width="10%">วันที่หมดเขต</th>
									  <th width="9%">รหัสประจำตัวประชาชน</th>
									  <th width="10%">ชื่อ-นามสกุล</th>
								  </tr>
							  </thead>   
							<tbody>
								<?php


								$q = "select a.effective_date from bill_payment_upload_list a where  $con order by bill_payment_upload_list_id desc";
								
								$last_day_import = $db->data($q);
								if($last_day_import){						
									$p_dd = substr($last_day_import, 0,2);
									$p_mm = substr($last_day_import, 2,2);
									$p_yy = substr($last_day_import, 4,4);
									$last_date = date("{$p_yy}-{$p_mm}-{$p_dd}");
									$start = convert_mktime($last_date." 23:59:59");
								}
								$i = 0;
								foreach ($r as $key => $data) {
									$register_id = $data["register_id"];	
									$exd = explode(" ", $data["expire_date"]);
									$expire_date = $exd[0];
									$stop = convert_mktime($expire_date." 23:59:59");
									$time = $stop - $start;
									$status = ($time>0) ? "T" : "F";
									if($status=="F"){
										?>
											<tr>
												<td><?php echo $i; ?></td>
												<td><?php echo $data["register_id"]; ?></td>
												<td><?php echo revert_date($data["date"]); ?></td>
												<td><?php echo revert_date($data["expire_date"]); ?></td>
												<td><?php echo $data["cid"]; ?></td>
												<td><?php echo $data["title"]; ?><?php echo $data["fname"]; ?> <?php echo $data["lname"]; ?></td>
											</tr>

										<?php
										$i++;
									}
/*									echo date("Y-m-d H:i:s", $start)." - ".date("Y-m-d H:i:s", $stop)." =  ".$status;
									echo "<hr>";*/
								}
								?>
							</tbody>
						</table>


		</div>
	</div>
<?php
}else{
	?>
		<div class="alert alert-warning">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<i class="fa fa-times-circle sign"></i><strong>warning!</strong> ไม่มีข้อมูลการเตะที่นั่งในรอบนี้
		</div>
			
		<?php
		die();	
}
?>