<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/datatype.php";
include_once "../share/course.php";
global $db;

// d($_POST); die();
/*
[section_id] => 1
[coursetype_id] => 2
[course_detail_id] => 8450
[course_id] => 40
[status] => 3,5,6
[active] => T
[type] => register-result
*/

$course_detail_id = $_POST["course_detail_id"];
$q = "SELECT date 
	FROM course_detail 
	WHERE course_detail_id={$course_detail_id}
		AND date>'2017-12-31'
";
// echo "$q"; die();
$course_detail_date = $db->data($q);
// echo $course_detail_date;
// die();
if ( !empty($course_detail_date) ) {
	include_once "report-registerlist-new.php";
	die();
}//end if


$apay = datatype(" and a.active='T'", "pay_status", true);
$arr_pay = array();
foreach ($apay as $key => $value) {
	$arr_pay[$value["pay_status_id"]] = $value["name"];
}
set_time_limit(600);
$single_info = $_POST["single"];
if($single_info=="T"){
	$aData = array();
	$id = $_POST["member_id"];
	if($id){
   $r = view_member("", $id);
    foreach ($r as $k => $v) {
    	$v["select_university"] = $v["grd_ugrp1"]."-".$v["grd_uid1"];
    	$v["birthdate"] = revert_date($v["birthdate"]);
    	$v["password2"] = $v["password"];     
	    $aData[] = $v;	   }  
	}
}else{

function fnColumnToField( $i ){
	/* Note that column 0 is the details column */
	if($_POST["type"]=="register-info"){

		if ( $i == 0 ||$i == 1 )
			return "a.register_id";
		else if ( $i == 2 )
			return "a.fname";
		else if ( $i == 4 )
			return "a.date";
		else if ( $i == 5 )
			return "a.expire_date";
		else if ( $i == 5 )
			return "a.pay_date";
		else
			return "a.register_id";
	}else{		
		if ( $i == 0 ||$i == 1 )
			return "a.register_id";
		else if ( $i == 2 )
			return "a.cid";
		else if ( $i == 3 )
			return "a.fname";
		else
			return "a.register_id";
	}
}

$sLimit = "";
if (isset( $_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1' )
{
	$sLimit = "LIMIT ".(int)($_POST['iDisplayStart'] );
	$sLimit .= ", ".(int)( $_POST['iDisplayLength'] );
}


/* Ordering */
if(isset($_POST['iSortCol_0'])){
	$sOrder = "ORDER BY  ";
	for ( $i=0 ; $i<$db->escape( $_POST['iSortingCols'] ) ; $i++ ){
		$sOrder .= fnColumnToField($db->escape( $_POST['iSortCol_'.$i] ))."
                ".$db->escape( $_POST['sSortDir_'.$i] ) .", ";
	}
	$sOrder = substr_replace( $sOrder, "", -2 );
}
 
/* Filtering */
$sWhere = "";
$WHERE = "WHERE a.active!='' and a.pay_status!=8 AND a.auto_del='F' ";
if($_POST['sSearch'] != ""){
   $sWhere = " and  (a.title LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "a.fname LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "a.lname LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "a.ref1 LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".			    
			    "a.ref2 LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".					    
			    "a.cid LIKE '%".$db->escape( $_POST['sSearch'] )."%' )";
	$sAND = "AND ";
	
}
$sWhere .= ($_POST["section_id"]) ? " and a.section_id={$_POST["section_id"]}" : "";
$sWhere .= ($_POST["member_id"]) ? " and a.member_id={$_POST["member_id"]}" : "";
$sWhere .= ($_POST["coursetype_id"]) ? " and a.coursetype_id={$_POST["coursetype_id"]}" : "";
$sWhere .= ($_POST["status"]) ? " and a.pay_status in ({$_POST["status"]})" : "";
$sWhere .= ($_POST["active"]) ? " and a.active='{$_POST["active"]}'" : " and a.active='T'";
$sWhere .= ($_POST["pub"]=="T") ? " and a.pay not in ('importfile', 'walkin')" : "";
$sWhere .= ($_POST["pub"]=="F") ? " and a.pay in ('importfile', 'walkin')" : "";
$register_ids = array();
$flag = 0;
if($_POST["course_detail_id"]){
	
	$q = "select a.register_id from register a where a.active='T' and a.course_detail_id={$_POST["course_detail_id"]}";

	$cd = $db->get($q);
	if($cd){
		foreach ($cd as $key => $value) {
			$register_ids[$value["register_id"]] = $value["register_id"];
		}
	}

	$q = "select a.course_detail_id, a.register_id from register_course_detail a where a.active='T' and a.course_detail_id={$_POST["course_detail_id"]}";
	$cd = $db->get($q);	
	if($cd){
		foreach ($cd as $key => $value) {
			$register_ids[$value["register_id"]] = $value["register_id"];
		}
	}
	/*print_r($register_ids);*/
	if(count($register_ids)>0){
		$register_ids = implode(",", $register_ids);
		$sWhere .= " and a.register_id in ($register_ids)";
	}else{
		$flag = 1;
		$sWhere .= " and a.register_id in (0)";
	}
}
$dateStart = ($_POST["date_start"]) ? thai_to_timestamp($_POST["date_start"]) :  "";
$dateStop =  ($_POST["date_stop"]) ? thai_to_timestamp($_POST["date_stop"]) : "";
if ($dateStart || $dateStop) {
    if (!$dateStart && $dateStop)
        $dateStart = $dateStop;
    if (!$dateStop && $dateStart)
        $dateStop = $dateStart;
    $t = $dateStart;
    if ($dateStart > $dateStop) {
        $dateStart = $dateStop;
        $dateStop = $t;
    }
}
$sWhere .= ($dateStart && $dateStop) ? " and a.date>='$dateStart' and a.date<='$dateStop'" : "";

$orderBy = "";
if ( !empty($_POST["sort_by"]) ) {
	switch ($_POST["sort_by"]) {
		case 'cos_id':
			$orderBy = "order by a.course_id, convert(a.fname using tis620) asc";
			break;
		case 'pay_date':
			$orderBy = "order by a.pay_date desc";
			break;
		case 'status':
			$orderBy = "order by a.pay_status desc";
			break;
		case 'no':
			$orderBy = "order by a.no asc";
			break;		
		case 'reg_date':
			$orderBy = "order by a.date desc";
			break;
	}
}elseif ( empty($_POST["sort_by"]) ) {
	$orderBy = "order by a.course_id, convert(a.fname using tis620) asc";
}

/* Paging */
$sQuery = "SELECT a.register_id,
			a.`no`,
			a.runno,
			a.docno,
			a.section_id,
			a.coursetype_id,
			a.member_id,
			a.title,
			a.fname,
			a.lname,
			a.cid,
			a.slip_type,
			a.receipttype_id,
			a.receipttype_text,
			a.receipt_id,
			a.taxno,
			a.slip_name,
			a.slip_address1,
			a.slip_address2,
			a.date,
			a.register_by,
			a.ref1,
			a.ref2,
			a.`status`,
			a.expire_date,
			a.exam_date,
			a.course_id,
			a.course_detail_id,
			a.course_price,
			a.course_discount,
			a.pay,
			a.pay_status,
			a.pay_date,
			a.pay_method,
			a.pay_yr,
			a.pay_mn,
			a.pay_id,
			a.pay_price,
			a.pay_diff,
			a.approve,
			a.approve_by,
			a.approve_date,
			a.register_mod,
			a.last_mod,
			a.last_mod_date,
			a.result,
			a.te_results,
			a.result_date,
			a.result_by,
			a.receipt_title,
			a.receipt_fname,
			a.receipt_lname,
			a.receipt_no,
			a.receipt_gno,
			a.receipt_moo,
			a.receipt_soi,
			a.receipt_road,
			a.receipt_district_id,
			a.receipt_amphur_id,
			a.receipt_province_id,
			a.receipt_postcode,
			a.active,
			a.recby_id,
			a.rectime,
			a.remark,
            b.name AS receipt_district_name,
            c.name AS receipt_amphur_name,
            d.name AS receipt_province_name,
            e.name AS receipt_name,
            f.name AS result_name,
            g.name AS te_results_name
	FROM register a LEFT JOIN district b ON b.district_id=a.receipt_amphur_id
        LEFT JOIN amphur c ON c.amphur_id=a.receipt_amphur_id
        LEFT JOIN province d ON d.province_id=a.receipt_province_id        
        LEFT JOIN receipt e ON e.receipt_id=a.receipt_id
        LEFT JOIN register_result f ON f.register_result_id=a.result
        LEFT JOIN register_result g ON g.register_result_id=a.te_results
		$WHERE $sWhere
		$orderBy
		$sLimit";
		//$sOrder
		//order by a.course_id, a.fname asc
		//ORDER BY CONVERT (rName USING tis620)  ASC;
$rResult = $db->get($sQuery);


	//write log
	//$str = $sQuery;
	//$str = $sWhere;	
	/*$str = $_POST["sort_by"];
	$date_now = date('Y-m-d H:i:s');
	$date_log = date('Y-m-d');
	$log_name = "./log/report_registerlist_".$date_log.".log";
	$file = fopen($log_name, 'a');
	$str_txt = $date_now."|".$str."\r\n";
	fwrite($file, $str_txt);
	fclose($file);*/


$a = array();
if(is_array($rResult)){
	$runNo = 1;
	foreach ($rResult as $v){
			$register_id = $v["register_id"];
			$sCourse_id = $v["course_id"];
			$con = " and a.course_id in ($sCourse_id)";
			$r = get_course($con);
			$title = "";
			$parent_id = 0;
			$price = 0;
			if($r){
				foreach ($r as $key => $row) {
					$course_id = $row['course_id'];
					$section_id = $row['section_id'];
					$code = $row['code'];
					$code_project = $row["code_project"];
					$title .= $row['title'].", ";
					$set_time = $row['set_time'];
					$life_time = $row['life_time'];
					$status = $row['status']; 
					$parent_id = (int)$row['parent_id'];
				}
			}
			$q = "select a.course_detail_id from register_course_detail a where a.register_id={$register_id} and a.course_id in ($sCourse_id)";
			$cd = $db->get($q);
			$parent_title = "";
			if($parent_id>0){
				$q = "select title from course where active='T' and course_id=$parent_id";
				$t = $db->data($q);
				$parent_title = "<strong>{$t}</strong><br>";	
			}

			$title = trim($title, ", ");
			$price = $v["course_price"];
			$discount = $v["course_discount"];

			if($cd){    
				$display_date = "";
				$display_time = "";
				$display_address = "";
				foreach ($cd as $kk => $vv) {
					$row = get_course_detail(" and a.course_detail_id={$vv["course_detail_id"]}");
					if($row) $row = $row[0];
					$course_detail_id = $row['course_detail_id'];
					$course_id = $row['course_id'];
					$day = $row['day'];
					$date = $row['date'];
					$time = $row['time'];
					$display_date .= $day." ".revert_date($date).", ";
					$display_time .= $time.", ";
					$display_address .= $row['address_detail']." ".$row['address'].", ";
				}

			}else{
				$display_date = "";
				$display_time = "";
				$display_address = "";
				$row = get_course_detail(" and a.course_detail_id={$v["course_detail_id"]}");
				if($row) $row = $row[0];
				$course_detail_id = $row['course_detail_id'];
				$course_id = $row['course_id'];
				$day = $row['day'];
				$date = $row['date'];
				$time = $row['time'];
				$display_date .= $day." ".revert_date($date).", ";
				$display_time .= $time.", ";
				$display_address .= $row['address']." ".$row['address_detail'].", ";
			}

			$display_date = trim($display_date, ", ");
			$display_time = trim($display_time, ", ");
			$display_address = trim($display_address, ", ");
			$id = $v["register_id"];
			$title = str_replace(", ", "<br>",trim($title, ", "));
			$display_date = str_replace(", ", "<br>",trim($display_date, ", "));
			$display_time = str_replace(", ", "<br>",trim($display_time, ", "));
			$display_address = str_replace(", ", "<br>",trim($display_address, ", "));
			$content = "";
			$pay_type = "";
			$print_style = "";

			$pay = $v["pay_id"];
			$status = $v["pay_status"];
			$ckBox = ($_POST["type"]=="register-approve") ? '<input type="text" name="no['.$id.']" id="no-'.$id.'" value="'.$v["no"].'" style="float:right; width:50px;"><input type="hidden" name="register_id[]" id="'.$id.'" value="'.$id.'">' : '';
			$ckBox = ($_POST["type"]=="register-receipt") ? '<input type="checkbox" name="ckbox['.$id.']" id="ckbox-'.$id.'" value="T" style="float:right;"><input type="hidden" name="register_id[]" id="'.$id.'" value="'.$id.'">' : $ckBox;
			$ckBox = ($_POST["type"]=="register-result") ? '<input type="checkbox" name="ckbox['.$id.']" id="ckbox-'.$id.'" value="'.$id.'" style="float:right;"><input type="hidden" name="register_id[]" id="'.$id.'" value="'.$id.'">' : $ckBox;
		$status_pay = $arr_pay[$v["pay_status"]];
	  	$print = '<a class="btn btn-info" onClick="printInfo(\''.$id.'\')"> <i class="fa fa-print"></i></a>';
	  	$acs = array(3);
	  	if(!in_array($status, $acs)){
	  		$print = "";
	  	}
		$pay_type_regis = "";
		if($v["pay"]=="walkin" || $v["pay"]=="importfile"){
			$pay_type_regis = "BACK";
		}else { 
			$pay_type_regis = "WEB";
		}
		if($v["pay"]=="at_ati"){
			$pay_type = "ชำระเงินสดผ่าน ATI";
		}else if($v["pay"]=="paysbuy"){
			$pay_type = "ชำระเงินช่องทางอื่นๆ (paysbuy)";
			$print_style = "visibility:hidden";
		}else if($v["pay"]=="mpay"){
			$pay_type = "ชำระเงินช่องทางอื่นๆ (mPAY)";
			$print_style = "visibility:hidden";
		}else if($v["pay"]=="bill_payment"){
			$pay_type = "ชำระเงินผ่าน Bill-payment";
		}else if($v["pay"]=="at_asco"){
			$pay_type = "เช็ค/เงินโอน";
		}else if($v["pay"]=="walkin"){
			if($v["pay_status"]==5 || $v["pay_status"]==6)
				$pay_type = $arr_pay[$v["pay_status"]];
			else	
				$pay_type = "ชำระเงินสดผ่าน ATI";
		}else if($v["pay"]=="importfile"){
			if($v["pay_status"]==5 || $v["pay_status"]==6)
				$pay_type = $arr_pay[$v["pay_status"]];
			else	
				$pay_type = "ชำระเงินสดผ่าน ATI";
		}
		if($v["pay_status"]==6){
			$print = "";
		}		
		$receipt_full = $v["receipt_title"].$v["receipt_fname"]." ".$v["receipt_lname"];
		$address = $v["receipt_no"];
		if($v["receipt_gno"]!="") 
			$address .= " ".$v["receipt_gno"];
		if($v["receipt_moo"]!="") 
			$address .= " ".$v["receipt_moo"];
		if($v["receipt_soi"]!="") 
			$address .= " ".$v["receipt_soi"];
		if($v["receipt_road"]!="") 
			$address .= " ".$v["receipt_road"];
        $address .= $v["receipt_district_name"];
        $address .= $v["receipt_amphur_name"];
        $address .= $v["receipt_province_name"];

        if($v["slip_type"]=="corparation"){
        	$receipt_full = $v["receipt_name"];
        }

        $str_address = $receipt_full."<br>";
        $str_address .= $address;
		if($_POST["type"]=="register-receipt"){
			$a[] = array($runNo." ".$ckBox
						  ,$v["docno"]
						  ,$code_project
					      ,$v['title']." ".$v['fname']." ".$v['lname']
					      //,$v['fname']." ".$v['lname']
					      ,$str_address
						  ,revert_date($v["pay_date"], true)
						  ,$pay_type
						  ,get_datatable_icon("edit", $id)
					      ,$print);

		}else if($_POST["type"]=="register-result"){

			if($v["coursetype_id"]==3 && $v["section_id"]==2){
				$a[] = array($runNo." ".$ckBox
							  ,$v["no"]
						      ,$v['title']." ".$v['fname']." ".$v['lname']
						      //,$v['fname']." ".$v['lname']
							  ,$title
						      ,$v["te_results_name"]
						      ,$v["result_name"]
							  ,revert_date($v["result_date"], true));
			}else{
				// echo "string"; die();
				$a[] = array($runNo." ".$ckBox
							  ,$v["no"]
						      ,$v['title']." ".$v['fname']." ".$v['lname']
							  //,$v['fname']." ".$v['lname']
							  ,$title
							  ,$display_address
						      ,$v["result_name"]
							  ,revert_date($v["result_date"], true));				
			}

		}else{
			$a[] = array($runNo." ".$ckBox
					      ,$v['title']." ".$v['fname']." ".$v['lname']
					      //,$v['fname']." ".$v['lname']
					      ,$title
						  ,revert_date($v["date"], true)
						  ,revert_date($v["expire_date"], true)
						  ,revert_date($v["pay_date"], true)
					      ,$status_pay
						  ,$v["remark"]
						  ,'<span style="text-align:center; width:100%;display:block;">'.$pay_type_regis.'</span>' 
					      ,$print);
			
		}
		$runNo++;
	}
}

$aData = array();
$sQuery = "SELECT COUNT(*) as total
			  FROM register a
			  $WHERE $sWhere";

$rs = $db->data($sQuery);
$iFilteredTotal = $rs;
 
$sQuery = "SELECT COUNT(*) as total
			  FROM register a";
$resultTotal = $db->data($sQuery);
$iTotal = $resultTotal;
						 
$aData["sEcho"] = intval($_POST['sEcho']);
$aData["iTotalRecords"] = $iTotal; 
$aData["iTotalDisplayRecords"] = $iFilteredTotal; 
$aData["aaData"] = $a; 

}

echo json_encode($aData);
?>
