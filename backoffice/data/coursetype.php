<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/member.php";
include_once "../share/datatype.php";
include_once "../share/section.php";
include_once "../share/course.php";

global $SECTIONID, $COURSETYPEID;
$data = array();

$id = $_GET['value'];
$qtype = $_GET['name'];
if($qtype == 'coursetype') {
    if($id!=""){

        $q = "select 
                a.aob_rom,
                a.sorb,
                a.aob_rom_sorb,
                a.aob_rom_tor_ar_yu                
         from section a             
         where a.active='T' and a.section_id=$id";
        $section = $db->rows($q);
        $array_id = array();;
        foreach ($section as $kk => $value) {
           if($value!="have") continue;
           $array_id[] = map_course_type($kk);
        }
        $ids = implode(",", $array_id);
        if($COURSETYPEID>0){
            $ids = $COURSETYPEID;
        }
        $con = " and a.active='T' and a.coursetype_id in ($ids)";
        $coursetype = datatype($con, "coursetype", true);        
        if($coursetype>0){
        	foreach ($coursetype as $key => $v) {

        		 $data[] = array($v["coursetype_id"],$v["name"]);
        	}
        }
    }   
}

echo json_encode($data);
?>