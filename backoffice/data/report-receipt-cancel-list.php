<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/datatype.php";
include_once "../share/course.php";
global $db;
$apay = datatype(" and a.active='T'", "pay_status", true);
$arr_pay = array();
foreach ($apay as $key => $value) {
	$arr_pay[$value["pay_status_id"]] = $value["name"];
}
set_time_limit(600);
$single_info = $_POST["single"];
if($single_info=="T"){
	$aData = array();
	$id = $_POST["member_id"];
	if($id){
   $r = view_member("", $id);
    foreach ($r as $k => $v) {
    	$v["select_university"] = $v["grd_ugrp1"]."-".$v["grd_uid1"];
    	$v["birthdate"] = revert_date($v["birthdate"]);
    	$v["password2"] = $v["password"];     
	    $aData[] = $v;	   }  
	}
}else{
  $aColumns = array( 'member_Id','title_th','fname_th','lname_th','cid');
/* Indexed column (used for fast and accurate table cardinality) */
$sIndexColumn = "memberId";

function fnColumnToField( $i ){
	/* Note that column 0 is the details column */
	if ( $i == 0)
		return "a.register_id";
	else if ( $i == 1)
		return "a.docno";
	else if ( $i == 3)
		return "a.fname";
	else if ( $i == 5)
		return "a.fname";
	else
		return "a.pay_date DESC";
}

$sLimit = "";
if (isset( $_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1' )
{
	$sLimit = "LIMIT ".(int)($_POST['iDisplayStart'] );
	$sLimit .= ", ".(int)( $_POST['iDisplayLength'] );
}


/* Ordering */
if(isset($_POST['iSortCol_0'])){
	$sOrder = "ORDER BY  ";
	for ( $i=0 ; $i<$db->escape( $_POST['iSortingCols'] ) ; $i++ ){
		$sOrder .= fnColumnToField($db->escape( $_POST['iSortCol_'.$i] ))."
                ".$db->escape( $_POST['sSortDir_'.$i] ) .", ";
	}
	$sOrder = substr_replace( $sOrder, "", -2 );
}else{
	// $sOrder  = "ORDER BY a.date ASC";
	$sOrder  = "ORDER BY a.docno DESC";
}

/* Filtering */
$sWhere = "";
//$WHERE = "WHERE a.active!='' and a.docno <> ''";
$WHERE = "WHERE a.active='T' AND a.auto_del = 'F' AND a.pay_status = 8 AND (a.docno<>'' AND a.docno IS NOT NULL) ".$aWhere;
if($_POST['sSearch'] != ""){
   $sWhere = " and  (a.title LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "a.fname LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "a.lname LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".			    
			    "a.docno LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".			    
			    // "a.ref1 LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".			    
			    // "a.ref2 LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "f.code_project LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".	
			    "g.code_project LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".		    
			    "a.cid LIKE '%".$db->escape( $_POST['sSearch'] )."%' )";
	$sAND = "AND ";
	$n = explode(" ", trim($_POST['sSearch']));
	if(count($n)>1){
		$sWhere .= " OR (a.fname='{$n[0]}' AND a.lname='{$n[1]}' )";
	}
}

switch ($_POST["pay_by"]) {
	case 'at_ati': 	
		$pay_type = " AND (a.pay='at_ati' OR a.pay='walkin' OR a.pay='importfile')";
		break;
	default:
		$pay_type = " AND a.pay='{$_POST["pay_by"]}'";
		break;
}

// $sWhere .= ($_POST["section_id"]) ? " and a.section_id={$_POST["section_id"]}" : "";
// $sWhere .= ($_POST["member_id"]) ? " and a.member_id={$_POST["member_id"]}" : "";
// $sWhere .= ($_POST["coursetype_id"]) ? " and a.coursetype_id={$_POST["coursetype_id"]}" : "";
$sWhere .= ($_POST["pay_status"]) ? " and a.pay_status={$_POST["pay_status"]}" : "";
//$sWhere .= ($_POST["active"]) ? " and a.active='{$_POST["active"]}'" : "";
//$sWhere .= ($_POST["pay_by"]) ? " and a.pay='{$_POST["pay_by"]}'" : "";
$sWhere .= ($_POST["pay_by"]) ? "$pay_type" : "";

$register_ids = array();
$flag = 0;
if($_POST["course_detail_id"]){
	
	$q = "select a.register_id from register a where a.active='T' and a.course_detail_id={$_POST["course_detail_id"]}";

	$cd = $db->get($q);
	if($cd){
		foreach ($cd as $key => $value) {
			$register_ids[$value["register_id"]] = $value["register_id"];
		}
	}

	$q = "select a.course_detail_id, a.register_id from register_course_detail a where a.active='T' and a.course_detail_id={$_POST["course_detail_id"]}";
	$cd = $db->get($q);	
	if($cd){
		foreach ($cd as $key => $value) {
			$register_ids[$value["register_id"]] = $value["register_id"];
		}
	}
	/*print_r($register_ids);*/
	if(count($register_ids)>0){
		$register_ids = implode(",", $register_ids);
		$sWhere .= " and a.register_id in ($register_ids)";
	}else{
		$flag = 1;
		$sWhere .= " and a.register_id in (0)";
	}
}
$dateStart = ($_POST["date_start"]) ? thai_to_timestamp($_POST["date_start"]) :  "";
$dateStop =  ($_POST["date_stop"]) ? thai_to_timestamp($_POST["date_stop"]) : "";
if ($dateStart || $dateStop) {
    if (!$dateStart && $dateStop)
        $dateStart = $dateStop;
    if (!$dateStop && $dateStart)
        $dateStop = $dateStart;
    $t = $dateStart;
    if ($dateStart > $dateStop) {
        $dateStart = $dateStop;
        $dateStop = $t;
    }
}
$sWhere .= ($dateStart && $dateStop) ? " and a.date>='$dateStart 00:00:00' and a.date<='$dateStop 23:59:59'" : "";
/* Paging */
$sQuery = "SELECT a.register_id,
			a.`no`,
			a.runno,
			a.docno,
			a.section_id,
			a.coursetype_id,
			a.member_id,
			a.title,
			a.fname,
			a.lname,
			a.cid,
			a.slip_type,
			a.receipttype_id,
			a.receipttype_text,
			a.receipt_id,
			a.taxno,
			a.slip_name,
			a.slip_address1,
			a.slip_address2,
			a.date,
			a.register_by,
			a.ref1,
			a.ref2,
			a.`status`,
			a.expire_date,
			a.exam_date,
			a.course_id,
			a.course_detail_id,
			a.course_price,
			a.course_discount,
			a.pay,
			a.pay_status,
			a.pay_date,
			a.pay_method,
			a.pay_yr,
			a.pay_mn,
			a.pay_id,
			a.pay_price,
			a.pay_diff,
			a.approve,
			a.approve_by,
			a.approve_date,
			a.register_mod,
			a.last_mod,
			a.last_mod_date,
			a.result,
			a.result_date,
			a.result_by,
			a.receipt_title,
			a.receipt_fname,
			a.receipt_lname,
			a.receipt_no,
			a.receipt_gno,
			a.receipt_moo,
			a.receipt_soi,
			a.receipt_road,
			a.receipt_district_id,
			a.receipt_amphur_id,
			a.receipt_province_id,
			a.receipt_postcode,
			a.active,
			a.recby_id,
			a.rectime,
			a.remark,
            b.name AS receipt_district_name,
            c.name AS receipt_amphur_name,
            d.name AS receipt_province_name,
            e.name AS receipt_name
	FROM register a LEFT JOIN district b ON b.district_id=a.receipt_district_id
        LEFT JOIN amphur c ON c.amphur_id=a.receipt_amphur_id
        LEFT JOIN province d ON d.province_id=a.receipt_province_id        
        LEFT JOIN receipt e ON e.receipt_id=a.receipt_id
        LEFT JOIN course f ON f.course_id = a.course_id
        LEFT JOIN course_detail g ON g.course_detail_id = a.course_detail_id
		$WHERE $sWhere
		$sOrder
		$sLimit";
/*echo $sQuery;*/
$rResult = $db->get($sQuery);

//echo $sQuery;

	/*//write log
	$str = $sQuery;
	//$str = $sWhere;	
	//$str = $_POST['rc_status'];
	$date_now = date('Y-m-d H:i:s');
	$date_log = date('Y-m-d');
	$log_name = "./log/report-recieptlist_".$date_log.".log";
	$file = fopen($log_name, 'a');
	$str_txt = $date_now."|".$str."\r\n";
	fwrite($file, $str_txt);
	fclose($file);*/


$a = array();
if(is_array($rResult)){
	$runNo = 1;
	foreach ($rResult as $v){
			$register_id = $v["register_id"];
			$sCourse_id = $v["course_id"];
			$con = " and a.course_id in ($sCourse_id)";
			$r = get_course($con);
			$title = "";
			$parent_id = 0;
			$price = 0;
			if($r){
				foreach ($r as $key => $row) {
					$course_id = $row['course_id'];
					$section_id = $row['section_id'];
					$code = $row['code'];
					/*$code_project = $row["code_project"];*/
					$title .= $row['title'].", ";
					$set_time = $row['set_time'];
					$life_time = $row['life_time'];
					$status = $row['status']; 
					$parent_id = (int)$row['parent_id'];
				}
			}
			$q = "select a.course_detail_id from register_course_detail a where a.register_id={$register_id} and a.course_id in ($sCourse_id)";
			$cd = $db->get($q);
			$parent_title = "";
			if($parent_id>0){
				$q = "select title from course where active='T' and course_id=$parent_id";
				$t = $db->data($q);
				$parent_title = "<strong>{$t}</strong><br>";	
			}

			$title = trim($title, ", ");
			$price = $v["course_price"];
			$discount = $v["course_discount"];

			if($cd){    
				$display_date = "";
				$display_time = "";
				$display_address = "";
				foreach ($cd as $kk => $vv) {
					$row = get_course_detail(" and a.course_detail_id={$vv["course_detail_id"]}");
					if($row) $row = $row[0];
					$course_detail_id = $row['course_detail_id'];
					$course_id = $row['course_id'];
					$code_project = $row["code_project"];
					$day = $row['day'];
					$date = $row['date'];
					$time = $row['time'];
					$display_date .= $day." ".revert_date($date).", ";
					$display_time .= $time.", ";
					$display_address .= $row['address_detail']." ".$row['address'].", ";
				}

			}else{
				$display_date = "";
				$display_time = "";
				$display_address = "";
				$row = get_course_detail(" and a.course_detail_id={$v["course_detail_id"]}");
				if($row) $row = $row[0];
				$course_detail_id = $row['course_detail_id'];
				$course_id = $row['course_id'];
				$code_project = $row["code_project"];
				$day = $row['day'];
				$date = $row['date'];
				$time = $row['time'];
				$display_date .= $day." ".revert_date($date).", ";
				$display_time .= $time.", ";
				$display_address .= $row['address']." ".$row['address_detail'].", ";
			}

			$display_date = trim($display_date, ", ");
			$display_time = trim($display_time, ", ");
			$display_address = trim($display_address, ", ");
			$id = $v["register_id"];
			$title = str_replace(", ", "<br>",trim($title, ", "));
			$display_date = str_replace(", ", "<br>",trim($display_date, ", "));
			$display_time = str_replace(", ", "<br>",trim($display_time, ", "));
			$display_address = str_replace(", ", "<br>",trim($display_address, ", "));
			$content = "";
			$pay_type = "";
			$print_style = "";

			$pay = $v["pay_id"];
			$status = $v["pay_status"];
			$ckBox = ($_POST["type"]=="register-receipt") ? '<input type="checkbox" name="ckbox['.$id.']" id="ckbox-'.$id.'" value="'.$id.'" style="float:right;"><input type="hidden" name="register_id[]" id="'.$id.'" value="'.$id.'">' : $ckBox;
		$status_pay = $arr_pay[$v["pay_status"]];
	  	$print = '<a class="btn btn-info" onClick="printInfo(\''.$id.'\')"> <i class="fa fa-print"></i></a>';
		$pay_type_regis = "";
		if($v["pay"]=="walkin" || $v["pay"]=="importfile"){
			$pay_type_regis = "IH";
		}else { 
			$pay_type_regis = "WR";
		}
		if($v["pay"]=="at_ati"){
			$pay_type = "ชำระเงินสดผ่าน ATI";
		}else if($v["pay"]=="walkin"){
			$pay_type = "ชำระเงินสดผ่าน ATI";			
		}else if($v["pay"]=="importfile"){
			$pay_type = "ชำระเงินสดผ่าน ATI";
		}else if($v["pay"]=="paysbuy"){
			$pay_type = "ชำระเงินช่องทางอื่นๆ (paysbuy)";
			$print_style = "visibility:hidden";
		}else if($v["pay"]=="mpay"){
			$pay_type = "ชำระเงินช่องทางอื่นๆ (mPAY)";
			$print_style = "visibility:hidden";
		}else if($v["pay"]=="bill_payment"){
			$pay_type = "ชำระเงินผ่าน Bill-payment";
		}else if($v["pay"]=="at_asco"){
			$pay_type = "เช็ค/เงินโอน";
		}
	  	/*$acs = array(3);
	  	if(!in_array($status, $acs)){
	  		$print = "";
	  	}*/
		if($v["pay_status"]==6){
			$print = "";
		}
		$receipt_full = $v["receipt_title"].$v["receipt_fname"]." ".$v["receipt_lname"];
		$address = $v["receipt_no"];
		if($v["receipt_gno"]!="") 
			$address .= " ".$v["receipt_gno"];
		if($v["receipt_moo"]!="") 
			$address .= " ".$v["receipt_moo"];
		if($v["receipt_soi"]!="") 
			$address .= " ".$v["receipt_soi"];
		if($v["receipt_road"]!="") 
			$address .= " ".$v["receipt_road"];
        $address .= " ".$v["receipt_district_name"];
        $address .= $v["receipt_amphur_name"];
        $address .= $v["receipt_province_name"];

        if($v["slip_type"]=="corparation"){
        	$receipt_full = $v["receipt_name"];
        }
        if($v["slip_type"]=="corparation" && $v["receipttype_id"]==5){
        	$receipt_full = $v["receipttype_text"];
        }

        $str_address = $receipt_full."<br>";
        if ( !empty($receipt_full) && ($receipt_full!="") && isset($receipt_full) ) {
        	$str_address .= "เลขที่ผู้เสียภาษี ".$v["taxno"]."<br>";
        }
        $str_address .= $address;

		if($_POST["type"]=="register-receipt"){
			$a[] = array($runNo
						  ,$v["docno"]
						  ,$code_project
					      ,$v['title']." ".$v['fname']." ".$v['lname']
					      ,$str_address
						  ,revert_date($v["date"], true)
						  ,$pay_type
			);

		}

		$runNo++;
	}
}

$aData = array();
$sQuery = "SELECT COUNT(*) as total
			  FROM register a
			  $WHERE $sWhere";

$rs = $db->data($sQuery);
$iFilteredTotal = $rs;
 
$sQuery = "SELECT COUNT(*) as total
			  FROM register a";
$resultTotal = $db->data($sQuery);
$iTotal = $resultTotal;
						 
$aData["sEcho"] = intval($_POST['sEcho']);
$aData["iTotalRecords"] = $iTotal; 
$aData["iTotalDisplayRecords"] = $iFilteredTotal; 
$aData["aaData"] = $a; 

}

echo json_encode($aData);
?>
