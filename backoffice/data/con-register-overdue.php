<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "overdue-sendmail.php";
include_once "../share/course.php";
global $db;

$q = "select register_id
		, expire_date
		, pay_status 
	from register 
	where active='T' and (pay_status=1 or pay_status='' or pay_status=null) and pay!='bill_payment'";
$r = $db->get($q);
//echo $q;
foreach ($r as $key => $data) {
	$pay_status = $data["pay_status"];
	$register_id = $data["register_id"];	
	$exd = explode(" ", $data["expire_date"]);
	$expire_date = $exd[0];
	$start = time();
	$stop = convert_mktime($expire_date." 23:59:59");
	$time = $stop - $start;
	$status = ($time>0) ? "T" : "F";
	if($status=="F"){
		$msg = "";
		if ($pay_status==1) {
			$msg = "ไม่ชำระเงินตามกำหนด";
		}
		if ( empty($pay_status) ) {
			$msg = "ล้างข้อมูล";
		}
		$args = array();
		$args["table"] = "register";
		$args["id"] = $register_id;
		$args["pay_status"] = 4;
		$args["remark"] = $msg;
		$db->set($args);
		sendmail($register_id);

		$this_file = basename($_SERVER["SCRIPT_FILENAME"], '.php');
		register_log_save($register_id, 'update', $this_file, false);
	}
	
	$msg = date("Y-m-d H:i:s", $start)." - ".date("Y-m-d H:i:s", $stop)." =  ".$status;
	write_log("con-register-overdue", $msg, "a", "./log/" ); 
	
	echo date("Y-m-d H:i:s", $start)." - ".date("Y-m-d H:i:s", $stop)." =  ".$status;
	echo "<hr>";
}
?>