<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/member.php";
global $db;
$single_info = $_POST["single"];
if($single_info=="T"){
	$aData = array();
	$id = $_POST["member_id"];
	if($id){
   $r = view_member("", $id);
    foreach ($r as $k => $v) {
    	
    	$v["select_university"] = $v["grd_ugrp1"]."-".$v["grd_uid1"];
    	$v["birthdate"] = revert_date($v["birthdate"]);
    	$v["password2"] = $v["password"];     
	    $aData[] = $v;	   }  
	}
}else{
  $aColumns = array( 'member_Id','title_th','fname_th','lname_th','cid');
/* Indexed column (used for fast and accurate table cardinality) */
$sIndexColumn = "memberId";

function fnColumnToField( $i ){
	/* Note that column 0 is the details column */
	if ( $i == 0 ||$i == 1 )
		return "member_id";
	else if ( $i == 2 )
		return "cid";
	else if ( $i == 3 )
		return "fname_th";
	else if ( $i == 4 )
		return "fname_en";
	else if ( $i == 5 )
		return "reg_date";
}

$sLimit = "";
if (isset( $_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1' )
{
	$sLimit = "LIMIT ".(int)($_POST['iDisplayStart'] );
	$sLimit .= ", ".(int)( $_POST['iDisplayLength'] );
}


/* Ordering */
if(isset($_POST['iSortCol_0'])){
	$sOrder = "ORDER BY  ";
	for ( $i=0 ; $i<$db->escape( $_POST['iSortingCols'] ) ; $i++ ){
		$sOrder .= fnColumnToField($db->escape( $_POST['iSortCol_'.$i] ))."
                ".$db->escape( $_POST['sSortDir_'.$i] ) .", ";
	}
	$sOrder = substr_replace( $sOrder, "", -2 );
}else{
	$sOrder = "member_id desc";
}
 
/* Filtering */
$WHERE = "WHERE  active!='' ";
$sWhere = " ";
$sAND = "";
if($_POST['sSearch'] != ""){
   $sWhere = "   ( title_th LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "fname_th LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "lname_th LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".			    
                "title_en LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "fname_en LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "lname_en LIKE '%".$db->escape( $_POST['sSearch'] )."%' OR ".
			    "cid LIKE '%".$db->escape( $_POST['sSearch'] )."%' )";
	$sAND = "AND ";
	
}
$dateStart = ($_POST["date_start"]) ? thai_to_timestamp($_POST["date_start"]) :  "";
$dateStop =  ($_POST["date_stop"]) ? thai_to_timestamp($_POST["date_stop"]) : "";
if ($dateStart || $dateStop) {
    if (!$dateStart && $dateStop)
        $dateStart = $dateStop;
    if (!$dateStop && $dateStart)
        $dateStop = $dateStart;
    $t = $dateStart;
    if ($dateStart > $dateStop) {
        $dateStart = $dateStop;
        $dateStop = $t;
    }
}
$sWhere .= ($dateStart && $dateStop) ? " and reg_date>='$dateStart 00:00:00' and reg_date<='$dateStop 23:59:59'" : "";
//$sWhere .= ($_POST["fa_member"]=="T") ? " and fa='T'" : "";
/* Paging */
$sQuery = "SELECT member_id
			, title_th
			, fname_th
			, title_th_text
			, title_en_text
			, lname_th
			, title_en
			, fname_en
			, lname_en
			, cid
			, reg_date
           FROM member
		   $WHERE $sAND $sWhere
		   $sOrder
		   $sLimit";

$rResult = $db->get($sQuery);
$a = array();
if(is_array($rResult)){
	$runNo = 1;
	foreach ($rResult as $r){
		$id = $r["member_id"]; 
	  $manage =  get_datatable_icon("edit", $id);
	  $active = ($r["active"]=="T") ? "active" : "nonActive"; 
	  $url = "index.php?p=mamber&type=register-history&member_id=".$r["member_id"];
	  $button = '<a class="btn btn-success" href="'.$url.'" target="_blank"><i class="fa fa-list"></i>ประวัติ/ลงทะเบียน </a>';   	  
	  $btn_reset_passwd = '<a class="btn btn-warning" title="Reset password" onClick="reset_passwd('.$r["member_id"].','.$r["cid"].')"><i class="fa fa-exclamation-triangle"></i></a>';
	  $btn_quick_edit = '<a class="btn btn-primary" target="_blank" onClick="quick_edit('.$r["member_id"].')"><i class="fa fa-th-large" aria-hidden="true"></i></a>';

		if ( ($r["title_en"]=='Other')&&(!empty($r["title_en_text"])) ) {			
			$r['title_en'] = $r["title_en_text"];
		}
		if ( ($r["title_th"]=='อื่นๆ')&&(!empty($r["title_th_text"])) ) {
			$r['title_th'] = $r["title_th_text"];
		}
		$a[] = array($runNo
				      ,$r["cid"]
				      ,$r['title_th']." ".$r['fname_th']." ".$r['lname_th']
				      ,$r['title_en']." ".$r['fname_en']." ".$r['lname_en']
				      ,revert_date($r['reg_date'], true)
				      ,$btn_quick_edit." ".$manage." ".$button." ".$btn_reset_passwd);
		$runNo++;
	}
}

$aData = array();
$sQuery = "SELECT COUNT(*) as total
			  FROM member
			  $WHERE $sAND $sWhere";

$rs = $db->data($sQuery);
$iFilteredTotal = $rs;
 
$sQuery = "SELECT COUNT(*) as total
			  FROM member";
$resultTotal = $db->data($sQuery);
$iTotal = $resultTotal;
						 
$aData["sEcho"] = intval($_POST['sEcho']);
$aData["iTotalRecords"] = $iTotal; 
$aData["iTotalDisplayRecords"] = $iFilteredTotal; 
$aData["aaData"] = $a; 

}

echo json_encode($aData);
?>
