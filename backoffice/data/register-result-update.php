<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/course.php";
global $db, $EMPID;
$datetime_now = date("Y-m-d H:i:s");

$id = trim($_POST["register_id"],",");
$result = $_POST["result"];
$te_results = $_POST["te_results"];
$cert_date = $_POST["cert_date"];

$course_detail_id = $_POST["course_detail_id"];
$course_id = $_POST["course_id"];

$course_info = get_course("", $course_id);
$course_info = $course_info[0];
$coursetype_id = $course_info["coursetype_id"];

// d($course_info); die();

if ( !empty($cert_date) ) {
	$tmp = explode("-", $cert_date);
	$year = $tmp[2]-543;
	$cert_date = $year."-".$tmp[1]."-".$tmp[0]." 00:00:00";	
}else{
	$cert_date = $datetime_now;
}//end else

if($id){

	$array = explode(",", $id);
	if($array){
		$c = 0;
		foreach ($array as $key => $register_id) {
			if(!$register_id) continue;

				$q = "SELECT register_course_detail_id 
					FROM register_course_detail 
					WHERE course_detail_id={$course_detail_id}
						AND course_id={$course_id}
						AND register_id = {$register_id}
				";
				// echo $q; die();
				$ids = $db->data($q);
				if($ids>0){
					$args = array();
					$args["table"] = "register_course_detail";
					$args["id"] = $ids;
					if($result>0 || $te_results>0){
						//$args["result_date"] = date("Y-m-d H:i:s");
						$args["result_date"] = ($result>0) ? $cert_date : "0000-00-00 00:00:00";
						$args["te_result_date"] = ($te_results>0) ? $cert_date : "0000-00-00 00:00:00";					
					}else{
						$args["result_date"] = "0000-00-00 00:00:00";
						$args["te_result_date"] = "0000-00-00 00:00:00";
					}//end else

					if ( !empty($result) && !empty($te_results) ) {
						$args["register_result_id"] = $result;
						$args["register_te_result_id"] = $te_results;
					}elseif ( !empty($result) || !empty($te_results) ) {
						if ( $coursetype_id==1 ) {
							$args["register_te_result_id"] = $result;
							$args["te_result_date"] = $cert_date;
						}elseif ( $coursetype_id==2 ) {
							$args["register_result_id"] = $result;
							$args["result_date"] = $cert_date;
						}//end else if
					}//end else if
					

					$args["result_by"] = $EMPID;
					$args["te_result_by"] = $EMPID;
					$args["recby_id"] = $EMPID;
					$args["result_from"] = "WR";
					$args["te_result_from"] = "WR";
					$args["rectime"] = date("Y-m-d H:i:s");
					// d($args); die();
					$db->set($args);
					// var_dump($db->set($args, true, true)); die();
				}else{
					$q = "SELECT course_detail_id
							,  course_id
						FROM register 
						WHERE active='T'
							AND pay_status IN (3,5,6)
							AND register_id=$register_id
					";
					$rs = $db->rows($q);
					$cos_dt_ids = explode(":", $rs["course_detail_id"]);
					if ( count($cos_dt_ids)==2 ) {
						$course_id = $rs["course_id"];
						$course_detail_id = $cos_dt_ids[1];
						$q = "SELECT register_course_detail_id 
							FROM register_course_detail 
							WHERE course_detail_id={$course_detail_id}
								AND course_id={$course_id} 
								AND register_id = {$register_id}
						";
						//echo $q;
						$ids = $db->data($q);
						if($ids>0){
							$args = array();
							$args["table"] = "register_course_detail";
							$args["id"] = $ids;
							if($result>0 || $te_results>0){
								//$args["result_date"] = date("Y-m-d H:i:s");
								$args["result_date"] = ($result>0) ? $cert_date : "0000-00-00 00:00:00";
								$args["te_result_date"] = ($te_results>0) ? $cert_date : "0000-00-00 00:00:00";
							}else{
								$args["result_date"] = "0000-00-00 00:00:00";
								$args["te_result_date"] = "0000-00-00 00:00:00";
							}//end else
							$args["register_result_id"] = $result;
							$args["register_te_result_id"] = $te_results;
							$args["result_by"] = $EMPID;
							$args["te_result_by"] = $EMPID;
							$args["recby_id"] = $EMPID;
							$args["result_from"] = "WR";
							$args["te_result_from"] = "WR";
							$args["rectime"] = date("Y-m-d H:i:s");
							// d($args); die();
							$db->set($args);	
						}//end if
					}else{				

						$args = array();
						$args["table"] = "register";
						$args["id"] = $register_id;
						if($result>0 || $te_results>0)
							//$args["result_date"] = date("Y-m-d H:i:s");
							$args["result_date"] = $cert_date;
						else{
							$args["result_date"] = "0000-00-00 00:00:00";
						}
						$args["result"] = $result;
						$args["te_results"] = $te_results;
						$args["result_by"] = $EMPID;
						$args["recby_id"] = $EMPID;
						$args["rectime"] = date("Y-m-d H:i:s");
						$db->set($args);	
										
					}//end else
				}//end else

			$c++;
		}
		if($c>0)
			echo "บันทึกผลกิจกรรมเรียบร้อย";
	}
}
die();
?>