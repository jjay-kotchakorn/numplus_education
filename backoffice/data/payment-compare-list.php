<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/bill-payment-upload.php";
global $db;

$date_now = date('Y-m-d H:i:s');
$date = date('Y-m-d');

$sLimit = "";
if (isset( $_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1' )
{
	$sLimit = "LIMIT ".(int)($_POST['iDisplayStart'] );
	$sLimit .= ", ".(int)( $_POST['iDisplayLength'] );
}

$sOrder = "ORDER BY a.payment_compare_file_list_id DESC"; 

/* Filtering */
  $sWhere = "";
  $WHERE = "WHERE a.active!='' ";
  $sAND = "";
if($_POST['sSearch'] != ""){
	$sAND = "AND ";	
	$sWhere .= "(";
   	$sWhere .= "a.file_name LIKE '%".$db->escape( $_POST['sSearch'] )."%'";
   	$sWhere .= " OR a.pay_date LIKE '%".$db->escape( $_POST['sSearch'] )."%'";
   	$sWhere .= ")";

}
/* Paging */
/*$sQuery = "SELECT a.bill_payment_upload_id, a.code, a.name, a.active, a.remark, CONCAT(b.prefix,b.fname,' ',b.lname) as recby_name
           FROM bill_payment_upload a inner join emp b on a.recby_id=b.emp_id
		   $WHERE $sAND $sWhere
		   $sOrder
		   $sLimit";
*/
$sQuery = "SELECT a.payment_compare_file_list_id
			, a.pay_date
			, a.file_name
			, CONCAT(b.prefix,b.fname,' ',b.lname) as recby_name
			, a.pay_type
			, a.rectime
			, a.active
			-- , a.result
           FROM payment_compare_file_list a 
           INNER JOIN emp b ON a.recby_id=b.emp_id
		   $WHERE $sAND $sWhere
		   $sOrder
		   $sLimit";

$rResult = $db->get($sQuery);

/*
	//write result to log
    $str_txt = "";
	$str_txt .= $date_now;

	$str_txt .= "|$sQuery";

	$str_txt .= "\r\n";

	$file_log = "../log/payment-compare-list.log";
	$file = fopen($file_log, 'a');
	fwrite($file, $str_txt);
	fclose($file);
*/

$a = array();
if(is_array($rResult)){
	$runNo = 1;
	foreach ($rResult as $r){
		// $id = $r["bill_payment_upload_id"]; 
	 	// $manage =  get_datatable_icon("edit", $id);
	  	$active = ($r["active"]=="T") ? "ใช้งานได้" : "ไฟล์ถูกเขียนทับ";   
		$a[] = array($runNo
				      ,$r['pay_date']
				      ,$r['file_name']
				      ,$r["pay_type"]
				      ,$active				      
				      ,$r["recby_name"]
				      ,$r["rectime"]
				      //,$r["result"]
				      );
		$runNo++;
	}
}

$aData = array();
$sQuery = "SELECT COUNT(*) as total
			  FROM payment_compare_file_list a
			  $WHERE $sAND $sWhere";

$rs = $db->data($sQuery);
$iFilteredTotal = $rs;
 
$sQuery = "SELECT COUNT(*) as total
			  FROM payment_compare_file_list a";
$resultTotal = $db->data($sQuery);
$iTotal = $resultTotal;
						 
$aData["sEcho"] = intval($_POST['sEcho']);
$aData["iTotalRecords"] = $iTotal; 
$aData["iTotalDisplayRecords"] = $iFilteredTotal; 
$aData["aaData"] = $a; 


echo json_encode($aData);
?>
