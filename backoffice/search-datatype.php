<?php
include_once "./share/authen.php";
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/menu.php";
include_once "./share/datatype.php";

$table = $_GET["table"] ? $_GET["table"] : "";
$map = $_GET["map"] ? $_GET["map"] : "";
$checkBox = $_GET["ckBox"] ? "T" : "";
$retFunc = $_GET["retFunc"] ? $_GET["retFunc"] : "retInfo";
$con = $_GET["con"] ? trim($_GET["con"]) : "";
global $db;
?>
<!DOCTYPE html>
<html lang="en">
<?php include ('inc/header.php'); ?>
<body>

        <div class="box-content">                    
            <div class="alert search-header">
                <button onClick="javascript:parent.$.fancybox.close();" class="close" type="button">×</button>
                รหัส&nbsp;<input class="form-control focused" id="srchcode" onKeyUp="disp();" style="width:100px;display:inline-block;" type="text" value="">
                ชื่อรายการ&nbsp;<input class="form-control focused" id="srchname" onKeyUp="disp();" style="width:150px;display:inline-block;" type="text" value="">
                ค้นหาภายในคำ&nbsp;<input type="checkbox" id="ck">
                <a href="#" class="btn btn-rad btn-info" onClick="disp();"><i class="fa fa-search"></i></a>&nbsp;&nbsp;
                <button onClick="getKey();" id="ok" class="btn btn-rad btn-success" type="button" title="คลิกเพื่อเลือกรายการ">เลือก/OK</button>
            </div>
            <div style="overflow-y:scroll; height:100%; vertical-align:top;"> 
                <table width="100%" class="table">
                    <thead>
                        <tr class="alert alert-success" style="font-weight:bold;">
                            <td width="10%"><input data-no-uniform="true" type="checkbox" onClick="ckAll(this)" id="ckAll">&nbsp;</td>
                            <td width="11%" class="center">รหัส</td>
                            <td width="79%" class="center">รายการ</td>                                      
                        </tr>
                    </thead>   
                </table>
            </div>
            <div style="overflow-y:scroll; height:285px; vertical-align:top; margin-top:-6px;">                      
                <table class="table table-striped" id="tbList">
                    <thead>
                        <tr style="cursor:pointer; " valign="middle" onClick="singleKey(this);">
                            <td width="10%"><input type="hidden" name="datatype_id" id="datatype_id"></td>
                            <td  width="11%" class="center"><span id="code"></span>-</td>
                            <td  width="79%" class="center"><span id="name"></span>-</td>                                   
                        </tr>
                    </thead>
                    <tbody>
                        <tr style="cursor:pointer;" onClick="singleKey(this);">
                            <td width="10%"><input type="hidden" name="datatype_id" id="datatype_id">
                                &nbsp;</td>
                            <td  width="11%" class="center"><span id="code"></span></td>
                            <td  width="79%" class="center"><span id="name"></span></td>                                   
                        </tr>
                        <tr style="cursor:pointer;">
                            <td width="10%">
                                <input type="hidden" name="datatype_id" id="datatype_id">
                                <input type="checkbox" id="ckBox">&nbsp;&nbsp;</td>
                            <td width="11%" class="center"><span id="code"></span></td>
                            <td width="79%" class="center"><span id="name"></span></td>                                   
                        </tr>
                    </tbody>
                </table>   
            </div>                      
        </div>
		

<?php   include_once ('inc/js-script.php'); ?>
    <?php include ('inc/footer.php') ?>
<script type="text/javascript">
        var checkBox = "<?= $checkBox ?>";
        var mapField = "<?php echo $map; ?>";
        var con = "<?php echo $con; ?>";
        if (checkBox == "T") {
            var trList = $("#tbList tbody tr:eq(1)").clone();
        } else {
            $("#ok").hide();
            var trList = $("#tbList tbody tr:eq(0)").clone();
            $("#ckAll").hide();
        }

        (checkBox == "T") ? delCkRow("#tbList", "1") : delRow("#tbList");

        $(document).ready(function() {
            disp();
        });

        function disp() {
            (checkBox == "T") ? delCkRow("#tbList") : delRow("#tbList");
            var url = "data/datatype.php";
            var param = "table=<?= $table ?>";
            param = param + "&name=" + $("#srchname").val();
            param = param + "&code=" + $("#srchcode").val();
            param = param + "&othercon=" + con;
            if ($("#ck").is(":checked")) {
                param = param + "&all=T";
            }
            $.ajax({
                "dataType": 'json',
                "type": "POST",
                "url": url,
                "data": param,
                "success": function(data) {
                    (checkBox == "T") ? delCkRow("#tbList") : delRow("#tbList");
                    $.each(data, function(index, array) {
                        var ck = "";
                        $("#tbList tbody tr :input").each(function() {
                            var t = $(this).val();
                            var ckId = array.datatype_id;
                            if (ckId == t) {
                                ck = "1";
                            }
                        });
                        if (ck == "1")
                            return;
                        addTrLine("#tbList", trList, array);
                    });
                }
            });
        }

        function selectData() {
            var id = new Array();
            id["id"] = "1";
            id["name"] = "name";
            id["name_eng"] = "name_eng";

            parent.<?php echo $retFunc ?>(id);
            parent.$.fancybox.close();
        }

        function singleKey(tag) {
            var id = $(tag).find('#datatype_id').val();
            var code = $(tag).find('#code').text();
            var name = $(tag).find('#name').text();
            if (mapField != "") {
                var r = mapField.split(",");
                parent.$("#" + r[0]).val(id);
                parent.$("#" + r[1]).val(name);
                parent.$("#" + r[2]).val(code);
                parent.$.fancybox.close();
            } else {
                parent.$("#" +<?= $table ?>).val(id);
                parent.$("#" +<?= $table ?>).val(name);
                parent.$("#" +<?= $table ?>).val(code);
                parent.$.fancybox.close();
            }
        }

        function getKey() {
            var ret = new Array();
            var i = 0;
            $("#tbList tbody tr :checkbox").each(function() {
                if ($(this).is(":checked")) {
                    var tag = $(this).parent().parent();
                    ret[i] = new Array();
                    ret[i]["id"] = $(tag).find('#datatype_id').val();
                    ret[i]["code"] = $(tag).find('#code').text();
                    ret[i]["name"] = $(tag).find('#name').text();
                }
                i++;
            });
            parent.<?php echo $retFunc; ?>(ret);
            parent.$.fancybox.close();
        }

        function ckAll(tag) {
            $("#tbList tbody tr :checkbox").each(function() {
                if ($(tag).is(":checked")) {
                    $(this).attr('checked', true);
                } else {
                    $(this).attr('checked', false);
                }
            });
        }
    </script>   
</body>
</html>