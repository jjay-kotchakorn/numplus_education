<?php
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";

global $db;

$code = trim($_POST["code"]);
$dup = 0;
$id = "";
if($code && $_POST["active"]!='F'){
	$id = $_POST["course_id"];
	$con = ($id) ? " and course_id != $id" : "";
	$q = "select count(course_id) as c from course where code='$code' $con and active!='F'";
	$rs = $db->data($q);
	if($rs>0) $dup = 1;
}
if(!$code || $dup==1){
	$args = array();
	$args["p"] = "course";
	$args["course_id"] = $id;
	$args["type"] = "info";
	$_SESSION["error"]["msg"] = "รหัส $code ซ้ำ";
	redirect_url($args);
}

if($_POST){
	$args = array();
	$args["table"] = "course";
	if($_POST["course_id"])
	   $args["id"] = $_POST["course_id"];
	else
		$args["createtime"] = date("Y-m-d H:i:s");
	$args["code"] = $_POST["code"];
	$args["unit_time"] = $_POST["unit_time"];
	//$args["unit_time_fa"] = $_POST["unit_time_fa"];
	$args["code_project"] = trim($_POST["code_project"]);
	$args["coursetype_id"] = (int)$_POST["coursetype_id"];
	$args["section_id"] = (int)$_POST["section_id"];
	$args["title"] = $_POST["title"];
	$args["detail"] = $_POST["detail"];
	$args["life_time"] = (int)$_POST["life_time"];
	$args["set_time"] = (float)$_POST["set_time"];
	//$args["set_time_fa"] = (float)$_POST["set_time_fa"];
	$args["price"] = (int) $_POST["price"];
	$args["status"] = 1;
	$args["inhouse"] = ($_POST["inhouse"]=="T") ? "T" : "F";
	$args["rereg_date"] = (int)$_POST["rereg_date"];
	$args["discount"] = (float)$_POST["discount"];
	$args["date_start"] = thai_to_timestamp($_POST["date_start"]);
	$args["date_stop"] = thai_to_timestamp($_POST["date_stop"]);
	$args["date_other"] = $_POST["date_other"];
	$args["active"] = $_POST["active"];
	$args["recby_id"] = (int)$EMPID;
	$args["rectime"] = date("Y-m-d H:i:s");
	
   $ret = $db->set($args);
   $course_id = $args["id"] ? $args["id"] : $ret;
	if(is_array($_POST["ckCourseDetail"])){
		foreach($_POST["ckCourseDetail"] as $index=>$v){
			$args = array();
			$args["table"] = "course_detail_list";
			if($_POST["course_detail_list_id"][$index])
				$args["id"] = $_POST["course_detail_list_id"][$index];
			else				
				$args["course_id"] = (int)$course_id;
			$args["course_detail_id"] = (int)$_POST["course_detail_id"][$index];
			$args["recby_id"] = (int)$EMPID;
			$args["rectime"] = date("Y-m-d H:i:s");
			if(!$args["id"] && $v=="F") continue;
			if($args["id"] && $v=="F"){
				$id = $args["id"];
				$q = "select course_detail_list_id from course_detail_list where course_detail_list_id=$id";
				$testId = $db->data($q);
				if($testId){
					$db->query("delete from course_detail_list where course_detail_list_id=$testId");
					continue;
				}
			}
			$db->set($args);
		}	
	}
}
$_SESSION["success"]["msg"] = "บันทึกข้อมูลเรียบร้อยแล้ว";

$args = array();
$args["p"] = "course";
$args["course_id"] = $course_id;
$args["type"] = "info";
redirect_url($args);
?>