<?php
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";
include_once "./share/member.php";
global $db;

if ($_GET) {
	$member_id = $_GET['member_id'];
	$cid = $_GET['cid'];

	$qry = "UPDATE member SET password='$cid' WHERE member_id=$member_id";
	$rs = $db->set_update($qry);
	if ($rs) {
		$ret = $member_id;
		/*$args = array();
		$args["reset_passwd"] = "T";
		redirect_url($args);*/
		/*header( "location: index.php?p=mamber&reset-password-success" );
 		exit(0);*/
 		header( "refresh: 0; url=index.php?p=mamber&reset-password-success" );
 		exit(0);
	}
	//var_dump($res);
}
//die();

if ( !empty($_POST) && $_POST["member_quick_edit"]=="T" ) {
	// d($_POST);
	$member_id = $_POST["member_id"];

	if ($_POST["title_th"] != "อื่นๆ") {
		$_POST["title_th_text"] = NULL;
	}//end if
	if ($_POST["title_en"] != "Other") {
		$_POST["title_en_text"] = NULL;
	}//end if

	$args = array();
	$args["table"] = "member";
	$args["id"] = $member_id;
    $args["nation"] = $_POST["nation"];
    $args["cid"] = $_POST["cid"];
    $args["title_th"] = $_POST["title_th"];
    $args["title_th_text"] = $_POST["title_th_text"];
    $args["fname_th"] = $_POST["fname_th"];
    $args["lname_th"] = $_POST["lname_th"];
    $args["title_en"] = $_POST["title_en"];
    $args["title_en_text"] = $_POST["title_en_text"];
    $args["fname_en"] = $_POST["fname_en"];
    $args["lname_en"] = $_POST["lname_en"];
    // $args["birthdate"] = 30-10-2560
    $args["password"] = $_POST["password"];
    $args["activelogin"] = $_POST["activelogin"];
    $db->set($args);

    $_SESSION["member_quick_edit"]["alert"]["status"] = "success";
    $_SESSION["member_quick_edit"]["alert"]["msg"] = "อัพเดทข้อมูลเรียบร้อย";

    $args = array();
	$args["member_id"] = $member_id;
	$args["type"] = "member-quick-edit";
	redirect_url($args);
    // d($args);
}//end if

if($_POST){
	$id = $_POST["member_id"];

	//d($_POST);
	if ($_POST["reset_passwd"] == "T") {
		$_POST["password"] = trim($_POST["cid"]);
	}else{
		$_POST["password"] = trim($_POST["password"]);
	}

	if ($_POST["title_th"] != "อื่นๆ") {
		$_POST["title_th_text"] = NULL;
	}
	if ($_POST["title_en"] != "Other") {
		$_POST["title_en_text"] = NULL;
	}
	/*if ($_POST["nation"]=='T') {
		$_POST["nation"] = 'F';
	}else{
		$_POST["nation"] = 'T';
	}*/

	$db->begin();
    $args = array();
	$args = array('table' => 'member',
			  'username' => $_POST["cid"],
			  'password' => $_POST["password"],
			  'title_th' => $_POST["title_th"],
			  'fname_th'  => $_POST["fname_th"],
			  'lname_th'  => $_POST["lname_th"],
			  'title_en' => $_POST["title_en"],
			  'fname_en'  => $_POST["fname_en"],
			  'lname_en'  => $_POST["lname_en"],
			  'email1'  => $_POST["email1"],
			  'email2'  => $_POST["email2"],
			  'slip_type'  => $_POST["slip_type"],
			  'slip_name' => $_POST["slip_name"],
			  'slip_address1'  => $_POST["slip_address1"],
			  'slip_address2'  => $_POST["slip_address2"],
			  'get_news1'  => $_POST["get_news1"],
			  'cid' => $_POST["cid"],
			  //'reg_date' => date("Y-m-d H:i:s"),
			  'gender' => $_POST["gender"],
			  'birthdate' => thai_to_timestamp($_POST["birthdate"]),
			  'no'  => $_POST["no"],
			  'gno'  => $_POST["gno"],
			  'moo'  => $_POST["moo"],
			  'soi'  => $_POST["soi"],
			  'road'  => $_POST["road"],
			  'district_id' => $_POST["district_id"],
			  'amphur_id'  => $_POST["amphur_id"],
			  'province_id'   => $_POST["province_id"],
			  'postcode'   => $_POST["postcode"],
			  'tel_home' => $_POST["tel_home"],
			  'tel_mobile' => $_POST["tel_mobile"],
			  'tel_office' => $_POST["tel_office"],
			  'tel_office_ext' => $_POST["tel_office_ext"],
			  'tel_other' => $_POST["tel_other"],
			  'receipt_id'   => (int)$_POST["receipt_id"],
			  'receipttype_id'   => (int)$_POST["receipttype_id"],
			  'receipttype_text'   => $_POST["receipttype_text"],
			  'receipt_title' => $_POST["receipt_title"],
			  'title_th_text' => $_POST["title_th_text"],
			  'title_en_text' => $_POST["title_en_text"],
			  'receipt_fname'  => $_POST["receipt_fname"],
			  'receipt_lname'  => $_POST["receipt_lname"],
			  'receipt_no'  => $_POST["receipt_no"],
			  'receipt_gno'  => $_POST["receipt_gno"],
			  'receipt_moo'  => $_POST["receipt_moo"],
			  'receipt_soi'  => $_POST["receipt_soi"],
			  'receipt_road'  => $_POST["receipt_road"],
			  'receipt_district_id'   => (int)$_POST["receipt_district_id"],
			  'receipt_amphur_id'   => (int)$_POST["receipt_amphur_id"],
			  'receipt_province_id'   => (int)$_POST["receipt_province_id"],
			  'receipt_postcode'   => $_POST["receipt_postcode"],
			  'receipt_tel_home' => $_POST["receipt_tel_home"],
			  'receipt_tel_mobile' => $_POST["receipt_tel_mobile"],
			  'receipt_tel_office' => $_POST["receipt_tel_office"],
			  'receipt_tel_other' => $_POST["receipt_tel_other"],
			  'org_type'  => $_POST["orgtype_id"],
			  'org_name'  => $_POST["org_name"],
			  'org_lv'  => $_POST["orgtype_text"],
			  'org_position'  => $_POST["org_position"],
			  'grd_lv1'  => $_POST["grd_lv1"],
			  'grd_yr1'   => $_POST["grd_yr1"],
			  'grd_ugrp1'  => $_POST["grd_ugrp1"],
			  'grd_uid1'   => $_POST["grd_uid1"],
			  'grd_uname1'  => $_POST["grd_uname1"],
			  'grd_major1'  => $_POST["grd_major1"],
			  'activelogin'  => $_POST["activelogin"],
			  'rectime'  => date("Y-m-d H:i:s"),
			  'branch'  => $_POST["branch"],
			  'taxno'   => $_POST["taxno"],
			  'grd_other' => $_POST["grd_other"],
			  'get_news1' => $_POST["get_news1"],
			  'nation' => $_POST["nation"]
			  
	);
	if($id){
		$args['id'] = array('key'=>'member_id', 'value'=>$id);
	}else{
		$args['activate_code'] = date("Y-m-d H:i:s");
	}

	$ret = $db->set($args);
	$ret = ($id) ? $id : $ret; 
	$db->commit();
	
}
$args = array();
$args["update_state"] = "T";
$args["member_id"] = $ret;
$args["type"] = "info";
redirect_url($args);
?>