<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
global $db;
$news_id = $_GET["news_id"];
$section = datatype(" and a.active='T'", "section", true);
$newstype = datatype(" and a.active='T'", "newstype", true);
$q = "select  name from news where news_id=$news_id";
$r = $db->rows($q);
$str = "";
if($r){
	$str = $r["name"];
}else{
	$str = "เพิ่มข่าวสาร";
}
?>
<link rel="stylesheet" type="text/css" href="js/bootstrap.summernote/dist/summernote.css" />
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="row">
				  <div class="col-md-12">			      
					<div class="block-flat">
					  <div class="header">							
					  	<ol class="breadcrumb">
							<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>');">หน้าหลัก</a></li>
							<li class="active"><?php echo $str; ?></li>
						</ol>
					  </div>
					  <div class="content">
						  <form id="frmMain" name="frmMain" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="update-news.php">
						  <input type="hidden" name="news_id" id="news_id" value="<?php echo $news_id; ?>">
						    <input type="hidden" name="tmpimg" />
  							<input type="hidden" name="delimg" />
						  <div class="col-sm-12">
							  <div class="form-group row">
								<label class="col-sm-1 control-label">รหัส</label>
								<div class="col-sm-2">
								  <input class="form-control" id="code" name="code" required="" placeholder="รหัส" type="text">
								</div>
								<label class="col-sm-2 control-label">ประเภทใบอนุญาต/คุณวุฒิ <span class="red">*</span></label>
								<div class="col-sm-3">
								    <select name="section_id" id="section_id" class="form-control required">
										<option value="">---- เลือก ----</option>
										<?php foreach ($section as $key => $value) {
											$id = $value['section_id'];
											$name = $value['name'];
											echo  "<option value='$id'>$name</option>";
										} ?>

									</select>
								</div>								
								<label class="col-sm-2 control-label">หมวดหมู่ข่าวสาร<span class="red">*</span></label>
								<div class="col-sm-2">
								    <select name="newstype_id" id="newstype_id" class="form-control required">
										<option value="">---- เลือก ----</option>
										<?php foreach ($newstype as $key => $value) {
											$id = $value['newstype_id'];
											$name = $value['name'];
											echo  "<option value='$id'>$name</option>";
										} ?>

									</select>
								</div>
								</div>				                
								<div class="form-group row">	                			                
								<label class="col-sm-1 control-label">หัวข้อ</label>
								<div class="col-sm-7">
								  <input class="form-control" name="name" id="name" required="" placeholder="หัวข้อข่าวสาร" type="text">
								</div>			                			                
								<label class="col-sm-2 control-label">ไฮไลท์</label>
								<div class="col-sm-2">
								  	<select name="highlight" id="highlight" class="form-control required">
									  <option selected="selected" value="T">Enable</option>
									  <option value="F">Disable</option>
									</select>
								</div>
							  </div>				                
								<div class="form-group row">	                			                
								<label class="col-sm-1 control-label">ลิงค์</label>
								<div class="col-sm-7">
								  <input class="form-control" name="link_to" id="link_to"  placeholder="ลิงค์ไปเว็บอื่น" type="text">
								</div>			                			                
									<label class="col-sm-2 control-label">สถานะ</label>
									<div class="col-sm-2">
										<select name="active" id="active" class="form-control required">
										  <option selected="selected" value="T">แสดง</option>
										  <option value="F">ไม่แสดง</option>
										</select>
									</div>
							  </div>
							  <div class="form-group row">
							  <label class="col-sm-1 control-label">รายละเอียด</label>
							  <div class="clear"></div><br>
							  	<div class="col-sm-12">
							  		<textarea id="detail" class="summernote" name="detail" placeholder="Description"></textarea>
							  	</div>
							  </div>
						  </div>
						  <div class="clear"></div>
						  <div class="form-group row" style="padding-left:10px;">
								<div class="col-sm-12">
									<button type="button" class="btn btn-primary" onClick="ckSave()">Save changes</button>
									<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p'] ?>');">Cancel</button>
								</div>
						  </div>
						</form>
					  </div>
					</div>
					
				  </div>
				</div>

		</div>
	</div> 
</div>
<?php include_once ('inc/js-script.php'); ?>

<script type="text/javascript" src="js/bootstrap.summernote/dist/summernote.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
   $("#frmMain").validate();
   var news_id = "<?php echo $_GET["news_id"]?>";
   if(news_id) viewInfo(news_id);
   $('#detail').summernote({
            height: 200,
            onImageUpload: function(files, editor, welEditable) {
                sendFile(files[0], editor, welEditable);
            }
        }); 
        function sendFile(file, editor, welEditable) {
            data = new FormData();
            data.append("file", file);//You can append as many data as you want. Check mozilla docs for this
            $.ajax({
                data: data,
                type: "POST",
                url: 'data/upload-img.php',
                cache: false,
                contentType: false,
                processData: false,
                success: function(url) {
                	var url = "newsImg/"+url;
                    editor.insertImage(welEditable, url);
                }
            });
        }

        
 });
 function viewInfo(news_id){
   if(typeof news_id=="undefined") return;
   getnewsInfo(news_id);
}

function getnewsInfo(id){
	if(typeof id=="undefined") return;
	var url = "data/newslist.php";
	var param = "news_id="+id+"&single=T";
	dataUrl(url, param,"#frmMain");
}


function ckSave(id){
  onCkForm("#frmMain");
 var tmp = $('#detail').code();
 $('#detail').val(tmp);
  $("#frmMain").submit();
}	
</script>