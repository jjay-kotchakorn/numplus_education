<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
global $db, $SECTIONID, $COURSETYPEID;

$con_section_id = ($SECTIONID>0) ? " and a.section_id={$SECTIONID}" : "";
$con_coursetype_id = ($COURSETYPEID>0) ? " and a.coursetype_id={$COURSETYPEID}" : "";
$section = datatype(" and a.active='T' {$con_section_id}", "section", true);
if($con_coursetype_id)
	$coursetype = datatype(" and a.active='T' {$con_coursetype_id}", "coursetype", true);
$type = $_GET["type"];
$str = "ตรวจสอบสถานะการชำระ";
if($type=="course_order") $str = "จัดเรียง หลักสูตร/วิชา";
$apay = datatype(" and a.active='T'", "pay_status", true);
$arr_pay = array();
foreach ($apay as $key => $value) {
	$arr_pay[$value["pay_status_id"]] = $value["name"];
}
?>
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="col-sm-12">
				<div class="content block-flat ">
					<div class="page-head">
						<button id="add" class="btn btn-success btn-small pull-right" onclick="addnew()" style="margin-top:10px;display:none;"><i class="fa fa-plus"></i> เพิ่มการลงทะเบียน</button>
						<h3><i class="fa fa-list"></i> &nbsp; <?php echo $str; ?></h3>
					</div>
						<div class="header">
							<div class="form-group row">
								<label class="col-sm-2 control-label">ประเภทใบอนุญาต/คุณวุฒิ</label>
								<div class="col-sm-3">
									<select name="section_id" id="section_id" class="form-control" onchange="reCall();">
										<option value="">---- เลือก ----</option>
										<?php foreach ($section as $key => $value) {
											$id = $value['section_id'];
											if($SECTIONID>0 && $SECTIONID!=$id) continue;
											if($SECTIONID>0 && $SECTIONID==$id){
												$select = "selected";
											}else{
												$select = "";
											}
											$name = $value['name'];
											echo  "<option {$select} value='$id'>$name</option>";
										} ?>

									</select>
								</div>                
								<label class="col-sm-2 control-label">ประเภทหลักสูตร</label>
								<div class="col-sm-2">
									<select name="coursetype_id" id="coursetype_id" class="form-control" onchange="reCall();">
										<option value="">---- เลือก ----</option>
										<?php foreach ($coursetype as $key => $value) {
											$id = $value['coursetype_id'];
											if($COURSETYPEID>0 && $COURSETYPEID==$id){
												$select = "selected";
											}else{
												$select = "";
											}
											$name = $value['name'];
											echo  "<option {$select} value='$id'>$name</option>";
										} ?>

									</select>
								</div>
								<label class="col-sm-1 control-label" style=" padding-right:0px;">ช่องทางชำระ</label>
								<div class="col-sm-2">
									<select name="sPay" id="sPay" class="form-control" onchange="reCall();">
										<option selected="selected" value="">แสดงทั้งหมด</option>
										<option value="paysbuy">paysbuy</option>
										<option value="bill_payment">Bill-payment</option>
										<option value="at_asco">เช็ค/เงินโอน</option>
										<option value="at_ati">ชำระเงินสดผ่าน ATI</option>
										<option value="mpay">mPAY</option>
									</select>
								</div>   
							</div> 							
							<div class="form-group row">
								<label class="col-sm-2 control-label">เวลา/สถานที่</label>
								<div class="col-sm-10">
									<div class="input-group">
										<input class="form-control" type="text" id="course_detail_name" readonly="true">
										<input name="course_detail_id" type="hidden" id="course_detail_id" value="" />
										<span class="input-group-btn">
											<button class="btn btn-primary" type="button" onclick="addcourse_detail_other();">...</button>
										</span>
									</div>				
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-1 control-label" style=" padding-right:0px;">สถานะ<br>การชำระเงิน </label>
								<div class="col-sm-2">
									<select name="sStatus" id="sStatus" class="form-control" onchange="reCall();">
										<option value="">แสดงทั้งหมด</option>
										<?php foreach ($apay as $key => $value) {
											$id = $value['pay_status_id'];
											$s = "";
											if($info["pay_status"]==$id){
												$s = "selected";
											}
											$name = $value['name'];
											echo  "<option value='$id' {$s}>$name</option>";
										} ?>
									</select>
								</div>
								<label class="col-sm-1 control-label">ชื่อ-นามสกุล</label>
								<div class="col-sm-2">
									<div class="input-group">
										<input type="text" class="form-control" id="membername" name="membername" readonly="true" value="">
										<input name="member_id" type="hidden" id="member_id" value="" />
										<span class="input-group-btn">
											<button class="btn btn-primary" type="button" onClick="selectMember();">...</button>
										</span>
									</div>
									
								</div>
								<label class="col-sm-1 control-label"  style=" padding-right:0px;">วันที่เริ่ม</label>
								<div class="col-sm-1"  style="padding-left:0px; padding-right:0px;">
									<input class="form-control" name="date_start" value="<?php echo revert_date(date("Y-m-d")); ?>" id="date_start" onblur="reCall();" placeholder="วันที่เริ่ม" type="text">
								</div>                                          
								<label class="col-sm-1 control-label"  style=" padding-right:0px;">วันที่สิ้นสุด</label>
								<div class="col-sm-1"  style="padding-left:0px; padding-right:0px;">
									<input class="form-control" name="date_stop" id="date_stop" onblur="reCall();" placeholder="วันที่สิ้นสุด" type="text">
								</div> 
								
								<label class="col-sm-2 control-label">
									<a href="#" class="btn" onclick="reCall();"><i class="fa fa-search">&nbsp;</i></a>&nbsp;&nbsp;
									<!-- <button onclick="clearSearch();" id="ok" class="btn btn-success" type="button">Clear</button> -->
								</label>                                            
							</div>
							
						</div>
					<table id="tbCourse" class="table" style="width:100%">
						  <thead>
							  <tr>
								  <th style="text-align:center" width="4%">ลำดับ</th>
								  <th style="text-align:center" width="4%">ID</th>
								  <th style="text-align:center" width="10%">วันที่สมัคร</th>
								  <th style="text-align:center" width="10%">วันที่หมดเขต</th>
								  <th width="9%">รหัสประจำตัวประชาชน</th>
								  <th width="10%">ชื่อ-นามสกุล</th>
								  <th style="text-align:center" width="6%">วัน</th>
								  <th style="text-align:center" width="6%">เวลา</th>
								  <th style="text-align:center" width="10%">สถานที่</th>
								  <th style="text-align:center" width="10%">หลักสูตร</th>
								  <th style="text-align:center" width="10%">สถานะการชำระเงิน</th>
								  <th style="text-align:center" width="10%">Manage</th>
							  </tr>
						  </thead>   
						<tbody>
						</tbody>
					</table>
					<div class="clear"></div>
					<br>
					<div class="filters">     
						<div class="btn-group" style="display:none;">
							<button class="btn btn-success btn-small pull-right" type="button" onclick="update_pay_status();">
								อัพเดทสถานะการชำระเงิน
							</button>
						</div>

					</div>
					<div class="clear"></div>
				</div>
			</div>

		</div>
	</div> 
</div>
<!-- Modal -->
<div class="modal fade" id="model-sync" tabindex="-1" role="dialog">
<div class="modal-dialog">
  <div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	</div>
	<div class="modal-body">
		<div class="text-center">			
			<h4 id="remark_header"></h4>
			<p><textarea name="remark" id="remark" cols="30" rows="5"></textarea></p>
			<input type="hidden" name="remark_id" id="remark_id">			
			<input type="hidden" name="remark_status" id="remark_status">
			<a class="btn btn-primary" title="บันทึก" onclick="saveInfo('remark')"><i class="fa fa-save"> บันทึกข้อมูล</i></a>			
		</div>
	</div>
	<div class="modal-footer">
	</div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php include ('inc/js-script.php') ?>

<script type="text/javascript">
$(document).ready(function() {
	var get_type = "<?php echo $_GET["type"]; ?>";
	if(get_type=="childlist" || get_type=="course_order") $("#add").hide();
	var oTable;
	listItem();	
    $("#date_start").datepicker({language:'th-th',format:'dd-mm-yyyy'});
    $("#date_stop").datepicker({language:'th-th',format:'dd-mm-yyyy'});
	$("#section_id").change(function(event) {
		var id = $(this).val();
		getDropDown('#coursetype_id', id, 'coursetype', 'data/coursetype.php')
	});  
});

function listItem(){
   var get_type = "<?php echo $_GET["type"]; ?>";
   var url = "data/registerlist.php";
   oTable = $("#tbCourse").dataTable({
	   "sDom": 'T<"clear">lfrtip',
	   "oLanguage": {
   	   "sInfoEmpty": "",
   		"sInfoFiltered": ""
						  },
		"oTableTools": {
			"aButtons":  ""
		},
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": url,
		"sPaginationType": "full_numbers",
		"aaSorting": [[ 0, "desc" ]],
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			aoData.push({"name":"section_id","value":$("#section_id").val()});			
			aoData.push({"name":"coursetype_id","value":$("#coursetype_id").val()});			
			aoData.push({"name":"member_id","value":$("#member_id").val()});			
			aoData.push({"name":"course_detail_id","value":$("#course_detail_id").val()});			
			aoData.push({"name":"status","value":$("#sStatus").val()});
			aoData.push({"name":"sPay","value":$("#sPay").val()});				
			aoData.push({"name":"active","value":"T"});			
			aoData.push({"name":"type","value":get_type});
			aoData.push({"name":"date_start","value":$("#date_start").val()});			
			aoData.push({"name":"date_stop","value":$("#date_stop").val()});	
			$.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});
		}
   }); 
   $('#tbCourse_filter input').unbind();
   $('#tbCourse_filter input').bind('keyup', function(e) {
       if(e.keyCode == 13) {
        oTable.fnFilter(this.value);   
    }
   });
}

function editInfo(id){
	if(typeof id=="undefined") return;
   var url = "index.php?p=<?php echo $_GET["p"];?>&register_id="+id+"&type=info";
   redirect(url);
}


function childlist(id){
	if(typeof id=="undefined") return;
   var url = "index.php?p=<?php echo $_GET["p"];?>&register_id="+id+"&type=childdetail";
   redirect(url);
}

function addnew(){
   var url = "index.php?p=<?php echo $_GET["p"];?>&type=select-course";
   redirect(url);
}

function reCall(){
	oTable.fnClearTable( 0 );
	oTable.fnDraw();
}

function selectMember(){
   var url = "member map";
   memberpop(url,"ret_member_select");	
}

function ret_member_select(id){
	for(var i in id){
		var ck = "";
		var data = id[i];
        $("#member_id").val(data["member_id"]);
        $("#membername").val(data.name_th);
        reCall();
	}
}

function addcourse_detail_other(){
   var url = "course_detail map";
   courseDetailData(url,"ret_detail_other");	
}

function ret_detail_other(id){
	for(var i in id){
		var ck = "";
		var data = id[i];
		$("#course_detail_id").val(data.course_detail_id);
		console.log(id);
		var str = data.date+" เวลา :  "+data.time+" สถานที่ : "+data.address_detail+" "+data.address+ " จำนวนที่นั่ง : "+data.chair_all;
		$("#course_detail_name").val(str);
		reCall();
	}
}

function clearSearch(){
 $("#course_detail_name").val("");
 $("#course_detail_id").val("");
 $("#member_id").val("");
 $("#membername").val("");
 reCall();
}

function update_pay_status(){
	var t = confirm("update  สถานะ ชำระเงินแล้ว ? ");
	if(!t) return false;
    var i = 0;
    var str = "";
    $("#tbCourse tbody tr :checkbox").each(function() {
        if ($(this).is(":checked")) {
            var id = $(this).val();
            str += id+",";
        }
        i++;
    });
   
  if(str!=""){
	$.ajax({
		"type": "POST",
		"async": false, 
		"url": "data/register-status-update.php",
		"data": {'register_id': str}, 
		"success": function(data){	
			$.gritter.removeAll({
		        after_close: function(){
		          $.gritter.add({
		          	position: 'center',
			        title: 'Success',
			        text: data,
			        class_name: 'success'
			      });
		        }
		      });
 			reCall();				      							 
		}
	});
  }else{
    $("input[name=menu_order_"+id+"]").val(tmp);
  }

}


function escapeHtml(unsafe) {
    return unsafe
         .replace(/&/g, "&amp;")
         .replace(/</g, "&lt;")
         .replace(/>/g, "&gt;")
         .replace(/"/g, "&quot;")
         .replace(/'/g, "&#039;");
 }


function saveInfo(id){
	if(typeof id=="undefined") return;
	if(id=="remark"){
		var remark = $("#remark").val();
		if($.trim(remark)==""){
			alert("กรุณาใส่เหตุผล");
			return;
		}else{
			$('#model-sync').modal('hide');
		}
		var status = $("#remark_status").val();
		var id = $("#remark_id").val();
		if(id=="" || status=="") return;
		$("#remark_header").html('');
		$("#remark_status").val('');
		$("#remark_id").val('');
	}else{
		//var t = confirm("ยืนยัน update  สถานะ การชำระเงิน ? ");
		//if(!t) return false;		
		var status = $("#pay_status"+id).val();
		if(status==7 || status==8){
			var title = "เหตุผลในการ";
			title += (status==8) ? 'ยกเลิก' : 'คืนเงิน';
			$("#remark_header").html(title);
			$("#remark_status").val(status);
			$("#remark_id").val(id);
			$('#model-sync').modal('show');
			return;
		}
		var remark = "";
	}

    var i = 0;
	if(status>0){
		$.ajax({
			"type": "POST",
			"async": false, 
			"url": "data/register-status-update.php",
			"data": {'register_id': id, 'status' : status, 'remark' : remark}, 
			"success": function(data){	
				console.log(data);
				$.gritter.removeAll({
			        after_close: function(){
			          $.gritter.add({
			          	position: 'center',
				        title: 'Success',
				        text: data,
				        class_name: 'success'
				      });
			        }
			      });
					reCall();				      							 
			}
		});

		$.ajax({
			"type": "POST",
			"async": false, 
			"url": "check-course-elearning.php",
			"data": {'register_id': id, 'pay_status': status}, 
			"success": function(data){	
				//console.log(data);			      							 
			}
		});
		
	}//end if
}//end func

function printInfo(id){
	var url = "receipt-print.php?register_id="+id;
   	window.open(url,'_blank');
}

function print_credit_note(id){
	var url = "receipt-print-credit-note.php?register_id="+id;
   	window.open(url,'_blank');
}

</script>