<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
global $db, $SECTIONID, $COURSETYPEID;

$error = $_SESSION["error"]["msg"];
unset($_SESSION["error"]["msg"]);
$success = $_SESSION["success"]["msg"];
unset($_SESSION["success"]["msg"]);

$course_id = $_GET["course_id"];

$con_section_id = ($SECTIONID>0) ? " and a.section_id={$SECTIONID}" : "";
$con_coursetype_id = ($COURSETYPEID>0) ? " and a.coursetype_id={$COURSETYPEID}" : "";
$section = datatype(" and a.active='T' {$con_section_id}", "section", true);
//if($con_coursetype_id)
$coursetype = datatype(" and a.active='T' {$con_coursetype_id}", "coursetype", true);

//$q = "select  title from course where course_id=$course_id";
$q = "select c.course_id
	, c.section_id
	, c.coursetype_id
	, ct.name cty_name
	, s.name sec_name
	from course c
	left join coursetype ct on ct.coursetype_id = c.coursetype_id
	left join section s on s.section_id = c.section_id
	where c.course_id=$course_id";
$r = $db->rows($q);
$str = "";
if($r){
	$str = $r["sec_name"];
}else{
	$str = "เพิ่มหลักสูตร";
}
?>
<link rel="stylesheet" type="text/css" href="js/bootstrap.summernote/dist/summernote.css" />
<link rel="stylesheet" type="text/css" href="js/jquery.nanoscroller/nanoscroller.css" />
<link href="js/jquery.icheck/skins/square/blue.css" rel="stylesheet">
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="row">
					<div class="col-md-12">           
					<div class="block-flat">
						<div class="header">              
						<ol class="breadcrumb">
							<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>');">หน้าหลัก</a></li>
							<li class="active"><?php echo $str; ?></li>
						</ol>
						</div>
						<div class="content">
							<form id="frmMain" name="frmMain" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="course-add-new-child.php">
							<input type="hidden" name="course_id" id="course_id" value="<?php echo $course_id; ?>">
							<div class="col-sm-12">
								<div class="form-group row">
								<label class="col-sm-1 control-label" style="width:118px;">รหัสหลักสูตร <span class="red">*</span></label>
								<div class="col-sm-2" style="width:150px;">
									<input class="form-control" id="code" name="code" required="" placeholder="รหัสหลักสูตร" type="text">
								</div>
								<label class="col-sm-2 control-label">ประเภทใบอนุญาต/คุณวุฒิ <span class="red">*</span></label>
								<div class="col-sm-3">
									<select name="section_id" id="section_id" class="form-control ">
										<option value='<?php echo $r["section_id"];?>'><?php echo $r["sec_name"];?></option>
									</select>
								</div>                
								<label class="col-sm-2 control-label">ประเภทหลักสูตร<span class="red">*</span></label>
								<div class="col-sm-2">
									<select name="coursetype_id" id="coursetype_id" class="form-control ">
										<option value='<?php echo $r["coursetype_id"];?>'><?php echo $r["cty_name"];?></option>
									</select>
								</div>
								</div>                        
								<div class="form-group row"> 
								<label class="col-sm-1 control-label"  style="width:115px;">รหัสโครงการ </label>
								<div class="col-sm-2" style="width:150px;">
									<input class="form-control" id="code_project" name="code_project"  placeholder="รหัสโครงการ" type="text">
								</div>                                       
									<label class="col-sm-1 control-label" style="padding-right:10px;" >ชื่อหลักสูตร <span class="red" style="display:inline-block; position:absolute;"> &nbsp;*</span></label>
									<div class="col-sm-4">
										<input class="form-control" name="title" id="title" required="" placeholder="ชื่อหลักสูตร" type="text">
									</div>                                            
									<label class="col-sm-2 control-label">ใบอนุญาต (ปี)</label>
									<div class="col-sm-2">
										<input class="form-control number" name="life_time" id="life_time"  placeholder="ใบอนุญาต (ปี)" type="text">
									</div>
								</div> 
								<div class="form-group row">
								  <label class="col-sm-1 control-label">รายละเอียด</label>
								  <div class="clear"></div><br>
								  	<div class="col-sm-12">
								  		<textarea id="detail" class="summernote" name="detail" placeholder="รายละเอียด"></textarea>
								  	</div>
							  </div>                       
							<div class="form-group row">                                        
								<label class="col-sm-1 control-label">ระยะเวลา <span class="red" >*</span></label>
								<div class="col-sm-3">
									<input class="form-control required number" name="set_time" id="set_time" style="width:95px; display:inline-block;"  placeholder="เวลา" type="text">
									<select name="unit_time" id="unit_time" class="form-control required" style="width:100px; display:inline-block;" >
										<option selected="selected" value=""> - </option>
										<option value="นาที">นาที</option>
										<option value="ชั่วโมง">ชั่วโมง</option>
										<option value="วัน">วัน</option>
									</select>
								</div>                                          
								<label class="col-sm-1 control-label" style="padding-left: 5px; padding-right: 5px;">ราคา (บาท) <span class="red">*</span></label>
								<div class="col-sm-2">
									<input class="form-control required number" name="price" id="price"  placeholder="ราคา (บาท)" type="text">
								</div> 
								 <label class="checkbox-inline" style="padding-left:10px;padding-top:0px"> <div aria-disabled="false" aria-checked="false" style="position: relative;" class="icheckbox_square-blue"><input style="position: absolute; opacity: 0;" value="T" name="inhouse" id="inhouse" class="icheck" type="checkbox"><ins style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;" class="iCheck-helper"></ins></div>&nbsp;&nbsp;<a data-toggle="tooltip" href="#" data-original-title="กรณีเป็นหลักสูตร In House  admin สามารถตั้งราคาของหลักสูตรได้เอง">In House</a></label>                                         
								<label style="display:none;" class="col-sm-2 control-label"><a data-toggle="tooltip" href="#" data-original-title="กรณีไม่ผ่าน สามารถสมัครใหม่หลังวันสอบ x วัน">สมัครใหม่หลังวันสอบ</a></label>
								<div class="col-sm-2" style="display:none;">
									<input class="form-control number" name="rereg_date" id="rereg_date"  placeholder="จำนวนวัน" type="text">
								</div> 
								 

							</div>      
<!-- 
							<div class="form-group row">                                       
								<label class="col-sm-4 control-label">จำนวนชั่วโมงสะสม สำหรับหลักสูตร FA </label>
								<div class="col-sm-4">
									<input class="form-control number" name="set_time_fa" id="set_time_fa" style="width:100px; display:inline-block;"  type="text">
									<select name="unit_time_fa" id="unit_time_fa" class="form-control" style="width:100px; display:inline-block;" >
										<option selected="selected" value=""> - </option>
										<option value="นาที">นาที</option>
										<option value="ชั่วโมง">ชั่วโมง</option>
										<option value="วัน">วัน</option>
									</select>
								</div> 				                                   
							</div>
 -->
								<div class="form-group row">
								<label class="col-sm-2 control-label">วันที่เริ่มส่วนลด </label>
								<div class="col-sm-2">
									<input class="form-control " name="date_start" id="date_start"  placeholder="วันที่" type="text">
								</div>  
								<label class="col-sm-2 control-label">ถึงวันที่สิ้นสุดส่วนลด</label>
								<div class="col-sm-2">
									<input class="form-control " name="date_stop" id="date_stop"  placeholder="ถึงวันที่" type="text">
								</div>

								<label class="col-sm-1 control-label">ส่วนลด</label>
								<div class="col-sm-2">
									<input class="form-control number" name="discount" id="discount"  placeholder="ส่วนลด" type="text">
								</div>                                          
							</div>
								<div class="form-group row"> 
									<label class="col-sm-1 control-label">แสดงที่ Front / Back <span class="red">*</span></label>
									<div class="col-sm-2">
										<select name="active" id="active" class="form-control required">
											<option selected="selected" value="T">แสดง</option>
											<option value="F">ไม่แสดง</option>
										</select>
									</div>
								</div>

								</div>
							</div>
							<div class="clear"></div>
							<div class="form-group row" style="padding-left:10px;">
								<div class="col-sm-12">
									<button type="button" class="btn btn-primary" onClick="ckSave()">Save changes</button>
									<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p'] ?>');">Cancel</button>
								</div>
							</div>
						</form>
						</div>
					</div>
					
					</div>
				</div>

		</div>
	</div> 
</div>
<?php include_once ('inc/js-script.php'); ?>
<script type="text/javascript" src="js/bootstrap.summernote/dist/summernote.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
	var nowDate = new Date();
	var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);		
	 $("#frmMain").validate();
	 var course_id = "<?php echo $_GET["course_id"]?>";
	 if(course_id) viewInfo(course_id);
	 $('#detail').summernote({height: 80});
	 $("#date_start").datepicker({language:'th-th',format:'dd-mm-yyyy'/*,startDate: today*/ });
	 $("#date_stop").datepicker({
	 	language:'th-th',
	 	format:'dd-mm-yyyy'/*,
	 	startDate: today */
	 });
     var error = "<?php echo $error ?>";
     if(error!=""){
		$.gritter.removeAll({
	        after_close: function(){
	          $.gritter.add({
	          	position: 'center',
		        title: 'Error',
		        text: error,
		        class_name: 'danger'
		      });
	        }
	     });
     }
     var success = "<?php echo $success ?>";
     if(success!=""){
		$.gritter.removeAll({
	        after_close: function(){
	          $.gritter.add({
	          	position: 'center',
		        title: 'success',
		        text: success,
		        class_name: 'success'
		      });
	        }
	     });
     }
	$("#section_id").change(function(event) {
		var id = $(this).val();
		getDropDown('#coursetype_id', id, 'coursetype', 'data/coursetype.php')
	}); 
     $("#date_start, #date_stop").blur(function(event) {
     	var from=$("#date_start").datepicker('getDate');
		var to =$("#date_stop").datepicker('getDate');
		var ckto = strTrim($("#date_stop").val());
		var ckf = strTrim($("#date_start").val());
		var msg = "";
		if(ckto=="" || ckf=="") return false;		
		if(to<from){
			msgError("วันที่ปิดรับสมัครต้องหลังจากวันที่เปิดวันรับสมัคร");
			$("#date_stop").val("");
		}
     });
   	 $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue checkbox',
        radioClass: 'iradio_square-blue'
      });

	$('input[name=inhouse]').on('ifUnchecked', function(event){
		$('#inhouse').val("F");
	});
	$('input[name=inhouse]').on('ifChecked', function(event){
		$('#inhouse').val("T");
	});	

 });
var trMenuList = $("#tbMenuList tbody tr:eq(0)").clone();
delRow("#tbMenuList");
 function viewInfo(course_id){
	 if(typeof course_id=="undefined") return;
	 getnewsInfo(course_id);
}

function getnewsInfo(id){
	/*if(typeof id=="undefined") return;
	var url = "data/courselist.php";
	var param = "course_id="+id+"&single=T";
	dataUrl(url, param,"#frmMain");
	disp_detail_other(id);*/
}

function addcourse_detail(){
   var course_id = "<?php echo $_GET["course_id"]?>";
   var url = "index.php?p=<?php echo $_GET["p"];?>&type=detail&course_id="+course_id;
   redirect(url);
}


function addcourse_detail_other(){
   var url = "course_detail map";
   courseDetailData(url,"ret_detail_other");	
}

function ret_detail_other(id){
	for(var i in id){
		var ck = "";
		var data = id[i];
       $("#tbMenuList tbody tr :input").each(function(){
             var t = $(this).val();
 				 var ckId = data["course_detail_id"];
 				 if(ckId==t){
 				    ck = "1";
 				 }
 			 });
 	   if(ck=="1"){
 	 	   ck = "";
 	 	   continue;
 	   }
	   var t = addTrLine("#tbMenuList", trMenuList, data, "runNo");
	}
}
function disp_detail_other(id){
	delRow("#tbMenuList");
    var url = "data/course-detail-list.php";
	var param = "course_id="+id;
	$.ajax( {
		"dataType":'json', 
		"type": "POST", 
		"url": url,
		"data": param, 
		"success": function(data){	
			 $.each(data, function(index, array){
             	addTrLine("#tbMenuList", trMenuList, array, "runNo");    
			 });				 
		}
	});
}

function editcourse_detail(id){
	if(typeof id=="undefined") return;
	var course_id = "<?php echo $_GET["course_id"]?>";
	var url = "index.php?p=<?php echo $_GET["p"];?>&course_detail_id="+id+"&type=detail&course_id="+course_id;
	redirect(url);
}

function ckSave(id){
	onCkForm("#frmMain");
	var tmp = $('#detail').code();
	$('#detail').val(tmp);
	$("#frmMain").submit();
} 
</script>
<style>
	#set_time-error{
		display: none !important;
	}
</style>