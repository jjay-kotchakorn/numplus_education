<?php
include_once "./connection/connection.php";
include_once "./lib/lib.php";
include_once "./share/smcard.php";
include_once "./share/member.php";
include_once "./share/course.php";
include_once "./chk-bls5-func.php";
include_once "./data/pay-status-sendmail-func.php";
require "./elerning/vendor/autoload.php";
global $db; //$EMPID
error_reporting(0);
//header('Content-type: application/json');
header('Access-Control-Allow-Origin: *');
$date_now = date('Y-m-d H:i:s');
$datetime_now = date('Y-m-d H:i:s');
// d($db);
use \Curl\Curl;

//echo "Smart Card Web Service";

// d($_SERVER);

$request_method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : '';
$request_url = !empty($_SERVER["PATH_INFO"]) ? $_SERVER["PATH_INFO"] : '';
$request_url = str_replace("/", "", $request_url);


if ( !empty($request_method) && !empty($request_url) ) {
	$msg = "";
	// $msg .= "request_url={$request_url}|user={$user}";
	// write_log("smcard-api-ctrl", $msg);
	$msg .= "request_method=$request_method|";
	$msg .= "request_url=$request_url|";

	$tmp_req = array();
	if ( $request_method==="GET" ) {
		$tmp_req = $_GET;
	}else if ( $request_method==="POST" ) {
		$tmp_req = $_POST;
	}//end else if

	foreach ($tmp_req as $key => $v) {
		$msg .= "{$key}=$v|";
	}//end loop $v

	$file_name = "smcard-api-ctrl";
	$write_mode = "a";
	$dir_log = "./log/";
	$str = $msg;	
	$date_now = date('Y-m-d H:i:s');
	$date_log = date('Y-m-d');
	$log_name = $dir_log.$file_name."_".$date_log.".log";
	$file = fopen($log_name, $write_mode);
	$str_txt = $date_now."|".$str."\r\n";
	fwrite($file, $str_txt);
	fclose($file);
}//end if



function token_key_authen($token_key=""){
	if ( empty($token_key) ) return false;
	global $db;
	$date_now = date('Y-m-d H:i:s');

	$q = "SELECT
	a.auth_api_token_list_id,
	a.auth_api_token_type_id,
	a.emp_id,
	a.token_key,
	a.token_expire_date,
	a.`code`,
	a.`name`,
	a.name_eng,
	a.active,
	a.recby_id,
	a.rectime,
	a.remark
	FROM auth_api_token_list AS a
	WHERE a.active='T'
	AND a.token_key='{$token_key}'
	";
	$info = $db->rows($q);

	if ( !empty($info) ) {
		$token_expire_date = $info["token_expire_date"];

		if ( $token_expire_date < $date_now ) {
			return false;
		}else{
			return true;
		}//end else
	}else{
		return false;
	}//end else

}//end func

function get_user_authen($token_key=""){
	if ( empty($token_key) ) return false;
	global $db;
	$date_now = date('Y-m-d H:i:s');

	$q = "SELECT
		a.auth_api_token_list_id,
		a.auth_api_token_type_id,
		a.login_id,
		a.emp_id,
		a.token_key,
		a.token_expire_date,
		a.`code`,
		a.`name`,
		a.name_eng,
		a.active,
		a.recby_id,
		a.rectime,
		a.remark
		FROM auth_api_token_list AS a
		WHERE a.active='T'
			AND a.token_key='{$token_key}'
	";
	$info = $db->rows($q);
	// return $q;
	return serialize($info);

}//end func


if ( !empty($request_method) ) {
	if ( $request_method==="POST" ) {

		if ( $request_url=="register_member" ){
			$cid = !empty($_POST["cid"]) ? trim($_POST["cid"]) : "";
			$token_key = !empty($_POST["token_key"]) ? trim($_POST["token_key"]) : "";
			$rs_chk_token = token_key_authen($token_key);

			if ( $rs_chk_token==true ) {

				if ( empty($cid) ) {
					$arr_rs = array();
					$arr_rs["status"] = "error";
					$arr_rs["datetime"] = $date_now;
					$arr_rs["data"] = "";
					$arr_rs["remark"] = " Not found ID card number";
					echo json_encode($arr_rs);
					die();

				}else{

					$q = "SELECT
							m.member_id,
							m.cid,
							m.title_th,
							m.fname_th,
							m.lname_th					
						FROM member AS m
						WHERE active='T'
							AND cid='{$cid}'
					";
					$rs = $db->rows($q);
					//d($rs); die();
					
					if ( !empty($rs) ){

						$arr_rs = array();
						$arr_rs["status"] = "error";
						$arr_rs["datetime"] = $date_now;
						$arr_rs["data"] = $rs;
						$arr_rs["remark"] = "You Have A Member Of System";
						echo json_encode($arr_rs);
						die();

					}//end if

				}//end else

				$authen_info = get_user_authen($token_key);
				// var_dump($authen_info); die();
				$authen_info = unserialize($authen_info);
				$emp_id = $authen_info["emp_id"];

				$title_th = !empty($_POST["title_th"]) ? trim($_POST["title_th"]) : "";
				$fname_th = !empty($_POST["fname_th"]) ? trim($_POST["fname_th"]) : "";
				$lname_th = !empty($_POST["lname_th"]) ? trim($_POST["lname_th"]) : "";
				$birthdate = !empty($_POST["birthdate"]) ? trim($_POST["birthdate"]) : "";
				$gender = !empty($_POST["gender"]) ? trim($_POST["gender"]) : "";
				$nation = !empty($_POST["nation"]) ? trim($_POST["nation"]) : "";
				$email1 = !empty($_POST["email1"]) ? trim($_POST["email1"]) : "";
				$slip_type = !empty($_POST["slip_type"]) ? trim($_POST["slip_type"]) : "";
				$no = !empty($_POST["no"]) ? trim($_POST["no"]) : "";
				$gno = !empty($_POST["gno"]) ? trim($_POST["gno"]) : "";
				$moo = !empty($_POST["moo"]) ? trim($_POST["moo"]) : "";
				$soi = !empty($_POST["soi"]) ? trim($_POST["soi"]) : "";
				$road = !empty($_POST["road"]) ? trim($_POST["road"]) : "";
				$district_id = !empty($_POST["district_id"]) ? trim($_POST["district_id"]) : "";
				$amphur_id = !empty($_POST["amphur_id"]) ? trim($_POST["amphur_id"]) : "";
				$province_id = !empty($_POST["province_id"]) ? trim($_POST["province_id"]) : "";
				$postcode = !empty($_POST["postcode"]) ? trim($_POST["postcode"]) : "";

				// d($_POST);

				$args = array();

				if ( $title_th != "นาย" && $title_th != "นางสาว" && $title_th != "นาง" ) {
					// echo "55555555555555555"; die();
						if ( $title_th=="น.ส." ) {
							$title_th == "นางสาว";
						}else{
							$args["title_th_text"] = $title_th;
							$title_th = "อื่นๆ";
							
    					}//end else
    
   				}//end if

				$args["table"] = "member";
				$args["cid"] = $cid;
				$args["username"] = $cid;
				$args["password"] = $cid;
				$args["title_th"] = $title_th;
				$args["fname_th"] = $fname_th;
				$args["lname_th"] = $lname_th;
				$args["birthdate"] = $birthdate;
				$args["gender"] = $gender;
				$args["nation"] = $nation;
				$args["email1"] = $email1;
				$args["slip_type"] = $slip_type;
				$args["taxno"] = $cid;
				$args["no"] = $no;
				$args["gno"] = $gno;
				$args["moo"] = $moo;
				$args["soi"] = $soi;
				$args["road"] = $road;
				$args["district_id"] = $district_id;
				$args["amphur_id"] = $amphur_id;
				$args["province_id"] = $province_id;
				$args["postcode"] = $postcode;
				$args["receipt_title"] = $title_th;
				$args["receipt_fname"] = $fname_th;
				$args["receipt_lname"] = $lname_th;
				$args["receipt_no"] = $no;
				$args["receipt_moo"] = $moo;
				$args["receipt_soi"] = $soi;
				$args["receipt_road"] = $road;
				$args["receipt_district_id"] = $district_id;
				$args["receipt_amphur_id"] = $amphur_id;
				$args["receipt_province_id"] = $province_id;
				$args["receipt_postcode"] = $postcode;
				$args["recby_id"] = $emp_id;
				$args["reg_date"] = $date_now;
				$args["last_update"] = $date_now;
				$args["rectime"] = $date_now;

				 // echo "<hr>";
				//var_dump( $db->set($args, true, true) ); die();
				// d($args); die();
				$member_id = $db->set($args);
				
				if ( !empty($member_id) ) {
				// if ( 1 ) {
					unset($_POST);
					unset($args);

					$tmp_data = array();
					$tmp_data["member_id"] = $member_id;
					$tmp_data["cid"] = $cid;
					$tmp_data["title_th"] = $title_th;
					$tmp_data["fname_th"] = $fname_th;
					$tmp_data["lname_th"] = $lname_th;

					$arr_rs = array();
					$arr_rs["status"] = "success";
					$arr_rs["datetime"] = $date_now;
					$arr_rs["data"] = $tmp_data;
					$arr_rs["remark"] = "Register member success";
					echo json_encode($arr_rs);
					// d($arr_rs);
					die();

				}//end if				

			}//end if rs_chk_token


        }else if ($request_url=="sign_stamp") {
        	
        	// d($_POST); die();

        	$cid = !empty($_POST["cid"]) ? trim($_POST["cid"]) : "";
        	$course_id = !empty($_POST["course_id"]) ? trim($_POST["course_id"]) : "";
        	$course_detail_id = !empty($_POST["course_detail_id"]) ? trim($_POST["course_detail_id"]) : "";
        	$smcard_report_id = !empty($_POST["smcard_report_id"]) ? trim($_POST["smcard_report_id"]) : "";
        	$smcard_stamp_type_id = !empty($_POST["smcard_stamp_type_id"]) ? trim($_POST["smcard_stamp_type_id"]) : "";
        	$datetime_stamp = !empty($_POST["datetime_stamp"]) ? trim($_POST["datetime_stamp"]) : $date_now;
        	$time_start = !empty($_POST["time_start"]) ? trim($_POST["time_start"]) : "";
        	$time_stop = !empty($_POST["time_stop"]) ? trim($_POST["time_stop"]) : "";

        	$token_key = !empty($_POST["token_key"]) ? trim($_POST["token_key"]) : "";
        	$rs_chk_token = token_key_authen($token_key);

        	if ( $rs_chk_token==true ) {

        		if ( empty($cid) ) {
        			$arr_rs = array();
        			$arr_rs["status"] = "error";
        			$arr_rs["datetime"] = $date_now;
        			$arr_rs["data"] = "";
        			$arr_rs["remark"] = " Not found ID card number";
        			echo json_encode($arr_rs);
        			die();

        		}elseif ( empty($smcard_report_id) ) {
        			$arr_rs = array();
        			$arr_rs["status"] = "error";
        			$arr_rs["datetime"] = $date_now;
        			$arr_rs["data"] = "";
        			$arr_rs["remark"] = " Not found smcard_report_id ";
        			echo json_encode($arr_rs);
        			die();

        		}elseif ( empty($course_id) ) {
        			$arr_rs = array();
        			$arr_rs["status"] = "error";
        			$arr_rs["datetime"] = $date_now;
        			$arr_rs["data"] = "";
        			$arr_rs["remark"] = " Not found Course ID ";
        			echo json_encode($arr_rs);
        			die();
        		
        		}elseif ( empty($smcard_stamp_type_id) ) {
        			$arr_rs = array();
        			$arr_rs["status"] = "error";
        			$arr_rs["datetime"] = $date_now;
        			$arr_rs["data"] = "";
        			$arr_rs["remark"] = " Not found smcard_stamp_type_id ";
        			echo json_encode($arr_rs);
        			die();
        		
        		}elseif ( empty($course_detail_id) ) {
        			$arr_rs = array();
        			$arr_rs["status"] = "error";
        			$arr_rs["datetime"] = $date_now;
        			$arr_rs["data"] = "";
        			$arr_rs["remark"] = " Not found Course_detail ID ";
        			echo json_encode($arr_rs);
        			die();

        		}elseif ( empty($time_start) && ($smcard_stamp_type_id==1 || $smcard_stamp_type_id==2) ) {
        			$arr_rs = array();
        			$arr_rs["status"] = "error";
        			$arr_rs["datetime"] = $date_now;
        			$arr_rs["data"] = "";
        			$arr_rs["remark"] = "time_start is not empty";
        			echo json_encode($arr_rs);
        			die();

        		}elseif ( empty($time_stop) && ($smcard_stamp_type_id==1 || $smcard_stamp_type_id==2) ) {
        			$arr_rs = array();
        			$arr_rs["status"] = "error";
        			$arr_rs["datetime"] = $date_now;
        			$arr_rs["data"] = "";
        			$arr_rs["remark"] = "time_stop is not empty";
        			echo json_encode($arr_rs);
        			die();

        		}else{

        			$member_info = view_member(" AND a.active='T' AND a.cid='{$cid}'");
        			$member_info = $member_info[0];
        			// d($member_info);
        			$member_id = $member_info["member_id"];

        			$q = "SELECT b.register_id
        					, c.register_course_detail_id
        					, c.course_id
        					, c.course_detail_id
        				FROM register AS b 
        				LEFT JOIN register_course_detail AS c ON c.register_id=b.register_id
        				WHERE b.active='T'
        					AND b.pay_status IN (3,5,6) 
        					AND b.member_id={$member_id}
        					AND c.active='T' 
							AND c.course_id={$course_id}
							AND c.course_detail_id={$course_detail_id}
        			";

        			// echo $q."<hr>";
        			$reg_info = $db->rows($q);
        			// d($reg_info); die();
        			$register_id = $reg_info["register_id"];
        			$register_course_detail_id = $reg_info["register_course_detail_id"];

        			if ( empty($member_info) ) {
        				$arr_rs = array();
	        			$arr_rs["status"] = "error";
	        			$arr_rs["datetime"] = $date_now;
	        			$arr_rs["data"] = "";
	        			$arr_rs["remark"] = "You don't have a member of system";
	        			echo json_encode($arr_rs);
	        			die();
        			}else if ( empty($register_course_detail_id) ) {
        				$arr_rs = array();
	        			$arr_rs["status"] = "error";
	        			$arr_rs["datetime"] = $date_now;
	        			$arr_rs["data"] = "";
	        			$arr_rs["remark"] = "คุณยังไม่ได้ลงทะเบียนในรอบนี้";
	        			echo json_encode($arr_rs);
	        			die();
        			}else{

        				$smcard_info = "";
        				$cond = " 
        					AND a.active='T' 
        					AND a.smcard_report_id={$smcard_report_id}
        					AND a.smcard_stamp_type_id={$smcard_stamp_type_id}
        					AND a.time_start='{$time_start}'
        					AND a.time_stop='{$time_stop}'
        					AND a.course_detail_id='{$course_detail_id}'
        					AND a.course_id='{$course_id}'
        					AND a.cid='{$cid}'
        				";
        				$smcard_info = get_smcard_report_list($cond);
        				// var_dump($smcard_info); die();
        				$enable_sign_stamp_for_date_other = "NO";
        				if ( !empty($smcard_info) ) {
        					$tmp = "";
        					$tmp = $smcard_info["rectime"];
        					$tmp = explode(" ", $tmp);
        					$smcard_info_ref_date = $tmp[0];

        					$tmp = "";
        					$tmp = $date_now;
        					$tmp = explode(" ", $tmp);
        					$date_now_ref = $tmp[0];

        					if ( $date_now_ref>$smcard_info_ref_date ) {
        						$enable_sign_stamp_for_date_other = "YES";
        					}else{
	        					unset($smcard_info);
	        					$arr_rs = array();
			        			$arr_rs["status"] = "error";
			        			$arr_rs["datetime"] = $date_now;
			        			$arr_rs["data"] = "";
			        			$arr_rs["remark"] = "Data dupplicate (ลงเวลาซํ้า)";
			        			echo json_encode($arr_rs);
			        			die();
        					}//end else
        				}//end if


    					$smcard_info = "";
        				$cond = " 
        					AND a.active='T' 
        					AND a.smcard_report_id={$smcard_report_id}
        					AND a.smcard_stamp_type_id IN (5)
        					AND a.course_detail_id='{$course_detail_id}'
        					AND a.course_id='{$course_id}'
        					AND a.member_id={$member_id}
        				";
        				$smcard_info = get_smcard_report_list($cond);

        				$config = get_config("smcard_sign_stamp");
        				$grant_sign_stamp = trim($config);

        				if ( empty($smcard_info) && $grant_sign_stamp=='F' ) {
	        				unset($smcard_info);
        					$arr_rs = array();
		        			$arr_rs["status"] = "error";
		        			$arr_rs["datetime"] = $date_now;
		        			$arr_rs["data"] = "";
		        			$arr_rs["remark"] = "ไม่ผ่านการตรวจสอบข้อมูล จึงไม่สารถลงเวลาเข้าร่วมกิจกรรรมได้";
		        			echo json_encode($arr_rs);
		        			die();
        				}//end if   			
        				// die();

        				$authen_info = get_user_authen($token_key);
						// var_dump($authen_info); die();
						$authen_info = unserialize($authen_info);
						$emp_id = $authen_info["emp_id"];

						unset($smcard_report_list_id);
						$args = array();
		    			$args["table"] = "smcard_report_list";
		    			$args["emp_id"] = $emp_id;
		    			$args["recby_id"] = $emp_id;
		    			$args["register_id"] = $register_id;
		    			$args["member_id"] = $member_id;
		    			$args["register_course_detail_id"] = $register_course_detail_id;
		    			$args["course_id"] = $course_id;
		    			$args["course_detail_id"] = $course_detail_id;
		    			$args["cid"] = $cid;
		    			$args["smcard_report_id"] = $smcard_report_id;
		    			$args["smcard_stamp_type_id"] = $smcard_stamp_type_id;
		    			$args["datetime_stamp"] = $datetime_stamp;
		    			$args["time_start"] = !empty($time_start) ? $time_start : "00:00:00";
		    			$args["time_stop"] = !empty($time_stop) ? $time_stop : "00:00:00";

		    			$smcard_report_list_id = $db->set($args);
		    			// var_dump($db->set($args, true, true));

		    			if ( !empty($smcard_report_list_id) ) {
		    				$arr_rs = array();
		    				$arr_rs["status"] = "success";
		    				$arr_rs["datetime"] = $date_now;
		    				$arr_rs["data"] = "";
		    				$arr_rs["remark"] = "บันทึกเวลาเรียบร้อย";
		    				echo json_encode($arr_rs);
        					die();
		    			}else{
		    				$arr_rs = array();
		        			$arr_rs["status"] = "error";
		        			$arr_rs["datetime"] = $date_now;
		        			$arr_rs["data"] = "";
		        			$arr_rs["remark"] = "บันทึกไม่สำเร็จ";
		        			echo json_encode($arr_rs);
		        			die();
		    			}//end if

        			}//end else

        		}//end else

        	}//end if

		}else if ($request_url=="register_walkin") {

       		$cid = !empty($_POST["cid"]) ? trim($_POST["cid"]) : "";
        	$section_id = !empty($_POST["section_id"]) ? trim($_POST["section_id"]) : "";
        	$course_id = !empty($_POST["course"]) ? trim($_POST["course"]) : "9999";
        	$course_detail_id = !empty($_POST["course_detail"]) ? trim($_POST["course_detail"]) : "";
        	$course_type = !empty($_POST["course_type"]) ? trim($_POST["course_type"]) : "";
        	$token_key = !empty($_POST["token_key"]) ? trim($_POST["token_key"]) : "";
        	
        	$check_data_before_register = !empty($_POST["check_data_before_register"]) ? trim($_POST["check_data_before_register"]) : "";

        	$rs_chk_token = token_key_authen($token_key);

        	if ( $rs_chk_token==true ) {

        		if ( empty($cid) ) {
        			$arr_rs = array();
        			$arr_rs["status"] = "error";
        			$arr_rs["datetime"] = $date_now;
        			$arr_rs["data"] = "";
        			$arr_rs["remark"] = " Not found ID card number";
        			echo json_encode($arr_rs);
        			die();

        		}elseif ( empty($section_id) ) {
        			$arr_rs = array();
        			$arr_rs["status"] = "error";
        			$arr_rs["datetime"] = $date_now;
        			$arr_rs["data"] = "";
        			$arr_rs["remark"] = " Not found Section_id ID ";
        			echo json_encode($arr_rs);
        			die();

        		}elseif ( empty($course_id) ) {
        			$arr_rs = array();
        			$arr_rs["status"] = "error";
        			$arr_rs["datetime"] = $date_now;
        			$arr_rs["data"] = "";
        			$arr_rs["remark"] = " Not found Course ID ";
        			echo json_encode($arr_rs);
        			die();
        		
        		}elseif ( empty($course_type) ) {
        			$arr_rs = array();
        			$arr_rs["status"] = "error";
        			$arr_rs["datetime"] = $date_now;
        			$arr_rs["data"] = "";
        			$arr_rs["remark"] = " Not found Coursetype ID ";
        			echo json_encode($arr_rs);
        			die();
        		
        		}elseif ( empty($course_detail_id) ) {
        			$arr_rs = array();
        			$arr_rs["status"] = "error";
        			$arr_rs["datetime"] = $date_now;
        			$arr_rs["data"] = "";
        			$arr_rs["remark"] = " Not found Course_detail ID ";
        			echo json_encode($arr_rs);
        			die();

        		}else{

        			$q = "SELECT a.section_id, a.coursetype_id FROM course a WHERE a.course_id={$course_id}";

        			$cos_info = $db->rows($q);

        			if ( !empty($cos_info) ) {    				
	        			$sec_id = $cos_info["section_id"];
	        			$type_id = $cos_info["coursetype_id"];

		        		$res = chk_bls5($cid, $sec_id, $type_id, $course_id, $course_detail_id);
        			}else{
        				$res = '0001:0000-00-00';
        			}//end else

        			$arr_res = array();
        			$arr_res = explode(":", $res);
        			$res_code = trim($arr_res[0]);
        			$res_date = trim($arr_res[1]);

	        		if( $res_code != '3000'){
	        			// response (ถ้า connect ได้)
						// 1000:0000-00-00 : connect ได้ ข้อมูลไม่ถูกต้อง (จำนวนข้อมูล)
						// 1001:0000-00-00 : connect ได้ ข้อมูลไม่ถูกต้อง (เลขที่บัตร)
						// 1002:0000-00-00 : connect ได้ ข้อมูลไม่ถูกต้อง (วันที่)
						// 2000:0000-00-00 : invalid user or password
						// 3000:0000-00-00 : ไม่อยู่ในรายการห้ามสอบ
						// 4000:0000-00-00 : อยู่ในรายการห้ามสอบ

	        			$msg_error = "";

	        			switch ( $res_code ) {
	        				case '0001':
	        					$msg_error = "connect ระบบ block list ไม่ได้";
	        					break;
	        				case '1000':
	        					$msg_error = "block list : connect ได้ ข้อมูลไม่ถูกต้อง (จำนวนข้อมูล)";
	        					break;
	        				case '1001':
	        					$msg_error = "block list : connect ได้ ข้อมูลไม่ถูกต้อง (เลขที่บัตร)";
	        					break;
	        				case '1002':
	        					$msg_error = "block list : connect ได้ ข้อมูลไม่ถูกต้อง (วันที่)";
	        					break;
	        				case '2000':
	        					$msg_error = "block list : invalid user or password";
	        					break;
	        				case '4000':
	        					$msg_error = "block list : อยู่ในรายการห้ามสอบ";
	        					break;
	        				case '4001':
	        					$msg_error = "อยู่ในรายการห้ามสอบ จนถึงวันที่ {$res_date} (block case 1 : โดย TSI)";
	        					break;
	        				case '4002':
	        					$msg_error = "อยู่ในรายการห้ามสอบ จนถึงวันที่ {$res_date} (block case 2 : กรณีผู้คุมสอบ";
	        					break;
	        				case '4003':
	        					$msg_error = "อยู่ในรายการห้ามสอบ จนถึงวันที่ {$res_date} (block case 3 : กรณีเคยสอบผ่าน วิชา และยังไม่หมดอายุ)";
	        					break;
	        			}//end sw

	        			$arr_rs = array();
	        			$arr_rs["status"] = "error";
	        			$arr_rs["datetime"] = $date_now;
	        			$arr_rs["data"] = "";
	        			$arr_rs["remark"] = $msg_error;
	        			echo json_encode($arr_rs);
	        			die();
	        		}//end if


        			$q = "SELECT member_id FROM member WHERE active='T' AND cid='{$cid}'";
        			$member_id = $db->data($q);

        			if ( empty($member_id) ) {
        				$arr_rs = array();
	        			$arr_rs["status"] = "error";
	        			$arr_rs["datetime"] = $date_now;
	        			$arr_rs["data"] = "";
	        			$arr_rs["remark"] = "You don't have a member of system";
	        			echo json_encode($arr_rs);
	        			die();
        			}//end if				
        			
        			$q = " SELECT
								a.register_id,
								a.cid,
								a.member_id,
								a.title,
								a.fname,
								a.lname,
								b.course_id,
								c.title AS course_name,
								b.course_detail_id,
								d.address,
								d.address_detail
								-- d.date,
								-- d.time
							FROM register AS a
							LEFT JOIN register_course_detail AS b ON b.register_id = a.register_id 
							LEFT JOIN course AS c ON c.course_id = b.course_id 
							LEFT JOIN course_detail AS d ON d.course_detail_id = b.course_detail_id 
							WHERE a.active = 'T' AND b.active = 'T'
							-- AND c.active = 'T'
							-- AND d.active = 'T'
							AND a.cid ='{$cid}'
							AND b.course_detail_id = '{$course_detail_id}'
        			";

        			$reg_info = $db->rows($q);
					// d($reg_info); die();
        			$reg_info_course_detail_id = $reg_info["course_detail_id"];

        			if ( $reg_info_course_detail_id==$course_detail_id ){

        				$arr_rs = array();
        				$arr_rs["status"] = "error";
        				$arr_rs["datetime"] = $date_now;
        				$arr_rs["data"] = $reg_info;
        				$arr_rs["remark"] = "คุณได้ลงทะเบียนแล้ว";
        				echo json_encode($arr_rs);
        				die();
					}//end if	

				}//end else

				
				//check course co room
				$q = "SELECT a.course_detail_list_id,
						a.course_id,
						a.course_detail_id,
						a.active,
						a.recby_id,
						a.rectime,
						a.remark,
						d.name AS section_name,
						e. NAME AS type_name,
						b.title AS course_name,
						c.address,
						c.address_detail,
						b.price AS course_price,
						c.discount AS course_discount
						
					FROM course_detail_list AS a
					LEFT JOIN course AS b ON b.course_id=a.course_id
					LEFT JOIN course_detail AS c ON c.course_detail_id=a.course_detail_id
					LEFT JOIN section AS d ON c.section_id=d.section_id
					LEFT JOIN coursetype AS e ON e.coursetype_id = c.coursetype_id
					WHERE a.active='T'
						AND a.course_detail_id={$course_detail_id}
						AND c.coursetype_id={$course_type}
						AND c.section_id={$section_id}
				";
				// echo $q; die();
				$cos_list = $db->get($q);
				// d($cos_list); die();

				if ( !empty($cos_list) ) {
					$data = array();
					foreach ($cos_list as $key => $v) {
						$course_price = $v["course_price"];
						$course_discount = $v["course_discount"];
						$course_price_total = $course_price-$course_discount;

						$v["course_price_total"] = $course_price_total;
						unset($v["course_detail_list_id"]);
						unset($v["recby_id"]);
						unset($v["rectime"]);
						unset($v["remark"]);
						$data[] = $v;
					}//end loop $v

					$arr_rs = array();
    				$arr_rs["status"] = "success";
    				$arr_rs["datetime"] = $date_now;
    				$arr_rs["data"] = $data;
    				$arr_rs["remark"] = "หลักสูตรที่ต้องการลงทะเบียน";

    				if ( $check_data_before_register == 'T' ) {
	    				echo json_encode($arr_rs);
	    				die();
    				}//end if


				}else{

					$q = " SELECT
								a.course_id,
								a.course_detail_id,
								a.active,
								a.date_start,
								a.date_stop,
								a.discount AS course_detail_discount,
								s.NAME AS section_name,
								b.NAME AS type_name,
								a.address,
								a.address_detail
							FROM course_detail AS a
							LEFT JOIN coursetype AS b ON b.coursetype_id = a.coursetype_id
							LEFT JOIN section AS s ON s.section_id = a.section_id
							WHERE a.active = 'T' AND c.active = 'T'
							AND a.course_detail_id = {$course_detail_id}
					";

					$reg_detail = $db->rows($q);
					// var_dump($reg_detail); die();
					//d($reg_detail); die();

					$cos_info = get_course("", $course_id);
					$cos_info = $cos_info[0];

					if ( !empty($reg_detail) && !empty($cos_info) ) {
						
						$data = array();
						$v = $reg_detail;						

						$course_discount = 0;
						$course_discount_start_date = $cos_info["date_start"];
						$course_discount_stop_date = $cos_info["date_stop"];

						$course_detail_discount_start_date = $v["date_start"];
						$course_detail_discount_stop_date = $v["date_stop"];

						if ( !empty($cos_info["course_discount"]) 
							&& ($date_now>=$course_discount_start_date) 
							&& ($date_now<=$course_discount_stop_date) 
						) {
							$course_discount = $cos_info["course_discount"];
						}//end if

						if ( !empty($v["course_detail_discount"]) 
							&& ($date_now>=$course_detail_discount_start_date) 
							&& ($date_now<=$course_detail_discount_stop_date) 
						) {
							$course_discount = $v["course_detail_discount"];
						}//end if

						$course_price_total = $course_price-$course_discount;

						$v["course_price_total"] = $course_price_total;
						//unset($v["course_detail_list_id"]);
						$data[] = $v;

						$arr_rs = array();
	    				$arr_rs["status"] = "success";
	    				$arr_rs["datetime"] = $date_now;
	    				$arr_rs["data"] = $data;
	    				$arr_rs["remark"] = "หลักสูตรที่ต้องการลงทะเบียน";

	    				if ( $check_data_before_register == 'T' ) {
		    				echo json_encode($arr_rs);
		    				die();
	    				}//end if

	    			}else{
	    				$arr_rs = array();
	                	$arr_rs["status"] = "error";
	                	$arr_rs["datetime"] = $date_now;
	                	$arr_rs["data"] = "";
	                	$arr_rs["remark"] = "ระบบไม่สามารถค้นหาหลักสูตรหรือรอบได้ กรุณาติดต่อผู้ดูแลระบบ";
	                	echo json_encode($arr_rs);
	                	die();
	    			}//end else
				
				}//end else


            	if( $check_data_before_register == 'F' ) {
            		// d($db);
					$authen_info = get_user_authen($token_key);
					// var_dump($authen_info); die();
					$authen_info = unserialize($authen_info);
					$emp_id = $authen_info["emp_id"];
					// d($authen_info); die();

					//if($pay_status==3){

						// $docno_info = rundocno($course_type, $section_id);
						// $docno = $docno_info["docno"];
						// $runyear = $docno_info["runyear"];
						// $runno = $docno_info["runno"];
						// $doc_prefix = $docno_info["doc_prefix"];
						// //d($docno_info); die();
					//}

					$title_th = !empty($_POST["title_th"]) ? trim($_POST["title_th"]) : "";
					$fname_th = !empty($_POST["fname_th"]) ? trim($_POST["fname_th"]) : "";
					$lname_th = !empty($_POST["lname_th"]) ? trim($_POST["lname_th"]) : "";
					$taxno = !empty($_POST["taxno"]) ? trim($_POST["taxno"]) : "";
					$slip_type = !empty($_POST["slip_type"]) ? trim($_POST["slip_type"]) : "";
					$receipt_id = !empty($_POST["receipt_id"]) ? trim($_POST["receipt_id"]) : "";
					$receipttype_id = !empty($_POST["receipttype_id"]) ? trim($_POST["receipttype_id"]) : "";
					$receipttype_text = !empty($_POST["receipttype_text"]) ? trim($_POST["receipttype_text"]) : "";
					$branch = !empty($_POST["branch"]) ? trim($_POST["branch"]) : "";
					$no = !empty($_POST["no"]) ? trim($_POST["no"]) : "";
					$gno = !empty($_POST["gno"]) ? trim($_POST["gno"]) : "";
					$moo = !empty($_POST["moo"]) ? trim($_POST["moo"]) : "";
					$soi = !empty($_POST["soi"]) ? trim($_POST["soi"]) : "";
					$road = !empty($_POST["road"]) ? trim($_POST["road"]) : "";
					$district_id = !empty($_POST["district_id"]) ? trim($_POST["district_id"]) : "";
					$amphur_id = !empty($_POST["amphur_id"]) ? trim($_POST["amphur_id"]) : "";
					$province_id = !empty($_POST["province_id"]) ? trim($_POST["province_id"]) : "";
					$postcode = !empty($_POST["postcode"]) ? trim($_POST["postcode"]) : "";
					$pay = !empty($_POST["pay"]) ? trim($_POST["pay"]) : "";
					$pay_status = !empty($_POST["pay_status"]) ? trim($_POST["pay_status"]) : "";
					$pay_date = !empty($_POST["pay_date"]) ? trim($_POST["pay_date"]) : "";
					$pay_date = $date_now;
					

					if($slip_type=="individuals"){
					
					$args = array();

					$args["receipt_title"] = $title_th;

					if ( $title_th != "นาย" && $title_th != "นางสาว" && $title_th != "นาง" ) {
					// echo "55555555555555555"; die();
						if ( $title_th=="น.ส." ) {
							$title_th == "นางสาว";
						}else{
							$args["title_th_text"] = $title_th;
							$args["receipt_title"] = $title_th;
							$title_th = "อื่นๆ";							
    					}//end else
   					}//end if

   					$args["table"] = "register";
					$args["member_id"] = $member_id;
					$args["cid"] = $cid;
					$args["title"] = $title_th;
					$args["fname"] = $fname_th;
					$args["lname"] = $lname_th;
					$args["taxno"] = $taxno;
					$args["status"] = $pay_status;
					$args["date"] = $date_now;
					$args["slip_type"] = $slip_type;
					$args["pay_status"] = $pay_status;
					$args["pay"] = $pay;
					$args["pay_date"] = $pay_date;
					$args["section_id"] = $section_id;
					$args["course_id"] = $course_id;
					$args["course_detail_id"] = $course_detail_id;
					$args["coursetype_id"] = $course_type;
					$args["course_price"] = $course_price;
					$args["course_discount"] = $course_discount;
					$args["pay_price"] = $course_price_total;
					$args["receipt_fname"] = $fname_th;
					$args["receipt_lname"] = $lname_th;
					$args["receipt_no"] = $no;
					$args["receipt_gno"] = $gno;
					$args["receipt_moo"] = $moo;
					$args["receipt_soi"] = $soi;
					$args["receipt_road"] = $road;
					$args["receipt_district_id"] = $district_id;
					$args["receipt_amphur_id"] = $amphur_id;
					$args["receipt_province_id"] = $province_id;
					$args["receipt_postcode"] = $postcode;
					$args["recby_id"] = $emp_id;
					$args["rectime"] = $date_now;

					if($pay_status==3){

						$docno_info = rundocno($course_type, $section_id);
						$docno = $docno_info["docno"];
						$runyear = $docno_info["runyear"];
						$runno = $docno_info["runno"];
						$doc_prefix = $docno_info["doc_prefix"];
						//d($docno_info); die();
						
						$args["docno"] = $docno;
						$args["runyear"] = $runyear;
						$args["runno"] = $runno;
						$args["doc_prefix"] = $doc_prefix;

					}

					// echo "<hr>";
					// var_dump( $db->set($args, true, true) ); die();
					$register_id = $db->set($args);

                }elseif ($slip_type=="corparation" && $pay_status=="3") {

                	$args = array();

                	$args["receipt_title"] = $title_th;

					if ( $title_th != "นาย" && $title_th != "นางสาว" && $title_th != "นาง" ) {
					// echo "55555555555555555"; die();
						if ( $title_th=="น.ส." ) {
							$title_th == "นางสาว";
						}else{
							$args["title_th_text"] = $title_th;
							$args["receipt_title"] = $title_th;
							$title_th = "อื่นๆ";
							
    					}//end else
    
   					}//end if

   					$args["table"] = "register";
					$args["member_id"] = $member_id;
					$args["cid"] = $cid;
					$args["title"] = $title_th;
					$args["fname"] = $fname_th;
					$args["lname"] = $lname_th;
					$args["taxno"] = $taxno;
					$args["status"] = $pay_status;
					$args["date"] = $date_now;
					$args["slip_type"] = $slip_type;
					$args["receipttype_id"] = $receipttype_id;
					$args["receipt_id"] = $receipt_id;
					$args["receipttype_text"] = $receipttype_text;
					$args["pay_status"] = $pay_status;
					$args["pay"] = $pay;
					$args["pay_date"] = $pay_date;
					$args["section_id"] = $section_id;
					$args["course_id"] = $course_id;
					$args["course_detail_id"] = $course_detail_id;
					$args["coursetype_id"] = $course_type;
					$args["course_price"] = $course_price;
					$args["course_discount"] = $course_discount;
					$args["pay_price"] = $course_price_total;
					$args["receipt_fname"] = $fname_th;
					$args["receipt_lname"] = $lname_th;
					$args["branch"] = $branch;
					$args["receipt_no"] = $no;
					$args["receipt_gno"] = $gno;
					$args["receipt_moo"] = $moo;
					$args["receipt_soi"] = $soi;
					$args["receipt_road"] = $road;
					$args["receipt_district_id"] = $district_id;
					$args["receipt_amphur_id"] = $amphur_id;
					$args["receipt_province_id"] = $province_id;
					$args["receipt_postcode"] = $postcode;
					$args["recby_id"] = $emp_id;
					$args["docno"] = $docno;
					$args["runyear"] = $runyear;
					$args["runno"] = $runno;
					$args["doc_prefix"] = $doc_prefix;
					$args["rectime"] = $date_now;
					// echo "<hr>";
					// var_dump( $db->set($args, true, true) ); die();
					$register_id = $db->set($args);
                	
                }else{

                	$arr_rs = array();
                	$arr_rs["status"] = "error";
                	$arr_rs["datetime"] = $date_now;
                	$arr_rs["data"] = "";
                	$arr_rs["remark"] = "สถานะการชำระต้องเป็น 'ชำระเงินแล้ว' เท่านั้นถึงจะออกใบเสร็จในนามนิติบุคคลได้";
                	echo json_encode($arr_rs);
                	die();

                }//endelse

                if ( !empty($register_id) ) {

                	$args = array();
                	$args["table"] = "register_course_detail";
                	$args["register_id"] = $register_id;
                	$args["course_id"] = $course_id;
                	$args["course_detail_id"] = $course_detail_id;
                	$args["recby_id"] = $emp_id;
                	$args["rectime"] = $date_now;
                	$args["remark"] = "walkin";
                	$register_course_detail_id = $db->set($args);

                	$args = array();
                	$args["table"] = "register_course";
                	$args["register_id"] = $register_id;
                	$args["course_id"] = $course_id;
                	$args["recby_id"] = $emp_id;
                	$args["rectime"] = $date_now;
                	$register_course_id = $db->set($args);


                	unset($_POST);
                	unset($args);

                	$tmp_data = array();
                	$tmp_data["register_id"] = $register_id;
                	$tmp_data["member_id"] = $member_id;
                	$tmp_data["cid"] = $cid;
                	$tmp_data["fname"] = $fname_th;
                	$tmp_data["lname"] = $lname_th;
                	$tmp_data["section_id"] = $section_id;
                	$tmp_data["coursetype_id"] = $course_type;
                	$tmp_data["course_id"] = $course_id;
                	$tmp_data["course_detail_id"] = $course_detail_id;

                	$q="SELECT
                	a.register_id,
                	a.member_id,
                	a.cid,
                	a.title,
                	a.fname,
                	a.lname,
                	d.`name` AS section_name,
                	e.`name` AS type_name,
                	a.course_id,
                	b.title AS course_name,
                	a.course_detail_id,
                	c.address,
                	c.address_detail,
                	a.pay_price
                	FROM register AS a
                	LEFT JOIN course AS b ON b.course_id = a.course_id
                	LEFT JOIN course_detail AS c ON c.course_detail_id = a.course_detail_id
                	LEFT JOIN section AS d ON d.section_id = a.section_id
                	LEFT JOIN coursetype AS e ON e.coursetype_id = a.coursetype_id
                	WHERE  a.active = 'T' 
                	AND a.register_id = {$register_id}
                	";
									//echo $q."<hr>";
                	$reg_data = $db->rows($q);
									//d($reg_data);

                	$arr_register_ids = array();
                	$arr_register_ids[0] = $register_id;
                	
                	//send email to member
                	pay_status_sendmail_func($arr_register_ids);

                	$arr_rs = array();
                	$arr_rs["status"] = "success";
                	$arr_rs["datetime"] = $date_now;
                	$arr_rs["data"] = $reg_data;
                	$arr_rs["remark"] = "ลงทะเบียนสำเร็จ";
                	echo json_encode($arr_rs);
									// d($arr_rs);
                	die();

				}//end if

				}//end if check_data_before_register		

			}//end if rs_chk_token

        }//end if register_walkin

//------------------------------------- NEW API ---------------------------------//
        else if ( $request_url=="update_member_profile" ){
			// $cid = !empty($_POST["cid"]) ? trim($_POST["cid"]) : "";
			$token_key = !empty($_POST["token_key"]) ? trim($_POST["token_key"]) : "";

			if ( empty($_POST) ) {
				$arr_rs = array();
				$arr_rs["status"] = "error";
				$arr_rs["datetime"] = $date_now;
				$arr_rs["data"] = "";
				$arr_rs["remark"] = "Data is not empty";
				echo json_encode($arr_rs);
				die();
			}//end if

			$tmp = array();
			foreach ($_POST as $key => $v) {
				$tmp[$key] = trim($v);
			}//end loop $v
			// d($tmp);

			$rs_chk_token = token_key_authen($token_key);

			// d($_POST);
			// die();

			if ( $rs_chk_token==true ) {

				$authen_info = get_user_authen($token_key);
				// var_dump($authen_info); die();
				$authen_info = unserialize($authen_info);
				$emp_id = $authen_info["emp_id"];

				$member_id = !empty($tmp["member_id"]) ? $tmp["member_id"] : "";
				$cid = !empty($tmp["cid"]) ? $tmp["cid"] : "";

				$q = "SELECT cid FROM member WHERE active='T' AND member_id={$member_id}";
				$member_cid = $db->data($q);
				unset($q);

				if ( !empty($member_id) && ($member_cid==$cid) ) {

					unset($tmp["token_key"]);
					unset($tmp["cid"]);
					unset($tmp["member_id"]);

					$title_th = $tmp["title_th"];

					$args = array();
					$args = $tmp;
					
					if ( $title_th != "นาย" && $title_th != "นางสาว" && $title_th != "นาง" ) {
						if ( $title_th=="น.ส." ) {
							$title_th == "นางสาว";
						}else{
							$args["title_th_text"] = $title_th;
							$title_th = "อื่นๆ";
						}//end else
						$args["title_th"] = $title_th;			
					}//end if

					$args["table"] = "member";
					$args["id"] = $member_id;
					$args["last_update"] = $date_now;
					$args["recby_id"] = $emp_id;
					
					// d($args);

					//var_dump( $db->set($args, true, true) ); die();
					$db->set($args);

					$info = view_member("", $member_id);
					$info = $info[0];
					// d($info);
					$member_fname_th = $info["fname_th"];
					$member_lname_th = $info["lname_th"];
					unset($info);

					// $check_data_update = false;
					if ( !empty($args["fname_th"]) ) {
						$check_data_update = false;
						if ( $args["fname_th"]==$member_fname_th ) {
							$check_data_update = true;
						}//end if
					}//end if
					if ( !empty($args["lname_th"]) ) {
						$check_data_update = false;
						if ( $args["lname_th"]==$member_lname_th ) {
							$check_data_update = true;
						}//end if
					}//end if

					if ( $check_data_update == true ) {
						$arr_rs = array();
		    			$arr_rs["status"] = "success";
		    			$arr_rs["datetime"] = $date_now;
		    			$arr_rs["data"] = "";
		    			$arr_rs["remark"] = "";
		    			echo json_encode($arr_rs);
		    			die();
					}else{
						$arr_rs = array();
		    			$arr_rs["status"] = "error";
		    			$arr_rs["datetime"] = $date_now;
		    			$arr_rs["data"] = "";
		    			$arr_rs["remark"] = "บันทึกข้อมูลไม่สำเร็จ กรุณาติดต่อผู้ดูแลระบบ";
		    			echo json_encode($arr_rs);
		    			die();
					}//end else

				}else{
					$arr_rs = array();
	    			$arr_rs["status"] = "error";
	    			$arr_rs["datetime"] = $date_now;
	    			$arr_rs["data"] = "";
	    			$arr_rs["remark"] = "Not found member_id or member_id invalid";
	    			echo json_encode($arr_rs);
	    			die();
				}//end else			
				
			}//end if rs_chk_token

        }//end if update_member_profile
//------------------------------------- END NEW API ---------------------------------// 
        
        else if ( $request_url=="approve_member_in_course_detail"){
        	// d($_POST); die();

        	$cid = !empty($_POST["cid"]) ? trim($_POST["cid"]) : "";
        	$course_id = !empty($_POST["course_id"]) ? trim($_POST["course_id"]) : "";
        	$course_detail_id = !empty($_POST["course_detail_id"]) ? trim($_POST["course_detail_id"]) : "";
        	$smcard_report_id = !empty($_POST["smcard_report_id"]) ? trim($_POST["smcard_report_id"]) : "";
        	$smcard_stamp_type_id = !empty($_POST["smcard_stamp_type_id"]) ? trim($_POST["smcard_stamp_type_id"]) : "";
        	$datetime_stamp = !empty($_POST["datetime_stamp"]) ? trim($_POST["datetime_stamp"]) : $date_now;
        	$time_start = !empty($_POST["time_start"]) ? trim($_POST["time_start"]) : "";
        	$time_stop = !empty($_POST["time_stop"]) ? trim($_POST["time_stop"]) : "";

        	$token_key = !empty($_POST["token_key"]) ? trim($_POST["token_key"]) : "";
        	$rs_chk_token = token_key_authen($token_key);

        	if ( $rs_chk_token==true ) {

        		if ( empty($cid) ) {
        			$arr_rs = array();
        			$arr_rs["status"] = "error";
        			$arr_rs["datetime"] = $date_now;
        			$arr_rs["data"] = "";
        			$arr_rs["remark"] = " Not found ID card number";
        			echo json_encode($arr_rs);
        			die();

        		}elseif ( empty($smcard_report_id) ) {
        			$arr_rs = array();
        			$arr_rs["status"] = "error";
        			$arr_rs["datetime"] = $date_now;
        			$arr_rs["data"] = "";
        			$arr_rs["remark"] = " Not found smcard_report_id ";
        			echo json_encode($arr_rs);
        			die();

        		}elseif ( empty($course_id) ) {
        			$arr_rs = array();
        			$arr_rs["status"] = "error";
        			$arr_rs["datetime"] = $date_now;
        			$arr_rs["data"] = "";
        			$arr_rs["remark"] = " Not found Course ID ";
        			echo json_encode($arr_rs);
        			die();
        		
        		}elseif ( empty($smcard_stamp_type_id) ) {
        			$arr_rs = array();
        			$arr_rs["status"] = "error";
        			$arr_rs["datetime"] = $date_now;
        			$arr_rs["data"] = "";
        			$arr_rs["remark"] = " Not found smcard_stamp_type_id ";
        			echo json_encode($arr_rs);
        			die();
        		
        		}elseif ( empty($course_detail_id) ) {
        			$arr_rs = array();
        			$arr_rs["status"] = "error";
        			$arr_rs["datetime"] = $date_now;
        			$arr_rs["data"] = "";
        			$arr_rs["remark"] = " Not found Course_detail ID ";
        			echo json_encode($arr_rs);
        			die();

        		}elseif ( empty($time_start) && ($smcard_stamp_type_id==1 || $smcard_stamp_type_id==2) ) {
        			$arr_rs = array();
        			$arr_rs["status"] = "error";
        			$arr_rs["datetime"] = $date_now;
        			$arr_rs["data"] = "";
        			$arr_rs["remark"] = "time_start is not empty";
        			echo json_encode($arr_rs);
        			die();

        		}elseif ( empty($time_stop) && ($smcard_stamp_type_id==1 || $smcard_stamp_type_id==2) ) {
        			$arr_rs = array();
        			$arr_rs["status"] = "error";
        			$arr_rs["datetime"] = $date_now;
        			$arr_rs["data"] = "";
        			$arr_rs["remark"] = "time_stop is not empty";
        			echo json_encode($arr_rs);
        			die();

        		}else{

        			$member_info = view_member(" AND a.active='T' AND a.cid='{$cid}'");
        			$member_info = $member_info[0];
        			// d($member_info);
        			$member_id = $member_info["member_id"];

        			$q = "SELECT b.register_id
        					, c.register_course_detail_id
        					, c.course_id
        					, c.course_detail_id
        				FROM register AS b 
        				LEFT JOIN register_course_detail AS c ON c.register_id=b.register_id
        				WHERE b.active='T' 
        					AND b.member_id={$member_id}
        					AND c.active='T' 
							AND c.course_id={$course_id}
							AND c.course_detail_id={$course_detail_id}
        			";
        			// echo $q."<hr>";
        			$reg_info = $db->rows($q);
        			// d($reg_info);
        			// $member_id = $reg_info["member_id"];
        			$register_id = $reg_info["register_id"];
        			$register_course_detail_id = $reg_info["register_course_detail_id"];

        			if ( empty($member_info) ) {
        				$arr_rs = array();
	        			$arr_rs["status"] = "error";
	        			$arr_rs["datetime"] = $date_now;
	        			$arr_rs["data"] = "";
	        			$arr_rs["remark"] = "You don't have a member of system";
	        			echo json_encode($arr_rs);
	        			die();
        			}else if ( empty($register_course_detail_id) ) {
        				$arr_rs = array();
	        			$arr_rs["status"] = "error";
	        			$arr_rs["datetime"] = $date_now;
	        			$arr_rs["data"] = "";
	        			$arr_rs["remark"] = "คุณยังไม่ได้ลงทะเบียนในรอบนี้";
	        			echo json_encode($arr_rs);
	        			die();
        			}else{

        				$authen_info = get_user_authen($token_key);
						// var_dump($authen_info); die();
						$authen_info = unserialize($authen_info);
						$emp_id = $authen_info["emp_id"];

        				$cond = "";
						$cond .= " AND a.active='T' 
							AND a.smcard_report_id=1
							AND a.smcard_stamp_type_id=5
							AND a.register_id={$register_id}
							AND a.register_course_detail_id={$register_course_detail_id}
							AND a.course_detail_id={$course_detail_id}
							AND a.member_id='{$member_id}'
						";
						$info = get_smcard_report_list($cond);

						// d($info);
						foreach ($info as $key => $v) {
							$smcard_report_list_id = $v["smcard_report_list_id"];

							$args = array();
							$args["table"] = "smcard_report_list";
							$args["id"] = $smcard_report_list_id;
							$args["recby_id"] = (int)$emp_id;
							$args["rectime"] = $date_now;
							$args["active"] = 'F';

							$db->set($args);
						}//end loop $v
        				
						unset($smcard_report_list_id);
						$args = array();
		    			$args["table"] = "smcard_report_list";
		    			$args["emp_id"] = $emp_id;
		    			$args["recby_id"] = $emp_id;
		    			$args["register_id"] = $register_id;
		    			$args["member_id"] = $member_id;
		    			$args["register_course_detail_id"] = $register_course_detail_id;
		    			$args["course_id"] = $course_id;
		    			$args["course_detail_id"] = $course_detail_id;
		    			$args["cid"] = $cid;
		    			$args["smcard_report_id"] = $smcard_report_id;
		    			$args["smcard_stamp_type_id"] = $smcard_stamp_type_id;
		    			$args["datetime_stamp"] = $datetime_stamp;
		    			$args["time_start"] = !empty($time_start) ? $time_start : "00:00:00";
		    			$args["time_stop"] = !empty($time_stop) ? $time_stop : "00:00:00";

		    			$smcard_report_list_id = $db->set($args);
		    			// var_dump($db->set($args, true, true));

		    			if ( $smcard_stamp_type_id==5 ) {
		    				
			    			$args = array();
			    			$args["table"] = "smcard_report_list";
			    			$args["emp_id"] = $emp_id;
			    			$args["recby_id"] = $emp_id;
			    			$args["register_id"] = $register_id;
			    			$args["member_id"] = $member_id;
			    			$args["register_course_detail_id"] = $register_course_detail_id;
			    			$args["course_id"] = $course_id;
			    			$args["course_detail_id"] = $course_detail_id;
			    			$args["cid"] = $cid;
			    			$args["smcard_report_id"] = $smcard_report_id;
			    			$args["smcard_stamp_type_id"] = 1;
			    			$args["datetime_stamp"] = $datetime_stamp;
			    			$args["time_start"] = !empty($time_start) ? $time_start : "00:00:00";
			    			$args["time_stop"] = !empty($time_stop) ? $time_stop : "00:00:00";

			    			$db->set($args);
			    			// var_dump($db->set($args, true, true));
		    			}//end if


		    			if ( !empty($smcard_report_list_id) ) {
		    				$arr_rs = array();
		    				$arr_rs["status"] = "success";
		    				$arr_rs["datetime"] = $date_now;
		    				$arr_rs["data"] = "";
		    				$arr_rs["remark"] = "บันทึกเวลาเรียบร้อย";
		    				echo json_encode($arr_rs);
        					die();
		    			}else{
		    				$arr_rs = array();
		        			$arr_rs["status"] = "error";
		        			$arr_rs["datetime"] = $date_now;
		        			$arr_rs["data"] = "";
		        			$arr_rs["remark"] = "บันทึกไม่สำเร็จ";
		        			echo json_encode($arr_rs);
		        			die();
		    			}//end if

        			}//end else

        		}//end else

        	}//end if

        }//end else if

//------------------------------------------------- GET ------------------------------------//
//------------------------------------------------- GET ------------------------------------//
//------------------------------------------------- GET ------------------------------------//
  
    }else if ( $request_method==="GET" ) {
		
    	if ( $request_url=="user_authen" ){
    		$user = !empty($_GET["user"]) ? $_GET["user"] : "";
    		$passwd = !empty($_GET["passwd"]) ? $_GET["passwd"] : "";
    		$auth_api_token_type_id = !empty($_GET["token_type"]) ? $_GET["token_type"] : "";

    		$q = "SELECT login_id, emp_id FROM login WHERE username='{$user}' AND `password`='{$passwd}'";
    		$login_info = $db->rows($q);

    		$login_id = $login_info["login_id"];
    		$emp_id = $login_info["emp_id"];

    		if ( !empty($login_id) ) {
    			$token = gen_passwords(30);

    			$q = "SELECT data_value
					FROM config
					WHERE `name`='smcard_token_expire'
				";
				$data_value = $db->data($q);

				if ( !empty($data_value) ) {
					$tmp = array();
					$tmp = explode(",", $data_value);
					$number = (int)$tmp[0];
					$time_unit_id = (int)$tmp[1];

					$q = "SELECT name_eng FROM time_unit WHERE time_unit_id={$time_unit_id}";
					$time_unit_name = $db->data($q);
    				$token_expire_date = get_increase_duration($number, $time_unit_name, "", true);
				}else{
					$arr_rs = array();
	    			$arr_rs["status"] = "error";
	    			$arr_rs["datetime"] = $date_now;
	    			$arr_rs["data"] = "";
	    			$arr_rs["remark"] = "ไม่สามารถคำนวนวันหมดอายุของ token ได้";
	    			echo json_encode($arr_rs);
	    			die();
				}//end else

    			$args = array();
    			$args["table"] = "auth_api_token_list";
    			$args["login_id"] = $login_id;
    			$args["emp_id"] = $emp_id;
    			$args["auth_api_token_type_id"] = $auth_api_token_type_id;
    			$args["token_key"] = $token;
    			$args["token_expire_date"] = $token_expire_date;
    			$auth_api_token_list_id = $db->set($args);

    			if ( !empty($auth_api_token_list_id) ) {
    				$arr_rs = array();
    				$arr_rs["status"] = "success";
    				$arr_rs["datetime"] = $date_now;
    				$arr_rs["data"] = $token;
    				$arr_rs["remark"] = "Your token key";
    				echo json_encode($arr_rs);
    				die();
    			}//end if

    		}else{
    			$arr_rs = array();
    			$arr_rs["status"] = "error";
    			$arr_rs["datetime"] = $date_now;
    			$arr_rs["data"] = "";
    			$arr_rs["remark"] = "authen fail !!";
    			echo json_encode($arr_rs);
    			die();
			}//end else


		}else if( $request_url=="member_info" ) {

			$cid = !empty($_GET["cid"]) ? trim($_GET["cid"]) : "";
			$token_key = !empty($_GET["token_key"]) ? $_GET["token_key"] : "";

			$rs_chk_token = token_key_authen($token_key);
			if ( $rs_chk_token==true ) {

				if ( empty($cid) ) {
					$arr_rs = array();
					$arr_rs["status"] = "error";
					$arr_rs["datetime"] = $date_now;
					$arr_rs["data"] = "";
					$arr_rs["remark"] = "Not found ID card number";
					echo json_encode($arr_rs);
					die();

				}else{

					$q = "SELECT
							m.member_id,
							m.title_th,
							m.fname_th,
							m.lname_th,
							m.cid,
							m.gender,
							m.birthdate
						FROM member AS m
						WHERE active='T'
							AND cid='{$cid}'
					";
					$rs = $db->rows($q);
					//var_dump($rs);

					if (!empty($rs)){

						$arr_rs = array();
						$arr_rs["status"] = "success";
						$arr_rs["datetime"] = $date_now;
						$arr_rs["data"] = $rs;
						$arr_rs["remark"] = "You have a member of system";
						echo json_encode($arr_rs);

					}else{

						$arr_rs = array();
						$arr_rs["status"] = "error";
						$arr_rs["datetime"] = $date_now;
						$arr_rs["data"] = "";
						$arr_rs["remark"] = "You don't have a member of system";
						echo json_encode($arr_rs);
					}//end else

				}//end else

			}else{
				$arr_rs = array();
				$arr_rs["status"] = "error";
				$arr_rs["datetime"] = $date_now;
				$arr_rs["data"] = "";
				$arr_rs["remark"] = "Not access !!";
				echo json_encode($arr_rs);
			}//end else


		}else if ( $request_url=="course_detail" ) {
			$start_date = !empty($_GET["start_date"]) ? $_GET["start_date"] : "";
			$stop_date = !empty($_GET["stop_date"]) ? $_GET["stop_date"] : "";
			$course_type = !empty($_GET["course_type"]) ? $_GET["course_type"] : "";
			$section_id = !empty($_GET["section_id"]) ? $_GET["section_id"] : "";
			$token_key = !empty($_GET["token_key"]) ? $_GET["token_key"] : "";

			$rs_chk_token = token_key_authen($token_key);
			if ( $rs_chk_token==true ) {

				if ( empty($start_date) && empty($stop_date) ) {
					$arr_rs = array();
					$arr_rs["status"] = "error";
					$arr_rs["datetime"] = $date_now;
					$arr_rs["data"] = "";
					$arr_rs["remark"] = "Not found start_date or stop_date";
					echo json_encode($arr_rs);
					die();
				}//end if

				$cond = "";
				if ( !empty($course_type) ) {
					$cond .= " AND cd.coursetype_id = '{$course_type}'";
				}//end if
				if ( !empty($section_id) ) {
					$cond .= " AND cd.section_id = '{$section_id}'";
				}//end if

				$q = "SELECT
						cd.course_detail_id,
						cd.course_id,
						c.title AS course_name,
						cd.coursetype_id,
						ct.name AS type_name,
						cd.section_id,
						s.name AS section_name,
						cd.`day`,
						cd.date,
						cd.time,
						cd.address,
						cd.address_detail,
						cd.`status`,
						cd.open_regis,
						cd.end_regis,
						cd.sit_all,
						cd.chair_all,
						cd.inhouse,
						cd.active
					FROM course_detail AS cd 
					LEFT JOIN course AS c ON c.course_id=cd.course_id 
					LEFT JOIN coursetype AS ct ON ct.coursetype_id=cd.coursetype_id 
					LEFT JOIN section AS s ON s.section_id=cd.section_id 
					WHERE cd.active='T'
				-- AND cd.coursetype_id = '{$course_type}'
				{$cond}
				AND date BETWEEN '{$start_date}' AND '{$stop_date}'
				";

				$rs = $db->get($q);
				//echo json_encode($rs);

				$arr_rs = array();
				$arr_rs["status"] = "success";
				$arr_rs["datetime"] = $date_now;
				$arr_rs["data"] = $rs;
				$arr_rs["remark"] = "";
				echo json_encode($arr_rs);

			}else{

				$arr_rs = array();
				$arr_rs["status"] = "success";
				$arr_rs["datetime"] = $date_now;
				$arr_rs["data"] = "";
				$arr_rs["remark"] = "Not access !!";
				echo json_encode($arr_rs);
			}//end else
		

		}

//------------------------------------- NEW API --------------------------------------//
		else if ( $request_url=="corparation_list" ) {
			$slip_type = !empty($_GET["slip_type"]) ? $_GET["slip_type"] : "";
			$token_key = !empty($_GET["token_key"]) ? $_GET["token_key"] : "";

			$rs_chk_token = token_key_authen($token_key);
			if ( $rs_chk_token==true ) {

				if ( empty($slip_type) ) {
					$arr_rs = array();
					$arr_rs["status"] = "error";
					$arr_rs["datetime"] = $date_now;
					$arr_rs["data"] = "";
					$arr_rs["remark"] = "Not found slip_type";
					echo json_encode($arr_rs);
					die();
				}//end if

				$q =" SELECT
							a.receipt_id,
							-- a. CODE,
							a. NAME AS corparation_name ,
							a.name_eng,
							a.receipttype_id,
							b.NAME AS receipttype_name,
							a. NO,
							a.gno,
							a.moo,
							a.soi,
							a.road,
							a.district_id,
							d. NAME AS district_name,
							a.amphur_id,
							e. NAME AS amphur_name,
							a.province_id,
							c. NAME AS province_name,
							a.postcode,
							-- a.address_other,
							a.taxno,
							a.branch,
							a.active
							-- a.recby_id,
							-- a.rectime,
							-- a.remark
						FROM receipt a
						LEFT JOIN receipttype b ON a.receipttype_id = b.receipttype_id
						LEFT JOIN province c ON c.province_id = a.province_id
						LEFT JOIN district d ON d.district_id = a.district_id
						LEFT JOIN amphur e ON e.amphur_id = a.amphur_id
						WHERE
							a.active != ''
						-- LIMIT 10
					";


					$rs = $db->get($q);
						//echo json_encode($rs);
						$arr_rs = array();
						$arr_rs["status"] = "success";
						$arr_rs["datetime"] = $date_now;
						$arr_rs["data"] = $rs;
						$arr_rs["remark"] = "";
						echo json_encode($arr_rs);

			}else{

				$arr_rs = array();
				$arr_rs["status"] = "success";
				$arr_rs["datetime"] = $date_now;
				$arr_rs["data"] = "";
				$arr_rs["remark"] = "Not access !!";
				echo json_encode($arr_rs);
			}//end else

		}//end else if

		//------------------------------------- END NEW API ---------------------------//

		//------------------------------------- NEW API -------------------------------//
		else if( $request_url=="check_member_in_course_detail" ){

			$cid = !empty($_GET["cid"]) ? trim($_GET["cid"]) : "";
			// $title_th = !empty($_GET["title_th"]) ? trim($_GET["title_th"]) : "";
			$fname_th = !empty($_GET["fname_th"]) ? trim($_GET["fname_th"]) : "";
			$lname_th = !empty($_GET["lname_th"]) ? trim($_GET["lname_th"]) : "";
			$course_detail_id = !empty($_GET["course_detail_id"]) ? trim($_GET["course_detail_id"]) : "";
			$token_key = !empty($_GET["token_key"]) ? $_GET["token_key"] : "";	

			$rs_chk_token = token_key_authen($token_key);
			if ( $rs_chk_token==true ) {

				$authen_info = get_user_authen($token_key);
				// var_dump($authen_info); die();
				$authen_info = unserialize($authen_info);
				$emp_id = $authen_info["emp_id"];

				if ( empty($cid) ) {
					$arr_rs = array();
					$arr_rs["status"] = "error";
					$arr_rs["datetime"] = $date_now;
					$arr_rs["data"] = "";
					$arr_rs["remark"] = "Not found ID card number";
					echo json_encode($arr_rs);
					die();
				}else if ( empty($fname_th) ) {
					$arr_rs = array();
					$arr_rs["status"] = "error";
					$arr_rs["datetime"] = $date_now;
					$arr_rs["data"] = "";
					$arr_rs["remark"] = "Not found fname_th";
					echo json_encode($arr_rs);
					die();
				}else if ( empty($lname_th) ) {
					$arr_rs = array();
					$arr_rs["status"] = "error";
					$arr_rs["datetime"] = $date_now;
					$arr_rs["data"] = "";
					$arr_rs["remark"] = "Not found lname_th";
					echo json_encode($arr_rs);
					die();
				}else if ( empty($course_detail_id) ) {
					$arr_rs = array();
					$arr_rs["status"] = "error";
					$arr_rs["datetime"] = $date_now;
					$arr_rs["data"] = "";
					$arr_rs["remark"] = "Not found course_detail_id";
					echo json_encode($arr_rs);
					die();
				}//end else if


				$q = "SELECT
						a.register_course_detail_id,
						a.register_id,
						a.course_id,
						a.course_detail_id,
						a.active,
						b.member_id,
						b.cid,
						b.title,
						b.fname,
						b.lname
					FROM register_course_detail AS a
					LEFT JOIN register AS b ON b.register_id=a.register_id
					WHERE a.active='T' AND b.active='T'
						AND b.pay_status IN (3,5,6)
						AND a.course_detail_id={$course_detail_id}
						AND ( b.cid='{$cid}' OR ( b.fname='{$fname_th}' AND b.lname='{$lname_th}' ) )
				";
				// echo $q;
				$reg_info = $db->get($q);
				unset($q);
				// d($reg_info);
				// die();

				if ( !empty($reg_info) ) {
					$reg_info = $reg_info[0];
					$register_id = $reg_info["register_id"];
					$register_course_detail_id = $reg_info["register_course_detail_id"];
					$member_id = $reg_info["member_id"];
					$course_id = $reg_info["course_id"];
					unset($reg_info);

					$info = view_member("", $member_id);
					$member_info = $info[0];
					unset($info);
					// d($member_info);
					// unset($member_info);

					if ( !empty($member_info) ) {
						$member_cid = $member_info["cid"];
						$member_title_th = $member_info["title_th"];
						$member_title_th_text = $member_info["title_th_text"];
						$member_fname_th = $member_info["fname_th"];
						$member_lname_th = $member_info["lname_th"];

						if ( $member_title_th=="อื่นๆ" ) {
							$member_title_th = $member_title_th_text;
						}//end if

						$tmp = array();
						$tmp["member_id"] = $member_id;
						$tmp["cid"] = $member_cid;
						$tmp["title_th"] = $member_title_th;
						$tmp["fname_th"] = $member_fname_th;
						$tmp["lname_th"] = $member_lname_th;

						if ( $member_cid!=$cid ) {
							$cond = "";
							$cond .= " AND a.active='T' 
								AND a.smcard_report_id=1
								AND a.smcard_stamp_type_id=6
								AND a.register_id={$register_id}
								AND a.register_course_detail_id={$register_course_detail_id}
								AND a.course_detail_id={$course_detail_id}
								AND a.member_id='{$member_id}'
							";
							$info = get_smcard_report_list($cond);

							// d($info);
							foreach ($info as $key => $v) {
								$smcard_report_list_id = $v["smcard_report_list_id"];

								$args = array();
								$args["table"] = "smcard_report_list";
								$args["id"] = $smcard_report_list_id;
								$args["recby_id"] = (int)$emp_id;
								$args["rectime"] = $date_now;
								$args["active"] = 'F';

								$db->set($args);
							}//end loop $v

							$args = array();
							$args["table"] = "smcard_report_list";
							$args["register_id"] = (int)$register_id;
							$args["register_course_detail_id"] = (int)$register_course_detail_id;
							$args["member_id"] = (int)$member_id;
							$args["course_id"] = (int)$course_id;
							$args["course_detail_id"] = (int)$course_detail_id;
							$args["datetime_stamp"] = $date_now;
							$args["smcard_report_id"] = 1;
							$args["smcard_stamp_type_id"] = 6;
							$args["cid"] = $cid;
							$args["time_start"] = "00:00:00";
		    				$args["time_stop"] = "00:00:00";
							$args["emp_id"] = (int)$emp_id;
							$args["recby_id"] = (int)$emp_id;
							$args["remark"] = "เลขบัตรประชาชนไม่ตรงกัน ID card: {$cid}, ID WR: {$member_cid}";
							$db->set($args);


							$arr_rs = array();
							$arr_rs["status"] = "error";
							$arr_rs["datetime"] = $date_now;
							$arr_rs["data"] = $tmp;
							$arr_rs["remark"] = "เลขบัตรประชาชนไม่ตรงกัน";
							echo json_encode($arr_rs);
							die();
						}else{
							$arr_rs = array();
							$arr_rs["status"] = "success";
							$arr_rs["datetime"] = $date_now;
							$arr_rs["data"] = $tmp;
							$arr_rs["remark"] = "พบข้อมูลของคุณที่ได้ลงทะเบียนในรอบกิจกรรมนี้";
							echo json_encode($arr_rs);
							die();
						}//end else

					}else{
						$arr_rs = array();
						$arr_rs["status"] = "error";
						$arr_rs["datetime"] = $date_now;
						$arr_rs["data"] = "";
						$arr_rs["remark"] = "ไม่พบสมาชิกท่านนี้ในระบบ";
						echo json_encode($arr_rs);
						die();
					}//end else
				}else{
					$arr_rs = array();
					$arr_rs["status"] = "error";
					$arr_rs["datetime"] = $date_now;
					$arr_rs["data"] = "";
					$arr_rs["remark"] = "คุณยังไม่ได้ลงทะเบียนในรอบนี้";
					echo json_encode($arr_rs);
					die();
				}//end else
			}else{
				$arr_rs = array();
				$arr_rs["status"] = "error";
				$arr_rs["datetime"] = $date_now;
				$arr_rs["data"] = "";
				$arr_rs["remark"] = "Not access !!";
				echo json_encode($arr_rs);
			}//end else

		}//end else if check_member_in_course_detail
//------------------------------------- END NEW API ---------------------------------//




//----------------------------------------NEW API -----------------------------------//
		else if ( $request_url=="check_course_detail" ) {
			
			$course_id = !empty($_GET["course"]) ? trim($_GET["course"]) : "9999";
			$course_detail_id = !empty($_GET["course_detail"]) ? trim($_GET["course_detail"]) : "";
			$token_key = !empty($_GET["token_key"]) ? $_GET["token_key"] : "";
			$rs_chk_token = token_key_authen($token_key);
			if ( $rs_chk_token==true ) {

				if ( empty($course_id) ) {
					$arr_rs = array();
					$arr_rs["status"] = "error";
					$arr_rs["datetime"] = $date_now;
					$arr_rs["data"] = "";
					$arr_rs["remark"] = " Not found Course ID ";
					echo json_encode($arr_rs);
					die();

				}elseif ( empty($course_detail_id) ) {
					$arr_rs = array();
					$arr_rs["status"] = "error";
					$arr_rs["datetime"] = $date_now;
					$arr_rs["data"] = "";
					$arr_rs["remark"] = " Not found Course_detail ID ";
					echo json_encode($arr_rs);
					die();

				}//end else if
				
				//chk ic exam
				$q="SELECT
						a.course_detail_id,
						a.course_id,
						a.coursetype_id,
						a.section_id,
						a.`day`,
						a.date,
						a.time,
						a.address,
						a.address_detail,
						a.`status`,
						a.open_regis,
						a.end_regis,
						a.sit_all,
						a.chair_all,
						a.inhouse,
						a.active
					FROM course_detail a
					WHERE a.active ='T'
					AND a.course_detail_id = {$course_detail_id}
				";

				$check_course = $db->rows($q);
				//var_dump($rs);
				 // echo json_encode($check_course); 
				// d($check_course);
				$section_id = $check_course["section_id"];
				$coursetype_id = $check_course["coursetype_id"];

				if($section_id==1 && $coursetype_id==2){
 				// echo "IC exam";
					$q="SELECT course_id,title FROM course WHERE active = 'T' AND section_id = {$section_id} AND coursetype_id = {$coursetype_id}";
					$course = $db->get($q);
     				// d($course);

					$tmp = array();
					foreach ($course as $key => $v) {
						$v["course_detail_id"] = $course_detail_id;
     					// $v["title"]
     					// $v["course_id"] 
						$tmp[] = $v;
     					// d($tmp);die();

				     }//end loop $v

				     $rs = $tmp;
				     // d($rs);
				     $arr_rs = array();
				     $arr_rs["status"] = "success";
				     $arr_rs["datetime"] = $date_now;
				     $arr_rs["data"] = $rs;
				     $arr_rs["remark"] = "";
				     //d($arr_rs);
					echo json_encode($arr_rs);	

				 }else{
				 	// echo "NOT IC exam";
				 	$q = "SELECT title FROM course WHERE course_id={$course_id}";
				 	$course_name = $db->data($q);

				 	$rs = array();
				 	$rs["course_id"] = $course_id;
				 	$rs["course_name"] = $course_name;
				 	$rs["course_detail_id"] = $course_detail_id;

				 	$arr_rs = array();
				 	$arr_rs["status"] = "success";
				 	$arr_rs["datetime"] = $date_now;
				 	$arr_rs["data"] = $rs;
				 	$arr_rs["remark"] = "";
					// d($arr_rs);
				 	echo json_encode($arr_rs);

				 }


			}else{

				$arr_rs = array();
				$arr_rs["status"] = "error";
				$arr_rs["datetime"] = $date_now;
				$arr_rs["data"] = "";
				$arr_rs["remark"] = "ไม่พบข้อมูล";
				echo json_encode($arr_rs);
			}//end else



		}//end $request_url=="check_course_detail"



//------------------------------------- END NEW API ---------------------------------//


//------------------------------------- NEW API --------------------------------------//
		else if ( $request_url=="check_register_in_course_detail" ) {
			$course_id = !empty($_GET["course_id"]) ? $_GET["course_id"] : "9999";
			$course_detail_id = !empty($_GET["course_detail_id"]) ? $_GET["course_detail_id"] : "";
			$smcard_stamp_type_id = !empty($_GET["smcard_stamp_type_id"]) ? $_GET["smcard_stamp_type_id"] : "";
			
			$token_key = !empty($_GET["token_key"]) ? $_GET["token_key"] : "";

			$rs_chk_token = token_key_authen($token_key);
			if ( $rs_chk_token==true ) {

				if ( empty($course_detail_id) ) {
					$arr_rs = array();
					$arr_rs["status"] = "error";
					$arr_rs["datetime"] = $date_now;
					$arr_rs["data"] = "";
					$arr_rs["remark"] = "Not found course_detail";
					echo json_encode($arr_rs);
					die();
				}//end if

				if ( empty($smcard_stamp_type_id) ) {
					$arr_rs = array();
					$arr_rs["status"] = "error";
					$arr_rs["datetime"] = $date_now;
					$arr_rs["data"] = "";
					$arr_rs["remark"] = "Not found smcard_stamp_type";
					echo json_encode($arr_rs);
					die();
				}//end if

				$cond = "";
				if ( !empty($course_detail_id) ) {
					$cond .= " AND a.course_detail_id = '{$course_detail_id}'";
				}//end if

				if ( !empty($smcard_stamp_type_id) ) {
					$cond .= " AND a.smcard_stamp_type_id = '{$smcard_stamp_type_id}'";
				}//end if


				$q = "SELECT
						a.smcard_report_list_id,
						a.course_id,
						a.course_detail_id,
						a.member_id,
						a.smcard_stamp_type_id,
						a.datetime_stamp,
						a.time_start,
						a.time_stop,
						b.title_th,
						b.fname_th,
						b.lname_th
					FROM smcard_report_list a
					LEFT JOIN member b ON a.member_id = b.member_id
					WHERE a.active = 'T'
					{$cond}
					ORDER BY a.smcard_report_list_id DESC
				";

				$rs = $db->get($q);
				//echo json_encode($rs);

				$arr_rs = array();
				$arr_rs["status"] = "success";
				$arr_rs["datetime"] = $date_now;
				$arr_rs["data"] = $rs;
				$arr_rs["remark"] = "";
				echo json_encode($arr_rs);

			}else{

				$arr_rs = array();
				$arr_rs["status"] = "error";
				$arr_rs["datetime"] = $date_now;
				$arr_rs["data"] = "";
				$arr_rs["remark"] = "Not access !!";
				echo json_encode($arr_rs);
			}//end else
		

		}//end $request_url=="register_in_course_detail"
		


//------------------------------------- END NEW API ---------------------------------//

	
	}else if ( $request_method==="PUT" ) {

	}//end else if



}//end if



?>