<?php
include_once "./share/authen.php";
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
global $db;
require_once dirname(__FILE__) . '/PHPExcel.php';
$sec_id = $_REQUEST['sec_id'];
$costy_id = $_REQUEST['cos_id'];
$pay_by = $_REQUEST['pay_by'];
$cos_dtail = $_REQUEST['cos_dtail'];
$name = $_REQUEST['name'];
$date_start_th = $_REQUEST['date_start'];
$date_stop_th = $_REQUEST['date_stop'];
$ar_date_st = explode("-", $date_start_th);
$ar_date_st[2] = $ar_date_st[2]-543;
$date_start = $ar_date_st[2]."-".$ar_date_st[1]."-".$ar_date_st[0];
$ar_date_en = explode("-", $date_stop_th);
$ar_date_en[2] = $ar_date_en[2]-543;
$date_stop = $ar_date_en[2]."-".$ar_date_en[1]."-".$ar_date_en[0];
$cond = "";
if ( (isset($date_start) && !empty($date_start)) || (isset($date_stop) && !empty($date_stop)) ) {
	if ( (isset($date_start) && !empty($date_start)) && !(isset($date_stop) && !empty($date_stop)) ) {
		$cond .= " AND cd.date='$date_start'";
	}
	if ( !(isset($date_start) && !empty($date_start)) && (isset($date_stop) && !empty($date_stop)) ) {
		$cond .= " AND cd.date='$date_stop'";
	}else{
		$cond .= " AND (cd.date BETWEEN '$date_start' AND '$date_stop')"; 
	}
}
$qry = "SELECT DISTINCT cd.code_project
		FROM course_detail AS cd
		WHERE cd.active='T' AND cd.inhouse='F' 
			AND cd.code_project<>''
			AND cd.coursetype_id=2
			AND cd.section_id=1 
			$cond
		GROUP BY cd.code_project
	;";
$rs = $db->get($qry);
//d($rs);
$proj_codes = array();
foreach ($rs as $key => $rs) {
	$proj_codes[] = $rs["code_project"];
}
//d($proj_codes);
$cos_dt_data = array();
foreach ($proj_codes as $key => $proj_code) {
	$tmp_cos_dt = array();
	$qry = "SELECT cd.course_detail_id
		FROM course_detail AS cd
		WHERE cd.active='T'  
			AND cd.code_project<>''
			AND cd.coursetype_id=2
			AND cd.section_id=1 
			AND cd.code_project='{$proj_code}' 
			$cond
	";
	$course_detail_ids = $db->get($qry);
	//d($course_detail_ids);
	//die();
	$ids = array();
	$list_ids = null;
	foreach ($course_detail_ids as $key => $cd_id) {
		$ids[] = $cd_id["course_detail_id"];
		$list_ids .= $cd_id["course_detail_id"].",";
	}// end loop
	$tmp_cos_dt["proj_code"] = $proj_code;
	$tmp_cos_dt["count_cos_id"] = count($ids);
	$tmp_cos_dt["ids"] = trim($list_ids, ",");
	//d($tmp_cos_dt);
	//die();
	$cos_dt_data[] = $tmp_cos_dt;
}// end loop for (main)
//d($cos_dt_data);
?>
<!DOCTYPE html>
<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>สรุปยอดตามเลขที่โครงการ</title>
<link href="css/printform-style.css" rel="stylesheet" type="text/css" media="all" />
<style>
	@media print{
		body{
			padding: 0px;
		}
		#btPrint{
			display: none;
		}
	}
	.hr{
		margin-top: 0px;
		margin-bottom: 0px;
		border-top: 1px solid black;
	}
</style>
<body onload="receipt_tax_export()">
<!-- <body onload=""> -->
	<div class="form-landscape">
		<div>
			<table class="no-border">
				<tbody>
					<tr>
						<td><span class="center font-weight-bold">รายงานสรุปรายได้ ตามเลขที่โครงการ</span></td>
					</tr>
					<tr>
						<td><span class="center"><?php echo "ระหว่างวันที่ ".$date_start_th." ถึง ".$date_stop_th;?></span></td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- <div> -->				
<?php 			
			$recc=1;
			$i=0;
			$rs_ttl = count($rs);
			$ttl_page = intval($rs_ttl/10)+1;
			$ttl_count_cos = 0;
			$ttl_count_human_all_cos = 0;
			$ttl_count_human_per_cos = 0;
			$ttl_sum_price = 0;
			foreach ($cos_dt_data as $key => $value) {
				$proj_code = $value["proj_code"];
				$count_cos_id = $value["count_cos_id"];
				$ttl_count_cos = $ttl_count_cos + $count_cos_id;
				$ids = $value["ids"];
				$arr_ids = explode(",", $ids);
				$disp_date = null;
				$disp_time = null;
				$disp_open_regis = 0;
				$disp_sum_chair_all = 0;
				$disp_chair_all = null;
				$disp_sit_all = 0;
				$count_tag_hr = 0;
				$disp_price_per_proj = 0;
				$disp_price_per_cos = null;
				$disp_count_human = null;
				foreach ($arr_ids as $key => $cos_dt_id) {
					//echo $proj_code." -> ".$cos_dt_id."<br>";
					$qry = "SELECT cd.course_detail_id
								, cd.address
								, cd.address_detail
								, cd.date
								, cd.time
								, cd.sit_all
								, cd.chair_all
								, cd.book
								, cd.regis_all
								, cd.book_all
								, SUM(cd.chair_all) AS ttl_chair_all
							FROM course_detail AS cd	
							WHERE cd.active='T'	AND cd.course_detail_id={$cos_dt_id}
						";
					$rs = $db->get($qry);					
					$cos_dt_info = $rs[0];
					//d($cos_dt_info);
					$disp_address = $cos_dt_info["address"];
					$disp_address_dt = $cos_dt_info["address_detail"];
					//$disp_sum_chair_all = $disp_sum_chair_all+$cos_dt_info["chair_all"];

					//count reg_id for pay_status == 6
					$qry = "SELECT count(r.register_id) AS count_human
							FROM register AS r
							WHERE r.active='T' 
								AND ( r.pay_status = 6 ) 
								AND (r.course_detail_id LIKE '%$cos_dt_id%')
						";
					$rs = $db->get($qry);
					$reg_info = $rs[0];	
					$count_human = $reg_info["count_human"];
					//count reg_id for pay_status == 3,5
					$qry = "SELECT count(r.register_id) AS count_human
								, SUM(r.pay_price) AS sum_price
							FROM register AS r
							WHERE r.active='T' 
								AND ( r.pay_status IN (3,5) ) 
								AND (r.course_detail_id LIKE '%$cos_dt_id%')
						";
					$rs = $db->get($qry);
					$reg_info = $rs[0];	
					//d($reg_info);
					//die();
					//sum human
					$reg_info["count_human"] = $count_human+$reg_info["count_human"];

					$disp_sum_chair_all = $disp_sum_chair_all+$reg_info["count_human"];
					$disp_price_per_proj = $disp_price_per_proj+$reg_info["sum_price"];
					$ttl_sum_price = $ttl_sum_price + $reg_info["sum_price"];
					$reg_info["sum_price"] = number_format( $reg_info["sum_price"], 2, ".", "," );
					if ( !isset($cos_dt_info["chair_all"]) ) {
						$cos_dt_info["chair_all"] = 0;
					}
					if ($count_cos_id>1) {
						if ($count_tag_hr==0) {
							$disp_date .= '<span class="center">'.$cos_dt_info["date"].'</span>';
							$disp_time .= '<span class="center">'.$cos_dt_info["time"].'</span>';
							$disp_chair_all .= '<span class="center">'.$cos_dt_info["chair_all"].'</span>';
							$disp_price_per_cos .= '<span class="right">'.$reg_info["sum_price"].'</span>';
							$disp_count_human .= '<span class="center">'.$reg_info["count_human"].'</span>';
						}else{
							$disp_date .= '<hr class="hr"><span class="center">'.$cos_dt_info["date"].'</span>';
							$disp_time .= '<hr class="hr"><span class="center">'.$cos_dt_info["time"].'</span>';
							$disp_chair_all .= '<hr class="hr"><span class="center">'.$cos_dt_info["chair_all"].'</span>';
							$disp_price_per_cos .= '<hr class="hr"><span class="right">'.$reg_info["sum_price"].'</span>';
							$disp_count_human .= '<hr class="hr"><span class="center">'.$reg_info["count_human"].'</span>';
						}
						//$disp_open_regis .= "<hr>".$cos_dt_info["sit_all"];
						$count_tag_hr++;
					}else{
						$disp_date = '<span class="center">'.$cos_dt_info["date"].'</span>';
						$disp_time = '<span class="center">'.$cos_dt_info["time"].'</span>';
						$disp_chair_all = '<span class="center">'.$cos_dt_info["chair_all"].'</span>';
						//$disp_open_regis = $cos_dt_info["sit_all"];
						$disp_price_per_cos = '<span class="right">'.$reg_info["sum_price"].'</span>';
						$disp_count_human = '<span class="center">'.$reg_info["count_human"].'</span>';
					}//end else
				}//end loop
				$disp_price_per_proj = number_format( $disp_price_per_proj, 2, ".", "," );
				$ttl_count_human_all_cos = $ttl_count_human_all_cos + $disp_sum_chair_all;
				$ttl_count_human_per_cos = $ttl_count_human_per_cos + $reg_info["count_human"];
				$i++;
				$cut_row = $i%30;				
				if ($i==1 && $cut_row==1) {
					echo "<h6>Public</h6>";
?>
		<div>
					<table class="td-center" style="table-layout:fixed">
						<col width=30>
						<col width=100>
						<col width=180>
						<col width=80>
						<col width=25>
						<col width=80>
						<col width=100>
						<col width=50>
						<col width=80>
						<col width=80>
						<col width=50>
						<col width=100>
						<col width=100>
						<thead>
							<tr>
								<td><span class="center">ลำดับที่</span></td>
								<td><span class="center">จังหวัด</span></td>
								<td><span class="center">สถานที่</span></td>
								<td><span class="center">วัน<br>(กรอกเอง)</span></td>
								<td><span class="center">รอบ</span></td>
								<td><span class="center">Date<br>(กรอกเอง)</span></td>
								<td><span class="center">Project Code</span></td>
								<td><span class="center">คนทั้งหมด</span></td>
								<td><span class="center">Date</span></td>
								<td><span class="center">Time</span></td>
								<td><span class="center">จำนวนคนต่อรอบ</span></td>
								<td><span class="center">รายได้ ต่อรอบ</span></td>
								<td><span class="center">รายได้ทั้งหมด</span></td>
							</tr>
						</thead>
<?php
				}//end if
?>
						<tbody>
							<tr>
								<td width=""><span class="center"><?php echo $i;?></span></td>
								<td width=""><span class="center"><?php echo $disp_address;?></span></td>
								<td width=""><span class="left"><?php echo $disp_address_dt;?></span></td>
								<td width=""><span class="center"></span></td>
								<td width=""><span class="center"><?php echo $count_cos_id; ?></span></td>
								<td width=""><span class="center"></span></td>
								<td width=""><span class="center"><?php echo $proj_code;?></span></td>
								<td width=""><span class="center"><?php echo $disp_sum_chair_all;?></span></td>
								<td width=""><?php echo $disp_date;?></td>
								<td width=""><?php echo $disp_time;?></td>
								<td width=""><?php echo $disp_count_human;?></td>
								<td width=""><?php echo $disp_price_per_cos;?></td>
								<td width=""><span class="right"><?php echo $disp_price_per_proj;?></span></td>
							</tr>
<?php			
				if ($cut_row==1 && $i!=1) {	
?>
					<!-- <div style="page-break-after: always;"></div> -->
<?php					
				}
				//echo "recc ".$recc." / ".$ttl_page;
				//die();
				$recc++;		
				//$ttl_count_human_all_cos = $ttl_count_human_all_cos + $disp_sum_chair_all;
			}// end loop for
				$ttl_count_cos = number_format( $ttl_count_cos );
				$ttl_count_human_all_cos = number_format($ttl_count_human_all_cos);
				$ttl_count_human_per_cos = number_format($ttl_count_human_per_cos);
				$ttl_sum_price = number_format($ttl_sum_price, 2, ".", ",");
?>
							<tr>
								<td colspan="4" class="center">รวม</td>				
								<td><span class="center"><?php echo $ttl_count_cos; ?></span></td>
								<td><span class="right"></span></td>
								<td><span class="center"></span></td>
								<td><span class="center"><?php echo $ttl_count_human_all_cos; ?></span></td>
								<td><span class="center"></span></td>
								<td><span class="center"></span></td>
								<td><span class="center"><?php echo $ttl_count_human_all_cos; ?></span></td>
								<td><span class="right"></span></td>
								<td><span class="right"><?php echo $ttl_sum_price; ?></span></td>
							</tr>
					</tbody>
			</table> 
		</div>
<?php 		
			$qry = "SELECT DISTINCT cd.code_project
					FROM course_detail AS cd
					WHERE cd.active='T' AND cd.inhouse='T' 
						AND cd.code_project<>''
						AND cd.code_project<>''
						AND cd.coursetype_id=2
						AND cd.section_id=1
						$cond
					GROUP BY cd.code_project
				;";
			$rs = $db->get($qry);
			//d($rs);
			$proj_codes = array();
			foreach ($rs as $key => $rs) {
				$proj_codes[] = $rs["code_project"];
			}
			//d($proj_codes);
			$cos_dt_data = array();
			foreach ($proj_codes as $key => $proj_code) {
				$tmp_cos_dt = array();
				$qry = "SELECT cd.course_detail_id
					FROM course_detail AS cd
					WHERE cd.active='T' AND cd.code_project='{$proj_code}' 
						$cond
				";
				$course_detail_ids = $db->get($qry);
				//d($course_detail_ids);
				//die();
				$ids = array();
				$list_ids = null;
				foreach ($course_detail_ids as $key => $cd_id) {
					$ids[] = $cd_id["course_detail_id"];
					$list_ids .= $cd_id["course_detail_id"].",";
				}// end loop
				$tmp_cos_dt["proj_code"] = $proj_code;
				$tmp_cos_dt["count_cos_id"] = count($ids);
				$tmp_cos_dt["ids"] = trim($list_ids, ",");
				//d($tmp_cos_dt);
				//die();
				$cos_dt_data[] = $tmp_cos_dt;
			}// end loop for (main)	
			$recc=1;
			$i=0;
			$rs_ttl = count($rs);
			$ttl_page = intval($rs_ttl/10)+1;
			$ttl_count_cos = 0;
			$ttl_count_human_all_cos = 0;
			$ttl_count_human_per_cos = 0;
			$ttl_sum_price = 0;
			foreach ($cos_dt_data as $key => $value) {
				$proj_code = $value["proj_code"];
				$count_cos_id = $value["count_cos_id"];
				$ttl_count_cos = $ttl_count_cos + $count_cos_id;
				$ids = $value["ids"];
				$arr_ids = explode(",", $ids);
				$disp_date = null;
				$disp_time = null;
				$disp_open_regis = 0;
				$disp_sum_chair_all = 0;
				$disp_chair_all = null;
				$disp_sit_all = 0;
				$count_tag_hr = 0;
				$disp_price_per_proj = 0;
				$disp_price_per_cos = null;
				$disp_count_human = null;
				foreach ($arr_ids as $key => $cos_dt_id) {
					//echo $proj_code." -> ".$cos_dt_id."<br>";
					$qry = "SELECT cd.course_detail_id
								, cd.address
								, cd.address_detail
								, cd.date
								, cd.time
								, cd.sit_all
								, cd.chair_all
								, cd.book
								, cd.regis_all
								, cd.book_all
								, SUM(cd.chair_all) AS ttl_chair_all
							FROM course_detail AS cd	
							WHERE cd.active='T'	AND cd.course_detail_id={$cos_dt_id}
						";
					$rs = $db->get($qry);					
					$cos_dt_info = $rs[0];
					//d($cos_dt_info);
					$disp_address = $cos_dt_info["address"];
					$disp_address_dt = $cos_dt_info["address_detail"];
					//$disp_sum_chair_all = $disp_sum_chair_all+$cos_dt_info["chair_all"];
					
					//count reg_id for pay_status == 6
					$qry = "SELECT count(r.register_id) AS count_human
							FROM register AS r
							WHERE r.active='T' 
								AND ( r.pay_status = 6 ) 
								AND (r.course_detail_id LIKE '%$cos_dt_id%')
						";
					$rs = $db->get($qry);
					$reg_info = $rs[0];	
					$count_human = $reg_info["count_human"];
					//count reg_id for pay_status == 3,5
					$qry = "SELECT count(r.register_id) AS count_human
								, SUM(r.pay_price) AS sum_price
							FROM register AS r
							WHERE r.active='T' 
								AND ( r.pay_status IN (3,5) ) 
								AND (r.course_detail_id LIKE '%$cos_dt_id%')
						";
					$rs = $db->get($qry);
					$reg_info = $rs[0];	
					//d($reg_info);
					//die();
					//sum human
					$reg_info["count_human"] = $count_human+$reg_info["count_human"];

					$disp_sum_chair_all = $disp_sum_chair_all+$reg_info["count_human"];
					$disp_price_per_proj = $disp_price_per_proj+$reg_info["sum_price"];
					$ttl_sum_price = $ttl_sum_price + $reg_info["sum_price"];
					$reg_info["sum_price"] = number_format( $reg_info["sum_price"], 2, ".", "," );
					if ( !isset($cos_dt_info["chair_all"]) ) {
						$cos_dt_info["chair_all"] = 0;
					}
					if ($count_cos_id>1) {
						if ($count_tag_hr==0) {
							$disp_date .= '<span class="center">'.$cos_dt_info["date"].'</span>';
							$disp_time .= '<span class="center">'.$cos_dt_info["time"].'</span>';
							$disp_chair_all .= '<span class="center">'.$cos_dt_info["chair_all"].'</span>';
							$disp_price_per_cos .= '<span class="right">'.$reg_info["sum_price"].'</span>';
							$disp_count_human .= '<span class="center">'.$reg_info["count_human"].'</span>';
						}else{
							$disp_date .= '<hr class="hr"><span class="center">'.$cos_dt_info["date"].'</span>';
							$disp_time .= '<hr class="hr"><span class="center">'.$cos_dt_info["time"].'</span>';
							$disp_chair_all .= '<hr class="hr"><span class="center">'.$cos_dt_info["chair_all"].'</span>';
							$disp_price_per_cos .= '<hr class="hr"><span class="right">'.$reg_info["sum_price"].'</span>';
							$disp_count_human .= '<hr class="hr"><span class="center">'.$reg_info["count_human"].'</span>';
						}
						//$disp_open_regis .= "<hr>".$cos_dt_info["sit_all"];
						$count_tag_hr++;
					}else{
						$disp_date = '<span class="center">'.$cos_dt_info["date"].'</span>';
						$disp_time = '<span class="center">'.$cos_dt_info["time"].'</span>';
						$disp_chair_all = '<span class="center">'.$cos_dt_info["chair_all"].'</span>';
						//$disp_open_regis = $cos_dt_info["sit_all"];
						$disp_price_per_cos = '<span class="right">'.$reg_info["sum_price"].'</span>';
						$disp_count_human = '<span class="center">'.$reg_info["count_human"].'</span>';
					}//end else
				}//end loop
				$disp_price_per_proj = number_format( $disp_price_per_proj, 2, ".", "," );
				$ttl_count_human_all_cos = $ttl_count_human_all_cos + $disp_sum_chair_all;
				$ttl_count_human_per_cos = $ttl_count_human_per_cos + $reg_info["count_human"];
				$i++;
				$cut_row = $i%30;				
				if ($i==1 && $cut_row==1) {
					echo "<br><h6>In House</h6>";
?>
		<div>
			<table class="td-center" style="table-layout:fixed">
						<col width=30>
						<col width=100>
						<col width=180>
						<col width=80>
						<col width=25>
						<col width=80>
						<col width=100>
						<col width=50>
						<col width=80>
						<col width=80>
						<col width=50>
						<col width=100>
						<col width=100>
						<thead>
							<tr>
								<td><span class="center">ลำดับที่</span></td>
								<td><span class="center">จังหวัด</span></td>
								<td><span class="center">สถานที่</span></td>
								<td><span class="center">วัน<br>(กรอกเอง)</span></td>
								<td><span class="center">รอบ</span></td>
								<td><span class="center">Date<br>(กรอกเอง)</span></td>
								<td><span class="center">Project Code</span></td>
								<td><span class="center">คนทั้งหมด</span></td>
								<td><span class="center">Date</span></td>
								<td><span class="center">Time</span></td>
								<td><span class="center">จำนวนคนต่อรอบ</span></td>
								<td><span class="center">รายได้ ต่อรอบ</span></td>
								<td><span class="center">รายได้ทั้งหมด</span></td>
							</tr>
						</thead>
<?php
				}//end if
?>
						<tbody>
							<tr>
								<td width=""><span class="center"><?php echo $i;?></span></td>
								<td width=""><span class="center"><?php echo $disp_address;?></span></td>
								<td width=""><span class="left"><?php echo $disp_address_dt;?></span></td>
								<td width=""><span class="center"></span></td>
								<td width=""><span class="center"><?php echo $count_cos_id; ?></span></td>
								<td width=""><span class="center"></span></td>
								<td width=""><span class="center"><?php echo $proj_code;?></span></td>
								<td width=""><span class="center"><?php echo $disp_sum_chair_all;?></span></td>
								<td width=""><?php echo $disp_date;?></td>
								<td width=""><?php echo $disp_time;?></td>
								<td width=""><?php echo $disp_count_human;?></td>
								<td width=""><?php echo $disp_price_per_cos;?></td>
								<td width=""><span class="right"><?php echo $disp_price_per_proj;?></span></td>
							</tr>
<?php			
				if ($cut_row==1 && $i!=1) {	
?>
					<!-- <div style="page-break-after: always;"></div> -->
<?php					
				}
				//echo "recc ".$recc." / ".$ttl_page;
				//die();
				$recc++;		
				//$ttl_count_human_all_cos = $ttl_count_human_all_cos + $disp_sum_chair_all;
			}// end loop for
				$ttl_count_cos = number_format( $ttl_count_cos );
				$ttl_count_human_all_cos = number_format($ttl_count_human_all_cos);
				$ttl_count_human_per_cos = number_format($ttl_count_human_per_cos);
				$ttl_sum_price = number_format($ttl_sum_price, 2, ".", ",");
?>
							<tr>
								<td colspan="4" class="center">รวม</td>				
								<td><span class="center"><?php echo $ttl_count_cos; ?></span></td>
								<td><span class="right"></span></td>
								<td><span class="center"></span></td>
								<td><span class="center"><?php echo $ttl_count_human_all_cos; ?></span></td>
								<td><span class="center"></span></td>
								<td><span class="center"></span></td>
								<td><span class="center"><?php echo $ttl_count_human_all_cos; ?></span></td>
								<td><span class="right"></span></td>
								<td><span class="right"><?php echo $ttl_sum_price; ?></span></td>
							</tr>
					</tbody>
			</table> 
		</div>
	</div>
</body>
</html>
<script type="text/javascript">
function printPage(){
   window.print();
   setTimeout(" parent.$.fancybox.close()",1000);
}
function receipt_tax_export(){
	var btn_confirm = confirm("หากต้องการ Export เป็นไฟล์ Excel ให้เลือก \"OK\"");
	if (btn_confirm == true) {
		var date_start = '<?php echo $date_start_th; ?>';
		var date_stop = '<?php echo $date_stop_th; ?>';
		url = "./report/report-sum-project-export.php?date_start="+date_start+"&date_stop="+date_stop;
		window.open(url,'_self');
	} else {
	}
}
</script>
