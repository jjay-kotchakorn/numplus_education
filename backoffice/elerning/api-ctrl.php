<?php
include_once "./connection/connection.php";
global $db;

function d($data=""){
	echo "<pre>";
	print_r($data);
	echo "</pre>";
	// die();
}//end func
// d($_SERVER);

function write_log($file_name="", $msg="", $write_mode="a"){
	$str = $msg;	
	$date_now = date('Y-m-d H:i:s');
	$date_log = date('Y-m-d');
	$log_name = "./log/".$file_name."-".$date_log.".log";
	$file = fopen($log_name, $write_mode);
	$str_txt = $date_now."|".$str."\r\n";
	fwrite($file, $str_txt);
}//end func

function check_auth($api_key="", $api_secret=""){
	if ( $api_key=="1234" && $api_secret=="555" ) {
		return true;
	}else{
		return false;
	}//end else
}//end func

/*
if ( !empty($_POST) ) {
	// d($_SERVER);die();
	$auth = check_auth( $_POST["api_key"], $_POST["api_secret"] );
}

if ( !empty($_POST) && $auth ) {
	global $db;
	$request_url = $_SERVER["PATH_INFO"];
	// return d($_SERVER);die();
	// return $request_url;die();

	if ( $request_url=="/auth/" ) {
		$q="SELECT * FROM products";
		$rs = $db->get($q);
		// return d($_SERVER);die();
		// $rs = json_encode($rs);
		// return json_encode($rs);
		// return var_dump($rs);
		echo json_encode($rs);
		// d($rs);
	}
}//end if
*/

$request_method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : '';
$request_url = !empty($_SERVER["PATH_INFO"]) ? $_SERVER["PATH_INFO"] : '';
if (!array_key_exists('CONTENT_TYPE', $_SERVER) && array_key_exists('HTTP_CONTENT_TYPE', $_SERVER)) {
    $_SERVER['CONTENT_TYPE'] = $_SERVER['HTTP_CONTENT_TYPE'];
}
$content_type = isset($_SERVER['CONTENT_TYPE']) ? $_SERVER['CONTENT_TYPE'] : '';
// $data_values = $_GET;
if ($request_method === 'POST') {
    $data_values = $_POST;

} elseif ($request_method === 'PUT') {
    if (strpos($content_type, 'application/x-www-form-urlencoded') === 0) {
        parse_str($http_raw_post_data, $_PUT);
        $data_values = $_PUT;
    }
} elseif ($request_method === 'PATCH') {
    if (strpos($content_type, 'application/x-www-form-urlencoded') === 0) {
        parse_str($http_raw_post_data, $_PATCH);
        $data_values = $_PATCH;
    }
} elseif ($request_method === 'DELETE') {
    if (strpos($content_type, 'application/x-www-form-urlencoded') === 0) {
        parse_str($http_raw_post_data, $_DELETE);
        $data_values = $_DELETE;
    }
} elseif ($request_method === 'GET') {
	$data_values = $_GET;
	if ( $request_url=="/product/list/" ) {
		$q="SELECT * FROM products";
		$rs = $db->get($q);
		// return d($_SERVER);die();
		// $rs = json_encode($rs);
		// return json_encode($rs);
		// return var_dump($rs);
		echo json_encode($rs);
		// d($rs);
	}
}

?>