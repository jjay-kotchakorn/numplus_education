<?php
include_once "../share/authen.php";
include_once "../connection/connection.php";
include_once "../lib/lib.php";
require "./vendor/autoload.php";
global $db;
// d($db);
use \Curl\Curl;
/*
$q="select * from course";
$rs = $db->get($q);
d($rs);
*/
?>
<link rel="stylesheet" type="text/css" href="../js/jquery.nanoscroller/nanoscroller.css" />
<link href="../js/jquery.icheck/skins/square/blue.css" rel="stylesheet">
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="col-sm-12">
				<div class="content block-flat ">
					<div class="page-head">
						<button style="visibility: hidden;" class="btn btn-success btn-small pull-right" onclick="addnew()" style="margin-top:10px; margin-right:-25px;"><i class="fa fa-plus"></i> เพิ่มข้อมูลสมาชิก</button>
						<h3><i class="fa fa-list"></i> &nbsp;ข้อมูลสมาชิก</h3>
						<div class="form-group row">
							<label class="col-md-offset-2 col-sm-1 control-label"  style=" padding-right:0px;">วันที่เริ่ม</label>
							<div class="col-sm-2"  style="padding-left:0px; padding-right:0px;">
								<input class="form-control" name="date_start" id="date_start" onblur="reCall();" placeholder="วันที่เริ่ม" type="text">
							</div>                                          
							<label class="col-sm-1 control-label"  style=" padding-right:0px;">วันที่สิ้นสุด</label>
							<div class="col-sm-2"  style="padding-left:0px; padding-right:0px;">
								<input class="form-control" name="date_stop" id="date_stop" onblur="reCall();" placeholder="วันที่สิ้นสุด" type="text">
							</div> 
							<div class="col-md-2">								
								<input  value="T" name="fa_member" id="fa_member"  type="checkbox">&nbsp;&nbsp;สมาชิกของหลักสูตร FA 
							</div>
							<label class="col-sm-2 control-label">
								<a href="#" class="btn" onclick="reCall();"><i class="fa fa-search">&nbsp;</i></a>&nbsp;&nbsp;
								<!-- <button onclick="clearSearch();" id="ok" class="btn btn-success" type="button">Clear</button> -->
							</label>  							
						</div>
					</div>
					<br>
					<table id="tbMember" class="table" style="width:100%">
						  <thead>
							  <tr>
								  <th width="8%">ลำดับ</th>
								  <th width="12%">รหัสประจำตัวประชาชน</th>
								  <th width="20%">ชื่อ-นามสกุล</th>
								  <th width="20%">ชื่อ-นามสกุล (ภาษาอังกฤษ)</th>
								  <th width="14%">วันเวลาที่สมัคร</th>
								  <th width="21%">Manage</th>
							  </tr>
						  </thead>   
						<tbody>
						</tbody>
					</table>
					<div class="clear"></div>
						<button onclick="member_import();" id="member-import" class="btn btn-success pull-right" type="button">Import Member</button>
						<div class="clear"></div>
				</div>
			</div>

		</div>
	</div> 
</div>
<?php include ('../inc/js-script.php') ?>


<script type="text/javascript">
$(document).ready(function() {
	var oTable;
	$("#frmMain").validate();
	listItem();
    $("#date_start").datepicker({language:'th-th',format:'dd-mm-yyyy'});
    $("#date_stop").datepicker({language:'th-th',format:'dd-mm-yyyy'});
   $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue checkbox',
        radioClass: 'iradio_square-blue'
      });
});

function listItem(){
   var url = "data/memberlist.php";
   oTable = $("#tbMember").dataTable({
	   "sDom": 'T<"clear">lfrtip',
	   "oLanguage": {
   	   "sInfomemberty": "",
   		"sInfoFiltered": ""
						  },
		"oTableTools": {
			"aButtons":  [	
					
			   ]
		},
		"aoColumns": [
		null,
		null,
		null,
		null,
		null,
		{ "bSortable": false }
		],
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": url,
		"sPaginationType": "full_numbers",
		"aaSorting": [[ 0, "desc" ]],
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			aoData.push({"name":"date_start","value":$("#date_start").val()});			
			aoData.push({"name":"date_stop","value":$("#date_stop").val()});	
			if($("#fa_member").is(":checked")){
				aoData.push({"name":"fa_member","value":"T"});	
			}
			$.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});
		}
   });
   $('#tbMember_filter input').unbind();
   $('#tbMember_filter input').bind('keyup', function(e) {
       if(e.keyCode == 13) {
        oTable.fnFilter(this.value);   
    }
   }); 
}

function editInfo(id){
	if(typeof id=="undefined") return;
   var url = "index.php?p=<?php echo $_GET["p"];?>&member_id="+id+"&type=info";
   redirect(url);
}
function addnew(){
   var url = "index.php?p=<?php echo $_GET["p"];?>&type=info";
   redirect(url);
}
function member_import(){
   var url = "index.php?p=<?php echo $_GET["p"];?>&type=member-import";
   redirect(url);
}

function reCall(){
	oTable.fnClearTable( 0 );
	oTable.fnDraw();
}

function reset_passwd(member_id, cid){
	//var cid = $("#cid").val();
	//var member_id = "<?php echo $member_id; ?>";
    var r = confirm("ยืนยันการ Reset Password \nเลขประจำตัวประชาชน "+cid);
    if (r == true) {
       	var url = "update-member.php?flag='reset_passwd'&member_id="+member_id+"&cid="+cid;
		window.open(url,'_self');
		//setTimeout(function(){ alert("Reset password success"); }, 3000);
		//alert("Update password success");
    } else {

    }
}

</script>