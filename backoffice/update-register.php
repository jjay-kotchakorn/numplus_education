<?php
include_once "./share/authen.php";
include_once "./share/course.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";
global $db, $EMPID;
$date_now = date('Y-m-d H:i:s');

// d($_POST); die();

$reg_id = $_POST["register_id"];
$pay_status = $_POST["pay_status"];

if (!empty($reg_id)) {
	$reg_info = get_register("", $reg_id);
	$reg_info = $reg_info[0];
	$chk_pay_date = true;
	if ( $reg_info["pay_status"]==1 && $pay_status==3 ) {
		if ( $_POST["pay_date_dd"]=="-" || empty($_POST["pay_date_dd"]) ) {
			$chk_pay_date = false;
			$register_id = $reg_id;

			$args = array();
			$args["p"] = "register";
			$args["register_id"] = $register_id;
			$args["type"] = "info";
			$args["msg"] = "error";
			$args["text"] = "กรุณากรอกวันที่ชำระเงิน";

			redirect_url($args);
			die();

		}//end if
	}//end if
}//end if

if($chk_pay_date){

	// $this_file = basename(__FILE__, '.php'); 
	$this_file = basename($_SERVER["SCRIPT_FILENAME"], '.php');
	register_log_save($reg_id, 'update', $this_file);

	$price = str_replace(",", "", $_POST["pay_price"]);
	$args = array();


	if ( $pay_status==3 && $reg_info["pay"]=='bill_payment' ) {			
		// $remark .= "/{$date_now} เปลี่ยนสถานะการชำระจาก ชำระเงินเกินกำหนด เป็น ชำระเงินแล้ว"; 
		$sql = "SELECT b.bank_code
			FROM bill_payment_upload_list_details AS b
			WHERE b.register_id={$reg_id}
		";
		// echo $sql;die();
		$bill_info = $db->rows($sql);
		if ( !empty($bill_info) ) {
			$bank_code = sprintf("%01d", $bill_info["bank_code"]);
			$args["pay_method"] = (int)$bank_code;
		}//end if		
	}//end if


	$args["table"] = "register";
	$args["id"] = $reg_id;
	//$args["pay_status"] = $_POST["pay_status"];
	$args["pay_status"] = $pay_status;
	if ($_POST["pay_date_dd"]=="-") {
		$pay_date = "0000-00-00 00:00";
	}elseif (!empty($_POST["pay_date_dd"]) || $_POST["pay_date_dd"]!="-") {
		$pay_date = $_POST["pay_date_dd"]." ".$_POST["pay_date_tt"];
	}

	// $arr_pay_status = array (1, 5, 6, 4);
	if ($args["pay_status"]==8) {
		$args["active"] = "T";
	}else if( $args["pay_status"]==1 || $args["pay_status"]==4 ){
		$args["pay_date"] = "0000-00-00 00:00";
	}else if ( $args["pay_status"]==5 || $args["pay_status"]==6 ) {
		$args["pay_date"] = $date_now;
	}//end else if

	if($args["pay_status"]==3){
		$args["pay_date"] = thai_to_timestamp($pay_date, true);
		$q = "select coursetype_id, section_id, docno from register where register_id=$reg_id";
		$v = $db->rows($q);
		if(!$v["docno"]){			
			$section_id = (int)$v["section_id"];
			$coursetype_id = (int)$v["coursetype_id"];
			$t = rundocno($coursetype_id, $section_id);
			$args = array_merge($args, $t);	
		}
	}//end if

	$args["pay_price"] = (int)$price;
	$args["pay_id"] = (int)$_POST["pay_id"];
	$args["remark"] = $_POST["remark"];
	$args["recby_id"] = (int)$EMPID;
	$args["rectime"] = date("Y-m-d H:i:s");

	// d($args);
	// die();

    $db->set($args);
    $register_id = $args["id"] ? $args["id"] : $reg_id;

    //write log
    $str = "reg_id=".$reg_id."|cos_type=".$coursetype_id."|sec_id=".$section_id."|docno=".$args["docno"]."|pay_status=".$args["pay_status"];
	$date_now = date('Y-m-d H:i:s');
	$date_log = date('Y-m');
	$log_name = "./data/log/update-register_and_register-status-update_".$date_log.".log";
	$file = fopen($log_name, 'a');
	$str_txt = $date_now."|".$str."\r\n";
	fwrite($file, $str_txt);
	fclose($file);
 
}

$args = array();
$args["p"] = "register";
$args["register_id"] = $register_id;
$args["type"] = "info";
if ($args["register_id"]) {
	$args["msg"] = "success";
}else{
	$args["msg"] = "error";
}
$args["text"] = "";

redirect_url($args);

?>
