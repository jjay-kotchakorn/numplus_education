<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/member.php";
global $db, $RIGHTTYPEID;
$educationlevel = datatype(" and a.active='T'", "educationlevel", true);
$province = datatype(" and a.active='T'", "province", true, "a.name asc, ");
$receipttype = datatype(" and a.active='T'", "receipttype", true);
$member_id = $_GET["member_id"];
$university = datatype_university(true);
$q = "select activate_code, title_th, title_th_text, fname_th, lname_th, activelogin from member where member_id=$member_id";
$r = $db->rows($q);

$str = "";
if($r){
	if($r["title_th"]=="อื่นๆ"){
		$r["title_th"] = $r["title_th_text"];
	}
	$str = $r["title_th"]." ".$r["fname_th"]." ".$r["lname_th"];
}else{
	$str = "เพิ่มสมาชิก";
}

?>
<link rel="stylesheet" type="text/css" href="js/jquery.nanoscroller/nanoscroller.css" />
<link href="js/jquery.icheck/skins/square/blue.css" rel="stylesheet">
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="row">
				  <div class="col-md-12">			      
					<div class="block-flat">
						<div class="col-sm-12">
							  <div class="header">							
							  	<ol class="breadcrumb">
							  		<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>');">หน้าหลัก</a></li>
							  		<li class="active"><?php echo $str; ?></li>
							  	</ol>
							  </div>
							  <br>
						</div>
					  <div class="content">
						  <form id="frmMain" name="frmMain" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="update-member.php">
						  <input type="hidden" name="member_id" id="member_id" value="<?php echo $member_id; ?>">
						  <input type="hidden" name="member_quick_edit" id="member_quick_edit" value="<?php echo 'T'; ?>">
							<div class="header">		
								<h4>1.ข้อมูลส่วนตัว</h4> 	 
								<div class="form-group row">
									<div class="col-sm-12">
										<label class="col-sm-1 control-label">เลือก <span class="red">*</span></label>
										<div class="col-md-2">								
											<!-- <input  value="T" name="nation" id="nation"  type="checkbox">&nbsp;&nbsp;สำหรับชาวต่างชาติ
											<input  value="" name="nation" id="nation"  type="checkbox">&nbsp;&nbsp;สำหรับชาวต่างชาติ -->
											<select name="nation" id="nation" class="form-control required"
												onchange="select_nation()"
												onclick="select_nation()">
											  <option selected="selected" value="T">ชาวไทย</option>
											  <option value="F">ชาวต่างชาติ</option>
											</select>
										</div>
										<label class="col-sm-2 control-label">เลขที่บัตรประชาชน <span class="red">*</span></label>
										<div class="col-sm-3">
											<input class="form-control required cart_id" name="cid" id="cid" placeholder="รหัสประจำตัวประชาชน" type="text" <?php echo ($RIGHTTYPEID==1 || $RIGHTTYPEID==4) ? '' : 'readonly'; ?>>
										</div>

									</div>
							  	</div>
						  	</div>
						  <div class="col-sm-12">
							  <div class="form-group row">
								<label class="control-label col-sm-1">คำนำหน้า <span class="red">*</span></label>
				                  <label class="col-sm-1" style="width: 120px;"> 
				                  	<select name="title_th" id="title_th" class="form-control">
				                  		<option value="นาย">นาย</option>
				                  		<option value="นาง">นาง</option>
				                  		<option value="นางสาว">นางสาว</option>
				                  		<option value="อื่นๆ">อื่นๆ</option>
				                  	</select>   
				                  </label> 
				                   <label class="col-sm-2">				                			                
								  		<input class="form-control" id="title_th_text" name="title_th_text" placeholder="คำนำหน้า" type="text" style="max-width:200px;display: none;">
								  </label>
										                
								<label class="col-sm-1 control-label">ชื่อ <span class="red">*</span></label>
								<div class="col-sm-2">
								  <input class="form-control required" name="fname_th" id="fname_th" placeholder="ชื่อ" type="text">
								</div>			                			                
								<label class="col-sm-1 control-label">นามสกุล <span class="red">*</span></label>
								<div class="col-sm-2">
								  <input class="form-control required " name="lname_th" id="lname_th" placeholder="นามสกุล" type="text">
								</div>
							  </div>
							  <div class="form-group row">							
								  <label class="control-label col-md-1" style="font-size:12px;">Title&nbsp;<span class="red">*</span></label>
				                  <label class="col-md-1" style="width: 120px;">
				                  	<select name="title_en" id="title_en" class="form-control">
				                  		<option value="Mr">Mr.</option>
				                  		<option value="Mrs">Mrs.</option>
				                  		<option value="Ms">Ms.</option>
				                  		<option value="Other">Other</option>
				                  	</select>  
								</label> 
								<label class="col-md-2" >				                			                
									<input class="form-control" id="title_en_text" name="title_en_text" placeholder="คำนำหน้า" type="text" style="max-width:200px;display: none;">
								</label>		                
								<label class="col-sm-1 control-label" style="font-size:10px; font-weight: bold;">First Name <span class="red">*</span></label>
								<div class="col-sm-2">
								  <input class="form-control required" name="fname_en" id="fname_en" placeholder="First Name" type="text">
								</div>			                			                
								<label class="col-sm-1 control-label" style="font-size:10px; font-weight: bold;">Last Name <span class="red">*</span></label>
								<div class="col-sm-2">
								  <input class="form-control required" name="lname_en" id="lname_en" placeholder="Last Name" type="text">
								</div>
							  </div>

<?php /*
							  
							  <div class="form-group row">
							  	<label class="col-sm-1 control-label">เพศ <span class="red">*</span></label>
							  	<div class="col-sm-2">
							  		<select class="form-control"  name="gender" id="gender">
							  			<option value="">เลือกเพศ</option>
							  			<option value="M">ชาย</option>
							  			<option value="F">หญิง</option>
							  		</select>
							  	</div>
								<label class="col-sm-2 control-label">วันเกิด <span class="red">*</span></label>
							  	<div class="col-sm-3">								
							  		<input class="form-control required" size="16" name="birthdate" id="birthdate" value="" type="text">		                 							 
							  	</div>														  	
							  	
							  </div>

*/ ?>

							  <div class="header">							
								<h4>2.ข้อมูลการเข้าสู่ระบบ 
									<!-- <lable class="checkbox-inline"><input type="checkbox" name="show_pass"> <span style="font-size: 14px;">แสดงรหัสผ่าน</span></lable> -->
									<!-- <lable class="checkbox-inline">
										<input type="checkbox" name="reset_passwd" id="reset_passwd" value=""> 
										<span style="font-size: 14px;">&nbsp;&nbsp;Reset password</span>
									</lable> -->
									<!-- <button type="button" class="btn btn-warning fa fa-exclamation-triangle" 
										onClick="reset_passwd()"> Reset password</button> -->
								</h4> 
							  </div><br>
							  <div class="form-group row">
									<label class="col-sm-1 control-label">รหัสผ่าน <span class="red">*</span></label>
									<div class="col-sm-3">
									  <input class="form-control required" name="password" id="password" placeholder="รหัสผ่าน (อย่างน้อย 8 ตัวอักษร)" type="password">
									</div>			                
									<label class="col-sm-2 control-label">ยืนยันรหัสผ่าน <span class="red">*</span></label>
									<div class="col-sm-2">
									  <input class="form-control required" name="password2" id="password2" placeholder="ยืนยันรหัสผ่าน " type="password">
									</div>
									<label class="col-sm-2 control-label">สถานะ login</label>
									<div class="col-sm-2">
										<select name="activelogin" id="activelogin" class="form-control required">
										  <option selected="selected" value="T">Enable</option>
										  <option value="F">Disable</option>
										</select>
									</div>
							  </div>




<?php /* 



							  <div class="header">							
								<h4>3.ที่อยู่ปัจจุบันที่ใช้จัดส่งเอกสาร</h4>
							  </div><br>
							  <div class="form-group row">
									<label class="col-sm-1 control-label">บ้านเลขที่ <span class="red">*</span></label>
									<div class="col-sm-1">
									  <input class="form-control required" name="no" id="no" placeholder="บ้านเลขที่" type="text">
									</div>			                
									<label class="col-sm-1 control-label">หมู่บ้าน</label>
									<div class="col-sm-2">
									  <input class="form-control " name="gno" id="gno" placeholder="หมู่บ้าน / คอนโด / อาคาร" type="text">
									</div>
									<label class="col-sm-1 control-label">หมู่ที่</label>
									<div class="col-sm-1">
									  <input class="form-control " name="moo" id="moo" placeholder="หมู่ที่" type="text">
									</div>
									<label class="col-sm-1 control-label">ซอย</label>
									<div class="col-sm-1">
									  <input class="form-control " name="soi" id="soi" placeholder="ซอย" type="text">
									</div>
									<label class="col-sm-1 control-label">ถนน </label>
									<div class="col-sm-2">
									  <input class="form-control " name="road" id="road" placeholder="ถนน " type="text">
									</div>
							  </div>							  							  
							  <div class="form-group row">
								<label class="col-sm-1 control-label">จังหวัด <span class="red">*</span></label>
								<div class="col-sm-2">
								    <select name="province_id" id="province_id" class="form-control required" 
								    	onchange="getDropDown('#amphur_id', this.value, 'amphur', 'data/province.php')"
								    	onclick="getDropDown('#amphur_id', this.value, 'amphur', 'data/province.php')">
										<option value="">---- เลือก ----</option>
										<?php foreach ($province as $key => $value) {
											$id = $value['province_id'];
											$name = $value['name'];
											echo  "<option value='$id'>$name</option>";
										} ?>

									</select>
								</div>
								<label class="col-sm-1 control-label">อำเภอ <span class="red">*</span></label>
								<div class="col-sm-2">
								<select name="amphur_id"  id="amphur_id" class="form-control required" 
									onchange="getDropDown('#district_id', this.value, 'district', 'data/province.php')"
									onclick="getDropDown('#district_id', this.value, 'district', 'data/province.php')">
									<option value="">---- เลือก ----</option>								
								</select>
								</div>
								<label class="col-sm-1 control-label">ตำบล <span class="red">*</span></label>
								<div class="col-sm-2">
									<select name="district_id" id="district_id" class="form-control required">
										<option value="">---- เลือก ----</option>								
									</select>
								</div>								
								<label class="col-sm-2 control-label">รหัสไปรษณีย์ <span class="red">*</span></label>
								<div class="col-sm-1">
									<input  id="postcode" class="form-control required" name="postcode" maxlength="6" type="text">
								</div>
							  </div>

							  <div class="form-group row">
								<label class="col-sm-1 control-label">E-Mail <span class="red">*</span></label>
								<div class="col-sm-2">
								  <input class="form-control required email" name="email1" id="email1" placeholder="Enter a valid e-mail" type="email">
								</div>
								<label class="col-sm-2 control-label">E-Mail สำรอง</label>
								<div class="col-sm-2">
								  <input class="form-control email" name="email2" id="email2" placeholder="Enter a valid e-mail" type="email">
								</div>
								<label class="col-sm-2 control-label">โทรศัพท์บ้าน</label>
								<div class="col-sm-3">
								  <input class="form-control" name="tel_home" id="tel_home" placeholder="โทรศัพท์บ้าน" type="text">
								</div>
							  </div>

							  <div class="form-group row">
								<label class="col-sm-1 control-label">มือถือ <span class="red">*</span></label>
								<div class="col-sm-2">
								  <input class="form-control required" name="tel_mobile" id="tel_mobile" placeholder="มือถือ" type="text">
								</div>
								<label class="col-sm-2 control-label">โทรศัพท์ที่ทำงาน </label>
								<div class="col-sm-2">
								  <input class="form-control" name="tel_office" id="tel_office" placeholder="โทรศัพท์ที่ทำงาน" type="text">
								</div>
								<label class="col-sm-2 control-label">เบอร์ต่อ (ถ้ามี)</label>
								<div class="col-sm-3">
								  <input class="form-control" name="tel_office_ext" id="tel_office_ext" placeholder="เบอร์ต่อ (ถ้ามี)" type="text">
								</div>
							  </div>
							  <div class="header">							
								<h4>4. ข้อมูลบนใบเสร็จรับเงิน</h4>
							  </div><br>
							  <div class="form-group row">						
									<div class="col-sm-12">								
					                  <label class="radio-inline"  onclick="corparation_click(this)" style="padding-left:10px;padding-top:0px"> <div aria-disabled="false" aria-checked="false" style="position: relative;" class="iradio_square-blue"><input style="position: absolute; opacity: 0;" value="individuals" name="slip_type" class="icheck" type="radio"><ins style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;" class="iCheck-helper"></ins></div> ออกในนามบุคคลธรรมดา <span class="red">*</span> </label> 
					                  <label class="radio-inline"  onclick="corparation_click(this)" style="padding-left:10px;padding-top:0px"> <div aria-disabled="false" aria-checked="false" style="position: relative;" class="iradio_square-blue"><input style="position: absolute; opacity: 0;" value="corparation" name="slip_type" class="icheck" type="radio"><ins style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;" class="iCheck-helper"></ins></div> ออกในนามนิติบุคคล (สำหรับเบิกค่าใช้จ่าย) <span class="red">*</span> </label> 
										<div style="display:inline-block;margin-left:25px;">
											<div id="corparationinfo" style="display: none;">										
												<?php foreach ($receipttype as $key => $value) {
													$click = "getDropDown('#receipt_id', '{$value["receipttype_id"]}', 'receipt', 'data/receipt.php')";
													echo '<label class="radio-inline" onclick="'.$click.'" style="padding-left:10px;padding-top:0px"> <div aria-disabled="false" aria-checked="false" style="position: relative;" class="iradio_square-blue"><input style="position: absolute; opacity: 0;" value="'.$value["receipttype_id"].'" name="receipttype_id"  class="icheck" type="radio"><ins style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;" class="iCheck-helper"></ins></div>&nbsp;&nbsp;'.$value["name"].'</label>';
												} ?>
												<label class="radio-inline" style="padding-left:10px;padding-top:0px">
													<input  maxlength="200" class="form-control" name="receipttype_text" id="receipttype_text" type="text">
												</label>
											</div>	
										</div>
									</div>
								</div>
								<div  id="section_corparation_receipt" style="display: none;"class="form-group row">	
									
										<label class="control-label col-sm-2">ออกใบเสร็จในนาม</label>
										<label class="radio-inline col-sm-3" style="padding-left:10px;padding-top:0px">
											<select name="receipt_id" class="form-control" id="receipt_id" onchange="receipt_info(this.value);">
												<option value="">กรุณาเลือกจากรายการที่ปรากฎ</option>
											</select>											
										</label>
										<label class="control-label col-sm-1">สาขา <span class="red">*</span></label>									
										<label class="radio-inline col-sm-2" style="padding-left:10px;padding-top:0px">
											<input  name="branch" class="form-control required" id="branch" type="text">											
										</label>										
										<label class="control-label col-sm-2">เลขที่บัตรประจำตัวผู้เสียภาษี <span class="red">*</span></label>									
										<label class="radio-inline col-sm-2" style="padding-left:10px;padding-top:0px">
											<input class="form-control required" name="taxno" id="taxno" maxlength="13" type="text"> 											
										</label>										
										
	                
								</div>
								  <div id="section_individuals_receipt" class="form-group row" style="display: none;">																
									  <label class="control-label col-sm-2">ออกใบเสร็จในนาม</label>
									  <div class="col-sm-1" style="padding-right:2px;">
									  <select name="receipt_title" id="receipt_title" class="regis_select form-control">
										<option value="นาย">นาย</option>
										<option value="นาง">นาง</option>
										<option value="นางสาว">นางสาว</option>
										<option value="อื่นๆ">อื่นๆ</option>
					  				  </select> </div>
					                  <label class="radio-inline col-sm-2" style="padding-left:10px;padding-top:0px">				                			                
									  		<input class="form-control" id="receipt_title_text" name="receipt_title_text" placeholder="คำนำหน้า" type="text" style="max-width:150px; display:none;">
									  </label>												                
									<label class="col-sm-1 control-label">ชื่อ</label>
									<div class="col-sm-2">
									  <input class="form-control required" name="receipt_fname" id="receipt_fname" placeholder="ชื่อ" type="text">
									</div>			                			                
									<label class="col-sm-1 control-label">นามสกุล</label>
									<div class="col-sm-2">
									  <input class="form-control required " name="receipt_lname" id="receipt_lname" placeholder="นามสกุล" type="text">
									</div>
								  </div>
								  <div class="form-group row">
									 <label class="radio-inline col-sm-3" style="padding-left:30px;padding-top:0px"> ที่อยู่ (ที่จะแสดงบนใบเสร็จรับเงิน)</label>
									  	<div class="col-md-9">
									  		<input type="checkbox" name="same_addr" id="same_addr" onClick="checkboxaddress();"> ใช้ข้อมูลที่อยู่ปัจจุบัน
									  	</div>
								  	</div>
								  <div class="form-group row"> 	
									<label class="col-sm-1 control-label">บ้านเลขที่ <span class="red">*</span></label>
									<div class="col-sm-1">
									  <input class="form-control required" name="receipt_no" id="receipt_no" placeholder="บ้านเลขที่" type="text">
									</div>			                
									<label class="col-sm-1 control-label">หมู่บ้าน</label>
									<div class="col-sm-2">
									  <input class="form-control" name="receipt_gno" id="receipt_gno" placeholder="หมู่บ้าน / คอนโด / อาคาร" type="text">
									</div>
									<label class="col-sm-1 control-label">หมู่ที่</label>
									<div class="col-sm-1">
									  <input class="form-control" name="receipt_moo" id="receipt_moo" placeholder="หมู่ที่" type="text">
									</div>
									<label class="col-sm-1 control-label">ซอย</label>
									<div class="col-sm-1">
									  <input class="form-control" name="receipt_soi" id="receipt_soi" placeholder="ซอย" type="text">
									</div>
									<label class="col-sm-1 control-label">ถนน </label>
									<div class="col-sm-2">
									  <input class="form-control" name="receipt_road" id="receipt_road" placeholder="ถนน " type="text">
									</div>
							  </div>							  							  
							  <div class="form-group row">
								<label class="col-sm-1 control-label">จังหวัด <span class="red">*</span></label>
								<div class="col-sm-2">
								    <select name="receipt_province_id" id="receipt_province_id" class="form-control required" 
								    	onchange="getDropDown('#receipt_amphur_id', this.value, 'amphur', 'data/province.php')"
								    	onclick="getDropDown('#receipt_amphur_id', this.value, 'amphur', 'data/province.php')">
										<option value="">---- เลือก ----</option>
										<?php foreach ($province as $key => $value) {
											$id = $value['province_id'];
											$name = $value['name'];
											echo  "<option value='$id'>$name</option>";
										} ?>

									</select>
								</div>
								<label class="col-sm-1 control-label">อำเภอ <span class="red">*</span></label>
								<div class="col-sm-2">
								<select name="receipt_amphur_id"  id="receipt_amphur_id" class="form-control required" 
									onchange="getDropDown('#receipt_district_id', this.value, 'district', 'data/province.php')"
									onclick="getDropDown('#receipt_district_id', this.value, 'district', 'data/province.php')">
									<option value="">---- เลือก ----</option>								
								</select>
								</div>
								<label class="col-sm-1 control-label">ตำบล/แขวง<span class="red">*</span></label>
								<div class="col-sm-2">
									<select name="receipt_district_id" id="receipt_district_id" class="form-control required">
										<option value="">---- เลือก ----</option>								
									</select>
								</div>								
								<label class="col-sm-2 control-label">รหัสไปรษณีย์ <span class="red">*</span></label>
								<div class="col-sm-1">
									<input  id="receipt_postcode" class="form-control required" name="receipt_postcode" maxlength="6" type="text">
								</div>
							  </div>

						  	<div class="header">							
							<h4>5. ประวัติการทำงานปัจจุบัน</h4>
							  </div><br>
								<div class="form-group row">
									<div class="col-sm-9">
									    <label class="col-sm-1 control-label" style="padding-left:0px; padding-right:0px;">บริษัท <span class="red">*</span></label>
										<div style="col-sm-11">
											<div>										
												<?php foreach ($receipttype as $key => $value) {
													$ext = "";
													$ext = "hide_org_name({$value["receipttype_id"]});";
													$click = "getDropDown('#org_name', '{$value["receipttype_id"]}', 'receipt', 'data/receipt.php');".$ext;
													echo '
														<label class="radio-inline" onclick="'.$click.'" style="padding-left:10px;padding-top:0px"> 
														<div aria-disabled="false" aria-checked="false" style="position: relative;" class="iradio_square-blue">
															<input style="position: absolute; opacity: 0;" 
																value="'.$value["receipttype_id"].'" name="orgtype_id"  
																class="icheck" type="radio">
																<ins style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;" class="iCheck-helper"></ins>
														</div>&nbsp;&nbsp;'.$value["name"].'
														</label>
														';
												} ?>
												<label class="radio-inline" style="padding-left:10px;padding-top:0px">
													<input  maxlength="100" class="form-control" name="org_lv" id="org_lv" type="text" style="max-width: 500px;">
												</label>
											</div>	
										</div>
									</div>
									<div class="col-sm-3">
										<select name="org_name" id="org_name" class="form-control" style="float:inherit;width:290px;" >
					                          <option value="">กรุณาเลือกจากรายการที่ปรากฎ</option>
					                     </select>
									</div>
								</div>
								<div class="form-group row">

									<label class="col-sm-1 control-label">ตำแหน่ง <span class="red">*</span></label>									
									<div class="col-sm-3">
 										<input type="text" class="form-control required"  name="org_position" id="org_position" maxlength="50"  style="float:inherit;">
									</div>
								</div>
								<div class="header">							
								<h4>6. การศึกษา</h4>
								</div><br>
								<div class="form-group row">							
									<label class="col-sm-1 control-label">ระดับ</label>
									<div class="col-sm-3">
										<select name="grd_lv1" id="grd_lv1" class="form-control required">
											<?php foreach ($educationlevel as $key => $value) {
												$id = $value['educationlevel_id'];
												$name = $value['name'];
												echo  "<option value='$id'>$name</option>";
											} ?>
										</select>
									</div>									
									<label class="col-sm-2 control-label">ปีที่สำเร็จการศึกษา</label>
									<div class="col-sm-2">										
										<select id="grd_yr1" name="grd_yr1" class="form-control required">
											<option value="">=ไม่ระบุ=</option>
											<?php
											$yearBegin = date('Y')*1+543;
											for($i=0; $i<=64; $i++){
												$my_y = $yearBegin*1-$i;
												echo "<option value='$my_y'>$my_y</option>
												";
											}
											?>
										</select> 
									</div>																	
									<label class="col-sm-1 control-label">สถาบัน</label>
									<div class="col-sm-3">
										<select id="select_university" name="select_university" class="required form-control"  
											style="float:inherit;" onchange="change_select_university();">
											<option value="">=ไม่ระบุ=</option>
											<?php foreach ($university as $key => $value) {
												$ugrp = $value['UGRP'];
												$uid = $value['UID'];
												$id = $ugrp."-".$uid;
												$name = $value['UNAME'];
												echo  "<option value='$id'>$name</option>";
											} ?>
											<option value="0-0">อื่นๆ</option>
										</select>
										<input type="hidden" id="grd_ugrp1" name="grd_ugrp1" value="0" />
										<input type="hidden" id="grd_uid1" name="grd_uid1" value="0" />
										<input type="hidden" id="grd_uname1" name="grd_uname1" value="" />
									</div>
								</div>
								<div class="form-group row">																	
									<label class="col-sm-1 control-label">สาขา</label>
									<div class="col-sm-3">
											<input class="form-control required" type="text"   name="grd_major1" id="grd_major1" maxlength="50">
									</div>																	
									<label class="col-sm-2 control-label">อื่น ๆ (ระบุเอง)</label>
									<div class="col-sm-3">
											<input class="form-control" type="text"   name="grd_other" id="grd_other" maxlength="50">
									</div>
							  </div>

*/ 
?>



						  </div>

						  <div class="clear"></div>						
						  <div class="form-group">
								<div class="col-md-12">
									<div class="form-group row" style="text-align:left;"><hr>																		
										<label style="display:none;" class="checkbox-inline" style="padding-left:10px;padding-top:0px"> <div aria-disabled="false" aria-checked="false" style="position: relative;" class="icheckbox_square-blue"><input style="position: absolute; opacity: 0;" value="T" name="get_news1" id="get_news1" class="icheck" type="checkbox"><ins style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;" class="iCheck-helper"></ins></div>&nbsp;&nbsp;ยินยอมรับข่าวสารทาง e-mail จาก ATI (ฟรี)</label> 
								 		<!--  <div class="clear"></div><hr><br> -->
										<button type="button" class="btn btn-primary" onClick="ckSave()">Save changes</button>
										<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p'] ?>');">Cancel</button>
									</div>
								</div>
						  </div>

						</form>
					  </div>
					</div>
					
				  </div>
				</div>

		</div>
	</div> 
</div>
<?php include_once ('inc/js-script.php'); ?>
<script type="text/javascript">
  $(document).ready(function() {
   $("#frmMain").validate();
   var member_id = "<?php echo $_GET["member_id"]?>";
   if(member_id) viewInfo(member_id);
   $("#birthdate").datepicker({language:'th-th',format:'dd-mm-yyyy'});
   $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue checkbox',
        radioClass: 'iradio_square-blue'
      });


   	//check alert
	$.ajax({
        url: 'data/ajax-check-session.php',
        data: {
            session_name: 'member_quick_edit',
            session_type: 'alert',
            destroy: true,
        },
        type: 'GET',
        dataType: 'json',
        success: function(data){
            // console.log(data);
            // location.reload();

            if(data.status=='error'){
				$.gritter.removeAll({
			        after_close: function(){
			          $.gritter.add({
			          	position: 'center',
				        title: 'Error',
				        text: data.msg,
				        class_name: 'danger'
				      });
			        }
			     });
		     }

		     if(data.status=='success'){
				$.gritter.removeAll({
			        after_close: function(){
			          $.gritter.add({
			          	position: 'center',
				        title: 'success',
				        text: data.msg,
				        class_name: 'success'
				      });
			        }
			    });
			 }
        }// success
    });	 //end ajax


	$(function(){
		$("#title_th").change(function(){
			if($(this).val() == 'นาย'){ 
				$("#title_en").val("Mr");
				$("#title_th_text").hide();
				$("#title_en_text").hide();
				if($("#title_th_text").hasClass('required'))  $("#title_th_text").removeClass('required');
				if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
				$("#gender").val("M");
			}
			else if($(this).val() == 'นาง'){ 
				$("#title_en").val("Mrs");
				$("#title_th_text").hide();
				$("#title_en_text").hide();
				if($("#title_th_text").hasClass('required'))  $("#title_th_text").removeClass('required');
				if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
				$("#gender").val("F");
			}
			else if($(this).val() == 'นางสาว'){ 
				$("#title_en").val("Ms");
				$("#title_th_text").hide();
				$("#title_en_text").hide();
				if($("#title_th_text").hasClass('required'))  $("#title_th_text").removeClass('required');
				if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
				$("#gender").val("F");
			}
			else if($(this).val() == 'อื่นๆ'){ 
				$("#title_en").val("Other");
				$("#title_th_text").show();
				$("#title_en_text").show();
				$("#title_th_text").addClass('required');
				$("#title_en_text").addClass('required');
				$("#gender").val("");
			}
		});

		$("#title_en").change(function(){
			if($(this).val() == 'Mr'){ 
				$("#title_en_text").hide();
				if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
				$("#gender").val("M");
			}
			else if($(this).val() == 'Mrs'){ 
				$("#title_en_text").hide();
				if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
				$("#gender").val("F");
			}
			else if($(this).val() == 'Ms'){ 
				$("#title_en_text").hide();
				if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
				$("#gender").val("F");
			}
			else if($(this).val() == 'Other'){ 
				$("#title_en_text").show();
				$("#title_en_text").addClass('required');
				$("#gender").val("");
			}
		});
		$("#receipt_title").change(function(){
			if($(this).val() == 'นาย'){ 
				$("#receipt_title_text").hide();
				if($("#receipt_title_text").hasClass('required'))  $("#receipt_title_text").removeClass('required');
			}
			else if($(this).val() == 'นาง'){ 
				$("#receipt_title_text").hide();
				if($("#receipt_title_text").hasClass('required'))  $("#receipt_title_text").removeClass('required');
			}
			else if($(this).val() == 'นางสาว'){ 
				$("#receipt_title_text").hide();
				if($("#receipt_title_text").hasClass('required'))  $("#receipt_title_text").removeClass('required');
			}
			else if($(this).val() == 'อื่นๆ'){ 
				$("#receipt_title_text").show();
				$("#receipt_title_text").addClass('required');
			}
		});
		$(".radio-inline").click(function(){
			$(this).find($("input[type=radio][name='slip_type']")).change(function(){   
				var t = $(this).val();
				if(t == 'corparation'){ 
					$("#taxno").addClass('required');				
				}else{ 
					if($("#taxno").hasClass('required'))  $("#taxno").removeClass('required');
				}
			});

		});


		$('input[name=show_pass]').on('ifChecked', function(event){
			$("#password, #password2").attr("type", "text");
		});			
		$('input[name=show_pass]').on('ifUnchecked', function(event){
			$("#password, #password2").attr("type", "password");
		});	

		$("input[name='receipttype_id']").on('ifChanged', function(event){
			if($(this).val() == '5'){ 
				$("#receipttype_text").addClass('required');
			}else{ 
				if($("#receipttype_text").hasClass('required'))  $("#receipttype_text").removeClass('required');
			}
			$("#taxno").val('');
			$("#branch").val('');
			checkboxaddress_reset();
		});
		$('input[name=same_addr]').on('ifChecked', function(event){
		    checkboxaddress();
		});		
		$('input[name=same_addr]').on('ifUnchecked', function(event){
		    checkboxaddress_reset();
		});	

		$('input[name=nation]').on('ifUnchecked', function(event){
			$("#cid").addClass("cart_id");
		});
		$('input[name=nation]').on('ifChecked', function(event){
			$("#cid").removeClass("cart_id");
		});	

	});

 });
function hide_org_name(t){   
	if(t == 5){ 
		$("#org_name").hide();			
	}else{ 
		$("#org_name").show();
	}
};
 function viewInfo(member_id){
   if(typeof member_id=="undefined") return;
   getmemberInfo(member_id);
}

function getmemberInfo(id){
	if(typeof id=="undefined") return;
	var url = "data/memberlist.php";
	var param = "member_id="+id+"&single=T";
		$.ajax( {
		"dataType":'json', 
		"type": "POST",
		"async": false, 
		"url": url,
		"data": param, 
		"success": function(data){	
			 $.each(data, function(index, array){   
			 	//console.log(array);             
				$("#taxno").val(array.taxno);
				$("#branch").val(array.branch);
				$("#receipt_no").val(array.no);
				$("#receipt_gno").val(array.gno);
				$("#receipt_moo").val(array.moo);
				$("#receipt_soi").val(array.soi);
				$("#receipt_road").val(array.road);
				$("#receipt_postcode").val(array.postcode);

				$("#nation").val(array.nation);
				var t = array.slip_type;
				corparation_info(t);
				$("select[name=receipt_province_id] option").filter(function() {
					return $(this).val() == array.province_id; 
				}).attr('selected', true);	
				var select = $("#receipt_amphur_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.receipt_amphur_id+'"> '+array.receipt_amphur_name+' </option>');
				var select = $("#receipt_district_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.receipt_district_id+'"> '+array.receipt_district_name+' </option>');	
				var select = $("#amphur_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.amphur_id+'"> '+array.amphur_name+' </option>');	
				var select = $("#district_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.district_id+'"> '+array.district_name+' </option>');
				var select = $("#receipt_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.receipt_id+'"> '+array.receipt_name+' </option>');
				setVal("#frmMain", array);
				var select = $("#org_name");
				$('option', select).remove();
				$(select).append('<option value="'+array.org_name+'"> '+array.org_name_text+' </option>'); 
				if(!array.org_name_text){
				  array.org_name_text = '-';
				}
				if(array.title_th=="อื่นๆ"){
					$("#title_th_text").show();
				}
				if(array.title_en=="Other"){
					$("#title_en_text").show();
				}
				hide_org_name(array.orgtype_id);
				if (array.receipttype_id == 5) {
					$("#receipttype_text").val(array.receipttype_text);
				}

				if (array.orgtype_id == 5) {
					//console.log(array.orgtype_id);
					//$("#receipttype_text").val(array.receipttype_text);
				}

			 });				 
		}
	});
}
function corparation_click(tag){
	$(tag).find('input').iCheck('check');
	var desp = $(tag).find('input').val();
	corparation_info(desp);
}
function corparation_info(desp){
	if(desp=="corparation") {
		$("#corparationinfo").show();
		$("#section_corparation_receipt").show();
		$("#section_individuals_receipt").hide();
        $("#taxno").val('');
        $("#branch").val('');
	}else{
		$("#corparationinfo").hide();
		$("#section_corparation_receipt").hide();
		$("#section_individuals_receipt").show();
		var select = $("#receipt_id");
		var options = select.attr('options');
		$('option', select).remove();
		$(select).append('<option value="">---- เลือก ----</option>');
        $("#taxno").val('');
        $("#branch").val('');
	}
	checkboxaddress_reset();
}

function receipt_info(id){
	if(typeof id=="undefined") return;
	var url = "data/receipt.php";
	var param = "name=receipt_detail&value="+id;
	$.ajax( {
		"dataType":'json', 
		"type": "GET",
		"async": false, 
		"url": url,
		"data": param, 
		"success": function(data){
			//console.log(data);	
			$.each(data, function(index, array){	
				// console.log(array);		 				 	
				$("#taxno").val(array.taxno);
				$("#branch").val(array.branch);
				$("#receipt_no").val(array.no);
				$("#receipt_gno").val(array.gno);
				$("#receipt_moo").val(array.moo);
				$("#receipt_soi").val(array.soi);
				$("#receipt_road").val(array.road);
				$("#receipt_postcode").val(array.postcode);

				$("#receipt_province_id").val(array.province_id);
				/*$("select[name=receipt_province_id] option").filter(function() {
					return $(this).val() == array.province_id; 
				}).attr('selected', true);*/		
				var select = $("#receipt_amphur_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.amphur_id+'"> '+array.amphur_name+' </option>');	
				var select = $("#receipt_district_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.district_id+'"> '+array.district_name+' </option>');	
				//console.log(array);				
			});	
		}
	});
}
function removeImg(){
  var defaultImg = "images/no-avatar-male.jpg";
  if($('input[name=tmpimg]').val()=="")return;
  var t = confirm("ลบรูปภาพ");
  if(!t) return;
  $('#img').attr('src', defaultImg);
  $('input[name=delimg]').val("T");	
  ckSave();
}

//==========================================================================//
function ckSave(id){
	//var checkboxes = $("#frmMain").find(':checkbox');
	onCkForm("#frmMain");
	$("#frmMain").submit();
}

function reset_passwd(){
	var cid = $("#cid").val();
	var member_id = "<?php echo $member_id; ?>";

    var r = confirm("ยืนยันการ Reset Password");
    if (r == true) {
       	var url = "update-member.php?flag='reset_passwd'&member_id="+member_id+"&cid="+cid;
		window.open(url,'_self');
    } else {

    }
}

function select_nation(){
	var nation = $("#nation").val();
	if ( nation=='T' ) {
		$("#cid").addClass("cart_id");
	}else{
		$("#cid").removeClass("cart_id");
	}
}
//=========================================================================//

function checkboxaddress(){    
  $("#receipt_title").val($("#title_th").val());
  if($("#receipt_title").val() == 'อื่นๆ'){ 
    $("#receipt_title_text").show();
    $("#receipt_title_text").addClass('required');
    $("#receipt_title_text").val($("#title_th_text").val());
  }
  else
  {
    $("#receipt_title_text").hide();
    $("#receipt_title_text").removeClass('required');
    $("#receipt_title_text").val("");
  }
  $("#receipt_fname").val($("#fname_th").val());
  $("#receipt_lname").val($("#lname_th").val());    
  
  $("#taxno").val($("#cid").val());
  $("#receipt_no").val($("#no").val());
  $("#receipt_gno").val($("#gno").val());
  $("#receipt_moo").val($("#moo").val());
  $("#receipt_soi").val($("#soi").val());
  $("#receipt_road").val($("#road").val());
  
  $('#receipt_province_id').val($('#province_id').val());
  
  var select = $("#receipt_amphur_id");
  $('option', select).remove();
  $(select).append('<option value="'+$("#amphur_id").val()+'"> '+ $("#amphur_id").find(":selected").text()+' </option>'); 
  
  var select = $("#receipt_district_id");
  $('option', select).remove();
  $(select).append('<option value="'+$("#district_id").val()+'"> '+$("#district_id").find(":selected").text()+' </option>');  
      
  $("#receipt_postcode").val($("#postcode").val());

}

function checkboxaddress_reset()
{
 $("#receipt_title").val("");
 $("#receipt_title_text").hide();
    $("#receipt_title_text").removeClass('required');
    $("#receipt_title_text").val("");
  $("#receipt_fname").val("");
  $("#receipt_lname").val("");    
  
  $("#taxno").val("");
  $("#receipt_no").val("");
  $("#receipt_gno").val("");
  $("#receipt_moo").val("");
  $("#receipt_soi").val("");
  $("#receipt_road").val("");
  
  $('#receipt_province_id').val("");
  
  var select = $("#receipt_amphur_id");
  $('option', select).remove();
  $(select).append('<option value="">---- เลือก ----</option>');
  
  var select = $("#receipt_district_id");
  $('option', select).remove();
  $(select).append('<option value="">---- เลือก ----</option>');
      
  $("#receipt_postcode").val("");
}
	function change_select_university()
	{
		 var id = $("#select_university").val().split("-");
		 $("#grd_ugrp1").val(id[0]);
		 $("#grd_uid1").val(id[1]);
		 $("#grd_uname1").val($("#select_university option:selected").text());


		if ($("#select_university option:selected").val() != '0-0')
		{
		   $("#grd_other").attr('disabled','disabled');
		   $("#grd_other").val("");
		}
		else
		{
		  $("#grd_other").removeAttr('disabled');
		}

	}
</script>