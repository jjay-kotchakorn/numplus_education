<?php
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/datatype.php";
include_once "../share/course.php";
global $db;
$date_start_th = $_GET['date_start'];
$date_stop_th = $_GET['date_stop'];
//-- test data
/*$date_start_th = '01-03-2559';
$date_stop_th = '01-03-2559';*/
//-- end
$sec_id = $_GET['sec_id'];
$costy_id = $_GET['costy_id'];
$pay_by = trim($_GET['pay_by']);
$cos_dtail = $_GET['cos_dtail'];
//$name = $_GET['name'];

header("Content-Type: application/vnd.ms-excel");
header('Content-Disposition: attachment; filename="receipt-tax2.xls"');# ชื่อไฟล์

$ar_date_st = explode("-", $date_start_th);
$ar_date_st[2] = $ar_date_st[2]-543;
$date_start = $ar_date_st[2]."-".$ar_date_st[1]."-".$ar_date_st[0]." 00:00:00";
$ar_date_en = explode("-", $date_stop_th);
$ar_date_en[2] = $ar_date_en[2]-543;
$date_stop = $ar_date_en[2]."-".$ar_date_en[1]."-".$ar_date_en[0]." 23:59:59";

$cond = "";
$cond_date = "";
$cond .= "r.active = 'T' AND r.auto_del = 'F' AND r.pay_status IN (3,9) AND (r.docno <> '') AND (r.docno IS NOT NULL)";
if (isset($sec_id) && !empty($sec_id)) {
    $cond .= " AND r.section_id=$sec_id";
}
if (isset($costy_id) && !empty($costy_id)) {
    $cond .= " AND r.coursetype_id=$costy_id";
}
if ( isset($pay_by) && !empty($pay_by) && ($pay_by != "undefined") ) {
    if ( $pay_by==="at_ati" ) {
        $cond .= " AND (r.pay='at_ati' OR r.pay='walkin' OR r.pay='importfile')";
    }else{
        $cond .= " AND r.pay='$pay_by'";
    }//end else
}
if (isset($name) && !empty($name))  {
    $name = explode(" ", $name);
    $lname = trim($name[1]);
    $cond .= " AND r.lname='$lname'";
}
if ( (isset($date_start) && !empty($date_start)) || (isset($date_stop) && !empty($date_stop)) ) {
    if ( (isset($date_start) && !empty($date_start)) && !(isset($date_stop) && !empty($date_stop)) ) {
        $cond_date .= " AND r.pay_date='$date_start'";
    }
    if ( !(isset($date_start) && !empty($date_start)) && (isset($date_stop) && !empty($date_stop)) ) {
        $cond_date .= " AND r.pay_date='$date_stop'";
    }else{
        $cond_date .= " AND (r.pay_date BETWEEN '$date_start' AND '$date_stop')"; 
    }
}
$qry = "SELECT r.register_id
        , r.course_price
        , r.course_discount
        , r.date
        , r.docno
        , r.rectime
        , r.title
        , r.fname
        , r.lname
        , r.taxno
        , r.receipt_id
        , r.pay
        , r.pay_date
        , r.course_id
        , r.course_detail_id
        , cd.code_project
        , rc.name
        , rc.branch  
    FROM register AS r 
    LEFT JOIN course_detail AS cd ON cd.course_detail_id = r.course_detail_id
    LEFT JOIN receipt AS rc ON rc.receipt_id = r.receipt_id  
    WHERE $cond $cond_date
    ORDER BY r.pay_date ASC
    ;";
$rs = $db->get($qry);
//var_dump($qry);
//echo count($rs);
// d($rs);

$cond_date = str_replace("r.pay_date", "credit_note_date", $cond_date);
$q = "SELECT register_credit_note_id
        , credit_note_date
        , ref_register_id
        , docno
    FROM register_credit_note
    WHERE active='T'
        $cond_date
";
// echo $q;
$cn_info = $db->get($q);
// d($cn_info);

if ( !empty($cn_info) ) {

    foreach ($cn_info as $key => $v) {
        
        $register_credit_note_id = $v["register_credit_note_id"];
        $credit_note_date = $v["credit_note_date"];
        $ref_register_id = $v["ref_register_id"];
        $docno = $v["docno"];

        $qry = "SELECT r.register_id
                , r.course_price
                , r.course_discount
                , r.date
                , r.docno
                , r.rectime
                , r.title
                , r.fname
                , r.lname
                , r.taxno
                , r.receipt_id
                , r.pay
                , r.pay_date
                , r.course_id
                , r.course_detail_id
                , cd.code_project
                , rc.name
                , rc.branch  
            FROM register AS r 
            LEFT JOIN course_detail AS cd ON cd.course_detail_id = r.course_detail_id
            LEFT JOIN receipt AS rc ON rc.receipt_id = r.receipt_id  
            WHERE $cond
                AND r.register_id={$ref_register_id} 
            ORDER BY r.pay_date ASC
        ";
        // echo $qry;
        $reg_info = $db->rows($qry);
        unset($qry);
        // d($reg_info);

        if ( !empty($reg_info) ) {
            // d($rs); die();
            $reg_info["pay_date"] = $credit_note_date;
            $reg_info["docno"] = $docno;
            $reg_info["pay_status"] = 99;
            $rs[] = $reg_info;
        }//end if
    }//end loop $v
}//end if



?>

<html xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<html lang="en">
<HEAD>
<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
</HEAD>
<body onload="">
    <!-- <div class="fix-width"> -->
    <div class="form-landscape">
        <div>
            <table class="no-border">
                <tbody>
                    <tr>
                        <td><span class="center font-weight-bold">รายงานภาษีขาย</span></td>
                    </tr>
                    <tr>
                        <td><span class="center"><?php echo "ระหว่างวันที่ ".$date_start_th." ถึง ".$date_stop_th;?></span></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div>
            <table class="no-border">
                <tbody>
                    <tr>
                        <td colspan="2" width="120" class="noborder font-weight-bold">ชื่อผู้ประกอบการ</td>
                        <td colspan="8" class="noborder">สมาคมบริษัทหลักทรัพย์ไทย</td>
                        <td colspan="2" width="170" class="noborder font-weight-bold">เลขประจำตัวผู้เสียภาษีอากร</td>
                        <td colspan="2" width="100" class="noborder font-weight-bold center">09930001324110</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="font-weight-bold noborder">สถานประกอบการ</td>
                        <td colspan="8" class="noborder">อาคารชุดเลครัชดาออฟฟิตคอมเพล็กซ์2 ชั้น 5 เลขที่ 195/6 ถนนรัชดาภิเษก แขวงคลองเตย เขตคลองเตย กรุงเทพมหานคร 10110</td>
                        <td colspan="4" class="center font-weight-bold noborder">สำนักงานใหญ่</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div><h6></h6></div>
        <div><span clas="left"><h5>VAT Code OG01 - Output VAT - Goods 7%</h5></span></div>
        <!-- <div> -->          
    <div>
        <table class="td-center">
            <br>    
            <thead>
                <tr>
                    <td><span class="center"><br><br>ลำดับที่</span></td>
                    <td colspan="3">
                        <table class="no-border">
                            <tbody>
                                <tr>
                                    <td colspan="3"><span class="under-line-center">ใบกำกับภาษี</span></td>
                                </tr>
                                <tr>
                                    <td width="80"><span class="right-line-center"><br><br><br>วัน เดือน ปี</span></td>
                                    <td width="80"><span class="right-line-center"><br><br><br>วันที่เอกสาร</span></td>
                                    <td width="80"><span class="center"><br><br><br>เลขที่เอกสาร</span></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td><span class="center"><br>เลขประจำตัวผู้เสียภาษีอากร<br>ของผู้ซื้อสินค้าบริการ</span></td>
                    <td colspan="2">
                        <table class="no-border">
                            <tbody>
                                <tr>
                                    <td colspan="2"><span class="under-line-center">สถานประกอบการ</span></td>
                                </tr>
                                <tr>
                                    <td width="100"><span class="right-line-center"><br><br><br>ชื่อผู้ซื้อบริการ</span></td>
                                    <td width="60"><span class="right-line-center"><br><br><br>สาขา</span></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td><span class="center"><br><br>มูลค่าสินค้า<br>หรือบริการ</span></td>
                    <td><span class="center"><br><br>จำนวนเงิน<br>ภาษีมูลค่าเพิ่ม</span></td>
                    <td><span class="center"><br><br>รวม</span></td>
                </tr>
            </thead>                    
<?php           
            $page=1;
            $i=0;
            $sum_pri = 0;
            $sum_no_vat = 0;
            $sum_vat = 0;
            $sum_dis = 0;
            $sum_all_cos_pri = 0;
            $sum_all_cos_dis = 0;
            $rs_ttl = count($rs);
            $ttl_page = intval($rs_ttl/10);
            foreach ($rs as $key => $value) {
                $pay_status = $value["pay_status"];
                $cos_dis = !empty($value["course_discount"]) ? (float)$value["course_discount"] : 0;
                $cos_pri = $value["course_price"]-$cos_dis;
                $cos_no_vat = ($cos_pri/1.07);

                if ( $pay_status==99 ) {
                    $cos_pri = $cos_pri*(-1);
                    $cos_dis = $cos_dis*(-1);
                    $cos_no_vat = $cos_no_vat*(-1);
                    $cos_vat = $cos_vat*(-1);
                }//end if

                $cos_vat = $cos_pri-$cos_no_vat;
                $sum_no_vat = $sum_no_vat+$cos_no_vat;
                $sum_vat = $sum_vat+$cos_vat;
                $sum_pri = $sum_pri+$cos_pri;
                $sum_dis = $sum_dis+$cos_dis;
                $name = $value["title"].$value["fname"]." ".$value["lname"];
                $receipt_id = $value["receipt_id"];
                $org_name = "";
                $org_branch = "";
                if ( $receipt_id=='' || $receipt_id==NULL ) {
                    $org_name = $name;
                    $org_branch = "";
                }else{
                    $org_name = $value["name"];
                    $org_branch = $value["branch"];
                }// end else
                if ( empty($value["code_project"]) ) {
                    $cos_dtail_ids = explode(",", $value["course_detail_id"]);
                    $cos_dtail_id = $cos_dtail_ids[1];
                    $cos_dtail_id = explode(":", $cos_dtail_id);
                    $cos_dtail_id = trim($cos_dtail_id[1]);
                    $info_cos_dtail = get_course_detail("", $cos_dtail_id);
                    $value["code_project"] = $info_cos_dtail[0]['code_project'];
                }
                //echo $value["course_id"];
                $cos_ids = $value["course_id"];
                $cos_ids = explode(",", $cos_ids);
                $sum_cos_pri = 0;
                foreach ($cos_ids as $key => $id) {
                    //var_dump($id);
                    $id = trim($id, ",");
                    $cos_info = get_course("", $id);
                    $cos_info = $cos_info[0];
                    //var_dump($cos_info["price"]);
                    //die();
                    $cos_info["price"] = ($pay_status==99) ? $cos_info["price"]*(-1) : $cos_info["price"];
                    $sum_cos_pri = $sum_cos_pri+$cos_info["price"];
                }//end loop $id
             
                $sum_cos_dis = $sum_cos_pri - $cos_pri;
                $sum_all_cos_pri = $sum_all_cos_pri+$sum_cos_pri;
                $sum_all_cos_dis = $sum_all_cos_dis+$sum_cos_dis;
                $sum_cos_pri = number_format($sum_cos_pri, 2, '.', ',');
                $sum_cos_dis = number_format($sum_cos_dis, 2, '.', ',');
                $cos_pri = number_format( $cos_pri, 2, '.', ',' );
                $cos_no_vat = number_format( $cos_no_vat, 2, '.', ',' );
                $cos_vat = number_format( $cos_vat, 2, '.', ',' );
                $cos_dis = number_format( $cos_dis, 2, '.', ',' );
                $i++;
                $cut_row = $i%10;
                if ($i==1 || $cut_row==1) {
?>
<?php
                }//end if
?>
                <tbody>
                    <tr>
                        <td width="30"><span class="center"><?php echo $i;?></span></td>
                        <td width="75"><span class="center"><?php $date = explode(" ", $value["pay_date"]); echo revert_date($date[0]);?></span></td>   
                        <td width="75"><span class="center"><?php $date_doc = explode(" ", $value["pay_date"]); echo revert_date($date_doc[0]); ?></span></td>      
                        <td width="75"><span class="center"><?php echo $value["docno"];?></span></td>
                        <td width="80"><span class="center"><?php echo $value["taxno"];?></span></td>
                        <td width="100"><span class="left"><?php echo $org_name;?></span></td>
                        <td width="60"><span class="left"><?php echo $org_branch;?></span></td>
                        <td width="70"><span class="right"><?php echo $cos_no_vat;?></span></td>
                        <td width="70"><span class="right"><?php echo $cos_vat;?></span></td>
                        <td width="70"><span class="right"><?php echo $cos_pri;?></span></td>
                    </tr>
<?php           
                if ($cut_row==1 && $i!=1) { 
?>
                    <br>
                    <div style="page-break-after: always;"></div>
<?php                   
                }
                //echo "Page ".$page." / ".$ttl_page;
                $page++;            
            }// end loop for
                $sum_pri = number_format( $sum_pri, 2, '.', ',' );
                $sum_no_vat = number_format( $sum_no_vat, 2, '.', ',' );
                $sum_vat = number_format( $sum_vat, 2, '.', ',' );
                $sum_dis = number_format( $sum_dis, 2, '.', ',' );
?>
                    <tr>
                        <td colspan="7" class="center">รวม</td>                
                        <td><span class="right"><?php echo $sum_no_vat;?></span></td>
                        <td><span class="right"><?php echo $sum_vat;?></span></td>
                        <td><span class="right"><?php echo $sum_pri;?></span></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</body>
</html>