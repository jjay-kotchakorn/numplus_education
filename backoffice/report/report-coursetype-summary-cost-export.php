<?php
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/course.php";
include_once "../share/datatype.php";
global $db, $EMPID;

/** PHPExcel */
require_once "./Classes/PHPExcel.php";

$date_now = date('Y-m-d H:i:s');
$date = date('Y-m-d');
// d($_GET);die();
$date_start = trim($_GET["date_start"]);
$date_stop = trim($_GET["date_stop"]);
$pay = trim($_GET["pay"]);
$coursetype_id = trim($_GET["coursetype_id"]);
$section_id = trim($_GET["section_id"]);
$code_project = trim($_GET["code_project"]);

$cond_cos = "";
if ( !empty($coursetype_id) ) {
	$cond_cos .= " AND r.coursetype_id={$coursetype_id}";
}//end if

if ( !empty($section_id) ) {
	$cond_cos .= " AND r.section_id={$section_id}";
}//end if

//filter cond payment_type
$con_pay="";
switch ($pay) {
	case 'money':
		$con_pay .= " AND p.payment_type='money'";
		$pay_txt = "เงินสด";
		break;
	case 'paysbuy':
		$con_pay .= " AND p.payment_type='paysbuy'";
		$pay_txt = "Paysbuy";
		break;
	case 'bbl':
		$con_pay .= " AND p.payment_type='bank' AND p.bank_code='002'";
		$pay_txt = "BBL";
		break;
	case 'kbank':
		$con_pay .= " AND p.payment_type='bank' AND p.bank_code='004'";
		$pay_txt = "KBANK";
		break;
	case 'mpay':
		$con_pay .= " AND p.payment_type='mpay'";
		$pay_txt = "mPAY";
		break;	
}//end sw
// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Nattapon Booncharoen")
							 ->setLastModifiedBy("Nattapon Booncharoen")
							 ->setTitle("report-coursetype-summary-cost-export")
							 ->setSubject("report-coursetype-summary-cost-export")
							 ->setDescription("-")
							 ->setKeywords("report-coursetype-summary-cost-export")
							 ->setCategory("export to excel file");	

// Add some data column						 
$objPHPExcel->getActiveSheet()
			->setCellValue('A1', "ช่องทาง {$pay_txt}")
            ->setCellValue('A2', 'วันที่')
            ->setCellValue('B2', 'Project code')
            ->setCellValue('C2', 'รายวิชา')
			->setCellValue('D2', 'จำนวน')
			->setCellValue('E2', 'ก่อน VAT')
            ->setCellValue('F2', 'VAT')
            ->setCellValue('G2', 'จำนวนเงิน')
            ->setCellValue('H2', 'รวม')
            ->setCellValue('I2', 'หัก 3%')
            ->setCellValue('J2', 'รวมเป็นเงิน');

// set width column
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(50);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);

$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);


// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle('sheet1');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->getActiveSheet();
// d($objPHPExcel);

$sql = "SELECT DISTINCT(p.payment_date) AS payment_date
		FROM payment_compare_all_list AS p
		INNER JOIN register AS r ON p.register_id=r.register_id
		WHERE p.active='T' AND r.active='T'
			-- AND (p.course_id<>'' OR p.course_id IS NOT NULL)
			AND p.check_result='Y' 
			AND p.payment_date BETWEEN '{$date_start}' AND '{$date_stop}'
			{$con_pay}
			{$cond_cos}
		ORDER BY p.payment_date ASC	
";

$rs = $db->get($sql);
// d($rs);die();

if ( !empty($code_project) ) {
	$code_project =  strtoupper($code_project);
	$cond_cos .= " AND UPPER(d.code_project) LIKE '{$code_project}%'";
}

$ttl_registed = 0;
$ttl_course_price_before_vat = 0;
$ttl_course_price_vat = 0;
$sum_course_price = 0;
$ttl_sum_course_price = 0;
$ttl_sum_fee = 0;
$ttl_sum_amount = 0;

$row = 2;
//get data for built rpt
foreach ($rs as $key => $c) {
	// $row++;
	$payment_date = $c["payment_date"];
	$sql = "SELECT COUNT(p.payment_compare_all_list_id) AS cnt_reg_id
				, p.payment_date
				, p.course_id
				, SUM(p.amount) AS ttl_amount
				, SUM(r.pay_diff) AS ttl_fee
				, r.course_detail_id
				, d.code_project
				, r.course_price
				, r.course_discount
			FROM payment_compare_all_list AS p
			INNER JOIN register AS r ON p.register_id=r.register_id
			INNER JOIN course_detail AS d ON p.course_detail_id=d.course_detail_id
			WHERE p.active='T' 				
				AND p.check_result='Y' 	
				AND p.payment_date='{$payment_date}'
				{$con_pay}
				{$cond_cos}				
			GROUP BY d.code_project, p.course_id
			ORDER BY d.code_project ASC				 	
	";
	// echo $sql;
	$rs = $db->get($sql);
	// d($rs);
	// die();

	foreach ($rs as $key => $v) {
		$row++;
		$cos_dt_id="";
		$course_ids = trim($v["course_id"]);
		if ( empty($course_ids) ) {continue;}
		$course_ids = explode(",", $course_ids);
		$count_course_ids = count($course_ids);
		// echo "cnt cos ids = ".$count_course_ids."<br>";

		$payment_date = explode("-", $v["payment_date"]);
		$payment_date_yyyymmdd = "{$payment_date[0]}{$payment_date[1]}{$payment_date[2]}";

		$course_title = "";
		$course_code_project = "";
		$course_price = 0;
		$course_discount = 0;
		$ttl_course_price = 0;
		$course_price_before_vat = 0;
		$course_price_vat = 0;

		if ( $count_course_ids>1 ) {
			// d($course_ids);

			foreach ($course_ids as $key => $id) {
				$id = trim($id);
				// echo $d."<br>";
				$info_course = get_course("", $id);
				$info_course = $info_course[0];

				if ( !empty($info_course["code_project"])&&isset($info_course["code_project"]) ) { //chk cos refresher 
					if( $coursetype_id==4 || $coursetype_id=='4' ){						
						$course_title .= '-,';
					}else{
						// $course_code_project .= $info_course["code_project"].",";
						$course_title .= $info_course["title"].",";
					}//end else
				}elseif ( empty($info_course["code_project"]) && ($coursetype_id!=4 || $coursetype_id!='4') ) {
					$course_title .= $info_course["title"].",";
				}//end elseif

			}//end loop $d
			$course_price = ($v["course_price"]-$v["course_discount"]);
			$ttl_course_price = $course_price*$v["cnt_reg_id"];

		}else{
			$info_course = get_course("", $course_ids[0]);
			$info_course = $info_course[0];
			
			$t = trim($v["course_detail_id"], ",");
			$x = explode(":", $t);
			$cos_dt_id = (empty($x[1])) ? $v["course_detail_id"] : $x[1];

			$course_price = $info_course["price"]-$info_course["discount"];
			$ttl_course_price = $course_price*$v["cnt_reg_id"];
			
			if ( !empty($info_course["code_project"])&&isset($info_course["code_project"]) ) { //cos refresher 
				if( $coursetype_id==4 || $coursetype_id=='4' ){
					$course_title .= '-,';
				}else{
					$course_title .= $info_course["title"].",";
				}//end else
			}elseif ( empty($info_course["code_project"]) && ($coursetype_id!=4 || $coursetype_id!='4') ) {
				$course_title .= $info_course["title"].",";
			}//end elseif

		}//end else

		$course_code_project = trim($v["code_project"], "");
		$course_title = trim($course_title, "<hr>");

		$course_price_before_vat =  $course_price-($course_price*0.06542);
		$course_price_vat = $course_price-$course_price_before_vat;
		$ttl_fee = (!empty($v["ttl_fee"])) ? $v["ttl_fee"] : 0;
		
		//chk course angry bird
		if ( $ttl_course_price != $v["ttl_amount"] ) {
			// $course_discount = $ttl_course_price-$v["ttl_amount"];
			$course_price = $v["ttl_amount"]/$v["cnt_reg_id"];
			$course_price_before_vat =  $course_price-($course_price*0.06542);
			$course_price_vat = $course_price-$course_price_before_vat;
			$ttl_course_price = $v["ttl_amount"];

		}//end if
		
		$ttl_amount = $v["ttl_amount"] - (float)$ttl_fee;
		$ttl_registed = $ttl_registed+$v["cnt_reg_id"];

		$cos_price = number_format($course_price, 2, ".", "");
		$before_vat = $course_price_before_vat;
		$vat = $course_price_vat;

		$vat = number_format($vat, 2, ".", "");
		$before_vat = number_format($before_vat, 2, ".", "");
		$sum = number_format($before_vat+$vat, 2, ".", "");

		if ( $cos_price != $sum ) { 
			$before_vat = $cos_price-$vat;
			$sum = number_format($before_vat+$vat, 2, ".", "");
			$course_price_before_vat = $before_vat;
			$course_price_vat = $vat;
			// echo "cos_price = $cos_price | before_vat = $before_vat | vat = $vat | sum = $sum <br>";
		}//end if
		
/*
		echo revert_date($v["payment_date"])."<br>";
		echo $course_code_project."<br>";
		echo $course_title."<br>";
		echo $v["cnt_reg_id"]."<br>";
		echo $course_price_before_vat."<br>";

		echo $course_price_vat."<br>";
		echo $course_price."<br>";
		echo $ttl_course_price."<br>";
		echo $course_discount."<br>";
		echo $ttl_fee."<br>";

		echo $ttl_amount."<br>, ";
*/

		// echo "payment_date_yyyymmdd={$payment_date_yyyymmdd}|";

		$objPHPExcel->getActiveSheet()->setCellValue('A' . $row, $payment_date_yyyymmdd);
		$objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $course_code_project);
		$objPHPExcel->getActiveSheet()->setCellValue('C' . $row, $course_title);
		$objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $v["cnt_reg_id"]);
		$objPHPExcel->getActiveSheet()->setCellValue('E' . $row, number_format($course_price_before_vat, 2, '.', ','));
		$objPHPExcel->getActiveSheet()->setCellValue('F' . $row, number_format($course_price_vat, 2, '.', ','));
		$objPHPExcel->getActiveSheet()->setCellValue('G' . $row, number_format($course_price, 2, '.', ','));		
		$objPHPExcel->getActiveSheet()->setCellValue('H' . $row, number_format($ttl_course_price, 2, '.', ','));
		$objPHPExcel->getActiveSheet()->setCellValue('I' . $row, number_format($ttl_fee, 2, '.', ','));
		$objPHPExcel->getActiveSheet()->setCellValue('J' . $row, number_format($ttl_amount, 2, '.', ','));
	
		$ttl_course_price_before_vat = $ttl_course_price_before_vat + $course_price_before_vat;
		$ttl_course_price_vat = $ttl_course_price_vat+$course_price_vat;
		$sum_course_price = $sum_course_price+$course_price;
		$ttl_sum_course_price = $ttl_sum_course_price+$ttl_course_price;
		$ttl_sum_fee = $ttl_sum_fee+$ttl_fee;
		$ttl_sum_amount = $ttl_sum_amount+$ttl_amount;
	
	}//end loop $v


	// $row++;
	/*
	<tr>
		<td colspan="3" rowspan="" headers="" class="center">รวม</td>
		<td colspan="" rowspan="" headers="" class="center"><?php echo number_format($ttl_registed);?></td>
		<td colspan="" rowspan="" headers="" class="right"><?php echo number_format($ttl_course_price_before_vat, 2 ,".", ",");?></td>
		<td colspan="" rowspan="" headers="" class="right"><?php echo number_format($ttl_course_price_vat, 2, ".", ",");?></td>
		<td colspan="" rowspan="" headers="" class="right"><?php echo number_format($sum_course_price, 2, ".", ",");?></td>
		<td colspan="" rowspan="" headers="" class="right"><?php echo number_format($ttl_sum_course_price, 2, ".", ",");?></td>
		<td colspan="" rowspan="" headers="" class="right"><?php echo number_format($ttl_sum_fee, 2, ".", ",");?></td>
		<td colspan="" rowspan="" headers="" class="right"><?php echo number_format($ttl_sum_amount, 2, ".", ",");?></td>
	</tr>
	*/
}//end loop $c
$row++;
$objPHPExcel->getActiveSheet()->setCellValue('A' . $row, '');
$objPHPExcel->getActiveSheet()->setCellValue('B' . $row, '');
$objPHPExcel->getActiveSheet()->setCellValue('C' . $row, 'รวม');
$objPHPExcel->getActiveSheet()->setCellValue('D' . $row, number_format($ttl_registed));
$objPHPExcel->getActiveSheet()->setCellValue('E' . $row, number_format($ttl_course_price_before_vat, 2 ,".", ","));

$objPHPExcel->getActiveSheet()->setCellValue('F' . $row, number_format($ttl_course_price_vat, 2, ".", ","));
$objPHPExcel->getActiveSheet()->setCellValue('G' . $row, number_format($sum_course_price, 2, ".", ","));		
$objPHPExcel->getActiveSheet()->setCellValue('H' . $row, number_format($ttl_sum_course_price, 2, ".", ","));
$objPHPExcel->getActiveSheet()->setCellValue('I' . $row, number_format($ttl_sum_fee, 2, ".", ","));
$objPHPExcel->getActiveSheet()->setCellValue('J' . $row, number_format($ttl_sum_amount, 2, ".", ","));

	// die();

//set FORMAT_NUMBER_COMMA_SEPARATED1
$objPHPExcel->getActiveSheet()
    ->getStyle("E3:E{$row}")
    ->getNumberFormat()
    ->setFormatCode(
        PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1
);
$objPHPExcel->getActiveSheet()
    ->getStyle("F3:F{$row}")
    ->getNumberFormat()
    ->setFormatCode(
        PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1
);
$objPHPExcel->getActiveSheet()
    ->getStyle("G3:G{$row}")
    ->getNumberFormat()
    ->setFormatCode(
        PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1
);
$objPHPExcel->getActiveSheet()
    ->getStyle("H3:H{$row}")
    ->getNumberFormat()
    ->setFormatCode(
        PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1
);
$objPHPExcel->getActiveSheet()
    ->getStyle("I3:I{$row}")
    ->getNumberFormat()
    ->setFormatCode(
        PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1
);
$objPHPExcel->getActiveSheet()
    ->getStyle("J3:J{$row}")
    ->getNumberFormat()
    ->setFormatCode(
        PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1
);


//set HORIZONTAL_RIGHT
$objPHPExcel->getActiveSheet()
    ->getStyle("G3:G{$row}")
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
);
$objPHPExcel->getActiveSheet()
    ->getStyle("H3:H{$row}")
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
);
$objPHPExcel->getActiveSheet()
    ->getStyle("J3:J{$row}")
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
);

// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="report-coursetype-summary-cost-export.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

die();
?>