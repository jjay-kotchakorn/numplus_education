<?php
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/course.php";
include_once "../share/datatype.php";
global $db, $EMPID;

/** PHPExcel */
require_once "./Classes/PHPExcel.php";

$date_now = date('Y-m-d H:i:s');
$date = date('Y-m-d');
// var_dump($_REQUEST);
$pay_date = $_REQUEST["pay_date_chk"];
//$pay_date_chk = thai_to_timestamp($pay_date);
$pay_date_chk = $pay_date;
$pay_type_chk = $_REQUEST["pay_type_chk"];
$pay_type_txt = ($pay_type_chk=='bank') ? 'ธนาคาร' : '';

// echo $pay_date;

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Nattapon Booncharoen")
							 ->setLastModifiedBy("Nattapon Booncharoen")
							 ->setTitle("report-payment-compare-bank-export")
							 ->setSubject("report-payment-compare-bank-export")
							 ->setDescription("-")
							 ->setKeywords("report-payment-compare-bank-export")
							 ->setCategory("export to excel file");	

// Add some data column						 
$objPHPExcel->getActiveSheet()
			->setCellValue('A1', 'ข้อมูลใบเสร็จรับเงินจาก WR')
            ->setCellValue('H1', 'ข้อมูลการชำระจาก ธนาคาร')         
            ->setCellValue('A2', 'ลำดับ')
            ->setCellValue('B2', 'เลขที่ใบเสร็จ')
            ->setCellValue('C2', 'วันที่ชำระ')
			->setCellValue('D2', 'ชื่อผู้สมัคร')
			->setCellValue('E2', 'หลักสูตร')
            ->setCellValue('F2', 'Project code')
            ->setCellValue('G2', 'ราคา')
            ->setCellValue('H2', 'ธนาคาร')
            ->setCellValue('I2', 'ชื่อผู้ชำระ')
            ->setCellValue('J2', 'Ref 1')
            ->setCellValue('K2', 'วันที่ชำระ')
            ->setCellValue('L2', 'เวลาที่ชำระ')
            ->setCellValue('M2', 'ยอดชำระ')
            ->setCellValue('N2', 'ผลการตรวจ')
            ->setCellValue('O2', 'หมายเหตุ');

// set width column
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(50);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);

$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15);

$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(50);

// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle('sheet1');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->getActiveSheet();
				 

$sql = "SELECT r.register_id
							, r.course_id
							, r.docno
							, r.pay_date
							, CONCAT(r.title,'',r.fname,' ',r.lname) AS cus_name
							, r.course_price
							, r.course_discount
							, r.ref1
							, r.ref2
							, r.course_detail_id
							, p.payment_compare_bank_list_id
							, p.bank_code
							, p.payment_time
							, p.customer_name
							, p.customer_no_ref1
							, p.ref2
							, p.payment_date
							, p.amount
							, p.payment_compare_file_list_id
							, p.payment_compare_file_list_id
							, b.short_name_eng
							-- , r.pay
						FROM register AS r
						LEFT JOIN payment_compare_bank_list AS p ON (r.ref1=p.customer_no_ref1 AND r.ref2=p.ref2) 
							 AND p.active='T'
							 AND p.pay_date_chk='{$pay_date_chk}'
						LEFT JOIN bank AS b ON b.code=p.bank_code	
						WHERE r.active='T' 
							AND r.pay_status IN (3,9) 
							AND r.pay='bill_payment' 
							AND ( r.docno IS NOT NULL OR r.docno<>'' ) 
							AND ( r.pay_date LIKE '{$pay_date_chk}%' ) 
						ORDER BY p.bank_code, p.payment_time ASC	
				";
// echo $sql;
$rs = $db->get($sql);	

// d($rs);
// die();

$runno=0;	
$ttl_amount=0;
$ttl_course_price=0;
$ttl_cus_from_wr=0;
$ttl_cus_from_bank=0;
$row = 2;

foreach ($rs as $key => $v) {
	$runno++;
	$txt_remark = "";
	$row++;
	if( !empty($v["register_id"]) ) $ttl_cus_from_wr++;
	if( !empty($v["payment_compare_bank_list_id"]) ) $ttl_cus_from_bank++;
	$amount = $v["amount"];
	$course_ids = trim($v["course_id"]);
	$course_ids = explode(",", $course_ids);
	$pay_date = explode(" ", $v["pay_date"]);
	$pay_date = $pay_date[0];
	$course_name="";
	$project_code="";
	$course_price = $v["course_price"]-$v["course_discount"];
	$ttl_course_price = $ttl_course_price+$course_price;
	$ttl_amount = $ttl_amount+$amount;
	// $status_chk = ($amount-$course_price == 0) ? $status_chk='Y' : $status_chk='N';

	if ( $amount-$course_price == 0 ) {
		$status_chk='Y';
	}else{
		$status_chk='N';

		if ( empty($v["customer_no_ref1"]) ) {
			$txt_remark = "ไม่พบข้อมูลผู้ชำระรายนี้ในไฟล์ที่อัพโหลด";
		}else{
			$txt_remark = "ราคาจากระบบ WR และยอดชำระที่ชำระผ่าน {$pay_type_txt} ไม่เท่ากัน";
		}//end else
	}//end else

	// get course name, project code		bankeyboy edit 9/3/60		
	$sql = "SELECT a.course_id
				, a.course_detail_id
				, b.code_project
			FROM register_course_detail a left join  course_detail b on a.course_detail_id=b.course_detail_id
			WHERE a.active='T' AND a.register_id={$v["register_id"]}	
	";
	$rs = $db->get($sql);
	$course_name = "";
	$project_code = "";			
	if($rs){					
		foreach ($rs as $key => $c) {
			$info_cos = get_course("", $c["course_id"]);
			$info_cos = $info_cos[0];
			if ( !empty($info_cos) ) {
				$course_name .= $info_cos["title"].",";
				if ( !empty($c["code_project"]) ) {
					$project_code .= $c["code_project"];
				}else{
					$project_code .= "-";
				}
				$project_code .= ",";
			}// end if
		}// end loop $c
	}else{
		$course_id = $v["course_id"];
		$info_cos = get_course("", $v["course_id"]);
		$info_cos = $info_cos[0];
		if ( !empty($info_cos) ) {
			$course_name .= $info_cos["title"];
		}// end if					
	}
	if(strpos(':', $v["course_detail_id"])==0 && !$project_code){
		$q = "select code_project from course_detail where course_detail_id={$v["course_detail_id"]}";
		$project_code  = $db->data($q);
	}
	$course_name = trim($course_name, "<hr>");
	$project_code = trim($project_code, "<hr>");

	$course_name = trim($course_name, ", ");
	$project_code = trim($project_code, ", ");

	$objPHPExcel->getActiveSheet()->setCellValue('A' . $row, $runno);
	$objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $v["docno"]);
	$objPHPExcel->getActiveSheet()->setCellValue('C' . $row, $pay_date);
	$objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $v["cus_name"]);
	$objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $course_name);
	$objPHPExcel->getActiveSheet()->setCellValue('F' . $row, $project_code);
	$objPHPExcel->getActiveSheet()->setCellValue('G' . $row, $course_price);
	
	$objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $v["short_name_eng"]);
	$objPHPExcel->getActiveSheet()->setCellValue('I' . $row, $v["customer_name"]);
	$objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $v["customer_no_ref1"]);
	$objPHPExcel->getActiveSheet()->setCellValue('K' . $row, $v["payment_date"]);
	$objPHPExcel->getActiveSheet()->setCellValue('L' . $row, $v["payment_time"]);
	$objPHPExcel->getActiveSheet()->setCellValue('M' . $row, $amount);
	$objPHPExcel->getActiveSheet()->setCellValue('N' . $row, $status_chk);

	$objPHPExcel->getActiveSheet()->setCellValue('O' . $row, $txt_remark);

	// echo $course_name."<br>";
	// die();

}//end loop
$ttl_customer = $runno;	
//chk status
if ( ($ttl_amount==$ttl_course_price) && ($ttl_cus_from_wr==$ttl_cus_from_bank) ) {
	$chk_status = 'Y';
}else{
	$chk_status = 'N';
	$txt_status_n = "หมายเหตุ :  ";
	if ( $ttl_amount!=$ttl_course_price ) {
		$txt_status_n .= "จำนวนเงินทั้งหมดจากระบบ WR และจำนวนเงินทั้งหมดที่ชำระผ่าน {$pay_type_txt} ไม่ตรงกัน, ";
	}
	if ( $ttl_cus_from_wr!=$ttl_cus_from_bank ) {
		$txt_status_n .= "จำนวนผู้ชำระเงินจากระบบ WR และจำนวนผู้ชำระเงินจาก {$pay_type_txt} ไม่ตรงกัน, ";
	}
	$txt_status_n = trim($txt_status_n, ", ");
}//end else

$objPHPExcel->getActiveSheet()
    ->getStyle("J3:J{$row}")
    ->getNumberFormat()
    ->setFormatCode(
        PHPExcel_Style_NumberFormat::FORMAT_NUMBER
);

$row++;
$objPHPExcel->getActiveSheet()->setCellValue('B' . $row, 'จำนวนเงินทั้งหมดจากระบบ WR');
$objPHPExcel->getActiveSheet()->setCellValue('G' . $row, $ttl_course_price);
$objPHPExcel->getActiveSheet()->setCellValue('I' . $row, 'จำนวนเงินทั้งหมดที่ชำระผ่าน ธนาคาร');
$objPHPExcel->getActiveSheet()->setCellValue('M' . $row, $ttl_amount);

$row++;
$objPHPExcel->getActiveSheet()->setCellValue('B' . $row, 'จำนวนผู้ชำระเงินจากระบบ WR');
$objPHPExcel->getActiveSheet()->setCellValue('G' . $row, $ttl_cus_from_wr);
$objPHPExcel->getActiveSheet()->setCellValue('I' . $row, 'จำนวนผู้ชำระเงินจาก ธนาคาร');
$objPHPExcel->getActiveSheet()->setCellValue('M' . $row, $ttl_cus_from_bank);
$objPHPExcel->getActiveSheet()->setCellValue('N' . $row, $chk_status);

// set FORMAT_NUMBER_COMMA_SEPARATED1
$row--;
$objPHPExcel->getActiveSheet()
    ->getStyle("G3:G{$row}")
    ->getNumberFormat()
    ->setFormatCode(
        PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1
);
$objPHPExcel->getActiveSheet()
    ->getStyle("M3:M{$row}")
    ->getNumberFormat()
    ->setFormatCode(
        PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1
);

if ( $chk_status == 'N' ) {
	$row = $row+2;
	$objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $txt_status_n);
}//end if
    
/*
$objPHPExcel->getActiveSheet()->getStyle("G3:G{$row}")
    ->getNumberFormat()->applyFromArray( 
        array( 
            'code' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1  
        )
    );
$objPHPExcel->getActiveSheet()->getStyle("L3:L{$row}")
    ->getNumberFormat()->applyFromArray( 
        array( 
            'code' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1  
        )
    );    
*/
// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="report-payment-compare-bank-export.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;


die();
?>



