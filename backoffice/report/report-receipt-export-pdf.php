<?php
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/datatype.php";
include_once "../share/course.php";
global $db;

require_once "./tcpdf-new/tcpdf.php";

// $symbo_unc = "&#9744;";
$symbo_unc = "[ ]";
// $symbo_c = "&#9745;";
$symbo_c = '[√]';

// d($_GET);

$date_start = $_GET["date_start"];
$date_stop = $_GET["date_stop"];
$rc_status = $_GET["rc_status"];
$sec_id = $_GET["sec_id"];
$cos_id = $_GET["cos_id"];
$pay_by = $_GET["pay_by"];
$ids = trim($_GET["ids"], ",");


$rc_status_txt = ($rc_status=='success') ? 'ใบเสร็จ (ใช้งาน)' : 'ใบเสร็จ (ที่ถูกคืนเงิน)';

$date_start_en = thai_to_timestamp($date_start);
$date_stop_en = thai_to_timestamp($date_stop);

$con_sec_id = !empty($sec_id) ? " AND a.section_id={$sec_id}" : "";
$con_cos_id = !empty($cos_id) ? " AND a.coursetype_id={$cos_id}" : "";
if($pay_by=="at_ati"){	
	$con_pay_by = !empty($pay_by) ? " AND (a.pay='at_ati' OR a.pay='walkin' OR a.pay='importfile' AND docno<>'')" : "";
}else{
	$con_pay_by = !empty($pay_by) ? " AND a.pay='{$pay_by}'" : "";
}
$con_rc_status = $rc_status=='cancel' ? " AND a.pay_status=8" : " AND a.pay_status=3";

$con_ids = !empty($ids) ? " AND a.register_id IN ({$ids})" : "";

$cond = " AND a.active='T' 
	AND a.auto_del='F' 
	AND a.pay_date BETWEEN '{$date_start_en} 00:00:00' AND '{$date_stop_en} 23:59:59'
	{$con_rc_status}
	{$con_sec_id}
	{$con_cos_id}
	{$con_pay_by}
	{$con_ids}
";

// echo $cond;die();
// $regis_all = get_register($cond, "", true);

$q = "SELECT a.register_id,
			a.`no`,
			a.runyear,
			a.docno,
			a.runno,
			a.member_id,
			a.title,
			a.fname,
			a.lname,
			a.cid,
			a.slip_type,
			a.receipttype_id,
			a.receipttype_text,
			a.receipt_id,
			a.taxno,
			a.slip_name,
			a.slip_address1,
			a.slip_address2,
			a.date,
			a.register_by,
			a.ref1,
			a.ref2,
			a.`status`,
			a.expire_date,
			a.exam_date,
			a.course_id,
			a.course_detail_id,
			a.course_price,
			a.course_discount,
			a.pay,
			a.pay_date,
			a.pay_method,
			a.pay_yr,
			a.pay_mn,
			a.pay_id,
			a.pay_price,
			a.pay_diff,
			a.approve,
			a.approve_by,
			a.approve_date,
			a.register_mod,
			a.last_mod,
			a.last_mod_date,
			a.result,
			a.result_date,
			a.result_by,
			a.receipt_title,
			a.receipt_fname,
			a.receipt_lname,
			a.receipt_no,
			a.receipt_gno,
			a.receipt_moo,
			a.receipt_soi,
			a.receipt_road,
			a.receipt_district_id,
			a.receipt_amphur_id,
			a.receipt_province_id,
			a.receipt_postcode,
			a.active,
			a.recby_id,
			a.rectime,
			a.remark,
			a.coursetype_id,
			a.section_id,
			a.pay_status,
			a.branch, 
			a.runno, 
			a.docno,
			a.runyear, 
			a.auto_del, 
			a.usebook,
			a.te_results,
            b.name AS receipt_district_name,
            c.name AS receipt_amphur_name,
            d.name AS receipt_province_name,
            e.name AS receipt_name
	FROM register a LEFT JOIN district b ON b.district_id=a.receipt_district_id
        LEFT JOIN amphur c ON c.amphur_id=a.receipt_amphur_id
        LEFT JOIN province d ON d.province_id=a.receipt_province_id        
        LEFT JOIN receipt e ON e.receipt_id=a.receipt_id
    WHERE a.active<>'' {$cond}
    ORDER BY a.pay_date, a.docno ASC
";    
mysql_query("SET character_set_results=utf8");
// mysql_query("SET character_set_client=tis620");
// mysql_query("SET character_set_connection=tis620");
$regis_all = $db->get($q);
// echo $q;
// echo count($regis_all);
// d($regis_all);
// die();

//get course type name & section name
$coursetype_name="";
$section_name = "";
if ( !empty($cos_id) || !empty($sec_id) ) {
	if ( !empty($cos_id) ) {
		$sql = "SELECT c.name
				FROM coursetype AS c
				WHERE c.active='T' AND c.coursetype_id={$cos_id}
		";
		$coursetype_name = $db->data($sql);		
		// d($coursetype_name);die();
	}//end if
	if ( !empty($sec_id) ) {
		$sql = "SELECT s.name
				FROM section AS s
				WHERE s.active='T' AND s.section_id={$sec_id}
		";
		$section_name = $db->data($sql);	
		// d($sql);die();	
	}//end if
}//end else
if ( empty($coursetype_name) ) {
	$coursetype_name = "ทุกประเภท";
}//end if
if ( empty($section_name) ) {
	$section_name = "ทุกประเภท";
}//end if

$cash = ($check_pay=='at_ati' || $check_pay=='importfile' || $check_pay=='walkin') ? $symbo_c : $symbo_unc;
$transfer = ($check_pay=='at_asco' || $check_pay=='paysbuy' || $check_pay=='bill_payment') ? $symbo_c : $symbo_unc;

$txt_pay_by = "";
if ( empty($pay_by) ) {
	$txt_pay_by = "ทุกประเภท";
}elseif ( $pay_by=='paysbuy' ) {
	$txt_pay_by = "paysbuy";
}elseif ( $pay_by=='bill_payment' ) {
	$txt_pay_by = "Bill-payment";
}elseif ( $pay_by=='at_asco' ) {
	$txt_pay_by = "เช็ค/เงินโอน";
}elseif ( $pay_by=='at_ati' ) {
	$txt_pay_by = "ชำระเงินสดผ่าน ATI";
}//end elseif

// d($section_name);

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false); 

$html_header_inc = "
	<!DOCTYPE html>
	<html lang=\"en\">
	<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
	<!-- <title>ใบเสร็จรับเงิน</title> -->
	<link href=\"../css/printform-receipt-style.css\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
	<style>
		@media print{
			body{
				padding: 0px;
				margin: 0px;
			}
			#btPrint{
				display: none;
			}
		}
		.page-header {
			text-align: center;
			font-size: 19px;
			/*height: 950px;*/
			height: 900px;
			/* width: 540px; */
			display: block;
			position: relative;
		}
	</style>
	<body>
";
$html_end_inc = "
	</body>
	</html>
";

$html_front_page = "
	{$html_header_inc}
	<table border=\"1\">
		<tbody>
			<tr>
				<td width=\"100%\">
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>

					<div><img src=\"../images/logo_invoice.png\" alt=\"Mountain View\" style=\"width:200px;height:80px;\"></div>
					<div><span style=\"text-align: center; font-weight: bold; font-size: 30px;\">ข้อมูลใบเสร็จ</span></div>		
					<div><span style=\"text-align: center; font-weight: bold; font-size: 22px;\">วันที่ {$date_start} ถึง {$date_stop}</span></div>
					<div><span style=\"text-align: center; font-weight: bold; font-size: 22px;\">ประเภทใบอนุญาต/คุณวุฒิ : {$section_name}</span></div>
					<div><span style=\"text-align: center; font-weight: bold; font-size: 22px;\">ประเภทหลักสูตร : {$coursetype_name}</span></div>
					<div><span style=\"text-align: center; font-weight: bold; font-size: 22px;\">ช่องทางการชำระ : {$txt_pay_by}</span></div>
					<div><span style=\"text-align: center; font-weight: bold; font-size: 22px;\">สถานะใบเสร็จ : {$rc_status_txt}</span></div>

					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>

				</td>

			</tr>
		</tbody>
	</table>
	{$html_end_inc}
";

//front page
$pdf->AddPage();
$pdf->SetAutoPageBreak(false, 0);
$pdf->setCellPaddings(0,0,0,0);
$pdf->SetFont('thsarabun', '', 14);		
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->writeHTML($html_front_page, true, false, true, false, 'C');
$pdf->lastPage();


foreach ($regis_all as $key => $v) {	

	$sum_course_price = 0;
	$sum_course_discount = 0;
	$ttl_course_price_txt = "";
	$ttl_course_price = 0;
	$vat = 0;
	$ttl_course_price_after_discount = 0;

	$runno = 1;
	$pay_date = explode(" ", $v["pay_date"]);
	$pay_date = $pay_date[0];
	$pay_date = revert_date($pay_date); 

	// set margins
	$pdf->SetAutoPageBreak(false, 0);
	$pdf->setCellPaddings(0,0,0,0);
	//set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	$pdf->SetFont('thsarabun', '', 14);		
	// add a page
	$pdf->setPrintHeader(false);
	$pdf->setPrintFooter(false);

	// add a page
	$pdf->AddPage();

	//get receipt info
	$txt_receipt_name = "";
	$name_full = $v["title"].$v["fname"]." ".$v["lname"];
	$receipt_full = $v["receipt_title"].$v["receipt_fname"]." ".$v["receipt_lname"];
	$txt_cid = "เลขประจำตัวประชาชน ".$v["cid"];
	$receipt_id = $v["receipt_id"];
	if($v["slip_type"]=='corparation'){
		if($v["receipttype_id"]==5){
			//$receipttype_name = "";
			$name = $v["receipttype_text"]."<br>เลขที่ผู้เสียภาษี ".$v["taxno"];
		}else{
			$q = "SELECT name, taxno, branch FROM receipt WHERE receipt_id={$receipt_id}";
			$rc = $db->get($q);
			$rc = $rc[0];
			$name = $rc["name"]."<br>เลขที่ผู้เสียภาษี ".$rc["taxno"]."&nbsp;&nbsp;&nbsp; สาขา ".$rc["branch"];		
		}//end else
		$txt_receipt_name = $name."&nbsp;&nbsp;&nbsp;";	
	}else{
		//echo $receipt_full;
		$txt_receipt_name = $receipt_full."&nbsp;&nbsp;&nbsp;".$txt_cid;
	}//end else

	$txt_receipt_address = $v["receipt_no"];
	if($v["receipt_gno"]!="") 
		$txt_receipt_address .= " ".$v["receipt_gno"];
	if($v["receipt_moo"]!="") 
		$txt_receipt_address .= " ".$v["receipt_moo"];
	if($v["receipt_soi"]!="") 
		$txt_receipt_address .= " ".$v["receipt_soi"];
	if($v["receipt_road"]!="") 
		$txt_receipt_address .= " ".$v["receipt_road"];
	$txt_receipt_address .= "<br>";
    $txt_receipt_address .= $v["receipt_district_name"];
    $txt_receipt_address .= $v["receipt_amphur_name"];
    $txt_receipt_address .= $v["receipt_province_name"];
    $txt_receipt_address .= $v["receipt_postcode"];


	$txt_header = "แผ่นที่ 1 (สำเนา)";
	$txt_receipt = "ใบเสร็จรับเงิน/ใบกำกับภาษี\nRECEIPT/TAX INVOICE";
	$txt_addres = "อาคารชุดเลครัชดาออฟฟิศคอมเพล็กซ์2 ชั้น 5 เลขที่ 195/6 ถนนรัชดาภิเษก แขวงคลองเตย\nเขตคลองเตย กรุงเทพมหานคร 10110 โทร 02-264-0909 โทรสาร 02-661-8505-6\nเลขประจำตัวผู้เสียภาษีอากร 0993000132416 สาขาที่ สำนักงานใหญ่";
	// $txt_inv_no = "เลขที่               {$v["docno"]}\nเลขที่โครงการ\nวันที่                {$pay_date}";

	// Logo
	$image_file = K_PATH_IMAGES.'logo_invoice.png';
	$pdf->Image($image_file, 8, 10, 40, '', 'png', '', 'T', false, 300, '', false, false, 0, false, false, false);

	$course_id = $v["course_id"];
	$course_detail_id = trim($v["course_detail_id"], ",");
	$arr_course_detail_ids = explode(",", $course_detail_id);
	$coursetype_id = $v["coursetype_id"];
	$q = "SELECT name FROM coursetype WHERE coursetype_id={$coursetype_id}";
	$coursetype_name = $db->data($q);

	$txt_list_course = "";
	$list_course = "";

	if ( count($arr_course_detail_ids) > 1 ) { // case angry bird
		$arr_grp_cos = array();
		foreach ($arr_course_detail_ids as $key => $c) {
			$tmp = explode(":", $c);
			$arr_grp_cos[$tmp[0]][] = $tmp[1];
		}

		foreach ($arr_grp_cos as $key => $id) {

			$cos_dt_id = $id[0];
			$course_id = $key;

			$info_cos = get_course(" and a.course_id={$course_id}");
			$info_cos = $info_cos[0];

			$info_cos_dt = get_course_detail(" and a.course_detail_id={$cos_dt_id}");
			$info_cos_dt = $info_cos_dt[0];

			$txt_date_th = revert_date($info_cos_dt["date"]);
			$txt_time = $info_cos_dt["time"];

			//
			$pri_per_cos = set_comma($info_cos["price"]);
			//

			$txt_list_course = "{$runno}. ค่าสมัคร{$coursetype_name}หลักสูตร[{$info_cos["code"]}] {$info_cos["title"]}<br>
				ผู้สมัคร{$coursetype_name} : {$name_full}<br>
				สอบวันที่ : {$info_cos_dt["day"]} {$txt_date_th} เวลา : {$txt_time} รหัสโครงการ : {$info_cos_dt["code_project"]}<br>
				{$info_cos_dt["address"]} {$info_cos_dt["address_detail"]}
			";
			$list_course .= "
				<tr>
					<td width=\"70%\" colspan=\"2\"><span style=\"text-align: left;\">{$txt_list_course}</span></td>
					<td width=\"10%\"><span style=\"text-align: center;\">1</span></td>
					<td width=\"20%\"><span style=\"text-align: right;\">{$pri_per_cos}</span></td>
				</tr>
			";
			$runno++;	
			$sum_course_price = $info_cos['price']+$sum_course_price;	
			$sum_course_discount = $info_cos["discount"]+$sum_course_discount;
		}//end loop

		// $sum_course_discount = $v['course_price']-$sum_course_price;
		$sum_course_discount = $v['course_discount'];
		$register_course_price = $v['course_price']-$v['course_discount'];
		$vat = $register_course_price-($register_course_price/1.07);
		$ttl_course_price_after_discount = ($register_course_price-$vat);
		$ttl_course_price = $register_course_price;
		
		$ttl_course_price_txt = number_letter($v["course_price"]);
	}else{
		$course_detail_id = explode(":", $arr_course_detail_ids[0]);
		$course_detail_id = (count($course_detail_id)>1) ? $course_detail_id[1] : $course_detail_id[0];
		$info_cos = get_course(" and a.course_id={$course_id}");
		$info_cos = $info_cos[0];
		$info_cos_dt = get_course_detail(" and a.course_detail_id={$course_detail_id}");
		$info_cos_dt = $info_cos_dt[0];
		$txt_date_th = revert_date($info_cos_dt["date"]);
		$txt_time = $info_cos_dt["time"];
		$pri_per_cos = set_comma($v["course_price"]);
		$ttl_course_price = $v['course_price'] - $v["course_discount"];	
		$sum_course_discount = $v["course_discount"];
		$vat = $ttl_course_price-($ttl_course_price/1.07);
		$ttl_course_price_after_discount = ($ttl_course_price-$vat);
		// $vat = $vat;
		// $sum_course_price = $sum_course_price;
		$ttl_course_price_txt = number_letter($v["course_price"]);

/*
		if ( $v["docno"]=="AT61/001452" ) {
			echo $v["pay_price"]."<hr>";
			echo $ttl_course_price_txt."<hr>";
			die();
		}
*/

		$sum_course_price = $info_cos["price"];

		$txt_list_course = "{$runno}. ค่าสมัคร{$coursetype_name}หลักสูตร[{$info_cos["code"]}] {$info_cos["title"]}<br>
		     ผู้สมัคร{$coursetype_name} : {$name_full}<br>
		     สอบวันที่ : {$info_cos_dt["day"]} {$txt_date_th} เวลา : {$txt_time} รหัสโครงการ : {$info_cos_dt["code_project"]}<br>
		     {$info_cos_dt["address"]} {$info_cos_dt["address_detail"]}
		";
		$list_course = "
			<tr>
				<td width=\"70%\" colspan=\"2\"><span style=\"text-align: left;\">{$txt_list_course}</span></td>
				<td width=\"10%\"><span style=\"text-align: center;\">1</span></td>
				<td width=\"20%\"><span style=\"text-align: right;\">{$pri_per_cos}</span></td>
			</tr>
		";
	}//end else

	$sum_course_discount = abs($sum_course_price-$ttl_course_price);

	$sum_course_price = set_comma($sum_course_price);
	$sum_course_discount = set_comma($sum_course_discount);
	$vat = set_comma($vat);
	$ttl_course_price_after_discount = set_comma($ttl_course_price_after_discount);
	$ttl_course_price = set_comma($ttl_course_price);

	$html_receipt_header = "
		{$html_header_inc}
		<table class=\"no-border\">
			<tbody>
				<tr>
					<td width=\"13%\">
						<div><span style=\"text-align: left;\">ได้รับเงินจาก</span></div>					
						<div><span style=\"text-align: left;\">ที่อยู่</span></div>
					</td>
					<td width=\"57%\">					
						<span style=\"text-align: left;\">{$txt_receipt_name}</span>
						<br>
						<span style=\"text-align: left;\" >{$txt_receipt_address}</span>				
					</td>
					<td width=\"30%\">
						<div>&nbsp;</div>
						<div style=\"border:1pt solid black;\">
							<span class=\"square-line-center\">&nbsp;อัตราภาษี [/] อัตราร้อยละ 7 [ ] อัตราศูนย์</span></div>
					</td>
				</tr>
			</tbody>
		</table>
		
	";


	$html_receipt_body = "
		{$html_header_inc}

		<table class=\"td-center\">
			<tbody>
				<tr>
					<th width=\"70%\" colspan=\"2\" style=\"border:1pt solid black; text-align: center;\"><span>รายการ</span></th>
					<th width=\"10%\" style=\"border:1pt solid black; text-align: center;\"><span>จำนวน</span></th>
					<th width=\"20%\" style=\"border:1pt solid black; text-align: center;\"><span>จำนวนเงิน</span></th>
				</tr>

				{$list_course}

				<tr>
					<td width=\"54%\" class=\"td-border-fix\" rowspan=\"5\" colspan=\"1\">
						<span style=\"text-align: center;\">&nbsp;</span>
						<span style=\"text-align: center;\"><br>จำนวนเงินทั้งสิ้น (ตัวอักษร)</span><br>
						<span style=\"text-align: center;\">{$ttl_course_price_txt}</span>
					</td>

					<td width=\"16%\" class=\"td-border-fix\"><span style=\"text-align: right;\">รวมราคา</span></td>
					<td width=\"10%\" class=\"td-border-fix\"><span style=\"text-align: right;\">&nbsp;</span></td>
					<td width=\"20%\" class=\"td-border-fix\">
						<span style=\"text-align: right;\">{$sum_course_price}</span>
					</td>
				</tr>

				<tr>
					<td class=\"td-border-fix\"><span style=\"text-align: right;\">ส่วนลด</span></td>
					<td class=\"td-border-fix\"><span style=\"text-align: center;\">&nbsp;</span></td>
					<td class=\"td-border-fix\">
						<span style=\"text-align: right;\">{$sum_course_discount}</span>
					</td>
				</tr>

				<tr>
					<td class=\"td-border-fix\"><span style=\"text-align: right;\">ราคาหลังหักส่วนลด&nbsp;</span></td>
					<td class=\"td-border-fix\"><span style=\"text-align: center;\">&nbsp;</span></td>
					<td class=\"td-border-fix\">
						<span style=\"text-align: right;\">{$ttl_course_price_after_discount}</span>
					</td>
				</tr>

				<tr>
					<td class=\"td-border-fix\"><span style=\"text-align: right;\">จำนวนภาษีมูลค่าเพิ่ม&nbsp;</span></td>
					<td class=\"td-border-fix\"><span style=\"text-align: center;\">&nbsp;</span></td>
					<td class=\"td-border-fix\">
						<span style=\"text-align: right;\">{$vat}</span>
					</td>
				</tr>
				<tr>
					<td class=\"td-border-fix\"><span style=\"text-align: right;\">จำนวนเงินรวมทั้งสิ้น&nbsp;</span></td>
					<td class=\"td-border-fix\"><span style=\"text-align: center;\">&nbsp;</span></td>
					<td class=\"td-border-fix\">
						<span style=\"text-align: right;\">{$ttl_course_price}</span>
					</td>
				</tr>
				

			</tbody>
		</table>
		
	";

	$check_pay = trim($v["pay"]);
	$cash = ($check_pay=='at_ati' || $check_pay=='importfile' || $check_pay=='walkin') ? $symbo_c : $symbo_unc;
	$transfer = ($check_pay=='at_asco' || $check_pay=='paysbuy' || $check_pay=='bill_payment') ? $symbo_c : $symbo_unc;
	
	$html_receipt_footer = "
		<table class=\"no-border\">
			<tbody>
				<tr>
					<td width=\"5%\"></td>
					<td><span style=\"text-align: left;\">{$cash} เงินสด</span></td>
					<td width=\"20%\"><span class=\"right\">&nbsp;</span></td>
					<td width=\"30%\"><span class=\"center\">&nbsp;</span></td>
					<td width=\"8%\"><span class=\"right\">&nbsp;</span></td>
					<td width=\"20%\"></td>
				</tr>
				<tr>
					<td></td>
					<td><span style=\"text-align: left;\">{$transfer} เงินโอน</span></td>
					<td><span class=\"right\">&nbsp;</span></td>
					<td><span class=\"center\">&nbsp;</span></td>
					<td><span class=\"right\">&nbsp;</span></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td><span style=\"text-align: left;\">{$symbo_unc} เช็ค</span></td>
					<td colspan=\"2\"><span style=\"text-align: left;\">เลขที่เช็ค ..........................................................................................&nbsp;</span></td>
					
					<td colspan=\"2\"><span style=\"text-align: left;\">ลงวันที่ ...............................&nbsp;</span></td>
				</tr>
				<tr>
					<td></td>
					<td><span style=\"text-align: left;\">{$symbo_unc} สาขา</span></td>
					<td><span class=\"right\">&nbsp;</span></td>
					<td><span class=\"center\">&nbsp;</span></td>
					<td><span class=\"right\">&nbsp;</span></td>
					<td></td>
				</tr>
			</tbody>
		</table>

		<table class=\"no-border\">
			<tbody>
				<tr>
					<td width=\"5%\"></td>
					<td width=\"6%\"><span style=\"text-align: left;\">ผู้รับเงิน</span></td>
					<td><span>.........................................................&nbsp;&nbsp;</span></td>
					<td width=\"58%\"><span class=\"right\">(การชำระโดยเช็ค จะสมบูรณ์เมื่อได้เรียกเก็บเงินจากธนาคารเรียบร้อยแล้ว)</span></td>
				</tr>
			</tbody>
		</table>

		{$html_end_inc}
	";

	// Title
	$pdf->MultiCell(150, 8, '', 0, 'L', 0, 0, 'M', 'M', true);
	$pdf->MultiCell(50, 8, $txt_header, 0, 'C', 0, 1, '', '', true);
	$pdf->MultiCell(140, 8, '', 0, 'C', 0, 0, '', '', true);
	$pdf->MultiCell(50, 8, $txt_receipt, 1, 'C', 0, 1, '', '', true);

	$pdf->MultiCell(150, 8, '', 0, 'L', 0, 1, '', '', true);
	$pdf->MultiCell(120, 10, $txt_addres, 0, 'L', 0, 0, '', '', true);

	$txt_inv_no = "เลขที่               {$v["docno"]}\nเลขที่โครงการ     {$info_cos_dt["code_project"]}\nวันที่                {$pay_date}";

	$pdf->MultiCell(150, 8, '', 0, '\n', 0, 1, '', '', true);
	$pdf->MultiCell(150, 8, '', 0, '\n', 0, 1, '', '', true);
	$pdf->MultiCell(150, 8, '', 0, '', 0, 0, '', '', true);
	$pdf->MultiCell(50, 8, $txt_inv_no, 0, 'L', 0, 1, '', '', true);

	/*
	$txt = <<<EOD
	TCPDF Example 002

	Default page header and footer are disabled using setPrintHeader() and setPrintFooter() methods.
	EOD;
	$pdf->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);
	*/

	// output the HTML content
	// $pdf->writeHTML($html);
	$pdf->writeHTML($html_receipt_header, true, false, true, false, 'L');

	$pdf->writeHTML($html_receipt_body, true, false, true, false, 'C');

	$pdf->writeHTML($html_receipt_footer, true, false, true, false, 'C');


	// reset pointer to the last page
	$pdf->lastPage();
}// end loop
// die();

// reset pointer to the last page
// $pdf->lastPage();

// $pdf->Output();
$pdf->Output('', 'I');
 

// die();
?>
ดาวโหลดรายงานในรูปแบบ PDF <a href="./export-pdf/report-receipt.pdf">คลิกที่นี้</a>
