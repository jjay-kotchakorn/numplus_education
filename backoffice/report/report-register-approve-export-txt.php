<?php
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/course.php";
include_once "../share/datatype.php";
require_once "./Classes/PHPExcel.php";
global $db, $EMPID;

$date_now = date('Y-m-d H:i:s');
$date = date('Y-m-d');

// d($_POST);
/*
	[coursetype_id] => 2
    [section_id] => 1
    [sort_by] => course
    [course_detail_id] => 2098
    [file_type] => txt
*/

// d($_POST);die();
function write_file($file_name="", $msg="", $write_mode="a", $file_type="ati"){
	$str = $msg;	
	$date_now = date('Y-m-d H:i:s');
	$date_log = date('Y-m-d');
	$log_name = "../file-export/".$file_name.".".$file_type;
	$file = fopen($log_name, $write_mode);
	// $str_txt = $date_now."\r\n".$str."\r\n";
	$str_txt = $str."\r\n";
	fwrite($file, $str_txt);
	fclose($file);
}//end func    

if ( !empty($_GET["type"]) ) {
	if ( $_GET["type"]=="export" ) {
		$file_name = $_GET["file_name"];
		$fullpath = $_GET["path_file"];
		// ignore_user_abort(true);
		set_time_limit(0); // disable the time limit for this script

		$headers = get_headers($fullpath, 1);// use file absolute path here
		$fsize = $headers['Content-Length'];

		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename='.basename($fullpath));
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header("Content-Length: " . $fsize);
		ob_clean();
		flush();
		readfile($fullpath);

	}
	die();
}//end if

if ( !empty($_POST) ) {
	set_time_limit(600);
	$course_detail_id = $_POST["course_detail_id"];
	$cos_dt_info = get_course_detail("", $course_detail_id);
	$cos_dt_info = $cos_dt_info[0];
	// d($cos_dt_info);
	// die();

	if( !empty($cos_dt_info) ){
		$file_name = "";
		$file_name = "course_detail_id_".$cos_dt_info["course_detail_id"]."_".$cos_dt_info["date"];
		unlink("../file-export/".$file_name.".ati");
		$str_txt = "";
		$str_txt .= "H|{$date_now}</tab>".$cos_dt_info["course_detail_id"];
		$str_txt .= "</tab>".$cos_dt_info["date"]."</tab>".$cos_dt_info["time"];
		$str_txt .= "</tab>".$cos_dt_info["open_regis"]."</tab>".$cos_dt_info["end_regis"];
		$str_txt .= "</tab>".$cos_dt_info["address"]."</tab>".$cos_dt_info["address_detail"];
		$str_txt .= "</tab>".$cos_dt_info["chair_all"]."</tab>".$cos_dt_info["sit_all"];
		$str_txt .= "</tab>".$cos_dt_info["inhouse"]."</br>";

		write_file($file_name, $str_txt);

		
		// $register_id = $v["register_id"];
		$WHERE = "WHERE 1 ";
		$cond = " AND ( a.register_course_detail_course_detail_id={$course_detail_id} OR a.course_detail_id={$course_detail_id}) 
			AND b.pay_status IN (3,5,6)
		";
		
		$q = "SELECT
				a.view_register_course_detail_id,
				a.register_course_detail_course_detail_id,
				a.course_detail_id,
				a.code_project,
				a.code_project_single,
				a.register_course_detail_course_id,
				a.course_id,
				b.register_id,
				b.title,
				b.fname,
				b.lname,
				b.date,
				b.expire_date,
				b.pay_date,
				b.pay_status,
				b.cid,
				b.member_id,
				c.name AS pay_status_name

			FROM view_register_course_detail AS a
			LEFT JOIN register AS b ON b.register_id=a.view_register_course_detail_id
			LEFT JOIN pay_status AS c ON c.pay_status_id=b.pay_status
			$WHERE
			$cond
		";
		// echo $q; die();
		$rs = $db->get($q);
		$ttl_register = 0;
		// echo json_encode($rs);
		foreach ($rs as $key => $v) {
			$ttl_register++;
			$str_txt = "";
			$str_txt .= "D|";		
			$course_id = ( $v["course_detail_id"]==$course_detail_id ) ? $v["course_id"] : $v["register_course_detail_course_id"];
			$q = "SELECT title FROM course WHERE course_id=$course_id";
			// echo $q; die();
			$course_title = $db->data($q);
			// echo $course_title;

			$str_txt .= $register_id."</tab>".$v["no"];
			$str_txt .= "</tab>".$v["title"]."</tab>".$v["fname"]."</tab>".$v["lname"];
			$str_txt .= "</tab>".$course_title."</tab>".$v["date"]."</tab>".$v["expire_date"];
			$str_txt .= "</tab>".$v["pay_date"]."</tab>".$v["pay_status"]."</tab>".$v["pay_status_name"];
			$str_txt .= "</tab>".$v["cid"]."</tab>".$v["member_id"]."</br>";
			
			write_file($file_name, $str_txt);
			// die();

		}//end loop $v

		$str_txt = "";
		$str_txt .= "F|";
		$str_txt .= "ttl_register={$ttl_register}";

		write_file($file_name, $str_txt);

		$return_msg = array();
		$return_msg["rand_no"] = rand(1000,9999);
		$return_msg["status"] = '1';
		$return_msg["msg"] = 'สร้างไฟล์สำเร็จ';
		$return_msg["file_name"] = "{$file_name}.ati";
		$return_msg["link_file"] = "../file-export/{$file_name}.ati";
		$file_size = filesize($return_msg["link_file"]);
		$file_size = $file_size*0.001;
		$return_msg["file_size"] = $file_size." KB";
		$return_msg["ttl_register"] = "จำนวนผู้เข้าร่วมกิจกรรม: ".$ttl_register." คน";

	}else{
		// $return_msg = array('status'=>'0', 'msg' => 'ไม่พบสถานที่จัดกิจกรรม กรุราติดต่อผู้ดูแลระบบ');
		$return_msg = array();
		$return_msg["rand_no"] = rand(1000,9999);
		$return_msg["status"] = '0';
		$return_msg["msg"] = 'ไม่พบสถานที่จัดกิจกรรม กรุณาติดต่อผู้ดูแลระบบ';

	}//end else

	
	echo json_encode($return_msg);
	die();

}//end if

?>