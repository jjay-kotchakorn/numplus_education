<?php
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/course.php";
include_once "../share/datatype.php";
global $db, $EMPID;

/** PHPExcel */
require_once "./Classes/PHPExcel.php";

$date_now = date('Y-m-d H:i:s');
$date = date('Y-m-d');
// var_dump($_REQUEST);
$pay_date = $_GET["pay_date_chk"];

// $pay_date_chk = thai_to_timestamp($pay_date);
$pay_type_chk = $_REQUEST["pay_type_chk"];
$pay_type_txt = ($pay_type_chk=='money') ? 'เงินสด' : '';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Nattapon Booncharoen")
							 ->setLastModifiedBy("Nattapon Booncharoen")
							 ->setTitle("report-payment-compare-money-export")
							 ->setSubject("report-payment-compare-money-export")
							 ->setDescription("-")
							 ->setKeywords("report-payment-compare-money-export")
							 ->setCategory("export to excel file");	

// Add some data column
$objPHPExcel->getActiveSheet()
            ->setCellValue('A1', 'ลำดับ')
            ->setCellValue('B1', 'เลขที่ใบเสร็จ')
            ->setCellValue('C1', 'วันที่ชำระ')
			->setCellValue('D1', 'ชื่อผู้สมัคร')
			->setCellValue('E1', 'หลักสูตร')
            ->setCellValue('F1', 'Project code')
            ->setCellValue('G1', 'ราคา');

// set width column
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(50);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);

// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle('sheet1');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->getActiveSheet();

// die();				 

// d($_REQUEST);
$sql = "SELECT r.register_id
			, r.docno
			, r.course_id
			, r.pay_price
			, CONCAT(r.title, '',r.fname, ' ',r.lname) AS full_name
			, r.pay_date
			, b.register_course_detail_course_detail_id
			, b.course_detail_id
		FROM register AS r inner join view_register_course_detail b on r.register_id=b.view_register_course_detail_id
		WHERE r.active='T' AND r.auto_del='F' AND r.pay_status IN (3)  
			AND (r.pay='walkin' or r.pay='importfile' or r.pay='at_ati')
			AND r.pay_date LIKE '{$pay_date}%'
		ORDER BY r.pay_date DESC";	


$rs = $db->get($sql);	

$runno=0;
$ttl_course_price=0;
$ttl_customer=0;
$row = 1;

foreach ($rs as $key => $v) {
	$runno++;
	$row++;
	$course_ids = trim($v["course_id"]);
	$course_ids = explode(",", $course_ids);
	$pay_date = explode(" ", $v["pay_date"]);
	$pay_date = $pay_date[0];
	$course_name="";
	$project_code="";
	$ttl_course_price = $ttl_course_price+$v["pay_price"];
	if ( count($course_ids)>1 ) {
		foreach ($course_ids as $key => $c) {
			$info_cos = get_course("", $course_ids[$key]);
			$info_cos = $info_cos[0];				
			$course_name .= $info_cos["title"]."<br><hr>";
		}// end loop $c
	}else{
		$info_cos = get_course("", $course_ids[0]);
		$info_cos = $info_cos[0];
		$course_name .= $info_cos["title"]."<br><hr>";
	}

	$course_name = trim($course_name, "<hr>");
	$t = $v["register_course_detail_course_detail_id"];
	$course_detail_id = ($t!="") ? $t : $v["course_detail_id"];

	$project_code = $db->data("select code_project from course_detail where course_detail_id={$course_detail_id}");

	$objPHPExcel->getActiveSheet()->setCellValue('A' . $row, $runno);
	$objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $v["docno"]);
	$objPHPExcel->getActiveSheet()->setCellValue('C' . $row, $pay_date);
	$objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $v["full_name"]);
	$objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $course_name);
	$objPHPExcel->getActiveSheet()->setCellValue('F' . $row, $project_code);
	$objPHPExcel->getActiveSheet()->setCellValue('G' . $row, $v["pay_price"]);

	// echo $course_name."<br>";
	// die();

}//end loop
$ttl_customer = $runno;	

$row++;
$objPHPExcel->getActiveSheet()->setCellValue('B' . $row, 'รวมจำนวนเงินทั้งหมด');
$objPHPExcel->getActiveSheet()->setCellValue('G' . $row, $ttl_course_price);
$row++;
$objPHPExcel->getActiveSheet()->setCellValue('B' . $row, 'จำนวนผู้ชำระเงิน');
$objPHPExcel->getActiveSheet()->setCellValue('G' . $row, $ttl_customer);

// set FORMAT_NUMBER_COMMA_SEPARATED1
$row--;
$objPHPExcel->getActiveSheet()->getStyle("G2:G{$row}")
    ->getNumberFormat()->applyFromArray( 
        array( 
            'code' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1  
        )
    );

// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="report-payment-compare-money-export.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;


die();
?>



