<?php
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/course.php";
include_once "../share/datatype.php";
global $db, $EMPID;

/** PHPExcel */
require_once "./Classes/PHPExcel.php";

$date_now = date('Y-m-d H:i:s');
$date = date('Y-m-d');
// var_dump($_GET);
$pay_date = $_GET["pay_date_chk"];
// $pay_date_chk = thai_to_timestamp($pay_date);
$pay_type_chk = $_GET["pay_type_chk"];
$pay_type_txt = ($pay_type_chk=='paysbuy') ? 'paysbuy' : '';

// echo $pay_date;

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Nattapon Booncharoen")
							 ->setLastModifiedBy("Nattapon Booncharoen")
							 ->setTitle("report-payment-compare-bank-export")
							 ->setSubject("report-payment-compare-bank-export")
							 ->setDescription("-")
							 ->setKeywords("report-payment-compare-bank-export")
							 ->setCategory("export to excel file");	

// Add some data column						 
$objPHPExcel->getActiveSheet()
			->setCellValue('A1', 'ข้อมูลใบเสร็จรับเงินจาก WR')
            ->setCellValue('H1', 'ข้อมูลการชำระจาก paysbuy')   

            ->setCellValue('A2', 'ลำดับ')
            ->setCellValue('B2', 'เลขที่ใบเสร็จ')
            ->setCellValue('C2', 'วันที่ชำระ')
			->setCellValue('D2', 'ชื่อผู้สมัคร')
			->setCellValue('E2', 'หลักสูตร')
            ->setCellValue('F2', 'Project code')
            ->setCellValue('G2', 'ราคา')

            ->setCellValue('H2', 'วันที่ชำระ')
            ->setCellValue('I2', 'สถานะการชำระ')
            ->setCellValue('J2', 'Send/Receive')
            ->setCellValue('K2', 'ยอดชำระ')
            ->setCellValue('L2', 'Invoice No.')
            ->setCellValue('M2', 'Subject')
            ->setCellValue('N2', 'Buyer Card Name')
            ->setCellValue('O2', 'ผลการตรวจ');

// set width column
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(50);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);

$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(50);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(15);

// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle('sheet1');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->getActiveSheet();
				 

$sql = "SELECT r.register_id
			, r.course_id
			, r.docno
			, r.pay_date
			, CONCAT(r.title,'',r.fname,' ',r.lname) AS full_name
			, r.course_price
			, r.course_discount
			, r.course_detail_id
			, p.payment_compare_paysbuy_list_id
			, p.payment_compare_file_list_id
			, CONCAT(p.payment_date, ' ', p.payment_time) AS payment_date
			, p.payment_status
			, p.send_receive
			, p.amount
			, p.fee
			, p.invoice_no
			, p.subject
			, p.buyer_card_name
		FROM register AS r
		LEFT JOIN payment_compare_paysbuy_list AS p ON p.invoice_no=r.register_id AND p.active='T'
		WHERE r.active='T' AND r.pay_status=3 
			AND r.pay='paysbuy'
			AND ( r.pay_date LIKE '{$pay_date}%' ) 
			
		GROUP BY r.register_id
		ORDER BY p.payment_time DESC
";
$rs = $db->get($sql);	


// d($rs);
// die();

$runno=0;	
$ttl_amount=0;
$ttl_course_price=0;
$ttl_cus_from_wr=0;
$ttl_cus_from_bank=0;
$row = 2;

foreach ($rs as $key => $v) {
	$runno++;
	$row++;
	if( !empty($v["register_id"]) ) $ttl_cus_from_wr++;
	if( !empty($v["payment_compare_paysbuy_list_id"]) ) $ttl_cus_from_bank++;
	$amount = $v["amount"];
	$course_ids = trim($v["course_id"]);
	$course_ids = explode(",", $course_ids);
	$pay_date = explode(" ", $v["pay_date"]);
	$pay_date = $pay_date[0];
	$course_name="";
	$project_code="";
	$course_price = $v["course_price"]-$v["course_discount"];
	$ttl_course_price = $ttl_course_price+$course_price;
	$ttl_amount = $ttl_amount+$amount;
	$status_chk = ($amount-$course_price == 0) ? $status_chk='Y' : $status_chk='N';

	// get course name, project code				
	$sql = "SELECT a.course_id
				, a.course_detail_id
				, b.code_project
			FROM register_course_detail a left join  course_detail b on a.course_detail_id=b.course_detail_id
			WHERE a.active='T' AND a.register_id={$v["register_id"]}	
	";

	$rs = $db->get($sql);
	$course_name = "";
	$project_code = "";		
	if($rs){					
		foreach ($rs as $key => $c) {
			$info_cos = get_course("", $c["course_id"]);
			$info_cos = $info_cos[0];
			if ( !empty($info_cos) ) {
				$course_name .= $info_cos["title"].",";
				if ( !empty($c["code_project"]) ) {
					$project_code .= $c["code_project"];
				}else{
					$project_code .= "-";
				}
				$project_code .= ",";
			}// end if
		}// end loop $c
	}else{
		$course_id = $v["course_id"];
		$info_cos = get_course("", $v["course_id"]);
		$info_cos = $info_cos[0];
		if ( !empty($info_cos) ) {
			$course_name .= $info_cos["title"];
		}// end if					
	}
	if(strpos(':', $v["course_detail_id"])==0 && !$project_code){
		$q = "select code_project from course_detail where course_detail_id={$v["course_detail_id"]}";
		$project_code  = $db->data($q);
	}
	$course_name = trim($course_name, ",");
	$project_code = trim($project_code, ",");

	$objPHPExcel->getActiveSheet()->setCellValue('A' . $row, $runno);
	$objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $v["docno"]);
	$objPHPExcel->getActiveSheet()->setCellValue('C' . $row, $pay_date);
	$objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $v["full_name"]);
	$objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $course_name);
	$objPHPExcel->getActiveSheet()->setCellValue('F' . $row, $project_code);
	$objPHPExcel->getActiveSheet()->setCellValue('G' . $row, $course_price);
	
	$objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $v["payment_date"]);
	$objPHPExcel->getActiveSheet()->setCellValue('I' . $row, $v["payment_status"]);
	$objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $v["send_receive"]);
	$objPHPExcel->getActiveSheet()->setCellValue('K' . $row, $amount);
	$objPHPExcel->getActiveSheet()->setCellValue('L' . $row, $v["invoice_no"]);
	$objPHPExcel->getActiveSheet()->setCellValue('M' . $row, $v["subject"]);
	$objPHPExcel->getActiveSheet()->setCellValue('N' . $row, $v["buyer_card_name"]);
	$objPHPExcel->getActiveSheet()->setCellValue('O' . $row, $status_chk);

	// echo $course_name."<br>";
	// die();

}//end loop
$ttl_customer = $runno;	

//chk status
if ( ($ttl_amount==$ttl_course_price) && ($ttl_cus_from_wr==$ttl_cus_from_bank) ) {
	$chk_status = 'Y';
}else{
	$chk_status = 'N';
	$txt_status_n = "หมายเหตุ :  ";
	if ( $ttl_amount!=$ttl_course_price ) {
		$txt_status_n .= "จำนวนเงินทั้งหมดจากระบบ WR และจำนวนเงินทั้งหมดที่ชำระผ่าน {$pay_type_txt} ไม่ตรงกัน, ";
	}
	if ( $ttl_cus_from_wr!=$ttl_cus_from_bank ) {
		$txt_status_n .= "จำนวนผู้ชำระเงินจากระบบ WR และจำนวนผู้ชำระเงินจาก {$pay_type_txt} ไม่ตรงกัน, ";
	}
	$txt_status_n = trim($txt_status_n, ", ");
}//end else
/*
$objPHPExcel->getActiveSheet()
    ->getStyle("J3:J{$row}")
    ->getNumberFormat()
    ->setFormatCode(
        PHPExcel_Style_NumberFormat::FORMAT_NUMBER
);
*/

$row++;
$objPHPExcel->getActiveSheet()->setCellValue('B' . $row, 'จำนวนเงินทั้งหมดจากระบบ WR');
$objPHPExcel->getActiveSheet()->setCellValue('G' . $row, $ttl_course_price);
$objPHPExcel->getActiveSheet()->setCellValue('I' . $row, 'จำนวนเงินทั้งหมดที่ชำระผ่าน ธนาคาร');
$objPHPExcel->getActiveSheet()->setCellValue('K' . $row, $ttl_amount);

$row++;
$objPHPExcel->getActiveSheet()->setCellValue('B' . $row, 'จำนวนผู้ชำระเงินจากระบบ WR');
$objPHPExcel->getActiveSheet()->setCellValue('G' . $row, $ttl_cus_from_wr);
$objPHPExcel->getActiveSheet()->setCellValue('I' . $row, 'จำนวนผู้ชำระเงินจาก ธนาคาร');
$objPHPExcel->getActiveSheet()->setCellValue('K' . $row, $ttl_cus_from_bank);
$objPHPExcel->getActiveSheet()->setCellValue('O' . $row, $chk_status);

// set FORMAT_NUMBER_COMMA_SEPARATED1

$row--;
$objPHPExcel->getActiveSheet()
    ->getStyle("G3:G{$row}")
    ->getNumberFormat()
    ->setFormatCode(
        PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1
);
$objPHPExcel->getActiveSheet()
    ->getStyle("K3:K{$row}")
    ->getNumberFormat()
    ->setFormatCode(
        PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1
);

if ( $chk_status == 'N' ) {
	$row = $row+2;
	$objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $txt_status_n);
}//end if

/*
$objPHPExcel->getActiveSheet()->getStyle("G3:G{$row}")
    ->getNumberFormat()->applyFromArray( 
        array( 
            'code' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1  
        )
    );
$objPHPExcel->getActiveSheet()->getStyle("L3:L{$row}")
    ->getNumberFormat()->applyFromArray( 
        array( 
            'code' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1  
        )
    );    
*/
    

// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="report-payment-compare-paysbuy-export.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;


die();
?>



