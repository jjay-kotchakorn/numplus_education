<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
global $db;

$error = $_SESSION["error"]["msg"];
unset($_SESSION["error"]["msg"]);
$success = $_SESSION["success"]["msg"];
unset($_SESSION["success"]["msg"]);

$section = datatype(" and a.active='T'", "section", true);
$coursetype = datatype(" and a.active='T'", "coursetype", true);
$emp_id = $_GET["emp_id"];
$q = "select  prefix, fname, lname from emp where emp_id=$emp_id";
$r = $db->rows($q);
$str = "";
if($r){
	$str = $r["prefix"]." ".$r["fname"]." ".$r["lname"];
}else{
	$str = "เพิ่มเจ้าหน้าที่";
}
?>
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="row">
				  <div class="col-md-12">			      
					<div class="block-flat">
					  <div class="header">
					  	<ol class="breadcrumb">
							<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>');">หน้าหลัก</a></li>
							<li class="active"><?php echo $str; ?></li>
						</ol>							
					  </div>
					  <div class="content">
						  <form id="frmMain" name="frmMain" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="update-emp.php">
						  <input type="hidden" name="emp_id" id="emp_id" value="<?php echo $emp_id; ?>">
						    <input type="hidden" name="tmpimg" />
  							<input type="hidden" name="delimg" />
						  <div class="col-sm-10">
							  <div class="form-group row">
								<label class="col-sm-1 control-label">รหัสพนักงาน</label>
								<div class="col-sm-1">
								  <input class="form-control" id="code" name="code" required="" placeholder="9999" type="text">
								</div>				                
								<label class="col-sm-2 control-label">คำนำหน้า</label>
								<div class="col-sm-2">
								  <input class="form-control" id="prefix" name="prefix" required="" placeholder="คำนำหน้า" type="text">
								</div>			                			                
								<label class="col-sm-1 control-label">ชื่อ</label>
								<div class="col-sm-2">
								  <input class="form-control" name="fname" id="fname" required="" placeholder="ชื่อ" type="text">
								</div>			                			                
								<label class="col-sm-1 control-label">นามสกุล</label>
								<div class="col-sm-2">
								  <input class="form-control" required="" name="lname" id="lname" placeholder="นามสกุล" type="text">
								</div>
							  </div>
							  <div class="form-group row">
								<label class="col-sm-1 control-label">ชื่อเล่น</label>
								<div class="col-sm-2">
								  <input name="nickname" type="text" placeholder="ชื่อเล่น" class="form-control" id="nickname">
								</div>
								<label class="col-sm-1 control-label">เลข ปปช</label>
								<div class="col-sm-2">
								  <input class="form-control" required="" name="cid" id="cid" placeholder="เลขประจำตัวประชาชน" type="text">
								</div>
								<label class="col-sm-1 control-label">วันเกิด</label>
								<div class="col-sm-2">								
				                    <input class="form-control" size="16" name="birthdate" id="birthdate" value="" type="text" required="">		                 							 
								</div>
								<label class="col-sm-1 control-label">E-Mail</label>
								<div class="col-sm-2">
								  <input class="form-control" name="email" required="" id="email" placeholder="Enter a valid e-mail" type="email">
								</div>
							  </div>
							  <div class="form-group row">
									<label class="col-sm-1 control-label">ที่อยู่</label>
									<div class="col-sm-2">
									  <textarea class="form-control" id="address" name="address"></textarea>
									</div>			                
									<label class="col-sm-1 control-label">เบอร์มือถือ</label>
									<div class="col-sm-2">
									  <input class="form-control" required="" name="phone" id="phone" placeholder="เบอร์โทรศัพท์มือถือ" type="text">
									</div>
									<label class="col-sm-1 control-label">เบอร์อื่นๆ</label>
									<div class="col-sm-2">
									  <input class="form-control" name="homephone" required="" id="homephone" placeholder="เบอร์โทรศัพท์อื่นๆ">
									</div>
									<label class="col-sm-1 control-label">สถานะ</label>
									<div class="col-sm-2">
										<select name="active" id="active" class="form-control required">
										  <option selected="selected" value="T">แสดง</option>
										  <option value="F">ไม่แสดง</option>
										</select>
									</div>
							  </div>
						  </div>
							<div class="col-sm-2">
								<div class="form-group">								
							  		<img id="img" src="images/no-avatar-male.jpg" style="width:110px;" onClick="removeImg();" class="thumbnail">
									<label class="col-sm-12 control-label">Image Upload</label>
									<div class="col-sm-12">
										<input type="file" name="empimg" id="empimg" size="19" onChange=" readURL(this);">							
								  	</div>
						  		</div>
							</div>
							<div class="clear"><hr></div>
						  <div class="form-group">
								<label class="col-sm-1 control-label">Username</label>
								<div class="col-sm-2">
								  <input class="form-control" required="" name="username" id="username" placeholder="username" type="text">
								</div>			                
								<label class="col-sm-1 control-label">Password</label>
								<div class="col-sm-2">
								  <input class="form-control" required="" name="password" id="password" placeholder="password" type="password">
								</div>
								<label class="col-sm-1 control-label">สิทธิ์</label>
								<div class="col-sm-2">
								  <select name="righttype_id"  id="righttype_id" class="form-control required">
					                <?php
					                $q = "select righttype_id, name from righttype where active='T'";
					                $r = $db->get($q);
					                foreach($r as $k=>$v){
					                 ?>
					                 <option  value="<?php echo $v["righttype_id"]?>"><?php echo $v["name"]?></option>
					                 <?php } ?>
					               </select>
								</div>
								<label class="col-sm-1 control-label">สถานะ login</label>
								<div class="col-sm-2">
									<select name="activelogin" id="activelogin" class="form-control required">
									  <option selected="selected" value="T">Enable </option>
									  <option value="F">Disable</option>
									</select>
								</div>
						  </div>
							<div class="clear"></div>
							<div class="form-group">
								<label class="col-sm-2 control-label">ประเภทใบอนุญาต/คุณวุฒิ</label>
								<div class="col-sm-3">
									<select name="section_id" id="section_id" class="form-control">
										<option value="">---- เลือก ----</option>
										<?php foreach ($section as $key => $value) {
											$id = $value['section_id'];
											$name = $value['name'];
											echo  "<option value='$id'>$name</option>";
										} ?>

									</select>
								</div> 
								<label class="col-sm-2 control-label">ประเภทหลักสูตร</label>
								<div class="col-sm-3">
									<select name="coursetype_id" id="coursetype_id" class="form-control">
										<option value="">---- เลือก ----</option>
										<?php foreach ($coursetype as $key => $value) {
											$id = $value['coursetype_id'];
											$name = $value['name'];
											echo  "<option value='$id'>$name</option>";
										} ?>

									</select>
								</div>  
							</div>
							<br>
						  <div class="form-group">
								<div class="col-md-12">
									<button type="button" class="btn btn-primary" onClick="ckSave()">Save changes</button>
									<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p'] ?>');">Cancel</button>
								</div>
						  </div>
						</form>
					  </div>
					</div>
					
				  </div>
				</div>

		</div>
	</div> 
</div>
<?php include_once ('inc/js-script.php'); ?>
<script type="text/javascript">
   $(document).ready(function() {
   $("#frmMain").validate();
   var emp_id = "<?php echo $_GET["emp_id"]?>";
   if(emp_id) viewInfo(emp_id);
   	$("#birthdate").datepicker({language:'th-th',format:'dd-mm-yyyy'});


   	 // show succes
	 ///////////////////////////////////////////////
	 var error = "<?php echo $error ?>";
	 if(error!=""){
		$.gritter.removeAll({
	        after_close: function(){
	          $.gritter.add({
	          	position: 'center',
		        title: 'Error',
		        text: error,
		        class_name: 'danger'
		      });
	        }
	     });
	 }
	 var success = "<?php echo $success ?>";
	 if(success!=""){
		$.gritter.removeAll({
	        after_close: function(){
	          $.gritter.add({
	          	position: 'center',
		        title: 'success',
		        text: success,
		        class_name: 'success'
		      });
	        }
	    });
	 }
	 /////////////////////////////////////////////



 });
function readURL(input) {
 if (input.files && input.files[0]) {
  var reader = new FileReader();
  reader.onload = function(e) {
   $('#img').attr('src', e.target.result);
 }
 reader.readAsDataURL(input.files[0]);
}
}
 function viewInfo(emp_id){
   if(typeof emp_id=="undefined") return;
   getEmpInfo(emp_id);
}

function getEmpInfo(id){
	if(typeof id=="undefined") return;
	var url = "data/emplist.php";
	var param = "emp_id="+id+"&single=T";
	dataUrl(url, param,"#frmMain");
}

function removeImg(){
  var defaultImg = "images/no-avatar-male.jpg";
  if($('input[name=tmpimg]').val()=="")return;
  var t = confirm("ลบรูปภาพ");
  if(!t) return;
  $('#img').attr('src', defaultImg);
  $('input[name=delimg]').val("T");	
  ckSave();
}
function ckSave(id){
  onCkForm("#frmMain");
  $("#frmMain").submit();
}	
</script>