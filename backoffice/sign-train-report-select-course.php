<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
global $db;
$section = datatype(" and a.active='T'", "section", true);
$coursetype = datatype(" and a.active='T'", "coursetype", true);
$type = $_GET["type"];
$str = "เลือกหลักสูตร/วิชา";
?>
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="col-sm-12">
				<div class="content block-flat ">
						<div class="header">						
						<h3><i class="fa fa-list"></i> &nbsp;ใบเซ็นชื่อ IC ,อบรม,อบรมต่ออายุ IBA สอบ,อบรม+สอบ,อบรมต่ออายุ FA>>สอบ,อบรม,อบรมต่ออายุ </h3>
						</div>
						<div class="form-group row">
							<label class="col-sm-2 control-label">ประเภทใบอนุญาต/คุณวุฒิ</label>
							<div class="col-sm-3">
								<select name="section_id" id="section_id" class="form-control" onchange="reCall();">
									<option value="">---- เลือก ----</option>
									<?php foreach ($section as $key => $value) {
										$id = $value['section_id'];
										if($SECTIONID>0 && $SECTIONID!=$id) continue;
										$name = $value['name'];
										echo  "<option value='$id'>$name</option>";
									} ?>

								</select>
							</div>                
							<label class="col-sm-2 control-label">ประเภทหลักสูตร</label>
							<div class="col-sm-2">
								<select name="coursetype_id" id="coursetype_id" class="form-control" onchange="reCall();">
									<option value="">---- เลือก ----</option>
									<?php foreach ($coursetype as $key => $value) {
										$id = $value['coursetype_id'];
										$name = $value['name'];
										echo  "<option value='$id'>$name</option>";
									} ?>

								</select>
							</div>                                         
						</div> 
					<table id="tbCourse" class="table" style="width:100%">
						  <thead>
							  <tr>
								  <th width="8%">ลำดับ</th>
								  <th width="8%">รหัส</th>
								  <th width="40%">ชื่อหลักสูตร</th>
								  <th width="17%">ประเภทใบอนุญาต/คุณวุฒิ</th>
								  <th width="13%">ประเภทหลักสูตร</th>
								  <th width="14%">Manage</th>
							  </tr>
						  </thead>   
						<tbody>
						</tbody>
					</table>
					<div class="clear"></div>
				</div>
			</div>

		</div>
	</div> 
</div>
<?php include ('inc/js-script.php') ?>

<script type="text/javascript">
$(document).ready(function() {
	var get_type = "<?php echo $_GET["type"]; ?>";
	if(get_type=="childlist" || get_type=="course_order") $("#add").hide();
	var oTable;
	listItem();	
});

function listItem(){
   var get_type = "<?php echo $_GET["type"]; ?>";
   var url = "data/courselist.php";
   oTable = $("#tbCourse").dataTable({
	   "sDom": 'T<"clear">lfrtip',
	   "oLanguage": {
   	   "sInfoEmpty": "",
   		"sInfoFiltered": ""
						  },
		"oTableTools": {
			"aButtons":  ""
		},
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": url,
		"sPaginationType": "full_numbers",
		"aaSorting": [[ 0, "desc" ]],
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			aoData.push({"name":"section_id","value":$("#section_id").val()});			
			aoData.push({"name":"coursetype_id","value":$("#coursetype_id").val()});			
			aoData.push({"name":"active","value":'T'});			
			aoData.push({"name":"type","value":'select-course'});			
			$.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});
		}
   }); 
}
function select_course(id){
    var url = "index.php?p=<?php echo $_GET["p"];?>&course_id="+id+"&type=select-course-detail";
   redirect(url);
}

function reCall(){
	oTable.fnClearTable( 0 );
	oTable.fnDraw();
}


</script>