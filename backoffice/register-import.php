<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
include_once "./share/member.php";
include_once "./chk-bls5-func.php";
require_once dirname(__FILE__) . '/PHPExcel.php';
global $db;
global $SECTIONID, $EMPID, $RIGHTTYPEID;
set_time_limit(0);

$apay = datatype(" and a.active='T' and a.pay_status_id in (3,5,6) ", "pay_status", true);
$to_elearnning_register_ids = "";
// unset($_SESSION["register_import"]["register_ids"]);
$educationlevel = datatype(" and a.active='T'", "educationlevel", true);
$province = datatype(" and a.active='T'", "province", true);
$receipttype = datatype(" and a.active='T'", "receipttype", true);
$university = datatype_university(true);

$error = $_SESSION["error"]["msg"];
unset($_SESSION["error"]["msg"]);
$success = $_SESSION["success"]["msg"];
unset($_SESSION["success"]["msg"]);

$course_id = $_GET["course_id"];
$course_detail_id = $_GET["course_detail_id"];
$section = datatype(" and a.active='T'", "section", true);
$coursetype = datatype(" and a.active='T'", "coursetype", true);
$q = "select  day, date, time , address, address_detail from course_detail where course_detail_id=$course_detail_id";
$r = $db->rows($q);
$str = "";
if($r){
	$str = $r["day"]." ".revert_date($r["date"])." ".$r["time"]." ".$r["address_detail"]." ".$r["address"];
	$info = get_course("", $course_id);
	if($info) $info = $info[0];
}else{
	$str = "เพิ่มหลักสูตร";
}
$preview = false;

$fileName = $_FILES['excelFile']['tmp_name'];
//
if (!empty($_FILES) && $_POST) {
     
	$uploaddir = 'uploads/'; 
	$file = $uploaddir . basename($_FILES['excelFile']['name']); 
	$name_photo=($_FILES['excelFile']['name']);
	$size=$_FILES['excelFile']['size'];
	if($size>10485760000)
	{
		echo "error file size > 100 MB";
		unlink($_FILES['excelFile']['tmp_name']);
		exit;
	}

	if (move_uploaded_file($_FILES['excelFile']['tmp_name'], $file)) { 
		$fileName = $file;
		//echo "success"; 
	} else {
		echo "error ".$_FILES['excelFile']['error']." --- ".$_FILES['excelFile']['tmp_name']." %%% ".$file."($size)";
	}  
}
	
if(isset($fileName)) $preview = true;
?>

<?php 
	$del_popup = "F";
	if($_POST["clearAll"]=="T" && $_POST["del_course_detail_id"]>0 && $_POST["del_course_id"]>0){
		$del_course_detail_id = $_POST["del_course_detail_id"];
		$del_course_id = $_POST["del_course_id"];
		$now = date("Y-m-d H:i:s");
		$q = "update register set active='F',remark='ล้างข้อมูล', recby_id=$EMPID, rectime='{$now}' where active='T' and pay='importfile' and course_detail_id=$del_course_detail_id and course_id=$del_course_id";
		$db->query($q);
		$del_popup = "T";
	}
 ?>
<link rel="stylesheet" type="text/css" href="js/jquery.nanoscroller/nanoscroller.css" />
<link href="js/jquery.icheck/skins/square/blue.css" rel="stylesheet">
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="row">

				<div class="col-md-12">           
					<div class="block-flat">
						<div class="header">              
							<ol class="breadcrumb">
								<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>');">หน้าหลัก</a></li>
								<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>&type=select-course');">เลือกหลักสูตร</a></li>
								<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>&course_id=<?php echo $course_id; ?>&type=select-course-detail');">เลือกสถานที่</a></li>
								<li class="active">สมัครแบบกลุ่ม</li>
							</ol>
						</div>
						<div class="content">
							<?php if($del_popup=="T"){ ?>
							<div class="alert alert-success col-md-12">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<i class="fa fa-check sign"></i><strong>Success!</strong> ล้างข้อมูลเรียบร้อยแล้ว
							</div>							
							<?php } ?>
							<?php if($preview===true){ 
								$args = array();
								$args[] = "title";
								$args[] = "fname";
								$args[] = "lname";
								$args[] = "cid";
								$args[] = "nation"; 
								$args[] = "slip_type";
								$args[] = "email1"; 
								$args[] = "receipttype_id";
								$args[] = "receipttype_text";
								$args[] = "receipt_id";
								$args[] = "taxno";
								$args[] = "receipt_title";
								$args[] = "receipt_fname";
								$args[] = "receipt_lname";
								$args[] = "receipt_no";
								$args[] = "receipt_gno";
								$args[] = "receipt_moo";
								$args[] = "receipt_soi";
								$args[] = "receipt_road";
								$args[] = "receipt_district_id";
								$args[] = "receipt_amphur_id";
								$args[] = "receipt_province_id";	
								$args[] = "receipt_postcode";  
								$data = array();
								$headBar = "";								
								//error_reporting(E_ALL);
								//ini_set('display_errors','off');

								$objPHPExcel = PHPExcel_IOFactory::load($fileName);
								$temp = array();
								foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
									$worksheetTitle     = $worksheet->getTitle();
													      $highestRow         = $worksheet->getHighestRow(); // e.g. 10
													      $highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
													      $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
													      $nrColumns = ord($highestColumn) - 64;
													      //echo"<div class='box-header '><h2><i class='icon-edit'></i>&nbsp;";
													      $headBar .= "นำเข้าไฟล์ ".$worksheetTitle." ";
													      $headBar .= $nrColumns . ' คอลัมน์ (A-' . $highestColumn . ') ';
													      $headBar .= ' จำนวน ' . ($highestRow-1) . ' เร็คคอร์ด.';
													      //echo "</h2></div>";
													       //echo '<table class="table table-striped">';
													      for ($row = 2; $row <= $highestRow; ++ $row) {
													          //echo '<tr>';
													      	$tr_temp = "<tr>";
													      	$t = array();
													      	for ($col = 0; $col < $highestColumnIndex; ++ $col) {
													      		$cell = $worksheet->getCellByColumnAndRow($col, $row);
													      		$val = $cell->getValue();

													      		$dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
													             //echo '<td>' .$val.'</td>';
													             $tr_temp .= '<td>' .$val.'</td>';
													      		$t[$args[$col]] = $val;
													      	}
													      	$data[] = $t;
													         //echo '</tr>';
													         $tr_temp .= '</tr>';
													         $temp[] = $tr_temp;
													      }
													       //echo '</table>';
													  }
/*															echo "<pre>";
															print_r($data);
															echo "</pre>"; */
													  	$book = $_POST["book"];
													  	if($_POST["usebook"]=="T" && $book<count($data)){ ?>
													  				<div class="alert alert-danger">
													  					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
													  					<i class="fa fa-times-circle sign"></i><strong>Error!</strong> จำนวนที่สำรองที่นั่ง (<strong> <?php echo $book; ?>) </strong> น้อยกว่า จำนวนที่เอาเข้า (<?php echo count($data) ?>)
													  				</div>
													  		<?php
													  		$data = array();
													  	}
													  if($data){
													  	$count = 0;
													  	$err = 0;
													  	$success = 0;
													  	//$db->begin();
													  	$array_error = array();
													  	$array_success = array();
 														ini_set('max_input_time', 600);
 														ini_set("memory_limit", "400M");
 														// $_SESSION["register_import"]["register_ids"] = null;
													  	foreach ($data as $key => $field) {													  		
													  		$count++;	
													  		if($count%30==0){
													  			$db = new MySQLDB;
													  		}
													  		$field["title"] = trim($field["title"]);												  		
													  		if(trim($field["title"])==""){ 
													  			$err++;
													  			$tr_error_msg = '';
													  			$tr_error_msg = 'คำนำหน้า เป็นค่าว่าง';
													  			$tr_error = '<tr class="alert-danger"><td colspan="24">'.alert_danger($tr_error_msg).'</td></tr>';													  			
													  			$array_error[$key] = $temp[$key].$tr_error;
													  			continue;
													  		}				
													  		$field["fname"] = trim($field["fname"]);								  		
													  		if(trim($field["fname"])==""){
													  			$err++;
													  			$tr_error_msg = '';
													  			$tr_error_msg = 'ชื่อ เป็นค่าว่าง';
													  			$tr_error = '<tr class="alert-danger"><td colspan="24">'.alert_danger($tr_error_msg).'</td></tr>';													  			
													  			$array_error[$key] = $temp[$key].$tr_error;
													  			continue;
													  		}			
													  		$field["lname"] = trim($field["lname"]);									  		
													  		if(trim($field["lname"])==""){
													  			$err++;
													  			$tr_error_msg = '';
													  			$tr_error_msg = 'นามสกุล เป็นค่าว่าง';
													  			$tr_error = '<tr class="alert-danger"><td colspan="24">'.alert_danger($tr_error_msg).'</td></tr>';													  			
													  			$array_error[$key] = $temp[$key].$tr_error;
													  			continue;
													  		}


												  			$chk_title = preg_match("/^[a-zA-Zก-เ. ]{1,}$/", trim($field["title"]));
												  			$chk_fname = preg_match("/^[a-zA-Zก-เ. ]{1,}$/", trim($field["fname"]));
												  			$chk_lname = preg_match("/^[a-zA-Zก-เ. ]{1,}$/", trim($field["lname"]));	
												  			$tr_error_msg = '';
													  		$tr_error_msg = 'ตรวจพบอักขระพิเศษใน ';
												  			$perfect_name = true;
												  			if ($chk_title!==1) {
												  				$tr_error_msg .= " คำนำหน้าชื่อ";
												  				$perfect_name = false;
												  			}else if ($chk_fname!==1) {
												  				$tr_error_msg .= " ชื่อ";
												  				$perfect_name = false;
												  			}else if ($chk_lname!==1) {
												  				$perfect_name = false;
												  				$tr_error_msg .= " นามสกุล";
												  			}
												  			if($perfect_name!==true){													  				
													  			$err++;													  			
													  			//$tr_error_msg = 'ตรวจพบอักขระพิเศษใน คำนำหน้าชื่อ, ชื่อ, นามสกุล';
													  			$tr_error = '<tr class="alert-danger"><td colspan="24">'.alert_danger($tr_error_msg).'</td></tr>';
													  			$array_error[$key] = $temp[$key].$tr_error;
													  			continue;
												  			}
													  		

													  		if(trim($field["cid"])==""){
													  			$err++;
													  			$tr_error_msg = '';
													  			$tr_error_msg = 'ไม่มีเลขที่บัตรประชาชน';
													  			$tr_error = '<tr class="alert-danger"><td colspan="24">'.alert_danger($tr_error_msg).'</td></tr>';													  			
													  			$array_error[$key] = $temp[$key].$tr_error;
													  			continue;
													  		}
													  		$cid = trim($field["cid"]);
/*													  		
													  		$q = "select member_id from member where cid='$cid'";
													  		$member_id = $db->data($q);		
*/

//---------------------------------- chk block list v5 ------------------------------------------//
													  		
													  		// write_log("register-import-cid", $cid, "a", "./log/");
													  		if ( !empty($cid) ) {
													  			
													  			$q = "select member_id from member where cid='$cid'";
													  			$member_id = $db->data($q);
/*
													  			// $cid = 1329900125954;
													  			$q = "SELECT section_id
													  					, coursetype_id 
													  				FROM course 
													  				WHERE course_id=$course_id
													  			";
													  			$cos_info = $db->rows($q);
																$sec_id = $cos_info["section_id"];
																$type_id = $cos_info["coursetype_id"];
*/
																$sec_id = $info["section_id"];
																$type_id = $info["coursetype_id"];
																// $course_id = 1137;
																// $course_detail_id = 6983;
																$res = chk_bls5($cid, $sec_id, $type_id, $course_id, $course_detail_id);

																if ( !empty($res) ) {
																	$str = "";
																	$str .= "cid={$cid}|res={$res}";
																	write_log("register-import_chk_blocklist", $str, "a", "./log/");

																	$res = explode(":", $res);
																	$rs_cd = trim($res[0]);
																	$rs_date = trim($res[1]);

																	$date_spit = explode("-", $rs_date);

																	$year = trim($date_spit[0])+543;
																	$month = trim($date_spit[1]);

																	$day = trim($date_spit[2]);
																	$rs_date = $day."-".$month."-".$year;			

																	// echo $rs_date;

																	$tr_error_msg = "";
																	if($rs_cd == '3000'){
																					
																	}else{

																		if ( $rs_cd == '1000' ) {
																			$tr_error_msg .= "connect ได้ ข้อมูลไม่ถูกต้อง (จำนวนข้อมูล)";
																		}else if ( $rs_cd == '1001' ) {
																			$tr_error_msg .= "connect ได้ ข้อมูลไม่ถูกต้อง (เลขบัตร)";
																		}else if ( $rs_cd == '1002' ) {
																			$tr_error_msg .= "connect ได้ ข้อมูลไม่ถูกต้อง (วันที่)";
																		}else if ( $rs_cd == '2000' ) {
																			$tr_error_msg .= "invalid user or password";
																		}else if ( $rs_cd == '4001' ) {
																			$tr_error_msg .= "ระบบไม่สามารถทำการรับสมัครสอบของท่านได้ ท่านไม่ได้รับอนุญาติให้สมัครจนถึงวันที่ ".$rs_date;
																		}else if ( $rs_cd == '4002' ) {
																			$tr_error_msg .= "ระบบไม่สามารถทำการรับสมัครสอบของท่านได้ ท่านไม่ได้รับอนุญาติให้สมัครจนถึงวันที่ ".$rs_date;
																		}else if ( $rs_cd == '4003' ) {
																			$tr_error_msg .= "ท่านสอบผ่านหลักสูตรนี้แล้ว ท่านจะสามารถสอบหลักสูตรนี้ได้อีกครั้ง ตั้งแต่วันที่ ".$rs_date;
																		}else if ( $rs_cd == '4000' ) {
																			$tr_error_msg .= "อยู่ในรายการห้ามสอบ จนถึงวันที่ ".$rs_date." (block case 4 :  กรณีเป็นเจ้าหน้าที่ ATI)";
																		}//end else if

																		$tr_error = '<tr class="alert-danger"><td colspan="24">'.alert_danger($tr_error_msg).'</td></tr>';

																		$array_error[$key] = $temp[$key].$tr_error;
																		continue;
																	}//end else		

																}//end if

													  		}//end if
												  		
//---------------------------------- chk block list v5 ------------------------------------------//


													  		if($field["nation"]=="T" || $field["nation"]==""){
													  			$ck = check_cart_id($cid);
													  			//var_dump($ck);
													  			if(!$ck){
														  			$err++;
														  			$tr_error_msg = '';
														  			$tr_error_msg = 'เลขที่บัตรประชาชน ไม่ถูกต้อง';
														  			$tr_error = '<tr class="alert-danger"><td colspan="24">'.alert_danger($tr_error_msg).'</td></tr>';
														  			//echo "<hr> เลขที่บัตรประชาชน ไม่ถูกต้อง : ID".$cid;
														  			$array_error[$key] = $temp[$key].$tr_error;
														  			continue;	
													  			}else{
																	$course_detail_id = $_POST["course_detail_id"];
																	$q = "select date from course_detail where course_detail_id={$course_detail_id}";
																	$date = $db->data($q);
																	$result = array();
																	$section_id= (int)$_POST["section_id"];
																	$coursetype_id = (int)$_POST["coursetype_id"];
																	if($date && $section_id==1 && $coursetype_id==2){
																		$ex = explode("-", $date);
																		$exd = $ex[2]; 
																		$exm = $ex[1]; 
																		$exy = $ex[0];
																		$res = check_block_list($cid, $exd, $exm, $exy);
																		$rs = explode(":", $res);
																		$rule = $rs[0];
																		$date = ($rs[1]) ? revert_date($rs[1]) : "";
																		$result[] = array('result' => $rule, "date"=> $date);
																		if($rule=="4000"){
																  			$tr_error_msg = '';
																  			$tr_error_msg = "เลขที่บัตรประชาชน ติด block List ".$rule;
																  			$tr_error = '<tr class="alert-danger"><td colspan="24">'.alert_danger($tr_error_msg).'</td></tr>';																			
																			//echo "<hr> เลขที่บัตรประชาชน ติด block List ".$rule." ID : ".$cid;
																  			$err++;
																  			$array_error[$key] = $temp[$key].$tr_error;
																  			continue;																			
																		}
																	}													  				
													  			}
													  		}
													  		if($field["slip_type"]=="1"){
													  			$field["slip_type"] = "individuals";
													  		}else if($field["slip_type"]=="2"){
													  			$field["slip_type"] = "corparation";
													  		}else{
													  			$err++;
													  			//echo "<hr>ไม่มีข้อมูล ประเภทการออกใบเสร็จ ID : ".$cid;
													  			$tr_error_msg = '';
													  			$tr_error_msg = "ไม่มีข้อมูล ประเภทการออกใบเสร็จ";
													  			$tr_error = '<tr class="alert-danger"><td colspan="24">'.alert_danger($tr_error_msg).'</td></tr>';  			
													  			$array_error[$key] = $temp[$key].$tr_error;
													  			continue;													  			
													  		}
													  		$email = $field["email1"];
													  		if (filter_var($email, FILTER_VALIDATE_EMAIL)) {													  			
													  		}else{
													  			$err++;
													  			//echo "<hr>ไม่มีข้อมูล ประเภทการออกใบเสร็จ ID : ".$cid;
													  			$tr_error_msg = '';
													  			$tr_error_msg = (trim($email)=="") ? "Email เป็นค่าว่าง" : "รูปแบบ Email ไม่ถูกต้อง";
													  			$tr_error = '<tr class="alert-danger"><td colspan="24">'.alert_danger($tr_error_msg).'</td></tr>';														  			
													  			$array_error[$key] = $temp[$key].$tr_error;
													  			continue;													  			
													  		}													  		
												  			$receipt_id = (int)$field["receipt_id"];
												  			if($receipt_id){
												  				$aReceipt = get_receipt("", $receipt_id);
																$receipt_info = $aReceipt[0];
																if($receipt_info){																		
													  				/*stert*/
																	$field["receipt_no"] = $receipt_info["no"];
																	$field["receipt_gno"] = $receipt_info["gno"];
																	$field["receipt_moo"] = $receipt_info["moo"];
																	$field["receipt_soi"] = $receipt_info["soi"];
																	$field["receipt_road"] = $receipt_info["road"];
																	$field["receipt_district_id"] = $receipt_info["district_id"];
																	$field["receipt_amphur_id"] = $receipt_info["amphur_id"];
																	$field["receipt_province_id"] = $receipt_info["province_id"];
																	$field["receipt_postcode"] = $receipt_info["postcode"];
																	$field["taxno"] = $receipt_info["taxno"];
													  				/*stop*/
																}
												  				
												  			}
													  		$check_status = $_POST["pay_status"];
													  		if($member_id){
													  			$member_info = view_member("", $member_id);
													  			$member_info = $member_info[0];

																$field["title"] = ($member_info["title_th"]=="อื่นๆ") ? $member_info["title_th_text"] : $member_info["title_th"];
																$field["fname"] = $member_info["fname_th"];
																$field["lname"] = $member_info["lname_th"];
																$field["slip_type"] = $member_info["slip_type"];
																$field["receipttype_id"] =  (int)$member_info["receipttype_id"];
																$field["receipttype_text"] = $member_info["receipttype_text"];
																$field["receipt_id"] = (int)$member_info["receipt_id"];
																$field["taxno"] = $member_info["taxno"];
																$field["receipt_title"] = $member_info["receipt_title"];
																$field["receipt_fname"] = $member_info["receipt_fname"];
																$field["receipt_lname"] = $member_info["receipt_lname"];
																$field["receipt_no"] = $member_info["receipt_no"];
																$field["receipt_gno"] = $member_info["receipt_gno"];
																$field["receipt_moo"] = $member_info["receipt_moo"];
																$field["receipt_soi"] = $member_info["receipt_soi"];
																$field["receipt_road"] = $member_info["receipt_road"];
																$field["receipt_province_id"] = (int)$member_info["receipt_province_id"];
																$field["receipt_amphur_id"] = (int)$member_info["receipt_amphur_id"];
																$field["receipt_district_id"] = (int)$member_info["receipt_district_id"];
																$field["receipt_postcode"] = $member_info["receipt_postcode"];													  			

													  		}else{
													  			if($field["title"]=="นาง" || $field["title"]=="นาย" || $field["title"]=="นางสาว"){
													  				$field["title_th"] = $field["title"];													  				
													  			}else{
													  				$field["title_th"] = "อื่นๆ";
													  			}
														  		$args = array();
																$args["table"] = "member";
																$args['username'] = $cid;
																$args['cid'] = $cid;
																$args['password'] = $cid;
																$args['title_th'] = $field["title_th"];	
																if($field["title_th"]=="อื่นๆ")															
																	$args['title_th_text'] = $field["title"];																
																$args['fname_th']  = $field["fname"];
																$args['lname_th']  = $field["lname"];
																$args['email1']  = $field["email1"];
																$args['email2']  = $field["email2"];
																$args['reg_date']  =  date("Y-m-d H:i:s");
																$args['last_update']  =  date("Y-m-d H:i:s");
																$args['slip_type']  = $field["slip_type"];
																$args['receipt_id']   = (int)$field["receipt_id"];
																$args['receipttype_id']   = (int)$field["receipttype_id"];
																$args['receipt_title'] = $field["receipt_title"];
																$args['receipt_title_text'] = $field["receipt_title_text"];
																$args['receipt_fname']  = $field["receipt_fname"];
																$args['receipt_lname']  = $field["receipt_lname"];
																$args['receipt_no']  = $field["receipt_no"];
																$args['receipt_gno']  = $field["receipt_gno"];
																$args['receipt_moo']  = $field["receipt_moo"];
																$args['receipt_soi']  = $field["receipt_soi"];
																$args['receipt_road']  = $field["receipt_road"];
																$args['receipt_district_id']   = (int) $field["receipt_district_id"];
																$args['receipt_amphur_id']   = (int) $field["receipt_amphur_id"];
																$args['receipt_province_id']   = (int) $field["receipt_province_id"];
																$args['receipt_postcode']   = (int)$field["receipt_postcode"];
																$args['nation']   = $field["nation"];
																$args['taxno']   = $field["taxno"];
																$ret = $db->set($args);
																if(!$ret){
																	//echo "<hr> สมัครสมาชิกไม่ได้ ID : ".$cid;
														  			$tr_error_msg = '';
														  			$tr_error_msg = " สมัครสมาชิกไม่ได้ mysql error";
														  			$tr_error = '<tr class="alert-danger"><td colspan="24">'.alert_danger($tr_error_msg).'</td></tr>';														  			
																	//print_r($args);
																	$err++;
														  			$array_error[$key] = $temp[$key].$tr_error;																	
																	continue;
																}																
													  		}		
															$member_id = ($member_id) ? $member_id : $ret; 
															if(!$member_id){
																	//echo "<hr>ไม่มีข้อมูลสมาชิก ID ".$cid;
														  			$tr_error_msg = '';
														  			$tr_error_msg = " ไม่มีข้อมูลสมาชิก";
														  			$tr_error = '<tr class="alert-danger"><td colspan="24">'.alert_danger($tr_error_msg).'</td></tr>';																	
																	//print_r($args);
																	$err++;
																	$array_error[$key] = $temp[$key].$tr_error;
																	continue;	
															}
															$q = "select register_id from register a where a.active='T' and  a.course_detail_id='{$_POST["course_detail_id"]}' and a.member_id = '$member_id' and a.pay_status in (1,3,5,6)";
															$register_id = $db->data($q);
															if($register_id){
																//echo "เคยสมัครรอบนี้ แล้ว".$cid;
													  			$tr_error_msg = '';
													  			$tr_error_msg = "เคยสมัครรอบนี้ แล้ว";
													  			$tr_error = '<tr class="alert-danger"><td colspan="24">'.alert_danger($tr_error_msg).'</td></tr>';																
																$err++;
																$array_error[$key] = $temp[$key].$tr_error;
																continue;
															}
															$q = " select a.register_id from register_course_detail a inner join register b on a.register_id=b.register_id where b.active='T' and a.course_detail_id=$course_detail_id and a.member_id = '$member_id' and b.pay_status in (1,3,5,6)";
															$register_id = $db->data($q);
															
															if($register_id){
																$err++;
													  			$tr_error_msg = '';
													  			$tr_error_msg = ">สมัครรอบสอบนี้ไม่แล้ว โดยใช้ร่วมกับหลักสูตรอื่น";
													  			$tr_error = '<tr class="alert-danger"><td colspan="24">'.alert_danger($tr_error_msg).'</td></tr>';																	
																//echo "<hr>สมัครรอบสอบนี้ไม่แล้ว โดยใช้ร่วมกับหลักสูตรอื่น ID : ".$cid;
																$array_error[$key] = $temp[$key].$tr_error;
																continue;
															}
															if($check_status!=3){
														  		$args = array();
																$args["table"] = "register";
																$args["member_id"] = $member_id;
																$args["cid"] = $cid;
																$args["title"] = $field["title"];
																$args["fname"] = $field["fname"];
																$args["lname"] = $field["lname"];
																$args["date"] = date("Y-m-d H:i:s");
																$args["pay_date"] = date("Y-m-d H:i:s");
																$args["pay_status"] = $_POST["pay_status"];
																$args["status"] =  $_POST["pay_status"];
																$args["pay"] = "importfile";
																$args["course_id"] = $_POST["course_id"];
																$args["course_detail_id"] = $_POST["course_detail_id"];
																$args["course_price"] = $_POST["price"];
																$args["course_discount"] = $_POST["discount"];
																$args["section_id"] = (int)$_POST["section_id"];
																$args["coursetype_id"] = (int)$_POST["coursetype_id"];
																$args["pay_price"] = $_POST["price"] - $_POST["discount"];
																$args["usebook"] = ($_POST["usebook"]=="T") ? "T" : "F";																
															}else{
																$args = array();
																$args["table"] = "register";
																$args["member_id"] = $member_id;
																$args["cid"] = $cid;
																$args["title"] = $field["title"];
																$args["fname"] = $field["fname"];
																$args["lname"] = $field["lname"];
																$args["slip_type"] = $field["slip_type"];
																$args["date"] = date("Y-m-d H:i:s");
																$args["pay_date"] = date("Y-m-d H:i:s");
																$args["pay_status"] = $_POST["pay_status"];
																$args["status"] =  $_POST["pay_status"];
																$args["pay"] = "importfile";
																$args["receipttype_id"] =  (int)$field["receipttype_id"];
																$args["receipttype_text"] = $field["receipttype_text"];
																$args["receipt_id"] = (int)$field["receipt_id"];
																$args["taxno"] = $field["taxno"];
																$args["receipt_title"] = $field["receipt_title"];
																$args["receipt_fname"] = $field["receipt_fname"];
																$args["receipt_lname"] = $field["receipt_lname"];
																$args["receipt_no"] = $field["receipt_no"];
																$args["receipt_gno"] = $field["receipt_gno"];
																$args["receipt_moo"] = $field["receipt_moo"];
																$args["receipt_soi"] = $field["receipt_soi"];
																$args["receipt_road"] = $field["receipt_road"];
																$args["receipt_province_id"] = (int)$field["receipt_province_id"];
																$args["receipt_amphur_id"] = (int)$field["receipt_amphur_id"];
																$args["receipt_district_id"] = (int)$field["receipt_district_id"];
																$args["receipt_postcode"] = (int)$field["receipt_postcode"];
																$args["course_id"] = $_POST["course_id"];
																$args["course_detail_id"] = $_POST["course_detail_id"];
																$args["course_price"] = $_POST["price"];
																$args["course_discount"] = $_POST["discount"];
																$args["section_id"] = (int)$_POST["section_id"];
																$args["coursetype_id"] = (int)$_POST["coursetype_id"];
																$args["pay_price"] = $_POST["price"] - $_POST["discount"];
																$args["usebook"] = ($_POST["usebook"]=="T") ? "T" : "F";
																// $x =  (int) $_POST["price"] -(int)$_POST["discount"];
																// $ck = $x - (int)$field["pay_price"];
																// if($ck<0){
																// 	$err++;
																// 	//echo "e3";
																// 	$array_error[$key] = $temp[$key];
																// 	continue;		
																// }
																$section_id = (int)$_POST["section_id"];
																$coursetype_id = (int)$_POST["coursetype_id"];
																$t = rundocno($coursetype_id, $section_id);
																$args = array_merge($args, $t);
															}//end else											
													  		
															$args["rectime"] = date("Y-m-d H:i:s");
															$register_id = $db->set($args);	
															if($register_id){
																$success++;
																$array_success[$key] = $temp[$key];
														  		if((int)$_POST["section_id"]==3){
														  			$db->query("update member set fa='T' where member_id=$member_id");
														  		}

														  		$to_elearnning_register_ids .= ",".$register_id;
														  		// $to_elearnning_register_ids = trim($to_elearnning_register_ids, ",");

															}else{
																$err++;
													  			$tr_error_msg = '';
													  			$tr_error_msg = ">mysql error update การสมัคร ไม่ได้ ";
													  			$tr_error = '<tr class="alert-danger"><td colspan="24">'.alert_danger($tr_error_msg).'</td></tr>';																
																//echo "<hr>update ไม่ได้  ID : ".$cid;
																$array_error[$key] = $temp[$key].$tr_error;
															}
													  	}

													  	if($err>0){
													  		//$db->rollback();
													  	}else{
													  		//$db->rollback();
													  		//$db->commit();

													  	}
													  	//print_r($result);
													  }
													  $to_elearnning_register_ids = trim($to_elearnning_register_ids, ",");
													  // $_SESSION["register_import"]["register_ids"] = $to_elearnning_register_ids;
													  // echo $to_elearnning_register_ids."<br>";
													  // echo $_SESSION["register_import"]["register_ids"]."<br>";
													  $pay_status = $_POST["pay_status"];
													  ?>
													  <div class="">
													  	<div class="content">
													  	<?php // print_r($_FILES); ?>
													  	<div class="">
													  			<div class="row" style="margin-top:0px;">
													  				<?php if($err>0){ ?>
													  				<div class="alert alert-danger">
													  					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
													  					<i class="fa fa-times-circle sign"></i><strong>Error!</strong> จำนวนที่ไม่สามารถนำเข้าข้อมูลได้ &nbsp;</strong> <?php echo $err;?> &nbsp;เร็คคอร์ด
													  				</div>
													  				<table class="table">
													  				<thead>												  					
														  				<tr>
														  					<th>คำนำหน้า</th>
														  					<th>ชื่อ</th>
														  					<th>นามสกุล</th>
														  					<th>เลขที่บัตรประชาชน </th>
														  					<th>สัญชาติไทย</th>														  					
														  					<th>ประเภทการออกใบเสร็จ</th>
														  					<th>อีเมล 1</th>
														  					<th>ID ประเภทบริษัท นิติบุคคล</th>
														  					<th>อื่นๆ</th>
														  					<th>ID บริษัทนิติบุคคล</th>
														  					<th>เลขที่บัตรประจำตัวผู้เสียภาษี</th>
														  					<th>คำนำหน้า</th>
														  					<th>ชื่อ</th>
														  					<th>นามสกุล</th>
														  					<th>บ้านเลขที่</th>
														  					<th>หมู่บ้าน</th>
														  					<th>หมู่ที่</th>
														  					<th>ซอย</th>
														  					<th>ถนน</th>
														  					<th>ID จังหวัด</th>
														  					<th>ID อำเภอ</th>
														  					<th>ID ตำบล/แขวง</th>
														  					<th>รหัสไปรษณีย์</th>
														  					<th>จำนวนเงิน</th>
														  				</tr>
													  				</thead>
													  					<?php if(count($array_error)>0){
													  							foreach ($array_error as $key => $value) {
													  								echo $value;
													  							}
													  					} ?>

													  				</table>
													  				<?php } ?>
													  				<?php if($success>0){ ?>
													  				<br>
													  				<div class="alert alert-success">
													  					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
													  					<i class="fa fa-check sign"></i><strong>Success!</strong> จำนวนที่นำเข้าข้อมูลได้ &nbsp;</strong> <?php echo $success;?> &nbsp;เร็คคอร์ด
													  				</div>
													  				
														  				<table class="table">

													  				<thead>												  					
														  				<tr>
														  					<th>คำนำหน้า</th>
														  					<th>ชื่อ</th>
														  					<th>นามสกุล</th>
														  					<th>เลขที่บัตรประชาชน </th>
														  					<th>สัญชาติไทย</th>														  					
														  					<th>ประเภทการออกใบเสร็จ</th>
														  					<th>อีเมล 1</th>
														  					<th>ID ประเภทบริษัท นิติบุคคล</th>
														  					<th>อื่นๆ</th>
														  					<th>ID บริษัทนิติบุคคล</th>
														  					<th>เลขที่บัตรประจำตัวผู้เสียภาษี</th>
														  					<th>คำนำหน้า</th>
														  					<th>ชื่อ</th>
														  					<th>นามสกุล</th>
														  					<th>บ้านเลขที่</th>
														  					<th>หมู่บ้าน</th>
														  					<th>หมู่ที่</th>
														  					<th>ซอย</th>
														  					<th>ถนน</th>
														  					<th>ID จังหวัด</th>
														  					<th>ID อำเภอ</th>
														  					<th>ID ตำบล/แขวง</th>
														  					<th>รหัสไปรษณีย์</th>
														  					<th>จำนวนเงิน</th>
														  				</tr>
													  				</thead>
													  					<?php if(count($array_success)>0){
													  							// echo $to_elearnning_register_ids;
													  							foreach ($array_success as $key => $value) {
													  								echo $value;
													  							}
													  					} ?>

													  				</table>
													  				<?php } ?>
													  			</div>
													  			
													  		</div>
													  	</div>
														<div class="clear"></div>
														<div class="form-group row" style="padding-left:10px;">
															<div class="col-sm-12">
																<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p'] ?>');">กลับสู่หน้าหลัก</button>																																			
															<button type="button" class="btn btn-primary" onClick="clearPage('<?php echo $_GET['p'] ?>&course_detail_id=<?php echo $course_detail_id; ?>&type=register-import&course_id=<?php echo $course_id; ?>')">ลงทะเบียนใหม่</button>
															</div>
														</div>
													  <?php

							}else{ ?>
							<form id="frmMain" name="frmMain" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="">
								<input type="hidden" name="course_id" id="course_id" value="<?php echo $course_id; ?>">
								<input type="hidden" name="course_detail_id" id="course_detail_id" value="<?php echo $course_detail_id; ?>">
								<input type="hidden" name="coursetype_id" id="coursetype_id" value="<?php echo $info["coursetype_id"]; ?>">
								<input type="hidden" name="section_id" id="section_id" value="<?php echo $info["section_id"]; ?>">
								<div class="col-sm-12">
									<div class="form-group row" <?php if(!$course_id) echo 'style="display:none;"'; ?>>
										<h4><?php echo $info["title"]; ?></h4>
										<table class="table table-striped" id="tbList">
											<thead>
												<tr class="alert alert-success" style="font-weight:bold;">
													<td width="3%">ลำดับ</td>
													<td width="8%" class="center">วัน</td>
													<td width="8%" class="center">เวลา</td>
													<td width="8%" class="center">สถานที่</td>
													<td width="15%" class="center">ข้อมูลสถานที่สอบ/อบรม</td>
													<td width="7%" class="center">ที่นั่งคงเหลือ (จอง)</td>
													<td width="5%" class="center">ที่นั่งทั้งหมด</td>
													<td width="6%" class="center">สถานะ</td>
													<td width="6%" class="center">เปิดรับสมัคร</td>
													<td width="6%" class="center">ปิดรับสมัคร</td>
													<td width="6%" class="center"> In House</td>												
												</tr>
											</thead>
											<tbody>
												<?php 

												// d($info);
												$today = date("Y-m-d 00:00:00");
												$price = $info["price"];
												$single_discount = 0;
												$date_start = $info["date_start"];
												$date_stop = $info["date_stop"];

												if($date_start!="" && $date_stop!="" && $today>=$date_start && $today<=$date_stop){
													$single_discount = ($info["discount"]>0) ? $info["discount"] : 0;
												}//end if
												
												$con = " and a.active='T' and a.course_detail_id=$course_detail_id";
												$r = get_course_detail($con);

												if( !empty($r) ){
													$runNo = 1; 
													$today = date("Y-m-d 00:00:00");
													$promotion = 0;
													$not_discount = false;
													$book = 0;
													foreach($r as $k=>$v){
												        $date_start = $v["date_start"];
														$date_stop = $v["date_stop"];
														if($date_start!="" && $date_stop!="" && $today>=$date_start && $today<=$date_stop){
															$promotion += $v["discount"];

															$single_discount = 0;
														}else{
															// $single_discount += $v["discount"];
														}
														$price += $v['price'];
														$course_detail_id = $v["course_detail_id"];
														$use = get_use_chair($course_detail_id);
														$chair_all = $v['chair_all'];
														$book = $v["book"];
														$usebook = 0;
														if($book>0) {
															$usebook += get_use_book($course_detail_id);
														}
														$sit_all = $chair_all - $book - $use;
														$status = get_course_status_name($sit_all, $v["status"], $v["coursetype_id"]);
														if($v["date"]<date("Y-m-d")){
															$status = "ปิดรับสมัคร";
														}	
														?>
														<tr style="cursor:pointer;">
																<td class="center"><?php echo $runNo; ?></td>
																<td class="center"><?php echo $v["day"]; ?> <?php echo revert_date($v["date"]); ?></td>
																<td  class="center"><?php echo $v["time"]; ?></td>
																<td  class="center"><?php echo $v["address"]; ?></td>
																<td  class="center"><?php echo $v["address_detail"]; ?></td>													
																<td  class="center"><?php echo $sit_all; ?> (<?php echo (int) $book - $usebook; ?>) </td>													
																<td  class="center"><?php echo $chair_all-$book ," (", $book; ?>)</td>															
																<td  class="center"><?php echo $status; ?></td>													
																<td  class="center"><?php echo revert_date($v["open_regis"]); ?></td>
																<td  class="center"><?php echo revert_date($v["end_regis"]); ?></td>
																<td  class="center"><?php echo ($v["inhouse"]=="T") ? '<i class="fa fa-check-square-o"></i>' : ''; ?></td>
														</tr>
														<?php
														$runNo++;
													} 
													$discount = $single_discount + $promotion;	
												}
												$total_price = $price - $discount;
												?>
											</tbody>
										</table>									
									</div>
									<div class="row">
										<div class="col-md-12" >
											<div class="form-group row">
														<label class="col-sm-2 control-label">จำนวนเงินที่ต้องชำระ</label>
												<div class="col-sm-3">
													<div class="input-text">
														<?php echo set_comma($total_price); ?>
													</div>
												</div> 
											</div> 
											<div class="form-group row">
														<label class="col-sm-2 control-label">เลือกไฟล์</label>
												<div class="col-sm-3">
													<div class="input-text">
														 <input type="file" name="excelFile" class="required " data-no-uniform="true">
													</div>
												</div> 
												<div class="col-md-2">
													<a href="documents/register-import.xlsx" class="btn"><i class="fa fa-download"></i> ดาวน์โหลด ตัวอย่างไฟล์</a>
												</div>
											</div> 
											<input type="hidden" name="member_id" id="member_id" value="<?php echo $member_id; ?>">
											<input type="hidden" name="price" id="price" value="<?php echo $price; ?>">
											<input type="hidden" name="discount" id="discount" value="<?php echo $discount; ?>">
											<input type="hidden" name="book" id="book" value="<?php echo $book; ?>">
												<div style="display:inline-block;margin-left:15px;" class="form-group row">
													<?php foreach ($apay as $key => $value) {
														
														echo '<label class="radio-inline"  style="padding-left:10px;padding-top:0px"> <div aria-disabled="false" aria-checked="false" style="position: relative;" class="iradio_square-blue"><input style="position: absolute; opacity: 0;" value="'.$value["pay_status_id"].'" name="pay_status"  class="icheck required" type="radio"><ins style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;" class="iCheck-helper"></ins></div>&nbsp;&nbsp;'.$value["name"].'  <span class="red">*</span></label>';
													} ?>
													&nbsp;&nbsp;
													<label class="checkbox-inline" style="padding-left:10px;padding-top:0px; <?php echo ($book<=0) ? "display:none; " : ""; ?> "> <div aria-disabled="false" aria-checked="false" style="position: relative;" class="icheckbox_square-blue"><input style="position: absolute; opacity: 0;" value="T" name="usebook" id="usebook" class="icheck" type="checkbox"><ins style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;" class="iCheck-helper"></ins></div>&nbsp;&nbsp;ลงทะเบียนจากสำรองที่นั่ง</label>

												</div>	
											</div>
										</div>
									</div>
								</div>
								<div class="clear"></div>
								<div class="form-group row" style="padding-left:10px;">
									<div class="col-sm-12">
										<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p'] ?>');">กลับสู่หน้าหลัก</button>
										<button type="button" id="bt_save" class="btn btn-primary" onClick="ckSave()">Import & Submit</button>																			
										<?php 
										 if($RIGHTTYPEID==4 || $RIGHTTYPEID==1 || $RIGHTTYPEID==2){
										 ?>
											<button type="button" id="bt_del" class="btn btn-danger pull-right" onClick="clear_import_data()">ล้างผู้สมัคร</button>																			
										<?php } ?>
									</div>
								</div>
							</form>
							<?php } ?>
						</div>
					</div>

				</div>
			</div>

		</div>
	</div> 
</div>

<form id="frmDel" name="frmDel" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="">
	<input type="hidden" name="clearAll">
	<input type="hidden" name="del_course_detail_id" value="<?php echo $course_detail_id; ?>">
	<input type="hidden" name="del_course_id" value="<?php echo $course_id; ?>">
</form>
<?php include_once ('inc/js-script.php'); ?>
<script type="text/javascript">

$(document).ready(function() {
	var register_id = "<?php echo $to_elearnning_register_ids; ?>";

	if (register_id!="" && register_id!="undefind") {
		// alert('reg_ids='+register_id);
		// var pay_status = "<?php //echo $pay_status; ?>";
		$.ajax({
			"type": "POST",
			"async": false, 
			"datatype": "json",			
			"url": "check-course-elearning.php",
			"data": {'register_id': register_id
				, 'pay_type': 'import-file'
			}, 
			"success": function(data){
				// console.log(data);			      							 
			}
		});//end ajax 

		$.ajax({
			"type": "POST",
			"async": false, 
			"datatype": "json",			
			"url": "data/update-register-course-detail.php",
			"data": {'register_id': register_id
				, 'pay_type': 'import-file'
			}, 
			"success": function(data){
				// console.log(data);			      							 
			}
		});//end ajax 
	}//end if
	
});


function ckSave(id){
	var t = confirm("ยืนยันการลงทะเบียนแบบกลุ่ม");
	if(!t) return false;
	var checkboxes = $("#frmMain").find(':checkbox');
	onCkForm("#frmMain");

	$("#frmMain").submit();
}
function clear_import_data(){
	var t = confirm("ยืนยันการล้างข้อมูล");
	if(!t) return false;
	$("input[name=clearAll]").val("T");
	$("#frmDel").submit();
}
</script>
<style>
	#set_time-error{
		display: none !important;
	}
	#pay_status-error, #slip_type-error{
		width: 400px;
		margin-top: 21px;
	}
</style>