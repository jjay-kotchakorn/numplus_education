<?php
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";
include_once "./share/member.php";
include_once "./share/course.php";
global $db, $EMPID;
set_time_limit(0);
$debug = true;
$debug = false;

$datetime_now = date('Y-m-d H:i:s');

// d($_POST); die();

$ret = "";
if ( $debug ) {
	$register_id = 179776;
}//end if

if ( !empty($_POST["register_id"]) ) {
	$register_id = $_POST["register_id"];

	$q = "SELECT
			a.register_credit_note_id,
			a.`code`,
			a.`name`,
			a.name_eng,
			a.active,
			a.ref_register_id,
			a.ref_new_register_id,
			a.runyear,
			a.runno,
			a.doc_prefix,
			a.docno,
			a.credit_note_date,
			a.ref_receipt_docno,
			a.new_receipt_docno,
			a.ref_pay_price,
			a.ref_pay_date,
			a.credit_note_price,
			a.diff_price,
			a.recby_id,
			a.rectime,
			a.remark,
			a.reason
		FROM register_credit_note a
		WHERE a.active='T'
			AND a.ref_register_id = {$register_id}
	";
	$cr_info = $db->rows($q);

	if ( !empty($cr_info) ) {
		
		$args = array();
		$args["table"] = "register_credit_note_log";
		$args["register_credit_note_id"] = $cr_info["register_credit_note_id"];
		$args["ref_register_id"] = $cr_info["ref_register_id"];
		$args["ref_receipt_docno"] = $cr_info["ref_receipt_docno"];
		$args["ref_pay_price"] = $cr_info["ref_pay_price"];
		$args["ref_pay_date"] = $cr_info["ref_pay_date"];
		$args["credit_note_price"] = $cr_info["credit_note_price"];
		$args["remark"] = $cr_info["remark"];
		$args["reason"] = $cr_info["reason"];
		$args["diff_price"] = (float)$cr_info["diff_price"];
		$args["credit_note_date"] = $cr_info["credit_note_date"];
		$args["runno"] = $cr_info["runno"];
		$args["docno"] = $cr_info["docno"];
		$args["runyear"] = $cr_info["runyear"];	
		$args["recby_id"] = $EMPID;
		$args["rectime"] = $datetime_now;

		$register_credit_note_log_id = $db->set($args);
		// var_dump($db->set($args, true, true)); die();

	}//end if


	$reg_info = get_register("", $register_id);
	$reg_info = $reg_info[0];

	$args = array();
	$args["table"] = "register_credit_note";
	if ( !empty($register_credit_note_log_id) ) {
		$args["id"] = (int)$cr_info["register_credit_note_id"];
	}else{
		$gen_cr = run_credit_note_no();
		$args["runno"] = $gen_cr["runno_credit_note"];
		$args["docno"] = $gen_cr["credit_note_docno"];
		$args["runyear"] = $gen_cr["runyear"];
	}//end else
	$args["ref_register_id"] = $register_id;
	$args["ref_receipt_docno"] = $reg_info["docno"];
	$args["ref_pay_price"] = $reg_info["pay_price"];
	$args["ref_pay_date"] = $reg_info["pay_date"];
	$args["credit_note_price"] = $_POST["credit_note_price"];
	$args["remark"] = $_POST["remark"];
	$args["reason"] = $_POST["reason"];
	$args["diff_price"] = $args["ref_pay_price"] - $args["credit_note_price"];
	
	$credit_note_date = thai_to_timestamp($_POST["receipt_date"]);
	$credit_note_date_tt = trim($_POST["receipt_date_tt"]);
	$args["credit_note_date"] = $credit_note_date." ".$credit_note_date_tt.":00";

	$args["recby_id"] = $EMPID;
	$args["rectime"] = $datetime_now;

	$ret = $db->set($args);

}//end if

$register_credit_note_id = !empty($cr_info["register_credit_note_id"]) ? $cr_info["register_credit_note_id"] : $ret;

if ( !empty($register_credit_note_id) ) {

	if ( $reg_info["pay_status"]!=9 ) {
		$args = array();
		$args["table"] = "register";
		$args["id"] = $register_id;
		$args["pay_status"] = 9;
		$args["recby_id"] = $EMPID;
		$args["rectime"] = $datetime_now;

		$db->set($args);

		$q = "SELECT pay_status FROM register WHERE register_id={$register_id}";
		$new_pay_status = $db->data($q);
		if ( $new_pay_status==9 ) {
			$this_file = basename($_SERVER["SCRIPT_FILENAME"], '.php');
			register_log_save($register_id, 'update', $this_file);
		}//end if

	}//end if

	$_SESSION["update_register_credit_note"]["alert"]["status"] = "success";
	$_SESSION["update_register_credit_note"]["alert"]["msg"] = "บันทึกข้อมูลเรียบร้อย";
}else{
	$_SESSION["update_register_credit_note"]["alert"]["status"] = "error";
	$_SESSION["update_register_credit_note"]["alert"]["msg"] = "บันทึกข้อมูลไม่สำเร็จ กรุณาติดต่อผู้ดูแลระบบ";
}//end else

$args = array();
$args["p"] = "report";
$args["register_id"] = $register_id;
$args["type"] = "credit-note";
$args["register_credit_note_id"] = $register_credit_note_id;
redirect_url($args);
?>