<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
global $db;
global $SECTIONID;

$error = $_SESSION["error"]["msg"];
unset($_SESSION["error"]["msg"]);
$success = $_SESSION["success"]["msg"];
unset($_SESSION["success"]["msg"]);

$course_id = ($_GET["course_id"]) ? $_GET["course_id"] : $_POST["course_id"];
//$course_id = $_POST["course_id"];

$section = datatype(" and a.active='T'", "section", true);
$coursetype = datatype(" and a.active='T'", "coursetype", true);
$q = "select  title from course where course_id=$course_id";
$r = $db->rows($q);
$str = "";
if($r){
	$str = $r["title"];
}else{
	$str = "เพิ่มหลักสูตร";
}
?>

<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="row">
					<div class="col-md-12">           
					<div class="block-flat">
						<div class="header">              
						<ol class="breadcrumb">
							<li><a href="#" onClick="clearPage('<?php echo $_GET['p']."&type=childlist" ?>');">หน้าหลัก</a></li>
							<li class="active"><?php echo $str; ?></li>
						</ol>
						</div>
						<div class="content">
							<form id="frmMain" name="frmMain" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="update-course-childdetail.php">
							<input type="hidden" name="course_id" id="course_id" value="<?php echo $course_id; ?>">							
								<div class="form-group row" <?php if(!$course_id) echo 'style="display:none;"'; ?>>
										<span class="btn btn-success btn-small pull-right" onclick="addcourse_detail_other();" style="margin-top:10px;"><i class="fa fa-plus"></i> เลือกหลักสูตร</span>
										<span class="btn btn-info btn-small pull-right" onclick="addnew();" style="margin-top:10px;"><i class="fa fa-plus"></i> เพิ่มหลักสูตร</span>
										<br>
										<h4><i class="fa  fa-list"></i> &nbsp;หลักสูตรย่อย</h4>
										<table  class="table table-striped" id="tbMenuList">
								            <thead>
								                <tr class="alert alert-success" style="font-weight:bold;">
												  <th width="10%">ลำดับ</th>
												  <th width="10%">รหัส</th>
												  <th width="40%">ชื่อหลักสูตร</th>
												  <th width="15%">ประเภทใบอนุญาต/คุณวุฒิ</th>
												  <th width="15%">ประเภทหลักสูตร</th>
												  <th width="10%" class="center">แก้ไข</th>
								              </tr>
								                </thead>
								            <tbody>
								                  <tr>
								                <td><input name="ckCourseDetail[]" type="checkbox" checked id="ckCourseDetail" value="T">
								                    <span id="runNo"></span>
								                    <input name="course_child_id[]" type="hidden" id="course_child_id">
								                </td>
												<td class="center" id="code"></td>
												<td  class="center" id="title"></td>
												<td  class="center" id="section_name"></td>
												<td  class="center" id="coursetype_name"></td>													
												<td  class="center" id="edit_info"></td>													
								              </tr>
								                </tbody>
								          </table>
									</div>
								</div>
							</div>
							<div class="clear"></div>
							<div class="form-group row" style="padding-left:10px;">
								<div class="col-sm-12">
									<button type="button" class="btn btn-primary" onClick="ckSave()">Save changes</button>
									<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p']."&type=childlist" ?>');">Cancel</button>
								</div>
							</div>
						</form>
						</div>
					</div>
					
					</div>
				</div>

		</div>
	</div> 
</div>
<?php include_once ('inc/js-script.php'); ?>
<script type="text/javascript">
	$(document).ready(function() {
	 $("#frmMain").validate();
	 var course_id = "<?php echo $_GET["course_id"]?>";
	 if(course_id) viewInfo(course_id);
	 

	 // show succes
	 ///////////////////////////////////////////////
	 var error = "<?php echo $error ?>";
     if(error!=""){
		$.gritter.removeAll({
	        after_close: function(){
	          $.gritter.add({
	          	position: 'center',
		        title: 'Error',
		        text: error,
		        class_name: 'danger'
		      });
	        }
	     });
     }
     var success = "<?php echo $success ?>";
     if(success!=""){
		$.gritter.removeAll({
	        after_close: function(){
	          $.gritter.add({
	          	position: 'center',
		        title: 'success',
		        text: success,
		        class_name: 'success'
		      });
	        }
	    });
	 }
	 /////////////////////////////////////////////


 });
var trMenuList = $("#tbMenuList tbody tr:eq(0)").clone();
delRow("#tbMenuList");
 function viewInfo(course_id){
    if(typeof course_id=="undefined") return;
	delRow("#tbMenuList");
    var url = "data/course-childdetail.php";
	var param = "course_id="+course_id;
	$.ajax( {
		"dataType":'json', 
		"type": "POST", 
		"url": url,
		"data": param, 
		"success": function(data){	
			 $.each(data, function(index, array){
             	var t = addTrLine("#tbMenuList", trMenuList, array, "runNo");
             	$(t).find('#edit_info').html(array.edit_info);    
			 });				 
		}
	});
}

function addcourse_detail_other(){
	var url = "course map childlist=parent"
   courseData(url, "ret_detail_other", 1);	
}

function ret_detail_other(id){
	var course_id = "<?php echo $_GET["course_id"]?>";
	for(var i in id){
		var ck = "";
		var data = id[i];
		data["course_child_id"] = data["course_id"];
       $("#tbMenuList tbody tr :input").each(function(){
             var t = $(this).val();
 				 var ckId = data["course_id"];
 				 if(ckId==t){
 				    ck = "1";
 				 }
 			 });
 	   if(ck=="1"){
 	 	   ck = "";
 	 	   continue;
 	   }
 	   if(data["course_id"]==course_id) continue;
	   var t = addTrLine("#tbMenuList", trMenuList, data, "runNo");
	}
}
function editInfo(id){
	if(typeof id=="undefined") return;
   var url = "index.php?p=<?php echo $_GET["p"];?>&course_id="+id+"&type=info";
   redirect(url);
}

function ckSave(id){
	onCkForm("#frmMain");
	$("#frmMain").submit();
} 

function addnew(){
	//var url = "index.php?p=<?php echo $_GET["p"];?>&type=info";
	var url = "index.php?p=<?php echo $_GET["p"];?>&type=add-child&course_id=<?php echo $course_id;?>";
	window.open(url, "_self");
}
</script>