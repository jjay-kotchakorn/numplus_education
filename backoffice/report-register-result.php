<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
include_once "./share/member.php";
global $db;
global $SECTIONID;
$apay = datatype(" and a.active='T'", "pay_status", true);
$register_result = datatype(" and a.active='T' ", "register_result", true);
$educationlevel = datatype(" and a.active='T'", "educationlevel", true);
$province = datatype(" and a.active='T'", "province", true);
$receipttype = datatype(" and a.active='T'", "receipttype", true);
$university = datatype_university(true);

$error = $_SESSION["error"]["msg"];
unset($_SESSION["error"]["msg"]);
$success = $_SESSION["success"]["msg"];
unset($_SESSION["success"]["msg"]);

$course_id = $_GET["course_id"];
$course_detail_id = $_GET["course_detail_id"];
$section = datatype(" and a.active='T'", "section", true);
$coursetype = datatype(" and a.active='T'", "coursetype", true);
$q = "select  day, date, time , address, address_detail from course_detail where course_detail_id=$course_detail_id";
$r = $db->rows($q);
$str = "";
if($r){
	$str = $r["day"]." ".revert_date($r["date"])." ".$r["time"]." ".$r["address_detail"]." ".$r["address"];
	$info = get_course("", $course_id);
	if($info) $info = $info[0];
	$coursetype_id = $info["coursetype_id"];
	$section_id = $info["section_id"];

}else{
	$str = "เพิ่มหลักสูตร";
}
?>
<link rel="stylesheet" type="text/css" href="js/jquery.nanoscroller/nanoscroller.css" />
<link href="js/jquery.icheck/skins/square/blue.css" rel="stylesheet">

<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="col-sm-12">
				<div class="content block-flat ">
						<div class="header">              
							<ol class="breadcrumb">
								<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>');">หน้าหลัก</a></li>
								<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>&type=select-course');">เลือกหลักสูตร</a></li>
								<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>&course_id=<?php echo $course_id; ?>&type=select-course-detail');">หน้ายอดผู้สมัคร</a></li>
								<li class="active">ดูข้อมูลผลกิจกรรม</li>
							</ol>
						</div>
										
							<div class="form-group row">
								<label class="col-sm-12 control-label">																		
									<a href="#" class="btn pull-right" onclick="reCall();"><i class="fa fa-refresh">&nbsp;</i> Refresh</a>&nbsp;&nbsp;									
								</label>                                            
							</div>
					<form id="frmMain" name="frmMain" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="">
					<table id="tbCourse" class="table" style="width:100%">
						  <thead>
							  <tr>
								  <th style="text-align:center" width="5%">ลำดับ <input type="checkbox" id="checkAll"></th>
								  <th style="text-align:center" width="5%">ที่นั่ง</th>
								  <th width="13%">ชื่อ-นามสกุล</th>
								  <th style="text-align:center" width="30%">หลักสูตร</th>
								  <th style="text-align:center" width="15%">สถานที่</th>
								  <?php if($coursetype_id==3 && $section_id==2) {  ?>	
								  	<th width="10%" style="text-align:center" width="15%">ผลอบรม</th> 
								  	<th width="10%" style="text-align:center" width="15%">ผลสอบ</th>
								  <?php }else{ ?>
								  	<th width="10%" style="text-align:center" width="10%">ผลการเข้าร่วมกิจกรรม</th>
								  <?php } ?>
								  <th style="text-align:center" width="10%">วันที่ลงผลการเข้าร่วมกิจกรรม</th>						  								  							  
							  </tr>
						  </thead>   
						<tbody>
						</tbody>
					</table>
						<div class="clear"></div>
					<?php if($coursetype_id==3 && $section_id==2) { ?>	

						<div style="display:inline-block;margin-left:15px;" class="form-group row"><p>(ผลอบรม)</p>
							<?php foreach ($register_result as $key => $value) {								
								echo '<label class="radio-inline"  style="padding-left:0px;padding-top:0px"> <div aria-disabled="false" aria-checked="false" style="position: relative;" class="iradio_square-blue"><input style="position: absolute; opacity: 0;" value="'.$value["register_result_id"].'" name="register_te_results"  class="icheck required" type="radio"><ins style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;" class="iCheck-helper"></ins></div>&nbsp;&nbsp;'.$value["name"].'</label>';
							} ?>
						</div>					
					<div class="clear"></div>
						<div style="display:inline-block;margin-left:15px;" class="form-group row"><p>(ผลสอบ)</p>
							<?php foreach ($register_result as $key => $value) {								
								echo '<label class="radio-inline"  style="padding-left:0px;padding-top:0px"> <div aria-disabled="false" aria-checked="false" style="position: relative;" class="iradio_square-blue"><input style="position: absolute; opacity: 0;" value="'.$value["register_result_id"].'" name="register_result"  class="icheck required" type="radio"><ins style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;" class="iCheck-helper"></ins></div>&nbsp;&nbsp;'.$value["name"].'</label>';
							} ?>
						</div>
						<div class="clear"></div>
					<?php }else{ ?>
						<div style="display:inline-block;margin-left:15px;" class="form-group row"><p>(ผลการเข้าร่วมกิจกรรม)</p>
							<?php foreach ($register_result as $key => $value) {								
								echo '<label class="radio-inline"  style="padding-left:0px;padding-top:0px"> 
									<div aria-disabled="false" aria-checked="false" style="position: relative;" class="iradio_square-blue">
										<input style="position: absolute; opacity: 0;" value="'.$value["register_result_id"].'" name="register_result"  class="icheck required" type="radio">
										<ins style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;" class="iCheck-helper"></ins>
									</div>&nbsp;&nbsp;'.$value["name"].'</label>';
							} ?>
							<label class="col-sm-2 control-label" style="margin-right: -10px; padding-top: 0px;">เลือกวันที่ในวุฒิบัตร</label>
							<div class="col-sm-3" >
								<input class="form-control" name="cert_date" id="cert_date" 
									value="<?php echo revert_date(date("Y-m-d"));?>" id="date_start" type="text">
							</div> 	
						</div>
						<div class="clear"></div>
					<?php } ?>
					</form>
					<div class="clear"></div>
					<br>
					<div class="filters">     
						<div class="btn-group pull-left">
							<a class="btn btn-small " href="#" onclick="save_result();"><i class="fa fa-save"></i> บันทึกผล</a>
						</div>      
						<div class="btn-group pull-left">
							<a class="btn btn-small " href="#" onclick="import_resutl();"><i class="fa fa-file"></i>Import</a>
						</div>      
						<div class="btn-group pull-left">
							<a class="btn btn-small " href="#" onclick="save_result('clear');"><i class="fa  fa-trash-o"></i> Clear</a>
						</div>
						<div class="btn-group pull-right">
							<a class="btn btn-small " href="#" onclick="sign_report_result_examiner_print();"><i class="fa fa-print"></i> รายงานผลการ<?php echo $info["coursetype_name"] ?>	</a>
						</div>
						<div class="btn-group pull-right">
							<a class="btn btn-small " href="#" onclick="sign_report_result_test_print();"><i class="fa fa-print"></i> รายชื่อผู้เข้า<?php echo $info["coursetype_name"] ?>	ส่ง กลต.</a>
						</div>
						<div class="btn-group pull-right">
							<a class="btn btn-small " href="#" onclick="sign_report_certificate_print();"><i class="fa fa-print"></i> วุฒิบัตร IC,IBA </a>
						</div> 
						<div class="btn-group pull-right">
							<a class="btn btn-small " href="#" onclick="sign_report_test_status_print();"><i class="fa fa-print"></i> รายงานให้อัพเดทสถานะ </a>
						</div>      
					</div>
					<div class="clear"></div>
				</div>
			</div>

		</div>
	</div> 
</div>
<?php include ('inc/js-script.php') ?>

<script type="text/javascript">
$(document).ready(function() {
	var get_type = "<?php echo $_GET["type"]; ?>";
	if(get_type=="childlist" || get_type=="course_order") $("#add").hide();
	var oTable;
	listItem();	
	$("#section_id").change(function(event) {
		var id = $(this).val();
		getDropDown('#coursetype_id', id, 'coursetype', 'data/coursetype.php')
	});
	$("#checkAll").click(function(){
 		$("#tbCourse tbody td :checkbox").prop('checked', $(this).prop("checked"));
	});  
	$("#cert_date").datepicker({language:'th-th',format:'dd-mm-yyyy'});
});

function listItem(){
   var get_type = "<?php echo $_GET["type"]; ?>";
   var url = "data/report-registerlist.php";
   oTable = $("#tbCourse").dataTable({
	   "sDom": 'T<"clear">lfrtip',
	   "oLanguage": {
   	   "sInfoEmpty": "",
   		"sInfoFiltered": ""
						  },
		"oTableTools": {
			"aButtons":  [	
/*			{
				"sExtends": "xls",
				"sButtonText": "Export ข้อมูลผู้มีสิทธิ์"
			}*/
			]
		},
		 "bSort": false ,
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": url,
		"sPaginationType": "full_numbers",
		"aaSorting": [[ 0, "desc" ]],
		'iDisplayLength': 100,
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			aoData.push({"name":"section_id","value":<?php echo (int) $info["section_id"]; ?>});			
			aoData.push({"name":"coursetype_id","value":<?php echo (int) $info["coursetype_id"]; ?>});	
			aoData.push({"name":"course_detail_id","value":<?php echo (int) $course_detail_id; ?>});			
			aoData.push({"name":"course_id","value":<?php echo (int) $course_id; ?>});			
			aoData.push({"name":"status","value":"3,5,6"});			
			aoData.push({"name":"active","value": "T"});			
			aoData.push({"name":"type","value":get_type});	
			$.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});
		}
   }); 
}

function editInfo(id){
	if(typeof id=="undefined") return;
   var url = "index.php?p=<?php echo $_GET["p"];?>&register_id="+id+"&type=info";
   redirect(url);
}


function childlist(id){
	if(typeof id=="undefined") return;
   var url = "index.php?p=<?php echo $_GET["p"];?>&register_id="+id+"&type=childdetail";
   redirect(url);
}

function addnew(){
   var url = "index.php?p=<?php echo $_GET["p"];?>&type=select-course";
   redirect(url);
}

function reCall(){
	oTable.fnClearTable( 0 );
	oTable.fnDraw();
}

function selectMember(){
   var url = "member map";
   memberpop(url,"ret_member_select");	
}

function ret_member_select(id){
	for(var i in id){
		var ck = "";
		var data = id[i];
        $("#member_id").val(data["member_id"]);
        $("#membername").val(data.name_th);
        reCall();
	}
}

function addcourse_detail_other(){
   var url = "course_detail map";
   courseDetailData(url,"ret_detail_other");	
}

function ret_detail_other(id){
	for(var i in id){
		var ck = "";
		var data = id[i];
		$("#course_detail_id").val(data.course_detail_id);
		console.log(id);
		var str = data.date+" เวลา :  "+data.time+" สถานที่ : "+data.address_detail+" "+data.address+ " จำนวนที่นั่ง : "+data.chair_all;
		$("#course_detail_name").val(str);
		reCall();
	}
}

function clearSearch(){
 $("#course_detail_name").val("");
 $("#course_detail_id").val("");
 $("#member_id").val("");
 $("#membername").val("");
 reCall();
}
function add_to_walkin(id){
    var url = "index.php?p=register&course_detail_id="+id+"&type=walk-in&course_id=<?php echo $course_id; ?>";
    window.open(url,'_blank');
}
function add_to_book(id){
	var url = "index.php?p=course-detail&course_detail_id="+id+"&type=detail&flag=detail";
    //var url = "index.php?p=register&course_detail_id="+id+"&type=walk-in&course_id=<?php echo $course_id; ?>";
    window.open(url,'_blank');
}
function printinfo(id){
	var url = "index.php?p=course-detail&course_detail_id="+id+"&type=detail&flag=detail";
    //var url = "index.php?p=register&course_detail_id="+id+"&type=walk-in&course_id=<?php echo $course_id; ?>";
    window.open(url,'_blank');
}
function import_resutl(){
    var url = "index.php?p=<?php echo $_GET["p"];?>&course_detail_id=<?php echo $course_detail_id; ?>&type=register-result-import&course_id=<?php echo $course_id; ?>";
    window.open(url,'_blank');

}
function clear_all_result(){
	var coursetype_id = "<?php echo $coursetype_id ?>";
	var section_id = "<?php echo $section_id; ?>";

}
function save_result(clear){

	if(typeof clear!="undefined"){
		var t = confirm("clear  ผลการเข้าร่วม ? ");
	}else{
		var t = confirm("update  ผลการเข้าร่วม ? ");
	}

	if(!t) return false;

    var i = 0;
    var str = "";
    var result = $("input[name=register_result]:checked").val();
    var te_results = $("input[name=register_te_results]:checked").val();
    var cert_date = $("input[name=cert_date]").val();
    
    if(typeof te_results=="undefined"){
    	var te_results = 0;
    }
    if(typeof clear!="undefined"){
    	var result = 0;
    	var te_results = 0;
    }else if(result=="undefined" && te_results==0){

    	alert('กรุณาเลือกผลกิจกรรม');
    	return;
    }
    $("#tbCourse tbody tr :checkbox").each(function() {
        if ($(this).is(":checked")) {
            var id = $(this).val();
            str += id+",";
        }
        i++;
    });
  if(str!=""){
	$.ajax({
		"type": "POST",
		"async": false, 
		"url": "data/register-result-update.php",
		"data": {'register_id': str, 'result' : result, 'te_results': te_results, 'cert_date': cert_date, 'course_detail_id' : <?php echo (int) $course_detail_id; ?>, 'course_id':<?php echo (int) $course_id; ?>}, 
		"success": function(data){	
			console.log(data);
			$.gritter.removeAll({
		        after_close: function(){
		          $.gritter.add({
		          	position: 'center',
			        title: ' ',
			        text: data,
			        class_name: 'success'
			      });
		        }
		      });
 			reCall();				      							 
		}
	});
  }else{
    
  }

}
function sign_report_result_examiner_print(){
   var url = "report-register-report-exam-print.php?course_detail_id=<?php echo $course_detail_id; ?>&type=info&course_id=<?php echo $course_id; ?>";
   window.open(url,'_blank');
}
function sign_report_test_status_print(){
   var url = "report-register-report-test-status-print.php?course_detail_id=<?php echo $course_detail_id; ?>&type=info&course_id=<?php echo $course_id; ?>";
   window.open(url,'_blank');
}
function sign_report_result_test_print(){
   var url = "report-register-report-test-print.php?course_detail_id=<?php echo $course_detail_id; ?>&type=info&course_id=<?php echo $course_id; ?>";
   window.open(url,'_blank');
}
function sign_report_certificate_print(){

	if(typeof id!="undefined"){

	}else{
	    var id = "";
	    $("#tbCourse tbody tr :checkbox").each(function() {
	        if ($(this).is(":checked")) {
	            var str = $(this).val();

	            id += str+",";
	        }
	    });
	}
	if(id==""){
		alert("ยังไม่ได้เลือกรายการ ? ");
		return;
	}
   	var url = "report-register-report-certificate-print.php?register_id="+id;

   window.open(url,'_blank');
}
</script>