<?php
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";
include_once "./share/member.php";
include_once "./share/course.php";
global $db, $EMPID;
// d($_POST); die();
set_time_limit(0);
$debug = true;
$debug = false;

$datetime_now = date('Y-m-d H:i:s');

$ret = "";
$register_change_receipt_id = "";
if ( $debug ) {
	$register_change_receipt_id = 1;
	$ret = 179793;
}//end if

function update_tb_register_course($register_id="", $new_register_id="", $debug=false){
	if( empty($register_id) || empty($new_register_id) ) return false;
	global $db, $EMPID;
	$datetime_now = date('Y-m-d H:i:s');

	$q = "SELECT
			a.register_course_id,
			a.course_id,
			a.register_id,
			a.active,
			a.recby_id,
			a.rectime,
			a.remark
		FROM register_course a
		WHERE a.active='T' 
			AND a.register_id={$register_id}
	";
	$reg_cos_list = $db->get($q);
	// d($reg_cos_list); die();

	if ( !empty($reg_cos_list) ) {
		foreach ($reg_cos_list as $key => $v) {
			$old_register_course_id = $v["register_course_id"];

			$args = array();
			$args["table"] = "register_course";
			$args["course_id"] = $v["course_id"];
			$args["register_id"] = $new_register_id;
			$args["active"] = 'T';
			$args["remark"] = "inherit from register_course_id={$old_register_course_id}";
			$args["recby_id"] = $EMPID;
			$args["rectime"] = $datetime_now;		
			// var_dump( $db->set($args, true, true));
			$register_course_id = $db->set($args);
/*
			if ( !empty($register_course_id) ) {
				$args = array();
				$args["table"] = "register_course";
				$args["id"] = $old_register_course_id;
				$args["active"] = 'F';
				$args["remark"] = "new receipt";
				$args["recby_id"] = $EMPID;
				$args["rectime"] = $datetime_now;
				$db->set($args);
			}//end if
*/
		}//end loop $v
	}else{

	}//end else


	return true;
}//end func

function update_tb_register_course_detail($register_id="", $new_register_id="", $debug=false){
	if( empty($register_id) || empty($new_register_id) ) return false;
	global $db, $EMPID;
	$datetime_now = date('Y-m-d H:i:s');

	$q = "SELECT
			a.register_course_detail_id,
			a.register_id,
			a.course_id,
			a.course_detail_id,
			a.active,
			a.recby_id,
			a.rectime,
			a.remark,
			a.register_result_id,
			a.result_date,
			a.result_by,
			a.result_from,
			a.register_te_result_id,
			a.te_result_date,
			a.te_result_by,
			a.te_result_from
		FROM register_course_detail a
		WHERE a.active='T' AND register_id={$register_id}
	";
	$reg_cos_dt_list = $db->get($q);
	if ( !empty($reg_cos_dt_list) ) {
		foreach ($reg_cos_dt_list as $key => $v) {
			$old_register_course_dt_id = $v["register_course_detail_id"];

			$args = array();
			$args["table"] = "register_course_detail";
			$args["course_id"] = $v["course_id"];
			$args["course_detail_id"] = $v["course_detail_id"];
			$args["register_id"] = $new_register_id;
			$args["active"] = 'T';
			$args["remark"] = "inherit from register_course_detail_id={$old_register_course_dt_id}";
			$args["recby_id"] = $EMPID;
			$args["rectime"] = $datetime_now;		
			// var_dump( $db->set($args, true, true));
			$register_course_detail_id = $db->set($args);

			if ( !empty($register_course_detail_id) ) {				
				$q = "SELECT
						a.register_result_id,
						a.result_date,
						a.result_by,
						a.result_from,
						a.register_te_result_id,
						a.te_result_date,
						a.te_result_by,
						a.te_result_from
					FROM register_course_detail a
					WHERE a.active='T' AND register_course_detail_id={$old_register_course_dt_id}
				";
				$info = $db->rows($q);

				if ( !empty( $info ) ) {

					$register_result_id = !empty($info["register_result_id"]) ? $info["register_result_id"] : 0;
					$result_date = !empty($v["result_date"]) ? $v["result_date"] : "0000-00-00 00:00:00";
					$result_by = !empty($info["result_by"]) ? $info["result_by"] : 0;
					$result_from = !empty($info["result_from"]) ? $info["result_from"] : "";

					$register_te_result_id = !empty($info["register_te_result_id"]) ? $info["register_te_result_id"] : 0;
					$te_result_date = !empty($v["te_result_date"]) ? $v["te_result_date"] : "0000-00-00 00:00:00";
					$te_result_by = !empty($info["te_result_by"]) ? $info["te_result_by"] : 0;
					$te_result_from = !empty($info["te_result_from"]) ? $info["te_result_from"] : "";


					$args = array();
					$args["table"] = "register_course_detail";
					$args["id"] = $register_course_detail_id;

					$args["register_result_id"] = $register_result_id;
					$args["result_date"] = $result_date;
					$args["result_by"] = $result_by;
					$args["result_from"] = $result_from;

					$args["register_te_result_id"] = $register_te_result_id;
					$args["te_result_date"] = $te_result_date;
					$args["te_result_by"] = $te_result_by;
					$args["te_result_from"] = $te_result_from;
					
					$args["recby_id"] = $EMPID;
					$args["rectime"] = $datetime_now;

					// var_dump( $db->set($args, true, true));
					$db->set($args);
				}//end if


			}//end if

/*
			if ( !empty($register_course_detail_id) ) {
				$args = array();
				$args["table"] = "register_course_detail";
				$args["id"] = $old_register_course_dt_id;
				$args["active"] = 'F';
				$args["remark"] = "new receipt";
				$args["recby_id"] = $EMPID;
				$args["rectime"] = $datetime_now;
				$db->set($args);
			}//end if
*/
		}//end loop $v
	}else{

	}//end else


	return true;
}//end func


if( !empty($_POST) ){

	// d($_POST); //die();
	$register_id = $_POST["register_id"];
	$use_slip = $_POST["use_slip"];

	$new_receipt = array();
	$new_receipt = $_POST;

	unset($new_receipt["register_id"]);
	unset($new_receipt["use_slip"]);
	unset($new_receipt["receipt_title_text"]);
	$pay_date = thai_to_timestamp($new_receipt["receipt_date"]);
	$pay_date_tt = trim($new_receipt["receipt_date_tt"]);
	$new_receipt["pay_date"] = $pay_date." ".$pay_date_tt.":00";
	unset($new_receipt["receipt_date"]);
	unset($new_receipt["remark"]);

	//d($new_receipt); die();

	if ( $use_slip=="new" ) {
		$reg_info = get_register("", $register_id);
		$reg_info = $reg_info[0];
		// d($reg_info); die();
		$coursetype_id = $reg_info["coursetype_id"];
		$section_id = $reg_info["section_id"];

		$args = array();
		$args = $reg_info;
		$args["table"] = "register";
		$args["pay_date"] = $new_receipt["pay_date"]; 
		$args["no"] = (int)$reg_info["no"]; 
		$args["receipttype_id"] = (int)$new_receipt["receipttype_id"]; 
		$args["receipt_id"] = (int)$new_receipt["receipt_id"]; 
		$args["register_by"] = (int)$reg_info["register_by"]; 
		$args["expire_date"] = !empty($reg_info["expire_date"]) ? $reg_info["expire_date"] : '0000-00-00 00:00:00';
		$args["exam_date"] = !empty($reg_info["exam_date"]) ? $reg_info["exam_date"] : '0000-00-00'; 
		$args["pay_yr"] = (int)$reg_info["pay_yr"]; 
		$args["pay_mn"] = (int)$reg_info["pay_mn"]; 
		$args["pay_id"] = (int)$reg_info["pay_id"]; 
		// $args["pay_price"] = (int)$new_receipt["pay_price"]; 
		$args["pay_method"] = (int)$reg_info["pay_method"]; 
		$args["pay_diff"] = (int)$reg_info["pay_diff"]; 
		$args["approve"] = (int)$reg_info["approve"]; 
		$args["approve_by"] = (int)$reg_info["approve_by"]; 
		$args["approve_date"] = !empty($reg_info["approve_date"]) ? $reg_info["approve_date"] : '0000-00-00 00:00:00';		
		$args["register_mod"] = (int)$reg_info["register_mod"]; 
		$args["last_mod"] = (int)$reg_info["last_mod"]; 	
		$args["last_mod_date"] = !empty($reg_info["last_mod_date"]) ? $reg_info["last_mod_date"] : '0000-00-00 00:00:00';	
		$args["result"] = (int)$reg_info["result"]; 	
		$args["result_date"] = !empty($reg_info["result_date"]) ? $reg_info["result_date"] : '0000-00-00 00:00:00';		
		$args["result_by"] = (int)$reg_info["result_by"]; 
		$args["te_results"] = (int)$reg_info["te_results"]; 

		$args["slip_type"] = $new_receipt["slip_type"];
		$args["branch"] = $new_receipt["branch"];
		$args["receipttype_text"] = $new_receipt["receipttype_text"];
		$args["receipt_fname"] = $new_receipt["receipt_fname"];
		$args["receipt_lname"] = $new_receipt["receipt_lname"];
		$args["taxno"] = $new_receipt["taxno"];
		$args["receipt_no"] = $new_receipt["receipt_no"];
		$args["receipt_gno"] = $new_receipt["receipt_gno"]; 
		$args["receipt_moo"] = $new_receipt["receipt_moo"];
		$args["receipt_soi"] = $new_receipt["receipt_soi"];
		$args["receipt_road"] = $new_receipt["receipt_road"];
		$args["receipt_soi"] = $new_receipt["receipt_soi"];
		$args["receipt_province_id"] = (int)$new_receipt["receipt_province_id"];
		$args["receipt_amphur_id"] = (int)$new_receipt["receipt_amphur_id"];
		$args["receipt_district_id"] = (int)$new_receipt["receipt_district_id"];
		$args["receipt_postcode"] = (int)$new_receipt["receipt_postcode"];
		
		$args["pay_status"] = 3; 
		$args["remark"] = $_POST["remark"]; 
		$args["recby_id"] = (int) $EMPID;
		$args["rectime"] = date("Y-m-d H:i:s");

		unset($args["register_id"]);
		unset($args["runyear"]);
		unset($args["runno"]);
		unset($args["doc_prefix"]);
		unset($args["docno"]);
		unset($args["receipt_district_name"]);
		unset($args["receipt_amphur_name"]);
		unset($args["receipt_province_name"]);
		unset($args["receipt_name"]);

		$rundocno_info = rundocno($coursetype_id, $section_id);
		// d($rundocno_info);
		$args = array_merge($args, $rundocno_info);

		// var_dump($db->set($args, true, true));
		// d($args); die();
		if ( !$debug ) {
			$ret = $db->set($args);
			// var_dump($db->set($args, true, true)); die();
			//echo $ret; die();
		}else{
			d($args);
			echo "<hr>";
		}//end else

	}else if ( $use_slip=="old" ) {
		$reg_info = get_register("", $register_id);
		$reg_info = $reg_info[0];
		$coursetype_id = $reg_info["coursetype_id"];
		$section_id = $reg_info["section_id"];

		$remark = (!empty($reg_info["remark"])) ? ", " : "";
		$no = (!empty($reg_info["no"])) ? $reg_info["no"] : 0;
		$receipttype_id = (!empty($reg_info["receipttype_id"])) ? $reg_info["receipttype_id"] : 0;
		$receipt_id = (!empty($reg_info["receipt_id"])) ? $reg_info["receipt_id"] : 0;
		$expire_date = !empty($reg_info["expire_date"]) ? $reg_info["expire_date"] : '0000-00-00 00:00:00';
		$exam_date = !empty($reg_info["exam_date"]) ? $reg_info["exam_date"] : '0000-00-00';
		$approve_date = !empty($reg_info["approve_date"]) ? $reg_info["approve_date"] : '0000-00-00 00:00:00';
		$last_mod_date = !empty($reg_info["last_mod_date"]) ? $reg_info["last_mod_date"] : '0000-00-00 00:00:00';	
		$result_date = !empty($reg_info["result_date"]) ? $reg_info["result_date"] : '0000-00-00 00:00:00';
		$pay_yr = !empty($reg_info["pay_yr"]) ? $reg_info["pay_yr"] : 0;
		$pay_mn = !empty($reg_info["pay_mn"]) ? $reg_info["pay_mn"] : 0;
		$pay_id = !empty($reg_info["pay_id"]) ? $reg_info["pay_id"] : 0;
		$pay_price = !empty($reg_info["pay_price"]) ? $reg_info["pay_price"] : 0;
		$pay_diff = !empty($reg_info["pay_diff"]) ? $reg_info["pay_diff"] : 0;
		$pay_method = !empty($reg_info["pay_method"]) ? $reg_info["pay_method"] : 0;

		$args = array();
		$args = $reg_info;
		$args["table"] = "register";
		$args["pay_date"] = $new_receipt["pay_date"];
		$args["remark"] = $remark.$_POST["remark"];
		$args["no"] = $no;
		$args["receipttype_id"] = $receipttype_id;
		$args["receipt_id"] = $receipt_id;
		$args["expire_date"] = $expire_date;
		$args["exam_date"] = $exam_date;
		$args["pay_method"] = $pay_method;
		$args["approve_date"] = $approve_date;
		$args["last_mod_date"] = $last_mod_date;
		$args["result_date"] = $result_date;	
		$args["pay_yr"] = $pay_yr;
		$args["pay_mn"] = $pay_mn;
		$args["pay_id"] = $pay_id;
		$args["pay_price"] = $pay_price;
		$args["pay_diff"] = $pay_diff;
		$args["pay_status"] = 3;

		$rundocno_info = rundocno($coursetype_id, $section_id);
		// d($rundocno_info);
		$args = array_merge($args, $rundocno_info);


		unset($args["register_id"]);
		// unset($args["runyear"]);
		// unset($args["runno"]);
		// unset($args["doc_prefix"]);
		// unset($args["docno"]);
		unset($args["receipt_district_name"]);
		unset($args["receipt_amphur_name"]);
		unset($args["receipt_province_name"]);
		unset($args["receipt_name"]);
		unset($args["register_by"]);
		unset($args["approve"]);
		unset($args["approve_by"]);
		unset($args["approve_date"]);
		unset($args["register_mod"]);
		unset($args["last_mod"]);
		unset($args["last_mod_date"]);
		unset($args["result"]);
		unset($args["result_date"]);
		unset($args["result_by"]);
		unset($args["te_results"]);

		// d($args); die();


		if ( !$debug ) {
			$ret = $db->set($args);
			// var_dump($db->set($args, true, true)); die();
			// echo $ret; die();
		}else{
			d($args);
			echo "<hr>";
		}//end else

	}//end else if


	if ( !empty($ret) || $debug ) {
		$new_register_id = $ret;
		$q = "SELECT register_credit_note_id 
			FROM register_credit_note 
			WHERE active='T' 
				AND ref_register_id={$register_id}
		";
		$register_credit_note_id = $db->data($q);
		
		$args = array();
		$args["table"] = "register_credit_note";
		$args["id"] = $register_credit_note_id; 
		$args["ref_new_register_id"] = $new_register_id; 
		$args["new_receipt_docno"] = $rundocno_info["docno"]; 
		$args["active"] = $debug ? 'F' : 'T'; 
		$args["recby_id"] = $EMPID; 
		$args["rectime"] = $datetime_now; 
		$args["remark"] = $debug ? 'ON debug mode' : NULL;
		// var_dump($db->set($args, true, true));
		$db->set($args);

		$q = "SELECT ref_new_register_id
			FROM register_credit_note 
			WHERE active='T' 
				AND register_credit_note_id={$register_credit_note_id}
		";
		$ref_new_register_id = $db->data($q);

		if ( !empty($ref_new_register_id) && ($ref_new_register_id==$new_register_id) ) {
			$_SESSION["update_register_receipt_name"]["alert"]["status"] = "success";
			$_SESSION["update_register_receipt_name"]["alert"]["msg"] = "บันทึกข้อมูลเรียบร้อย";
		}else{
			$_SESSION["update_register_receipt_name"]["alert"]["status"] = "error";
			$_SESSION["update_register_receipt_name"]["alert"]["msg"] = "บันทึกข้อมูลไม่สำเร็จ กรุณาติดต่อผู้ดูแลระบบ";
		}//end else

		$rs_update_register_course = update_tb_register_course($register_id, $new_register_id, $debug);
		$rs_update_register_course_detail = update_tb_register_course_detail($register_id, $new_register_id, $debug);


	}//end if

	// die();

	// $db->begin();
/*	
    $args = array();
	if($register_id){
		$args['id'] = $register_id;
	}
	$args["active"] = $_POST["active"];
	$args["recby_id"] = (int) $EMPID;
	$args["rectime"] = date("Y-m-d H:i:s");
		// At last.. a query!
*/
	// $ret = $db->set($args);
	// $ret = ($id) ? $id : $ret; 
	// $db->commit();
	
}//end if

$args = array();
$args["p"] = "report";
// $args["register_id"] = $register_id;
$args["type"] = "register-receipt";
// $args["register_credit_note_id"] = $register_credit_note_id;
redirect_url($args);
?>