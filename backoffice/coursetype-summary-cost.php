<?php
include_once "./share/authen.php";
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
global $db, $SECTIONID, $COURSETYPEID, $EMPID;
$apay = datatype(" and a.active='T'", "pay_status", true);
$con_section_id = ($SECTIONID>0) ? " and a.section_id={$SECTIONID}" : "";
$con_coursetype_id = ($COURSETYPEID>0) ? " and a.coursetype_id={$COURSETYPEID}" : "";
$section = datatype(" and a.active='T' {$con_section_id}", "section", true);
$coursetype = datatype(" and a.active='T' {$con_coursetype_id}", "coursetype", true);
$educationlevel = datatype(" and a.active='T'", "educationlevel", true);
$province = datatype(" and a.active='T'", "province", true);
$receipttype = datatype(" and a.active='T'", "receipttype", true);
$university = datatype_university(true);


?>
<link rel="stylesheet" type="text/css" href="js/jquery.nanoscroller/nanoscroller.css" />
<link href="js/jquery.icheck/skins/square/blue.css" rel="stylesheet">

<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="col-sm-12">
				<div class="content block-flat ">
						<div class="header">              
							<h3><i class="fa fa-list"></i> &nbsp; สรุปรายการสอบและอบรม</h3>	
						</div>
  										
						<div class="header">
							<div class="form-group row">
								<label class="col-sm-1 control-label">ประเภทใบอนุญาต/คุณวุฒิ</label>
								<div class="col-sm-2">
									<select name="section_id" id="section_id" class="form-control">
										<option value="">---- เลือก ----</option>
										<?php foreach ($section as $key => $value) {
											$id = $value['section_id'];
											if($SECTIONID>0 && $SECTIONID!=$id) continue;
											if($SECTIONID>0 && $SECTIONID==$id){
												$select = "selected";
											}else{
												$select = "";
											}
											$name = $value['name'];
											echo  "<option {$select} value='$id'>$name</option>";
										} ?>
									</select>
								</div> 
								<label class="col-sm-1 control-label">ประเภทหลักสูตร</label>
								<div class="col-sm-2">
									<select name="coursetype_id" id="coursetype_id" class="form-control">
										<option value="">---- เลือก ----</option>
										<?php foreach ($coursetype as $key => $value) {
											$id = $value['coursetype_id'];
											if($COURSETYPEID>0 && $COURSETYPEID==$id){
												$select = "selected";
											}else{
												$select = "";
											}
											$name = $value['name'];
											echo  "<option {$select} value='$id'>$name</option>";
										} ?>

									</select>
								</div>

 								<label class="col-sm-1 control-label"  style="margin-bottom:0px;">รหัสโครงการ</label>
								<div class="col-sm-2">
									<input class="form-control" name="code_project" id="code_project" placeholder="รหัสโครงการ" type="text">
								</div>


								<div class="btn-group pull-righ" style="padding-left: 186px">
									<a class="btn btn-small btn-warning" href="#" onclick="btn_clear();"><i class="fa fa-refresh"></i>&nbsp;Clear</a>
								</div>

							</div> 							
						
							<div class="form-group row">
                                <label class="col-sm-1 control-label" style=" padding-right:0px;">ช่องทางชำระ</label>
								<div class="col-sm-2">
									<select name="pay" id="pay" class="form-control">
										<option selected="selected" value="">---- เลือก ----</option>
										<option value="kbank">KBANK</option>
										<option value="bbl">BBL</option>
										<option value="money">เงินสด</option>
										<option value="paysbuy">paysbuy</option>
										<option value="mpay">mPAY</option>
									</select>
								</div>   

								<label class="col-sm-1 control-label" style=" padding-right:0px;">วันที่ชำระ</label>
								<div class="col-sm-1"  style="padding-left:0px; padding-right:0px;">
									<input class="form-control" name="date_start" id="date_start" placeholder="เลือกวันที่" type="text">
								</div>                                          
								<label class="col-sm-1 control-label" style=" padding-right:0px;"> ถึงวันที่ </label>
								<div class="col-sm-1"  style="padding-left:0px; padding-right:0px; margin-right: 100px">
									<input class="form-control" name="date_stop" id="date_stop" placeholder="เลือกวันที่" type="text">
								</div> 
                                 
								<div class="col-sm-2" style="padding-left:0px; padding-right:0px; margin-right: 55px">
									<select name="source_data" id="source_data" class="form-control">
										<!-- <option selected="selected" value="bank">ดูข้อมูลจาก ไฟล์ธนาคาร</option>
										<option value="wr">ดูข้อมูลจาก ระบบ WR</option> -->
										<option selected="selected" value="wr">ดูข้อมูลจาก ระบบ WR</option>
										<option value="bank">ดูข้อมูลจาก ไฟล์ธนาคาร</option>
									</select>
								</div>   

								<div class="btn-group pull-righ">
									<a class="btn btn-small btn-primary" href="#" onclick="report();"><i class="fa fa-book"></i>&nbsp;ออกรายงาน</a>
								</div>   

							</div>							
						</div>
					
					<div class="clear"></div>
					<br>
					<div class="filters">       
<!-- 
						<div class="btn-group pull-center">
							<a class="btn btn-small " href="#" onclick="report();"><i class="fa fa-book"></i> ออกรายงาน</a>
						</div>          
 -->
					</div>
					<div class="clear"></div>
				</div>
			</div>

		</div>
	</div> 
</div>
<form id="print_form" action="" method="post">
	<input type="hidden" id="register_ids" name="register_id">;		
</form>
<?php include ('inc/js-script.php') ?>

<script type="text/javascript">
$(document).ready(function() {
	// $("#frmMain").validate();
	$("#date_start").datepicker({language:'th-th',format:'dd-mm-yyyy'});
	$("#date_stop").datepicker({language:'th-th',format:'dd-mm-yyyy'});

	$("#source_data").hide();
	$( "#pay" ).change(function() {
		var pay = $("#pay").val();
	  	// alert( pay );
	  	if (pay=='kbank' || pay=='bbl') {
	  		$("#source_data").show();
	  	}else{
	  		$("#source_data").hide();
	  	}//end else
	});//end onchg

});

function report(){
	var date_start = $("#date_start").val();
	var date_stop = $("#date_stop").val();
	var coursetype_id = $("#coursetype_id").val();
	var section_id = $("#section_id").val();
	var pay = $("#pay").val();
	var code_project = $("#code_project").val();
	// alert(date_start+' => '+date_stop);

	//if ( date_start && date_stop && coursetype_id && coursetype_id!="" && pay && section_id && section_id!="" ) {
	if ( date_start && date_stop && pay ) {	
		var source_data = $("#source_data").val();
		// alert(source_data);

		if ( (pay=='kbank' || pay=='bbl') && source_data=='wr' ) {
			var url = "report.php?type=coursetype-summary-cost-rpt-wr&date_start="+date_start+"&date_stop="+date_stop+"&pay="+pay+"&coursetype_id="+coursetype_id+"&section_id="+section_id+"&code_project="+code_project;

		}else if (pay=='mpay') {
			var url = "report.php?type=coursetype-summary-cost-rpt-mpay&date_start="+date_start+"&date_stop="+date_stop+"&pay="+pay+"&coursetype_id="+coursetype_id+"&section_id="+section_id+"&code_project="+code_project;
		}else{
			var url = "report.php?type=coursetype-summary-cost-rpt&date_start="+date_start+"&date_stop="+date_stop+"&pay="+pay+"&coursetype_id="+coursetype_id+"&section_id="+section_id+"&code_project="+code_project;
		}//end else
		// console.log(url);
		window.open(url, '_blank');
	}else{
		alert("กรุณาเลือกช่องทางชำระและช่วงวันที่");
	}//end else

}

function btn_clear(){
	// alert("55");
	$("#date_start").val('');
	$("#date_stop").val('');
	$("#coursetype_id").val('');
	$("#pay").val('');
	$("#section_id").val('');
	$("#code_project").val('');
}

</script>