<?php
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
include_once "./share/member.php";
require "./elerning/vendor/autoload.php";
global $db, $SECTIONID, $COURSETYPEID;

use \Curl\Curl;

$apay = datatype(" and a.active='T'", "pay_status", true);
$con_section_id = ($SECTIONID>0) ? " and a.section_id={$SECTIONID}" : "";
$con_coursetype_id = ($COURSETYPEID>0) ? " and a.coursetype_id={$COURSETYPEID}" : "";
$section = datatype(" and a.active='T' {$con_section_id}", "section", true);
if($con_coursetype_id)
	$coursetype = datatype(" and a.active='T' {$con_coursetype_id}", "coursetype", true);

$educationlevel = datatype(" and a.active='T'", "educationlevel", true);
$province = datatype(" and a.active='T'", "province", true);
$receipttype = datatype(" and a.active='T'", "receipttype", true);
$university = datatype_university(true);

$error = $_SESSION["error"]["msg"];
unset($_SESSION["error"]["msg"]);
$success = $_SESSION["success"]["msg"];
unset($_SESSION["success"]["msg"]);

?>
<link rel="stylesheet" type="text/css" href="./js/jquery.nanoscroller/nanoscroller.css" />
<link href="./js/jquery.icheck/skins/square/blue.css" rel="stylesheet">
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="col-sm-12">
				<div class="content block-flat ">
					<div class="page-head">
						<h3><i class="fa fa-list"></i> &nbsp; ลงทะเบียนเรียน E-learning</h3>
					</div>
					<div class="header">              
						<ol class="breadcrumb">
							<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>');">หน้าหลัก</a></li>
							<li class="active">หน้าใบเสร็จรับเงิน</li>
						</ol>
					</div>
<?php  
/*
$a = ",1057:5658,1058:5659,1059:5660,1060:5661";
// $a = "5661";

$rs = strpos($a, ":") ? "yes" : "no";
// $rs =  strpos($a, ":");
var_dump($rs);
 */
?>
					<div class="header">
						<div class="form-group row">
							<label class="col-sm-2 control-label">ประเภทใบอนุญาต/คุณวุฒิ</label>
							<div class="col-sm-3">
								<select name="section_id" id="section_id" class="form-control" onchange="reCall();">
									<option value="">---- เลือก ----</option>
									<?php foreach ($section as $key => $value) {
										$id = $value['section_id'];
										if($SECTIONID>0 && $SECTIONID!=$id) continue;
										if($SECTIONID>0 && $SECTIONID==$id){
											$select = "selected";
										}else{
											$select = "";
										}
										$name = $value['name'];
										echo  "<option {$select} value='$id'>$name</option>";
									} ?>

								</select>
							</div>                
							<label class="col-sm-2 control-label">ประเภทหลักสูตร</label>
							<div class="col-sm-2">
								<select name="coursetype_id" id="coursetype_id" class="form-control" onchange="reCall();">
									<option value="">---- เลือก ----</option>
									<?php foreach ($coursetype as $key => $value) {
										$id = $value['coursetype_id'];
										if($COURSETYPEID>0 && $COURSETYPEID==$id){
											$select = "selected";
										}else{
											$select = "";
										}
										$name = $value['name'];
										echo  "<option {$select} value='$id'>$name</option>";
									} ?>

								</select>
							</div>
							<label class="col-sm-1 control-label" style=" padding-right:0px;">ช่องทางชำระ</label>
							<div class="col-sm-2">
								<select name="sPay" id="sPay" class="form-control" onchange="reCall();">
									<option selected="selected" value="">แสดงทั้งหมด</option>
									<option value="paysbuy">paysbuy</option>
									<option value="bill_payment">Bill-payment</option>
									<option value="at_asco">เช็ค/เงินโอน</option>
									<option value="at_ati">ชำระเงินสดผ่าน ATI</option>
									<option value="mpay">mPAY</option>
								</select>
							</div>                                          
						</div> 							
						<div class="form-group row">
							<label class="col-sm-1 control-label">เวลา/สถานที่</label>
							<div class="col-sm-5">
								<div class="input-group">
									<input class="form-control" type="text" id="course_detail_name" readonly="true">
									<input name="course_detail_id" type="hidden" id="course_detail_id" value="" />
									<span class="input-group-btn">
										<button class="btn btn-primary" type="button" onclick="addcourse_detail_other();">...</button>
									</span>
								</div>				
							</div>
							<label class="col-sm-1 control-label">ชื่อ-นามสกุล</label>
							<div class="col-sm-3">
								<div class="input-group">
									<input type="text" class="form-control" id="membername" name="membername" readonly="true" value="">
									<input name="member_id" type="hidden" id="member_id" value="" />
									<span class="input-group-btn">
										<button class="btn btn-primary" type="button" onClick="selectMember();">...</button>
									</span>
								</div>
							</div>
							<div class="col-sm-1">
								<label class="col-sm-1 control-label">
									<a onclick="clearSearch();" id="ok" class="btn btn-warning" 
										type="button"><i class="fa fa fa-exchange">&nbsp;</i>&nbsp;Clear</a>
								</label> 
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-1 control-label"  style=" padding-right:0px;">วันที่ชำระ</label>
							<div class="col-sm-2"  style="padding-left:0px; padding-right:0px;">
								<input class="form-control" name="date_start" id="date_start" onblur="reCall();" placeholder="วันที่ชำระ" type="text">
							</div>                                          
							<label class="col-sm-1 control-label"  style=" padding-right:0px;"> ถึงวันที่ </label>
							<div class="col-sm-3"  style="padding-left:0px; padding-right:0px;">
								<input class="form-control" name="date_stop" id="date_stop" onblur="reCall();" placeholder=" ถึงวันที่ " type="text">
							</div> 
							<label class="col-sm-1 control-label">
								<a href="#" class="btn" onclick="reCall();"><i class="fa fa-search">&nbsp;</i></a>&nbsp;
								<!-- <button onclick="clearSearch();" id="ok" class="btn btn-success" type="button">Clear</button> -->
							</label>  
<!-- 							
       <label class="col-sm-1 control-label"  style=" padding-right:0px;"> สถานะใบเสร็จ </label>
       <div class="col-sm-3">
       	<select name="rc_status" id="rc_status" class="form-control" onchange="">
       		<option selected="selected"  value="success">ใบเสร็จ ( ใช้งาน )</option>
       		<option value="cancel">ใบเสร็จ ( ที่ถูกคืนเงิน )</option>
       	</select>
       </div> 
                                     -->       
						</div>
						
					</div>

					<form id="frmMain" name="frmMain" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="">
					<table id="tbCourse" class="table" style="width:100%">
						  <thead>
							  <tr>
								  <th style="text-align:center" width="2%">ลำดับ 
								  <!-- <input type="checkbox" id="checkAll" style="float:right;"> -->
								  </th>
								  <th style="text-align:center" width="2%">ID</th>
								  <!-- <th style="text-align:left" width="10%">รหัสโครงการ</th> -->
								  <th style="text-align:center" width="8%">วันที่สมัคร</th>
								  <!-- <th style="text-align:center" width="12%">วันที่หมดเขต</th> -->
								  <th style="text-align:center" width="7%">รหัสประจำตัวประชาชน</th>
								  <th style="text-align:center" width="13%">ชื่อ-นามสกุล</th>
								  <th style="text-align:center" width="7%">วันที่</th>
								  <th style="text-align:center" width="6%">เวลา</th>				  
								  <th style="text-align:center" width="12%">สถานที่</th>				  
								  <th style="text-align:center" width="12%">หลักสูตร</th>				  
								  <th style="text-align:center" width="8%">ประเภทการชำระเงิน</th>				  
								  <!-- <th style="text-align:center" width="6%">แก้ไข</th> -->				  
								  <th style="text-align:center" width="5%">ส่งข้อมูล</th>								  								  							  
							  </tr>
						  </thead>   
						<tbody>
						</tbody>
					</table>
					</form>

					<div class="clear"></div>
					<br>
					<div class="filters">     
						
					</div>
					<div class="clear"></div>
				</div>
			</div>

		</div>
	</div> 
</div>

<form id="print_form" action="" method="post">
	<input type="hidden" id="register_ids" name="register_id">;		
</form>
<?php include ('inc/js-script.php') ?>

<script type="text/javascript">
$(document).ready(function() {
	var get_type = "<?php echo $_GET["type"]; ?>";
	if(get_type=="childlist" || get_type=="course_order") $("#add").hide();
	var oTable;
	listItem();	
    $("#date_start").datepicker({language:'th-th',format:'dd-mm-yyyy'});
    $("#date_stop").datepicker({language:'th-th',format:'dd-mm-yyyy'});    
	$("#section_id").change(function(event) {
		var id = $(this).val();
		getDropDown('#coursetype_id', id, 'coursetype', 'data/coursetype.php')
	});

	$("#rc_status").change(function(event) {
		reCall();
	});

});
$("#checkAll").click(function(){
 		$("#tbCourse tbody td :checkbox").prop('checked', $(this).prop("checked"));
	}); 
function listItem(){
   var get_type = "<?php echo $_GET["type"]; ?>";
   var url = "data/elearning-register-list.php";
   oTable = $("#tbCourse").dataTable({
	   "sDom": 'T<"clear">lfrtip',
	   "oLanguage": {
   	   "sInfoEmpty": "",
   		"sInfoFiltered": ""
						  },
		"oTableTools": {
			"aButtons":  [	
/*			{
				"sExtends": "xls",
				"sButtonText": "Export ข้อมูลผู้มีสิทธิ์"
			}*/
			]
		},
		 "bSort": false ,
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": url,
		"sPaginationType": "full_numbers",
		"aaSorting": [[ 0, "desc" ]],
		'iDisplayLength': 50,
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			aoData.push({"name":"section_id","value":$("#section_id").val()});			
			aoData.push({"name":"coursetype_id","value":$("#coursetype_id").val()});			
			aoData.push({"name":"member_id","value":$("#member_id").val()});			
			aoData.push({"name":"course_detail_id","value":$("#course_detail_id").val()});						
			aoData.push({"name":"date_start","value":$("#date_start").val()});			
			aoData.push({"name":"date_stop","value":$("#date_stop").val()});			
			//aoData.push({"name":"status","value":"3,5,6"});			
			aoData.push({"name":"pay","value":$("#sPay").val()});			
			//aoData.push({"name":"active","value": "T"});			
			aoData.push({"name":"type","value":"register-receipt"});
			aoData.push({"name":"rc_status","value":$("#rc_status").val()});		
			$.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});
			//console.log(aoData);
		}
   });
   /*$('#tbCourse_filter input').unbind();
   $('#tbCourse_filter input').bind('keyup', function(e) {
       if(e.keyCode == 13) {
        oTable.fnFilter(this.value);  
        console.log(this.value); 
    }
   }); */
}



function reCall(){
	oTable.fnClearTable( 0 );
	oTable.fnDraw();
}

function selectMember(){
   var url = "member map";
   memberpop(url,"ret_member_select");	
}

function ret_member_select(id){
	for(var i in id){
		var ck = "";
		var data = id[i];
        $("#member_id").val(data["member_id"]);
        $("#membername").val(data.name_th);
        reCall();
	}
}

function addcourse_detail_other(){
   var url = "course_detail map";
   courseDetailData(url,"ret_detail_other");	
}

function ret_detail_other(id){
	for(var i in id){
		var ck = "";
		var data = id[i];
		$("#course_detail_id").val(data.course_detail_id);
		console.log(id);
		var str = data.date+" เวลา :  "+data.time+" สถานที่ : "+data.address_detail+" "+data.address+ " จำนวนที่นั่ง : "+data.chair_all;
		$("#course_detail_name").val(str);
		reCall();
	}
}

function clearSearch(){
 $("#course_detail_name").val("");
 $("#course_detail_id").val("");
 $("#member_id").val("");
 $("#membername").val("");
 $("#section_id").val("");
 $("#coursetype_id").val("");
 $("#sPay").val("");
 $("#date_start").val("");
 $("#date_stop").val("");
 $("#rc_status").val("success");
 reCall();
}


function printInfo(id){
	if(typeof id!="undefined"){

	}else{
	    var id = "";
	    $("#tbCourse tbody tr :checkbox").each(function() {
	        if ($(this).is(":checked")) {
	            var str = $(this).val();
	            id += str+",";
	        }
	    });
	}
	if(id==""){
		alert("ยังไม่ได้เลือกรายการ ? ");
		return;
	}
   	var url = "receipt-print.php";
   	$("#register_ids").val(id);
   	$("#print_form").attr("action", url);
   	$("#print_form").submit();
   	return;
   // window.open(url,'_blank');
}


//send data to receipt-tax.php
function printtax(){
	var sec_id = $("#section_id").val();
	var cos_id = $("#coursetype_id").val();
	var pay_by = $("#sPay").val();
	var cos_dtail = $("#course_detail_name").val();
	var name = $("#membername").val();
	var date_start = $("#date_start").val();
	var date_stop = $("#date_stop").val();

   if ( (date_start == '' || date_start == null) || (date_stop == '' || date_stop == null) ) {
   	alert("กรุณาเลือกช่วงเวลา"); 
	location.href = "index.php?p=report&type=register-receipt#";
   }else{
   	var url = "receipt-tax.php?sec_id="+sec_id+"&cos_id="+cos_id+"&pay_by="+pay_by+"&cos_dtail="+cos_dtail+"&name="+name+"&date_start="+date_start+"&date_stop="+date_stop;
   	window.open(url,'_blank');
   };
}

function to_elerning(register_id){
/*
	$.ajax({
		url: 'elerning-site-ati-ctrl.php',
		type: 'POST',
		// dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
		data: {register_id: register_id, type: 'register', username: 'ati'},
	})
*/
	$.ajax({
		url: 'check-course-elearning.php',
		type: 'POST',
		// dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
		data: {register_id: register_id, pay_status: 3},
		success: function(data) {
	         // return data; 
	         // alert(data);
	         if ( data=='777' ) {
	         	$.gritter.removeAll({
			        after_close: function(){
			          $.gritter.add({
			          	position: 'center',
				        title: 'Error',
				        text: 'สมัครเรียน E-Lerning ไม่สำเร็จ (ไม่มีการบันทึกรหัสหลักสูตรของระบบ E-Learning กรุณาตรวจสอบ)',
				        class_name: 'danger'
				      });
			        }
			     });
	         }else{
	         	$.gritter.removeAll({
			        after_close: function(){
			          $.gritter.add({
			          	position: 'center',
				        title: 'success',
				        text: 'สมัครเรียน E-Lerning สำเร็จ',
				        class_name: 'success'
				      });
			        }
			    });
	         }//end else
	      }//end success
	})
/*
	.done(function() {
		console.log("success");
		$.gritter.removeAll({
	        after_close: function(){
	          $.gritter.add({
	          	position: 'center',
		        title: 'success',
		        text: 'สมัครเรียน E-Lerning สำเร็จ',
		        class_name: 'success'
		      });
	        }
	    });
	})
	.fail(function() {
		console.log("error");
		$.gritter.removeAll({
	        after_close: function(){
	          $.gritter.add({
	          	position: 'center',
		        title: 'Error',
		        text: 'สมัครเรียน E-Lerning ไม่สำเร็จ',
		        class_name: 'danger'
		      });
	        }
	     });
	})
	.always(function() {
		console.log("complete");
	});
	*/
}//end func


</script>