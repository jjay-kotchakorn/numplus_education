<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
global $db;
?>
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<h3 class="text-center">Content Dashboard</h3>
			    <div class="box-content">
			      <div class="alert alert-block ">
			        <button onClick="javascript:parent.$.fancybox.close();" class="close" type="button">×</button>
			        <span style="margin-left:55px;">รหัสพนักงาน</span>&nbsp;
			        <input class="input-xsmall focused" id="srchCode" onKeyUp="disp();" style="width:150px;" type="text" value="">
			        &nbsp;&nbsp;ชื่อ-นามสกุล&nbsp;
			        <input class="input-xsmall focused" id="srchName" onKeyUp="disp();" style="width:150px;" type="text" value="">
			        <br>
			        &nbsp;รหัสประจำตัวประชาชน&nbsp;
			        <input class="input-xsmall focused" id="srchCardId" onKeyUp="disp();" style="width:150px;" type="text" value="">
			        &nbsp;
			        Search All&nbsp;&nbsp;&nbsp;
			        <input type="checkbox" id="ck">
			        <a href="#" class="btn" onClick="disp();"><i class="icon-search">&nbsp;</i></a>&nbsp;&nbsp;
			        <button onClick="getKey();" id="ok" class="btn btn-success" type="button" title="คลิกเพื่อเลือกรายการ">เลือก/OK</button>
			      </div>
			      <table width="100%" class="table">
			        <thead>
			          <tr class="alert alert-success" style="font-weight:bold;">
			            <td width="8%"><input data-no-uniform="true" type="checkbox" onClick="ckAll(this)" id="ckAll">
			              &nbsp;</td>
			            <td width="16%" class="center">รหัสพนักงาน</td>
			            <td width="30%" class="center">ชื่อ-นามสกุล</td>
			            <td width="24%" class="center">รหัสประจำตัวประชาชน</td>
			            <td width="12%" class="center">สถานะ</td>
			          </tr>
			        </thead>
			      </table>
			      <div style="overflow-y:scroll; height:255px; vertical-align:top; margin-top:-18px;">
			        <table class="table table-striped" id="tbList">
			          <thead>
			            <tr style="cursor:pointer;" onClick="singleKey(this);">
			              <td width="5%"><input type="hidden" name="dataTypeId" id="dataTypeId">
			                &nbsp;</td>
			              <td  width="18%" class="center">-<span id="code"></span></td>
			              <td   class="center"><span id="name"></span></td>
			              <td  width="10%" class="center"><span id="cardId"></span></td>
			              <td width="10%" class="center"><span id="active"></span></td>
			            </tr>
			          </thead>
			          <tbody>
			            <tr style="cursor:pointer;" onClick="singleKey(this);">
			              <td width="8%"><input type="hidden" name="dataTypeId" id="dataTypeId">
			                &nbsp;</td>
			              <td  width="16%" class="center"><span id="code"></span></td>
			              <td  width="30%" class="center"><span id="name"></span></td>
			              <td  width="24%" class="center"><span id="cardId"></span></td>
			              <td width="12%" class="center"><span id="active"></span></td>
			            </tr>
			            <tr style="cursor:pointer;">
			              <td width="8%"><input type="hidden" name="dataTypeId" id="dataTypeId">
			                <input type="checkbox" id="ckBox">
			                &nbsp;&nbsp;</td>
			              <td width="13%" class="center"><span id="code"></span></td>
			              <td width="30%" class="center"><span id="name"></span></td>
			              <td  width="24%" class="center"><span id="cardId"></span></td>
			              <td width="15%" class="center"><span id="active"></span></td>
			            </tr>
			          </tbody>
			        </table>
			      </div>
			    </div>
		<?php include ('inc/js-script.php') ?>
		<script type="text/javascript">
			var checkBox = "<?=$checkBox?>";
			if(checkBox=="T"){
			   var trList = $("#tbList tbody tr:eq(1)").clone();
			}else{
				$("#ok").hide();
			   var trList = $("#tbList tbody tr:eq(0)").clone();
				$("#ckAll").hide();
			}

			(checkBox=="T") ? delCkRow("#tbList","1") : delRow("#tbList");

			$(document).ready(function(){
			   disp();
			});

			function disp(){
			   (checkBox=="T") ? delCkRow("#tbList") : delRow("#tbList");
			   var url = "data/empSearch.php";
				var param = "name="+$("#srchName").val();
				param = param+"&code="+$("#srchCode").val();
				param = param+"&cardId="+$("#srchCardId").val();

				
			   if($("#ck").is(":checked")){
			      param = param+"&all=T";
				}
				$.ajax( {
					"dataType":'json', 
					"type": "POST",
					"async" : false,  
					"url": url,
					"data": param, 
					"success": function(data){	
			             (checkBox=="T") ? delCkRow("#tbList") : delRow("#tbList");      
						 $.each(data, function(index, array){
							      var ck = "";
			                  $("#tbList tbody tr :input").each(function(){
			                     var t = $(this).val();
										 var ckId = array.dataTypeId;
										 if(ckId==t){
										    ck = "1";
										 }
									 });
								 if(ck=="1") return;
			               addTrLine("#tbList", trList, array, "runNo");
						 });				 
					}
				});
			}

			function getKey(){
				   var ret = new Array();
				   var i = 0;
				   $("#tbList tbody tr :checkbox").each(function(){
						if($(this).is(":checked")){
			            var tag =  $(this).parent().parent();
			            ret[i] = new Array();
						ret[i]["id"] = $(tag).find('#dataTypeId').val();
						ret[i]["code"] = $(tag).find('#code').text();
						ret[i]["name"] = $(tag).find('#name').text();
						i++;								    	   
					}
				  });
			    parent.<?php echo $retFunc;?>(ret); 
			    parent.$.fancybox.close();
			}

			function singleKey(tag){
			    var ret = new Array();
				ret["id"] = $(tag).find('#dataTypeId').val();
				ret["code"] = $(tag).find('#code').text();
				ret["name"] = $(tag).find('#name').text();
				ret["cardId"] = $(tag).find('#cardId').text();
				parent.<?php echo $retFunc;?>(ret); 
			    parent.$.fancybox.close();
			}


			function ckAll(tag){
			   $("#tbList tbody tr :checkbox").each(function(){
					if($(tag).is(":checked")){
						$(this).attr('checked', true);  
					}else{
					   $(this).attr('checked', false);  	
					}
			   });
			}
			</script>
		</div>
	</div> 
</div>
