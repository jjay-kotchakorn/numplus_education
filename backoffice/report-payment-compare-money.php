<?php
include_once "./share/authen.php";
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/course.php";
include_once "./share/datatype.php";
global $db, $EMPID;

$date_now = date('Y-m-d H:i:s');
$date = date('Y-m-d');
// var_dump($_GET);
$pay_date = $_GET["pay_date_chk"];
$pay_date_chk = thai_to_timestamp($pay_date);
$pay_type_chk = $_GET["pay_type_chk"];
$pay_type_txt = ($pay_type_chk=='money') ? 'เงินสด' : '';
$sql = "SELECT r.register_id
			, r.docno
			, r.course_id
			, r.pay_price
			, CONCAT(r.title, '',r.fname, ' ',r.lname) AS full_name
			, r.pay_date
			, b.register_course_detail_course_detail_id
			, b.course_detail_id
		FROM register AS r inner join view_register_course_detail b on r.register_id=b.view_register_course_detail_id
		WHERE r.active='T' AND r.auto_del='F' AND r.pay_status IN (3) 
			AND (r.pay='walkin' or r.pay='importfile' or r.pay='at_ati')
			AND r.pay_date LIKE '{$pay_date_chk}%'
		ORDER BY r.pay_date DESC";	
$rs = $db->get($sql);

// echo $sql;
// d($rs);

?>


<!DOCTYPE html>
<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ผลการตรวจข้อมูลการชำระเงิน</title>
<link href="css/printform-style.css" rel="stylesheet" type="text/css" media="all" />
<style>
	@media print{
		body{
			padding: 0px;
			margin: 0px;
		}
		#btPrint{
			display: none;
		}
	}
	.rpt-print{
		table-layout: fixed;
		/*width: 1199;*/
		/*width: 100%;*/
		word-wrap:break-word;
	}
</style>
<!-- <body> -->
<body onload="receipt_tax_export()">
<!-- <body onload=""> -->
	<!-- <div class="form-portrait"> -->
	<div class="form-landscape">
		<div>
			<table class="no-border">
				<tbody>
					<tr>
						<td><span class="center font-weight-bold"><?php echo "ผลการตรวจข้อมูลการชำระเงิน";?></span></td>
					</tr>
					<tr>
						<td><span class="center"><?php echo "ช่องทางการชำระ : {$pay_type_txt} / วันที่ชำระ : {$pay_date}";?></span></td>
					</tr>
				</tbody>
			</table>

			<table class="td-center rpt-print" width=1200 style='table-layout:fixed'>
				<col width=20>

				<col width=30>
				<col width=25>
				<col width=80>
				<col width=180>
				<col width=80>
				<col width=40>


				<thead>
					<tr>
						<td ><span class="center">ลำดับ</span></td>					
						<td ><span class="center">เลขที่ใบเสร็จ</span></td>
						<td ><span class="center">วันที่ชำระ</span></td>
						<td ><span class="center">ชื่อผู้สมัคร</span></td>
						<td ><span class="center">หลักสูตร</span></td>
						<td ><span class="center">Project code</span></td>
						<td ><span class="center">ราคา</span></td>

					</tr>
				</thead>

		<?php
			$runno=0;
			$ttl_course_price=0;
			$ttl_customer=0;
			foreach ($rs as $key => $v) {

				$runno++;
				$course_ids = trim($v["course_id"]);
				$course_ids = explode(",", $course_ids);
				$pay_date = explode(" ", $v["pay_date"]);
				$pay_date = $pay_date[0];
				$course_name="";
				$project_code="";
				$ttl_course_price = $ttl_course_price+$v["pay_price"];
				if ( count($course_ids)>1 ) {
					foreach ($course_ids as $key => $c) {
						$info_cos = get_course("", $course_ids[$key]);
						$info_cos = $info_cos[0];				
						$course_name .= $info_cos["title"]."<br><hr>";
					}// end loop $c
				}else{
					$info_cos = get_course("", $course_ids[0]);
					$info_cos = $info_cos[0];
					$course_name .= $info_cos["title"]."<br><hr>";
				}

				$course_name = trim($course_name, "<hr>");
				$t = $v["register_course_detail_course_detail_id"];
				$course_detail_id = ($t!="") ? $t : $v["course_detail_id"];

				$project_code = $db->data("select code_project from course_detail where course_detail_id={$course_detail_id}");
?>

				<tbody>
					<td ><span class="center"><?php echo $runno;?></span></td>
					<td ><span class="center"><?php echo $v["docno"];?></span></td>
					<td ><span class="center"><?php echo $pay_date;?></span></td>
					<td ><span class="left"><?php echo $v["full_name"];?></span></td>
					<td ><span class="left"><?php echo $course_name;?></span></td>
					<td ><span class="center"><?php echo $project_code;?></span></td>
					<td ><span class="right"><?php echo number_format($v["pay_price"]);?></span></td>
				</tbody>

<?php
				// die();
			}// end loop $v	
			$ttl_customer = $runno;
?>

				<tbody>
					<tr>
						<td colspan="6"><span class="center font-weight-bold">รวมจำนวนเงินทั้งหมด</span></td>
						<td colspan=""><span class="right"><?php echo number_format($ttl_course_price);?></span></td>				
					</tr>
					<tr>
						<td colspan="6"><span class="center font-weight-bold"><?php echo "จำนวนผู้ชำระเงิน"?></span></td>
						<td colspan=""><span class="center"><?php echo number_format($ttl_customer);?></span></td>
					</tr>
				</tbody>

			</table>
		</div>
		<br>
	</div>
</html>	

<script type="text/javascript">
function printPage(){
	   window.print();
	   setTimeout(" parent.$.fancybox.close()",1000);
	}
function receipt_tax_export(){
	var btn_confirm = confirm("หากต้องการ Export เป็นไฟล์ Excel ให้เลือก \"OK\"");
	if (btn_confirm == true) {
		var pay_date_chk = '<?php echo $pay_date_chk; ?>';
		var pay_type_chk = '<?php echo $pay_type_chk; ?>';
		url = "./report/report-payment-compare-money-export.php?pay_date_chk="+pay_date_chk+"&pay_type_chk="+pay_type_chk;
		// window.open(url,'_self');
		window.open(url,'_blank');
	} else {
	}
}	
</script>
