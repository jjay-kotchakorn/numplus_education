-- 2017-09-04
ALTER TABLE `register_data_list`
ADD COLUMN `elerning_map_course_detail_id`  int NULL AFTER `register_data_list_id`;

ALTER TABLE `register_data_list`
ADD COLUMN `map_course_detail_info`  text NULL AFTER `remark`;
------------------------------------------------------------------------

-- 2017-07-26
ALTER TABLE `register_data_list`
ADD COLUMN `code_project`  varchar(255) NULL AFTER `code_course_wr`;
------------------------------------------------------------------------

-- 2017-12-04
INSERT INTO payment_options (
	payment_options_id
	, code
	, name
) VALUES (
	5
	, '05'
	, 'mPAY'
);


/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : 127.0.0.1:3306
Source Database       : register_ati

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-12-04 16:28:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- -----------------------------
-- Table structure for payment_options_mpay
-- -----------------------------
DROP TABLE IF EXISTS `payment_options_mpay`;
CREATE TABLE `payment_options_mpay` (
  `payment_options_mpay_id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) DEFAULT NULL,
  `name` varchar(256) DEFAULT NULL,
  `name_eng` varchar(256) DEFAULT NULL,
  `active` varchar(1) DEFAULT 'T',
  `recby_id` int(11) DEFAULT NULL,
  `rectime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `remark` text,
  PRIMARY KEY (`payment_options_mpay_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of payment_options_mpay
-- ----------------------------
INSERT INTO `payment_options_mpay` VALUES ('1', '01', 'mPAY Station', '', 'T', null, '2015-04-10 01:31:18', '');
INSERT INTO `payment_options_mpay` VALUES ('2', '02', 'PS', '', 'F', '1', '2015-05-17 22:32:27', '');
INSERT INTO `payment_options_mpay` VALUES ('3', '03', 'ATM', '', 'T', '1', '2015-05-17 23:13:25', '');
INSERT INTO `payment_options_mpay` VALUES ('4', '04', 'Credit Card', '', 'T', '1', '2015-05-25 01:40:36', '');
INSERT INTO `payment_options_mpay` VALUES ('5', '05', '-', null, 'F', null, '2017-11-30 19:08:58', null);
INSERT INTO `payment_options_mpay` VALUES ('6', '06', 'mPAY Wallet ', null, 'T', null, '2017-12-01 10:58:03', null);


/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : 127.0.0.1:3306
Source Database       : register_ati

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-12-04 16:28:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for payment_mpay_register_list
-- ----------------------------
DROP TABLE IF EXISTS `payment_mpay_register_list`;
CREATE TABLE `payment_mpay_register_list` (
  `payment_mpay_register_list_id` int(11) NOT NULL AUTO_INCREMENT,
  `register_id` int(11) DEFAULT NULL,
  `payment_options_mpay_id` int(11) DEFAULT NULL,
  `code` varchar(32) DEFAULT NULL,
  `active` varchar(1) DEFAULT 'T',
  `recby_id` int(11) DEFAULT NULL,
  `rectime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `remark` text,
  `xml_status` varchar(1) DEFAULT NULL,
  `xml_resp_code` varchar(10) DEFAULT NULL,
  `xml_resp_desc` varchar(100) DEFAULT NULL,
  `xml_sale_id` varchar(30) DEFAULT NULL,
  `xml_end_point_url` varchar(255) DEFAULT NULL,
  `xml_result_code` varchar(10) DEFAULT NULL,
  `xml_result_status` varchar(1) DEFAULT NULL,
  `xml_result_desc` varchar(100) DEFAULT NULL,
  `xml_result_payment_status` varchar(100) DEFAULT NULL,
  `xml_credit_card_no` varchar(100) DEFAULT NULL,
  `xml_order_expire_date` varchar(50) DEFAULT NULL,
  `xml_amount` float(11,0) DEFAULT NULL,
  `xml_exc_customer_fee` float(11,0) DEFAULT NULL,
  `xml_purchase_amt` float(11,0) DEFAULT NULL,
  `xml_payment_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`payment_mpay_register_list_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- -----------------------------------------------------------------------------------------------
-- 2017-12-15
INSERT INTO pay_status (
  pay_status_id
  , code
  , name
) VALUES (
  9
  , '09'
  , 'ใบลดหนี้ (Credit note)'
);

ALTER TABLE `register`
ADD COLUMN `runno_credit_note`  int NULL AFTER `ref_exam_id`,
ADD COLUMN `credit_note_no`  varchar(255) NULL AFTER `runno_credit_note`;
-- -----------------------------------------------------------------------------------------------
-- 2017-12-19
ALTER TABLE `register_course_detail`
ADD COLUMN `register_te_result_id`  int NULL AFTER `result_by`,
ADD COLUMN `te_result_date`  datetime NULL AFTER `register_te_result_id`,
ADD COLUMN `te_result_by`  int NULL AFTER `te_result_date`;
-- -----------------------------------------------------------------------------------------------
-- 2017-12-20
ALTER TABLE `register_course_detail`
ADD COLUMN `result_from`  varchar(10) NULL AFTER `result_by`,
ADD COLUMN `te_result_from`  varchar(10) NULL AFTER `te_result_by`;

ALTER TABLE `register_data_list`
ADD COLUMN `te_result_id`  int NULL AFTER `result_id`,
ADD COLUMN `coursetype_el`  int NULL DEFAULT NULL AFTER `te_result_id`,
ADD COLUMN `coursetype_wr`  int NULL AFTER `coursetype_el`;

ALTER TABLE `register_data_list`
ADD COLUMN `register_course_detail_id`  int NULL AFTER `register_id`;

ALTER TABLE `register_data_list`
MODIFY COLUMN `send_status`  varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'T' AFTER `expire_date`,
MODIFY COLUMN `receive_status`  varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'F' AFTER `send_status`,
MODIFY COLUMN `result_id`  int(11) NULL DEFAULT NULL AFTER `receive_status`,
ADD COLUMN `te_result_date`  datetime NULL AFTER `te_result_id`;
-- -----------------------------------------------------------------------------------------------

-- 2017-12-28
ALTER TABLE `register`
ADD COLUMN `receipt_date`  datetime NULL AFTER `receipt_postcode`;

ALTER TABLE `register_log`
ADD COLUMN `receipt_date`  datetime NULL AFTER `receipt_postcode`;
-- -----------------------------------------------------------------------------------------------

-- 2018-01-09
ALTER TABLE `payment_mpay_register_list`
ADD COLUMN `curl_url_for_payment`  varchar(255) NULL AFTER `xml_payment_code`,
ADD COLUMN `curl_params`  text NULL AFTER `curl_url_for_payment`;
-- -----------------------------------------------------------------------------------------------

-- 2018-01-26
/*
Navicat MySQL Data Transfer

Source Server         : DEV_DB_ati-develop
Source Server Version : 50545
Source Host           : 203.150.230.5:3306
Source Database       : ati_develop

Target Server Type    : MYSQL
Target Server Version : 50545
File Encoding         : 65001

Date: 2018-01-26 17:42:28
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for register_credit_note
-- ----------------------------
DROP TABLE IF EXISTS `register_credit_note`;
CREATE TABLE `register_credit_note` (
  `register_credit_note_id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) DEFAULT NULL,
  `name` varchar(256) DEFAULT NULL,
  `name_eng` varchar(256) DEFAULT NULL,
  `active` varchar(1) DEFAULT 'T',
  `ref_register_id` int(11) DEFAULT NULL,
  `ref_new_register_id` int(11) DEFAULT NULL,
  `runyear` varchar(50) DEFAULT NULL,
  `runno` int(11) DEFAULT NULL,
  `doc_prefix` varchar(50) DEFAULT NULL,
  `docno` varchar(255) DEFAULT NULL,
  `credit_note_date` datetime DEFAULT NULL,
  `ref_receipt_docno` varchar(255) DEFAULT NULL,
  `new_receipt_docno` varchar(255) DEFAULT NULL,
  `ref_pay_price` float DEFAULT NULL,
  `ref_pay_date` datetime DEFAULT NULL,
  `credit_note_price` float DEFAULT NULL,
  `diff_price` float DEFAULT NULL,
  `recby_id` int(11) DEFAULT NULL,
  `rectime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `remark` text,
  `reason` text,
  PRIMARY KEY (`register_credit_note_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -----------------------------------------------------------------------------------------------

/*
Navicat MySQL Data Transfer

Source Server         : DEV_DB_ati-develop
Source Server Version : 50545
Source Host           : 203.150.230.5:3306
Source Database       : ati_develop

Target Server Type    : MYSQL
Target Server Version : 50545
File Encoding         : 65001

Date: 2018-01-26 17:42:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for register_credit_note_log
-- ----------------------------
DROP TABLE IF EXISTS `register_credit_note_log`;
CREATE TABLE `register_credit_note_log` (
  `register_credit_note_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `register_credit_note_id` int(11) DEFAULT NULL,
  `code` varchar(32) DEFAULT NULL,
  `name` varchar(256) DEFAULT NULL,
  `name_eng` varchar(256) DEFAULT NULL,
  `active` varchar(1) DEFAULT 'T',
  `ref_register_id` int(11) DEFAULT NULL,
  `ref_new_register_id` int(11) DEFAULT NULL,
  `runyear` varchar(50) DEFAULT NULL,
  `runno` int(11) DEFAULT NULL,
  `doc_prefix` varchar(50) DEFAULT NULL,
  `docno` varchar(255) DEFAULT NULL,
  `credit_note_date` datetime DEFAULT NULL,
  `ref_receipt_docno` varchar(255) DEFAULT NULL,
  `new_receipt_docno` varchar(255) DEFAULT NULL,
  `ref_pay_price` float DEFAULT NULL,
  `ref_pay_date` datetime DEFAULT NULL,
  `credit_note_price` float DEFAULT NULL,
  `diff_price` float DEFAULT NULL,
  `recby_id` int(11) DEFAULT NULL,
  `rectime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `remark` text,
  `reason` text,
  PRIMARY KEY (`register_credit_note_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -----------------------------------------------------------------------------------------------

-- 2018-01-26
/*
Navicat MySQL Data Transfer

Source Server         : DEV_DB_ati-develop
Source Server Version : 50545
Source Host           : 203.150.230.5:3306
Source Database       : ati_develop

Target Server Type    : MYSQL
Target Server Version : 50545
File Encoding         : 65001

Date: 2018-03-21 13:17:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for smcard_stamp_type
-- ----------------------------
DROP TABLE IF EXISTS `smcard_stamp_type`;
CREATE TABLE `smcard_stamp_type` (
  `smcard_stamp_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) DEFAULT NULL,
  `name` varchar(256) DEFAULT NULL,
  `name_eng` varchar(256) DEFAULT NULL,
  `smcard_report_id` int(11) DEFAULT NULL,
  `active` varchar(1) DEFAULT 'T',
  `recby_id` int(11) DEFAULT NULL,
  `rectime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `remark` text,
  PRIMARY KEY (`smcard_stamp_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of smcard_stamp_type
-- ----------------------------
INSERT INTO `smcard_stamp_type` VALUES ('1', '01', 'ลงเวลาเข้า (กิจกรรม)', '', '1', 'T', null, '2018-03-16 11:32:17', null);
INSERT INTO `smcard_stamp_type` VALUES ('2', '02', 'ลงเวลาออก (กิจกรรม)', '', '1', 'T', null, '2018-03-16 11:59:45', null);
INSERT INTO `smcard_stamp_type` VALUES ('3', '03', 'ลงเวลา (รับใบเสร็จ)', null, '1', 'T', null, '2018-03-16 12:00:41', null);
INSERT INTO `smcard_stamp_type` VALUES ('4', '04', 'ลงเวลา (รับผลกิจกรรม)', null, '1', 'T', null, '2018-03-16 12:05:05', null);
INSERT INTO `smcard_stamp_type` VALUES ('5', '05', 'ผ่านการตรวจสอบข้อมูล', null, '1', 'T', null, '2018-03-29 11:26:36', null);
INSERT INTO `smcard_stamp_type` VALUES ('6', '06', 'ไม่ผ่านการตรวจสอบข้อมูล', null, '1', 'T', null, '2018-03-29 11:26:45', null);
-- -----------------------------------------------------------------------------------------------
/*
Navicat MySQL Data Transfer

Source Server         : DEV_DB_ati-develop
Source Server Version : 50545
Source Host           : 203.150.230.5:3306
Source Database       : ati_develop

Target Server Type    : MYSQL
Target Server Version : 50545
File Encoding         : 65001

Date: 2018-03-21 13:17:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for smcard_report
-- ----------------------------
-- DROP TABLE IF EXISTS `smcard_report`;
CREATE TABLE `smcard_report` (
  `smcard_report_id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) DEFAULT NULL,
  `name` varchar(256) DEFAULT NULL,
  `name_eng` varchar(256) DEFAULT NULL,
  `active` varchar(1) DEFAULT 'T',
  `recby_id` int(11) DEFAULT NULL,
  `rectime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `remark` text,
  PRIMARY KEY (`smcard_report_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of smcard_report
-- ----------------------------
INSERT INTO `smcard_report` VALUES ('1', '01', 'รายงานการเข้าร่วมกิจกรรม', 'sign report', 'T', null, '2018-03-16 11:32:17', null);
-- -----------------------------------------------------------------------------------------------
/*
Navicat MySQL Data Transfer

Source Server         : DEV_DB_ati-develop
Source Server Version : 50545
Source Host           : 203.150.230.5:3306
Source Database       : ati_develop

Target Server Type    : MYSQL
Target Server Version : 50545
File Encoding         : 65001

Date: 2018-03-21 13:17:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for smcard_report_list
-- ----------------------------
DROP TABLE IF EXISTS `smcard_report_list`;
CREATE TABLE `smcard_report_list` (
  `smcard_report_list_id` int(11) NOT NULL AUTO_INCREMENT,
  `register_id` int(11) DEFAULT NULL,
  `register_course_detail_id` int(11) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `course_detail_id` int(11) DEFAULT NULL,
  `coursetype_id` int(11) DEFAULT NULL,
  `section_id` int(11) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `cid` varchar(100) DEFAULT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `smcard_report_id` int(11) DEFAULT NULL,
  `smcard_stamp_type_id` int(11) DEFAULT NULL,
  `datetime_stamp` datetime DEFAULT NULL,
  `time_start` time DEFAULT NULL,
  `time_stop` time DEFAULT NULL,
  `active` varchar(1) DEFAULT 'T',
  `recby_id` int(11) DEFAULT NULL,
  `rectime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `remark` text,
  PRIMARY KEY (`smcard_report_list_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
-- -----------------------------------------------------------------------------------------------
-- 2018-04-05

INSERT INTO `config` VALUES ('10', 'smcard_sign_stamp', 'F', 'ถ้ายังไม่ผ่านการ approve จะให้ลงเวลาเข้าออกกิจกรรมหรือไม่ F=ไม่ให้ลง, T=ให้ลงเวลา', null, '2018-04-05 15:21:03', 'T');

-- -----------------------------------------------------------------------------------------------
-- 2018-05-03
/*
Navicat MySQL Data Transfer

Source Server         : DEV_DB_ati-develop
Source Server Version : 50555
Source Host           : 203.150.231.128:3306
Source Database       : ati_develop

Target Server Type    : MYSQL
Target Server Version : 50555
File Encoding         : 65001

Date: 2018-05-04 17:26:13
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cronjob_qman
-- ----------------------------
DROP TABLE IF EXISTS `cronjob_qman`;
CREATE TABLE `cronjob_qman` (
  `cronjob_qman_id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) DEFAULT NULL,
  `path_file` varchar(256) DEFAULT NULL,
  `file_name` varchar(256) DEFAULT NULL,
  `name` varchar(256) DEFAULT NULL,
  `name_eng` varchar(256) DEFAULT NULL,
  `cronjob_qman_time` int(11) DEFAULT NULL,
  `time_unit_id` int(11) DEFAULT NULL,
  `active` varchar(1) DEFAULT 'T',
  `recby_id` int(11) DEFAULT NULL,
  `rectime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `remark` text,
  PRIMARY KEY (`cronjob_qman_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cronjob_qman
-- ----------------------------
INSERT INTO `cronjob_qman` VALUES ('1', '01', '/backoffice/data/', 'cronjob-running-3min.php', 'cronjob-running-3min', null, '3', '2', 'T', null, '2018-05-03 17:15:57', null);


/*
Navicat MySQL Data Transfer

Source Server         : DEV_DB_ati-develop
Source Server Version : 50555
Source Host           : 203.150.231.128:3306
Source Database       : ati_develop

Target Server Type    : MYSQL
Target Server Version : 50555
File Encoding         : 65001

Date: 2018-05-04 17:26:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cronjob_qman_list
-- ----------------------------
DROP TABLE IF EXISTS `cronjob_qman_list`;
CREATE TABLE `cronjob_qman_list` (
  `cronjob_qman_list_id` int(11) NOT NULL AUTO_INCREMENT,
  `cronjob_qman_id` int(11) DEFAULT NULL,
  `code` varchar(32) DEFAULT NULL,
  `path_file` varchar(256) DEFAULT NULL,
  `file_name` varchar(256) DEFAULT NULL,
  `name` varchar(256) DEFAULT NULL,
  `name_eng` varchar(256) DEFAULT NULL,
  `active` varchar(1) DEFAULT 'T',
  `recby_id` int(11) DEFAULT NULL,
  `rectime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `remark` text,
  PRIMARY KEY (`cronjob_qman_list_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cronjob_qman_list
-- ----------------------------
INSERT INTO `cronjob_qman_list` VALUES ('1', '1', '', '/backoffice/data/', 'con-register-elearning.php', 'con-register-elearning', null, 'T', null, '2018-05-03 17:19:51', '178148,178223,178178,178153,178221,178021,178171,178295,177673,178180,178224,178250,178209,178191,178050');


/*
Navicat MySQL Data Transfer

Source Server         : DEV_DB_ati-develop
Source Server Version : 50555
Source Host           : 203.150.231.128:3306
Source Database       : ati_develop

Target Server Type    : MYSQL
Target Server Version : 50555
File Encoding         : 65001

Date: 2018-05-04 17:26:38
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for time_unit
-- ----------------------------
DROP TABLE IF EXISTS `time_unit`;
CREATE TABLE `time_unit` (
  `time_unit_id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) DEFAULT NULL,
  `name` varchar(256) DEFAULT NULL,
  `name_eng` varchar(256) DEFAULT NULL,
  `active` varchar(1) DEFAULT 'T',
  `recby_id` int(11) DEFAULT NULL,
  `rectime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `remark` text,
  PRIMARY KEY (`time_unit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of time_unit
-- ----------------------------
INSERT INTO `time_unit` VALUES ('1', '01', 'วินาที', null, 'T', null, '2018-05-03 14:55:43', null);
INSERT INTO `time_unit` VALUES ('2', '02', 'นาที', null, 'T', null, '2018-05-03 14:55:48', null);
INSERT INTO `time_unit` VALUES ('3', '03', 'ชั่วโมง', null, 'T', null, '2018-05-03 14:55:55', null);
INSERT INTO `time_unit` VALUES ('4', '04', 'วัน', null, 'T', null, '2018-05-03 14:56:00', null);
INSERT INTO `time_unit` VALUES ('5', '05', 'เดือน', null, 'T', null, '2018-05-03 14:56:07', null);
INSERT INTO `time_unit` VALUES ('6', '06', 'ปี', null, 'T', null, '2018-05-03 14:56:14', null);
INSERT INTO `time_unit` VALUES ('7', '07', 'สัปดาห์', null, 'T', null, '2018-05-03 17:22:17', null);

-- -----------------------------------------------------------------------------------------------
-- 2018-05-24

ALTER TABLE `payment_mpay_register_list`
ADD COLUMN `xml_inc_customer_fee`  float NULL AFTER `xml_amount`;

ALTER TABLE `payment_mpay_register_list`
MODIFY COLUMN `xml_inc_customer_fee`  float(11,0) NULL DEFAULT NULL AFTER `xml_amount`;
-- -----------------------------------------------------------------------------------------------
--2018-05-25

ALTER TABLE `payment_options_mpay`
ADD COLUMN `charge`  float NULL AFTER `name_eng`,
ADD COLUMN `charge_unit`  int NULL AFTER `charge`;

ALTER TABLE `payment_options_mpay`
CHANGE COLUMN `charge_unit` `price_unit`  int(11) NULL DEFAULT NULL AFTER `charge`;





/*
Navicat MySQL Data Transfer

Source Server         : DEV_DB_ati-develop
Source Server Version : 50555
Source Host           : 203.150.231.128:3306
Source Database       : ati_develop

Target Server Type    : MYSQL
Target Server Version : 50555
File Encoding         : 65001

Date: 2018-05-25 13:33:15
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for price_unit
-- ----------------------------
DROP TABLE IF EXISTS `price_unit`;
CREATE TABLE `price_unit` (
  `price_unit_id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) DEFAULT NULL,
  `name` varchar(256) DEFAULT NULL,
  `name_eng` varchar(256) DEFAULT NULL,
  `active` varchar(1) DEFAULT 'T',
  `recby_id` int(11) DEFAULT NULL,
  `rectime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `remark` text,
  PRIMARY KEY (`price_unit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of price_unit
-- ----------------------------
INSERT INTO `price_unit` VALUES ('1', null, 'บาท', 'THB', 'T', null, '2018-05-25 13:32:01', null);
INSERT INTO `price_unit` VALUES ('2', null, '%', '%', 'T', null, '2018-05-25 13:32:11', null);





/*
Navicat MySQL Data Transfer

Source Server         : DEV_DB_ati-develop
Source Server Version : 50555
Source Host           : 203.150.231.128:3306
Source Database       : ati_develop

Target Server Type    : MYSQL
Target Server Version : 50555
File Encoding         : 65001

Date: 2018-05-25 15:05:45
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for payment_options_mpay
-- ----------------------------
DROP TABLE IF EXISTS `payment_options_mpay`;
CREATE TABLE `payment_options_mpay` (
  `payment_options_mpay_id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) DEFAULT NULL,
  `name` varchar(256) DEFAULT NULL,
  `name_eng` varchar(256) DEFAULT NULL,
  `charge` float DEFAULT NULL,
  `price_unit` int(11) DEFAULT NULL,
  `active` varchar(1) DEFAULT 'T',
  `recby_id` int(11) DEFAULT NULL,
  `rectime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `remark` text,
  PRIMARY KEY (`payment_options_mpay_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of payment_options_mpay
-- ----------------------------
INSERT INTO `payment_options_mpay` VALUES ('1', '01', 'mPAY ', '', null, null, 'F', null, '2015-04-10 01:31:18', '');
INSERT INTO `payment_options_mpay` VALUES ('2', '02', 'mPAY Station', '', null, null, 'T', '1', '2015-05-17 22:32:27', '');
INSERT INTO `payment_options_mpay` VALUES ('3', '03', 'ATM', '', null, null, 'T', '1', '2015-05-17 23:13:25', '');
INSERT INTO `payment_options_mpay` VALUES ('4', '04', 'Credit Card', '', '3.21', '2', 'T', '1', '2015-05-25 01:40:36', '');
INSERT INTO `payment_options_mpay` VALUES ('5', '05', '-', null, null, null, 'F', null, '2017-11-30 19:08:58', null);
INSERT INTO `payment_options_mpay` VALUES ('6', '06', 'mPAY Wallet ', null, null, null, 'T', null, '2017-12-01 10:58:03', null);



ALTER TABLE `payment_options_mpay`
CHANGE COLUMN `price_unit` `price_unit_id`  int(11) NULL DEFAULT NULL AFTER `charge`;
-- -----------------------------------------------------------------------------------------------
-- 2018-06-05
/*
Navicat MySQL Data Transfer

Source Server         : ATI-ati_develop
Source Server Version : 50555
Source Host           : 203.150.231.128:3306
Source Database       : ati_develop

Target Server Type    : MYSQL
Target Server Version : 50555
File Encoding         : 65001

Date: 2018-06-05 17:29:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cronjob_qman
-- ----------------------------
DROP TABLE IF EXISTS `cronjob_qman`;
CREATE TABLE `cronjob_qman` (
  `cronjob_qman_id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) DEFAULT NULL,
  `path_file` varchar(256) DEFAULT NULL,
  `file_name` varchar(256) DEFAULT NULL,
  `name` varchar(256) DEFAULT NULL,
  `name_eng` varchar(256) DEFAULT NULL,
  `cronjob_qman_time` int(11) DEFAULT NULL,
  `time_unit_id` int(11) DEFAULT NULL,
  `active` varchar(1) DEFAULT 'T',
  `recby_id` int(11) DEFAULT NULL,
  `rectime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `remark` text,
  PRIMARY KEY (`cronjob_qman_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cronjob_qman
-- ----------------------------
INSERT INTO `cronjob_qman` VALUES ('1', '01', '/backoffice/data/', 'cronjob-running-10min.php', 'cronjob-running-10min', null, '10', '2', 'T', null, '2018-05-03 17:15:57', null);
SET FOREIGN_KEY_CHECKS=1;


/*
Navicat MySQL Data Transfer

Source Server         : ATI-ati_develop
Source Server Version : 50555
Source Host           : 203.150.231.128:3306
Source Database       : ati_develop

Target Server Type    : MYSQL
Target Server Version : 50555
File Encoding         : 65001

Date: 2018-06-05 17:29:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cronjob_qman_list
-- ----------------------------
DROP TABLE IF EXISTS `cronjob_qman_list`;
CREATE TABLE `cronjob_qman_list` (
  `cronjob_qman_list_id` int(11) NOT NULL AUTO_INCREMENT,
  `cronjob_qman_id` int(11) DEFAULT NULL,
  `code` varchar(32) DEFAULT NULL,
  `path_file` varchar(256) DEFAULT NULL,
  `file_name` varchar(256) DEFAULT NULL,
  `name` varchar(256) DEFAULT NULL,
  `name_eng` varchar(256) DEFAULT NULL,
  `active` varchar(1) DEFAULT 'T',
  `recby_id` int(11) DEFAULT NULL,
  `rectime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `remark` text,
  PRIMARY KEY (`cronjob_qman_list_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cronjob_qman_list
-- ----------------------------
INSERT INTO `cronjob_qman_list` VALUES ('1', '1', '', '/backoffice/data/', 'con-register-elearning.php', 'con-register-elearning', null, 'T', null, '2018-06-05 17:27:13', '');
INSERT INTO `cronjob_qman_list` VALUES ('2', '1', null, '/backoffice/data/', 'con-check-auto-del.php', 'con-check-auto-del', null, 'T', null, '2018-06-05 17:14:17', null);
SET FOREIGN_KEY_CHECKS=1;

-- -----------------------------------------------------------------------------------------------
-- 2018-06-06

ALTER TABLE `payment_mpay_register_list`
ADD COLUMN `charge`  float(11,0) NULL AFTER `xml_payment_code`;
-- -----------------------------------------------------------------------------------------------
-- 2018-07-10

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for pay_type
-- ----------------------------
DROP TABLE IF EXISTS `pay_type`;
CREATE TABLE `pay_type` (
  `pay_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) DEFAULT NULL,
  `name` varchar(256) DEFAULT NULL,
  `name_eng` varchar(256) DEFAULT NULL,
  `active` varchar(1) DEFAULT 'T',
  `recby_id` int(11) DEFAULT NULL,
  `rectime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `remark` text,
  PRIMARY KEY (`pay_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_type
-- ----------------------------
INSERT INTO `pay_type` VALUES ('1', 'at_ati', 'ชำระเงินสดผ่าน ATI', null, 'T', null, '2018-07-10 12:29:00', null);
INSERT INTO `pay_type` VALUES ('2', 'walkin', 'ชำระเงินสดผ่าน ATI', null, 'T', null, '2018-07-10 12:29:19', null);
INSERT INTO `pay_type` VALUES ('3', 'importfile', 'ชำระเงินสดผ่าน ATI', null, 'T', null, '2018-07-10 12:29:42', null);
INSERT INTO `pay_type` VALUES ('4', 'paysbuy', 'ชำระเงินช่องทางอื่นๆ (paysbuy)', null, 'T', null, '2018-07-10 12:29:56', null);
INSERT INTO `pay_type` VALUES ('5', 'mpay', 'ชำระเงินช่องทางอื่นๆ (mPAY)', null, 'T', null, '2018-07-10 12:30:13', null);
INSERT INTO `pay_type` VALUES ('6', 'bill_payment', 'ชำระเงินผ่าน Bill-payment', null, 'T', null, '2018-07-10 12:30:26', null);
INSERT INTO `pay_type` VALUES ('7', 'at_asco', 'เช็ค/เงินโอน', null, 'T', null, '2018-07-10 12:30:36', null);
-- -----------------------------------------------------------------------------------------------
-- 2018-07-26
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for coursetype
-- ----------------------------
DROP TABLE IF EXISTS `coursetype`;
CREATE TABLE `coursetype` (
  `coursetype_id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) DEFAULT NULL,
  `name` varchar(256) DEFAULT NULL,
  `name_eng` varchar(256) DEFAULT NULL,
  `active` varchar(1) DEFAULT 'T',
  `recby_id` int(11) DEFAULT NULL,
  `rectime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `remark` text,
  PRIMARY KEY (`coursetype_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of coursetype
-- ----------------------------
INSERT INTO `coursetype` VALUES ('1', '01', 'อบรม', 'aob_rom', 'T', null, '2015-02-26 05:20:02', null);
INSERT INTO `coursetype` VALUES ('2', '02', 'สอบ', 'sorb', 'T', null, '2015-02-26 05:20:07', null);
INSERT INTO `coursetype` VALUES ('3', '03', 'อบรม+สอบ', 'aob_rom_sorb', 'T', null, '2015-02-26 05:20:16', null);
INSERT INTO `coursetype` VALUES ('4', '04', 'อบรมต่ออายุ', 'aob_rom_tor_ar_yu', 'T', null, '2015-03-23 17:48:39', null);

-- -----------------------------------------------------------------------------------------------
-- 2018-08-15
/*
Navicat MySQL Data Transfer

Source Server         : DEV_ATI-devsite.numplus.com
Source Server Version : 50545
Source Host           : devsite.numplus.com:3306
Source Database       : devsite-ati

Target Server Type    : MYSQL
Target Server Version : 50545
File Encoding         : 65001

Date: 2018-08-15 12:19:58
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for time_unit
-- ----------------------------
DROP TABLE IF EXISTS `time_unit`;
CREATE TABLE `time_unit` (
  `time_unit_id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) DEFAULT NULL,
  `name` varchar(256) DEFAULT NULL,
  `name_eng` varchar(256) DEFAULT NULL,
  `active` varchar(1) DEFAULT 'T',
  `recby_id` int(11) DEFAULT NULL,
  `rectime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `remark` text,
  PRIMARY KEY (`time_unit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of time_unit
-- ----------------------------
INSERT INTO `time_unit` VALUES ('1', '01', 'วินาที', 'Seconds', 'T', null, '2018-05-03 14:55:43', null);
INSERT INTO `time_unit` VALUES ('2', '02', 'นาที', 'Minutes', 'T', null, '2018-05-03 14:55:48', null);
INSERT INTO `time_unit` VALUES ('3', '03', 'ชั่วโมง', 'Hours', 'T', null, '2018-05-03 14:55:55', null);
INSERT INTO `time_unit` VALUES ('4', '04', 'วัน', 'Days', 'T', null, '2018-05-03 14:56:00', null);
INSERT INTO `time_unit` VALUES ('5', '05', 'เดือน', 'Month', 'T', null, '2018-05-03 14:56:07', null);
INSERT INTO `time_unit` VALUES ('6', '06', 'สัปดาห์', 'Weeks', 'T', null, '2018-05-03 14:56:14', null);
INSERT INTO `time_unit` VALUES ('7', '07', 'ปี', 'Years', 'T', null, '2018-05-03 17:22:17', null);
-- -----------------------------------------------------------------------------------------------
INSERT INTO `config` VALUES ('11', 'smcard_token_expire', '60,2', 'number,time_unit_id', null, '2018-08-15 11:44:43', 'T');
-- -----------------------------------------------------------------------------------------------
-- 2018-09-10
ALTER TABLE `pay_type`
CHANGE COLUMN `pay_status_id` `pay_type_id`  int(11) NOT NULL AUTO_INCREMENT FIRST ;
-- -----------------------------------------------------------------------------------------------
INSERT INTO `pay_type` VALUES ('8', 'free', '- ( Free )', null, 'T', null, '2018-09-10 10:55:42', null);
-- -----------------------------------------------------------------------------------------------
