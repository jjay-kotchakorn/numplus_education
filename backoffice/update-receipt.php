<?php
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";
include_once "./share/member.php";
global $db;
if($_POST){
	$id = $_POST["receipt_id"];
	$db->begin();
    $args = array();
	$args = array('table' => 'receipt',
			  'no'  => $_POST["no"],
			  'name'  => $_POST["name"],
			  'gno'  => $_POST["gno"],
			  'moo'  => $_POST["moo"],
			  'soi'  => $_POST["soi"],
			  'road'  => $_POST["road"],
			  'receipttype_id' => $_POST["receipttype_id"],
			  'district_id' => $_POST["district_id"],
			  'district_id' => $_POST["district_id"],
			  'amphur_id'  => $_POST["amphur_id"],
			  'province_id'   => $_POST["province_id"],
			  'postcode'   => $_POST["postcode"],
			  'branch'  => $_POST["branch"],
			  'taxno'   => $_POST["taxno"]
	);
	if($id){
		$args['id'] = array('key'=>'receipt_id', 'value'=>$id);
	}
	$args["active"] = $_POST["active"];
	$args["recby_id"] = (int) $EMPID;
	$args["rectime"] = date("Y-m-d H:i:s");
		// At last.. a query!
	$ret = $db->set($args);
	$ret = ($id) ? $id : $ret; 
	$db->commit();
	
}
$args = array();
$args["update_state"] = "T";
$args["receipt_id"] = $ret;
$args["type"] = "info";
redirect_url($args);
?>