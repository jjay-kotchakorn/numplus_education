<?php
include_once "./share/authen.php";
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
global $db;
require_once dirname(__FILE__) . '/PHPExcel.php';

// $sec_id = $_REQUEST['sec_id'];
// $costy_id = $_REQUEST['cos_type_id'];
$pay_by = $_REQUEST['pay_by'];
//$cos_dtail = $_REQUEST['cos_dtail'];
//$name = $_REQUEST['name'];
$date_start_th = $_REQUEST['date_start'];
$date_stop_th = $_REQUEST['date_stop'];

//d($_REQUEST);
//die();

/*if ( (!isset($date_start_th) || empty($date_start_th)) || (!isset($date_stop_th) || empty($date_stop_th)) )  {
	echo "<script language=\"JavaScript\">
	alert(\"กรุณากลับไปเลือกช่วงเวลา\"); 
	window.close();
	</script>";
}*/

$ar_date_st = explode("-", $date_start_th);
$ar_date_st[2] = $ar_date_st[2]-543;
$date_start = $ar_date_st[2]."-".$ar_date_st[1]."-".$ar_date_st[0]." 00:00:00";

$ar_date_en = explode("-", $date_stop_th);
$ar_date_en[2] = $ar_date_en[2]-543;
$date_stop = $ar_date_en[2]."-".$ar_date_en[1]."-".$ar_date_en[0]." 23:59:59";

$cond = "r.active = 'T' AND r.auto_del = 'F' AND r.pay_status = 8 AND (r.docno <> '' AND r.docno IS NOT NULL)";
if (isset($sec_id) && !empty($sec_id)) {
	$cond .= " AND r.section_id=$sec_id";
}
if (isset($costy_id) && !empty($costy_id)) {
	$cond .= " AND r.coursetype_id=$costy_id";
}
if ( isset($pay_by) && !empty($pay_by) && ($pay_by != "undefined") ) {

	switch ($pay_by) {
		case 'at_ati': 	
			$cond .= " AND (r.pay='at_ati' OR r.pay='walkin' OR r.pay='importfile')";
			break;
		default:
			$cond .= " AND a.pay='{$pay_by}'";
			break;
	}

	//$cond .= " AND r.pay='$pay_by'";
}

if ( (isset($date_start) && !empty($date_start)) || (isset($date_stop) && !empty($date_stop)) ) {
	if ( (isset($date_start) && !empty($date_start)) && !(isset($date_stop) && !empty($date_stop)) ) {
		$cond .= " AND r.date='$date_start'";
	}
	if ( !(isset($date_start) && !empty($date_start)) && (isset($date_stop) && !empty($date_stop)) ) {
		$cond .= " AND r.date='$date_stop'";
	}else{
		$cond .= " AND (r.date BETWEEN '$date_start' AND '$date_stop')"; 
	}
}

$qry = "SELECT r.course_price
		, r.course_discount
		, r.date
		, r.docno
		, r.rectime
		, r.title
		, r.fname
		, r.lname
		, r.taxno
		, r.receipt_id
		, r.pay
		, r.pay_date
		, r.course_id
		, r.course_detail_id
		, cd.code_project
		, rc.name
		, rc.branch  
	FROM register AS r 
	LEFT JOIN course_detail AS cd ON cd.course_detail_id = r.course_detail_id
	LEFT JOIN receipt AS rc ON rc.receipt_id = r.receipt_id  
	WHERE $cond 
	ORDER BY r.date ASC
	;";
$rs = $db->get($qry);
//var_dump($qry);
//echo count($rs);
// d($qry);
?>
<!DOCTYPE html>
<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>รายงานภาษีขาย</title>
<link href="css/printform-style.css" rel="stylesheet" type="text/css" media="all" />
<style>
	@media print{
		body{
			padding: 0px;
		}
		#btPrint{
			display: none;
		}
	}
	.noborder
	{
		border:none;
	}
</style>
<!-- <body onload=""> -->
<body onload="receipt_tax_export()">
	<div class="form-landscape">		
<?php 			
			$page=1;
			$i=0;
			$sum_pri = 0;
			$sum_no_vat = 0;
			$sum_vat = 0;
			$sum_dis = 0;
			$sum_all_cos_pri = 0;
			$sum_all_cos_dis = 0;
			$rs_ttl = count($rs);
			$ttl_page = intval($rs_ttl/10);
			foreach ($rs as $key => $value) {
				$cos_dis = $value["course_discount"];
				$cos_pri = $value["course_price"]-$cos_dis;
				$cos_no_vat = ($cos_pri/1.07);
				$cos_vat = $cos_pri-$cos_no_vat;
				
				$sum_no_vat = $sum_no_vat+$cos_no_vat;
				$sum_vat = $sum_vat+$cos_vat;
				$sum_pri = $sum_pri+$cos_pri;
				$sum_dis = $sum_dis+$cos_dis;
				
				$name = $value["title"].$value["fname"]." ".$value["lname"];
				$receipt_id = $value["receipt_id"];

				$org_name = "";
				$org_branch = "";
				if ( $receipt_id=='' || $receipt_id==NULL ) {
					$org_name = $name;
					$org_branch = "";
				}else{
					$org_name = $value["name"];
					$org_branch = $value["branch"];
				}// end else

				if ( empty($value["code_project"]) ) {
					$cos_dtail_ids = explode(",", $value["course_detail_id"]);
					$cos_dtail_id = $cos_dtail_ids[1];
					$cos_dtail_id = explode(":", $cos_dtail_id);
					$cos_dtail_id = trim($cos_dtail_id[1]);
					$info_cos_dtail = get_course_detail("", $cos_dtail_id);
					$value["code_project"] = $info_cos_dtail[0]['code_project'];
				}

				//echo $value["course_id"];
				$cos_ids = $value["course_id"];
				$cos_ids = explode(",", $cos_ids);
				$sum_cos_pri = 0;
				foreach ($cos_ids as $key => $id) {
					//var_dump($id);
					$id = trim($id, ",");
					$cos_info = get_course("", $id);
					$cos_info = $cos_info[0];
					//var_dump($cos_info["price"]);
					//die();
					$sum_cos_pri = $sum_cos_pri+$cos_info["price"];
				}
				$sum_cos_dis = $sum_cos_pri - $cos_pri;

				if($value["pay"]=="at_ati"){
					$pay_type = "ชำระเงินสดผ่าน ATI";
				}else if($value["pay"]=="walkin"){
					$pay_type = "ชำระเงินสดผ่าน ATI";			
				}else if($value["pay"]=="importfile"){
					$pay_type = "ชำระเงินสดผ่าน ATI";
				}else if($value["pay"]=="paysbuy"){
					$pay_type = "ชำระเงินช่องทางอื่นๆ (paysbuy)";
					$print_style = "visibility:hidden";
				}else if($value["pay"]=="mpay"){
					$pay_type = "ชำระเงินช่องทางอื่นๆ (mPAY)";
					$print_style = "visibility:hidden";
				}else if($value["pay"]=="bill_payment"){
					$pay_type = "ชำระเงินผ่าน Bill-payment";
				}else if($value["pay"]=="at_asco"){
					$pay_type = "เช็ค/เงินโอน";
				}

				$sum_all_cos_pri = $sum_all_cos_pri+$sum_cos_pri;
				$sum_all_cos_dis = $sum_all_cos_dis+$sum_cos_dis;

				$sum_cos_pri = number_format($sum_cos_pri, 2, '.', ',');
				$sum_cos_dis = number_format($sum_cos_dis, 2, '.', ',');
				$cos_pri = number_format( $cos_pri, 2, '.', ',' );
				$cos_no_vat = number_format( $cos_no_vat, 2, '.', ',' );
				$cos_vat = number_format( $cos_vat, 2, '.', ',' );
				$cos_dis = number_format( $cos_dis, 2, '.', ',' );

				$i++;
				$cut_row = $i%10;
				if ($i==1 || $cut_row==1) {
?>

		<div style="width: 1197px;">
			<table class="td-center" style="width:100%">
				<br>
				<thead>
					<tr>
						<td colspan="12" class="noborder"><span class="center font-weight-bold">สรุปยอดซื้อ-ขายหลักสูตรของใบเสร็จที่ถูกยกเลิก</span></td>
					</tr>
					<tr>
						<td colspan="12" class="noborder"><span class="center"><?php echo "ระหว่างวันที่ ".$date_start_th." ถึง ".$date_stop_th;?></span></td>
					</tr>
					<tr>
						<td colspan="2" width="" class="noborder font-weight-bold">ชื่อผู้ประกอบการ</td>
						<td colspan="7" class="noborder">สมาคมบริษัทหลักทรัพย์ไทย</td>
						<td colspan="2" width="" class="noborder font-weight-bold center">เลขประจำตัวผู้เสียภาษีอากร</td>
						<td colspan="1" width="" class="noborder font-weight-bold center">0993000132416</td>
					</tr>
					<tr>
						<td colspan="2" class="font-weight-bold noborder">สถานประกอบการ</td>
						<td colspan="7" class="noborder">อาคารชุดเลครัชดาออฟฟิตคอมเพล็กซ์2 ชั้น 5 เลขที่ 195/6 ถนนรัชดาภิเษก แขวงคลองเตย เขตคลองเตย กรุงเทพมหานคร 10110</td>
						<td colspan="3" class="center font-weight-bold noborder center">&nbsp;&nbsp;&nbsp;&nbsp;สำนักงานใหญ่</td>
					</tr>
					<tr>
						<td colspan="12" class="noborder"><span class="left"><h5>VAT Code OG01 - Output VAT - Goods 7%</h5></span></td>
					</tr>
				</thead>

				<tbody>
					<tr>
						<td width="5%"><span class="center"><br><br>ลำดับที่</span></td>
						<td colspan="3">
							<table class="no-border">
								<tbody>
									<tr>
										<td colspan="3" width=21%><span class="under-line-center">ใบกำกับภาษี</span></td>
									</tr>
									<tr>
										<td width="9%"><span class="right-line-center"><br><br>วัน เดือน ปี</span></td>
										<td width="7%"><span class="right-line-center"><br><br>วันที่เอกสาร</span></td>
										<td width="7%"><span class="center"><br><br>เลขที่เอกสาร</span></td>
									</tr>
								</tbody>
							</table>
						</td>
						<td width=7%><span class="center"><br>เลขที่<br>โครงการ</span></td>
						<td width=10%><span class="center"><br><br>ช่องทางชำระ</span></td>
						
						<td width=7%><span class="center">เลขประจำตัวผู้เสียภาษีอากร<br>ของผู้ซื้อสินค้าบริการ</span></td>
						<td colspan="2" width=20%>
							<table class="no-border">
								<tbody>
									<tr>
										<td colspan="2"><span class="under-line-center">สถานประกอบการ</span></td>
									</tr>
									<tr>
										<td width="9%"><span class="right-line-center"><br><br>ชื่อผู้ซื้อบริการ</span></td>
										<td width="10%"><span class="right-line-center"><br><br>สาขา</span></td>
									</tr>
								</tbody>
							</table>
						</td>
						<td width=10%><span class="center"><br>มูลค่าสินค้า<br>หรือบริการ</span></td>
						<td width=10%><span class="center"><br>จำนวนเงิน<br>ภาษีมูลค่าเพิ่ม</span></td>
						<td width=10%><span class="center"><br><br>รวม</span></td>
					</tr>
				</tbody>
<?php
				}//end if
?>
				<col width=5%>
				<col width=7%>
				<col width=7%>
				<col width=7%>
				<col width=7%>
				<col width=10%>
				<col width=7%>
				<col width=10%>
				<col width=10%>
				<col width=10%>
				<col width=10%>
				<col width=10%>

				<tbody>
					<tr>
						<td><span class="center"><?php echo $i;?></span></td>
						<td><span class="center"><?php $date = explode(" ", $value["date"]); echo revert_date($date[0]);?></span></td>	
						<td><span class="center"><?php $date_doc = explode(" ", $value["date"]); echo revert_date($date_doc[0]); ?></span></td>		
						<td><span class="center"><?php echo $value["docno"];?></span></td>
						<td><span class="center"><?php echo $value["code_project"];?></span></td>
						<td><span class="left"><?php echo $pay_type;?></span></td>
						<td><span class="center"><?php echo $value["taxno"];?></span></td>
						<td><span class="left"><?php echo $org_name;?></span></td>
						<td><span class="left"><?php echo $org_branch;?></span></td>
						<td><span class="right"><?php echo $cos_no_vat;?></span></td>
						<td><span class="right"><?php echo $cos_vat;?></span></td>
						<td><span class="right"><?php echo $cos_pri;?></span></td>
					</tr>
<?php			
				if ($cut_row==1 && $i!=1) {	
?>
					<!-- <br> -->
					<div style="page-break-after: always;"></div>

<?php					
				}
				//echo "Page ".$page." / ".$ttl_page;
				$page++;			
			}// end loop for
				$sum_all_cos_pri = number_format( $sum_all_cos_pri, 2, '.', ',' );
				$sum_all_cos_dis = number_format( $sum_all_cos_dis, 2, '.', ',' );
				$sum_pri = number_format( $sum_pri, 2, '.', ',' );
				$sum_no_vat = number_format( $sum_no_vat, 2, '.', ',' );
				$sum_vat = number_format( $sum_vat, 2, '.', ',' );
				$sum_dis = number_format( $sum_dis, 2, '.', ',' );
?>
					<tr>
						<td colspan="9" class="center">รวม</td>
						<td><span class="right"><?php echo $sum_no_vat;?></span></td>
						<td><span class="right"><?php echo $sum_vat;?></span></td>
						<td><span class="right"><?php echo $sum_pri;?></span></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</body>
<script type="text/javascript">
function printPage(){
   window.print();
   setTimeout(" parent.$.fancybox.close()",1000);
}

function receipt_tax_export(){
	var btn_confirm = confirm("หากต้องการ Export เป็นไฟล์ Excel ให้เลือก \"OK\"");
	if (btn_confirm == true) {
		var date_start = '<?php echo $date_start_th; ?>';
		var date_stop = '<?php echo $date_stop_th; ?>';

		// var sec_id = '<?php echo $sec_id; ?>';
		// var costy_id = '<?php echo $costy_id; ?>';
		var pay_by = '<?php echo $pay_by; ?>';

		url = "./report/report-receipt-cancel-export.php?date_start="+date_start+"&date_stop="+date_stop+"&pay_by="+pay_by;
		window.open(url,'_self');
	} else {

	}
}

</script>
</html>