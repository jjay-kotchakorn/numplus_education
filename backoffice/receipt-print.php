<?php
include_once "./share/authen.php";
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
global $db;
require_once dirname(__FILE__) . '/PHPExcel.php';
$symbo_unc = "&#9744;";
$symbo_c = "&#9745;";
?>
<!DOCTYPE html>
<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ใบเสร็จรับเงิน</title>
<link href="css/printform-receipt-style.css" rel="stylesheet" type="text/css" media="all" />
<style>
	@media print{
		body{
			padding: 0px;
			margin: 0px;
		}
		#btPrint{
			display: none;
		}
	}
	.page-header {
		text-align: center;
		font-size: 19px;
		/*height: 950px;*/
		height: 900px;
		/* width: 540px; */
		display: block;
		position: relative;
	}
</style>
<body>
<?php
$apay = datatype(" and a.active='T'", "pay_status", true); 
$section = datatype(" and a.active='T'", "section", true);
$arr_pay = array();
foreach ($apay as $key => $value) {
	$arr_pay[$value["pay_status_id"]] = $value["name"];
}
$id = $_POST["register_id"];
$ids = explode(",", $id);
if(!$id){
	$id = $_GET["register_id"];
	$ids = explode(",", $id);
}

$page = 1;
$all_page = count($ids);
//echo "$all_page";
$array_print = array();
$course_running = array();
foreach ($ids as $key => $register_id) {
//echo "Page=".$page."\r\n";
	if($register_id){		
		$v = get_register("", $register_id);		
		$str = "";
		if($v){
			$v = $v[0];
			$info = $v;
			$str = "เลขที่ ".$v["register_id"];
			$register_id = $v["register_id"];
			$sCourse_id = $v["course_id"];
			//d($sCourse_id);
			//$list_course = $v["course_id"];
			$list_course_detail = $v["course_detail_id"];
			$register_pay_status = $v["pay_status"];
			$single = false;
			$page_size = 0;
			
			if(strrpos($sCourse_id, ",")!==false){
				$count_course = explode(",", $sCourse_id);
				//d($count_course);die();
				foreach ($count_course as $course_index => $course_value) {
					$course_running[$register_id][$course_value] = $course_index +1;
				}//end loop $course_value
				//d($course_running);
				$ac = array_chunk($count_course, 4, true);
				// d($ac);die();
				foreach ($ac as $key => $vc){
					$array_print[$register_id][$key]["data"] = $v;
					$array_print[$register_id][$key]["course"] = $vc;
					
					$q = "SELECT SUM(price) AS sum_price
						FROM course
						WHERE course_id IN ($sCourse_id)
					";
					$cos_price_ttl = $db->data($q);
					// echo $cos_price_ttl;


					$course_price = $v['course_price'] - $v["course_discount"];
					$vat = $course_price-($course_price/1.07);	
					$course_discount = 0;

					if ( $cos_price_ttl > $v['course_price'] ) {
						$course_discount = $cos_price_ttl - $v['course_price'];//200
						$course_discount = $course_discount + $v["course_discount"];
					}else{
						$course_discount = $v['course_discount'];
					}

					$array_print[$register_id][$key]["footer"]["course_price"] = $course_price;
					$array_print[$register_id][$key]["footer"]["cos_price_ttl"] = $cos_price_ttl;
					$array_print[$register_id][$key]["footer"]["vat"] = $vat;
					$array_print[$register_id][$key]["footer"]["course_discount"] = $course_discount;

				}//end loop $vc

			}else{
				$array_print[$register_id][0]["data"] = $v;
				$array_print[$register_id][0]["course"][0] = $sCourse_id;
				$single = true;

				$q = "SELECT SUM(price) AS sum_price
					FROM course
					WHERE course_id IN ($sCourse_id)
				";
				$cos_price_ttl = $db->data($q);
				// echo $cos_price_ttl;

				$course_price = $v['course_price'] - $v["course_discount"];
				$vat = $course_price-($course_price/1.07);	
				$course_discount = 0;

				if ( $cos_price_ttl > $v['course_price'] ) {
					$course_discount = $cos_price_ttl - $v['course_price'];//200
					$course_discount = $course_discount + $v["course_discount"];
				}else{
					$course_discount = $v['course_discount'];
				}

				$array_print[$register_id][0]["footer"]["course_price"] = $course_price;
				$array_print[$register_id][0]["footer"]["cos_price_ttl"] = $cos_price_ttl;
				$array_print[$register_id][0]["footer"]["vat"] = $vat;
				$array_print[$register_id][0]["footer"]["course_discount"] = $course_discount;
			}//end else			
		}//end if
		$v="";
   } //end if
}//end loop $register_id

//d($array_print);

if(count($array_print)>0){
	foreach ($array_print as $register_id => $group) {
		// d($group);
		// die();
    $header_page=0;
    for($loop_page=1;$loop_page<=2;$loop_page++){
		$all_page = count($group);
		foreach ($group as $page => $value) {
    		$header_page = $page + 1;
	    	if ($loop_page==1) {
	    		$copy_page = "แผ่นที่ {$header_page} (ต้นฉบับ)";
	    	}else{
	    		$copy_page = "แผ่นที่ {$header_page} (สำเนา)";
	    	}		
			$v = $value["data"];
			$tr_footer = $value["footer"];
			// d($tr_footer);
			// die();
			$info = $v;
			$str = "เลขที่ ".$v["register_id"];
			$register_id = $v["register_id"];
			$sCourse_id = $v["course_id"];
			$list_course = $v["course_id"];
			$list_course_detail = $v["course_detail_id"];
			$register_pay_status = $v["pay_status"];
			$sCourse = $value["course"];
			if(count($sCourse)>1){
				$sCourse_id = implode(",", $value["course"]);
			}else{
				$t = $sCourse;
				$sCourse_id = array_shift($t);
			}		
			// echo $sCourse_id;die();	
			$con = " and a.course_id in ($sCourse_id)";
			$r = get_course($con, "", true);
			$title = "";
			$parent_id = 0;
			$price = 0;
			$sum_pri_no_dis = 0;
			$array_course = array();
			$array_course_detail = array();
			// d($r);
			if($r){
				foreach ($r as $key => $row) {
					$course_id = $row['course_id'];
					$array_course[$course_id] = $row;
					$section_id = $row['section_id'];
					$code = $row['code'];
					$title .= $row['title'].", ";
					$set_time = $row['set_time'];
					$life_time = $row['life_time'];
					$status = $row['status']; 
					$parent_id = (int)$row['parent_id'];
					//$pri_per_cos = $row['price'];
				}//end loop $row

			}//end if
			// d($array_course);
			$q = "select a.course_detail_id, a.course_id from register_course_detail a where a.register_id={$register_id} and a.course_id in ($sCourse_id)";
			$cd = $db->get($q);
			$parent_title = "";
			if($parent_id>0){
				$q = "select title from course where active='T' and course_id=$parent_id";
				$t = $db->data($q);
				$parent_title = "<strong>{$t}</strong><br>";	
			}
			$title = trim($title, ", ");
			$price = $v["course_price"];
			$discount = $v["course_discount"];

			if($cd){    
				$display_date = "";
				$display_time = "";
				$display_address = "";
				$running=0;
				foreach ($cd as $kk => $vv) {
					$running = $running++;
					$row = get_course_detail(" and a.course_detail_id={$vv["course_detail_id"]}");
					if($row) $row = $row[0];
					$course_detail_id = $row['course_detail_id'];
					$course_id = $vv['course_id'];
					$array_course_detail[$course_id][$course_detail_id] = $row;
					$day = $row['day'];
					$date = $row['date'];
					$time = $row['time'];
					$display_date .= $day." ".revert_date($date).", ";
					$display_time .= $time.", ";
					$display_address .= $row['address_detail']." ".$row['address'].", ";
				}//end loop $vv

			}else{
				$row = get_course_detail(" and a.course_detail_id={$v["course_detail_id"]}");
				if($row) $row = $row[0];
				$course_detail_id = $row['course_detail_id'];
				$course_id = $v['course_id'];
				$array_course_detail[$course_id][$course_detail_id] = $row;
				$day = $row['day'];
				$date = $row['date'];
				$time = $row['time'];
				$display_date .= $day." ".revert_date($date).", ";
				$display_time .= $time.", ";
				$display_address .= $row['address']." ".$row['address_detail'].", ";
			}
			/*print_r($array_course_detail);*/
			$display_date = trim($display_date, ", ");
			$display_time = trim($display_time, ", ");
			$display_address = trim($display_address, ", ");
			$id = $v["register_id"];
			$title = str_replace(", ", "<br>",trim($title, ", "));
			$display_date = str_replace(", ", "<br>",trim($display_date, ", "));
			$display_time = str_replace(", ", "<br>",trim($display_time, ", "));
			$display_address = str_replace(", ", "<br>",trim($display_address, ", "));
			$content = "";
			$pay_type = "";
			$print_style = "";
			$check_pay = $v["pay"];
			if($v["pay"]=="at_ati"){
				$pay_type = "ชำระเงินสดผ่าน ATI";
			}else if($v["pay"]=="paysbuy"){
				$pay_type = "ชำระเงินช่องทางอื่นๆ (paysbuy)";
				$print_style = "visibility:hidden";
			}else if($v["pay"]=="bill_payment"){
				$pay_type = "ชำระเงินผ่าน Bill-payment";
			}else if($v["pay"]=="at_asco"){
				$pay_type = "เช็ค/เงินโอน";
			}else if($v["pay"]=="walkin"){
				if($info["pay_status"]==5 || $info["pay_status"]==6)
					$pay_type = $arr_pay[$info["pay_status"]];
				else	
					$pay_type = "ชำระเงินสดผ่าน ATI";
			}else if($v["pay"]=="importfile"){
				if($info["pay_status"]==5 || $info["pay_status"]==6)
					$pay_type = $arr_pay[$info["pay_status"]];
				else	
					$pay_type = "ชำระเงินสดผ่าน ATI";
			}
			$pay = $v["pay_id"];
			$status = $v["pay_status"];
			$pay_date = $v["pay_date"];
			$pay_date = ($pay_date && $pay_date!="") ? revert_date($pay_date, false) : '';
			$status_pay = ($status==3) ? "ชำระเงินแล้ว" : "รอการชำระเงิน";
			$td = "";
			if($info["slip_type"]=="individuals") $slip_type_text = "ออกในนามบุคคลธรรมดา";
			if($info["slip_type"]=="corparation") $slip_type_text = "ออกในนามนิติบุคคล (สำหรับเบิกค่าใช้จ่าย)";
			$q = "select name from receipttype where receipttype_id={$info["receipttype_id"]}";
			$receipttype_name = $db->data($q);
			$q = "select name from district where district_id={$info["receipt_district_id"]}";
			$district_name = $db->data($q);
			$q = "select name from amphur where amphur_id={$info["receipt_amphur_id"]}";
			$amphur_name = $db->data($q);
			$q = "select name from province where province_id={$info["receipt_province_id"]}";
			$province_name = $db->data($q);
			$q = "select register_id from register a where a.active='T' and  a.course_id='$course_id'";
			$id = $db->data($q);
			
			$t = convert_mktime($info["rectime"]) + (2 * 24 * 60 * 60);
			//$pay_date = $info["expire_date"];
			$receipt_full = $info["receipt_title"].$info["receipt_fname"]." ".$info["receipt_lname"]."&nbsp&nbsp เลขประจำตัวประชาชน ".$info["cid"]."<br>";
			$name_full = $info["title"].$info["fname"]." ".$info["lname"];
			$address = $info["receipt_no"];
			if($info["receipt_gno"]!="") 
				$address .= " ".$info["receipt_gno"];
			if($info["receipt_moo"]!="") 
				$address .= " ".$info["receipt_moo"];
			if($info["receipt_soi"]!="") 
				$address .= " ".$info["receipt_soi"];
			if($info["receipt_road"]!="") 
				$address .= " ".$info["receipt_road"];
			$address .= "<br>";
	        $address .= $info["receipt_district_name"];
	        $address .= $info["receipt_amphur_name"];
	        $address .= $info["receipt_province_name"];
	        $address .= $info["receipt_postcode"];
	        $coursetype_name = $db->data("select name from coursetype where coursetype_id={$info["coursetype_id"]}");
		//d($row);
	        $code_project = $row["code_project"];
	        $docno = $info["docno"];

		?>
		<div class="form-portrait">
			<div class="page-header">
				<div>
					<table class="no-border">
						<tbody>
							<tr>
								<td><br><span class="left"><img src="images/logo_invoice.png" alt="" /></span></td>
								<td width="30%">
									<div><?php echo $copy_page;?></div>
									<div><span class="square-line-center">ใบเสร็จรับเงิน/ใบกำกับภาษี<br>RECIPT/TAX INVOICE</span></div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div>
					<table class="no-border">
						<tbody>
							<tr>
								<td>
									<div><span class="left">อาคารชุดเลครัชดาออฟฟิศคอมเพล็กซ์2 ชั้น 5 เลขที่ 195/6 ถนนรัชดาภิเษก แขวงคลองเตย </span></div>
									<div><span class="left">เขตคลองเตย กรุงเทพมหานคร 10110 โทร 02-264-0909 โทรสาร 02-661-8505-6</span></div>
									<div><span class="left">เลขประจำตัวผู้เสียภาษีอากร 0993000132416 สาขาที่ สำนักงานใหญ่</span></div>
								</td>
								<td width="15%"></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td><span class="left">เลขที่</span></td>
								<td><span class="left"><?php echo $docno ?></span></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td><span class="left">เลขที่โครงการ</span></td>
								<td><span class="left"><?php echo $code_project; ?></span></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td><span class="left">วันที่</span></td>
								<td><span class="left"><?php echo revert_date($info["pay_date"]) ?></span></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div>
					<table class="no-border">
						<tbody>
							<tr>
								<td width="13%">
									<div><span class="left">ได้รับเงินจาก</span></div>
									<br>
									<div><span class="left">ที่อยู่</span></div>
								</td>
								<td>
									<div>
										<span class="left">
										<?php 
										$receipt_id = $info["receipt_id"];
										if($receipttype_name!="" && $info["slip_type"]=="corparation"){
											if($info["receipttype_id"]==5){
												//$receipttype_name = "";
												$name = $info["receipttype_text"]."<br>เลขที่ผู้เสียภาษี ".$info["taxno"];
											}else{
												$name = $db->data("select name from receipt where receipt_id=$receipt_id");
												$taxno = $db->data("select taxno from receipt where receipt_id=$receipt_id");
												$branch = $db->data("select branch from receipt where receipt_id=$receipt_id");
												$name = $name."<br>เลขที่ผู้เสียภาษี ".$taxno."&nbsp&nbsp&nbsp สาขา ".$branch;											}
											echo $name;
										}else{
											echo $receipt_full;
										}

										?>
										</span>
									</div>
									<div><span class="left" ><?php echo $address; ?></span></div>
								</td>

								<td width="30%">
									<div>&nbsp;</div>
									<div><span class="square-line-center">อัตราภาษี [/] อัตราร้อยละ 7 [] อัตราศูนย์</span></div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div>
					<table class="td-center">
						
						<tbody>
							<tr>
								<td><span class="center">รายการ</span></td>
								<td width="7%">จำนวน</td>
								<td width="15%"><span class="center">จำนวนเงิน</span></td>
							</tr>
						
							<?php 
							
							if($array_course){
								$run = 1;
								$runNo = 1;		
								$sum_pri_no_dis = 0;						
								foreach($array_course as $k=>$row){
									// $course_price = 0;
									// $sum_price_all_cos = 0;
									// $course_discount = 0;
									$course_id = $row['course_id'];
									foreach ($sCourse as $kkk => $vvv) {
										if($vvv==$course_id) $runNo = $kkk +1;
									}
									$code = $row['code'];
									$title = $row['title'];
									$pri_per_cos = $row['price'];
/*									
									$sum_pri_no_dis = $sum_pri_no_dis + $pri_per_cos;
									//$cos_dis = $sum_pri_no_dis - $price;
									if ($discount) {
										$cos_dis = "-".$discount;
									}else{
										$cos_dis = $sum_pri_no_dis - $price;
										$cos_dis = "-".$cos_dis;
									}
									
*/
									$t = $array_course_detail[$course_id];
									if($t) foreach ($t as $key => $v) {}

									// $sum_price_all_cos = $sum_price_all_cos+$pri_per_cos;
									//d($course_running);
									$run = !empty($course_running[$register_id][$course_id]) ? $course_running[$register_id][$course_id] : $run;	
								?>
									<tr>
										<td>
											<span class="left"><?php echo $run; ?>. ค่าสมัคร<?php echo $coursetype_name; ?>หลักสูตร <?php echo "[{$code}] {$title}"; ?><br>&nbsp;&nbsp;&nbsp;&nbsp;ผู้สมัคร<?php echo $coursetype_name; ?> : <?php echo $name_full; ?><br>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $coursetype_name; ?>วันที่ : <?php echo $v["day"]; ?></strong> <?php echo revert_date($v["date"]); ?> เวลา <?php echo $v["time"]; ?>&nbsp&nbsp&nbsp รหัสโครงการ : <?php echo $v["code_project"]; ?><br>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $v["address"]; ?> <?php echo $v["address_detail"]; ?></span>
										</td>
										<td width="7%"><span class="center">1</span></td>
										<td width="15%"><span class="right"><?php echo set_comma($pri_per_cos); ?></span></td>
									</tr>	                        
							<?php
									$run++;
									$runNo++;
								}//end loop $row 

							}//end if
							?>			
										
						</tbody>
					</table>
				</div>
<?php 
				//show footer in laste page
				if ( ($page+1) == $all_page ) {
?>

				<div>
					<table class="td-center no-border">
						<tbody>
							<tr>
								<td class="td-border-fix" rowspan="5">
									<span class="center">&nbsp;</span>
									<span class="center">จำนวนเงินทั้งสิ้น (ตัวอักษร)</span>
									<span class="center"><?php echo number_letter($tr_footer["course_price"]); ?></span>
								</td>

								<td class="td-border-fix" width="16%"><span class="right">รวมราคา&nbsp;</span></td>
								<td class="td-border-fix" width="7%"><span class="center">&nbsp;</span></td>
								<td class="td-border-fix" width="15%">
									<span class="right"><?php echo set_comma($tr_footer["cos_price_ttl"]); ?></span>
								</td>
							</tr>


							<tr>
								<td class="td-border-fix"><span class="right">ส่วนลด&nbsp;</span></td>
								<td class="td-border-fix"><span class="center">&nbsp;</span></td>
								<td class="td-border-fix">
									<span class="right"><?php echo set_comma($tr_footer["course_discount"]); ?></span>
								</td>
							</tr>

							<tr>
								<td class="td-border-fix"><span class="right">ราคาหลังหักส่วนลด&nbsp;</span></td>
								<td class="td-border-fix"><span class="center">&nbsp;</span></td>
								<td class="td-border-fix">
									<span class="right"><?php echo set_comma($tr_footer["course_price"]-$tr_footer["vat"]); ?></span>
								</td>
							</tr>

							<tr>
								<td class="td-border-fix"><span class="right">จำนวนภาษีมูลค่าเพิ่ม&nbsp;</span></td>
								<td class="td-border-fix"><span class="center">&nbsp;</span></td>
								<td class="td-border-fix">
									<span class="right"><?php echo set_comma($tr_footer["vat"]); ?></span>
								</td>
							</tr>
							<tr>
								<td class="td-border-fix"><span class="right">จำนวนเงินรวมทั้งสิ้น&nbsp;</span></td>
								<td class="td-border-fix"><span class="center">&nbsp;</span></td>
								<td class="td-border-fix">
									<span class="right"><?php echo set_comma($tr_footer["course_price"]); ?></span>
								</td>
							</tr>						
						</tbody>
					</table>
				</div>
<?php
				}//end if
 ?>
			</div>
			<div>
				<div style="margin-top:85px;">
				<table class="no-border">
					<?php 
						$cash = ($check_pay=="at_ati" || $check_pay=="importfile" || $check_pay=="walkin") ? $symbo_c : $symbo_unc;
						$transfer = ($check_pay=="at_asco" || $check_pay=="paysbuy" || $check_pay=="bill_payment" || $check_pay=="mpay") ? $symbo_c : $symbo_unc;
					 ?>
					<tbody>
						<tr>
							<td width="5%"></td>
							<td><span class="left"><?php echo $cash; ?> เงินสด</span></td>
							<td width="20%"><span class="right">&nbsp;</span></td>
							<td width="30%"><span class="center">&nbsp;</span></td>
							<td width="8%"><span class="right">&nbsp;</span></td>
							<td width="20%"></td>
						</tr>
						<tr>
							<td></td>
							<td><span class="left"><?php echo $transfer; ?> เงินโอน</span></td>
							<td><span class="right">&nbsp;</span></td>
							<td><span class="center">&nbsp;</span></td>
							<td><span class="right">&nbsp;</span></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td><span class="left"><?php echo $symbo_unc; ?> เช็ค</span></td>
							<td><span class="right">เลขที่เช็ค</span></td>
							<td><span class="dot-line-center">&nbsp;</span></td>
							<td><span class="right">ลงวันที่</span></td>
							<td><span class="dot-line-center">&nbsp;</span></td>
						</tr>
						<tr>
							<td></td>
							<td><span class="left"><?php echo $symbo_unc; ?> สาขา</span></td>
							<td><span class="right">&nbsp;</span></td>
							<td><span class="center">&nbsp;</span></td>
							<td><span class="right">&nbsp;</span></td>
							<td></td>
						</tr>
					</tbody>
				</table>
				</div>
				<table class="no-border">
					<tbody>
						<tr>
							<td width="5%"></td>
							<td width="6%"><span class="left">ผู้รับเงิน</span></td>
							<td><span class="dot-line-center">&nbsp;</span></td>
							<td width="58%"><span class="right">(การชำระโดยเช็ค จะสมบูรณ์เมื่อได้เรียกเก็บเงินจากธนาคารเรียบร้อยแล้ว)</span></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<?php 

		//} // end loop_age
		 ?>
		<!-- <br style="clear:both;page-break-before: always; "> -->
		<div style="page-break-after: always"></div>
		
<?php	
			
		  }

	   }
	}
}
?>

</body>
<script type="text/javascript">
function printPage(){
	   window.print();
	   setTimeout(" parent.$.fancybox.close()",1000);
	}
</script>
</html>