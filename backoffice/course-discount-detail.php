<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
global $db;
$course_discount_id = $_GET["course_discount_id"];
$section = datatype(" and a.active='T'", "section", true);
$coursetype = datatype(" and a.active='T'", "coursetype", true);

$q = "select  name  from course_discount where course_discount_id=$course_discount_id";
$r = $db->data($q);
$str = "";
if($course_discount_id){
	$str = 'รายละเอียด '.$r;
}else{
	$str = "เพิ่มรายการ";
}
?>
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="row">
					<div class="col-md-12">           
					<div class="block-flat">						             
						<ol class="breadcrumb">
							<li><a href="#" onClick="clearPage('<?php echo $_GET['p']."&type=discount" ?>');">หน้าหลัก</a></li>
							<li class="active"><?php echo $str; ?></li>
						</ol>					
						<div class="content">
							<form id="frmMain" name="frmMain" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="update-course-discount.php">							
							<input type="hidden" name="flag" id="flag" value="<?php echo $flag; ?>">
							<input type="hidden" name="course_discount_id" id="course_discount_id" value="<?php echo $course_discount_id; ?>">
							<div class="form-group row">
								<label class="col-sm-2 control-label">ประเภทใบอนุญาต/คุณวุฒิ <span class="red">*</span></label>
								<div class="col-sm-4">
									<select name="section_id" id="section_id" class="form-control required">
										<option value="">---- เลือก ----</option>
										<?php foreach ($section as $key => $value) {
											$id = $value['section_id'];
											if($SECTIONID>0 && $SECTIONID!=$id) continue;
											$name = $value['name'];
											echo  "<option value='$id'>$name</option>";
										} ?>

									</select>
								</div>                
								<label class="col-sm-2 control-label">ประเภทหลักสูตร<span class="red">*</span></label>
								<div class="col-sm-4">
									<select name="coursetype_id" id="coursetype_id" class="form-control required">
										<option value="">---- เลือก ----</option>
										<?php foreach ($coursetype as $key => $value) {
											$id = $value['coursetype_id'];
											$name = $value['name'];
											echo  "<option value='$id'>$name</option>";
										} ?>

									</select>
								</div>
							</div>  							
							<div class="col-sm-12">
                      
								<div class="form-group row">                                        
								<label class="col-sm-2 control-label">ส่วนลด</label>
								<div class="col-sm-2">
									<input class="form-control" name="discount" id="discount"  placeholder="ส่วนลด" type="text">
								</div>                                          
								<label class="col-sm-2 control-label">วันที่</label>
								<div class="col-sm-2">
									<input class="form-control" name="date_start" id="date_start"  placeholder="วันที่" type="text">
								</div>                                          
								<label class="col-sm-2 control-label">ถึงวันที่</label>
								<div class="col-sm-2">
									<input class="form-control" name="date_stop" id="date_stop"  placeholder="ถึงวันที่" type="text">
								</div>                                          
								</div>
								<div class="form-group row">                                            
									<label class="col-sm-1 control-label">สถานะ</label>
									<div class="col-sm-2">
										<select name="active" id="active" class="form-control required">
											<option selected="selected" value="T">แสดง</option>
											<option value="F">ไม่แสดง</option>
										</select>
									</div>                                           
								</div>
							</div>
							</div>
							<div class="clear"></div><hr>
							<div class="form-group row" style="padding-left:60px;">
								<div class="col-sm-12">
									<button type="button" class="btn btn-primary" onClick="ckSave()">Save changes</button>
									<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p'] ?>');">Cancel</button>
								</div>
							</div>
						</form>
						</div>
					</div>
					
					</div>
				</div>

		</div>
	</div> 
</div>
<?php include_once ('inc/js-script.php'); ?>
<script type="text/javascript">
	$(document).ready(function() {
	 $("#frmMain").validate();
	 var course_discount_id = "<?php echo $_GET["course_discount_id"]?>";
	 if(course_discount_id) viewInfo(course_discount_id);
	 var flag = "<?php echo $_GET["flag"]; ?>";
	 $("#flag").val(flag);
	 //$("#date").datepicker({language:'th-th',format:'dd-mm-yyyy'});
	 $("#date_start").datepicker({language:'th-th',format:'dd-mm-yyyy'});
	 $("#date_stop").datepicker({language:'th-th',format:'dd-mm-yyyy'});
	 
 });
 function viewInfo(course_discount_id){
	 if(typeof course_discount_id=="undefined") return;
	 getInfo(course_discount_id);
}

function getInfo(id){
	if(typeof id=="undefined") return;
	var url = "data/coursediscount.php";
	var param = "course_discount_id="+id+"&single=detail";
	dataUrl(url, param,"#frmMain");
}


function ckSave(id){
	onCkForm("#frmMain");
	$("#frmMain").submit();
} 
</script>