<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
global $db;
global $SECTIONID;

$error = $_SESSION["error"]["msg"];
unset($_SESSION["error"]["msg"]);
$success = $_SESSION["success"]["msg"];
unset($_SESSION["success"]["msg"]);

$course_id = $_GET["course_id"];
$section = datatype(" and a.active='T'", "section", true);
$coursetype = datatype(" and a.active='T'", "coursetype", true);
$q = "select  title from course where course_id=$course_id";
$r = $db->rows($q);
$str = "";
if($r){
	$str = $r["title"];
	$info = get_course("", $course_id);
	if($info) $info = $info[0];
}else{
	$str = "เพิ่มหลักสูตร";
}
?>
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="row">
				<div class="col-md-12">           
					<div class="block-flat">
						<div class="header">              
							<ol class="breadcrumb">
								<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>');">หน้าหลัก</a></li>
								<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>&type=select-course');">เลือกหลักสูตร</a></li>
								<li class="active">เลือกสถานที่</li>
							</ol>
						</div>
						<div class="content">
							<form id="frmMain" name="frmMain" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="update-course.php">
								<input type="hidden" name="course_id" id="course_id" value="<?php echo $course_id; ?>">
								<div class="col-sm-12">
									<div class="form-group row"> 
										<label class="col-sm-1 control-label" style="padding-right:10px;" >ชื่อหลักสูตร <span class="red" style="display:inline-block; position:absolute;"> &nbsp;*</span></label>
										<div class="col-sm-11">
											<div class="input-text">
												<?php echo $info["title"]; ?>
											</div>	
											<?php echo $info["detail"]; ?>
										</div>                               
									</div>
									<div class="form-group row" <?php if(!$course_id) echo 'style="display:none;"'; ?>>
										<h4><i class="fa  fa-clock-o"></i> &nbsp;ข้อมูลเวลา/สถานที่</h4>
										<table class="table table-striped" id="tbList">
											<thead>
												<tr class="alert alert-success" style="font-weight:bold;">
													<td width="5%">ลำดับ</td>
													<td width="8%" class="center">วัน</td>
													<td width="8%" class="center">เวลา</td>
													<td width="8%" class="center">สถานที่</td>
													<td width="28%" class="center">ข้อมูลสถานที่สอบ/อบรม</td>
													<td width="8%" class="center">เปิดรับสมัคร</td>
													<td width="8%" class="center">ปิดรับสมัคร</td>
													<td width="12%" class="center">รายงาน</td>
												</tr>
											</thead>
											<tbody>
												<?php 
												$select_year = date("Y");
												$select_month = date("m");
												$r = get_course_detail(" and a.active='T' and a.course_id=$course_id");
												if($r){
													$runNo = 1; 
													foreach($r as $k=>$v){
														?>
														<tr style="cursor:pointer;">
															<td class="center"><?php echo $runNo; ?></td>
															<td class="center"><strong style="font-size:15px;"><?php echo $v["day"]; ?></strong> <?php echo revert_date($v["date"]); ?></td>
															<td  class="center"><?php echo $v["time"]; ?></td>
															<td  class="center"><?php echo $v["address"]; ?></td>
															<td  class="center"><?php echo $v["address_detail"]; ?></td>													
															<td  class="center"><?php echo revert_date($v["open_regis"]); ?></td>
															<td  class="center"><?php echo revert_date($v["end_regis"]); ?></td>
															<td  class="center">																
																<a class="btn btn-small " data-toggle="tooltip" data-original-title="ใบเซ็นชื่อ" href="#" onclick="add_to_export(<?php echo $v["course_detail_id"]; ?>);"><i class="fa  fa-list-alt"></i></a>
															</td>
														</tr>
														<?php
														$runNo++;
													} 	
												}
												?>
												<?php 
													$con = "and a.course_id={$course_id}";
												    $r = get_course_detail_list($con);
													if($r){
														//$runNo = 1; 
														foreach($r as $k=>$v){
															?>
															<tr style="cursor:pointer;">
																<td class="center"><?php echo $runNo; ?></td>
																<td class="center"><strong style="font-size:15px;"><?php echo $v["day"]; ?></strong> <?php echo revert_date($v["date"]); ?></td>
																<td  class="center"><?php echo $v["time"]; ?></td>
																<td  class="center"><?php echo $v["address"]; ?></td>
																<td  class="center"><?php echo $v["address_detail"]; ?></td>													
																<td  class="center"><?php echo revert_date($v["open_regis"]); ?></td>
																<td  class="center"><?php echo revert_date($v["end_regis"]); ?></td>
																<td  class="center">																	
																	<a class="btn btn-small " data-toggle="tooltip" data-original-title="ใบเซ็นชื่อ" href="#" onclick="add_to_export(<?php echo $v["course_detail_id"]; ?>);"><i class="fa  fa-list-alt"></i></a>
																</td>
															</tr>
															<?php
															$runNo++;
														} 	
													}
												 ?>
												<?php 
													$sec_action = str_course_type($info["coursetype_id"]);
													$coursetype_id = $info["coursetype_id"];
													$section_id = $info["section_id"];
													$x = get_select_course($section_id, $sec_action);													
													$con = " and a.active='T' and a.coursetype_id={$coursetype_id} and a.section_id={$section_id}";
												    $r = get_course_detail($con);
													if($r && $x==4){
														//$runNo = 1; 
														foreach($r as $k=>$v){
															?>
															<tr style="cursor:pointer;">
																<td class="center"><?php echo $runNo; ?></td>
																<td class="center"><strong style="font-size:15px;"><?php echo $v["day"]; ?></strong> <?php echo revert_date($v["date"]); ?></td>
																<td  class="center"><?php echo $v["time"]; ?></td>
																<td  class="center"><?php echo $v["address"]; ?></td>
																<td  class="center"><?php echo $v["address_detail"]; ?></td>													
																<td  class="center"><?php echo revert_date($v["open_regis"]); ?></td>
																<td  class="center"><?php echo revert_date($v["end_regis"]); ?></td>
																<td  class="center">
																	
																	<a class="btn btn-small " data-toggle="tooltip" data-original-title="ใบเซ็นชื่อ" href="#" onclick="add_to_export(<?php echo $v["course_detail_id"]; ?>);"><i class="fa  fa-list-alt"></i></a>
																</td>
															</tr>
															<?php
															$runNo++;
														} 	
													}
												 ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="clear"></div>
							<div class="form-group row" style="padding-left:10px;">
								<div class="col-sm-12">
									<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p'] ?>');">กลับสู่หน้าหลัก</button>
								</div>
							</div>
						</form>
					</div>
				</div>

			</div>
		</div>

	</div>
</div> 
</div>
<?php include_once ('inc/js-script.php'); ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		
	});
	function add_to_export(id){
	    var url = "sign-train-report-print.php?course_detail_id="+id+"&type=info&course_id=<?php echo $course_id; ?>";
	   popUp(url, 1200,800);
	}

</script>
<style>
	#set_time-error{
		display: none !important;
	}
</style>