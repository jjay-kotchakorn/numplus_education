<?php
include_once "./share/authen.php";
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
global $db;
require_once dirname(__FILE__) . '/PHPExcel.php';
$info = get_course_detail(" and a.course_detail_id={$_GET["course_detail_id"]}");
$course_info = get_course(" and a.course_id={$_GET["course_id"]}");
if($info){
	$info = $info[0];
	$course_info = $course_info[0];
	$course_detail_id = $info["course_detail_id"];
	$course_id = $course_info["course_id"];
	$fileName ="";
	$apay = datatype(" and a.active='T'", "pay_status", true);
	$arr_pay = array();
	foreach ($apay as $key => $value) {
		$arr_pay[$value["pay_status_id"]] = $value["name"];
	}
?>

<!DOCTYPE html>
<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>รายชื่อผู้สอบ <?php echo $course_info["title"]; ?></title>
<link href="css/printform-style.css" rel="stylesheet" type="text/css" media="all" />
<style>
	@media print{
		body{
			padding: 0px;

		}
		#btPrint{
			display: none;
		}
	}
	.test-status-report,
	.test-status-report td span{
		line-height: 18px;
		font-size: 15px;
	}
</style>
<body>

	<div class="form-landscape">
		<div class="page-header">
			<div class="title">รายงานผลการ<?php echo $course_info["coursetype_name"]; ?>หลักสูตร<?php echo $course_info["title"]; ?></div>
			<div class="date-info">วันที่ <?php echo revert_date($info["date"])."&nbsp;เวลา ".$info["time"]; ?></div>
			<div class="detail-info"><?php echo $info['address_detail']." ".$info['address'] ?></div>
		</div>
		<table class="td-center test-status-report">
				<thead>
				<tr>
					<td width="30" rowspan="3"><span class="center">ลำดับ</span></td>
					<td width="120" rowspan="3"><span class="center">ชื่อ-สกุล</span></td>
					<td width="80" rowspan="3"><span class="center">เลขที่บัตรประชาชน</span></td>
					<td width="120" rowspan="3"><span class="center">ชื่อบริษัท</span></td>
					<td width="80" rowspan="3"><span class="center">เบอร์โทรศัพท์<br>มือถือ</span></td>
					<td rowspan="3"><span class="center">E-mail</span></td>
					<td colspan="2"><span class="center">สรุปผล<br>การเข้าอบรม</span></td>
					<td width="45" rowspan="3"><span class="center">สรุปรวม<br>ช่วงที่<br>เข้าอบรม</span></td>
					<td colspan="2"><span class="center"></span></td>
					<td width="35" rowspan="3"><span class="center">ผลการอบรม</span></td>
					<td colspan="2"><span class="center">วุฒิบัตร</span></td>
					<td width="55"><span class="center">ผลการวัดความรู้</span></td>
					<td width="55"></td>
					<td width="70"></td>
					<td width="40" rowspan="3"><span class="center">% เทียบกับคะแนน ก่อนอบรม</span></td>
					<td width="40" rowspan="3"><span class="center">% เทียบกับ คะแนนเต็ม</span></td>
					<td width="40" rowspan="3"><span class="center">หมาย<br>เหตุ</span></td>
				</tr>
				<tr>

					<td width="30"><span class="center">ช่วงที่</span></td>
					<td width="30"><span class="center">ช่วงที่</span></td>
					<td width="45"><span class="center">เวลาที่ขาด</span></td>
					<td width="40"><span class="center">180</span></td>
					<td width="30" rowspan="2"><span class="center">วันที่ออก</span></td>
					<td width="35" rowspan="2"><span class="center">สถานะ</span></td>
					<td><span class="center">คะแนนเต็ม</span></td>
					<td><span class="center">10</span></td>
					<td><span class="center">ความก้าวหน้า</span></td>
				</tr>
				<tr>

					<td><span class="center">1</span></td>
					<td><span class="center">2</span></td>
					<td><span class="center">นาที</span></td>
					<td><span class="center">%</span></td>
					<td><span class="center">ก่อนอบรม</span></td>
					<td><span class="center">หลังอบรม</span></td>
					<td><span class="center">ผลต่างคะแนน<br>ก่อน-หลัง</span></td>
				</tr>
			</thead>
			<?php 

			$ids = array();
			$q = " select register_id from register_course_detail where active='T'  and course_detail_id=$course_detail_id";
			$get_all = $db->get($q);
			if($get_all){
				foreach ($get_all as $key => $value) {
					$ids[] = $value["register_id"];
				}
			}
			$q = " select register_id from register where active='T'  and course_detail_id='$course_detail_id'";
			$get_register_all = $db->get($q);
			if($get_register_all){
				foreach ($get_register_all as $key => $value) {
					$ids[] = $value["register_id"];
				}
			}
			$t = array_unique($ids);
			$con_ids = implode(",", $t);
			$q = "select a.title,
						 a.fname,
						 a.lname,
						 a.cid,
						 a.branch,
						 b.email1,
						 b.tel_home,
						 b.tel_mobile,
						 b.tel_office,
						 a.pay_status,
						 a.course_price,
						 a.course_discount,
						 a.pay_date,
						 a.pay,
						 b.org_position,
						 c.name as org,
						 d.name as result_name,
						 a.remark
			from register a inner join member b on a.member_id=b.member_id
				left join receipt c on c.receipt_id=b.org_name
				left join register_result d on d.register_result_id=a.result
			where a.register_id in ($con_ids) and a.pay_status in (3,5,6) and a.active='T' 
			order by a.no asc";
			$r = $db->get($q);
			if($r){
				$i = 1;
			?>
			<tbody>
				<?php foreach ($r as $key => $value): ?>
					<tr>
						<td width="30"><span class="center"><?php echo $i; ?></span></td>
						<td width="120"><span class="center"><?php echo $value["title"]; ?><?php echo $value["fname"]; ?></span></td>
						<td width="80"><span class="center"><?php echo $value["cid"]; ?></span></td>
						<td width="120"><span class="center"><?php echo $value["org"]; ?></span></td>
						<td width="80"><span class="center"><?php echo $value["tel_mobile"]; ?></span></td>
						<td><span class="center"><?php echo $value["email1"]; ?></span></td>
						<td width="30"><span class="center"></span></td>
						<td width="30"><span class="center"></span></td>
						<td width="45"><span class="center"></span></td>
						<td width="45"><span class="center"></span></td>
						<td width="40"><span class="center"></span></td>
						<td width="35"><span class="center"><?php echo $value["result_name"] ?></span></td>
						<td width="30"><span class="center"></span></td>
						<td width="35"><span class="center"></span></td>
						<td width="55"><span class="center"></span></td>
						<td width="55"><span class="center"></span></td>
						<td width="70"><span class="center"></span></td>
						<td width="40"><span class="center"></span></td>
						<td width="40"><span class="center"></span></td>
						<td width="40"><span class="center"></span></td>
					</tr>
	
					<?php 
						$i++;
					endforeach ?>
			</tbody>
			<tfoot>
				<tr>
					<td rowspan="3" colspan="10" class="td-no-border"></td>
					<td width="40"><span class="center">ผ่าน</span></td>
					<td width="35"><span class="center"></span></td>
					<td width="30" class="td-no-border"></td>
					<td width="35"><span class="center" style="font-size: 14px; line-height:14px;">คะแนนเฉลี่ย</span></td>
					<td width="55"><span class="center"></span></td>
					<td width="55"><span class="center"></span></td>
					<td width="70"><span class="center"></span></td>
					<td width="40"><span class="center"></span></td>
					<td width="40"><span class="center"></span></td>
					<td width="40" class="td-no-border"></td>
				</tr>
				<tr>
					<td width="40"><span class="center">ไม่ผ่าน</span></td>
					<td width="35"><span class="center"></span></td>
					<td width="30" class="td-no-border"></td>
					<td width="35" class="td-no-border"></td>
					<td width="55" class="td-no-border"></td>
					<td width="55" class="td-no-border"></td>
					<td width="70" class="td-no-border"></td>
					<td width="40" class="td-no-border"></td>
					<td width="40" class="td-no-border"></td>
					<td width="40" class="td-no-border"></td>
				</tr>
				<tr>
					<td width="40"><span class="center" style="font-size: 14px; line-height:16x;">ไม่เข้าร่วม</span></td>
					<td width="35"><span class="center"></span></td>
					<td width="30" class="td-no-border"></td>
					<td width="35" class="td-no-border"></td>
					<td width="55" class="td-no-border"></td>
					<td width="55" class="td-no-border"></td>
					<td width="70" class="td-no-border"></td>
					<td width="40" class="td-no-border"></td>
					<td width="40" class="td-no-border"></td>
					<td width="40" class="td-no-border"></td>
				</tr>
			</tfoot>
			<?php 
			} ?>
		</table>
	</div>

</body>
<script type="text/javascript">
function printPage(){
	   window.print();
	   setTimeout(" parent.$.fancybox.close()",1000);
	}
</script>
</html>
<?php } 
?>