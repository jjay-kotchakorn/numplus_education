<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
global $db;
$section = datatype(" and a.active='T'", "section", true);
$coursetype = datatype(" and a.active='T'", "coursetype", true);
$type = $_GET["type"];
$member_id = $_GET["member_id"];
$q = "select activate_code, title_th, fname_th, cid, lname_th from member where member_id=$member_id";
$r = $db->rows($q);
$cid = $r["cid"];
$str = "ประวัติการลงทะเบียน/ชำระเงิน ".$r["title_th"]." ".$r["fname_th"]." ".$r["lname_th"];
?>
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="col-sm-12">
				<div class="content block-flat ">
					<div class="page-head">
						<h3><i class="fa fa-list"></i> &nbsp; <?php echo $str; ?></h3>
					</div>
						
<?php /*
						<div class="header">
							<div class="form-group row">
								<label class="col-sm-2 control-label">ประเภทใบอนุญาต/คุณวุฒิ <span class="red">*</span></label>
								<div class="col-sm-3">
									<select name="section_id" id="section_id" class="form-control" onchange="reCall();">
										<option value="">---- เลือก ----</option>
										<?php foreach ($section as $key => $value) {
											$id = $value['section_id'];
											if($SECTIONID>0 && $SECTIONID!=$id) continue;
											$name = $value['name'];
											echo  "<option value='$id'>$name</option>";
										} ?>

									</select>
								</div>                
								<label class="col-sm-2 control-label">ประเภทหลักสูตร<span class="red">*</span></label>
								<div class="col-sm-2">
									<select name="coursetype_id" id="coursetype_id" class="form-control" onchange="reCall();">
										<option value="">---- เลือก ----</option>
										<?php foreach ($coursetype as $key => $value) {
											$id = $value['coursetype_id'];
											$name = $value['name'];
											echo  "<option value='$id'>$name</option>";
										} ?>

									</select>
								</div>                                          
							</div> 												
						</div> 
*/?>

					<table id="tbCourse" class="table" style="width:100%">
						  <thead>
							  <tr>
								  <th style="text-align:center" width="5%">ลำดับ</th>
								  <th style="text-align:center" width="5%">ID</th>
								  <th style="text-align:center" width="10%">วันที่สมัคร</th>
								  <th width="5%">รหัสประจำตัวประชาชน</th>
								  <th width="15%">ชื่อ-นามสกุล</th>
								  <th style="text-align:center" width="10%">วัน</th>
								  <th style="text-align:center" width="10%">เวลา</th>
								  <th style="text-align:center" width="15%">สถานที่</th>
								  <th style="text-align:center" width="15%">หลักสูตร</th>
								  <th style="text-align:center" width="10%">สถานะการชำระเงิน</th>
								  <th style="text-align:center" width="10%">ผล<br>อบรม</th>
								  <th style="text-align:center" width="10%">ผลสอบ</th>
								  <!-- <th style="text-align:center" width="7%">Manage</th> -->
							  </tr>
						  </thead>   
						<tbody>
						</tbody>
					</table>
					<div class="clear"></div>
					<br>
					 <?php 
					/*วันสอบ ﻿(REGISTER_EXAM_DATE) 
					/ ชื่อย่อหลักสูตร (CODE) 
					/ หลักสูตร (COURSE_NAME) 
					/ วันที่ลงทะเบียน 
					(REGISTER_DATE) 
					/ REGISTER_REF1 
					/ REGISTER_REF2 
					/ ผลสอบ (REGISTER__RESULT)*/

											  ?>

					<?php 
					$q = "SELECT a.`﻿EXAMINATION_ID`,
							a.REGISTER_NO,
							a.REGISTER_ID,
							a.REGISTER_MEMBER_ID,
							a.REGISTER_TITLE,
							a.REGISTER_FNAME,
							a.REGISTER_LNAME,
							a.REGISTER_CID,
							a.REGISTER_SLIP,
							a.REGISTER_SLIP_NAME,
							a.REGISTER_SLIP_ADDRESS1,
							a.REGISTER_SLIP_ADDRESS2,
							a.REGISTER_DATE,
							a.REGISTER_BY,
							a.REGISTER_REF1,
							a.REGISTER_REF2,
							a.REGISTER_STATUS,
							a.REGISTER_EXPIRE_DATE,
							a.REGISTER_EXAM_DATE,
							a.REGISTER_COURSE_ID,
							a.REGISTER_COURSE_PRICE,
							a.REGISTER_PAY,
							a.REGISTER_PAY_DATE,
							a.REGISTER_PAY_METHOD,
							a.REGISTER_PAY_YR,
							a.REGISTER_PAY_MN,
							a.REGISTER_PAY_ID,
							a.REGISTER_PAY_PRICE,
							a.REGISTER_PAY_DIFF,
							a.REGISTER_APPROVE,
							a.REGISTER_APPROVE_BY,
							a.REGISTER_APPROVE_DATE,
							a.REGISTER_MOD,
							a.REGISTER__LAST_MOD,
							a.REGISTER__LAST_MOD_DATE,
							a.REGISTER__RESULT,
							a.REGISTER__RESULT_DATE,
							a.REGISTER__RESULT_BY,
							c.code,
							c.title as COURSE_NAME,
							b.EXAMINATION_PLACE,
							b.EXAMINATION_PV
					FROM register_history a left join examination b on a.﻿EXAMINATION_ID=b.EXAMINATION_ID
						 left join course_history c on a.REGISTER_COURSE_ID=c.course_id
					WHERE a.REGISTER_CID='$cid'";
					$r = $db->get($q);
					if(count($r)>0 && $r){
					 ?>
					
					<div class="page-head">
						
					</div>
					<table class="table" style="width:100%">
						<thead>
							<tr>
								<td>วันที่สอบ</td>
								<td>ชื่อย่อหลักสูตร</td>
								<td>หลักสูตร</td>
								<td>วันที่ลงทะเบียน</td>
								<td>REF1</td>
								<td>REF2</td>
								<td>ผลสอบ</td>
							</tr>
						</thead>
						<tbody>
						<?php
							/*print_r($r);*/
							if($r){
								foreach ($r as $key => $v) { 
									$a = explode("-", substr($v[﻿EXAMINATION_ID], 0,-2));
									$a[0] -= 543;

									$rs_test = $v["REGISTER__RESULT"];
									switch ($rs_test) {
										case '0':
											$rs_test = 'ไม่ทราบ';
											break;
										case '1':
											$rs_test = 'P';
											break;
										case '2':
											$rs_test = 'F';
											break;
										
										default:
											$rs_test = 'ไม่ทราบ';
											break;
									}
									?>
									<tr>
										<td style="text-align:center;"><?php echo revert_date(implode("-", $a)); ?></td>
										<td><?php echo $v["code"] ?></td>
										<td><?php echo $v["COURSE_NAME"]; ?></td>
										<td><?php echo revert_date($v["REGISTER_DATE"], true); ?></td>
										<td><?php echo $v["REGISTER_REF1"]; ?></td>
										<td><?php echo $v["REGISTER_REF2"]; ?></td>
										<td><?php echo $rs_test; ?></td>
									</tr>
								<?php
								}
							}
						?>
						</tbody>
					</table>
					<div class="clear"></div>
					<br>
				<?php } ?>
					<div class="clear"></div>
				</div>
			</div>

		</div>
	</div> 
</div>
<?php include ('inc/js-script.php') ?>

<script type="text/javascript">
$(document).ready(function() {
	var get_type = "<?php echo $_GET["type"]; ?>";
	if(get_type=="childlist" || get_type=="course_order") $("#add").hide();
	var oTable;
	var oHistory
	listItem();
	//gethistory();
	$("#section_id").change(function(event) {
		var id = $(this).val();
		getDropDown('#coursetype_id', id, 'coursetype', 'data/coursetype.php')
	});  	
});

function listItem(){
   var get_type = "<?php echo $_GET["type"]; ?>";
   var url = "data/registerlist.php";
   oHistory = $("#tbCourse").dataTable({
	   "sDom": 'T<"clear">lfrtip',
	   "oLanguage": {
   	   "sInfoEmpty": "",
   		"sInfoFiltered": ""
						  },
		"oTableTools": {
			"aButtons":  ""
		},
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": url,
		"sPaginationType": "full_numbers",
		"aaSorting": [[ 0, "desc" ]],
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			aoData.push({"name":"section_id","value":$("#section_id").val()});			
			aoData.push({"name":"coursetype_id","value":$("#coursetype_id").val()});			
			aoData.push({"name":"member_id","value": <?php echo $member_id; ?>});		
			aoData.push({"name":"type","value":get_type});
			aoData.push({"name":"active","value":'T'});
			$.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});
		}
   }); 
}


function gethistory(){
   var get_type = "<?php echo $_GET["type"]; ?>";
   var url = "data/register-history.php";
   oTable = $("#tbHistory").dataTable({
	   "sDom": 'T<"clear">lfrtip',
	   "oLanguage": {
   	   "sInfoEmpty": "",
   		"sInfoFiltered": ""
						  },
		"oTableTools": {
			"aButtons":  ""
		},
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": url,
		"sPaginationType": "full_numbers",
		"aaSorting": [[ 0, "desc" ]],
		"fnServerData": function ( sSource, aoData, fnCallback ) {		
			aoData.push({"name":"cid","value": <?php echo $cid; ?>});		
			$.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});
		}
   }); 
}

function editInfo(id){
	if(typeof id=="undefined") return;
   var url = "index.php?p=<?php echo $_GET["p"];?>&register_id="+id+"&type=info";
   redirect(url);
}


function childlist(id){
	if(typeof id=="undefined") return;
   var url = "index.php?p=<?php echo $_GET["p"];?>&register_id="+id+"&type=childdetail";
   redirect(url);
}

function addnew(){
   var url = "index.php?p=<?php echo $_GET["p"];?>&type=select-course";
   redirect(url);
}

function reCall(){
	oTable.fnClearTable( 0 );
	oTable.fnDraw();
}

function selectMember(){
   var url = "member map";
   memberpop(url,"ret_member_select");	
}

function ret_member_select(id){
	for(var i in id){
		var ck = "";
		var data = id[i];
        $("#member_id").val(data["member_id"]);
        $("#membername").val(data.name_th);
        reCall();
	}
}

function addcourse_detail_other(){
   var url = "course_detail map";
   courseDetailData(url,"ret_detail_other");	
}

function ret_detail_other(id){
	for(var i in id){
		var ck = "";
		var data = id[i];
		$("#course_detail_id").val(data.course_detail_id);
		console.log(id);
		var str = data.date+" เวลา :  "+data.time+" สถานที่ : "+data.address_detail+" "+data.address+ " จำนวนที่นั่ง : "+data.chair_all;
		$("#course_detail_name").val(str);
		reCall();
	}
}

function clearSearch(){
 $("#course_detail_name").val("");
 $("#course_detail_id").val("");
 $("#member_id").val("");
 $("#membername").val("");
 reCall();
}

function update_pay_status(){
	var t = confirm("update  สถานะ ชำระเงินแล้ว ? ");
	if(!t) return false;
    var i = 0;
    var str = "";
    $("#tbCourse tbody tr :checkbox").each(function() {
        if ($(this).is(":checked")) {
            var id = $(this).val();
            str += id+",";
        }
        i++;
    });
   
  if(str!=""){
	$.ajax({
		"type": "POST",
		"async": false, 
		"url": "data/register-status-update.php",
		"data": {'register_id': str}, 
		"success": function(data){	
			$.gritter.removeAll({
		        after_close: function(){
		          $.gritter.add({
		          	position: 'center',
			        title: 'Success',
			        text: data,
			        class_name: 'success'
			      });
		        }
		      });
 			reCall();				      							 
		}
	});
  }else{
    $("input[name=menu_order_"+id+"]").val(tmp);
  }

}
</script>
<style>
	.dataTables_filter { visibility: hidden;}
</style>