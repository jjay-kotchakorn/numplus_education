<?php
include_once "./share/authen.php";
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/course.php";
include_once "./share/datatype.php";
global $db;

function thai_to_effective_date($date){
	if(!$date) return 'NULL';
	$a = explode(" ",$date);
	$r = str_replace("/", "-", $a[0]);
	$arr_data = explode("-", $r);
	$arr_data[2] = ($arr_data[2]>2400) ? $arr_data[2]-543 : $arr_data[2];
	$t = (strlen($a[1])>5) ? substr($a[1], 0, 5) : $a[1];
	$d = $arr_data[0].$arr_data[1].$arr_data[2];
	return $d ;
}

$date_now = date('Y-m-d H:i:s');
$date = date('Y-m-d');
// var_dump($_GET);
$pay_date = $_GET["pay_date_chk"];
$pay_date_chk_file = thai_to_effective_date($pay_date);
$pay_date_chk = thai_to_timestamp($pay_date);
$pay_type_chk = $_GET["pay_type_chk"];
$pay_type_txt = ($pay_type_chk=='bank') ? 'ธนาคาร' : 'paysbuy';

$sql = "SELECT *
		FROM payment_compare_file_list AS p
		WHERE p.active='T'
			AND p.pay_date='{$pay_date_chk}'
			AND p.pay_type='{$pay_type_chk}'
";
$q = " select * from bill_payment_upload_list where effective_date='{$pay_date_chk_file}'";

$rs = $db->get($q);

// d($rs);

if ($rs) {

	foreach ($rs as $key => $f) {
		$path_file = $f["url"]."".$f["name"];
		$file = file($path_file);
		//d($file);
		foreach ($file as $key => $v) {
			$line = trim(substr($v, 0,1));
			 //var_dump($line);
			$args = array();
			if($line=="H"){
				$record_type = trim( substr($v,0,1) );
				$sequence_number = trim( substr($v,1,6) );
				$bank_code = trim( substr($v,7,3) );
				$company_account = trim( substr($v,10,10) );
				$company_name = trim( substr($v,20,40) );
				$effective_date = trim( substr($v,60,8) );
				$service_code = trim( substr($v,68,8) );
				$filler = trim( substr($v,76,180) );
/*				
				$arr_data["H"]  = array(
					'record_type' => $record_type,
					'sequence_number' => $sequence_number,
					'bank_code' => $bank_code,
					'company_account' => $company_account,
					'company_name' => $company_name,
					'effective_date' => $effective_date,
					'service_code' => $service_code,
					'filler' => $filler
				);
*/
				//set active='F'	
				$sql = "UPDATE payment_compare_bank_list
						SET active='F'
							, date_update='{$date_now}'
							, rec_by={$EMPID}
						WHERE active='T' 
							AND pay_date_chk='{$pay_date_chk}' 
							AND pay_type_chk='{$pay_type_chk}' 
							AND bank_code='{$bank_code}'
				";
				// echo $sql;
				$db->get($sql);		

				//clear dup data in TB payment_compare_all_list
				$sql = "DELETE FROM payment_compare_all_list
						WHERE active='T' 
						-- AND payment_date='{$pay_date_chk}' 
						AND pay_date_chk='{$pay_date_chk}' 
						AND payment_type='{$pay_type_chk}'
						AND bank_code='{$bank_code}'
				";
				// echo $sql;
				// $db->query($sql);			
/*
				$p_dd = substr($effective_date, 0,2);
				$p_mm = substr($effective_date, 2,2);
				$p_yy = substr($effective_date, 4,4);
				$expire_date = date("{$p_yy}-{$p_mm}-{$p_dd}");
				$full_payment_date = $payment_date." 00:00:00";
				$start = convert_mktime($full_payment_date);
				$stop = convert_mktime($expire_date." 00:00:00");
				$time = $stop - $start;
*/
/*				
				echo "<pre>";
				print_r($arr_data["H"]);
				echo "</pre>";
*/
			}else if ($line=="D") {
				$record_type = trim( substr($v,0,1) );
				$sequence_number = trim( substr($v,1,6) );
				$bank_code = trim( substr($v,7,3) );
				$company_account = trim( substr($v,10,10) );
				$payment_date = trim( substr($v,20,8) );
				$payment_time = trim( substr($v,28,6) );
				$customer_name = trim( substr($v,34,50) );
				$customer_name = iconv("TIS-620", "UTF-8", $customer_name);
				$customer_no_ref1 = trim( substr($v,84,20) );
				$ref2 = trim( substr($v,104,20) );
				$ref3 = trim( substr($v,124,20) );
				$branch_no = trim( substr($v,144,4) );
				$teller_no = trim( substr($v,148,4) );
				$transaction_type = trim( substr($v,152,1) );
				$transaction_code = trim( substr($v,153,3) );
				$cheque_number = trim( substr($v,156,7) );
				$amount = trim( substr($v,163,13) );
				$cheque_bank_code = trim( substr($v,176,3) );
				$payee_fee_same_zone = trim( substr($v,179,8) );
				$payee_fee_diff_zone = trim( substr($v,187,8) );
				$filler = trim( substr($v,195,51) );
				$new_cheque_no = trim( substr($v,246,10) );
/*
				$arr_data["D"][] = array(
					'record_type' => $record_type,
					'sequence_number' => $sequence_number,
					'bank_code' => $bank_code,
					'company_account' => $company_account,
					'payment_date' => $payment_date,
					'payment_time' => $payment_time,
					'customer_name' => $customer_name,
					'customer_no_ref1' => $customer_no_ref1,
					'ref2' => $ref2,
					'ref3' => $ref3,
					'branch_no' => $branch_no,
					'teller_no' => $teller_no,
					'transaction_type' => $transaction_type,
					'transaction_code' => $transaction_code,
					'cheque_number' => $cheque_number,
					'amount' => $amount,
					'cheque_bank_code' => $cheque_bank_code,
					'payee_fee_same_zone' => $payee_fee_same_zone,
					'payee_fee_diff_zone' => $payee_fee_diff_zone,
					'filler' => $filler,
					'new_cheque_no' => $new_cheque_no
				);	
*/
				$args["table"] = "payment_compare_bank_list";
				// $args["record_type"] = $record_type;
				$args["payment_compare_file_list_id"] = (int)$f["payment_compare_file_list_id"];
				$args["sequence_number"] = (int)$sequence_number;
				$args["bank_code"] = $bank_code;
				$args["company_account"] = $company_account;
				$args["company_name"] = $company_name;
				$args["service_code"] = $service_code;
/*
				$effective_date_dd = substr($effective_date, 0, 2);
				$effective_date_mm = substr($effective_date, 2, 2);
				$effective_date_yy = substr($effective_date, 4, 4);
				$effective_date = "{$effective_date_yy}-{$effective_date_mm}-{$effective_date_dd}";				
				$args["effective_date"] = $effective_date;
*/
				$payment_date_dd = substr($payment_date, 0, 2);
				//$args["payment_date_dd"] = $payment_date_dd;
				$payment_date_mm = substr($payment_date, 2, 2);
				//$args["payment_date_mm"] = $payment_date_mm;
				$payment_date_yy = substr($payment_date, 4, 4);
				//$args["payment_date_yy"] = $payment_date_yy;
				$payment_time_hh = substr($payment_time, 0, 2);
				$payment_time_mm = substr($payment_time, 2, 2);
				$payment_time_ss = substr($payment_time, 4, 2);
				$payment_date = "{$payment_date_yy}-{$payment_date_mm}-{$payment_date_dd}";
				$payment_time = "{$payment_time_hh}:{$payment_time_mm}:{$payment_time_ss}";

				$args["payment_date"] = $payment_date;
				$args["payment_time"] = $payment_time;
				$args["customer_name"] = $customer_name;
				$args["customer_no_ref1"] = $customer_no_ref1;
				$args["ref2"] = $ref2;
				// $args["ref3"] = $ref3;
				$args["branch_no"] = $branch_no;
				$args["teller_no"] = $teller_no;
				$args["transaction_type"] = $transaction_type;
				$args["transaction_code"] = $transaction_code;
				$args["cheque_number"] = $cheque_number;
				
				$amount = (int)$amount;
				$amount = (int)substr($amount, 0, -2);

				$args["amount"] = $amount;
				$args["cheque_bank_code"] = $cheque_bank_code;
				$args["payee_fee_same_zone"] = $payee_fee_same_zone;
				$args["payee_fee_diff_zone"] = $payee_fee_diff_zone;
				$args["filler"] = $filler;
				$args["new_cheque_no"] = $new_cheque_no;
				$args["pay_date_chk"] = $pay_date_chk;
				$args["pay_type_chk"] = $pay_type_chk;
				$args["date_create"] = $date_now;
				$args["rec_by"] = (int)$EMPID;

				$rs_id = $db->set($args);

/*				echo "<pre>";
				print_r($args);
				echo "</pre>";	*/			

			}else if ($line=="T") {
				$record_type = trim( substr($v,0,1) );
				$sequence_number = trim( substr($v,1,6) );
				$bank_code = trim( substr($v,7,3) );
				$company_account_number = trim( substr($v,10,10) );
				$total_debit_amount = trim( substr($v,20,13) );
				$total_debit_transaction = trim( substr($v,33,6) );
				$total_credit_amount = trim( substr($v,39,13) );
				$total_credit_transaction = trim( substr($v,52,6) );
				$filler = trim( substr($v,58,198) );
/*
				$arr_data["T"] = array(
					'record_type' => $record_type,
					'sequence_number' => $sequence_number,
					'bank_code' => $bank_code,
					'company_account_number' => $company_account_number,
					'total_debit_amount' => $total_debit_amount,
					'total_debit_transaction' => $total_debit_transaction,
					'total_credit_amount' => $total_credit_amount,
					'total_credit_transaction' => $total_credit_transaction,
					'filler' => $filler,
				);
*/				
			}

			// die();
		}//end loop v

		// var_dump($file);
		// echo "<br><br>";
		// die();
	}//end loop f
}else{
	echo "ไม่มีข้อมูล";
	die();
}//end else

?>

<!DOCTYPE html>
<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ผลการตรวจข้อมูลการชำระเงิน</title>
<link href="css/printform-style.css" rel="stylesheet" type="text/css" media="all" />
<style>
	@media print{
		body{
			padding: 0px;
			margin: 0px;
		}
		#btPrint{
			display: none;
		}
	}
	.rpt-print{
		table-layout: fixed;
		/*width: 1199;*/
		/*width: 100%;*/
		word-wrap:break-word;
	}
</style>
<!-- <body> -->
<body onload="receipt_tax_export()">
	<!-- <div class="form-portrait"> -->
	<div class="form-landscape">
		<div>
			<table class="no-border">
				<tbody>
					<tr>
						<td><span class="center font-weight-bold"><?php echo "ผลการตรวจข้อมูลการชำระเงิน";?></span></td>
					</tr>
					<tr>
						<td><span class="center"><?php echo "ช่องทางการชำระ : {$pay_type_txt} / วันที่ชำระ : {$pay_date}";?></span></td>
					</tr>
				</tbody>
			</table>

			<table class="td-center rpt-print" width=1200 style='table-layout:fixed'>
				<col width=30>

				<col width=60>
				<col width=50>
				<col width=120>
				<col width=120>
				<col width=80>
				<col width=50>

				<col width=40>
				<col width=120>
				<col width=70>
				<col width=50>
				<col width=50>				
				<col width=50>

				<col width=35>
				<col width=70>
				<thead>
					<tr>
						<td colspan="7">
							<span class="center font-weight-bold">ข้อมูลใบเสร็จรับเงินจาก WR</span>
						</td>
						<td colspan="6">
							<span class="center font-weight-bold">ข้อมูลการชำระจาก <?php echo $pay_type_txt?></span>
						</td>
						<td rowspan="2">
							<span class="center font-weight-bold">ผลการตรวจ</span>
						</td>
						<td rowspan="2">
							<span class="center font-weight-bold">หมายเหตุ</span>
						</td>

					</tr>

					<tr>
						<td ><span class="center">ลำดับ</span></td>					
						<td ><span class="center">เลขที่ใบเสร็จ</span></td>
						<td ><span class="center">วันที่ชำระ</span></td>
						<td ><span class="center">ชื่อผู้สมัคร</span></td>
						<td ><span class="center">หลักสูตร</span></td>
						<td ><span class="center">Project code</span></td>
						<td ><span class="center">ราคา</span></td>

						<td ><span class="right-line-center">ธนาคาร</span></td>
						<td ><span class="right-line-center">ชื่อผู้ชำระ</span></td>
						<td ><span class="center">Ref 1</span></td>
						<td ><span class="center">วันที่ชำระ</span></td>
						<td ><span class="right-line-center">เวลาที่ชำระ</span></td>
						<td ><span class="center">ยอดชำระ</span></td>
						<!-- <td ><span class="center">Y / N</span></td> -->
					</tr>
				</thead>
<?php
				$sql = "SELECT r.register_id
							, r.course_id
							, r.docno
							, r.pay_date
							, CONCAT(r.title,'',r.fname,' ',r.lname) AS cus_name
							, r.course_price
							, r.course_discount
							, r.ref1
							, r.ref2
							, r.course_detail_id
							, p.payment_compare_bank_list_id
							, p.bank_code
							, p.payment_time
							, p.customer_name
							, p.customer_no_ref1
							, p.ref2
							, p.payment_date
							, p.amount
							, p.payment_compare_file_list_id
							, p.payment_compare_file_list_id
							, b.short_name_eng
						FROM register AS r
						LEFT JOIN payment_compare_bank_list AS p ON (r.ref1=p.customer_no_ref1 AND r.ref2=p.ref2) 
							 AND p.active='T'
							 AND p.pay_date_chk='{$pay_date_chk}'
						LEFT JOIN bank AS b ON b.code=p.bank_code	
						WHERE r.active='T' 
							AND r.pay_status IN (3,9)
							AND r.pay='bill_payment' 
							AND ( r.docno IS NOT NULL OR r.docno<>'' ) 
							AND ( r.pay_date LIKE '{$pay_date_chk}%' ) 
						ORDER BY p.bank_code, p.payment_time ASC	
				";
				$rs = $db->get($sql);	
				// echo $sql;die();
// d($rs);
// die();
			$runno=0;	
			$ttl_amount=0;
			$ttl_course_price=0;
			$ttl_cus_from_wr=0;
			$ttl_cus_from_bank=0;
			foreach ($rs as $key => $v) {
				$runno++;		
				$txt_remark = "";
				if( !empty($v["register_id"]) ) $ttl_cus_from_wr++;
				if( !empty($v["payment_compare_bank_list_id"]) ) $ttl_cus_from_bank++;
				$amount = $v["amount"];
				// $amount = number_format($v["amount"]);
				$payment_date = explode(' ', $v["payment_date"]);
				$payment_date = $payment_date[0];
				$pay_date = explode(' ', $v["pay_date"]);
				$pay_date = $pay_date[0];
				$course_price = $v["course_price"]-$v["course_discount"];
				// $course_price = number_format($course_price);
				// $status_chk = ($amount-$course_price == 0) ? $status_chk='&#10003' : $status_chk='&#x2718;';

				if ( $amount-$course_price == 0 ) {
					$status_chk='&#10003;';
				}else{
					$status_chk='&#x2718;';

					if ( empty($v["customer_no_ref1"]) ) {
						$txt_remark = "ไม่พบข้อมูลผู้ชำระรายนี้ในไฟล์ที่อัพโหลด";
					}else{
						$txt_remark = "ราคาจากระบบ WR และยอดชำระที่ชำระผ่าน {$pay_type_txt} ไม่เท่ากัน";
					}//end else
				}//end else
				
				// get course name, project code				
				$sql = "SELECT a.course_id
							, a.course_detail_id
							, b.code_project
						FROM register_course_detail a left join  course_detail b on a.course_detail_id=b.course_detail_id
						WHERE a.active='T' AND a.register_id={$v["register_id"]}	
				";
				$rs = $db->get($sql);
				$course_name = "";
				$project_code = "";			
				if($rs){					
					foreach ($rs as $key => $c) {
						$info_cos = get_course("", $c["course_id"]);
						$info_cos = $info_cos[0];
						if ( !empty($info_cos) ) {
							$course_name .= $info_cos["title"]."<br><hr>";
							if ( !empty($c["code_project"]) ) {
								$project_code .= $c["code_project"];
							}else{
								$project_code .= "-";
							}
							$project_code .= "<br><hr>";
						}// end if
					}// end loop $c
				}else{
					$course_id = $v["course_id"];
					$info_cos = get_course("", $v["course_id"]);
					$info_cos = $info_cos[0];
					if ( !empty($info_cos) ) {
						$course_name .= $info_cos["title"];
					}// end if					
				}
				if(strpos(':', $v["course_detail_id"])==0 && !$project_code){
					$q = "select code_project from course_detail where course_detail_id={$v["course_detail_id"]}";
					$project_code  = $db->data($q);
				}
				$course_name = trim($course_name, "<hr>");
				$project_code = trim($project_code, "<hr>");
/*
				echo "<pre>";
				print_r($rs);
				echo "</pre>";
*/				
				//insert data to TB payment_compare_all_list
				$arg = array();
				$arg["table"] = "payment_compare_all_list";
				$arg["payment_compare_file_list_id"] = $v["payment_compare_file_list_id"];
				$arg["table_payment_compare_ref_id"] = $v["payment_compare_bank_list_id"];
				$arg["register_id"] = $v["register_id"];
				$arg["course_id"] = $v["course_id"];
				$arg["payment_date"] = $payment_date;
				$arg["payment_type"] = "{$pay_type_chk}";
				$arg["bank_code"] = $v["bank_code"];
				$arg["amount"] = (float)$v["amount"];
				$arg["check_result"] = 'Y';
				$arg["recby_id"] = (int)$EMPID;
				$arg["date_create"] = "{$date_now}";
				$arg["pay_date_chk"] = "{$pay_date_chk}";
				// d($arg);
				// $rs_add = $db->set($arg);
				// var_dump($rs_add);
				// die();
?>				
				<tbody>
					<td ><span class="center"><?php echo $runno;?></span></td>
					<td ><span class="center"><?php echo $v["docno"];?></span></td>
					<td ><span class="center"><?php echo $pay_date;?></span></td>
					<td ><span class="left"><?php echo $v["cus_name"];?></span></td>
					<td ><span class="left"><?php echo $course_name;?></span></td>
					<td ><span class="center"><?php echo $project_code;?></span></td>
					<td ><span class="right"><?php echo number_format($course_price);?></span></td>

					<td ><span class="center"><?php echo $v["short_name_eng"];?></span></td>
					<td ><span class="left"><?php echo $v["customer_name"];?></span></td>
					<td ><span class="center"><?php echo $v["customer_no_ref1"];?></span></td>
					<td ><span class="center"><?php echo $payment_date;?></span></td>
					<td ><span class="center"><?php echo $v["payment_time"];?></span></td>					
					<td ><span class="right"><?php echo number_format($amount);?></span></td>

					<td ><span class="center font-weight-bold"><?php echo $status_chk;?></span></td>
					<td ><span class="left"><?php echo $txt_remark?></span></td>
				</tbody>
<?php
				$ttl_amount = $ttl_amount+$amount;
				$ttl_course_price = $ttl_course_price+$course_price;
			}// end loop v	

				//chk status
				if ( ($ttl_amount==$ttl_course_price) && ($ttl_cus_from_wr==$ttl_cus_from_bank) ) {
					$chk_status = 'Y';
					$bg_color = "bgcolor=\"#00FF00\"";
				}else{
					$chk_status = 'N';
					$bg_color = "bgcolor=\"#FF0000\"";
/*
					$txt_status_n = "หมายเหตุ :  ";
					if ( $ttl_amount!=$ttl_course_price ) {
						$txt_status_n .= "จำนวนเงินทั้งหมดจากระบบ WR และจำนวนเงินทั้งหมดที่ชำระผ่าน {$pay_type_txt} ไม่ตรงกัน, ";
					}
					if ( $ttl_cus_from_wr!=$ttl_cus_from_bank ) {
						$txt_status_n .= "จำนวนผู้ชำระเงินจากระบบ WR และจำนวนผู้ชำระเงินจาก {$pay_type_txt} ไม่ตรงกัน, ";
					}
					$txt_status_n = trim($txt_status_n, ", ");
*/					
/*		
					$sql = "UPDATE payment_compare_all_list
							SET active='F', check_result='N', date_update='{$date_now}', recby_id={$EMPID}
							WHERE active='T' 
								AND payment_date='{$payment_date}' 
								AND payment_type='{$pay_type_chk}' 
					";
					// echo $sql;
					$db->set($sql);		
*/					
					//rs incorrect							
					$sql = "DELETE FROM payment_compare_all_list
						WHERE active='T' 
						-- AND payment_date='{$payment_date}' 
						AND payment_type='{$pay_type_chk}'
						AND pay_date_chk='{$pay_date_chk}'
					";			
					// $db->query($sql);

				}//end else
/*
				//upd note for chk_status
				$sql = "UPDATE payment_compare_file_list
						SET result='{$chk_status}'
						WHERE payment_compare_file_list_id={$v["payment_compare_file_list_id"]}
				";
				echo $sql;
				$db->query($sql);
*/
				$ttl_amount = number_format($ttl_amount);	
				$ttl_course_price = number_format($ttl_course_price);
?>
				<tbody>
					<tr>
						<td colspan="6"><span class="center">จำนวนเงินทั้งหมดจากระบบ WR</span></td>
						<td colspan=""><span class="right"><?php echo $ttl_course_price;?></span></td>				
						<td colspan="5"><span class="center"><?php echo "จำนวนเงินทั้งหมดที่ชำระผ่าน {$pay_type_txt}"?></span></td>
						<td colspan=""><span class="right"><?php echo $ttl_amount;?></span></td>
						<td rowspan="2" <?php echo $bg_color;?>><span class="center font-weight-bold"><br><?php echo $chk_status;?></span></td>
					
					</tr>
					<tr>
						<td colspan="6"><span class="center">จำนวนผู้ชำระเงินจากระบบ WR</span></td>
						<td colspan=""><span class="center"><?php echo number_format($ttl_cus_from_wr);?></span></td>
						<td colspan="5"><span class="center"><?php echo "จำนวนผู้ชำระเงินจาก {$pay_type_txt}"?></span></td>
						<td colspan=""><span class="center"><?php echo number_format($ttl_cus_from_bank);?></span></td>
					</tr>
				</tbody>

			</table>
		</div>
		<?php //echo $txt_status_n; ?>
		<br>
	</div>

</html>	

<script type="text/javascript">
function printPage(){
   window.print();
   setTimeout(" parent.$.fancybox.close()",1000);
}

function receipt_tax_export(){
	var btn_confirm = confirm("หากต้องการ Export เป็นไฟล์ Excel ให้เลือก \"OK\"");
	if (btn_confirm == true) {
		var pay_date_chk = '<?php echo $pay_date_chk; ?>';
		var pay_type_chk = '<?php echo $pay_type_chk; ?>';
		url = "./report/report-payment-compare-bank-export.php?pay_date_chk="+pay_date_chk+"&pay_type_chk="+pay_type_chk;
		// window.open(url,'_self');
		window.open(url,'_blank');
	} else {
	}//end else
}		
</script>