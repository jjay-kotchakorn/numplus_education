<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
include_once "./share/member.php";
global $db;
global $SECTIONID;
$apay = datatype(" and a.active='T'", "pay_status", true);
$educationlevel = datatype(" and a.active='T'", "educationlevel", true);
$province = datatype(" and a.active='T'", "province", true);
$receipttype = datatype(" and a.active='T'", "receipttype", true);
$university = datatype_university(true);

// d($_GET);

$register_id = $_GET["register_id"];
if(!$register_id) $register_id = $_POST["register_id"];

$section = datatype(" and a.active='T'", "section", true);
$v = get_register("", $register_id);

$str = "";
if(!empty($v)){
	$v = $v[0];
	$info = $v;
	
	// d($info);
	$pay_status_id = $info["pay_status"];
	
	$str = "ใบเสร็จเลขที่ ".$v["docno"];
	$register_id = $v["register_id"];
	$coursetype_id = $v["coursetype_id"];
	$section_id = $v["section_id"];

	$remark_new = "ออกใบลดหนี้จาก ".$str;

	if($info["slip_type"]=="individuals"){
		$slip_type_text = "ออกในนามบุคคลธรรมดา";
		$chk_receipt_type_indy = 'checked="checked"';	
		$chk_receipt_type_corp = "";
	} 
	if($info["slip_type"]=="corparation"){
		$slip_type_text = "ออกในนามนิติบุคคล (สำหรับเบิกค่าใช้จ่าย)";
		$chk_receipt_type_corp = 'checked="checked"';
		$chk_receipt_type_indy = "";
	} 
/*
	$q = "select name from receipttype where receipttype_id={$info["receipttype_id"]}";
	$receipttype_name = $db->data($q);
	$q = "select name from district where district_id={$info["receipt_district_id"]}";
	$district_name = $db->data($q);
	$q = "select name from amphur where amphur_id={$info["receipt_amphur_id"]}";
	$amphur_name = $db->data($q);
	$q = "select name from province where province_id={$info["receipt_province_id"]}";
	$province_name = $db->data($q);
	$q = "select register_id from register a where a.active='T' and  a.course_id='$course_id'";
	$id = $db->data($q);
	$total_price = $price - $discount;
	$t = convert_mktime($info["rectime"]) + (2 * 24 * 60 * 60);
	*/
	if($v["pay"]=="at_ati"){
		$pay_type = "ชำระเงินสดผ่าน ATI";
	}else if($v["pay"]=="paysbuy"){
		$pay_type = "ชำระเงินช่องทางอื่นๆ (paysbuy)";
		$print_style = "visibility:hidden";
    }else if($v["pay"]=="mpay"){
		$pay_type = "ชำระเงินช่องทางอื่นๆ (mpay)";
		$print_style = "visibility:hidden";
	}else if($v["pay"]=="bill_payment"){
		$pay_type = "ชำระเงินผ่าน Bill-payment";
	}else if($v["pay"]=="at_asco"){
		$pay_type = "เช็ค/เงินโอน";
	}else if($v["pay"]=="walkin"){
		if($info["pay_status"]==5 || $info["pay_status"]==6)
			$pay_type = $arr_pay[$info["pay_status"]];
		else	
			$pay_type = "ชำระเงินสดผ่าน ATI";
	}else if($v["pay"]=="importfile"){
		if($info["pay_status"]==5 || $info["pay_status"]==6)
			$pay_type = $arr_pay[$info["pay_status"]];
		else	
			$pay_type = "ชำระเงินสดผ่าน ATI";
	}else if ($v["pay"]=="free") {
		$pay_type = "-";
	}//end else if

	$q = "SELECT name FROM pay_status WHERE pay_status_id={$pay_status_id}";
	$pay_status = $db->data($q);
}//end if	

$credit_note_date = revert_date(date('Y-m-d'));
$credit_note_date_tt = date('H:i');

$cr_info = array();
if ( !empty($_GET["register_credit_note_id"]) ) {
	$register_credit_note_id = $_GET["register_credit_note_id"];

	$q = "SELECT
			a.register_credit_note_id,
			a.`code`,
			a.`name`,
			a.name_eng,
			a.active,
			a.ref_register_id,
			a.ref_new_register_id,
			a.runyear,
			a.runno,
			a.doc_prefix,
			a.docno,
			a.credit_note_date,
			a.ref_receipt_docno,
			a.new_receipt_docno,
			a.ref_pay_price,
			a.ref_pay_date,
			a.credit_note_price,
			a.diff_price,
			a.recby_id,
			a.rectime,
			a.remark,
			a.reason
		FROM register_credit_note a
		WHERE a.active='T'
			AND a.register_credit_note_id = {$register_credit_note_id}
	";
	$cr_info = $db->rows($q);
	// d($cr_info);
	$remark_new = $cr_info["remark"];
	$credit_note_date = strtotime($cr_info["credit_note_date"]);
	$credit_note_date = revert_date(date( 'Y-m-d', $credit_note_date ));
}//end if

?>
<link rel="stylesheet" type="text/css" href="js/jquery.nanoscroller/nanoscroller.css" />
<link href="js/jquery.icheck/skins/square/blue.css" rel="stylesheet">

<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="col-sm-12">
				<div class="content block-flat ">
					<div class="header">              
						<ol class="breadcrumb">
							<li><a href="#" onClick="clearPage('report&type=register-receipt');">หน้าหลัก</a></li>
							<li class="active"><?php echo $str; ?></li>
						</ol>
					</div>

					
					<div class="header">							

						<div class="header">							
							<h4>ข้อมูลการชำระเงิน ใบเสร็จเลขที่ <?php echo $info["docno"]; ?> </h4>
						</div>

						<div class="form-group row">	                			                
							<label class="col-sm-2 control-label">เลขที่บัตรประจำตัว 13 หลัก</label>
							<div class="col-sm-2"><?php echo $info["cid"]; ?></div>		                			                
							<label class="col-sm-2 control-label">ชื่อ-สกุล ผู้สมัคร</label>
							<div class="col-sm-2"><?php echo $info["title"]; ?> &nbsp;<?php echo $info["fname"]; ?>&nbsp;&nbsp;<?php echo $info["lname"]; ?></div>
							<label class="col-sm-2 control-label">วันที่สมัคร</label>
							<div class="col-sm-2"><?php echo revert_date($info['date']); ?></div>
						</div>

						<div class="form-group row">	                			                
							<label class="col-sm-2 control-label">รูปแบบการชำระเงิน</label>
							<div class="col-sm-2"><?php echo $pay_type; ?></div>		                			                
							<label class="col-sm-2 control-label">สถานะการชำระ</label>
							<div class="col-sm-2"><?php echo $pay_status; ?></div>
						</div>

						<div class="form-group row">	                		                
							<label class="col-sm-2 control-label">ราคา (บาท)</label>
							<div class="col-sm-2"><?php echo set_comma($info["course_price"])?> 
							</div>		                			                
							<label class="col-sm-2 control-label">ส่วนลด (บาท)</label>
							<div class="col-sm-2"><?php echo set_comma((int)$info["course_discount"]); ?> 
							</div>
						</div>										
						<hr>

					<form id="frmMain" action="update-register-credit-note.php" method="POST" accept-charset="utf-8">
						
						<input type="hidden" name="register_id" id="register_id" value="<?php echo $register_id; ?>">
						<input type="hidden" name="credit_note_price" id="credit_note_price" value="<?php echo $v['pay_price']; ?>">

						<div class="header">							
							<h4><i class="fa fa-pencil"></i>&nbsp;แก้ไขข้อมูลใบลดหนี้</h4>
												
							<div class="form-group row">
								<label class="col-sm-2 control-label" style="padding-right:0px; text-align: center;">ใบแจ้งหนี้ลงวันที่</label>
								<div class="col-sm-1" style="padding-left:0px; padding-right:10px;">
									<input class="form-control" name="receipt_date" value="<?php echo $credit_note_date ?>" id="receipt_date" placeholder="เลือกวันที่" type="text" required>							
								</div>  
								<div class="col-sm-1" style="padding-left:0px; padding-right:10px;">
									<input class="form-control" name="receipt_date_tt" id="receipt_date_tt" value="<?php echo $credit_note_date_tt;?>" placeholder="เวลา" type="text" style="width:50px; display:inline-block;">
								</div> 
								                            
							</div>			

							<div class="form-group row">
								<label class="col-sm-2 control-label" style="padding-right:0px; text-align: center;">เหตุผลในการลดหนี้</label>
								<div class="col-sm-7" style="padding-left:0px; padding-right:0px;">
									<textarea id="reason" name="reason" class="form-control" placeholder="กรุณาใส่เหตุผลของการลดหนี้"><?php echo $cr_info["reason"] ?></textarea>
								</div>		
							</div>	
							<div class="form-group row">
								<label class="col-sm-2 control-label" style="padding-right:0px; text-align: center;">หมายเหตุ</label>
								<div class="col-sm-7" style="padding-left:0px; padding-right:0px;">
									<textarea id="remark" name="remark" class="form-control" placeholder="หมายเหตุ"><?php echo $remark_new ?></textarea>
								</div>		
							</div>	

						</div>
						
						
						<div class="form-group row" style="padding-left:10px;">
							<div class="col-sm-12">
								<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p'] ?>&type=register-receipt');">กลับสู่หน้าหลัก</button>
								<button type="button" id="bt_save" class="btn btn-primary" onClick="ckSave()">บันทึกข้อมูล</button>	
								
								<a id="bt_new_receipt" class="btn btn-warning pull-right" onClick="edit_receipt_name('<?php echo $_GET['register_id']; ?>')"><i class="fa fa-paste"></i> แก้ไข / ออกใบเสร็จใหม่</a>	
								<a id="bt_print" class="btn btn-info pull-right" onClick="print_credit_note('<?php echo $_GET['register_id']; ?>')"><i class="fa fa-print"></i> Print ใบลดหนี้</a>	
							
							</div>
						</div>


					</div>
					<!-- <div class="clear"></div> -->
				</div>
			</div>

		</div>
	</div> 
</div>
<?php include ('inc/js-script.php') ?>

<script type="text/javascript">
	$(document).ready(function() {
		var fv = $("#frmMain").validate();
		var member_id = "<?php echo $info["member_id"]?>";
		var register_credit_note_id = "<?php echo $_GET["register_credit_note_id"]?>";

		if(member_id) viewInfo(member_id);
		$("#birthdate").datepicker({language:'th-th',format:'dd-mm-yyyy'});
/*		$('input').iCheck({
			checkboxClass: 'icheckbox_square-blue checkbox',
			radioClass: 'iradio_square-blue'
		});*/
		$("#receipt_date").datepicker({language:'th-th',format:'dd-mm-yyyy' });

		$('#cid').keypress(function(event){ 
			var keycode = (event.keyCode ? event.keyCode : event.which);
			if(keycode == '13'){
				getCode();
			}
		});

		if ( register_credit_note_id>0 ) {
			$("#bt_print").show();
			$("#bt_new_receipt").show();
		}else{
			$("#bt_print").hide();
			$("#bt_new_receipt").hide();
		}//end else

		//check alert
		$.ajax({
			url: 'data/ajax-check-session.php',
			data: {
				session_name: 'update_register_credit_note',
				session_type: 'alert',
				destroy: true,
			},
			type: 'GET',
			dataType: 'json',
			success: function(data){
				// console.log(data);
				// location.reload();

				if(data.status=='error'){
					$.gritter.removeAll({
						after_close: function(){
						$.gritter.add({
							position: 'center',
							title: 'Error',
							text: data.msg,
							class_name: 'danger'
						});
						}
					});
				}

				if(data.status=='success'){
					$.gritter.removeAll({
						after_close: function(){
						$.gritter.add({
							position: 'center',
							title: 'success',
							text: data.msg,
							class_name: 'success'
						});
						}
					});				
				}
			}// success
		});	 //end ajax


		var slip_type = "<?php echo $info["slip_type"]?>";
		if ( slip_type=="individuals" ) {
			$("#corparationinfo").hide();
			$("#section_corparation_receipt").hide();
			$("#section_individuals_receipt").show();
			var select = $("#receipt_id");
			var options = select.attr('options');
			$('option', select).remove();
			$(select).append('<option value="">---- เลือก ----</option>');
			$("#taxno").val('');
			$("#branch").val('');

			var receipt_title = "<?php echo $info["title"]?>";
			var fname = "<?php echo $info["fname"]?>";
			var lname = "<?php echo $info["lname"]?>";
			var receipttype_text = "<?php echo $info["receipttype_text"]?>";
			var taxno = "<?php echo $info["taxno"]?>";
			// alert(fname);
			$("#receipt_title").val(receipt_title);
			$("#receipt_fname").val(fname);
			$("#receipt_lname").val(lname);
			$("#receipt_title_text").val(receipt_title_text);
			$("#taxno").val(taxno);

		}else if( slip_type=="corparation" ){
			$("#corparationinfo").show();
			$("#section_corparation_receipt").show();
			$("#section_individuals_receipt").hide();
			$("#taxno").val('');
			$("#branch").val('');
		}//end else if
	
		$("#receipt_address").hide();
		// $("#receipt_address").show();

	});//end ready
 

	$(function(){
		$("#title_th").change(function(){
			if($(this).val() == 'นาย'){ 
				$("#title_en").val("Mr");
				$("#title_th_text").hide();
				$("#title_en_text").hide();
				if($("#title_th_text").hasClass('required'))  $("#title_th_text").removeClass('required');
				if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
				$("#gender").val("M");
			}
			else if($(this).val() == 'นาง'){ 
				$("#title_en").val("Mrs");
				$("#title_th_text").hide();
				$("#title_en_text").hide();
				if($("#title_th_text").hasClass('required'))  $("#title_th_text").removeClass('required');
				if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
				$("#gender").val("F");
			}
			else if($(this).val() == 'นางสาว'){ 
				$("#title_en").val("Ms");
				$("#title_th_text").hide();
				$("#title_en_text").hide();
				if($("#title_th_text").hasClass('required'))  $("#title_th_text").removeClass('required');
				if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
				$("#gender").val("F");
			}
			else if($(this).val() == 'อื่นๆ'){ 
				$("#title_en").val("Other");
				$("#title_th_text").show();
				$("#title_en_text").show();
				$("#title_th_text").addClass('required');
				$("#title_en_text").addClass('required');
				$("#gender").val("");
			}
		});

		$("#title_en").change(function(){
			if($(this).val() == 'Mr'){ 
				$("#title_en_text").hide();
				if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
				$("#gender").val("M");
			}
			else if($(this).val() == 'Mrs'){ 
				$("#title_en_text").hide();
				if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
				$("#gender").val("F");
			}
			else if($(this).val() == 'Ms'){ 
				$("#title_en_text").hide();
				if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
				$("#gender").val("F");
			}
			else if($(this).val() == 'Other'){ 
				$("#title_en_text").show();
				$("#title_en_text").addClass('required');
				$("#gender").val("");
			}
		});
		$("#receipt_title").change(function(){
			if($(this).val() == 'นาย'){ 
				$("#receipt_title_text").hide();
				if($("#receipt_title_text").hasClass('required'))  $("#receipt_title_text").removeClass('required');
			}
			else if($(this).val() == 'นาง'){ 
				$("#receipt_title_text").hide();
				if($("#receipt_title_text").hasClass('required'))  $("#receipt_title_text").removeClass('required');
			}
			else if($(this).val() == 'นางสาว'){ 
				$("#receipt_title_text").hide();
				if($("#receipt_title_text").hasClass('required'))  $("#receipt_title_text").removeClass('required');
			}
			else if($(this).val() == 'อื่นๆ'){ 
				$("#receipt_title_text").show();
				$("#receipt_title_text").addClass('required');
			}
		});
		$(".radio-inline").click(function(){
			$(this).find($("input[type=radio][name='slip_type']")).change(function(){   
				var t = $(this).val();
				if(t == 'corparation'){ 
					$("#taxno").addClass('required');				
				}else{ 
					if($("#taxno").hasClass('required'))  $("#taxno").removeClass('required');
				}
			});
		});
		$('input[name=show_pass]').on('ifChecked', function(event){
			$("#password, #password2").attr("type", "text");
		});			
		$('input[name=show_pass]').on('ifUnchecked', function(event){
			$("#password, #password2").attr("type", "password");
		});	

		$("input[name='receipttype_id']").on('ifChanged', function(event){
			if($(this).val() == '5'){ 
				$("#receipttype_text").addClass('required');
				$("#section_corparation_receipt").hide();
			}else{ 
				if($("#receipttype_text").hasClass('required'))  $("#receipttype_text").removeClass('required');
				$("#section_corparation_receipt").show();
			}
			$("#taxno").val('');
			$("#branch").val('');
			//checkboxaddress_reset();
		});
		$('input[name=usebook]').on('ifChecked', function(event){
			$(this).val("T");
		});		
		$('input[name=usebook]').on('ifUnchecked', function(event){
			$(this).val("F");
			//checkboxaddress_reset();
		});

});



function viewInfo(member_id){
	if(typeof member_id=="undefined") return;
	// getmemberInfo(member_id);
}

function getmemberInfo(id){
	if(typeof id=="undefined") return;
	var url = "data/memberlist.php";
	var param = "member_id="+id+"&single=T";
	$.ajax( {
		"dataType":'json', 
		"type": "POST",
		"async": false, 
		"url": url,
		"data": param, 
		"success": function(data){	
			$.each(data, function(index, array){    	     
				// console.log(array);
				$("#taxno").val(array.taxno);
				$("#branch").val(array.branch);
				$("#receipt_no").val(array.no);
				$("#receipt_gno").val(array.gno);
				$("#receipt_moo").val(array.moo);
				$("#receipt_soi").val(array.soi);
				$("#receipt_road").val(array.road);
				$("#receipt_postcode").val(array.postcode);
				var t = array.slip_type;
				corparation_info(t);
				$("select[name=receipt_province_id] option").filter(function() {
					return $(this).val() == array.province_id; 
				}).attr('selected', true);	
				var select = $("#receipt_amphur_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.receipt_amphur_id+'"> '+array.receipt_amphur_name+' </option>');	
				var select = $("#receipt_district_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.receipt_district_id+'"> '+array.receipt_district_name+' </option>');				
				var select = $("#amphur_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.amphur_id+'"> '+array.amphur_name+' </option>');	
				var select = $("#district_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.district_id+'"> '+array.district_name+' </option>');
				var select = $("#receipt_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.receipt_id+'"> '+array.receipt_name+' </option>');
				setVal("#frmMain", array);
				var select = $("#org_name");
				$('option', select).remove();
				$(select).append('<option value="'+array.org_name+'"> '+array.org_name_text+' </option>'); 
				if(!array.org_name_text){
					array.org_name_text = '-';
				}
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-blue checkbox',
					radioClass: 'iradio_square-blue'
				});
				if($("#title_th").val() == 'อื่นๆ'){ 
					$("#title_th").val(array.title_th_text);
				}	
				if($("#receipt_title").val() == 'อื่นๆ'){ 
					$("#receipt_title_text").show();
					$("#receipt_title_text").val(array.receipt_title_text);
				}				
			});	

			// console.log(array);			 
}
});
}
function corparation_click(tag){
	$(tag).find('input').iCheck('check');
	var desp = $(tag).find('input').val();
	corparation_info(desp);
}
function corparation_info(desp){
	if(desp=="corparation") {
		$("#corparationinfo").show();
		$("#section_corparation_receipt").show();
		$("#section_individuals_receipt").hide();
		$("#taxno").val('');
		$("#branch").val('');
	}else{
		$("#corparationinfo").hide();
		$("#section_corparation_receipt").hide();
		$("#section_individuals_receipt").show();
		var select = $("#receipt_id");
		var options = select.attr('options');
		$('option', select).remove();
		$(select).append('<option value="">---- เลือก ----</option>');
		$("#taxno").val('');
		$("#branch").val('');
	}
	//checkboxaddress_reset();
}

function receipt_info(id){
	if(typeof id=="undefined") return;
	var url = "data/receipt.php";
	var param = "name=receipt_detail&value="+id;
	$.ajax( {
		"dataType":'json', 
		"type": "GET",
		"async": false, 
		"url": url,
		"data": param, 
		"success": function(data){
			// console.log(data);	
			$.each(data, function(index, array){		
				// console.log(array);	 				 	
				$("#taxno").val(array.taxno);
				$("#branch").val(array.branch);
				$("#receipt_no").val(array.no);
				$("#receipt_gno").val(array.gno);
				$("#receipt_moo").val(array.moo);
				$("#receipt_soi").val(array.soi);
				$("#receipt_road").val(array.road);
				$("#receipt_postcode").val(array.postcode);
				$("select[name=receipt_province_id] option").filter(function() {
					return $(this).val() == array.province_id; 
				}).attr('selected', true);		
				var select = $("#receipt_amphur_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.amphur_id+'"> '+array.amphur_name+' </option>');	
				var select = $("#receipt_district_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.district_id+'"> '+array.district_name+' </option>');					
			});				 
		}
	});
}

function ckSave(){
	var t = confirm("ยืนยันการออกใบลดหนี้");
	if(!t) return false;
	onCkForm("#frmMain");
	$("#frmMain").submit();
}

function checkboxaddress(){

	$("#receipt_title").val($("#title_th").val());
	if($("#receipt_title").val() == 'อื่นๆ'){ 
		$("#receipt_title_text").show();
		$("#receipt_title_text").addClass('required');
		$("#receipt_title_text").val($("#title_th_text").val());
	}
	else
	{
		$("#receipt_title_text").hide();
		$("#receipt_title_text").removeClass('required');
		$("#receipt_title_text").val("");
	}
	$("#receipt_fname").val($("#fname_th").val());
	$("#receipt_lname").val($("#lname_th").val());    

	$("#taxno").val($("#cid").val());
	$("#receipt_no").val($("#no").val());
	$("#receipt_gno").val($("#gno").val());
	$("#receipt_moo").val($("#moo").val());
	$("#receipt_soi").val($("#soi").val());
	$("#receipt_road").val($("#road").val());

	$('#receipt_province_id').val($('#province_id').val());

	var select = $("#receipt_amphur_id");
	$('option', select).remove();
	$(select).append('<option value="'+$("#amphur_id").val()+'"> '+ $("#amphur_id").find(":selected").text()+' </option>'); 

	var select = $("#receipt_district_id");
	$('option', select).remove();
	$(select).append('<option value="'+$("#district_id").val()+'"> '+$("#district_id").find(":selected").text()+' </option>');  

	$("#receipt_postcode").val($("#postcode").val());

}

function checkboxaddress_reset()
{
	$("#receipt_title").val("");
	$("#receipt_title_text").hide();
	$("#receipt_title_text").removeClass('required');
	$("#receipt_title_text").val("");
	$("#receipt_fname").val("");
	$("#receipt_lname").val("");    

	$("#taxno").val("");
	$("#receipt_no").val("");
	$("#receipt_gno").val("");
	$("#receipt_moo").val("");
	$("#receipt_soi").val("");
	$("#receipt_road").val("");

	$('#receipt_province_id').val("");

	var select = $("#receipt_amphur_id");
	$('option', select).remove();
	$(select).append('<option value="">---- เลือก ----</option>');

	var select = $("#receipt_district_id");
	$('option', select).remove();
	$(select).append('<option value="">---- เลือก ----</option>');

	$("#receipt_postcode").val("");
}
function change_select_university()
{
	var id = $("#select_university").val().split("-");
	$("#grd_ugrp1").val(id[0]);
	$("#grd_uid1").val(id[1]);
	$("#grd_uname1").val($("#select_university option:selected").text());
}
function getCode(){
	var url = "data/member-info.php";
	var param = "cid="+$('#cid').val();
	$.ajax( {
		"dataType":'json', 
		"type": "POST",
		"async": false, 
		"url": url,
		"data": param, 
		"success": function(data){	
			if(data==""){
				msgError("ไม่พบข้อมูลของ "+$('#cid').val());
			}
			$.each(data, function(index, array){                
				$("#taxno").val(array.taxno);
				$("#branch").val(array.branch);
				$("#receipt_no").val(array.no);
				$("#receipt_gno").val(array.gno);
				$("#receipt_moo").val(array.moo);
				$("#receipt_soi").val(array.soi);
				$("#receipt_road").val(array.road);
				$("#receipt_postcode").val(array.postcode);
				var t = array.slip_type;
				corparation_info(t);
				$("select[name=receipt_province_id] option").filter(function() {
					return $(this).val() == array.province_id; 
				}).attr('selected', true);	
				var select = $("#receipt_amphur_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.receipt_amphur_id+'"> '+array.receipt_amphur_name+' </option>');	
				var select = $("#receipt_district_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.receipt_district_id+'"> '+array.receipt_district_name+' </option>');				
				var select = $("#amphur_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.amphur_id+'"> '+array.amphur_name+' </option>');	
				var select = $("#district_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.district_id+'"> '+array.district_name+' </option>');
				var select = $("#receipt_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.receipt_id+'"> '+array.receipt_name+' </option>');
				setVal("#frmMain", array);
				var select = $("#org_name");
				$('option', select).remove();
				$(select).append('<option value="'+array.org_name+'"> '+array.org_name_text+' </option>'); 
				if(!array.org_name_text){
					array.org_name_text = '-';
				}
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-blue checkbox',
					radioClass: 'iradio_square-blue'
				});			
			});				 
		}
	});
}


function receipt_check(use_slip){
	// var use_slip = $("#use_slip").val();
	// alert(use_slip);
	if ( use_slip=="new" ) {
		$("#receipt_address").show();
	}else if ( use_slip=="old" ) {
		$("#receipt_address").hide();
	}//end if

}//end func


function print_credit_note(id){
    var url = "receipt-print-credit-note.php?register_id="+id;
    // var url = "index.php?p=report&type=credit-note";
    window.open(url,'_blank');
}//end func

function edit_receipt_name(id){
   if(typeof id=="undefined") return;
   var url = "index.php?p=<?php echo $_GET["p"];?>&register_id="+id+"&type=receipt-name";
 	/*redirect(url);*/
 	window.open(url,'_self');
}//end func

</script>