<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
global $db;

$afile = array();
$afile[] = array('name'=>'รายละเอียดของจังหวัด', 'file'=>'documents/province.xlsx');
$afile[] = array('name'=>'ข้อมูลบริษัท', 'file'=>'documents/receipt.xlsx');
$afile[] = array('name'=>'ตัวอย่างไฟล์ สมัครแบบกลุ่ม', 'file'=>'documents/register-import.xlsx');
?>
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="block-flat">
				<div class="header">
					<h3>ดาวน์โหลดเอกสาร</h3>
				</div>
				<div class="content">
						<table class="no-border">
							<thead class="no-border">
								<tr>
									<th style="width:5%;">ลำดับ</th>
									<th>รายการ</th>
									<th class="text-right">ดาวน์โหลด</th>
								</tr>
							</thead>
							<tbody class="no-border-x no-border-y">
									<?php 
										$i = 1;
										foreach ($afile as $k => $v) { ?>
										<tr>
											<td style="width:5%;"><?php echo $i; ?></td>
											<td><?php echo $v["name"]; ?></td>
											<td class="text-right"><a href="<?php echo $v["file"]; ?>"><i class="fa fa-download"></i></a></td>
										</tr>
									<?php
										$i++;	
									}  ?>
							</tbody>
						</table>
				</div>
			</div>			
		</div>
	</div> 
</div>
<?php include ('inc/js-script.php') ?>