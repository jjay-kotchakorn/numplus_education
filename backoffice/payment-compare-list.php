<?php
include_once "./share/authen.php";
include_once "./lib/lib.php";
include_once "./connection/connection.php";
global $db;

?>
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="col-sm-12">
				<div class="content block-flat ">

					<div class="page-head form-inline">
						
						<h3><i class="fa fa-list"></i> &nbsp;ประวัติการอัพโหลดไฟล์</h3>				
					
						<div class="form-inline">							
							<label class="col-sm-7 control-label"  style="text-align: right; padding-right:10px; padding-top: 5px"><span style="color: red">เลือกวันที่จ่ายเงินก่อน upload *</span></label>
							<div class="col-sm-2"  style="padding-left:0px; padding-right:10px;">
								<input class="form-control" name="pay_date" id="pay_date" onblur="" placeholder="วันที่จ่ายเงิน" type="text">
							</div>

							<div class="col-sm-1">
								<select name="pay_type" id="pay_type" class="form-control" onchange="">
									<?php /*
									<option selected="selected" value="none">ประเภทการจ่ายเงิน</option>
									<option value="bank">ธนาคาร</option>
									*/?>
									<option value="paysbuy">paysbuy</option>
									<option value="mpay">mPAY</option>
								</select>
							</div>  				
							
							<div>
								<button class="btn btn-info btn-small pull-right" onclick="reCall()" style="margin-top:2px;"><i class="fa fa-refresh"></i></button>						
								<button class="btn btn-warning btn-small pull-right" onclick="upload()" style="margin-top:2px;"><i class="fa fa-upload"></i> upload</button>
							</div>
							<br>						
						</div>
	
					</div>

					<table id="tbNews" class="table" style="width:100%">
						<col width=5>
						<col width=30>
						<col width=100>
						<col width=50>
						<col width=50>
						<col width=80>
						<col width=50>
						<!-- <col width=70> -->
						  <thead>
							  <tr>
								  <th width="">ลำดับ</th>
								  <th width="">วันที่จ่ายเงิน</th>
								  <th width="">ชื่อไฟล์</th>
								  <th width="">ไฟล์นำเข้าจาก</th>
								  <th width="">สถานะไฟล์</th>
								  <th width="">ผู้บันทึก</th>								  
								  <th width="">วันที่อัพโหลด</th>	
								 <!--  <th width="">ผลการตรวจ</th> -->								  					 
							  </tr>
						  </thead>   
						<tbody>
						</tbody>
					</table>
					<div class="clear">
						<hr>
						<div class="form-inline">							
							<label class="col-sm-7 control-label"  style="text-align: right; padding-right:10px; padding-top: 5px"><span style="color: red">สำหรับตรวจสอบข้อมูล</span></label>
							<div class="col-sm-2"  style="padding-left:0px; padding-right:10px;">
								<input class="form-control" name="pay_date_chk" id="pay_date_chk" onblur="reCall();" placeholder="วันที่จ่ายเงิน" type="text">
							</div>

							<div class="col-sm-1">
								<select name="pay_type_chk" id="pay_type_chk" class="form-control" onchange="reCall();">
									<option selected="selected" value="none">ประเภทการจ่ายเงิน</option>
									<option value="bank">ธนาคาร</option>
									<option value="money">เงินสด</option>
									<option value="paysbuy">paysbuy</option>
									<option value="mpay">mPAY</option>
								</select>
							</div>  				
							
							<button class="btn btn-primary btn-small pull-right" onclick="compare()" style="margin-top:2px;"><i class="fa fa-check"></i> ตรวจสอบข้อมูล</button>						
							<br>						
						</div>	

					</div>
				</div>
			</div>

		</div>
	</div> 
</div>
<?php include ('inc/js-script.php') ?>

<script type="text/javascript">
$(document).ready(function() {
	var oTable;
	$("#frmMain").validate();
	$("#pay_date").datepicker({language:'th-th',format:'dd-mm-yyyy'});
	$("#pay_date_chk").datepicker({language:'th-th',format:'dd-mm-yyyy'});
	listItem();	
});

function listItem(){
   var url = "data/payment-compare-list.php";
   oTable = $("#tbNews").dataTable({
	   "sDom": 'T<"clear">lfrtip',
	   "oLanguage": {
   	   "sInfoEmpty": "",
   		"sInfoFiltered": ""
						  },
		"oTableTools": {
			"aButtons":  ""
		},
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": url,
		"sPaginationType": "full_numbers",
		"aaSorting": [[ 0, "desc" ]],
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			aoData.push({"name":"menu_id","value":$("#srchMenuId").val()});
			$.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});
		}
   }); 
}

function editInfo(id){
	if(typeof id=="undefined") return;
   var url = "index.php?p=<?php echo $_GET["p"];?>&bill_payment_upload_id="+id+"&type=info";
   redirect(url);
}
function addnew(){
   var url = "index.php?p=<?php echo $_GET["p"];?>&type=info";
   redirect(url);
}

function upload(){
	var pay_date = $("#pay_date").val();
	var pay_type = $("#pay_type").val();
	// alert(pay_type);
	if (pay_date && pay_type!='none') {		
	   	var url = "payment-compare-upload-file.php?pay_date="+pay_date+"&pay_type="+pay_type;
		popUp(url, 700, 450);
	}else{
		alert("กรุณาเลือกวันที่จ่ายเงิน และ ประเภทการจ่าเงิน");
	}//end else
}

function compare(){
	var pay_date_chk = $("#pay_date_chk").val();
	var pay_type_chk = $("#pay_type_chk").val();
	// alert(pay_type);
	if (pay_date_chk && pay_type_chk!='none') {
		if ( pay_type_chk=='bank' ) {
		   	var url = "report.php?type=payment-compare-bank&pay_date_chk="+pay_date_chk+"&pay_type_chk="+pay_type_chk;
			window.open(url, '_blank');
		}else if( pay_type_chk=='paysbuy' ){
			var url = "report.php?type=payment-compare-paysbuy&pay_date_chk="+pay_date_chk+"&pay_type_chk="+pay_type_chk;
			window.open(url, '_blank');
		}else if( pay_type_chk=='money' ){
			var url = "report.php?type=payment-compare-money&pay_date_chk="+pay_date_chk+"&pay_type_chk="+pay_type_chk;
			window.open(url, '_blank');
		}else if( pay_type_chk=='mpay' ){
			var url = "report.php?type=payment-compare-mpay&pay_date_chk="+pay_date_chk+"&pay_type_chk="+pay_type_chk;
			window.open(url, '_blank');
		}//end else if	
	}else{
		alert("กรุณาเลือกวันที่จ่ายเงิน และ ประเภทการจ่าเงิน");
	}//end else
}

function reCall(){
	oTable.fnClearTable( 0 );
	oTable.fnDraw();
}

</script>