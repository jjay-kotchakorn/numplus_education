<?php
include_once "./share/authen.php";
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
global $db;
require_once dirname(__FILE__) . '/PHPExcel.php';
$info = get_course_detail(" and a.course_detail_id={$_GET["course_detail_id"]}");
$course_info = get_course(" and a.course_id={$_GET["course_id"]}");
if($info){
	$info = $info[0];
	$course_info = $course_info[0];
	$course_detail_id = $info["course_detail_id"];
	$course_id = $course_info["course_id"];
	$fileName ="";
	$apay = datatype(" and a.active='T'", "pay_status", true);
	$arr_pay = array();
	foreach ($apay as $key => $value) {
		$arr_pay[$value["pay_status_id"]] = $value["name"];
	}
?>

<!DOCTYPE html>
<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>รายชื่อผู้สอบ <?php echo $course_info["title"]; ?></title>
<link href="css/printform-style.css" rel="stylesheet" type="text/css" media="all" />
<style>
	@media print{
		body{
			padding: 0px;

		}
		#btPrint{
			display: none;
		}
	}
</style>
<body>

	<div class="form-landscape">
		<div class="page-header">
			<div>
				<h5 class="font-weight-bold">รายงานผลการ<?php echo $course_info["coursetype_name"]; ?>หลักสูตร<?php echo $course_info["title"]; ?></h5>
			</div>
		</div>
		<table class="td-center">
			<thead>
				<tr>
					<td width="50"><span class="center">ลำดับที่</span></td>
					<td width="200"><span class="center">ชื่อ-สกุล</span></td>
					<td width="300"><span class="center">บริษัท</span></td>
					<td width="100"><span class="center">ผลการทดสอบ</span></td>
					<td><span class="center">หมายเหตุ</span></td>
				</tr>
			</thead>
			<?php 

			$ids = array();
			$q = " select register_id from register_course_detail where active='T'  and course_detail_id=$course_detail_id";
			$get_all = $db->get($q);
			if($get_all){
				foreach ($get_all as $key => $value) {
					$ids[] = $value["register_id"];
				}
			}
			$q = " select register_id from register where active='T'  and course_detail_id='$course_detail_id'";
			$get_register_all = $db->get($q);
			if($get_register_all){
				foreach ($get_register_all as $key => $value) {
					$ids[] = $value["register_id"];
				}
			}
			$t = array_unique($ids);
			$con_ids = implode(",", $t);
			$q = "select a.title,
						 a.fname,
						 a.lname,
						 a.cid,
						 a.branch,
						 b.email1,
						 b.tel_home,
						 b.tel_mobile,
						 b.tel_office,
						 a.pay_status,
						 a.course_price,
						 a.course_discount,
						 a.pay_date,
						 a.pay,
						 b.org_position,
						 c.name as org,
						 d.name as result_name,
						 a.remark,
						 a.register_id
			from register a inner join member b on a.member_id=b.member_id
				left join receipt c on c.receipt_id=b.org_name
				left join register_result d on d.register_result_id=a.result
			where a.register_id in ($con_ids) and a.pay_status in (3,5,6) and a.active='T' 
			order by a.no asc";
			$r = $db->get($q);
			if($r){
				$i = 1;
			?>
			<tbody>
				<?php foreach ($r as $key => $value):
					if($_GET["course_id"]> 0 && $_GET["course_detail_id"] > 0 && $value["register_id"]>0){
						$q = "select a.*
						,b.name as result_name 
						from register_course_detail a left join register_result b on a.register_result_id=b.register_result_id
						where a.register_id={$value["register_id"]} and a.course_id={$_GET["course_id"]} and a.course_detail_id={$_GET["course_detail_id"]}
						"; 

						$t = $db->rows($q); 
						if($t){
							$value["result_name"] = $t["result_name"];
							$value["result_date"] = $t["result_date"];
						}
					}
				 ?>
					<tr>
						<td width="40"><span class="center"><?php echo $i; ?></span></td>
						<td width="200"><span class="left"><?php echo $value["title"]; ?><?php echo $value["fname"]; ?>&nbsp;&nbsp;<?php echo $value["lname"]; ?></span></td>
						<td><span class="left"><?php echo $value["org"]; ?></span></td>
						<td width="230"><span class="center"><?php echo $value["result_name"]; ?></span></td>
						<td width="230"><span class="left"><?php echo $value["remark"]; ?></span></td>
					</tr>
		
					<?php 
						$i++;
					endforeach ?>
			</tbody>
			<?php 
			} ?>
		</table>
	</div>

</body>
<script type="text/javascript">
function printPage(){
	   window.print();
	   setTimeout(" parent.$.fancybox.close()",1000);
	}
</script>
</html>
<?php } 
?>