<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
global $db;
if($_GET["type"]=="info"){
	include_once ('courseinfo.php');
}else if($_GET["type"]=="detail"){
	include_once ('coursedetail.php');
}else if($_GET["type"]=="childdetail"){
	include_once 'course-childdetail.php';
}else if($_GET["type"]=="discount"){
	include_once 'course-discount.php';
}else if($_GET["type"]=="discount-detail"){
	include_once 'course-discount-detail.php';
}else if($_GET["type"]=="add-child"){
	include_once 'course-add-child.php';
}else if($_GET["type"]=="course-master"){
	include_once 'course-master-list.php';
}else if($_GET["type"]=="elearning"){
	include_once 'course-select-elearning.php';
}else if($_GET["type"]=="course-elearning"){
	include_once 'course-elearning.php';
}else{
	include_once 'courselist.php';
}
?>
