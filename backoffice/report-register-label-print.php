<?php
include_once "./share/authen.php";
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
global $db;
require_once dirname(__FILE__) . '/PHPExcel.php';
$info = get_course_detail(" and a.course_detail_id={$_GET["course_detail_id"]}");
$course_info = get_course(" and a.course_id={$_GET["course_id"]}");
if($info){
	$info = $info[0];
	$course_info = $course_info[0];
	$course_detail_id = $info["course_detail_id"];
	$course_id = $course_info["course_id"];
	$fileName ="";
	$apay = datatype(" and a.active='T'", "pay_status", true);
	$arr_pay = array();
	foreach ($apay as $key => $value) {
		$arr_pay[$value["pay_status_id"]] = $value["name"];
	}
?>

<!DOCTYPE html>
<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>รายชื่อผู้สอบ <?php echo $course_info["title"]; ?></title>
<link href="css/printform-style.css" rel="stylesheet" type="text/css" media="all" />
<style>
	@media print{
		body{
			padding: 0px;

		}
		#btPrint{
			display: none;
		}
	}
</style>
<body>

	<div class="form-landscape">
		<div class="page-header">
			<div>ชื่อหลักสูตร <?php echo $course_info["coursetype_name"]; ?></div>
			<div>หลักสูตร <?php echo $course_info["title"]; ?></div>
		</div>
			<?php 

			$ids = array();
			$q = " select register_id from register_course_detail where active='T'  and course_detail_id=$course_detail_id";
			$get_all = $db->get($q);
			if($get_all){
				foreach ($get_all as $key => $value) {
					$ids[] = $value["register_id"];
				}
			}
			$q = " select register_id from register where active='T'  and course_detail_id='$course_detail_id'";
			$get_register_all = $db->get($q);
			if($get_register_all){
				foreach ($get_register_all as $key => $value) {
					$ids[] = $value["register_id"];
				}
			}
			$t = array_unique($ids);
			$con_ids = implode(",", $t);
			$q = "select a.title,
						 a.fname,
						 a.lname,
						 a.cid,
						 a.branch,
						 b.email1,
						 b.tel_home,
						 b.tel_mobile,
						 b.tel_office,
						 a.pay_status,
						 a.course_price,
						 a.course_discount,
						 a.pay_date,
						 a.pay,
						 b.org_position,
						 CONCAT(b.no, ' ',
						 	CASE b.gno
						 		WHEN '' THEN ' หมู่บ้าน -'
						 		ELSE CONCAT(' หมู่บ้าน ',b.gno)
						 	END,
						 	CASE b.moo
						 		WHEN '' THEN ' หมู่ที่ -'
						 		ELSE CONCAT(' หมู่ที่ ', b.moo)
						 	END,
						 	CASE b.soi
						 		WHEN '' THEN ' ซอย -'
						 		ELSE CONCAT(' ซอย ', b.soi)
						 	END,
						 	CASE b.road
						 		WHEN '' THEN ' ถนน -'
						 		ELSE CONCAT(' ถนน ', b.road)
						 	END,
						 	' ', d.name, ' ', am.name, ' ', p.name, ' ', b.postcode) AS address,
						 c.name as org
			from register a inner join member b on a.member_id=b.member_id
				left join receipt c on c.receipt_id=b.org_name
				left join district d on b.district_id = d.district_id
				left join amphur am on b.amphur_id = am.amphur_id
				left join province p on b.province_id = p.province_id
			where a.register_id in ($con_ids) and a.pay_status in (3,5,6) and a.active='T' 
			order by a.no asc";
			$r = $db->get($q);
			$c = count($r);
			$group = array_chunk($r, 24);
			$last = count($group[count($group) - 1]);
			while ($last < 24) {
				$group[count($group)-1][] = array();
				$last++;
			}
			if($r){
				$i = 1;
			?>
			<tbody>
				<?php foreach ($group as $key => $r): ?>
					<table class="td-center tb-group">
						<tbody><tr>
						<?php 
						$runno = 0;


						/*echo "<pre>";
						print_r($r);
						echo "</pre>";
						die();*/

						foreach ($r as $key => $value) {
							if($runno>0 && $runno%4==0 && $runno!=23){
								echo "</tr><tr>";
							}
								?>
						
								<td width="25%">
									<table  class="no-border">
										<tbody>
											<tr>
												<td width="60">&nbsp;</td>
												<td></td>
												<td width="10"></td>
											</tr>
											<tr>
												<td width="60">&nbsp;ชื่อ-สกุล</td>
												<!-- <td><span class="dot-line-left"><?php echo $value["title"]; ?><?php echo $value["fname"]; ?>&nbsp;&nbsp;<?php echo $value["lname"]; ?></span></td> -->
												<td><span class=""><?php echo $value["title"]; ?><?php echo $value["fname"]; ?>&nbsp;&nbsp;<?php echo $value["lname"]; ?></span></td>
												<td width="10"></td>
											</tr>
											
											<tr>
												<td width="60">&nbsp;ที่อยู่</td>
												<!-- <td><span class="dot-line-left"></span></td> -->
												<td><span class="" style="font-size:15px;"><?php echo $value["address"]; ?></span></td>
												<td width="10"></td>
											</tr>
											<tr>
												<td width="60">&nbsp;</td>
												<td></td>
												<td width="10"></td>
											</tr>
										</tbody>
									</table>
								</td>
								<?php
							$runno++;
						} ?></tr>
						</tbody>
					</table>
		
					<?php 
						$i++;
					endforeach ?>
			</tbody>
			<?php 
			} ?>

	</div>

</body>
<script type="text/javascript">
function printPage(){
	   window.print();
	   setTimeout(" parent.$.fancybox.close()",1000);
	}
</script>
</html>
<style>
	.tb-group tr{

	}
</style>
<?php } 
?>