<?php
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";
include('share/class.upload.php');
global $db;

$date_now = date('Y-m-d H:i:s');
$date = date('Y-m-d');


$pay_type = $_REQUEST["pay_type"];
$pay_date = $_REQUEST["pay_date"];
$pay_date = thai_to_timestamp($pay_date);

// $ds = DIRECTORY_SEPARATOR;  //1 
// $storeFolder = 'uploads/payment-compare/';   //2
$uploaddir = './uploads/payment-compare/'; 
$file = $uploaddir . basename($_FILES['file']['name']); 
$file_name=($_FILES['file']['name']);
$size=$_FILES['file']['size'];
if($size>10485760000)
{
	echo "error file size > 100 MB";
	unlink($_FILES['file']['tmp_name']);
	exit;
}

$sql = "SELECT count(payment_compare_file_list_id) AS c 
	FROM payment_compare_file_list 
	WHERE file_name='{$file_name}' AND active='T'";
$chk_file = $db->data($sql);
if ($chk_file) {
	$path_file = "{$uploaddir}{$file_name}";
	if (file_exists($path_file)) {
		$rs_unlink = unlink($path_file);
	}

	$sql = "UPDATE payment_compare_file_list
		SET active='F', date_update='{$date_now}'
		WHERE active='T'
			AND file_name='{$file_name}'  
			AND pay_date='{$pay_date}'
	";
	$qry = $db->query($sql);
}

$rs = move_uploaded_file($_FILES['file']['tmp_name'], $file);
if ($rs) {
	
	$args = array();
	$args["table"] = "payment_compare_file_list";
	// $args["bill_payment_upload_id"] = $_POST["bill_payment_upload_id"];
	$args["url"] = $uploaddir;
	$args["file_name"] = $file_name;
	$args["pay_date"] = $pay_date;
	$args["pay_type"] = $pay_type;
	$args["active"] = 'T';
	$args["recby_id"] = (int)$EMPID;
	$args["rectime"] = $date_now;		
    $rs = $db->set($args);


    //write result to log
    $str_txt = "";
	$str_txt .= $date_now;

	$str_txt .= "|rs={$rs}|url={$args["url"]}|file_name={$args["file_name"]}";
	$str_txt .= "|pay_date={$pay_date}";
	$str_txt .= "|emp={$EMPID}";
	$str_txt .= "|path={$uploaddir}{$file_name}";
	$str_txt .= "|rs_unlink={$rs_unlink}";

	$str_txt .= "\r\n";

	$file_log = "./log/payment-compare-list.log";
	$file = fopen($file_log, 'a');
	fwrite($file, $str_txt);
	fclose($file);

}

die();

?>