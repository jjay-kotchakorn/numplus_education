<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
include_once "./share/member.php";
global $db;
global $SECTIONID;
$apay = datatype(" and a.active='T' and a.pay_status_id in (3,5,6) ", "pay_status", true);
// d($apay);
$educationlevel = datatype(" and a.active='T'", "educationlevel", true);
$province = datatype(" and a.active='T'", "province", true);
$receipttype = datatype(" and a.active='T'", "receipttype", true);
$university = datatype_university(true);

$error = $_SESSION["error"]["msg"];
unset($_SESSION["error"]["msg"]);
$success = $_SESSION["success"]["msg"];
unset($_SESSION["success"]["msg"]);

$course_id = $_GET["course_id"];
$course_detail_id = $_GET["course_detail_id"];
$section = datatype(" and a.active='T'", "section", true);
// d($section);
$coursetype = datatype(" and a.active='T'", "coursetype", true);
$q = "select  day, date, time , address, address_detail from course_detail where course_detail_id=$course_detail_id";
$r = $db->rows($q);
$str = "";
if($r){
	$str = $r["day"]." ".revert_date($r["date"])." ".$r["time"]." ".$r["address_detail"]." ".$r["address"];
	$info = get_course("", $course_id);
	if($info) $info = $info[0];
}else{
	$str = "เพิ่มหลักสูตร";
}


$q = "SELECT section_id, coursetype_id FROM course WHERE course_id={$course_id}";
$cos_info = $db->rows($q);
$section_id = $cos_info["section_id"];
$coursetype_id = $cos_info["coursetype_id"];

?>
<link rel="stylesheet" type="text/css" href="js/jquery.nanoscroller/nanoscroller.css" />
<link href="js/jquery.icheck/skins/square/blue.css" rel="stylesheet">
<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="row">
				<div class="col-md-12">           
					<div class="block-flat">
						<div class="header">              
							<ol class="breadcrumb">
								<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>');">หน้าหลัก</a></li>
								<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>&type=select-course');">เลือกหลักสูตร</a></li>
								<li><a href="#" onClick="clearPage('report&course_detail_id=<?php echo $course_detail_id; ?>&type=register-info&course_id=<?php echo $course_id; ?>');">เลือกสถานที่</a></li>
								<li class="active">ลงทะเบียน</li>
							</ol>
						</div>
						<div class="content">

							<form id="frmMain" name="frmMain" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="">
								<input type="hidden" name="course_id" id="course_id" value="<?php echo $course_id; ?>">
								<input type="hidden" name="course_detail_id" id="course_detail_id" value="<?php echo $course_detail_id; ?>">
								<input type="hidden" name="coursetype_id" id="coursetype_id" value="<?php echo $info["coursetype_id"]; ?>">
								<input type="hidden" name="section_id" id="section_id" value="<?php echo $info["section_id"]; ?>">
				               <div class="form-group row">
				                    <div class="col-md-12">                        
				                            <input id="scourse_id" class="" style="width:100%;" onchange="auto_select();" name="optionvalue" type="hidden" data-placeholder="ค้นหาหลักสูตร" />
				                    </div>
				                </div>
								<div class="col-sm-12">
									<div class="form-group row" <?php if(!$course_id) echo 'style="display:none;"'; ?>>
										<h4><?php echo $info["title"]; ?></h4>
										<table class="table table-striped" id="tbList">
											<thead>
												<tr class="alert alert-success" style="font-weight:bold;">
													<td width="3%">ลำดับ</td>
													<td width="8%" class="center">วัน</td>
													<td width="8%" class="center">เวลา</td>
													<td width="8%" class="center">สถานที่</td>
													<td width="15%" class="center">ข้อมูลสถานที่สอบ/อบรม</td>
													<td width="7%" class="center">ที่นั่งคงเหลือ (จอง)</td>
													<td width="5%" class="center">ที่นั่งทั้งหมด (จอง)</td>
													<td width="6%" class="center">สถานะ</td>
													<td width="6%" class="center">เปิดรับสมัคร</td>
													<td width="6%" class="center">ปิดรับสมัคร</td>
													<td width="6%" class="center"> In House</td>											
												</tr>
											</thead>
											<tbody>
											<?php 

												// d($info);
												$today = date("Y-m-d 00:00:00");
												$price = $info["price"];
												$single_discount = 0;
												$date_start = $info["date_start"];
												$date_stop = $info["date_stop"];

												if($date_start!="" && $date_stop!="" && $today>=$date_start && $today<=$date_stop){
													$single_discount = ($info["discount"]>0) ? $info["discount"] : 0;
												}//end if

												$con = " and a.active='T' and a.course_detail_id=$course_detail_id";
												$r = get_course_detail($con);
									
												if( !empty($r) ){
													$course_detail_date =  "";
													$runNo = 1; 
													$today = date("Y-m-d 00:00:00");
													$promotion = 0;
													$not_discount = false;
													$book = 0;
													foreach($r as $k=>$v){
												        $date_start = $v["date_start"];
														$date_stop = $v["date_stop"];
														$costype_id = $v["coursetype_id"];
														$sec_id = $v["section_id"];
														$discount = $v["discount"];

														if($date_start!="" && $date_stop!="" && $today>=$date_start && $today<=$date_stop){
															$promotion += $v["discount"];

															$single_discount = 0;
														}else{
															//$single_discount += $v["discount"];
														}//end else

														$price += $v['price'];
														$course_detail_id = $v["course_detail_id"];
														$use = get_use_chair($course_detail_id);
														$chair_all = $v['chair_all'];
														$book = $v["book"];
														$usebook = 0;
														if($book>0) {
															$usebook += get_use_book($course_detail_id);
														}
														$sit_all = $chair_all - $book - $use;
														$status = get_course_status_name($sit_all, $v["status"], $v["coursetype_id"]);
														if($v["date"]<date("Y-m-d")){
															$status = "ปิดรับสมัคร";
														}
														$course_detail_date = 	$v["date"];
														?>
														<tr style="cursor:pointer;">
																<td class="center"><?php echo $runNo; ?></td>
																<td class="center"><?php echo $v["day"]; ?>. <?php echo revert_date($v["date"]); ?></td>
																<td  class="center"><?php echo $v["time"]; ?></td>
																<td  class="center"><?php echo $v["address"]; ?></td>
																<td  class="center"><?php echo $v["address_detail"]; ?></td>													
																<td  class="center"><?php echo $sit_all; ?> (<?php echo (int) $book - $usebook; ?>) </td>													
																<td  class="center"><?php echo $chair_all-$book ," (", $book; ?>)</td>														
																<td  class="center"><?php echo $status; ?></td>													
																<td  class="center"><?php echo revert_date($v["open_regis"]); ?></td>
																<td  class="center"><?php echo revert_date($v["end_regis"]); ?></td>
																<td  class="center"><?php echo ($v["inhouse"]=="T") ? '<i class="fa fa-check-square-o"></i>' : ''; ?></td>
														</tr>
														<?php
														$runNo++;
													} 
													$discount = $single_discount + $promotion;	
												}

												$total_price = $price - $discount;
												?>
											</tbody>
										</table>									
									</div>
									<div class="row">
										<div class="col-md-12" >
											<div class="form-group row">
														<label class="col-sm-2 control-label">จำนวนเงินที่ต้องชำระ</label>
												<div class="col-sm-3">
													<div class="input-text">
														<?php echo set_comma($total_price); ?>
													</div>
												</div> 
											</div> 
											<input type="hidden" name="member_id" id="member_id" value="<?php echo $member_id; ?>">
											<input type="hidden" name="price" id="price" value="<?php echo $price; ?>">
											<input type="hidden" name="discount" id="discount" value="<?php echo $discount; ?>">
											<input type="hidden" name="book" id="book" value="<?php echo $book; ?>">
											<div class="form-group row">
												<div class="col-sm-12">
													<label class="col-sm-2 control-label">เลขที่บัตรประชาชน <span class="red">*</span></label>
													<div class="col-sm-3">
														<input class="form-control required cart_id" name="cid" id="cid" placeholder="รหัสประจำตัวประชาชน" type="text">
													</div>
												</div>
											</div>
											<div class="form-group row">
												<label class="control-label col-sm-1">คำนำหน้า  <span class="red">*</span></label>
												<label class="col-sm-2"> 
													<input class="form-control required" name="title_th" id="title_th"   type="text" readonly="readonly">  
												</label> 
												<label class="col-sm-1">				                			                
													<input class="form-control" id="title_th_text" name="title_th_text" placeholder="คำนำหน้า" type="text" style="max-width:100px;display: none;">
												</label>

												<label class="col-sm-1 control-label">ชื่อ <span class="red">*</span></label>
												<div class="col-sm-3">
													<input class="form-control required" name="fname_th" id="fname_th" placeholder="ชื่อ" type="text" readonly="readonly">
												</div>			                			                
												<label class="col-sm-1 control-label">นามสกุล <span class="red">*</span></label>
												<div class="col-sm-3">
													<input class="form-control required " name="lname_th" id="lname_th" placeholder="นามสกุล" type="text" readonly="readonly">
												</div>
							 				</div>
											<div class="header">							
												<h4>ข้อมูลสำหรับแสดงบนใบเสร็จรับเงิน</h4>
											</div><br>
											<div class="form-group row">						
												<div class="col-sm-12">								
													<label class="radio-inline"  onclick="corparation_click(this)" style="padding-left:10px;padding-top:0px"> <div aria-disabled="false" aria-checked="false" style="position: relative;" class="iradio_square-blue"><input style="position: absolute; opacity: 0;" value="individuals" name="slip_type" class="icheck required" type="radio"><ins style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;" class="iCheck-helper"></ins></div> ออกในนามบุคคลธรรมดา <span class="red">*</span> </label> 
													<label class="radio-inline"  onclick="corparation_click(this)" style="padding-left:10px;padding-top:0px"> <div aria-disabled="false" aria-checked="false" style="position: relative;" class="iradio_square-blue"><input style="position: absolute; opacity: 0;" value="corparation" name="slip_type" class="icheck required" type="radio"><ins style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;" class="iCheck-helper"></ins></div> ออกในนามนิติบุคคล (สำหรับเบิกค่าใช้จ่าย) <span class="red">*</span> </label> 
												</div>
											</div>

											<div id="corparationinfo" style="display: none;">										
												<div style="display:inline-block;margin-left:15px;" class="form-group row">
													<?php foreach ($receipttype as $key => $value) {
														$click = "getDropDown('#receipt_id', '{$value["receipttype_id"]}', 'receipt', 'data/receipt.php')";
														echo '<label class="radio-inline" onclick="'.$click.'" style="padding-left:10px;padding-top:0px"> <div aria-disabled="false" aria-checked="false" style="position: relative;" class="iradio_square-blue"><input style="position: absolute; opacity: 0;" value="'.$value["receipttype_id"].'" name="receipttype_id"  class="icheck" type="radio"><ins style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;" class="iCheck-helper"></ins></div>&nbsp;&nbsp;'.$value["name"].'</label>';
													} ?>
													<label class="radio-inline" style="padding-left:10px;padding-top:0px">
														<input  maxlength="200" class="form-control" name="receipttype_text" id="receipttype_text" type="text">
													</label>
												</div>	
											</div>							

											<div  id="section_corparation_receipt" style="display: none;"class="form-group row">
												<label class="control-label col-sm-2">ออกใบเสร็จในนาม</label>
												<label class="radio-inline col-sm-3" style="padding-left:10px;padding-top:0px">
													<select name="receipt_id" class="form-control" id="receipt_id" onchange="receipt_info(this.value);">
														<option value="">กรุณาเลือกจากรายการที่ปรากฎ</option>
													</select>											
												</label>
												<label class="control-label col-sm-1">สาขา </label>									
												<label class="radio-inline col-sm-2" style="padding-left:10px;padding-top:0px">
													<input  name="branch" class="form-control required" id="branch" type="text">											
												</label>
											</div>
											<div id="section_individuals_receipt" class="form-group row" style="display: none;">																
												<label class="control-label col-sm-2">ออกใบเสร็จในนาม</label>
												<div class="col-sm-1" style="padding-right:0px;">
													<select name="receipt_title" id="receipt_title" class="regis_select form-control">
														<option value="นาย">นาย</option>
														<option value="นาง">นาง</option>
														<option value="นางสาว">นางสาว</option>
														<option value="อื่นๆ">อื่นๆ</option>
													</select> </div>
													<label class="radio-inline col-sm-2" style="padding-left:10px;padding-top:0px">				                			                
														<input class="form-control" id="receipt_title_text" name="receipt_title_text" 
															placeholder="คำนำหน้า" type="text" style="max-width:150px; display:none;">
													</label>												                
													<label class="col-sm-1 control-label">ชื่อ</label>
													<div class="col-sm-2">
														<input class="form-control required" name="receipt_fname" id="receipt_fname" placeholder="ชื่อ" type="text">
													</div>			                			                
													<label class="col-sm-1 control-label">นามสกุล</label>
													<div class="col-sm-2">
														<input class="form-control required " name="receipt_lname" id="receipt_lname" placeholder="นามสกุล" type="text">
													</div>

												</div>
												<div class="form-group row">

													<label class="control-label col-sm-2" style="font-size:12px;">เลขที่บัตรประจำตัวผู้เสียภาษี <span class="red">*</span></label>									
													<label class="radio-inline col-sm-3" style="padding-left:10px;padding-top:0px">
														<input class="form-control required" name="taxno" id="taxno" maxlength="13" type="text"> 											
													</label>
												</div>
												<div class="form-group row"> 	
													<label class="col-sm-2 control-label">บ้านเลขที่  <span class="red">*</span></label>
													<div class="col-sm-2">
														<input class="form-control required" name="receipt_no" id="receipt_no" placeholder="บ้านเลขที่" type="text">
													</div>			                
													<label class="col-sm-2 control-label">หมู่บ้าน / คอนโด / อาคาร  </label>
													<div class="col-sm-2">
														<input class="form-control" name="receipt_gno" id="receipt_gno" placeholder="หมู่บ้าน / คอนโด / อาคาร" type="text">
													</div>
													<label class="col-sm-2 control-label">หมู่ที่ </label>
													<div class="col-sm-2">
														<input class="form-control " name="receipt_moo" id="receipt_moo" placeholder="หมู่ที่" type="text">
													</div>
												</div>
												<div class="form-group row">
													<label class="col-sm-2 control-label">ซอย</label>
													<div class="col-sm-2">
														<input class="form-control " name="receipt_soi" id="receipt_soi" placeholder="ซอย" type="text">
													</div>
													<label class="col-sm-2 control-label">ถนน </label>
													<div class="col-sm-2">
														<input class="form-control " name="receipt_road" id="receipt_road" placeholder="ถนน " type="text">
													</div>
												</div>							  		
												<div class="form-group row">
													<label class="col-sm-2 control-label">จังหวัด  <span class="red">*</span></label>
													<div class="col-sm-2">
														<select name="receipt_province_id" id="receipt_province_id" class="form-control required" 
															onchange="getDropDown('#receipt_amphur_id', this.value, 'amphur', 'data/province.php')"
															onclick="getDropDown('#receipt_amphur_id', this.value, 'amphur', 'data/province.php')">
															<option value="">---- เลือก ----</option>
															<?php foreach ($province as $key => $value) {
																$id = $value['province_id'];
																$name = $value['name'];
																echo  "<option value='$id'>$name</option>";
															} ?>

														</select>
													</div>
													<label class="col-sm-2 control-label">อำเภอ  <span class="red">*</span></label>
													<div class="col-sm-2">
														<select name="receipt_amphur_id"  id="receipt_amphur_id" class="form-control required" 
															onchange="getDropDown('#receipt_district_id', this.value, 'district', 'data/province.php')"
															onclick="getDropDown('#receipt_district_id', this.value, 'district', 'data/province.php')">
															<option value="">---- เลือก ----</option>								
														</select>
													</div>
													<label class="col-sm-2 control-label">ตำบล/แขวง  <span class="red">*</span></label>
													<div class="col-sm-2">
														<select name="receipt_district_id" id="receipt_district_id" class="form-control required">
															<option value="">---- เลือก ----</option>								
														</select>
													</div>
												</div>
												<div class="form-group row">								
													<label class="col-sm-2 control-label">รหัสไปรษณีย์  <span class="red">*</span></label>
													<div class="col-sm-2">
														<input  id="receipt_postcode" class="form-control required" name="receipt_postcode" maxlength="6" type="text">
													</div>
												</div>
												<div style="display:inline-block;margin-left:15px;" class="form-group row">
													<?php foreach ($apay as $key => $value) {
														
														echo '<label class="radio-inline"  style="padding-left:10px;padding-top:0px"> <div aria-disabled="false" aria-checked="false" style="position: relative;" class="iradio_square-blue"><input style="position: absolute; opacity: 0;" value="'.$value["pay_status_id"].'" name="pay_status"  class="icheck required" type="radio"><ins style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;" class="iCheck-helper"></ins></div>&nbsp;&nbsp;'.$value["name"].'  <span class="red">*</span></label>';
													} ?>
													&nbsp;&nbsp;
													<label class="checkbox-inline" style="padding-left:10px;padding-top:0px; <?php echo ($book<=0) ? "display:none; " : ""; ?> "> <div aria-disabled="false" aria-checked="false" style="position: relative;" class="icheckbox_square-blue"><input style="position: absolute; opacity: 0;" value="" name="usebook" id="usebook" class="icheck" type="checkbox"><ins style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;" class="iCheck-helper"></ins></div>&nbsp;&nbsp;ลงทะเบียนจากสำรองที่นั่ง</label>
												</div>	
											</div>
										</div>
									</div>
								</div>
								<div class="clear"></div>
								<div class="form-group row" style="padding-left:10px;">
									<div class="col-sm-12">
										<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p'] ?>');">กลับสู่หน้าหลัก</button>
										<button type="button" id="bt_save" class="btn btn-primary" onClick="ckSave()">ลงทะเบียน</button>										
										<button type="button" class="btn btn-primary pull-right" onClick="clearPage('<?php echo $_GET['p'] ?>&course_detail_id=<?php echo $course_detail_id; ?>&type=walk-in&course_id=<?php echo $course_id; ?>')">ลงทะเบียนเพิ่ม</button>										
									</div>
								</div>
							</form>
						</div>
					</div>

				</div>
			</div>

		</div>
	</div> 
</div>
<?php include_once ('inc/js-script.php'); ?>


<script type="text/javascript">
	$(document).ready(function() {
		var fv = $("#frmMain").validate();
		var member_id = "<?php echo $_GET["member_id"]?>";
		if(member_id) viewInfo(member_id);
		$("#birthdate").datepicker({language:'th-th',format:'dd-mm-yyyy'});
/*		$('input').iCheck({
			checkboxClass: 'icheckbox_square-blue checkbox',
			radioClass: 'iradio_square-blue'
		});*/

		$('#cid').keypress(function(event){ 
			var keycode = (event.keyCode ? event.keyCode : event.which);
			if(keycode == '13'){
				var rs_cd = chk_block_list();
				// alert(rs_cd);
				if ( rs_cd==3000 || rs_cd=='3000' ) {
					// getCode();
				}//end if
			}//end if

		});
        $('#scourse_id').select2({
            minimumInputLength: 3,
            ajax: {
              url: "data/course-optionlist.php",
              dataType: 'json',
              data: function (term, page) {
                return {
                  q: term
                };
              },
              results: function (data, page) {
                return { results: data };
              }
            }
          });
		$("#frmMain").on("submit", function(event) {
			event.preventDefault();
			if(!fv.valid()){
				return false;
			}
		    var url = "data/walk-in-register-update.php";
		    onCkForm("#frmMain");
		    var formData = $("#frmMain").serializeArray();
		    $.post(url, formData).done(function (data) {
		    	if(data==0){

		    	}else if(data==4){
		    		msgError("ลงทะเบียน ซ้ำ");
		    		return;
		    	}else if(data>0){
		    		var register_id = data;
		    		// var pay_status = $("#pay_status").val();
		    		$.ajax({
						"type": "POST",
						"async": false, 
						"url": "check-course-elearning.php",
						"data": {'register_id': register_id
							, 'pay_type': 'walk-in'
						}, 
						"success": function(data){	
							console.log(data);			      							 
						}
					}); 

		    		$("#bt_save").hide();
		       	    msgSuccess("ลงทะเบียนเรียบร้อยแล้ว");
		       	    setTimeout(function(){back_to_info()}, 1000);
		       }
		    });
		});
		$(function(){
			$("#title_th").change(function(){
				if($(this).val() == 'นาย'){ 
					$("#title_en").val("Mr");
					$("#title_th_text").hide();
					$("#title_en_text").hide();
					if($("#title_th_text").hasClass('required'))  $("#title_th_text").removeClass('required');
					if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
					$("#gender").val("M");
				}
				else if($(this).val() == 'นาง'){ 
					$("#title_en").val("Mrs");
					$("#title_th_text").hide();
					$("#title_en_text").hide();
					if($("#title_th_text").hasClass('required'))  $("#title_th_text").removeClass('required');
					if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
					$("#gender").val("F");
				}
				else if($(this).val() == 'นางสาว'){ 
					$("#title_en").val("Ms");
					$("#title_th_text").hide();
					$("#title_en_text").hide();
					if($("#title_th_text").hasClass('required'))  $("#title_th_text").removeClass('required');
					if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
					$("#gender").val("F");
				}
				else if($(this).val() == 'อื่นๆ'){ 
					$("#title_en").val("Other");
					$("#title_th_text").show();
					$("#title_en_text").show();
					$("#title_th_text").addClass('required');
					$("#title_en_text").addClass('required');
					$("#gender").val("");
				}
			});

$("#title_en").change(function(){
	if($(this).val() == 'Mr'){ 
		$("#title_en_text").hide();
		if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
		$("#gender").val("M");
	}
	else if($(this).val() == 'Mrs'){ 
		$("#title_en_text").hide();
		if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
		$("#gender").val("F");
	}
	else if($(this).val() == 'Ms'){ 
		$("#title_en_text").hide();
		if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
		$("#gender").val("F");
	}
	else if($(this).val() == 'Other'){ 
		$("#title_en_text").show();
		$("#title_en_text").addClass('required');
		$("#gender").val("");
	}
});
$("#receipt_title").change(function(){
	if($(this).val() == 'นาย'){ 
		$("#receipt_title_text").hide();
		if($("#receipt_title_text").hasClass('required'))  $("#receipt_title_text").removeClass('required');
	}
	else if($(this).val() == 'นาง'){ 
		$("#receipt_title_text").hide();
		if($("#receipt_title_text").hasClass('required'))  $("#receipt_title_text").removeClass('required');
	}
	else if($(this).val() == 'นางสาว'){ 
		$("#receipt_title_text").hide();
		if($("#receipt_title_text").hasClass('required'))  $("#receipt_title_text").removeClass('required');
	}
	else if($(this).val() == 'อื่นๆ'){ 
		$("#receipt_title_text").show();
		$("#receipt_title_text").addClass('required');
	}
});
$(".radio-inline").click(function(){
	$(this).find($("input[type=radio][name='slip_type']")).change(function(){   
		var t = $(this).val();
		if(t == 'corparation'){ 
			$("#taxno").addClass('required');				
		}else{ 
			if($("#taxno").hasClass('required'))  $("#taxno").removeClass('required');
		}
	});
});
$('input[name=show_pass]').on('ifChecked', function(event){
	$("#password, #password2").attr("type", "text");
});			
$('input[name=show_pass]').on('ifUnchecked', function(event){
	$("#password, #password2").attr("type", "password");
});	

$("input[name='receipttype_id']").on('ifChanged', function(event){
	if($(this).val() == '5'){ 
		$("#receipttype_text").addClass('required');
		$("#section_corparation_receipt").hide();
	}else{ 
		if($("#receipttype_text").hasClass('required'))  $("#receipttype_text").removeClass('required');
		$("#section_corparation_receipt").show();
	}
	$("#taxno").val('');
	$("#branch").val('');
	//checkboxaddress_reset();
});
$('input[name=usebook]').on('ifChecked', function(event){
	$(this).val("T");
});		
$('input[name=usebook]').on('ifUnchecked', function(event){
	$(this).val("F");
	//checkboxaddress_reset();
});

});

});

function viewInfo(member_id){
	if(typeof member_id=="undefined") return;
	getmemberInfo(member_id);
}

function getmemberInfo(id){
	if(typeof id=="undefined") return;
	var url = "data/memberlist.php";
	var param = "member_id="+id+"&single=T";
	$.ajax( {
		"dataType":'json', 
		"type": "POST",
		"async": false, 
		"url": url,
		"data": param, 
		"success": function(data){	
			$.each(data, function(index, array){                
				$("#taxno").val(array.taxno);
				$("#branch").val(array.branch);
				$("#receipt_no").val(array.no);
				$("#receipt_gno").val(array.gno);
				$("#receipt_moo").val(array.moo);
				$("#receipt_soi").val(array.soi);
				$("#receipt_road").val(array.road);
				$("#receipt_postcode").val(array.postcode);
				var t = array.slip_type;
				corparation_info(t);
				//console.log(array);
				$("select[name=receipt_province_id] option").filter(function() {
					return $(this).val() == array.province_id; 
				}).attr('selected', true);	
				var select = $("#receipt_amphur_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.receipt_amphur_id+'"> '+array.receipt_amphur_name+' </option>');	
				var select = $("#receipt_district_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.receipt_district_id+'"> '+array.receipt_district_name+' </option>');				
				var select = $("#amphur_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.amphur_id+'"> '+array.amphur_name+' </option>');	
				var select = $("#district_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.district_id+'"> '+array.district_name+' </option>');
				var select = $("#receipt_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.receipt_id+'"> '+array.receipt_name+' </option>');
				setVal("#frmMain", array);
				var select = $("#org_name");
				$('option', select).remove();
				$(select).append('<option value="'+array.org_name+'"> '+array.org_name_text+' </option>'); 
				if(!array.org_name_text){
					array.org_name_text = '-';
				}
				
			});				 
}
});
}
function corparation_click(tag){
	$(tag).find('input').iCheck('check');
	var desp = $(tag).find('input').val();
	//console.log(desp);
	corparation_info(desp);
}
function corparation_info(desp){
	if(desp=="corparation") {
		$("#corparationinfo").show();
		$("#section_corparation_receipt").show();
		$("#section_individuals_receipt").hide();
		$("#taxno").val('');
		$("#branch").val('');
	}else{
		$("#corparationinfo").hide();
		$("#section_corparation_receipt").hide();
		$("#section_individuals_receipt").show();
		var select = $("#receipt_id");
		var options = select.attr('options');
		$('option', select).remove();
		$(select).append('<option value="">---- เลือก ----</option>');
		$("#taxno").val('');
		$("#branch").val('');
	}
	//checkboxaddress_reset();
}

function receipt_info(id){
	if(typeof id=="undefined") return;
	var url = "data/receipt.php";
	var param = "name=receipt_detail&value="+id;
	$.ajax( {
		"dataType":'json', 
		"type": "GET",
		"async": false, 
		"url": url,
		"data": param, 
		"success": function(data){
			//console.log(data);	
			$.each(data, function(index, array){			 				 	
				$("#taxno").val(array.taxno);
				$("#branch").val(array.branch);
				$("#receipt_no").val(array.no);
				$("#receipt_gno").val(array.gno);
				$("#receipt_moo").val(array.moo);
				$("#receipt_soi").val(array.soi);
				$("#receipt_road").val(array.road);
				$("#receipt_postcode").val(array.postcode);
				$("select[name=receipt_province_id] option").filter(function() {
					return $(this).val() == array.province_id; 
				}).attr('selected', true);		
				var select = $("#receipt_amphur_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.amphur_id+'"> '+array.amphur_name+' </option>');	
				var select = $("#receipt_district_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.district_id+'"> '+array.district_name+' </option>');					
			});				 
		}
	});
}
function removeImg(){
	var defaultImg = "images/no-avatar-male.jpg";
	if($('input[name=tmpimg]').val()=="")return;
	var t = confirm("ลบรูปภาพ");
	if(!t) return;
	$('#img').attr('src', defaultImg);
	$('input[name=delimg]').val("T");	
	ckSave();
}
function ckSave(id){
	var t = confirm("ยืนยันการลงทะเบียน");
	if(!t) return false;
	$("#frmMain").submit();
}

function checkboxaddress(){

	$("#receipt_title").val($("#title_th").val());
	if($("#receipt_title").val() == 'อื่นๆ'){ 
		$("#receipt_title_text").show();
		$("#receipt_title_text").addClass('required');
		$("#receipt_title_text").val($("#title_th_text").val());
	}
	else
	{
		$("#receipt_title_text").hide();
		$("#receipt_title_text").removeClass('required');
		$("#receipt_title_text").val("");
	}
	$("#receipt_fname").val($("#fname_th").val());
	$("#receipt_lname").val($("#lname_th").val());    

	$("#taxno").val($("#cid").val());
	$("#receipt_no").val($("#no").val());
	$("#receipt_gno").val($("#gno").val());
	$("#receipt_moo").val($("#moo").val());
	$("#receipt_soi").val($("#soi").val());
	$("#receipt_road").val($("#road").val());

	$('#receipt_province_id').val($('#province_id').val());

	var select = $("#receipt_amphur_id");
	$('option', select).remove();
	$(select).append('<option value="'+$("#amphur_id").val()+'"> '+ $("#amphur_id").find(":selected").text()+' </option>'); 

	var select = $("#receipt_district_id");
	$('option', select).remove();
	$(select).append('<option value="'+$("#district_id").val()+'"> '+$("#district_id").find(":selected").text()+' </option>');  

	$("#receipt_postcode").val($("#postcode").val());

}

function checkboxaddress_reset()
{
	$("#receipt_title").val("");
	$("#receipt_title_text").hide();
	$("#receipt_title_text").removeClass('required');
	$("#receipt_title_text").val("");
	$("#receipt_fname").val("");
	$("#receipt_lname").val("");    

	$("#taxno").val("");
	$("#receipt_no").val("");
	$("#receipt_gno").val("");
	$("#receipt_moo").val("");
	$("#receipt_soi").val("");
	$("#receipt_road").val("");

	$('#receipt_province_id').val("");

	var select = $("#receipt_amphur_id");
	$('option', select).remove();
	$(select).append('<option value="">---- เลือก ----</option>');

	var select = $("#receipt_district_id");
	$('option', select).remove();
	$(select).append('<option value="">---- เลือก ----</option>');

	$("#receipt_postcode").val("");
}
function change_select_university()
{
	var id = $("#select_university").val().split("-");
	$("#grd_ugrp1").val(id[0]);
	$("#grd_uid1").val(id[1]);
	$("#grd_uname1").val($("#select_university option:selected").text());
}

<?php /*
function check_block_list(cid){

	var art = 1;
	var ex_date = '<?php echo $course_detail_date; ?>'.split("-");
	var exd = ex_date[2].replace(/^0+/, '');
	var exm = ex_date[1].replace(/^0+/, '');
	var exy = ex_date[0];

	var url = "data/member-block.php";
	var param = "cid='"+cid+"'&exd="+exd+"&exm="+exm+"&exy="+exy;
	$.ajax( {
		"dataType":'json', 
		"type": "POST",
		"async": false, 
		"url": url,
		"data": param, 
		"success": function(data){
			if(data.result == '1000') // connect ได้ ข้อมูลไม่ถูกต้อง (จานวนข้อมูล)
			{
				msgError("ข้อมูลไม่ถูกต้อง (จานวนข้อมูล)");
			}	 
			else if (data.result == '1001')  // connect ได้ ข้อมูลไม่ถูกต้อง (เลขที่บัตร)
			{
				msgError("ข้อมูลไม่ถูกต้อง (เลขที่บัตร)");
			}
			else if (data.result == '1002')  // connect ได้ ข้อมูลไม่ถูกต้อง (วันที่)
			{
				msgError("ข้อมูลไม่ถูกต้อง (วันที่)");
			}
			else if (data.result == '2000')  // invalid user or password
			{
				msgError("invalid user or password");
			}
			else if (data.result == '3000')  // ไม่อยู่ในรายการห้ามสอบ
			{
				art = 2;
			}
			else if (data.result == '4000')  // อยู่ในรายการห้าม สอบถึงวันที่ YYYY-MM-DD
			{
				if(data.date!="") data.date = "ถึงวันที่ "+data.date;
				msgError("อยู่ในรายการห้าม<?php echo $name_type; ?>  " + data.date);
			}
			else
			{
				msgError("Unknown Error");
			}
		}
	});
	
	return art ;
}

*/ ?>

function getCode(){
	var t;
	var cid = $('#cid').val();
	var costype_id = "<?php echo $costype_id; ?>";	
	var sec_id = "<?php echo $sec_id; ?>";	
	//console.log(sec_id);
	//console.log(costype_id);
	if (costype_id==2 && sec_id==1) {
		//var t = check_block_list(cid);		
	}else{
		t = 2;
	}
	////console.log(t);
	if(t==1) return;

	var url = "data/member-info.php";
	var param = "cid="+$('#cid').val();
	$.ajax( {
		"dataType":'json', 
		"type": "POST",
		"async": false, 
		"url": url,
		"data": param, 
		"success": function(data){	
			if(data==""){
				msgError("ไม่พบข้อมูลของ "+$('#cid').val());
			}
			$.each(data, function(index, array){                
				$("#taxno").val(array.taxno);
				$("#branch").val(array.branch);
				$("#receipt_no").val(array.no);
				$("#receipt_gno").val(array.gno);
				$("#receipt_moo").val(array.moo);
				// $("#receipt_soi").val(array.soi);
				$("#receipt_soi").val(array.soi);
				$("#receipt_road").val(array.road);
				$("#receipt_postcode").val(array.postcode);
				var t = array.slip_type;

				//console.log(t);
				if (t=='corparation') {
					//console.log('t');
					$("#receipt_no").val(array.receipt_no);
					$("#receipt_gno").val(array.receipt_gno);
					$("#receipt_moo").val(array.receipt_moo);
					$("#receipt_soi").val(array.receipt_soi);
					$("#receipt_road").val(array.receipt_road);
					$("#receipt_postcode").val(array.receipt_postcode);
				}

				corparation_info(t);
				$("select[name=receipt_province_id] option").filter(function() {
					return $(this).val() == array.province_id; 
				}).attr('selected', true);	
				var select = $("#receipt_amphur_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.receipt_amphur_id+'"> '+array.receipt_amphur_name+' </option>');	
				var select = $("#receipt_district_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.receipt_district_id+'"> '+array.receipt_district_name+' </option>');				
				var select = $("#amphur_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.amphur_id+'"> '+array.amphur_name+' </option>');	
				var select = $("#district_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.district_id+'"> '+array.district_name+' </option>');
				var select = $("#receipt_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.receipt_id+'"> '+array.receipt_name+' </option>');
				setVal("#frmMain", array);
				var select = $("#org_name");
				$('option', select).remove();
				$(select).append('<option value="'+array.org_name+'"> '+array.org_name_text+' </option>'); 
				if(!array.org_name_text){
					array.org_name_text = '-';
				}
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-blue checkbox',
					radioClass: 'iradio_square-blue'
				});

				if ($("#receipt_title").val() == 'อื่นๆ') {
					$("#receipt_title_text").show();
				}else{
					$("#receipt_title_text").hide();
				}

				if ($("#title_th").val() == 'อื่นๆ') {
					$("#title_th").val(array.title_th_text);
				}

				//console.log(array);			
			});				 
		}
	});
}
function ckdup_register(id){
	if(typeof id=="undefined") return;
	var member_id = "<?php echo $member_id;?>";
	var url = "data/ckdup-register.php";
	var param = "course_detail_id="+id+"&member_id="+member_id+"&format=<?php echo $format; ?>";
	$.ajax({
			"dataType":'json', 
			"type": "POST",
			"async": false, 
			"url": url,
			"data": param, 
			"success": function(data){  
         		var ck = 0;
         		$.each(data, function(index, array){
         			ck++;
         		});
         		if(ck>0){
         			msgError("");
         		}
			}
	});
}
function auto_select(){
	var id = $("#scourse_id").val();
	clearPage('<?php echo $_GET['p'] ?>&course_detail_id=<?php echo $course_detail_id; ?>&type=walk-in&course_id='+id);
}
function back_to_info(){
    var url = "index.php?p=report&course_detail_id=<?php echo $course_detail_id; ?>&type=register-info&course_id=<?php echo $course_id; ?>";
   redirect(url);
  // window.open(url,'_blank');
}

function chk_block_list(){
	var cid = $('#cid').val();
	var course_detail_id = "<?php echo $course_detail_id;?>";
	var course_id = "<?php echo $course_id;?>";
	// var url = "chk_bls4.php";
	var url = "chk_bls5.php";
	var sec_id = "<?php echo $section_id;?>";
	var type_id = "<?php echo $coursetype_id;?>";
	var t = 99;
	
	// alert(cid);

	$.ajax({
		//"dataType":'json', 
		"type": "POST",
		"url": url,
		//"data": param, 
		"data":{cid:cid
				, course_detail_id:course_detail_id
				, sec_id:sec_id
				, type_id:type_id
				, course_id:course_id
		},
		"success": function(data){
			// response (ถ้า connect ได้)
			// 1000:0000-00-00 : connect ได้ ข้อมูลไม่ถูกต้อง (จำนวนข้อมูล)
			// 1001:0000-00-00 : connect ได้ ข้อมูลไม่ถูกต้อง (เลขที่บัตร)
			// 1002:0000-00-00 : connect ได้ ข้อมูลไม่ถูกต้อง (วันที่)
			// 2000:0000-00-00 : invalid user or password
			// 3000:0000-00-00 : ไม่อยู่ในรายการห้ามสอบ
			// 4000:0000-00-00 : อยู่ในรายการห้ามสอบ
			console.log(data);

			var res = data.split(':');
			var rs_cd = res[0];
			var rs_date = res[1];
			var date_spit = rs_date.split('-');
			var year = parseInt(date_spit[0])+543;
			var month = date_spit[1];
			var day = date_spit[2];
			rs_date = day+'-'+month+'-'+year;				
			//alert(rs_date);
			// rs_cd = 4002;
			
			if(rs_cd == '3000'){
/*
				$('#form1').attr('action', 'index.php?p=new-step-three');
				$('#form1').attr('target', '_self');

				if( ($("#my_select_course_detail").val() != "want") ){
					$('#form1').submit();
				} 
*/				getCode();
			}else{
				
				// $("#bt_save").hide();

				if ( rs_cd == '1000' ) {
					msgError("connect ได้ ข้อมูลไม่ถูกต้อง (จำนวนข้อมูล)");
				}else if ( rs_cd == '1001' ) {
					msgError("connect ได้ ข้อมูลไม่ถูกต้อง (เลขบัตร)");
				}else if ( rs_cd == '1002' ) {
					msgError("connect ได้ ข้อมูลไม่ถูกต้อง (วันที่)");
				}else if ( rs_cd == '2000' ) {
					msgError("invalid user or password");
				}else if ( rs_cd == '4001' ) {
					//msgError("อยู่ในรายการห้ามสอบ จนถึงวันที่ "+rs_date+" (block case 1 : โดย TSI)");
					msgError("ระบบไม่สามารถทำการรับสมัครสอบของท่านได้ ท่านไม่ได้รับอนุญาติให้สมัครจนถึงวันที่ "+rs_date);
				}else if ( rs_cd == '4002' ) {
					//msgError("อยู่ในรายการห้ามสอบ จนถึงวันที่ "+rs_date+" (block case 2 : กรณีผู้คุมสอบ)");
					msgError("ระบบไม่สามารถทำการรับสมัครสอบของท่านได้ ท่านไม่ได้รับอนุญาติให้สมัครจนถึงวันที่ "+rs_date);
				}else if ( rs_cd == '4003' ) {
					//msgError("อยู่ในรายการห้ามสอบ จนถึงวันที่ "+rs_date+" (block case 3 : กรณีเคยสอบผ่าน วิชา และยังไม่หมดอายุ)");
					msgError("ท่านสอบผ่านหลักสูตรนี้แล้ว ท่านจะสามารถสอบหลักสูตรนี้ได้อีกครั้ง ตั้งแต่วันที่ "+rs_date);
				}else if ( rs_cd == '4000' ) {
					msgError("อยู่ในรายการห้ามสอบ จนถึงวันที่ "+rs_date+" (block case 4 :  กรณีเป็นเจ้าหน้าที่ ATI)");
				}//end else if

			}//end else	
		}
	});
}//end func

</script>
<style>
	#set_time-error{
		display: none !important;
	}
	#pay_status-error, #slip_type-error{
		width: 400px;
		margin-top: 21px;
	}
</style>