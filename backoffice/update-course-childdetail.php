<?php
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";

global $db;

if($_POST){
   $course_id = $_POST["course_id"];
	if(is_array($_POST["ckCourseDetail"])){
		foreach($_POST["ckCourseDetail"] as $index=>$v){
			$args = array();
			$args["table"] = "course";
			$args["id"] = $_POST["course_child_id"][$index];			
			$args["parent_id"] = $course_id;
			if($args["id"] && $v=="F"){
				$args["parent_id"] = "0";
			}
			$db->set($args);
		}	
	}
}
// show succes
$_SESSION["success"]["msg"] = "บันทึกข้อมูลเรียบร้อยแล้ว";

$args = array();
$args["p"] = "course";
$args["course_id"] = $course_id;
$args["type"] = "childdetail";
redirect_url($args);
?>