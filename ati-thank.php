<?php 
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";
include_once "./share/course.php";
global $db;

// d($_SESSION);
$member_info = $_SESSION["login"]["info"];
$member_id = $member_info['member_id'];
$register_id = (int)$_SESSION["last_register_login"][$member_id];
// echo $register_id;
if ( !empty($register_id) ) {
  
  $info = get_register("", $register_id);
  $reg_info = $info[0];

  $pay_type = trim($reg_info["pay"]);
  $section_id = $reg_info["section_id"];

  // d($reg_info);
  // unset($_SESSION["last_register_login"][$member_id]);
}//end if

?>

<!DOCTYPE html>
<html lang="en">
<?php include ('include/header.php'); ?>
<body>
  <!-- nav -->
  <?php include ('include/top-menu.php'); ?>

  <form action='' method="POST">
   <div class="contrainer">
    <div class="mainsite">
      <div class="label">
        <p>สถาบันฝึกอบรม สมาคมบริษัทหลักทรัพย์ไทย ASCO Training Institute (ATI)</p>
      </div>
      <div class="select-zone">
        <div class="row">
          <div class="bottom-center-zone" style="width:47%;margin-top:80px;text-align: center;">
          <label style="color:#000; font-size: 24px;font-weight: bold;font-weight: bold;">สถาบันฝึกอบรม สมาคมบริษัทหลักทรัพย์ไทย ASCO Training Institute (ATI)</label>
          <br><br>
          <label style="color:#000; font-size: 24px;font-weight: bold;">ขอขอบพระคุณท่านเป็นอย่างสูง ที่ท่านเลือกใช้บริการของเรา</label>
          <br><br>
          <label style="color:#000; font-size: 24px;font-weight: bold;">หากท่านต้องการทำรายการเพิ่มเติม สามารถคลิ๊กได้<a href="#" onclick="goToindex();">ที่นี่</a></label>
        </div>
        <!-- <div class="bottom" id="action2" onclick="goToindex();"><a href="#">กลับไปทำรายการเพิ่มเติม</a></div>                  -->
      </div>
    </div>
  </div>
</div>

<!-- <form id='formBack' name='formBack' method='post' action='' enctype='application/x-www-form-urlencoded'>
  <input name="sec_id" type="hidden" value="<?php //echo $sec_id; ?>" id="sec_id" />
  <input name="sec_action" type="hidden" value="<?php //echo $sec_action; ?>" id="sec_action" />
  <input name="ch_submit_section" type="hidden" value="yes" id="ch_submit_section" />
</form> -->

<!-- footer -->
<?php //include ('include/footer.php') ?>

</body>
</html>

<script type="text/javascript">

  $(document).ready(function(){
    var pay_type = "<?php echo $pay_type; ?>";
    var register_id = "<?php echo $register_id; ?>";

    if ( pay_type!="undefined" && (pay_type=="bill_payment" || pay_type=="at_ati" ) ) {
      if ( pay_type=="bill_payment" ) {
        var url = "invoice.php?register_id="+register_id;
      }else if ( pay_type=="at_ati" ) {
        var url = "invoice-ati.php?register_id="+register_id;
      }
      // alert(url);
      // window.open(url);
    }else if ( pay_type!="undefined" && pay_type=="mpay" ) {
      mpayNow();
    }

  });

  function mpayNow(){
    var register_id = "<?php echo $register_id; ?>";
    var url = "./mpay/pay-now.php?register_id="+register_id;
    window.open(url);
  }//end func

  function goBack(){
    $('#formBack').attr('action', 'index.php?p=new-condition-one');
    $('#formBack').attr('target', '_self');
    $('#formBack').submit();
  }
  function goToindex(){
    window.open('index.php?p=main','_self');
  }
</script>

<style>
.billinfo {
  padding: 20px;
  padding-top: 20px;
  padding-bottom: 20px;
  background-color: #FFF;
  border: 2px solid #dcdcdc;
  border-radius: 10px;
  font-size: 24px;
}
</style>