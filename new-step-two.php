﻿<?php
include_once "./lib/lib.php";
include_once "./share/course.php";
global $db;
global $thai_month_arr;

if(!isset($_POST['ch_submit_section'])){
echo <<<holyy
<script>
window.open('index.php','_self');
</script>
holyy;
}
if($_POST['ch_submit_section'] != "yes"){
echo <<<holy
<script>
window.open('index.php?p=select_s1','_self');
</script>
holy;
}
$sec_id = $_POST['sec_id'];
$sec_action = $_POST['sec_action'];
$select_course = $_POST['my_select_course'];
$type_id = map_course_type($sec_action);
$q = "SELECT name FROM `coursetype` WHERE coursetype_id=$type_id";
$name_type = $db->data($q);
$list = explode(",", $_POST['my_select_course']);

$m_now = date('m');
if( !isset($_POST['select_year']) || !isset($_POST['select_month']) ){
	$select_year = date('Y');
	$select_year_TH = $select_year+543;
	$select_month = date('m');

} else {
	$select_year = $_POST['select_year'];
	$select_year_TH = $select_year+543;
	$select_month = $_POST['select_month'];
}
//var_dump($select_month);
if (!isset($_SESSION)) {//initialize the session
  session_start();
}
$_SESSION["select_year"] = $select_year_TH;
$_SESSION["select_month"] = $select_month;
$select_month_txt =  $thai_month_arr[$select_month];
$msg_data = "";
$sub_msg_class = "";
$format = get_select_course($sec_id, $sec_action);
// var_dump($format);

//member data
$member_info = $_SESSION["login"]["info"];
$member_id = $member_info['member_id'];
$my_select_course_detail = "";

//get $cid
$q = "SELECT cid FROM member WHERE member_id = $member_id;";
$r = $db->get($q);
foreach ($r as $key => $v) {
	//var_dump($v['cid']);
	$cid = $v['cid'];
}

$ids = trim($select_course, ",");

$con = "";
$q = "SELECT a.course_detail_id, a.course_id 
	FROM course_detail_list a 
	INNER JOIN course_detail b ON a.course_detail_id=b.course_detail_id
	WHERE a.active='T' AND a.course_id IN ($ids) $con";
$r = $db->get($q);
$tmp = array();
$con_ids = "";
if($r){
	$ids = "";
	$ids_3 = array();
	foreach ($r as $key => $value) {
		$ids .= $value["course_detail_id"].", ";
		$ids_3[$value["course_id"]] .= $value["course_detail_id"].", ";//" a.course_detail_id in ({$value["course_detail_id"]})";
	}
	$ids = trim($ids, ", ");
	if($format==1){
		$con_ids = "a.course_detail_id in ($ids)";
	}
	//$tmp = get_course_detail($con);
}
$count = 0;
$tmp_name = array();
$q = "select title from course where course_id in ($select_course)";
$r = $db->get($q);
$i = 0;
foreach ($r as $x => $xx) {
	$i++;
	$tmp_name[] = "(".$i.") ".$xx["title"];
}

if ( (date("Y")==$select_year) && (date("m")==$select_month) ) {
	$day_now = date("{$select_year}-{$select_month}-d");
}else{
	$day_now = date("{$select_year}-{$select_month}-1");
}

foreach ($list as $my_select_course) {	
		$no = 1;  
		$res = array();
		if($format==1){
			if($con_ids){				
				$con = " and ( a.course_id=$my_select_course or  ($con_ids ) )
				and a.date>='$day_now'
				and a.active='T' and a.inhouse='F'";
			}else{
				$con = "
						and a.course_id=$my_select_course
						and a.coursetype_id=$type_id
						and a.section_id=$sec_id 
						and a.date>='$day_now' 
						and a.active='T' and a.inhouse='F'";				
			}
		}else if($format==2){
			if($ids_3[$my_select_course]){
				$t = trim($ids_3[$my_select_course], ", ");
				$con_ids = " or  ( a.course_detail_id in ($t)) ";
			}else{
				$con_ids = "";
			}
			$con = "and ( a.course_id=$my_select_course $con_ids)
			and a.date>='$day_now'
			and a.active='T' and a.inhouse='F'";
		}else if($format==3){
			if($ids_3[$my_select_course]){
				$t = trim($ids_3[$my_select_course], ", ");
				$con_ids = " or  ( a.course_detail_id in ($t)) ";
			}else{
				$con_ids = "";
			}
			$con = "and ( a.course_id=$my_select_course $con_ids)
			and a.date>='$day_now'
			and a.active='T' and a.inhouse='F'";
		}else{    
			$con = "
					and a.coursetype_id=$type_id
					and a.section_id=$sec_id 
					and a.date>='$day_now' 
					and a.active='T' and a.inhouse='F'";
		}

		//add new $con
		/*$strt_date = $select_year."-".$select_month."-01 00:00:00";
		$stop_date = $select_year."-".$select_month."-31 23:59:59";
		$con .= " AND a.date BETWEEN '$strt_date' AND '$stop_date'";*/

		$res = get_course_detail($con, "", false, 1000, true);
		$q = "select title from course where active='T' and course_id=$my_select_course";
		$name = $db->data($q);		
		$count += count($res);
		if($res){
			if($format==2 || $format==3){
				$msg_data .= "
					<tr class='tr-header-fa'>
						<td class='td-col-1' colspan='10'>{$name}</td>
					</tr> 
				";
			}			
			foreach ($res as  $row){	
				/*echo "<pre>";
				print_r($res);
				echo "</pre>";*/
				$course_id = $row['course_id'];
				$con_c = " and a.course_id = $course_id";
				$r_c = get_course($con_c);
				$title = $r_c[0]['title'];
				$course_detail_id = $row['course_detail_id'];
				
				if ($my_select_course_detail == "") {
						$my_select_course_detail = $course_detail_id;
				} else {
						$my_select_course_detail .= ",".$course_detail_id;
				}						
				$day = $row['day'];
				$date = revert_date($row['date']);
				$time = $row['time'];
				
				$address = $row['address'];
				$date_other = trim($row["date_other"]);
				$pr = "";
				if($date_other!="") $pr .= " <br> <a class='plus' data='".$date_other."'>รายละเอียด</a>";
				$time .= $pr; 
				$remark = trim($row["remark"], '<p><br></br></p>');
				$pr = "";
				if($remark!="") $pr .= " <hr><div class='custom-detail' ><".$remark."</div>";
				$date .= $pr; 
				//$address_detail = $pr.$row['address_detail'];

				
				$address_detail = $row['address_detail'];
/*				if ( strlen($address_detail) > 200 ) {
					// $address_detail .= "<br> <a class='plus' data='".$address_detail."'>ดูเพิ่มเติม</a>";
					$address_detail = "<a class='short-text plus' data='".$address_detail."'>".$address_detail."</a>";
					// $address_detail = "<span class='short-text' title='".$address_detail."' data-toggle='tooltip' data-placement='top'>".$address_detail."</span>";
				}//end if*/

				// echo $address_detail."<br>";
				// echo strlen($address_detail);


				$get_all = 0;
				$not_id = "";
				
				//$use = get_use_chair($course_detail_id);
				$book = $row["book"];
				$book_all = 0;
				if($book>0) {
					$book_all += $row["book_all"];
					//$usebook += get_use_book($course_detail_id);
				}

				$use = $row["sit_all"];				 
				$chair_all = $row['chair_all'];
				$sit_all = $chair_all - $book - $use + $book_all;
				if($sit_all<0) $sit_all = 0;


				$status = $row['status'];
				$open_regis = revert_date($row['open_regis']);
				$end_regis = revert_date($row['end_regis']);
				$datetime_add = $row['datetime_add'];
				$datetime_update = $row['datetime_update'];
				if($sub_msg_class == ""){ $sub_msg_class = "se2-row-silver"; }
				if($sub_msg_class == "se2-row-white"){
					$sub_msg_class = "se2-row-silver";
				} else {
					$sub_msg_class = "se2-row-white";
				}
				$sub_msg_class_final = $sub_msg_class;
				
				if($tr_current_bg == "tr-bg-white"){
					$tr_current_bg = "tr-bg-silver";
				} else if($tr_current_bg == "tr-bg-silver"){
					$tr_current_bg = "tr-bg-white";
				}
				else {
					$tr_current_bg = "tr-bg-silver"; 
				}
				
				$exd = explode(" ", $row["end_regis"]);
				$t_end_regis = $exd[0];
				$date_end = $t_end_regis." 23:59:59";
				$date_now = date("Y-m-d H:m:s");

				if ( ($date_end < $date_now) && ($status=='เปิดรับสมัคร') ) {
					$status='ปิดรับ Online';
				}

				if($status == "ปิดรับ Online" || $status == "ปิดรับสมัครชั่วคราว"){
					$sub_msg_class_final = "se2-row-close";
					$sub_msg_white = " color:#FFFFFF;";
					$sub_msg_bg_white = " style='background:#FFFFFF;'";
					$sub_msg_hide_radio = " style='display:none;'";
				} else {
					$sub_msg_white = "";
					$sub_msg_bg_white = "";
					$sub_msg_hide_radio = "";
				}
				if($sit_all == 0 && $status != "ปิดรับ Online"){
					$status = "รอบ{$name_type}เต็ม";
				}
				if($status == "รอบ{$name_type}เต็ม"){
					$sub_msg_hide_radio = " style='display:none;'";
					$status = "<span style='color:#F00;'>รอบ{$name_type}เต็ม</span>";
				}

		/*check duplicate register*/
/*		if($format==3 || $format==2){
			    $q = " select count(register_id) as c from register where active='T' and member_id = '$member_id' and course_detail_id like '%{$course_detail_id}%' and pay_status in (1,3,5,6) and usebook='F'";
				//$q = " select a.register_id from register_course_detail a inner join register b on a.register_id=b.register_id where b.active='T' and a.course_detail_id=$course_detail_id and  b.member_id = '$member_id'  and b.pay_status in (1,3,5,6)";
				$register_id = $db->data($q);
				if($register_id > 0){
					$sub_msg_hide_radio = " style='display:none;'";
				}
		}else{
			$q = "select register_id from register a where a.active='T' and  a.course_detail_id='$course_detail_id' and a.member_id = '$member_id'  and pay_status in (1,3,5,6)";
			$register_id = $db->data($q);
			if($register_id) $sub_msg_hide_radio = " style='display:none;'";
				
		}
		echo $q;*/
		/*end ck duplicate register*/										
				if($format==1){
					if($status == "ปิดรับ Online" || $status == "ปิดรับสมัครชั่วคราว"){
						if ($status == "ปิดรับสมัครชั่วคราว") {
							$status = "ปิดรับสมัคร<br>ชั่วคราว";
						}
						$msg_data .= "<div class='$sub_msg_class_final custom-detail-row'>
										<div class='se2-colum1-check'><input type='radio' name='address_course' value='$course_detail_id' id='address_course$course_detail_id' onclick=\"getaddressCourse('#address_course$course_detail_id');\"$sub_msg_hide_radio></div>
										<div class='se2-row-colum2-c'>
												<div class='se2-colum2-day-c'$sub_msg_bg_white><center><p style='white-space:normal;'>$day.</p></center></div>
												<div class='se2-colum2-dtext-c' ><center><p style='$sub_msg_white'>$date</p></center></div>
										</div>
										<div class='se2-row-colum3-c'><center><p style='$sub_msg_white'>$time</p></center></div>
										<div class='se2-row-colum4-c'><center><p style='white-space:normal;$sub_msg_white'>$address</p></center></div>
										<div class='se2-row-colum5-c'><p style='white-space:normal;$sub_msg_white'>$address_detail</p></div>
										<div class='se2-row-colum6-c'><center><p style='white-space:normal;$sub_msg_white'>".$sit_all."</p></center></div>
												<div class='se2-row-colum6-c'><center><p style='white-space:normal;$sub_msg_white'>$chair_all</p></center></div>
												<div class='se2-row-colum7-c'><center><p style='white-space:normal;$sub_msg_white'><span class='red'>$status</span></p></center></div>
												<div class='se2-row-colum8-c'>
												<div class='se2-colum5-text-c'><p>$open_regis</p></div></div>
										<div class='se2-row-colum9-c'><div class='se2-colum5-text-c'><p>$end_regis
										</p></div></div>
									</div>
									";
					}else if($status != "ยกเลิกรอบสอบ"){
						$msg_data .= "<div class='$sub_msg_class_final custom-detail-row'>
										<div class='se2-colum1-check'><input type='radio' name='address_course' value='$course_detail_id' id='address_course$course_detail_id' onclick=\"getaddressCourse('#address_course$course_detail_id');\"$sub_msg_hide_radio></div>
										<div class='se2-row-colum2'>
												<div class='se2-colum2-day'$sub_msg_bg_white><center><p style='white-space:normal;'>$day.</p></center></div>
												<div class='se2-colum2-dtext' ><center><p style='$sub_msg_white'>$date</p></center></div>
										</div>
										<div class='se2-row-colum3'><center><p style='$sub_msg_white'>$time</p></center></div>
										<div class='se2-row-colum4'><center><p style='white-space:normal;$sub_msg_white'>$address</p></center></div>
										<div class='se2-row-colum5'><p style='white-space:normal;$sub_msg_white'>$address_detail</p></div>
										<div class='se2-row-colum6'><center><p style='white-space:normal;$sub_msg_white'>$sit_all</p></center></div>
												<div class='se2-row-colum6'><center><p style='white-space:normal;$sub_msg_white'>$chair_all</p></center></div>
												<div class='se2-row-colum7'><center><p style='white-space:normal;$sub_msg_white'>$status</p></center></div>
												<div class='se2-row-colum8'>
												<div class='se2-colum5-text'><p>$open_regis</p></div></div>
										<div class='se2-row-colum9'><div class='se2-colum5-text'><p>$end_regis
										</p></div></div>
									</div>
									";
					}

				}else if($format==2){
						 if($status == "ปิดรับ Online" || $status == "ปิดรับสมัครชั่วคราว"){
					 		if ($status == "ปิดรับสมัครชั่วคราว") {
								$status = "ปิดรับสมัคร<br>ชั่วคราว";
							}
								$msg_data .= "
									<tr class='$tr_current_bg tr-item-fa'>
											<td class='td-col-0'><input type='radio' name='address_course' value='$course_detail_id' id='address_course$course_detail_id' onclick=\"getaddressCourse('#address_course$course_detail_id');\"$sub_msg_hide_radio></td>
											
											<td class='td-col-2'>
												 <span class='span-day'>$day.</span>
												 <span class='span-date'>$date</span>
											</td>
											<td class='td-col-3'>$time</td>
											<td class='td-col-4'>$address</td>
											<td class='td-col-5'>$address_detail</td>
											<td class='td-col-6'>$sit_all</td>
											<td class='td-col-7'>$chair_all</td>
											<td class='td-col-8'><center><span class='red'>$status</span></center></td>
											<td class='td-col-9'><center>$open_regis</center></td>
											<td class='td-col-10'><center>$end_regis</center></td>
									</tr> ";
						 }else if($status != "ยกเลิกรอบสอบ"){
								 $msg_data .= "
									<tr class='$tr_current_bg tr-item-fa'>
											<td class='td-col-0'><input type='radio' class='address_course_$my_select_course'  data='$my_select_course'  name='address_course_$my_select_course' value='$course_detail_id' id='address_course$course_detail_id' onclick=\"getaddressCourse('#address_course$course_detail_id');\"$sub_msg_hide_radio></td>
											
											<td class='td-col-2'>
												 <span class='span-day'>$day.</span>
												 <span class='span-date'>$date</span>
											</td>
											<td class='td-col-3'>$time</td>
											<td class='td-col-4'>$address</td>
											<td class='td-col-5'>$address_detail</td>
											<td class='td-col-6'>$sit_all</td>
											<td class='td-col-7'>$chair_all</td>
											<td class='td-col-8'><center>$status</center></td>
											<td class='td-col-9'><center>$open_regis</center></td>
											<td class='td-col-10'><center>$end_regis</center></td>
									</tr> ";
						}
				}else if($format==3){
						 if($status == "ปิดรับ Online" || $status == "ปิดรับสมัครชั่วคราว"){
						 	if ($status == "ปิดรับสมัครชั่วคราว") {
								$status = "ปิดรับสมัคร<br>ชั่วคราว";
							}
								$msg_data .= "
									<tr class='$tr_current_bg tr-item-fa'>
											<td class='td-col-0'><input type='radio' name='address_course' value='$course_detail_id' id='address_course$course_detail_id' onclick=\"getaddressCourse('#address_course$course_detail_id');\"$sub_msg_hide_radio></td>
											
											<td class='td-col-2'>
												 <span class='span-day'>$day.</span>
												 <span class='span-date'>$date</span>
											</td>
											<td class='td-col-3'>$time</td>
											<td class='td-col-4'>$address</td>
											<td class='td-col-5'>$address_detail</td>
											<td class='td-col-6'>$sit_all</td>
											<td class='td-col-7'>$chair_all</td>
											<td class='td-col-8'><center><span class='red'>$status</span></center></td>
											<td class='td-col-9'><center>$open_regis</center></td>
											<td class='td-col-10'><center>$end_regis</center></td>
									</tr> ";
						 }else if($status != "ยกเลิกรอบสอบ"){
								 $msg_data .= "
									<tr class='$tr_current_bg tr-item-fa'>
											<td class='td-col-0'><input type='checkbox' class='address_course_$my_select_course' name='address_course' data='$my_select_course' value='$course_detail_id' id='address_course$course_detail_id' onclick=\"getaddressCourse('#address_course$course_detail_id');\"$sub_msg_hide_radio></td>
											
											<td class='td-col-2'>
												 <span class='span-day'>$day.</span>
												 <span class='span-date'>$date</span>
											</td>
											<td class='td-col-3'>$time</td>
											<td class='td-col-4'>$address</td>
											<td class='td-col-5'>$address_detail</td>
											<td class='td-col-6'>$sit_all</td>
											<td class='td-col-7'>$chair_all</td>
											<td class='td-col-8'><center>$status</center></td>
											<td class='td-col-9'><center>$open_regis</center></td>
											<td class='td-col-10'><center>$end_regis</center></td>
									</tr> ";
						}
				}else if($format==4){
					if($status == "ปิดรับ Online" || $status == "ปิดรับสมัครชั่วคราว"){
						if ($status == "ปิดรับสมัครชั่วคราว") {
							$status = "ปิดรับสมัคร<br>ชั่วคราว";
						}
						$msg_data .= "<div class='$sub_msg_class_final'>
										<div class='se2-colum1-check'><input type='radio' name='address_course' value='$course_detail_id' id='address_course$course_detail_id' onclick=\"getaddressCourse('#address_course$course_detail_id');\"$sub_msg_hide_radio></div>
										<div class='se2-row-colum2-c'>
												<div class='se2-colum2-day-c'$sub_msg_bg_white><center><p style='white-space:normal;'>$day.</p></center></div>
												<div class='se2-colum2-dtext-c' ><center><p style='$sub_msg_white'>$date</p></center></div>
										</div>
										<div class='se2-row-colum3-c'><center><p style='$sub_msg_white'>$time</p></center></div>
										<div class='se2-row-colum4-c'><center><p style='white-space:normal;$sub_msg_white'>$address</p></center></div>
										<div class='se2-row-colum5-c'><p style='white-space:pre-line; $sub_msg_white'>$address_detail</p></div>
										<div class='se2-row-colum6-c'><center><p style='white-space:normal;$sub_msg_white'>".$sit_all."</p></center></div>
												<div class='se2-row-colum6-c'><center><p style='white-space:normal;$sub_msg_white'>$chair_all</p></center></div>
												<div class='se2-row-colum7-c'><center><p style='white-space:normal;$sub_msg_white'><span class='red'>$status</span></p></center></div>
												<div class='se2-row-colum8-c'>
												<div class='se2-colum5-text-c'><p>$open_regis</p></div></div>
										<div class='se2-row-colum9-c'><div class='se2-colum5-text-c'><p>$end_regis
										</p></div></div>
									</div>
									";
					}else if($status != "ยกเลิกรอบสอบ"){
						$msg_data .= "<div class='$sub_msg_class_final custom-detail-row'>
										<div class='se2-colum1-check'><input type='radio' name='address_course' value='$course_detail_id' id='address_course$course_detail_id' onclick=\"getaddressCourse('#address_course$course_detail_id');\"$sub_msg_hide_radio></div>
										<div class='se2-row-colum2'>
												<div class='se2-colum2-day'$sub_msg_bg_white><center><p style='white-space:normal;'>$day.</p></center></div>
												<div class='se2-colum2-dtext' ><center><p style='$sub_msg_white'>$date</p></center></div>
										</div>
										<div class='se2-row-colum3'><center><p style='$sub_msg_white'>$time</p></center></div>
										<div class='se2-row-colum4'><center><p style='white-space:normal;$sub_msg_white'>$address</p></center></div>
										<div class='se2-row-colum5'><p style='white-space:pre-line;
										 $sub_msg_white'>$address_detail</p></div>
										<div class='se2-row-colum6'><center><p style='white-space:normal;$sub_msg_white'>$sit_all</p></center></div>
												<div class='se2-row-colum6'><center><p style='white-space:normal;$sub_msg_white'>$chair_all</p></center></div>
												<div class='se2-row-colum7'><center><p style='white-space:normal;$sub_msg_white'>$status</p></center></div>
												<div class='se2-row-colum8'>
												<div class='se2-colum5-text'><p>$open_regis</p></div></div>
										<div class='se2-row-colum9'><div class='se2-colum5-text'><p>$end_regis
										</p></div></div>
									</div>
									";
					}

				}
				$no++;
			}//end foreach
		}
}

?>
		<div class="bgselect2" style="padding-bottom: 28px; height:auto;">
				<div class="bodyselect2" style="overflow:hidden; height:auto;"> 
						<div class="imgstep1">
							<table id="Table_01" width="1248" height="52" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td>
										<a href="#" onclick="goBack();"><img src="images/step02_01.jpg" width="302" height="52" alt=""></a>
									</td>
									<td>
										<img src="images/step02_02.jpg" width="306" height="52" alt="">
									</td>
									<td>
										<img src="images/step02_03.jpg" width="308" height="52" alt="">
									</td>
									<td>
										<img src="images/step02_04.jpg" width="332" height="52" alt="">
									</td>
								</tr>
							</table>
						</div>
						
						<div class="se-title"><center><p>เลือกเวลา<?php echo $name_type ; ?></p></center></div>
						<div class="se2-botton"><center><p id="update_from">อัพเดท</p></center></div>
						<div class="se2-title"><center><p><?php echo "$select_month_txt $select_year_TH";?></p></center></div>
						<form action="">
						<div class="se2-bottonmonth">
								<div class="se2-bottonmonth">
								<select name= "month" id="month">
										<option value="00">เลือกเดือน</option>
										<option value="01"<?php if($select_month == "01"){ echo " selected";} ?>>มกราคม</option>
										<option value="02"<?php if($select_month == "02"){ echo " selected";} ?>>กุมภาพันธ์</option>
										<option value="03"<?php if($select_month == "03"){ echo " selected";} ?>>มีนาคม</option>
										<option value="04"<?php if($select_month == "04"){ echo " selected";} ?>>เมษายน</option>
										<option value="05"<?php if($select_month == "05"){ echo " selected";} ?>>พฤษภาคม</option>
										<option value="06"<?php if($select_month == "06"){ echo " selected";} ?>>มิถุนายน</option>
										<option value="07"<?php if($select_month == "07"){ echo " selected";} ?>>กรกฎาคม</option>
										<option value="08"<?php if($select_month == "08"){ echo " selected";} ?>>สิงหาคม</option>
										<option value="09"<?php if($select_month == "09"){ echo " selected";} ?>>กันยายน</option>
										<option value="10"<?php if($select_month == "10"){ echo " selected";} ?>>ตุลาคม</option>
										<option value="11"<?php if($select_month == "11"){ echo " selected";} ?>>พฤษจิกายน</option>
										<option value="12"<?php if($select_month == "12"){ echo " selected";} ?>>ธันวาคม</option></select>
								 </div>
								<select name= "year" id="year">
										<option value="0000"> เลือกปี </option>
										<?php
										$yBegin = date('Y')+1;
										$yBeginTH = $yBegin+543;
										$year_selected = "";
										for($i=0; $i<=60; $i++){
											$yy = $yBegin*1 - $i;
											$yyTH = $yBeginTH*1 - $i;
											if($select_year == $yy){ $year_selected = " selected";} else { $year_selected = ""; }
											echo "<option value='$yy'$year_selected>$yyTH</option>
											";
										}
										?>
										</select>
								 </div>
						</form>				
					<?php  if($format==3){ ?>
								<div class="se-title"><center><p>เวลา<?php echo $name_type ; ?></p></center></div>
								<table class="new-step-two tb-fa" width="100%">
										<tr class="tr-header-fa">
												<td class="td-col-0"></td>												
												<td class="td-col-2" style="width:12%;">วัน</td>
												<td class="td-col-3" style="width:7%;">เวลา</td>
												<td class="td-col-4">สถานที่</td>
												<td class="td-col-5">ข้อมูลสถานที่<?php echo $name_type ; ?></td>
												<td class="td-col-6" style="width:5%;">ที่นั่ง</br>คงเหลือ</td>
												<td class="td-col-7" style="width:5%;">ที่นั่ง</br>ทั้งหมด</td>
												<td class="td-col-8" style="width:7%;">สถานะ</td>												<td class="td-col-6" style="width:5%;">ที่นั่ง</br>คงเหลือ</td>
												<td class="td-col-7" style="width:5%;">ที่นั่ง</br>ทั้งหมด</td>
												<td class="td-col-8" style="width:7%;">สถานะ</td>
												<td class="td-col-9" style="width:7%;">เปิด</br>รับสมัคร</td>
												<td class="td-col-10" style="width:7%;">ปิด</br>รับสมัคร</td>
										</tr> 
										<?php echo $msg_data; ?>
								</table>
				 <?php  
		 }else if($format==2){ ?>
								<div class="se-title"><center><p>เวลา<?php echo $name_type ; ?></p></center></div>

								<table class="new-step-two tb-fa" width="100%">
										<tr class="tr-header-fa">
												<td class="td-col-0"></td>												
												<td class="td-col-2" style="width:12%;">วัน</td>
												<td class="td-col-3" style="width:7%;">เวลา</td>
												<td class="td-col-4">สถานที่</td>
												<td class="td-col-5">ข้อมูลสถานที่<?php echo $name_type ; ?></td>
												<td class="td-col-6" style="width:5%;">ที่นั่ง</br>คงเหลือ</td>
												<td class="td-col-7" style="width:5%;">ที่นั่ง</br>ทั้งหมด</td>
												<td class="td-col-8" style="width:7%;">สถานะ</td>
												<td class="td-col-9" style="width:7%;">เปิด</br>รับสมัคร</td>
												<td class="td-col-10" style="width:7%;">ปิด</br>รับสมัคร</td>
										</tr> 
										<?php echo $msg_data; ?>
								</table>
				 <?php  
		 }else if($format==1){ ?>
					
							<div class="se2-td-title">
									<div class="se2-colum1"><center><p>เลือก</p></center></div>
									<div class="se2-colum2"><center><p>วัน</p></center></div>
									<div class="se2-colum3"><center><p>เวลา</p></center></div>
									<div class="se2-colum4"><center><p>สถานที่</p></center></div>
									<div class="se2-colum5"><center><p>ข้อมูลสถานที่<?php echo $name_type ; ?></p></center></div>
									<div class="se2-colum6"><center><p>ที่นั่ง</br>คงเหลือ</p></center></div>
									<div class="se2-colum6"><center><p>ที่นั่ง</br>ทั้งหมด</p></center></div>
									<div class="se2-colum7"><center><p>สถานะ</p></center></div>
									<div class="se2-colum8"><center><p>เปิด</br>รับสมัคร</p></center></div>
									<div class="se2-colum8"><center><p>ปิด</br>รับสมัคร</p></center></div>
							</div>
						<?php echo $msg_data; ?>
						<?php }else if($format==4){ ?>
						
				
							<div class="se2-td-title">
									<div class="se2-colum1"><center><p>เลือก</p></center></div>
									<div class="se2-colum2"><center><p>วัน</p></center></div>
									<div class="se2-colum3"><center><p>เวลา</p></center></div>
									<div class="se2-colum4"><center><p>สถานที่</p></center></div>
									<div class="se2-colum5"><center><p>ข้อมูลสถานที่<?php echo $name_type ; ?></p></center></div>
									<div class="se2-colum6"><center><p>ที่นั่ง</br>คงเหลือ</p></center></div>
									<div class="se2-colum6"><center><p>ที่นั่ง</br>ทั้งหมด</p></center></div>
									<div class="se2-colum7"><center><p>สถานะ</p></center></div>
									<div class="se2-colum8"><center><p>เปิด</br>รับสมัคร</p></center></div>
									<div class="se2-colum8"><center><p>ปิด</br>รับสมัคร</p></center></div>
							</div>
						<?php echo $msg_data; ?>
						<?php } ?>
					<div class="se-tdbottom"></div> 
						<div class="se-bottonarea2">
								<a href="#" onClick="javascript: window.open('index.php?p=main','_self');"><div class="se-botton"><center><p>กลับสู่หน้าหลัก</p></center></div></a>
								<div class="se-bottonarearight">    
								<a href="#" onClick="goBack();"><div class="se-botton"><center><p>ย้อนกลับ</p></center></div></a><!-- javascript: window.open('index.php?p=new_s1','_self'); -->
								<a href="#" onClick="my_f_submit();"><div class="se-botton-next"><center><p>ขั้นตอนต่อไป</p></center></div></a><!-- javascript: window.open('index.php?p=new_s3','_self'); -->
								</div>
						</div>
				</div>
		</div>
<form id='formSelect' name='formSelect' method='post' action='' enctype='application/x-www-form-urlencoded'>
	<input name="select_year" type="hidden" value="<?php echo $select_year; ?>" id="select_year" />
	<input name="select_month" type="hidden" value="<?php echo $select_month; ?>" id="select_month" />
	<input name="sec_id" type="hidden" value="<?php echo $sec_id; ?>" id="sec_id" />
	<input name="sec_action" type="hidden" value="<?php echo $sec_action; ?>" id="sec_action" />
	<input name="my_select_course" type="hidden" value="<?php echo $select_course; ?>" id="my_select_course" />
	<input name="ch_submit_section" type="hidden" value="yes" id="ch_submit_section" />
</form>
<form id='form1' name='form1' method='post' action='' enctype='application/x-www-form-urlencoded'>
	<input name="sec_id" type="hidden" value="<?php echo $sec_id; ?>" id="sec_id" />
	<input name="sec_action" type="hidden" value="<?php echo $sec_action; ?>" id="sec_action" />
	<input name="my_select_course" type="hidden" value="<?php echo $select_course; ?>" id="my_select_course" />
	<input name="my_select_course_detail" type="hidden" value="want" id="my_select_course_detail" />
	<input name="ch_submit_section" type="hidden" value="yes" id="ch_submit_section" />
</form>
<form id='formBack' name='formBack' method='post' action='' enctype='application/x-www-form-urlencoded'>
	<input name="sec_id" type="hidden" value="<?php echo $sec_id; ?>" id="sec_id" />
	<input name="sec_action" type="hidden" value="<?php echo $sec_action; ?>" id="sec_action" />
	<input name="ch_submit_section" type="hidden" value="yes" id="ch_submit_section" />
</form>

<?php 

	$t = $tmp_name;
	$a = array();
	foreach ($t as $key => $value) {
		$a[] = $db->escape($value);
	}
	$r = json_encode($a);
	$tmp_name = $r;
/*	$tmp_name = json_decode($r, true);
	print_r($tmp_name);*/

 ?>
<script type="text/javascript">
jQuery(document).ready(function($) {
	var count = "<?php echo $count; ?>";
	var format = "<?php echo $format; ?>";
	var m = "<?php echo $m_now; ?>";
	var sm = "<?php echo $select_month; ?>";
	if((format==2 || format==3) && m==sm){
		var sCourse = "<?php echo $select_course ?>";
		var course_name = <?php echo $tmp_name; ?>;
		var a = sCourse.split(",");
		/*var a_name = course_name.split(",");*/
		console.log(course_name);
		var str = "";
		var flag = 0;
		$.each(a, function(index, el) {
			var c = $(".address_course_"+el).length;
			if(c==0){
				flag = 1;
				str += course_name[index]+"<br> ไม่มีเวลา<?php echo $name_type ; ?> หรือ เวลา<?php echo $name_type ; ?>เต็ม กรุณาเลือกหลักสูตรไหม่อีกครั้ง";
				
			}else if(c==1){
				$(".address_course_"+el).filter(function() {
				     return $(this).css('display') != 'none';
				}).trigger('click');
			}
		});
		if(flag==1 && str!=""){
			msgError(str);
			//setTimeout(function(){goBack();}, 3000);
		}
	}else if(count==1){
		/*$("input[name=address_course]").trigger('click');*/
		$("input[name=address_course]").filter(function() {
		     return $(this).css('display') != 'none';
		}).trigger('click');
	}
	$('#update_from').on('click', function (e) {
			var valueSelected = $("#year option:selected").val();
			$("#select_year").val(valueSelected);
			var valueSelected = $("#month option:selected").val();
 			$("#select_month").val(valueSelected);
			$("#formSelect").submit();
	});
	$(".custom-detail-row").each(function(index, el) {
		var t = $(this).find('.se2-colum2-dtext').css("height","auto").height();
		var fix = 0;
		var c5 = $(this).find('.se2-row-colum5').height();
		if(t>30){
			var plus = $(this).find("div.custom-detail").css("width","73px").height();
			t = t- -plus;
			t = t - 50;
			$(this).find('.se2-colum2-dtext div.custom-detail > p').css("white-space", "normal");
			$(this).find("div.se2-colum2-dtext").css("wordBreak","break-all");
			$(this).css("height",t);
			$(this).find("> div").css("height",t);
			$(this).find("div.se2-colum2-dtext").css("height",t);
		}
		if(c5>50 && c5>t){
			var plus = $(this).find(".se2-row-colum5 > p").height();
			if(plus > 108){
				c5 = plus - 25;
				// console.log(plus);	
			}else  if(plus > 100){
				c5 = plus - 10;
				// console.log(plus);
			 // $(this).find('.se2-row-colum5 > p').css('background', 'green');
				
			}	
			$(this).css("height",c5);
			$(this).find("> div").css("height",c5);
		}
	});

	$("a.plus").click(function(event) {
		var text = $(this).attr("data");
		if(text!="")
			msgError(text);		
	});

	$('[data-toggle="tooltip"]').tooltip();
});
function goBack(){
		$('#formBack').attr('action', 'index.php?p=new-step-one');
		$('#formBack').attr('target', '_self');
		$('#formBack').submit();
}



//submit_btn
function my_f_submit(){
	var cid = "<?php echo $cid;?>";
	var course_detail_id = $("#my_select_course_detail").val();
	// var url = "chk_bls4.php";
	var url = "chk_bls5.php";
	var sec_id = "<?php echo $sec_id;?>";
	var type_id = "<?php echo $type_id;?>";

	//alert(sec_id+'='+type_id);
	var ck = ckdup_register(course_detail_id);
	// console.log(ck);
	if(ck) return;

	var format = "<?php echo $format; ?>";
	if(format==1)
		ckMemberCheck(course_detail_id);
	var str = "";
	if(format==2 || format==3){
		var sCourse = "<?php echo $select_course ?>";
		var a = sCourse.split(",");
		var flag = 0;
		$.each(a, function(index, el) {
			var c = 0;
			$(".address_course_"+el+":checked").each(function(index, el) {
				var course = $(this).attr("data");
				str += ","+course+":"+$(this).val();
				c++;
			});
			if(c==0){
				flag = 1;
			}
		});
		if(flag==1){
			msgError("คุณเลือกเวลา<?php echo $name_type ; ?>ยังไม่ครบ หรือ เคยสมัครไปแล้ว  กรุณาตรวจ<?php echo $name_type ; ?>อีกครั้ง");
			return false;
		}
		course_detail_id = str;
		$("#my_select_course_detail").val( str );
	}else{
		var course_id = "<?php echo $select_course ?>";
	}

	if( ($("#my_select_course_detail").val() == "want") ){
		msgError("คุณยังไม่ได้เลือกเวลา<?php echo $name_type ; ?>  หรือ เคยสมัครไปแล้ว");
	}
	
	$.ajax({
		//"dataType":'json', 
		"type": "POST",
		"url": url,
		//"data": param, 
		"data":{cid:cid
				, course_detail_id:course_detail_id
				, sec_id:sec_id
				, type_id:type_id
				, course_id:course_id
		},
		"success": function(data){
			// response (ถ้า connect ได้)
			// 1000:0000-00-00 : connect ได้ ข้อมูลไม่ถูกต้อง (จำนวนข้อมูล)
			// 1001:0000-00-00 : connect ได้ ข้อมูลไม่ถูกต้อง (เลขที่บัตร)
			// 1002:0000-00-00 : connect ได้ ข้อมูลไม่ถูกต้อง (วันที่)
			// 2000:0000-00-00 : invalid user or password
			// 3000:0000-00-00 : ไม่อยู่ในรายการห้ามสอบ
			// 4000:0000-00-00 : อยู่ในรายการห้ามสอบ
			console.log(data);

			var res = data.split(':');
			var rs_cd = res[0];
			var rs_date = res[1];
			var date_spit = rs_date.split('-');
			var year = parseInt(date_spit[0])+543;
			var month = date_spit[1];
			var day = date_spit[2];
			rs_date = day+'-'+month+'-'+year;				
			//alert(rs_date);

			if(rs_cd == '3000'){

				$('#form1').attr('action', 'index.php?p=new-step-three');
				$('#form1').attr('target', '_self');

				if( ($("#my_select_course_detail").val() != "want") ){
					$('#form1').submit();
				} 
			}else{
/*				
				if(rs_cd == '4000'){
					msgError("คุณติด black list ไม่มีสิทธิสมัครสอบจนถึงวันที่ "+rs_date);				
				}//end if
*/
				if ( rs_cd == '1000' ) {
					msgError("connect ได้ ข้อมูลไม่ถูกต้อง (จำนวนข้อมูล)");
				}else if ( rs_cd == '1001' ) {
					msgError("connect ได้ ข้อมูลไม่ถูกต้อง (เลขบัตร)");
				}else if ( rs_cd == '1002' ) {
					msgError("connect ได้ ข้อมูลไม่ถูกต้อง (วันที่)");
				}else if ( rs_cd == '2000' ) {
					msgError("invalid user or password");
				}else if ( rs_cd == '4001' ) {
					//msgError("อยู่ในรายการห้ามสอบ จนถึงวันที่ "+rs_date+" (block case 1 : โดย TSI)");
					msgError("ระบบไม่สามารถทำการรับสมัครสอบของท่านได้ ท่านไม่ได้รับอนุญาติให้สมัครจนถึงวันที่ "+rs_date);
				}else if ( rs_cd == '4002' ) {
					//msgError("อยู่ในรายการห้ามสอบ จนถึงวันที่ "+rs_date+" (block case 2 : กรณีผู้คุมสอบ)");
					msgError("ระบบไม่สามารถทำการรับสมัครสอบของท่านได้ ท่านไม่ได้รับอนุญาติให้สมัครจนถึงวันที่ "+rs_date);
				}else if ( rs_cd == '4003' ) {
					//msgError("อยู่ในรายการห้ามสอบ จนถึงวันที่ "+rs_date+" (block case 3 : กรณีเคยสอบผ่าน วิชา และยังไม่หมดอายุ)");
					msgError("ท่านสอบผ่านหลักสูตรนี้แล้ว ท่านจะสามารถสอบหลักสูตรนี้ได้อีกครั้ง ตั้งแต่วันที่ "+rs_date);
				}else if ( rs_cd == '4000' ) {
					msgError("อยู่ในรายการห้ามสอบ จนถึงวันที่ "+rs_date+" (block case 4 :  กรณีเป็นเจ้าหน้าที่ ATI)");
				}//end else if

			}//end else			
		}
	});

	/*
	$('#form1').attr('action', 'index.php?p=new-step-three');
	$('#form1').attr('target', '_self');
	if( $("#my_select_course_detail").val() != "want" ){
		$('#form1').submit();
	} else {
		msgError("คุณยังไม่ได้เลือกเวลา<?php echo $name_type ; ?>  หรือ เคยสมัครไปแล้ว");
	}*/
}

function getaddressCourse(radio_id_in){
		$("#my_select_course_detail").val( $(radio_id_in).val() );
}
$('#year').on('change', function (e) {
		var optionSelected = $("option:selected", this);
		var valueSelected = this.value;
		$("#select_year").val(valueSelected);
});
$('#month').on('change', function (e) {
		var optionSelected = $("option:selected", this);
		var valueSelected = this.value;
		$("#select_month").val(valueSelected);
		$("#formSelect").submit();
});
function ckMemberCheck(id){
		if(typeof id=="undefined") return;
		var member_id = "<?php echo $member_id;?>";
		var url = "data/member-block.php";
		var param = "course_detail_id="+id+"&member_id="+member_id;
		$.ajax( {
				"dataType":'json', 
				"type": "POST",
				"async": false, 
				"url": url,
				"data": param, 
				"success": function(data){  
						$.each(data, function(index, array){
								var code = array.result;
								var date = array.date;
								if(code=='3000'){
								}else if(code=='4000'){
										alert("คุณ อยู่ในรายการห้าม<?php echo $name_type ; ?> ถึงวันที่"+date);
										$('#formBack').attr('action', 'index.php?p=main');
										$('#formBack').attr('target', '_self');
										$('#formBack').submit();
								}
						});              
				}
		});
}
function ckdup_register(id){
	if(typeof id=="undefined") return;
	var member_id = "<?php echo $member_id;?>";
	var url = "data/ckdup-register.php";
	var param = "course_detail_id="+id+"&member_id="+member_id+"&format=<?php echo $format; ?>";
	console.log(param);
	var flag = false;
	$.ajax({
			"dataType":'json', 
			"type": "POST",
			"async": false, 
			"url": url,
			"data": param, 
			"success": function(data){  
         		var ck = 0;
         		$.each(data, function(index, array){
         			ck++;
         		});
         		if(ck>0){
         			flag = true;
         			msgError("คุณเคยสมัคร<?php echo $name_type ; ?>ไปแล้ว");
         		}
			}
	});

	return flag;

}
</script>
<style>
 a.plus{
  clear: both;
  display: block;
  position: relative;
  color: #2F2E7D;
  margin-top: 0px;
  text-align: center;
  width: 100%;
  padding-left: 0px;
  cursor: pointer;
}

span.short-text{
	white-space: nowrap;
	text-overflow: ellipsis;
	width: auto;
	display: block;
	overflow: hidden;
	cursor: pointer;
	font-size: 18px;
}
a.short-text{
	white-space: nowrap;
	text-overflow: ellipsis;
	width: auto;
	display: block;
	overflow: hidden;
	cursor: pointer;
	font-size: 18px;
}
a.hover { 
    background-color: yellow;
}
/* Tooltip */
.short-text + .tooltip > .tooltip-inner {
  background-color: #73AD21; 
  color: #FFFFFF; 
  border: 1px solid green; 
  padding: 15px;
  font-size: 26px;
}
/* Tooltip on top */
.short-text + .tooltip.top > .tooltip-arrow {
  border-top: 1px solid green;
}
/* Tooltip on bottom */
.short-text + .tooltip.bottom > .tooltip-arrow {
  border-bottom: 1px solid blue;
}
/* Tooltip on left */
.short-text + .tooltip.left > .tooltip-arrow {
  border-left: 1px solid red;
}
/* Tooltip on right */
.short-text + .tooltip.right > .tooltip-arrow {
  border-right: 1px solid black;
}

</style>