<?php
function edu_level_option($conn){
	global $db;
	//for edu level
	$presql = "SELECT edu_level.el_id, edu_level.el_name, edu_level.el_status FROM edu_level WHERE edu_level.el_status = '1' ";
	$res = $db->get($presql);
	$msg_option_eduLV = "";
	foreach ($res as $key => $row) {
		$el_id = $row['el_id'];
		$el_name = $row['el_name'];
		$msg_option_eduLV .= "<option value='$el_id'>$el_name</option>
		";
	}//end while
	return $msg_option_eduLV;
}

function edu_level_option_select($conn,$idEDU){
	global $db;
	//for edu level
	$presql = "SELECT edu_level.el_id, edu_level.el_name, edu_level.el_status FROM edu_level WHERE edu_level.el_status = '1' ";
	$res = $db->get($presql);
	$msg_option_eduLV = "";
	foreach ($res as $key => $row) {
		$el_id = $row['el_id'];
		$el_name = $row['el_name'];
		if($el_id == $idEDU){ $txt_selected = " selected"; } else { $txt_selected = ""; }
		$msg_option_eduLV .= "<option value='$el_id'$txt_selected>$el_name</option>
		";
	}//end while
	return $msg_option_eduLV;
}
?>