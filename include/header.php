<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="UTF-8">
	<title>numplus_education</title>
	<link rel="stylesheet" type="text/css" href="./css/style.css">
	<link rel="stylesheet" type="text/css" href="./css/style2.css">
	<link rel="stylesheet" type="text/css" href="./css/reset.css">
	<link rel="stylesheet" type="text/css" href="./css/template.css">
	<link rel="stylesheet" type="text/css" href="./css/jquery-ui.css">
	<link href='./css/uniform.default.css' rel='stylesheet'>
	<link rel="stylesheet" type="text/css" href="./css/jquery.noty.css">
	<link href='./css/noty_theme_default.css' rel='stylesheet'>
	<!--insert datatable core-->
	<link href="media/css/demo_table.css" rel="stylesheet" type="text/css" />
	<link href="media/css/TableTools.css" rel="stylesheet" type="text/css" />
	<!--insert fancybox css core-->
 	
	<script type="text/javascript" src="./js/jquery.js"></script>
	<script type="text/javascript" src="./js/jquery-1.7.2.min.js"></script>
	<script type="text/javascript" src="./js/jquery-ui.js"></script>

	<!--insert fancybox css core-->
	<link rel="stylesheet" href="./fancyapps/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
	<script type="text/javascript" src="./fancyapps/source/jquery.fancybox.pack.js?v=2.1.5"></script>

   <!--insert datatable core-->
	<script type="text/javascript" charset="utf-8" src="./media/js/jquery.dataTables.js"></script>
	<script type="text/javascript" charset="utf-8" src="./media/js/ZeroClipboard.js"></script>
	<script type="text/javascript" charset="utf-8" src="./media/js/TableTools.js"></script>
	<!-- notification plugin -->
	<script src="./js/jquery.noty.js"></script>
	<script src="./js/jquery.printPage.js"></script>
    <script src="./js/lib.js"></script>
   	<!-- application script for jquery validate demo -->
	<script src="./js/jquery.validate.js"></script>
	<script type="text/javascript" src="./js/jquery.maskedinput-1.0.js"></script>
	<script type="text/javascript" src="./js/additional-methods.js"></script>
	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script type="text/javascript">
	function myMenuShow() {
		$('#submenu').show();
	}
	function myMenuHide(){
		$('#submenu').hide();
	}

	function myMenuShow2() {
		$('#submenu2').show();
	}
	function myMenuHide2(){
		$('#submenu2').hide();
	}
	function myMenuShow3() {
		$('#submenu3').show();
	}
	function myMenuHide3(){
		$('#submenu3').hide();
	}
	function myMenuShow4() {
		$('#submenu4').show();
	}
	function myMenuHide4(){
		$('#submenu4').hide();
	}
	
	function myMenuShow21() {
		$('#submenu21').show();
	}
	function myMenuHide21(){
		$('#submenu21').hide();
		$('#submenu211').hide();
	}
	
	function myMenuShow211() {
		$('#submenu21').show();
		$('#submenu211').show();
	}
	function myMenuHide211(){
		$('#submenu21').hide();
		$('#submenu211').hide();
	}
	
	function myMenuShow22() {
		$('#submenu22').show();
		$("#sec_action").val("aob_rom");
	}
	function myMenuHide22(){
		$('#submenu22').hide();
	}
	
	function myMenuShow23() {
		$('#submenu23').show();
		$("#sec_action").val("sorb");
	}
	function myMenuHide23(){
		$('#submenu23').hide();
	}
	
	function myMenuShow24() {
		$('#submenu24').show();
		$("#sec_action").val("aob_rom_sorb");
	}
	function myMenuHide24(){
		$('#submenu24').hide();
	}
	
	function myMenuShow25() {
		$('#submenu25').show();
		$("#sec_action").val("aob_rom_tor_ar_yu");
	}
	function myMenuHide25(){
		$('#submenu25').hide();
	}
	
	function show_left(d1,d2,d3,d4){
		if(d1 == "have"){ $("#submenu2111").show(); } else { $("#submenu2111").hide(); }
		if(d2 == "have"){ $("#submenu2112").show(); } else { $("#submenu2112").hide(); }
		if(d3 == "have"){ $("#submenu2113").show(); } else { $("#submenu2113").hide(); }
		if(d4 == "have"){ $("#submenu2114").show(); } else { $("#submenu2114").hide(); }
		
		$('#submenu211').show();
	}
	/*
	function myMenu21Over(this){
		$(this).css("color","#2e2d7b");
	}
	*/
	</script>
</head>