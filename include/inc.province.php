<?php
function province_option($conn){
	global $db;
	$presql = "SELECT tb_pv.PV_CODE, tb_pv.PV_NAME FROM tb_pv ORDER BY tb_pv.PV_NAME ";
	$res = $db->get($presql);
	$msg_option_pv = "";
	foreach ($res as $key => $row) {
		$PV_CODE = $row['PV_CODE'];
		$PV_NAME = $row['PV_NAME'];
		$msg_option_pv .= "<option value='$PV_CODE'>$PV_NAME</option>
		";
	}//end loop
	return $msg_option_pv;
}

function province_option_selected($conn,$idPV){
	global $db;
	$presql = "SELECT tb_pv.PV_CODE, tb_pv.PV_NAME FROM tb_pv ORDER BY tb_pv.PV_NAME ";
	$res = $db->get($presql);
	$msg_option_pv = "";
	foreach ($res as $key => $row) {
		$PV_CODE = $row['PV_CODE'];
		$PV_NAME = $row['PV_NAME'];
		if($PV_CODE == $idPV){ $txt_selected = " selected"; } else { $txt_selected = ""; }
		$msg_option_pv .= "<option value='$PV_CODE'$txt_selected>$PV_NAME</option>
		";
	}//end loop
	return $msg_option_pv;
}

?>