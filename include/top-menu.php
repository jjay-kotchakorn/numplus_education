<?php
include_once "./share/section.php";
$q = " and a.active='T'";
if ( $_SESSION["login"]["info"]["fa"]=='F' ) {
	// $q .= " AND section_id<>3";
}//end if
$res = get_section($q, "", true);
$li_top_msg = ""; 
$li_top_msg2 = "";
$li_msg = "";
$li_msg_script = "";
$li_msg_script_sub_clear_color = "";
$li_msg_sub1 = "";
$li_msg_sub2 = "";
$li_msg_sub3 = "";
$li_msg_sub4 = "";

$mynum = 0;
foreach ($res as $k => $row) {
	$id = $row['section_id'];
	$name = $row['name'];
	$aob_rom = $row['aob_rom'];
	$sorb = $row['sorb'];
	$aob_rom_sorb = $row['aob_rom_sorb'];
	$aob_rom_tor_ar_yu = $row['aob_rom_tor_ar_yu'];
	$datetime_create = $row['datetime_create'];
	$last_update = $row['last_update'];
	$status = $row['status'];
	$mynum = $mynum*1+1;
	//$path_file_regis_rule = "./pdf/";
	$path_file_flow_rule = "./pdf/";

	//switch choose path file pdf
	switch ($id) {	
		case 1:
			$path_file_regis_rule = "./pdf/";
			$tab_target = "_blank";
			$path_file_regis_rule .= "pdf_for_test.pdf";
			$path_file_flow_rule .= "pdf_for_test.pdf";
			break;
		case 2:
			$path_file_regis_rule = "";
			$tab_target = "_self";
			$path_file_regis_rule .= "#";
			//$path_file_regis_rule .= "condition_iba.pdf";
			$path_file_flow_rule .= "pdf_for_test.pdf";
			break;
		case 3:
			$path_file_regis_rule = "./pdf/";
			$tab_target = "_blank";
			$path_file_regis_rule .= "pdf_for_test.pdf";
			$path_file_flow_rule .= "pdf_for_test.pdf";
			break;
		default:
			# code...
			break;
	}

	$li_top_msg .= "<li style=\"width:288px;\"><a href=\"$path_file_regis_rule\" target=\"$tab_target\">$name</a></li>";
	$li_top_msg2 .= "<li style=\"width:288px;\"><a href=\"$path_file_flow_rule\" target=\"_blank\">$name</a></li>";
	//$li_top_msg2 .= "<li style=\"width:288px;\"><a href=\"#\" onClick=\"javascript: window.open('./pdf/b$mynum.pdf','_blank');\">$name</a></li>";
	$li_msg .= "<li onclick=\"show_left('$aob_rom','$sorb','$aob_rom_sorb','$aob_rom_tor_ar_yu');\" id='submenu21s$mynum'>$name<span id=\"submenu21t$mynum\" style=\"display:none;\">$aob_rom,$sorb,$aob_rom_sorb,$aob_rom_tor_ar_yu,$id</span></li>";
	$li_msg_script .= "$( \"#submenu21s$mynum\" )
	.mouseover(function() {
		d4_ctrl( $(\"#submenu21t$mynum\").html() );
		$('#submenu211').show();
		mouse_over_color('#submenu21s$mynum');
	})
	.mouseout(function() {
		//mouse_out_color('#submenu21s$mynum');
	});";
	$li_msg_script_sub_clear_color .= "$(\"#submenu21s$mynum\").css('color','#666666');	$(\"#submenu21s$mynum\").css('background-color','#ffffff');";
	if($aob_rom == "have"){
		$li_msg_sub1 .= "<li onclick=\"menu_link2('$id');\">$name</li>";
	}
	if($sorb == "have"){
		$li_msg_sub2 .= "<li onclick=\"menu_link2('$id');\">$name</li>";
	}
	if($aob_rom_sorb == "have"){
		$li_msg_sub3 .= "<li onclick=\"menu_link2('$id');\">$name</li>";
	}
	if($aob_rom_tor_ar_yu == "have"){
		$li_msg_sub4 .= "<li onclick=\"menu_link2('$id');\">$name</li>";
	}
}//end foreach
global $aLogin;

?>
	<div class="topmenu">
		<div class="menu-zone">
			<div class="label-homepage">
				<a href="#" onClick="javascript: window.open('index.php','_self');">หน้าแรก</a>
			</div>
			<div class="right-menu">
				<ul>
					<li>
						<a href=""><div class="first-menu" onMouseOver="myMenuShow(this);" onMouseOut="myMenuHide(this);">
							<div class="first-menu-pic">
								<img src="./images/subm1.png">
							</div>
							<div class="first-menu-text">
								<p>
									ระเบียบการรับสมัคร
								</p>
								</div>
						</div></a>
						<ul class="list" id="submenu" 
						onmouseover="myMenuShow(this);"
						onmouseout="myMenuHide(this);" style="width:288px;">
							<div class="triangle-down" style="margin-bottom: 9px;"></div>
                            <?php echo $li_top_msg; ?>
						</ul>
					</li>
					
					<li>
						<a href=""><div class="third-menu" 
							onmouseover="myMenuShow2(this);"
							onmouseout="myMenuHide2(this);">
							<div class="third-menu-pic">
								<img src="./images/subm3.png">
							</div>
							<div class="third-menu-text"><p>ขั้นตอนการสมัคร</p></div>
						</div></a>
						<ul class="list2" id="submenu2"
						onmouseover="myMenuShow2(this);"
							onmouseout="myMenuHide2(this);" style="width:288px;">
							<div class="triangle-down" style="margin-bottom: 9px;"></div>
                            <?php echo $li_top_msg2; ?>
						</ul>
					</li>
					<li>
						<a href="#"><div class="fourth-menu"
							onmouseover="myMenuShow3(this);"
							onmouseout="myMenuHide3(this);"
							>
							<div class="fourth-menu-pic">
								<img src="./images/subm4.png" >
							</div>
							<div class="fourth-menu-text">
								<p>คู่มือชำระเงิน</p>
							</div>
						</div></a>
						<ul class="list3" id="submenu3"
						    onmouseover="myMenuShow3(this);"
							onmouseout="myMenuHide3(this);">
							<div class="triangle-down2" style="margin-bottom: 9px;"></div>
							<li><a href="#" onClick="javascript: window.open('./pdf/Bill_Payment_ATM.pdf','_blank');">ชำระผ่าน Bill-payment</a></li>
							<li><a href="#" onClick="javascript: window.open('http://www.ais.co.th/mpay/online.html','_blank');">ชำระผ่าน (mPAY)</a></li>
							<!-- <li><a href="#" onClick="javascript: window.open('./pdf/c3.pdf','_blank');">ชำระด้วยเงินสด</a></li> -->
							<li><a href="#" onClick="javascript: window.open('./pdf/pdf_for_test.pdf','_blank');">ชำระด้วยเงินสดที่ Numplus</a></li>
							<!-- <li><a href="#" onClick="javascript: window.open('./pdf/pay_ibclub.pdf','_blank');">ชำระที่ IB Club</a></li> -->
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="clr"></div>
	<div class="logobar">
	<div class="imglogo">
		<img src="./images/logo1.png">
		<!-- <img src="./images/logo1.png"> -->
		<!-- <img src="./images/logo2.png"> -->		
		<?php if($is_login){ ?><div class="menu2-right">
		<div class="logout-bottom" onClick="runLogout();">
			<a href="#"><p>• ออกจากระบบ</p>
			<img src="./images/logout.png" >
			</a>
		</div>
		<div class="profile-bottom">				
				<ul>
					 <li  onclick="document.getElementById('submenu4').style.display='block';">
					<a href="#"><p>• ข้อมูลส่วนตัว <?php echo $aLogin["fname_th"]; ?></p>
					<img src="./images/profile-bottom.png" ></a>
					<ul class="submenu2" id="submenu4"
					onmouseover="myMenuShow4(this);" 
					onmouseout="myMenuHide4(this);">
						<li class="submenu2-1"><a href="#" onClick="javascript: window.open('index.php?p=profile','_self');">รายละเอียดข้อมูลส่วนตัว</a></li>
						<li class="submenu2-3"><a href="#" onClick="javascript: window.open('index.php?p=register-history','_self');">ประวัติการสมัครและผลการอบรม/สอบ</a></li>
						<li class="submenu2-2"><a href="#" onClick="javascript: window.open('index.php?p=change-password','_self');">เปลี่ยนรหัสผ่าน</a></li>
					</ul>
					</li> 
				</ul>
			</div>

			<div class="menu2">
				<ul>
					<li class="list-menu2-2" ><a href="#" onClick="javascript: window.open('index.php?p=main','_self');" onMouseOver="myMenuShow21(this);" onMouseOut="myMenuHide21(this);">ประเภทใบอนุญาต/คุณวุฒิ</a>
                    <ul class="list99" id="submenu21" onmouseover="myMenuShow21(this);" onmouseout="myMenuHide21(this);" style="display:none;">
							<div class="triangle-down99" style="margin-bottom: 9px;"></div>
                            <?php echo $li_msg; ?>
						</ul>
                        <!-- <ul class="list99" id="submenu211" style="display:none; margin-left: 322px; width: 205px; height: 120px;" onmouseover="myMenuShow211(this);" onmouseout="myMenuHide211(this);"> -->
                        <ul class="list99" id="submenu211" style="display:none; margin-left: 323px; width: 99px; height: 121px;" onmouseover="myMenuShow211(this);" onmouseout="myMenuHide211(this);">
                        <div style="margin-top: 19px;"></div>
							<li id="submenu2111" onclick="menu_link('aob_rom');">อบรม</li>
							<li id="submenu2112" onclick="menu_link('sorb');">สอบ</li>
							<li id="submenu2113" onclick="menu_link('aob_rom_sorb');">อบรม + สอบ</li>
							<!-- <li id="submenu2114" onclick="menu_link('aob_rom_tor_ar_yu');">อื่นๆ</li> -->
						</ul>
                        </li>
					<li class="list-menu2-3" ><a href="#" onMouseOver="myMenuShow22(this);" onMouseOut="myMenuHide22(this);">อบรม</a>
                    <ul class="list99" id="submenu22" onmouseover="myMenuShow22(this);" onmouseout="myMenuHide22(this);" style="display:none;">
							<div class="triangle-down99" style="margin-bottom: 9px; margin-left: 25px;"></div>
                            <?php echo $li_msg_sub1; ?>
						</ul>
                        </li>
					<li class="list-menu2-4" ><a href="#" onMouseOver="myMenuShow23(this);" onMouseOut="myMenuHide23(this);">สอบ</a>
                    <ul class="list99" id="submenu23" onmouseover="myMenuShow23(this);" onmouseout="myMenuHide23(this);" style="display:none;">
							<div class="triangle-down99" style="margin-bottom: 9px; margin-left: 25px;"></div>
                            <?php echo $li_msg_sub2; ?>
						</ul>
                        </li>
					<li class="list-menu2-5" ><a href="#" onMouseOver="myMenuShow24(this);" onMouseOut="myMenuHide24(this);">อบรม+สอบ</a>
                    <ul class="list99" id="submenu24" onmouseover="myMenuShow24(this);" onmouseout="myMenuHide24(this);" style="display:none;">
							<div class="triangle-down99" style="margin-bottom: 9px; margin-left: 25px;"></div>
                            <?php echo $li_msg_sub3; ?>
						</ul>
                        </li>
<?php /*                      
					<li class="list-menu2-6" ><a href="#" onMouseOver="myMenuShow25(this);" onMouseOut="myMenuHide25(this);">อื่นๆ</a>

                    	<ul class="list99" id="submenu25" onmouseover="myMenuShow25(this);" onmouseout="myMenuHide25(this);" style="display:none;">
							<div class="triangle-down99" style="margin-bottom: 9px; margin-left: 40px;"></div>
                            <?php //echo $li_msg_sub4; ?>
                            <?php  echo "<p style= 'font-size: 20px; padding-top: 7px; padding-bottom: 7px;'>ไม่มีหัวข้อ</p>"; ?>
						</ul>
  
                    </li>
 */ ?>                   
				</ul>
		</div>
	</div>
<form id='form_menu' name='form_menu' method='post' action='' enctype='application/x-www-form-urlencoded'>
<input name="sec_id" type="hidden" value="no" id="sec_id" />
<input name="sec_action" type="hidden" value="no" id="sec_action" />
<input name="ch_submit_section" type="hidden" value="yes" id="ch_submit_section" />
</form>
<script language="javascript" type="text/javascript">
function menu_link(s_action){
	$('#form_menu').attr('action', 'index.php?p=new-condition-one');
	$('#form_menu').attr('target', '_self');
	$("#sec_action").val(s_action);
	$('#form_menu').submit();
}

function menu_link2(s_id){
	$('#form_menu').attr('action', 'index.php?p=new-condition-one');
	$('#form_menu').attr('target', '_self');
	$("#sec_id").val(s_id);
	$('#form_menu').submit();
}
function runLogout(){
	$.ajax({
		url: "update-logout.php",
		cache: false
		})
		.done(function( html ) {
		if(html == "completedLogout"){
			window.open('index.php','_self');
		}
	});
}
<?php echo $li_msg_script; ?>
function d4_ctrl(d4in){
	var res = d4in.split(','); 
	
	if(res[0] == "have"){ $("#submenu2111").show(); } else { $("#submenu2111").hide(); }
	if(res[1] == "have"){ $("#submenu2112").show(); } else { $("#submenu2112").hide(); }
	if(res[2] == "have"){ $("#submenu2113").show(); } else { $("#submenu2113").hide(); }
	if(res[3] == "have"){ $("#submenu2114").show(); } else { $("#submenu2114").hide(); }
	$("#sec_id").val(res[4]);
}
function mouse_over_color(id_in){
	<?php echo $li_msg_script_sub_clear_color; ?>
	$(id_in).css('color','#ffffff');
	$(id_in).css('background-color','#2e2d7b');
}
function mouse_out_color(id_in){
	$(id_in).css('color','#666666');
	$(id_in).css('background-color','#ffffff');
}
</script>
<?php } ?>
</div>