﻿<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>

<?php
// echo '<script type="text/javascript">msgError("<span style=\'font-size: 22px; color: white; font-weight: bold;\'>-----------------------------------------</span><span style=\'font-size: 22px; color: blue; font-weight: bold;\'>ยินดีต้อนรับสู่การเป็นสมาชิก</span><span style=\'font-size: 22px; color: white; font-weight: bold;\'>-----------------------------------------</span>");</script>';
global $db;

$datetime_now = date("Y-m-d H:i:s");
$member_id = $_SESSION["login"]["info"]["member_id"];

$q = " and a.active='T'";
if ( $_SESSION["login"]["info"]["fa"]=='F' ) {
	// $q .= " AND section_id<>3";
}//end if
$res = get_section($q, "", true);
$msg_left = "";
$c_num_rows = "";
// d($_SESSION["login"]["info"]["fa"]);
// d($res);
foreach ($res as $key => $row) {
	$id = $row['section_id'];
	$name = $row['name'];
	$aob_rom = $row['aob_rom'];
	$sorb = $row['sorb'];
	$aob_rom_sorb = $row['aob_rom_sorb'];
	$aob_rom_tor_ar_yu = $row['aob_rom_tor_ar_yu'];
	$datetime_create = $row['datetime_create'];
	$last_update = $row['last_update'];
	$status = $row['status'];

	$title = "";
	if ($id == 1) {
		$title = "data-toggle=\"tooltip\" title=\"ผู้แนะนำการลงทุน (IC) / แนวทางปฏิบัติด้านอนุพันธ์ (DRG) / ผู้วางแผนการลงทุน (IP) / นักวางแผนการเงิน (CFP) / อื่นๆ\"";
	}

	$msg_left .= "<div class=\"bottom\" onclick=\"runRight('$id','$aob_rom','$sorb','$aob_rom_sorb','$aob_rom_tor_ar_yu','label_right$id');\" id='label_right$id' name='label_right$id'><a class='no-action' href=\"#\" $title>$name</a></div><div class=\"clr\"></div>";
	$c_num_rows = $c_num_rows*1+1;
}
//$_SESSION["popup_one"]["msg"] = "ยินดีต้อนรับเข้าสู่การเป็นสมาชิก  ของสถาบันฝึกอบรม สมาคมบริษัทหลักทรัพย์ไทย ASCO Training Institute (ATI)";	
//$_SESSION["popup_one"]["msg"] = "ยินดีต้อนรับเข้าสู่การเป็นสมาชิก";	
$popup_one = $_SESSION["popup_one"]["msg"];
unset($_SESSION["popup_one"]["msg"]);
if($popup_one!=""){	
	//echo '<script type="text/javascript">msgError("'.$popup_one.'");</script>';
	echo '<script type="text/javascript">msgError("<span style=\'font-size: 22px; color: white; font-weight: bold;\'>----</span><span style=\'font-size: 22px; color: blue; font-weight: bold;\'>'.$popup_one.'</span><span style=\'font-size: 22px; color: white; font-weight: bold;\'>----</span>");</script>';
}//end if

$q = "SELECT a.register_id
	FROM register a 
	WHERE a.active='T' 
		AND a.pay_status=1 AND a.expire_date>'{$datetime_now}'
		AND a.member_id={$member_id}
";
$order_list = $db->get($q);
// echo $q;
// d($rs);
$cnt_order_list = count($order_list);
?>
<div class="contrainer">
	<div class="mainsite">
		<div class="row" style="padding-top: 10px;">
			<label style="font-size: 26px; font-weight:  bold; color: #ff5400; padding-left: 300px;">
<?php
			if ( $cnt_order_list>0 ) {
?>
					<span>ท่านยังมีรายการที่ยังไม่ได้ชำระเงินอยู่ <?php echo number_format($cnt_order_list);?> รายการ ท่านสามารถเข้าไปดูรายการที่ท่านสมัครได้ <a href="index.php?p=register-history" >ที่นี่</a></span>
<?php
			}//end if
?>
		</label>	
	</div>
		<div class="select-zone">
			<div class="select-left" id="head-label" >
				<p>
					1. เลือกประเภทใบอนุญาต/คุณวุฒิ
				</p>
			</div>
			<div class="select-center" id="head-label" >
				<p>
					2. เลือกการสมัคร
				</p>
			</div>

			<div class="bottom-left" id="bottom-label">
				<div class="bottom-left-zone" style="">
					<?php echo $msg_left; ?>
				</div>
			</div>

			<div class="bottom-center" id="bottom-label">
				<div class="bottom-center-zone">
					<div class="bottom_2" id="action1" onclick="selAction('action1');"><a href="#">อบรม</a></div>
					<div class="bottom_2" id="action2" onclick="selAction('action2');"><a href="#">สอบ</a></div>
					<div class="bottom_2" id="action3" onclick="selAction('action3');"><a href="#">อบรม + สอบ</a></div>
					<!-- <div class="bottom_2" id="action4" onclick="selAction('action4');"><a href="#">อบรมต่ออายุ</a></div> -->
				</div>
			</div>
		</div>
	</div>
</div>
<form id='form1' name='form1' method='post' action='' enctype='application/x-www-form-urlencoded'>
	<input name="sec_id" type="hidden" value="no" id="sec_id" />
	<input name="sec_action" type="hidden" value="no" id="sec_action" />	
	<input name="select_id" type="hidden" value="no" id="select_id" />
	<input name="select_action" type="hidden" value="no" id="select_action" />
	<input name="ch_submit_section" type="hidden" value="yes" id="ch_submit_section" />
	<input name="c_num_rows" type="hidden" value="<?php echo $c_num_rows; ?>" id="c_num_rows" />
</form>
<script type="text/javascript">

$(window).load(function() {
 	// $("#label_right1").click(); 
 	jQuery('#label_right1').click();
});

function runRight(sec_id_in,aob_rom_in,sorb_in,aob_rom_sorb_in,aob_rom_tor_ar_yu_in,id_label_in){
	$('#sec_id').val(sec_id_in);	
	$(".bottom").css({"border":"3px solid #f9f9f9","opacity":"0.4"});
	id_label_in = "#" + id_label_in;
	$(id_label_in).css({"border":"3px solid #4a9bea","opacity":"1"});	
	$(".bottom_2").css({"border":"","opacity":""});
	$(".bottom_2").show();
	if(aob_rom_in == "nothave"){ $("#action1").hide(); }
	if(sorb_in == "nothave"){ $("#action2").hide(); }
	if(aob_rom_sorb_in == "nothave"){ $("#action3").hide(); }
	if(aob_rom_tor_ar_yu_in == "nothave"){ $("#action4").hide(); }
}
function selAction(my_selected){
	if( $("#sec_id").val() == "no" ){ return; }
	
	$(".bottom_2").css({"border":"2px solid #f9f9f9","opacity":"0.4"});
	my_selected2 = "#" + my_selected;
	$(my_selected2).css({"border":"2px solid #4a9bea","opacity":"1"});	
	$('#form1').attr('action', 'index.php?p=new-condition-one');
	$('#form1').attr('target', '_self');

	if(my_selected == "action1"){ $("#sec_action").val("aob_rom"); }
	if(my_selected == "action2"){ $("#sec_action").val("sorb"); }
	if(my_selected == "action3"){ $("#sec_action").val("aob_rom_sorb"); }
	if(my_selected == "action4"){ $("#sec_action").val("aob_rom_tor_ar_yu"); }
	var t = $('#sec_id').val();
	var tx = $('#sec_action').val();
	$("#select_id").val(t);
	$("#select_action").val(tx);
	$('#form1').submit();	
}
</script>

<style>
	.noty_bar.noty_theme_default.noty_layout_center.noty_error{
		left: 50% !important;
		z-index: 999999999999;
		margin-left: -170px!important;
	}
</style>