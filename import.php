<?php 
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";
?>
<meta charset="UTF-8">
<?php 
include ('share/datatype.php');
global $db;
require_once dirname(__FILE__) . '/PHPExcel.php';
$preview = false;
$args = array();
$args[] = "run_no";
$args[] = "receipttype_id";
$args[] = "name";
$args[] = "no";
$args[] = "gno";
$args[] = "moo";
$args[] = "soi";
$args[] = "road";
$args[] = "district_id";
$args[] = "amphur_id";
$args[] = "province_id";
$args[] = "postcode";
$args[] = "taxno";
$args[] = "branch";
   
$data = array();
$headBar = "";
$fileName = "import.xls";
error_reporting(E_ALL);
ini_set('display_errors','off');

$objPHPExcel = PHPExcel_IOFactory::load($fileName);
					foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
						$worksheetTitle     = $worksheet->getTitle();
				      $highestRow         = $worksheet->getHighestRow(); // e.g. 10
				      $highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
				      $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
				      $nrColumns = ord($highestColumn) - 64;
				      //echo"<div class='box-header well' data-original-title>
				      //<h2><i class='icon-edit'></i>&nbsp;";
				      $headBar .= "นำเข้าไฟล์ ".$worksheetTitle." ";
				      $headBar .= $nrColumns . ' คอลัมน์ (A-' . $highestColumn . ') ';
				      $headBar .= ' จำนวน ' . ($highestRow-1) . ' เร็คคอร์ด.';
				     // echo "</h2>
				      //</div>";
				       //echo '<table class="table table-striped">';
				      for ($row = 3; $row <= $highestRow; ++ $row) {
				          //echo '<tr>';
				  		$t = array();
				      	for ($col = 1; $col < $highestColumnIndex; ++ $col) {
				      		$cell = $worksheet->getCellByColumnAndRow($col, $row);
				      		$val = $cell->getValue();

				      		$dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
				            // echo '<td>' .$val.'</td>';
				      		$t[$args[$col]] = $val;
				      	}
				      	$data[] = $t;
				         //echo '</tr>';
				      }
				       //echo '</table>';
				  }				

				  foreach ($data as $key => $field) {
				  		if(trim($field["name"])=="") continue;
				  		$address_other  = $field["no"]." ".$field["gno"]." ".$field["moo"]." ".$field["soi"]." ".$field["road"]." ".$field["district_id"]." ".$field["amphur_id"]." กรุงเทพมหานคร ".$field["postcode"];
				  		$field["district_id"] = str_replace("แขวง", "", $field["district_id"]);
				  		$field["district_id"] = str_replace("ต.", "", $field["district_id"]);				  		
				  		$receipttype_id = get_datatype_import($field["receipttype_id"], "receipttype", true);
				  		$amphur_id = get_datatype_import($field["amphur_id"], "amphur", true);
				  		$district_id = get_datatype_import($field["district_id"], "district", true);
						$args = array('table' => 'receipt',
								  'name' => $field["name"],
								  'receipttype_id' => $receipttype_id,
								  'no' => $field["no"],
								  'gno' => $field["gno"],
								  'moo' => $field["moo"],
								  'soi' => $field["soi"],
								  'road' => $field["road"],
								  'district_id' => $district_id,
								  'amphur_id' => $amphur_id,
								  'province_id' => 1,
								  'postcode' => $field["postcode"],
								  'address_other' => $address_other,
								  'taxno' => $field["taxno"],
								  'branch' => $field["branch"],
								  'recby_id' => 1
						);
						//$db->set($args);
				  }
?>