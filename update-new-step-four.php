<?php
include_once "./share/authen.php";
include_once "./share/course.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";
global $db, $EMPID;
$date_now = date('Y-m-d H:i:s');
//member data
$member_info = $_SESSION["login"]["info"];
$member_id = $member_info['member_id'];

$register_id = !empty($_POST["register_id"]) 
				? (int)$_POST["register_id"] 
				: (int)$_SESSION["last_register_login"][$member_id];
$_SESSION["last_payment_type"][$member_id] = trim($_POST["payment_type"]);
// echo $register_id;
// d($_POST); 
// die();

if ( !empty($register_id) && !empty($_SESSION["last_payment_type"][$member_id]) ) {

	$info = get_register("", $register_id);
	$reg_info = $info[0];
	unset($info);
	// d($reg_info);


	$q = "SELECT data_value FROM config WHERE name='payment_options_setting'";
	$array_data = $db->data($q);
	if(!$array_data){
		$array_data = array();
	}else{
		$array_data = json_decode($array_data, true);
	}//end else

	$coursetype_id = $reg_info["coursetype_id"];
	$section_id = $reg_info["section_id"];
	$q = "SELECT name_eng FROM coursetype WHERE coursetype_id={$coursetype_id}";
	$sec_action = $db->data($q);

	$option_billpayment = (int) $array_data[$section_id][$sec_action][2]["day"];
	$option_paysbuy = (int) $array_data[$section_id][$sec_action][1]["day"];
	$option_ati = (int) $array_data[$section_id][$sec_action][3]["day"];
	$option_asco = (int) $array_data[$section_id][$sec_action][4]["day"];
	$option_mpay = (int) $array_data[$section_id][$sec_action][5]["day"];
	// d($array_data[$section_id][$sec_action][2]);
	
	$pay = $_SESSION["last_payment_type"][$member_id];
	if($pay=="bill_payment") $d = $option_billpayment;
	if($pay=="paysbuy") $d = $option_paysbuy;
	if($pay=="at_ati") $d = $option_ati;
	if($pay=="at_asco") $d = $option_asco;
	if($pay=="mpay") $d = $option_mpay;
	if($d==0){
		$d = 2;
	}//end if
	$expire_date = convert_mktime(date("Y-m-d H:i:s")) + ($d * 24 * 60 * 60);


	$args = array();
	$args["table"] = "register";
	$args["id"] = $register_id;
	$args["pay"] = $pay;


	if ( $pay=="bill_payment" ) {	
/*		
		$tmp = array();
		$tmp = rundocno($coursetype_id, $section_id);
		$args = array_merge($args, $tmp);
*/
	}else{
		// $args["ref1"] = "";
		// $args["ref2"] = "";
	}//end else


	// $args["docno"] = "";
	$args["auto_del"] = "F";
	$args["expire_date"] = date("Y-m-d", $expire_date);

	// d($args); 
	$db->set($args);
	// die();

	//write log
	$str_txt = "";
	foreach ($args as $key => $v) {
		// $data = trim($v[$key]);
		$data = trim($v);
		$str_txt .= "{$key}={$data}|";
	}//end loop $v
	write_log("update-new-step-four", $str_txt, "a","./log/");

	// header("Location: ati-thank.php");

  	if ( !empty($pay) && ($pay=="bill_payment" || $pay=="at_ati" ) ) {
      if ( $pay=="bill_payment" ) {
        $url = "invoice.php?register_id=".$register_id;
      }else if ( !empty($pay) && $pay=="at_ati" ) {
        $url = "invoice-ati.php?register_id=".$register_id;
      }
    }else if ( !empty($pay) && $pay=="mpay" ) {
      	// $url = "ati-thank.php;
/*
    	var register_id = "<?php echo $_SESSION["last_register_login"][$member_id]; ?>";
		var url = "./mpay/pay-now.php?register_id="+register_id;
*/		
		$url = "./mpay/pay-now.php?register_id=".$register_id;
    }//end else if

    // d($_POST);
    // echo $url;
    header("Location: $url");
	die();
}//end if



/*
$args = array();
$args["p"] = "register";
$args["register_id"] = $register_id;
$args["type"] = "info";
if ($args["register_id"]) {
	$args["msg"] = "success";
}else{
	$args["msg"] = "error";
}
$args["text"] = "";

redirect_url($args);
*/

?>
