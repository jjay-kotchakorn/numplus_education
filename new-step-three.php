<?php
include_once "./lib/lib.php";
include_once "./share/member.php";
include_once "./share/datatype.php";
include_once "./share/course.php";
global $db;
$educationlevel = datatype(" and a.active='T'", "educationlevel", true);
$province = datatype_sort_name(" and a.active='T'", "province", true);
$receipttype = datatype_sort_name(" and a.active='T'", "receipttype", true);

if(!isset($_POST['ch_submit_section'])){
echo <<<holyy
<script>
window.open('index.php','_self');
</script>
holyy;
}
if($_POST['ch_submit_section'] != "yes"){
echo <<<holy
<script>
window.open('index.php?p=select_s1','_self');
</script>
holy;
}
if (!isset($_SESSION)) {//initialize the session
  session_start();
}
global $thai_month_arr;
//member data
$member_info = $_SESSION["login"]["info"];
$member_id = $member_info['member_id'];
$sec_id = $_POST['sec_id'];
$sec_action = $_POST['sec_action'];
$my_select_course = $_POST['my_select_course'];
$my_select_course_detail = $_POST['my_select_course_detail'];

/*echo "<pre>";
print_r($member_info);
echo "</pre>";*/

if ( ($member_info["title_th"]=='อื่นๆ')||($member_info["title_en"]=='Other') ) {
	$member_info["title_th"] = $member_info["title_th_text"];
	$member_info["title_en"] = $member_info["title_en_text"];
}
$check_sit_full = "F";
$format = get_select_course($sec_id, $sec_action);
$type_id = map_course_type($sec_action);
$q = "SELECT name FROM `coursetype` WHERE coursetype_id=$type_id";
$name_type = $db->data($q);
$list = explode(",", $_POST['my_select_course']);
$list_detail = explode(",", trim($_POST['my_select_course_detail'], ","));
$details_id = "";
$details = "";
if($format==3){
 	foreach ($list_detail as $key => $value) {
 		$c = explode(":", $value);
 		$details_id[$c[0]] .= ",".$c[1];
 		$details .= ",".$c[1];
 	}
	$details = trim($details, ",");
	$con = " and a.course_id in ($my_select_course)";
	$r = get_course($con);
	$title = "";
	$no = 1;
	foreach ($r as $key => $row) {
		$course_id = $row['course_id'];
		$parent_title = $row['title'];
		$id = trim($details_id[$course_id], ",");
		if($id){	    
			$con = " and a.course_detail_id IN ($id) ";
			$res = get_course_detail($con);
			$msg_data .= "<br/>";
	    	
			$q = "select title from course where active='T' and course_id=$course_id";
			$name = $db->data($q);		
			if($res){
				$msg_data .= "
					<tr class='tr-header-fa'>
						<td class='td-col-1' colspan='10'>{$name}</td>
					</tr> 
				";  		
				foreach ($res as  $row) {
				$course_detail_id = $row['course_detail_id'];
				$course_id = $row['course_id'];

				$con_c = " and a.course_id = $course_id";
				$r_c = get_course($con_c);
				$title = $r_c[0]['title'];

				$day = $row['day'];
				$date = revert_date($row['date']);
				$time = $row['time'];
				$address = $row['adress'];
				$date_other = trim($row["date_other"]);
				$pr = "";
				if($date_other!="") $date .= " ".$date_other; 
				$address_detail = $pr.$row['address_detail'];
				$get_all = 0;
				$not_id = "";
				
					$use = $row["sit_all"];
					//$use = get_use_chair($course_detail_id);				 
					$chair_all = $row['chair_all'];
					$sit_all = $chair_all - ($use + $row["book"]);
		          $status = $row['status'];
		          $open_regis = $row['open_regis'];
		          $end_regis = $row['end_regis'];
		          $datetime_add = $row['datetime_add'];
		          $datetime_update = $row['datetime_update'];
				$q = " select count(register_id) as c from register where active='T' and course_detail_id like '%{$course_detail_id}%' and pay_status in (1,3,5,6)";
				$count = $db->data($q);
				$count_full = $count+$row["book"];
				if($count_full>=$chair_all){
					
					$check_sit_full = "T";
				}		                

					if($tr_current_bg == "tr-bg-white"){
						$tr_current_bg = "tr-bg-silver";
					} else if($tr_current_bg == "tr-bg-silver"){
						$tr_current_bg = "tr-bg-white";
					}
		            else {
		            	$tr_current_bg = "tr-bg-silver"; 
		            }
		            
		            $msg_data .= "<tr class='$tr_current_bg tr-item-fa'>
		          				  <td class='td-col-0'>$no.</td>
		                          <td class='td-col-1'>$title</td>
	                      				<td class='td-col-2'>
										 <span class='span-day'>$day.</span>
										 <span class='span-date'>$date</span>
									</td>
									<td class='td-col-3'>$time</td>
									<td class='td-col-4'>$address</td>
									<td class='td-col-5'>$address_detail</td>
									<td class='td-col-6'>$sit_all</td>
									<td class='td-col-7'>$chair_all</td>
									<td class='td-col-8'><center>$status</<center></td>
									<td class='td-col-9'><center>$open_regis</<center></td>
									<td class='td-col-10'><center>$end_regis</<center></td>
		                      </tr> ";
		                      	                      
		      	}
	    	}
		}
		$no++;
	}

}else if($format==2){
 	foreach ($list_detail as $key => $value) {
 		$c = explode(":", $value);
 		$details_id[$c[0]] .= ",".$c[1];
 		$details .= ",".$c[1];
 	}
	$details = trim($details, ",");
	$con = " and a.course_id in ($my_select_course)";
	$r = get_course($con);
	$title = "";
	
	$no = 1;	    		
	foreach ($r as $key => $row) {
		$course_id = $row['course_id'];
		$parent_title = $row['title'];
		$id = trim($details_id[$course_id], ",");
		if($id){	    
			$con = " and a.course_detail_id IN ($id) ";
			$res = get_course_detail($con);
			$msg_data .= "<br/>";
			$q = "select title from course where active='T' and course_id=$course_id";
			$name = $db->data($q);	    	
	    	if($res){
	    		
				$msg_data .= "
					<tr class='tr-header-fa'>
						<td class='td-col-1' colspan='10'>{$name}</td>
					</tr> 
				";  
				foreach ($res as  $row) {
		          $course_detail_id = $row['course_detail_id'];
		          $course_id = $row['course_id'];
		  
		          $con_c = " and a.course_id = $course_id";
		          $r_c = get_course($con_c);
		          $title = $r_c[0]['title'];

				$day = $row['day'];
				$date = revert_date($row['date']);
				$time = $row['time'];
				$address = $row['address'];
				$date_other = trim($row["date_other"]);
				$pr = "";
				if($date_other!="") $date .= " ".$date_other; 
				$address_detail = $pr.$row['address_detail'];
				
				$use = $row["sit_all"];
				//$use = get_use_chair($course_detail_id);				 
				$chair_all = $row['chair_all'];
				$sit_all = $chair_all - ($use + $row["book"]);

				$status = $row['status'];
				$open_regis = revert_date($row['open_regis']);
				$end_regis = revert_date($row['end_regis']);
		          $datetime_add = $row['datetime_add'];
		          $datetime_update = $row['datetime_update'];
				$q = " select count(register_id) as c from register where active='T' and course_detail_id like '%{$course_detail_id}%' and pay_status in (1,3,5,6)";
				$count = $db->data($q);
				$count_full = $count+$row["book"];
				if($count_full>=$chair_all){
					
					$check_sit_full = "T";
				}		                
					if($tr_current_bg == "tr-bg-white"){
						$tr_current_bg = "tr-bg-silver";
					} else if($tr_current_bg == "tr-bg-silver"){
						$tr_current_bg = "tr-bg-white";
					}
		            else {
		            	$tr_current_bg = "tr-bg-silver"; 
		            }
		            
					$msg_data .= "
						<tr class='$tr_current_bg tr-item-fa'>
								 <td class='td-col-0'>$no.</td>											
								<td class='td-col-2'>
									 <span class='span-day'>$day.</span>
									 <span class='span-date'>$date</span>
								</td>
								<td class='td-col-3'>$time</td>
								<td class='td-col-4'>$address</td>
								<td class='td-col-5'>$address_detail</td>
								<td class='td-col-6'>$sit_all</td>
								<td class='td-col-7'>$chair_all</td>
								<td class='td-col-8'><center>$status</<center></td>
								<td class='td-col-9'><center>$open_regis</<center></td>
								<td class='td-col-10'><center>$end_regis</<center></td>
						</tr> ";		       					
		      	}
		        $no++;              
	    	}
		}
		
	}
}else{
		$con = " and a.course_id in ($my_select_course)";
		$r = get_course($con);
		$title = "";
		//where and time ...my_select_course_detail

		$parent_con = " and a.course_id = $parent_id";
		$parent_rec = get_course($parent_con);
		$parent_title = $parent_rec[0]['title'];
		foreach ($r as $key => $row) {
			$course_id = $row['course_id'];
		    $parent_id = $row['parent_id'];
			$section_id = $row['section_id'];
			$code = $row['code'];
			$title = $row['title'];
			$set_time = $row['set_time'];
			$life_time = $row['life_time'];
			$price = $row['price'];
			$rereg_date = $row['reg_date'];
			$status = $row['status'];	
		}
		$con = " and a.course_detail_id IN ($my_select_course_detail) ";
	    $row = get_course_detail($con);
	    if($row) $row = $row[0];
	    $course_detail_id = $row['course_detail_id'];
	    $course_id = $row['course_id'];
	    $day = $row['day'];
	    $date = revert_date($row['date']);
	    $time = $row['time'];
	    $address = $row['address'];
	    $date_other = trim($row["date_other"]);
		$pr = "";
		if($date_other!="") $date .= " ".$date_other; 
		$address_detail = $pr.$row['address_detail'];
		$use = $row["sit_all"];
		//$use = get_use_chair($course_detail_id);				 
		$chair_all = $row['chair_all'];
		$sit_all = $chair_all - ($use + $row["book"]);
	    $status = $row['status'];
	    $open_regis = $row['open_regis'];
	    $end_regis = $row['end_regis'];
	    $datetime_add = $row['datetime_add'];
	    $datetime_update = $row['datetime_update'];
		$q = " select count(register_id) as c from register where active='T' and course_detail_id like '%{$course_detail_id}%' and pay_status in (1,3,5,6)";
		$count = $db->data($q);
		//$count_full = $count+$row["book"];
		$count_full = $count + ($row["book"] - $row["book_all"]);
		// echo $q."\r\n";
		// echo $count." / ".$count_full." / ".$chair_all;

		if($count_full>=$chair_all){
			
			$check_sit_full = "T";
		}
}      
?>
<div class="bgselect3"  style="height:auto;padding-bottom:32px;">
		<div class="bodyselect3">
			<div class="imgstep1">
            <table id="Table_01" width="1248" height="52" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
		 <a href="#" onclick="goBackStepOne();"><img src="images/step03_01.jpg" width="302" height="52" alt=""></a></td>
		<td>
			<a href="#" onclick="goBack();"><img src="images/step03_02.jpg" width="306" height="52" alt=""></a></td>
		<td>
			<img src="images/step03_03.jpg" width="308" height="52" alt=""></td>
		<td>
			<img src="images/step03_04.jpg" width="332" height="52" alt=""></td>
	</tr>
</table>
            </div>
			<div class="se-title"><center><p>ยืนยันการลงทะเบียน  <?php //echo "{$thai_month_arr[$_SESSION["select_month"]]} {$_SESSION["select_year"]} ";?></p></center></div>
            <form id='form1' name='form1' method='post' action='' enctype='application/x-www-form-urlencoded'>           
             <?php  if($format==2){ ?>             			
             			<div class="se3-title"><p>หลักสูตรที่สมัคร</p></div>                      
                         <table class="new-step-three tb-fa" width="100%">
							<tr class="tr-header-fa">
									<td class="td-col-0"></td>												
									<td class="td-col-2" style="width:12%;">วัน</td>
									<td class="td-col-3" style="width:7%;">เวลา</td>
									<td class="td-col-4">สถานที่</td>
									<td class="td-col-5">ข้อมูลสถานที่<?php echo $name_type; ?></td>
									<td class="td-col-6" style="width:7%;">ที่นั่ง</br>คงเหลือ</td>
									<td class="td-col-7" style="width:7%;">ที่นั่ง</br>ทั้งหมด</td>
									<td class="td-col-8" style="width:7%;">สถานะ</td>
									<td class="td-col-9" style="width:7%;">เปิด</br>รับสมัคร</td>
									<td class="td-col-10" style="width:7%;">ปิด</br>รับสมัคร</td>
							</tr> 
                            <?php echo $msg_data; ?>
                         </table>
			 <?php } else if($format==3){ ?>			 
             			<div class="se3-title"><p>หลักสูตรที่สมัคร</p></div> 
                         <table class="new-step-three tb-fa" width="100%">
								<tr class="tr-header-fa">
										<td class="td-col-0"></td>												
										<td class="td-col-2" style="width:12%;">วัน</td>
										<td class="td-col-3" style="width:7%;">เวลา</td>
										<td class="td-col-4">สถานที่</td>
										<td class="td-col-5">ข้อมูลสถานที่<?php echo $name_type; ?></td>
										<td class="td-col-6" style="width:7%;">ที่นั่ง</br>คงเหลือ</td>
										<td class="td-col-7" style="width:7%;">ที่นั่ง</br>ทั้งหมด</td>
										<td class="td-col-8" style="width:7%;">สถานะ</td>
										<td class="td-col-9" style="width:7%;">เปิด</br>รับสมัคร</td>
										<td class="td-col-10" style="width:7%;">ปิด</br>รับสมัคร</td>
								</tr> 
                            <?php echo $msg_data; ?>
                         </table>
			 <?php } else { ?>
                      <div class="se3-title"><p>หลักสูตรที่สมัคร</p></div>
                      <div class="se3-colum-title"><p>หลักสูตร</p></div>
                      <div class="se3-colum-text"><p><?php echo $title; ?></p><input name="c_title" type="hidden" value="<?php echo $title; ?>" id="c_title" /></div>
                      <div class="se3-line"></div>
                      <div class="se3-title"><p>เวลา / ห้อง</p></div>
                      <div class="se3-colum-title"><p>วัน</p></div>
                      <div class="se3-colum-text2"><div class="se3-colum2-day">
                      <!-- <center><p><?php echo $day; ?></p><input name="day" type="hidden" value="<?php echo $day; ?>" id="day" /></center></div> -->
                      <center><p><?php echo $day."."; ?></p><input name="day" type="hidden" value="<?php echo $day; ?>" id="day" /></center></div>
                      <div class="se3-colum-textday"><p><?php echo $date; ?></p><input name="date" type="hidden" value="<?php echo $date; ?>" id="date" /></div></div>	
                      <div class="se3-colum-title"><p>เวลา</p></div>
                      <div class="se3-colum-text"><p><?php echo $time; ?></p><input name="time" type="hidden" value="<?php echo $time; ?>" id="time" /></div>
                      <div class="se3-colum-title"><p>สถานที่</p></div>
                      <div class="se3-colum-text"><p><?php echo $address_detail; ?></p><input name="address" type="hidden" value="<?php echo $address; ?>" id="address" /></div>
                      <!-- <div class="se3-colum-text"><p><?php echo $address; ?></p><input name="address_detail" type="hidden" value="<?php echo $address_detail; ?>" id="address_detail" /></div> -->
                      <div class="se3-colum-title"><p>ข้อมูลสถานที่<?php echo $name_type; ?></p></div>
                      <div class="se3-colum-text"><p><?php echo $address; ?></p><input name="address_detail" type="hidden" value="<?php echo $address_detail; ?>" id="address_detail" /></div>
                      <!-- <div class="se3-colum-text"><p><?php echo $address_detail; ?></p><input name="address" type="hidden" value="<?php echo $address; ?>" id="address" /></div> -->
			 <?php } ?>
                
                <div class="se3-line"></div>
				<div class="se3-title"><p>ข้อมูลผู้สมัคร</p></div>
				<div class="se3-colum-title"><p>เลขที่บัตรประจำตัว 13 หลัก</p></div>
				<div class="se3-colum-text"><p><?php echo $member_info["username"];?></p><input name="username" type="hidden" value="<?php echo $member_info["username"];?>" id="username" /></div>
				<div class="se3-colum-title">
				  <p>คำนำหน้า - ชื่อ - นามสกุล</p></div>
				<div class="se3-colum-text">
				<p><?php echo $member_info["title_th"];?> &nbsp&nbsp&nbsp&nbsp&nbsp <?php echo $member_info["fname_th"];?> &nbsp&nbsp&nbsp&nbsp&nbsp <?php echo $member_info["lname_th"];?> </p>
				<input name="title_th" type="hidden" value="<?php echo $member_info["title_th"];?>" id="title_th" />
                <input name="fname_th" type="hidden" value="<?php echo $member_info["fname_th"];?>" id="fname_th" />
                <input name="lname_th" type="hidden" value="<?php echo $member_info["lname_th"];?>" id="lname_th" /></div>
				<div class="se3-line"></div>
				<div class="se3-title"><p>ข้อมูลสำหรับแสดงบนใบเสร็จรับเงิน</p></div>
				<div class="se3-colum-title"><p>ออกใบเสร็จ <span class="red">*</span></p></div>
				<div class="se3-colum-textform">
					<div class="se3-from-area">
						<div class="se3-from-radio">
							<p><input type="radio" name="slip_type" id="slip_type" value="individuals" class="required"
								<?php if($member_info["slip_type"] == "individuals"){ echo "checked"; } ?>  
								onclick="select_normal_niti('normal');">ออกในนามบุคคลธรรมดา</p> 
						</div>
					</div>
					<div class="se3-from-area">
						<div class="se3-from-radio">
							<p><input type="radio" name="slip_type" id="slip_type" value="corparation" class="required"
								<?php if($member_info["slip_type"] == "corparation"){ echo "checked"; } ?> 
								onclick="select_normal_niti('niti');">ออกในนามนิติบุคคล (สำหรับเบิกค่าใช้จ่าย)</p> 
						</div>
					</div>
				</div>
                
                <div id="divNiti">
				<div class="se3-colum-title-L"><p>ออกใบเสร็จในนาม <span class="red">*</span></p></div>
				<div class="se3-colum-textform">
					<div class="se3-from-area"><div class="se3-from-radio"><p><input type="radio"  class="required" name="receipttype_id"  id="receipttype_id" value="1" onclick="getDropDown('#receipt_id', this.value, 'receipt', 'data/receipt.php'); receipttype_text_required('no');">บริษัทหลักทรัพย์</p> </div></div>
					<div class="se3-from-area">	<div class="se3-from-radio"><p><input type="radio"  class="required" name="receipttype_id"  id="receipttype_id" value="2" onclick="getDropDown('#receipt_id', this.value, 'receipt', 'data/receipt.php'); receipttype_text_required('no');">ธนาคาร</p> </div></div>
					<div class="se3-from-area">	<div class="se3-from-radio"><p><input type="radio"  class="required" name="receipttype_id"  id="receipttype_id" value="3" onclick="getDropDown('#receipt_id', this.value, 'receipt', 'data/receipt.php'); receipttype_text_required('no');">บริษัทหลักทรัพย์จัดการกองทุน</p> </div></div>
					<div class="se3-from-area">	<div class="se3-from-radio"><p><input type="radio"  class="required" name="receipttype_id"  id="receipttype_id" value="4" onclick="getDropDown('#receipt_id', this.value, 'receipt', 'data/receipt.php'); receipttype_text_required('no');">บ.ประกันชีวิต</p> </div></div>
					<!-- <div class="se3-from-area">	<div class="se3-from-radio"><p><input type="radio"  class="required" name="receipttype_id" id="receipttype_id" value="5" onclick="receipt_other(); receipttype_text_required('yes');"/>อื่น ๆ (โปรดระบุ)</p> </div></div>
					<div class="se3-from-textother">
						<input type="text" onkeypress="enterToSubmit();" maxlength="20" name="receipttype_text" id="receipttype_text" >
					</div>	
					<div class="se3-colum-text">
						<div class="se3-from-address" id="wait_menu">
							<select name="receipt_id" id="receipt_id" onchange="receipt_info(this.value);">
								<option value="">กรุณาเลือกจากรายการที่ปรากฎ</option>
							</select>
							<p style="display: block;line-height: 20px;margin: 15px 5px 0px 0px;float: left;">สาขาที่ <span class="red">*</span></p>
							<div style="display:inline-block;float:left;">
								
							<input aria-required="true" name="branch" class="required" maxlength="50"  id="branch" type="text" style="width:150px;" />
							</div>
						</div>
					</div> -->

				</div>

				<div class="se3-colum-textform">
					<div class="se3-from-area">	<div class="se3-from-radio"><p><input type="radio"  class="required" name="receipttype_id" id="receipttype_id" value="5" onclick="receipt_other(); receipttype_text_required('yes');"/>อื่น ๆ (โปรดระบุ)</p> </div></div>
					<div class="se3-from-textother">
						<input type="text" onkeypress="enterToSubmit();" maxlength="50" name="receipttype_text" id="receipttype_text" >
					</div>	
					<div>
						<div class="se3-from-address" id="wait_menu">
							<select name="receipt_id" id="receipt_id" onchange="receipt_info(this.value);">
								<option value="">กรุณาเลือกจากรายการที่ปรากฎ</option>
							</select>
						</div>
						<div class="se3-from-address" >
							<p style="display: block;line-height: 20px; margin: 15px 5px 0px 0px; float: left; font-size:130%;">
								<font color="#0066CC"> สาขาที่ </font><span class="red">*</span>
							</p>
						</div>
						<!-- <div><p style="display: block;line-height: 20px;margin: 10px 5px 0px 0px;float: left;">สาขาที่ <span class="red">*</span></p></div> -->
						<div class="se3-from-textother" style="display:inline-block;float:left;">
							<input aria-required="true" name="branch" class="required" maxlength="50"  id="branch" type="text" style="width:100px;" />
						</div>
					</div>
				</div>

                </div>
                
				<div class="clr"></div>
                				
				<div id="receipt_normal">                                
				<div class="se3-colum-title"><p>ออกใบเสร็จในนาม <span class="red">*</span></p></div>
				<div class="se3-colum-text">
					<div class="se3-from-title"><select name="receipt_title" id="receipt_title" class="regis_select" style="display:inline-block;float:left; height:32px;margin:7px 0px 7px 20px; width:90px;">
								<option value="นาย">นาย</option>
								<option value="นาง">นาง</option>
                            	<option value="นางสาว">นางสาว</option>
								<option value="อื่นๆ">อื่นๆ</option>
					  		</select>    
                    		<input type="text" name="receipt_title_text" maxlength="50" tabindex="2" 
                    			onkeypress="enterToSubmit();" id="receipt_title_text" 
                    			class="receipgroup othern regis_receipt_title_other" hidden="true"  
                    			style="width:150px;">
                       </div>
					<p style="display: block;line-height: 20px;margin: 15px 5px 0px 0px;float: left;">ชื่อ <span style="color:red">*</span> </p>
						<div class="se3-from-name" style="display:inline-block;float:left;">
							<input type="text" class="required th_fonts" name="receipt_fname" maxlength="50"  
								onkeypress="Enter_to_submit();" id="receipt_fname" value="">
						</div>
					<p style="display: block;line-height: 20px;margin: 15px 5px 0px 0px;float: left;">นามสกุล<span style="color:red">*</span> </p>
						<div class="se3-from-name" style="display:inline-block;">
							<input type="text" class="required th_fonts" name="receipt_lname" 
								onkeypress="Enter_to_submit();" maxlength="50" id="receipt_lname" value="">
						</div>
				</div>
                </div>
                
                <div class="se3-colum-title"><p>เลขที่ประจำตัวผู้เสียภาษี <span class="red">*</span></p></div>
				<div class="se3-colum-text"><div class="se3-from-address">
				<input type="text" class="required number taxno"  name="taxno" id="taxno" maxlength="13"></div></div>
                
				<!--<div class="se3-line"></div>
				<div class="se3-title"><p>ที่อยู่ (ที่จะแสดงบนใบเสร็จรับเงิน)</p></div>-->
				<div class="se3-colum-title"><p>บ้านเลขที่ <span class="red">*</span></p></div>
				<div class="se3-colum-text"><div class="se3-from-address">
				<input id="receipt_no"  class="required" name="receipt_no" onkeypress="Enter_to_submit();" type="text" value="<?php echo $slip_num_home; ?>"></div></div>
				<div class="se3-colum-title"><p>หมู่บ้าน / คอนโด / อาคาร</p></div>
				<div class="se3-colum-text"><div class="se3-from-address">
				<input type="text" id="receipt_gno"   onkeypress="Enter_to_submit();" name="receipt_gno" value="<?php echo $slip_mooban_home; ?>"></div></div>
				<div class="se3-colum-title"><p>หมู่ที่ </p></div>
				<div class="se3-colum-text"><div class="se3-from-address">
				<input id="receipt_moo"  type="text" onkeypress="enterToSubmit();" name="receipt_moo" ></div></div>
				<div class="se3-colum-title"><p>ซอย </p></div>
				<div class="se3-colum-text"><div class="se3-from-address">
				<input id="receipt_soi"  type="text" onkeypress="enterToSubmit();" name="receipt_soi" ></div></div>
				<div class="se3-colum-title"><p>ถนน</p></div>
				<div class="se3-colum-text"><div class="se3-from-address">
				<input type="text" name="receipt_road" onkeypress="Enter_to_submit();"   id="receipt_road" value=""></div></div>
				<div class="se3-colum-title"><p>จังหวัด <span class="red">*</span></p></div>
				<div class="se3-colum-text"><div class="se3-from-address">
				<select name="receipt_province_id" tabindex="16" id="receipt_province_id" class="required" onchange="getDropDown('#receipt_amphur_id', this.value, 'amphur', 'data/province.php')">
						<option value="">---- เลือกจังหวัด ----</option>
						<?php foreach ($province as $key => $value) {
							$id = $value['province_id'];
							$name = $value['name'];
							echo  "<option value='$id'>$name</option>";
						} ?>
					</select>
				</div></div>
				<div class="se3-colum-title"><p>อำเภอ / เขต  <span class="red">*</span></p></div>
				<div class="se3-colum-text"><div class="se3-from-address">
					<select name="receipt_amphur_id" tabindex="17" id="receipt_amphur_id" class="required" onchange="getDropDown('#receipt_district_id', this.value, 'district', 'data/province.php')">
						<option value="">---- เลือกอำเภอ ----</option>								
					</select>
				</div></div>
				<div class="se3-colum-title"><p>ตำบล / แขวง <span class="red">*</span></p></div>
				<div class="se3-colum-text"><div class="se3-from-address">
					<select name="receipt_district_id" tabindex="18" id="receipt_district_id" class="required">
						<option value="">---- เลือกตำบล ----</option>								
					</select>
				</div></div>
				<div class="se3-colum-title"><p>รหัสไปรษณีย์ <span class="red">*</span></p></div>
				<div class="se3-colum-text"><div class="se3-from-address">
				<input type="text" name="receipt_postcode" class="required" maxlength="5" onkeypress="enterToSubmit();" title="กรอกเฉพาะตัวเลขให้ครบ 5 หลัก"  pattern="\d{5}" id="receipt_postcode"></div></div>
				<input name="sec_id" type="hidden" value="<?php echo $sec_id; ?>" id="sec_id" />
				<input name="sec_action" type="hidden" value="<?php echo $sec_action; ?>" id="sec_action" />
				<input name="my_select_course" type="hidden" value="<?php echo $my_select_course; ?>" id="my_select_course" />
				<input name="my_select_course_detail" type="hidden" value="<?php echo $my_select_course_detail; ?>" id="my_select_course_detail" />
				<input name="ch_submit_section" type="hidden" value="yes" id="ch_submit_section" />
				<input name="ch_register_course" type="hidden" value="yes" id="ch_register_course" />
				</form>
				<div class="se3-line"></div>
				<div class="se3-fix">
					<div class="se3-fixtext"><input type="checkbox" name="this_true_data" id="this_true_data" value="yes">
						<!-- <p>ข้าพเจ้าได้ตรวจ<?php echo $name_type; ?>ข้อมูล และยอมรับว่าข้อมูลที่แสดงถูกต้องทุกประการ</p> -->
						<p>ข้าพเจ้าได้ตรวจสอบข้อมูล และยอมรับว่าข้อมูลที่แสดงถูกต้องทุกประการ</p>
					</div>
				</div>
				<div class="se-tdbottom"></div> 
			<div class="se-bottonarea2" style="margin-top:38px;">
				<a href="#" onClick="javascript: window.open('index.php?p=select_s1','_self');"><div class="se-botton"><center><p>กลับสู่หน้าหลัก</p></center></div></a>
				<div class="se-bottonarearight">	
				<a href="#" onClick="goBack();"><div class="se-botton"><center><p>ย้อนกลับ</p></center></div></a>
				<a href="#" onClick="my_f_submit();"><div class="se-botton-next"><center><p>ขั้นตอนต่อไป</p></center></div></a><!--onClick="javascript: window.open('index.php?p=new_s4','_self');" -->
				</div>
			</div>
			<div style="clear:both"></div>
		</div>
</div>
<form id='formBack' name='formBack' method='post' action='' enctype='application/x-www-form-urlencoded'>
<input name="sec_id" type="hidden" value="<?php echo $sec_id; ?>" id="sec_id" />
<input name="sec_action" type="hidden" value="<?php echo $sec_action; ?>" id="sec_action" />
<input name="my_select_course" type="hidden" value="<?php echo $my_select_course; ?>" id="my_select_course" />
<input name="ch_submit_section" type="hidden" value="yes" id="ch_submit_section" />
</form>
<style>
	input#receipt_title.error,
	input#receipt_fname.error,
	input#receipt_lname.error,
	input.error{
		margin: 7px 5px 0 20px;
	}

	#receipt_title-error,
	#receipt_fname-error,
	#receipt_lname-error{
		display: none !important;
	}
</style>
<script type="text/javascript">
$(document).ready(function($) {
	var sit_full = "<?php echo $check_sit_full; ?>";
	if(sit_full=="T"){
		msgError("รอบสอบเต็ม");
		setTimeout(function(){ goBack() }, 3000);
	}
	$("#form1").validate();
	receipttype_text_required("no");
	var id = "<?php echo $member_id; ?>";
	if(id) getInfo(id);
	$("#receipt_title").change(function(){
		if($(this).val() == 'นาย'){ 
			$("#receipt_title_text").hide();
			if($("#receipt_title_text").hasClass('required'))  $("#receipt_title_text").removeClass('required');
		}
		else if($(this).val() == 'นาง'){ 
			$("#receipt_title_text").hide();
			if($("#receipt_title_text").hasClass('required'))  $("#receipt_title_text").removeClass('required');
		}
		else if($(this).val() == 'นางสาว'){ 
			$("#receipt_title_text").hide();
			if($("#receipt_title_text").hasClass('required'))  $("#receipt_title_text").removeClass('required');
		}
		else if($(this).val() == 'อื่นๆ'){ 
			$("#receipt_title_text").show();
			$("#receipt_title_text").addClass('required');
		}
	});
});
function getInfo(id){
	if(typeof id=="undefined") return;
	var url = "data/member-info.php";
	var param = "id="+id;
	$.ajax({
		"dataType":'json', 
		"type": "POST",
		"async": false, 
		"url": url,
		"data": param, 
		"success": function(data){	
			$.each(data, function(index, array){
				array.sec_id = "<?php echo $sec_id; ?>";			 				 	
				array.c_title = "<?php echo $db->escape($title); ?>";			 				 	
				array.time = "<?php echo $time; ?>";			 				 	
				array.day = "<?php echo $day; ?>";			 				 	
				array.date = "<?php echo $date; ?>";			 				 	
				array.address_detail = "<?php echo $db->escape($address_detail); ?>";			 				 	
				array.sec_action = "<?php echo $sec_action; ?>";			 				 	
				array.my_select_course = "<?php echo $my_select_course; ?>";			 				 	
				array.my_select_course_detail = "<?php echo $my_select_course_detail; ?>";			 				 	
				array.ch_submit_section = "yes";			 				 	
				array.ch_register_course = "yes";			 				 	
				setVal("#form1", array);
				
				var t = array.slip_type;
				if(typeof t!="undefined"){					
					select_normal_niti_value(t);
					$("#"+t).trigger('click');
				}
				
				var select = $("#receipt_amphur_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.receipt_amphur_id+'"> '+array.receipt_amphur_name+' </option>');	
				var select = $("#receipt_district_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.receipt_district_id+'"> '+array.receipt_district_name+' </option>');				
				var select = $("#receipt_id");
				$('option', select).remove();
				if(array.receipttype_id==5){
					receipttype_text_required('yes');
				}
				$(select).append('<option value="'+array.receipt_id+'"> '+array.receipt_name+' </option>');
				if($("#receipt_title").val() == 'อื่นๆ'){ 
					$("#receipt_title_text").show();
					$("#receipt_title_text").addClass('required');
				}
			});				 
		}
	});
}

function receipt_info(id){
	if(typeof id=="undefined") return;
	var url = "data/receipt.php";
	var param = "name=receipt_detail&value="+id;
	$.ajax( {
		"dataType":'json', 
		"type": "GET",
		"async": false, 
		"url": url,
		"data": param, 
		"success": function(data){
			console.log(data);	
			$.each(data, function(index, array){			 				 	
				$("#taxno").val(array.taxno);
				$("#branch").val(array.branch);
				$("#receipt_no").val(array.no);
				$("#receipt_gno").val(array.gno);
				$("#receipt_moo").val(array.moo);
				$("#receipt_soi").val(array.soi);
				$("#receipt_road").val(array.road);
				$("#receipt_postcode").val(array.postcode);
				$("select[name=receipt_province_id] option").filter(function() {
					return $(this).val() == array.province_id; 
				}).attr('selected', true);		
				var select = $("#receipt_amphur_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.amphur_id+'"> '+array.amphur_name+' </option>');	
				var select = $("#receipt_district_id");
				$('option', select).remove();
				$(select).append('<option value="'+array.district_id+'"> '+array.district_name+' </option>');					
			});				 
		}
	});
}

function receipt_other(){
	var select = $("#receipt_id");
	var options = select.attr('options');
	$('option', select).remove();
	$(select).append('<option value=""> - เลือก - </option>');
}

function my_f_submit(){
	if( $('#this_true_data').is(':checked') ){
		var sec_id = '<?php echo $sec_id; ?>';
		var sec_action = '<?php echo $sec_action; ?>';
		/*if(sec_id == 1 && sec_action == 'sorb')
		{
			check_block_list();
		}
		else
		{*/
			$('#form1').attr('action', 'index.php?p=new-step-four');
			$('#form1').attr('target', '_self');
			$("#form1").submit();
		//}
	} else {
		msgError("คุณยังไม่ได้ยืนยันข้อมูลถูกต้อง");
	}
}

function goBackStepOne(){
	$('#formBack').attr('action', 'index.php?p=new-step-one');
	$('#formBack').attr('target', '_self');
	$('#formBack').submit();
}

function goBack(){
	$('#formBack').attr('action', 'index.php?p=new-step-two');
	$('#formBack').attr('target', '_self');
	$('#formBack').submit();
}

$( window ).load(function() {
/*	if($('#receipttype_id').is(':checked')){ change_slip_for_menu('#bor_lor_select'); }
	if($('#receipttype_id').is(':checked')){ change_slip_for_menu('#bank_select'); }
	if($('#receipttype_id').is(':checked')){ change_slip_for_menu('#bor_lor_jor_select'); }
	if($('#receipttype_id').is(':checked')){ change_slip_for_menu('#insurance_select'); }
	if($('#receipttype_id').is(':checked')){ $("#wait_menu").hide(); }*/
});

function select_normal_niti(select_in){
	if(select_in == "normal"){			
		//$("#divNiti").hide();	
		//$("#receipt_normal").show();
		select_normal_niti_value('individuals');
		var id = "<?php echo $member_id; ?>";
		var url = "data/member-info.php";
		var param = "id="+id;
		$.ajax( {
			"dataType":'json', 
			"type": "POST",
			"async": false, 
			"url": url,
			"data": param, 
			"success": function(data){
				console.log(data);	
				$.each(data, function(index, array){			 				 	
					$("#taxno").val(array.taxno);
					$("#branch").val(array.branch);
					$("#receipt_no").val(array.no);
					$("#receipt_gno").val(array.gno);
					$("#receipt_moo").val(array.moo);
					$("#receipt_soi").val(array.soi);
					$("#receipt_road").val(array.road);
					$("#receipt_postcode").val(array.postcode);
					$("select[name=receipt_province_id] option").filter(function() {
						return $(this).val() == array.province_id; 
					}).attr('selected', true);		
					var select = $("#receipt_amphur_id");
					$('option', select).remove();
					$(select).append('<option value="'+array.amphur_id+'"> '+array.district_name+' </option>');	
					var select = $("#receipt_district_id");
					$('option', select).remove();
					$(select).append('<option value="'+array.district_id+'"> '+array.district_name+' </option>');					
				});				 
			}
		});
	}else if(select_in == "niti"){
		select_normal_niti_value('corparation');	
		//$("#divNiti").show();	
		//$("#receipt_normal").hide();	
		//$("#wait_menu").hide(); 
	}
}

function select_normal_niti_value(value)
{
	if(value == "individuals"){			
		$("#divNiti").hide();	
		$("#receipt_normal").show();
		$("#receipt_title").addClass('required');	
		$("#receipt_fname").addClass('required');	
		$("#receipt_lname").addClass('required');	
		
	}else if(value == "corparation"){
		$("#divNiti").show();	
		$("#receipt_normal").hide();
		$("#receipt_title").removeClass('required');	
		$("#receipt_fname").removeClass('required');	
		$("#receipt_lname").removeClass('required');	
	}
}

function check_block_list()
{

	var cid = '<?php echo $member_info["username"];?>';
	var ex_date = '<?php echo $date; ?>'.split("-");
	var exd = ex_date[0].replace(/^0+/, '');
	var exm = ex_date[1].replace(/^0+/, '');
	var exy = ex_date[2] - 543;

	var url = "data/member-block.php";
	var param = "cid='"+cid+"'&exd="+exd+"&exm="+exm+"&exy="+exy;
	$.ajax( {
		"dataType":'json', 
		"type": "POST",
		"async": false, 
		"url": url,
		"data": param, 
		"success": function(data){
			if(data.result == '1000') // connect ได้ ข้อมูลไม่ถูกต้อง (จานวนข้อมูล)
			{
				msgError("ข้อมูลไม่ถูกต้อง (จานวนข้อมูล)");
			}	 
			else if (data.result == '1001')  // connect ได้ ข้อมูลไม่ถูกต้อง (เลขที่บัตร)
			{
				msgError("ข้อมูลไม่ถูกต้อง (เลขที่บัตร)");
			}
			else if (data.result == '1002')  // connect ได้ ข้อมูลไม่ถูกต้อง (วันที่)
			{
				msgError("ข้อมูลไม่ถูกต้อง (วันที่)");
			}
			else if (data.result == '2000')  // invalid user or password
			{
				msgError("invalid user or password");
			}
			else if (data.result == '3000')  // ไม่อยู่ในรายการห้ามสอบ
			{
				$('#form1').attr('action', 'index.php?p=new-step-four');
				$('#form1').attr('target', '_self');
				$("#form1").submit();
			}
			else if (data.result == '4000')  // อยู่ในรายการห้าม สอบถึงวันที่ YYYY-MM-DD
			{
				if(data.date!="") data.date = "ถึงวันที่ "+data.date;
				msgError("อยู่ในรายการห้าม<?php echo $name_type; ?>  " + data.date);
			}
			else
			{
				msgError("Unknown Error");
			}
		}
	});
}


function receipttype_text_required(value)
{
	if(value == 'yes')
	{
		$("#receipttype_text").addClass('required');
		$("#receipttype_text").removeAttr('disabled');
		$("#receipt_id").removeClass('required')
		$("#wait_menu").hide(); 
	}
	else
	{
		$("#receipttype_text").val("");
		$("#receipttype_text").removeAttr('class');
		$("#receipttype_text-error").hide();
		$("#receipttype_text").attr('disabled','disabled');
		$("#receipt_id").addClass('required');
		$("#wait_menu").show(); 
	}
}
</script>

<style>
	select{
font-family: "circularregular";
font-size: 18px;
color: #0080C3;
text-indent: 6px;		
	}
	#receipt_title_text-error{
		display: none !important;
	}
	#receipttype_text-error{
  display: none;
  position: absolute;
  bottom:0px;
}

</style>