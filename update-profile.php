<?php
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";
include_once "./share/member.php";
require './PHPMailer-master/PHPMailerAutoload.php';

/*echo "<pre>";
print_r($_POST);
echo "</pre>";
die();*/


if($_POST){
	$id = $_POST["member_id"];
	if(!$id && $_POST["mode"] == "register"){
		$cid = $_POST["cid"];
		$q = "select count(member_id) from member where cid='$cid'";
		$r = $db->data($q);
		if($r>0){
			$_SESSION["forgot"]["error"] = " เลขบัตรประชาชน $cid ซ้ำ <br> กรุณาติดต่อผู้ดูแลระบบ";	
			redirect_url();	
		}
	}
	$db->begin();
    $args = array();
	$args = array('table' => 'member',
			  'username' => $_POST["cid"],
			  'password' => $_POST["password"],
			  'title_th' => $_POST["title_th"],
			  'title_th_text' => $_POST["title_th_text"],
			  'fname_th'  => $_POST["fname_th"],
			  'lname_th'  => $_POST["lname_th"],
			  'title_en' => $_POST["title_en"],
			  'title_en_text' => $_POST["title_en_text"],
			  'fname_en'  => $_POST["fname_en"],
			  'lname_en'  => $_POST["lname_en"],
			  'email1'  => $_POST["email1"],
			  'email2'  => $_POST["email2"],
			  'slip_type'  => $_POST["slip_type"],
			  'slip_name' => $_POST["slip_name"],
			  'slip_address1'  => $_POST["slip_address1"],
			  'slip_address2'  => $_POST["slip_address2"],
			  'get_news1'  => $_POST["get_news1"],
			  'cid' => $_POST["cid"],
			  'reg_date' => date("Y-m-d H:i:s"),
			  'gender' => $_POST["gender"],
			  'birthdate' => thai_to_timestamp($_POST["birthdate"]),
			  'no'  => $_POST["no"],
			  'gno'  => $_POST["gno"],
			  'moo'  => $_POST["moo"],
			  'soi'  => $_POST["soi"],
			  'road'  => $_POST["road"],
			  'district_id' => $_POST["district_id"],
			  'amphur_id'  => $_POST["amphur_id"],
			  'province_id'   => $_POST["province_id"],
			  'postcode'   => $_POST["postcode"],
			  'tel_home' => $_POST["tel_home"],
			  'tel_mobile' => $_POST["tel_mobile"],
			  'tel_office' => $_POST["tel_office"],
			  'tel_office_ext' => $_POST["tel_office_ext"],
			  'tel_other' => $_POST["tel_other"],
			  'receipt_id'   => (int)$_POST["receipt_id"],
			  'receipttype_id'   => (int)$_POST["receipttype_id"],
			  'receipttype_text' => $_POST["receipttype_text"],
			  'receipt_title' => $_POST["receipt_title"],
			  'receipt_title_text' => $_POST["receipt_title_text"],
			  'receipt_fname'  => $_POST["receipt_fname"],
			  'receipt_lname'  => $_POST["receipt_lname"],
			  'receipt_no'  => $_POST["receipt_no"],
			  'receipt_gno'  => $_POST["receipt_gno"],
			  'receipt_moo'  => $_POST["receipt_moo"],
			  'receipt_soi'  => $_POST["receipt_soi"],
			  'receipt_road'  => $_POST["receipt_road"],
			  'receipt_district_id'   => (int) $_POST["receipt_district_id"],
			  'receipt_amphur_id'   => (int) $_POST["receipt_amphur_id"],
			  'receipt_province_id'   => (int) $_POST["receipt_province_id"],
			  'receipt_postcode'   => $_POST["receipt_postcode"],
			  'receipt_tel_home' => $_POST["receipt_tel_home"],
			  'receipt_tel_mobile' => $_POST["receipt_tel_mobile"],
			  'receipt_tel_office' => $_POST["receipt_tel_office"],
			  'receipt_tel_other' => $_POST["receipt_tel_other"],
			  'org_type'  => $_POST["orgtype_id"],
			  'org_name'  => $_POST["org_name"],
			  'org_lv'  => $_POST["orgtype_text"],
			  'org_position'  => $_POST["org_position"],
			  'grd_lv1'  => $_POST["grd_lv1"],
			  'grd_yr1'   => $_POST["grd_yr1"],
			  'grd_ugrp1'  => $_POST["grd_ugrp1"],
			  'grd_uid1'   => $_POST["grd_uid1"],
			  'grd_uname1'  => $_POST["grd_uname1"],
			  'grd_major1'  => $_POST["grd_major1"],
			  'rectime'  => date("Y-m-d H:i:s"),
			  'branch'  => $_POST["branch"],
			  'taxno'   => $_POST["taxno"],
			  'grd_other' => $_POST["grd_other"],
			  'get_news1' => $_POST["get_news1"],
			  'nation' => 'T',
			  'activate_code' => date("Y-m-d H:i:s")

	);
	if($id){
		$args['id'] = array('key'=>'member_id', 'value'=>$id);
	}

	/*echo "<pre>";
	print_r($args);
	echo "</pre>";
	die();*/

		// At last.. a query!
	$ret = $db->set($args);
	$ret = ($id) ? $id : $ret; 
	$db->commit();
	if($_POST["mode"] == "register")
	{
		$aLogin = view_member("", $ret);
		
		$_SESSION["login"]["info"] = $aLogin[0];
		$_SESSION["login"]["isLogin"] = true;

		$to_email = $_POST["email1"];
		/*$subject = "=?UTF-8?B?".base64_encode("ATI - สมัครสมาชิกเว็บ ATI เรียบร้อยแล้ว")."?=";*/
		$subject = "=?UTF-8?B?".base64_encode("สมัครสมาชิกระบบลงทะเบียนเรียบร้อยแล้ว")."?=";
		/*$message = "เรียน คุณ ".$_POST["title_th"]." ".$_POST["fname_th"]." ".$_POST["lname_th"]."<br/> ";*/
		$message = "เรียน คุณ".$_POST["fname_th"]." ".$_POST["lname_th"]."<br/> ";
		$html = '
		<table class="deviceWidth" align="left" border="0" cellpadding="0" cellspacing="0" width="800"> 
			<tbody>
				<tr>
					<td class="center" style="font-size: 12px; color: #687074; font-weight: bold; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 10px 0px; " bgcolor="#ffffff">
						'.$message.'
						การสมัครสมาชิกของคุณเรียบร้อยแล้ว 
					</td>

				</tr>
				<tr>
					<td class="center" style="font-size: 12px; color: #687074; font-weight: bold; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 10px 0px 0px; " bgcolor="#ffffff">
						ยินดีต้อนรับเข้าสู่ระบบรับลงทะเบียน						
					</td>
				</tr>
				<tr>
					<td class="center" style="font-size: 12px; color: #687074; font-weight: bold; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 10px 0px 0px; " bgcolor="#ffffff">
						ท่านสามารถใช้ Username และ Password ตามรายละเอียดด้านล่างเพื่อเข้าสู่ระบบสมาชิกได้ทันที						
					</td>
				</tr>
				<tr>
					<td class="center" style="font-size: 12px; color: #687074; font-weight: bold; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 10px 0px 0px; " bgcolor="#ffffff">
						Username : '.$_POST["cid"].'
					</td>
				</tr>
				<tr>
					<td class="center" style="font-size: 12px; color: #687074; font-weight: bold; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 10px 0px 0px; " bgcolor="#ffffff">
						Password : '.$_POST["password"].'
					</td>
				</tr>
				<tr>
					<td class="center" style="font-size: 12px; color: #687074; font-weight: bold; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 10px 0px 0px; " bgcolor="#ffffff">
						ท่านสามารถเข้าสู่ระบบ โดยใช้ข้อมูลข้างต้นโดย <a style="text-decoration: none; color: #27d7e7;" href="https://register.ati-asco.org/index.php">คลิกที่นี่</a> 
					</td>
				</tr>
				<tr>
					<td class="center" style="font-size: 12px; color: #687074; font-weight: bold; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 10px 0px; " bgcolor="#ffffff">
						สถาบันฝึกอบรม สมาคมบริษัทหลักทรัพย์ (ATI)<br>
						ส่วนฝึกอบรมติดต่อ: Training@ati-asco.org โทร 02-264-0909 ต่อ 223<br>
						ส่วนทดสอบผู้แนะนำการลงทุนฯติดต่อ: Examination@ati-asco.org โทร 02-264-0909 ต่อ 203-207<br>
						ชมรมวาณิชธนกิจ (IB Club) ส่วนฝึกอบรมและทดสอบติดต่อ: fatraining@asco.or.th โทร 02-264-0909 ต่อ 124,125<br>
					</td>
				</tr>
			</tbody>
		</table>
		';	
		sendmail($to_email, $subject, $html);
		$args = array();
		$args["p"] = "main";
		$_SESSION["popup_one"]["msg"] = "ยินดีต้อนรับสู่การเป็นสมาชิก";	
		redirect_url($args);
		die();
	}
}

$args = array();
$args["member_id"] = $ret;
$args["update_state"] = "T";
redirect_url($args);

?>
