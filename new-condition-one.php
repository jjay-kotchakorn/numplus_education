<?php
include_once "./share/authen.php";
include_once "./lib/lib.php";
include_once "./share/course.php";

global $db;

// d($_SESSION);

$member_id = $_SESSION["login"]["info"]["member_id"];
$q = "SELECT activate_code, email1, email2 FROM member WHERE member_id={$member_id}";
$info = $db->rows($q);
$activate = $info["activate_code"];
$email1 = trim($info["email1"]);
$email2 = trim($info["email2"]);

$q = "SELECT email_blacklist_id 
	FROM email_blacklist 
	WHERE active='T' 
		AND (name='{$email1}' OR name='{$email2}')
";
$email_blacklist_id = $db->data($q);
if( empty($activate) || !empty($email_blacklist_id) ){
echo <<<holy
<script>
window.open('index.php?p=profile','_self');
</script>
holy;
}//end if

if(!isset($_POST['ch_submit_section'])){
echo <<<holyy
<script>
window.open('index.php','_self');
</script>
holyy;
}

if($_POST['ch_submit_section'] != "yes"){
echo <<<holy
<script>
window.open('index.php?p=select_s1','_self');
</script>
holy;
}

$sec_id = $_POST['select_id'];
$sec_action = $_POST['select_action'];
if(!$sec_id && $_POST["sec_id"]){
	$sec_id = $_POST["sec_id"];
	$sec_action =  $_POST['sec_action'];
}
$type_id = map_course_type($sec_action);
$q = "select title, detail, news, ati_news from section where section_id={$sec_id}";
$section_info = $db->rows($q);

if ($sec_id == 1 || $sec_id == 2 || $sec_id == 3) {
	$title_news = "ประกาศและข่าวสารประชาสัมพันธ์จาก ATI";
}else{
	$title_news = "ประกาศและข่าวประชาสัมพันธ์จากชมรมวาณิชธนกิจ";
}

?>
<div id="dialog" title="ยอมรับเงื่อนไข" class="dialog" > 
</div>

<div class="contrainer1">
	<div class="mainsite1">
		<div class="tag_page">
			
			</div>
			<div class="condition-zone">
				<div class="condition-detail">
					<h2>
						<?php echo $section_info["title"]; ?>
					</h2>
					<div class="clr"></div>
					<div class="section-detail">
						<?php echo $section_info["detail"]; ?>
					</div>
				</div>
				<div class="clr"></div>
				<?php if($section_info["news"]=="T"  && $type_id==2){ ?>
				<div class="table1">
                	<div class="table-top">
						<p>ประกาศและข่าวประชาสัมพันธ์</p>
					</div>
					<div id="news-feed">
                    
                    </div>
				<div class="clr"></div>
				<div class="break" style="margin-top:30px;"></div>
				<?php } ?>
				<?php if($section_info["ati_news"]=="T"){ ?>
				<div class="table1">
					<div class="table-top">
						<p><?php echo $title_news;?></p>
					</div>
					<div id="conent-news">
						
					</div>
				</div>
				<?php } ?>
				<div class="condition-bottom">
					<h2>การยอมรับเงื่อนไข</h2>
					<p><input type="checkbox" name="vehicle" value="Car" id="vehicle">
					ข้าพเจ้ารับทราบและเข้าใจเงื่อนไขการสมัคร รวมถึงประกาศและข่าวประชาสัมพันธ์</p>
					<!-- <a href="#" onClick="javascript: window.open('./pdf/rule_exam.pdf','_blank');">อ่านเงื่อนไขการใช้งานระบบ</a> -->
				<?php 
					switch ($sec_id) {
					 	case 1:
					 		$url_link = "<a href=\"#\" onClick=\"javascript: window.open('./pdf/condition_ic.pdf','_blank');\">อ่านเงื่อนไขการใช้งานระบบ</a>";
					 		break;
					 	case 2:
					 		$url_link = "<a href=\"#\" onClick=\"javascript: window.open('./pdf/condition_iba.pdf','_blank');\">อ่านเงื่อนไขการใช้งานระบบ</a>";
					 		break;					 		
				 		case 3:
				 			$url_link = "<a href=\"#\" onClick=\"javascript: window.open('./pdf/condition_fa.pdf','_blank');\">อ่านเงื่อนไขการใช้งานระบบ</a>";
					 		break;
					 	default:
					 		# code...
					 		break;
					 } 									
					//echo $url_link;
				?>
				</div>
				<div class="clr"></div>
				<div class="conditon-submit">
					<a href="#" onClick="javascript: window.open('index.php?p=main','_self');"><div class="cancel-bottom">
						<p>ยกเลิก</p>
					</div></a>
					<a href="#" onClick="my_f_submit();"><!--onClick="javascript: window.open('index.php?p=new_s1','_self');" --><div class="cancel-submit">
						<p>ขั้นตอนต่อไป</p>
					</div></a>
				</div>
			</div>
		</div>
	</div>
<form id='form1' name='form1' method='post' action='' enctype='application/x-www-form-urlencoded'>
<input name="sec_id" type="hidden" value="<?php echo $sec_id; ?>" id="sec_id" />
<input name="sec_action" type="hidden" value="<?php echo $sec_action; ?>" id="sec_action" />
<input name="ch_submit_section" type="hidden" value="yes" id="ch_submit_section" />
</form>
<script type="text/javascript">
function dialog(){
	$("#dialog").dialog({
	    modal: true,
	    draggable: false,
	    resizable: false,
	    closeOnEscape: true,
	    show: 'blind',
	    hide: 'blind',
	    width: 300,
	    dialogClass: 'showdialog',
        height: $(window).height()-120,
	    buttons: {
	        "ตกลง": function() {
	            $(this).dialog("close");
	        }
	    }
	});
}

function my_f_submit(){
	$('#form1').attr('action', 'index.php?p=new-step-one');
	$('#form1').attr('target', '_self');
	if( $('#vehicle').is(':checked') ){
		$('#form1').submit();
	} else {
		$("#dialog").html("คุณยังไม่ได้ยอมรับเงื่อนไข");
		dialog();
	}
}
function news_info(){
	var url = "data/news-info.php";
	var param = "section_id=<?php echo $sec_id; ?>";
		$.ajax( {
			"type": "POST",
			"async": false, 
			"url": url,
			"data": param, 
			"success": function(data){
				$("#conent-news").html(data);				 
			}
		});
}
function news_feed_info(){
		var url = "data/xml-news.php";
	var param = "";
		$.ajax( {
			"type": "POST",
			"async": false, 
			"url": url,
			"data": param, 
			"success": function(data){
				$("#news-feed").html(data);				 
			}
		});
}
$(document).ready(function() {
	
	var news_feed = "<?php echo $section_info["news"] ?>";
	if(news_feed == "T") news_feed_info();

	var ck = "<?php echo $section_info['ati_news']; ?>";
	if(ck=="T")	news_info();

});

</script>