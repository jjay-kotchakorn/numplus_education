<?php
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";
include_once "./share/member.php";

$_SESSION["change-password"]["msg"] = NULL;

$member_info = $_SESSION["login"]["info"];
$member_id = $member_info['member_id'];
$old_pass = $db->escape(trim($_POST["old_pass"]));
$new_pass = $db->escape(trim($_POST["new_pass"]));

if(isset($member_id)){
	$q = "SELECT a.member_id FROM member  a WHERE a.member_id='{$member_id}' AND a.password='{$old_pass}'	AND a.active = 'T'";
	$login_id = $db->data($q);
	
	if(!$login_id){
		$msg = "Password เดิม ไม่ถูกต้อง";
		$_SESSION["change-password"]["msg"] = $msg;
		redirect_url();
	}else{
		// update
		$id = $member_id;
		$db->begin();
    	$args = array();
		$args = array('table' => 'member',
			  'password' => $new_pass,
		);
		if($id){
			$args['id'] = array('key'=>'member_id', 'value'=>$id);
		}
		// At last.. a query!
		$ret = $db->set($args);
		$ret = ($id) ? $id : $ret; 
		$db->commit();
		
		$msg = "Password ของคุณถูกเปลี่ยนเรียบร้อยแล้ว";
		$_SESSION["change-password"]["msg"] = $msg;
		redirect_url();
	}
}
die("error");
?>
