<?php 
$msg = $_SESSION["forgot"]["msg"];
unset($_SESSION["forgot"]["msg"]);
$error = $_SESSION["forgot"]["error"];
unset($_SESSION["forgot"]["error"]);
 ?>
<div class="bgprofile-pass" style="height:380px;">
	<div class="bodypass" style="height:380px;">
		<div class="pass-title"><center><p>ลืมรหัสผ่าน</p></center></div>
		<form action="update-forgot-password.php" method="post" name="form_forgot" id="form_forgot">
			<div class="se3-title"><p>กรุณากรอกข้อมูล ให้ถูกต้อง</p>
			</div>
			<div class="pass-area" style="height:150px;">
				<div class="pass-colum">   			
					<div class="pass-fromtext"><p>กรุณาระบุ เลขบัตรประชาชน<span>&nbsp;*</span></p></div>
					<div class="pass-from" style="position:relative;"><input type="text" class="required cart_id" name="your_id" maxlength="32"id="your_id"></div>
					<div class="pass-from">
						<input type="hidden" name="ch_submit_forget_pass" id="ch_submit_forget_pass" value="yes">
					</div>
				</div>
			</div>
			<div class="" style="height: 30px;margin: 0 auto; display:block; text-align:center; clear:both; padding-top:40px;">
				<div class="se-botton-next" onclick="javascript: window.open('index.php?p=main','_self');" style="float:none;display:inline-block;"><center><p>กลับสู่หน้าหลัก</p></center></div>
				<a href="#" onclick="chFrmSubmit();"><div class="se-botton-next" style="float:none;display:inline-block;"><center><p>ส่งรหัสผ่าน</p></center></div></a>
			</div>
		</form>
	</div>
</div>

<div id="changepassword" class="dialog" title="ลืมรหัสผ่าน"></div>

<script language="javascript" type="text/javascript">
	$(document).ready(function(){
		$("#form_forgot").validate();
	});
	var msg = "<?php echo $msg?>";
	var error_msg = "<?php echo $error?>";
	if(msg!=""){
		var options =  $.parseJSON('{"text":"<p>'+msg+'</p>","layout":"center","type":"error"}');
		noty(options); 
	}	
	if(error_msg!=""){
		var options =  $.parseJSON('{"text":"<p>'+error_msg+'</p>","layout":"center","type":"error"}');
		noty(options); 
	}
  function chFrmSubmit(){
		$("#form_forgot").submit();    
	}

</script>
<style>
	.noty_bar.noty_theme_default.noty_layout_center.noty_error p{
		font-size: 20px; 
	}
</style>