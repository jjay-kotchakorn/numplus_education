<?php 
$msg = $_SESSION["login"]["msg"];
unset($_SESSION["login"]["msg"]);

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
<div id="mainbody">
	<div class="homecontainer">

		<div id="loginbox">
			
			<table class="browser-support" width="90%" cellpadding="10" align="center">
				<thead>
					<tr>
						<th colspan="2"><h2>สถาบันฝึกอบรม สมาคมบริษัทหลักทรัพย์ไทย</h2></th>
					</tr>
					<tr>
						<th colspan="2" style="text-align: left;">สถาบันฝึกอบรม สมาคมบริษัทหลักทรัพย์ไทย ขอแจ้งรายละเอียดเครื่องคอมพิวเตอร์ที่ผู้สมัครสามารถใช้ในการสมัครสอบ/อบรม ควรมีคุณสมบัติขั้นต่ำดังนี้</th>
					</tr>
					<tr>
						<th colspan="2" style="padding-top: 20px;"><h2>เบราว์เซอร์ขั้นต่ำที่รองรับ</h2></th>
					</tr>
				</thead>
			</table>
			<table class="browser-support" width="85%" cellpadding="10" align="center">	
				<thead style="border: 1px solid black; font-weight: bold;  background-color: #4169E1;
	    color: white;">
					<tr>
						<td class="color-boder">Browser</td>
						<td class="color-boder">Minimum Browser Version</td>
					</tr>
				</thead>
				<tbody>
					<tr class="color-boder">
						<td class="color-boder">Internet Explorer</td>
						<td class="color-boder">10</td>		
					</tr>
					<tr class="color-boder">
						<td class="color-boder">Firefox</td>
						<td class="color-boder">32.0</td>		
					</tr>
					<tr class="color-boder">
						<td class="color-boder">Chrome</td>
						<td class="color-boder">31.0</td>		
					</tr>
					<tr class="color-boder">
						<td class="color-boder">Mac Safari</td>
						<td class="color-boder">7.1.8</td>		
					</tr>
					<tr class="color-boder">
						<td class="color-boder">Windows Safari</td>
						<td class="color-boder">5.1.7</td>		
					</tr>
				</tbody>	
				
			</table>	
			
		</div>

	</div>
</div>
</body>
</html>

<style>
	#f_id_login.error{
		margin-bottom: 24px;
	}
	#f_id_login-error{
		position: absolute;
		display: block;
		color: #F00;
		font-weight: normal;
		font-family: "circularregular";
		font-size: 16px;
		white-space: nowrap;
		margin-left: -6px;
		margin-top: -23px;
		padding: 5px;
	}
	#f_password_login-error{
		position: absolute;
		display: block;
		color: #F00;
		font-weight: normal;
		font-family: "circularregular";
		font-size: 16px;
		white-space: nowrap;
		margin-left: -4px;
		margin-top: -15px;
		padding: 5px;
	}
	.browser-support{
		table-layout: fixed; 
		margin-left: 50px;
		margin-top: 15px;
		margin-bottom: 15px;
	}
	.tbody tr, td{
		text-align: center;
	    /*border: 1px solid black;*/

	}
	h2{
		text-align: center;
		font-size: 20px;
		color: #4264a1;
		margin-bottom: 10px;
	}
	.color-boder{
		/*border: 1px solid black;*/
		border-style: outset;
		border-color: #FF8000;
	}
</style>
<script language="javascript" type="text/javascript">
	$(document).ready(function(){
		//$("#form_login").validate();
	});
	var msg = "<?php echo $msg?>";
	if(msg!=""){
		msgError(msg);
		//var options =  $.parseJSON('{"text":"<p>'+msg+'</p>","layout":"center","type":"error"}');
		//noty(options); 
	}
</script>