<?php 
include_once "./share/authen.php";
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/course.php";
global $db;

$member_info = $_SESSION["login"]["info"];
$member_id = $member_info['member_id'];
$con = " and a.member_id=$member_id and a.active='T'";
$register_id = $_GET["register_id"];
$pay_type = $_GET["pay_type"];

if ( !empty($register_id) && !empty($pay_type) && $pay_type=="mpay" ) {
  $q = "SELECT xml_end_point_url 
    FROM payment_mpay_register_list
    WHERE register_id={$register_id}
  ";
  $reprint_url = $db->data($q);
  if ( !empty($reprint_url) ) {
    header( "Location: {$reprint_url}" );
  }else{
    $reprint_url = "./mpay/pay-now.php?register_id={$register_id}";
    header( "Location: {$reprint_url}" );
  }//end else
  // echo $reprint_url;
  die();
}//end if

if($register_id){
  $rs = get_register("", $register_id);
  $data = $rs[0];
  $pay_by = $data["pay"];
/*
  if ($pay_by=='at_ati' || $pay_by=='at_asco') {
    $args = array();
    $args["table"] = "register";
    $args['id'] = $register_id;
    $args["ref1"] = NULL;
    $args["ref2"] = NULL;
    $db->set($args);
  }//end if
*/

  $str = "";
  foreach ($data as $key => $v) {
    $str .= "{$key}={$v}|";
  }//end loop $v

  if ( empty($pay_by) ) {
    $args = array();
    $args["table"] = "register";
    $args['id'] = $register_id;
    $args["pay"] = "at_ati";
    $db->set($args);

    $str .= "=====>";
    foreach ($args as $key => $v) {
      $str .= "{$key}={$v}|";
    }//end loop $v
    unset($args);
  }//end if

  //write log
/*  
  $str = "register_id=".$register_id."|pay=".$pay_by."|";
  $str .= "ref1=".$args["ref1"]."|ref2=".$args["ref2"];
*/


  $date_now = date('Y-m-d H:i:s');
  $date_log = date('Y-m-d');
  $log_name = "./log/invoice-ati_".$date_log.".log";
  $file = fopen($log_name, 'a');
  $str_txt = $date_now."|".$str."\r\n";
  fwrite($file, $str_txt);
  fclose($file);

  $sCourse_id = $data["course_id"];
  $con = " and a.course_id in ($sCourse_id)";
  $r = get_course($con);
  $title = "";
  $parent_id = 0;
  $price = 0;
  foreach ($r as $key => $row) {
    $course_id = $row['course_id'];
    $section_id = $row['section_id'];
    $code = $row['code'];
    $title .= $row['title'].", ";
    $set_time = $row['set_time'];
    $life_time = $row['life_time'];
    $status = $row['status']; 
    $parent_id = (int)$row['parent_id'];
  }
  $q = "select a.course_detail_id from register_course_detail a where a.register_id={$register_id} and a.course_id in ($sCourse_id)";
  $cd = $db->get($q);

  $title = trim($title, ", ");
  $price = $data["course_price"];
  $discount = $data["course_discount"];
  if($cd){    
    $display_date = "";
    $display_address = "";
    foreach ($cd as $kk => $vv) {
      $row = get_course_detail(" and a.course_detail_id={$vv["course_detail_id"]}");
      if($row) $row = $row[0];
      $course_detail_id = $row['course_detail_id'];
      $course_id = $row['course_id'];
      $day = $row['day'];
      $date = $row['date'];
      $time = $row['time'];
      $display_date .= revert_date($date)."&nbsp;เวลา ".$time.", ";
      $display_address .= $row['address_detail']." ".$row['address'].", ";
    }

  }else{
      $row = get_course_detail(" and a.course_detail_id={$data["course_detail_id"]}");
      if($row) $row = $row[0];
      $course_detail_id = $row['course_detail_id'];
      $course_id = $row['course_id'];
      $day = $row['day'];
      $date = $row['date'];
      $time = $row['time'];
      $display_date .= revert_date($date)."&nbsp;เวลา ".$time.", ";
      $display_address .= $row['address']." ".$row['address_detail'].", ";
  }

  $display_date = trim($display_date, ", ");
  $display_address = trim($display_address, ", ");
  $slip_type_text = "";
  if($data["slip_type"]=="individuals") $slip_type_text = "ออกในนามบุคคลธรรมดา";
  if($data["slip_type"]=="corparation") $slip_type_text = "ออกในนามนิติบุคคล (สำหรับเบิกค่าใช้จ่าย)";
  $q = "select name from receipttype where receipttype_id={$data["receipttype_id"]}";
  $receipttype_name = $db->data($q);
  $q = "select name from district where district_id={$data["receipt_district_id"]}";
  $district_name = $db->data($q);
  $q = "select name from amphur where amphur_id={$data["receipt_amphur_id"]}";
  $amphur_name = $db->data($q);
  $q = "select name from province where province_id={$data["receipt_province_id"]}";
  $province_name = $db->data($q);
  $q = "select register_id from register a where a.active='T' and  a.course_id='$course_id'";
  $id = $db->data($q);
  $total_price = $price - $discount;
  $t = convert_mktime($data["rectime"]) + (2 * 24 * 60 * 60);
  $expire_date = $data["expire_date"];
}

 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ชำระเงินสดที่ ATI</title>
<link rel="stylesheet" type="text/css" media="print, handheld" href="./css/print.css" />
<link href="./css/style.css" rel="stylesheet" type="text/css" />
</head>
<style>
    @media print{
      .buttonb,
      #btnPrint{
        display: none;
      }
    }
    .buttonb1 {
      background: linear-gradient(to bottom, #6580b5 0%, #6580b5 56%, #6580b5 56%, #4264a1 56%, #4264a1 100%);
      margin-top: 20px;
      margin-bottom: 20px;
      float: left;
      border-radius: 5px;
      color: #FFF;
      padding: 10px;
      font-size: 20px;
    }
    .buttonb1:hover {
      background: linear-gradient(to bottom, #6580b5 0%, #6580b5 56%, #6580b5 56%, #254487 56%, #254487 100%);
      cursor: pointer;
    }
    body{
      padding: 0px;
    }
    .container{
      width: 1000px;
    }
    .billinfo{
      padding-top: 25px;
      padding-bottom: 10px;
    }
    .bill1 table, .bill2 table{
      font-size: 18px;
    }
</style>
<body>
<div class="container">
<div class="billinfo">
<h5>แบบฟอร์มชำระค่าธรรมเนียม</h5>
<?php if ($section_id){ ?>
      <ol>
      <li>โปรดตรวจสอบความถูกต้องของข้อมูลที่ปรากฎในแบบฟอร์มชำระค่าธรรมเนียม ก่อนนำไปชำระเงินทุกครั้ง อาทิ วันอบรม วันสอบ กำหนดชำระค่าธรรมเนียม เป็นต้น</li>
      <li>ผู้สมัครต้องดำเนินการชำระค่าธรรมเนียมภายในวันที่กำหนดในแบบฟอร์มชำระค่าธรรมเนียม </li>
      <li>สถาบันฝึกอบรมสมาคมบริษัทหลักทรัพย์ไทย ขอสงวนสิทธิ์ไม่คืนค่าธรรมเนียมให้ หลังจากรับชำระค่าธรรมเนียมแล้ว</li>
      <li>กรณีที่ผู้สมัครไม่ปฏิบัติตามระเบียบการและข้อกำหนดข้างต้น อันเป็นเหตุให้เกิดความผิดพลาดในการสมัคร  สถาบันฝึกอบรมสมาคมบริษัทหลักทรัพย์ไทย จะไม่รับผิดชอบต่อความเสียหายที่เกิดขึ้น</li>
      </ol>
<?php } 
  //switch logo for invoice
  $logo = "images/";
  if ( ($pay_by == 'bill_payment') || ($pay_by == 'at_ati') ) {
    $logo .= "logo1.png";
  }
  /*if ($pay_by == 'at_asco') {
    $logo .= "payment-bill-ati.png"; 
  }*/
  if ($pay_by == 'at_asco') {
    $logo .= "logo2.png"; 
  }

?>
</div>
<div class="cut"></div>
<div class="bill1">
<div class="header">
<table width="100%">
<tbody>
<tr>
<!-- <td width="50%"><img src="images/logo2.png" height="46px" /></td> -->
<td width="50%"><img src="<?php echo $logo;?>" height="46px" /></td>
<td align="right">ส่วนที่ 1 ส่วนของลูกค้า</td>
</tr>
</tbody>
</table>
</div>
<table width="100%" cellpadding="10">
<tbody>
<tr>
<td class="gray">วันสอบ/อบรม</td>
<td class="fix-tdline" width="50%"><?php echo $display_date; ?></td>
<td class="gray">วันที่สมัคร</td>
<td><?php echo revert_date($data["rectime"]); ?></td>
</tr>
<tr>
<td class="gray">ผู้สมัครสอบ/อบรม</td>
<td class="fix-tdline" width="50%"><?php echo $data["title"]; ?> &nbsp;<?php echo $data["fname"]; ?>&nbsp;&nbsp;<?php echo $data["lname"]; ?></td>
<td class="gray">ชำระเงินภายในวันที่</td>
<td><?php echo revert_date($expire_date); ?></td>
</tr>
<tr>
<td class="gray">สถานที่สอบ/อบรม</td>
<td class="fix-tdline" width="50%"><?php echo $display_address; ?></td>
<td class="gray">จำนวนเงินที่ต้องชำระ</td>
<td><?php echo set_comma($data["course_price"]-(int)$data["course_discount"]); ?> บาท</td>
</tr>
<tr>
<td rowspan="2" valign="top" class="gray">หลักสูตรที่สอบ/อบรม</td>
<td class="fix-tdline" width="50%" rowspan="2" valign="top"><?php echo $title; ?></td>
<td class="gray"> </td>
<td> </td>
</tr>
<tr>
<td class="gray"> </td>
<td> </td>
</tr>
</tbody>
</table>
</div>
<div class="cut"></div>
<div class="bill2">
<div class="header">
<table width="100%">
<tbody>
<tr>
<!-- <td width="50%"><img src="images/logo2.png" height="46px" /></td> -->
<td width="50%"><img src="<?php echo $logo;?>" height="46px" /></td>
<td align="right">ส่วนที่ 2 ส่วนของธนาคาร</td>
</tr>
</tbody>
</table>
</div>
<div class="choose">

    <p>
     <table cellpadding="5" class="noborder">
     <tbody>
     <tr>
         <td> การชำระค่าธรรมเนียมสามารถกระทำได้ 2 ช่องทาง คือ

  <div style="color:#23408f;">ช่องทางที่ 1 การชำระโดยชำระเงินสดที่ สมาคมบริษัทหลักทรัพย์ไทย</div>
  </td>
     </tr>
       <tr>     
         <td> 
  <div style="color:#23408f;">ช่องทางที่ 2 การชำระโดยการโอนเงินผ่านบัญชีธนาคาร</div>
  โอนเงินเข้าบัญชีออมทรัพย์ <!-- <img src="images/payment-bank-kasikorn.png" alt="" style="border:none;vertical-align: middle;" /> --> 
    ธนาคารกสิกรไทย จำกัด (มหาชน) สาขาถนนรัชดาภิเษก (สุขุมวิท-พระรามที่ 4) <br>ชื่อบัญชี “สถาบันฝึกอบรม สมาคมบริษัทหลักทรัพย์ไทย” 
เลขที่บัญชี <span style="color:#23408f;">718-2-56289-5</span> และส่งเอกสารการโอนเงินพร้อมแบบฟอร์มชำระค่าธรรมเนียมการอบรม/ทดสอบ <br>มายัง 
<span style="color:#23408f;">E-mail : training@ati-asco.org </span>หรือทาง<span style="color:#23408f;">โทรสาร 02-661-8504</span>

  </td>
     </tr>
     </tbody>
     </table>
    </p>
  
</div>
  <table width="92%" cellpadding="10" align="center" class="moneyresult">
<tbody>
<tr>
<th width="70%" style="border-right:1px solid #c9c9c9;">จำนวนเงินตัวอักษร</th>
<th>จำนวนเงินตัวเลข (บาท)</th>
</tr>
<tr>
<td style="border-right:1px solid #c9c9c9;"><?php echo number_letter($data["course_price"]-(int)$data["course_discount"]); ?></td>
<td><?php echo set_comma($data["course_price"]-(int)$data["course_discount"]); ?></td>
</tr>
</tbody>
</table>
</div>

<div class="row">
  <div class="col-md-6">
    <div id="btnHome" onclick="redirect_home()" class="buttonb1 homebutton">
      กลับหน้าแรก 
    </div>
  </div>
  <div class="col-md-6">
    <div id="btnPrint" onclick="PrintElem()" class="buttonb printbutton">
     <img src="images/print.png" /> พิมพ์ใบ Pay-in Slip
   </div>
 </div>
</div>

<script type="text/javascript">
	function PrintElem()
	{
		window.print();
	}
   function redirect_home(){
    window.open('./index.php?p=main','_self');
  }
</script>

</div>

</body>
</html>
