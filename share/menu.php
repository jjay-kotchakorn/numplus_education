<?php 
function get_menu($con="", $menu_id=""){
    if(isset($_SESSION["menuinfo"])){
        return $_SESSION["menuinfo"];
    }else{
        global $db;
        if($db){
            $q = "select a.menu_id, a.code, a.name, a.name_eng, a.active
                        ,a.recby_id, a.rectime
                  from menu a 
                  where a.active='T' $con
                  order by a.code, a.menu_id";
            $rs = $db->get($q);
            return $_SESSION["menuinfo"] = (is_array($rs)) ? $rs : "";
        }
   }
}

function get_menu_list($menu_id){
    if(isset($_SESSION["menulistinfo"][$menu_id])){
        return $_SESSION["menulistinfo"][$menu_id];
    }else{
       global $db;
       if($db){
            $q = "select a.menulist_id, a.code, a.name, a.name_eng, a.active,
                         a.recby_id, a.rectime, a.url, a.menu_id, a.short
                  ,b.name as menu_name, b.name_eng as menu_name_eng, a.useclass
                from menulist a inner join menu b on a.menu_id=b.menu_id
                where a.active='T' and a.menu_id=$menu_id
                order by a.code, a.menulist_id";
            $rs = $db->get($q);
           return $_SESSION["menulistinfo"][$menu_id] = (is_array($rs)) ? $rs : "";
       }
    }
}
?>