<?php
if (!isset($_SESSION)) {//initialize the session
  session_start();
}
header ('Content-type: text/html; charset=utf-8');
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_COMPILE_ERROR);
date_default_timezone_set('Asia/Bangkok');

$is_login = $_SESSION["login"]["isLogin"];
$is_admin_login = $_SESSION["login"]["isAdminLogin"];
$aLogin = $_SESSION["login"]["info"];
if(isset($_SESSION["emp"])){
  $_SESSION["login"]["isLogin"] = false;
  session_destroy();
  //to fully log out a visitor we need to clear the session varialbles
  header("Location:index.php?p=default");
}
$inactive = (60*60*8); 
// echo $inactive;
// check to see if $_SESSION['timeout'] is set
if(isset($_SESSION['timeout']) ) {	
	$session_life = time() - $_SESSION['timeout'];	
		if($session_life > $inactive) { 
			session_destroy(); 
			header("Location:index.php?p=default");
		}
}
$_SESSION['timeout'] = time();
?>
