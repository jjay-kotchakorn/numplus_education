<?php 
function view_news($con="", $news_id="", $order=false){
   if($con=="" && $news_id=="") return array();
   global $db;   
   $con_news_id = $news_id ? " and a.news_id=$news_id" : "";
   $con = $news_id ? "" : $con;
   $con_orders = ($order==true) ? " a.news_id " : " a.news_id desc";
    $q = "select 
            a.news_id,
            a.code,
            a.name,
            a.name_eng,
            a.detail,
            a.section_id,
            a.link_to,
            a.newstype_id,
            a.active,
            a.recby_id,
            a.rectime,
            a.remark,
            a.highlight
    from  news a 
    where a.active='T' $con $con_news_id
    order by highlight desc, $con_orders
    limit 400";

   $r = $db->get($q);   
   return $r;
}
?>
