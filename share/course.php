<?php 
function get_course($con="", $course_id="", $order=false, $limit_row=1000){
   if($con=="" && $course_id=="") return array();
   global $db;   
   $con_course_id = $course_id ? " and a.course_id=$course_id" : "";
   $con = $course_id ? "" : $con;
   // $con_orders = ($order==true) ? " a.course_id " : " a.course_id desc";
   $con_orders = ($order==true) ? " a.course_order " : " a.course_order desc";
   
   $q = "select a.course_id,
				a.section_id,
				a.code,
				a.code_project,	
				a.set_time,
				a.unit_time,	
				a.unit_time_fa,	
				a.set_time_fa,
				a.title,
				a.life_time,
				a.price,
				a.rereg_date,
				a.discount,
				a.date_other,
				a.`status`,
				a.coursetype_id,
				a.parent_id,
				a.active,
				a.recby_id,
				a.rectime,
				a.remark,
				a.detail,
				a.date_start,
				a.date_stop,
				a.inhouse,
				a.course_order,
				b.name as coursetype_name,
				c.name as section_name
		 from course a left join coursetype b on b.coursetype_id=a.coursetype_id
		 	  left join section c on c.section_id=a.section_id            
		 where a.active!='' $con $con_course_id
		 order by $con_orders
		 limit $limit_row";
		
   $r = $db->get($q);   
   return $r;
}

function get_course_detail($con="", $course_detail_id="", $order=false, $limit_row=1000){
   if($con=="" && $course_detail_id=="") return array();
   global $db;   
   $con_course_detail_id = $course_detail_id ? " and a.course_detail_id=$course_detail_id" : "";
   $con = $course_detail_id ? " and a.course_detail_id=$course_detail_id" : $con;
   //$con_orders = ($order==true) ? " a.course_detail_id " : " a.course_detail_id desc";
   $con_orders = ($order==true) ? " a.course_detail_id " : " a.date asc";
   $q = "select a.course_detail_id,
				a.course_id,
				a.code_project,	
				a.day,
				a.date,
				a.time,
				a.date_phase1,
				a.date_phase2,		
				a.address,
				a.address_detail,
				a.date_other,
				a.sit_all,
				a.chair_all,
				a.`status`,
				a.open_regis,
				a.end_regis,
				a.datetime_add,
				a.datetime_update,
				a.active,
				a.recby_id,
				a.rectime,
				a.remark,
				a.date_start,
				a.date_stop,
				a.discount,
				a.inhouse,
				a.book,
				a.section_id,
				a.coursetype_id,
				a.book, 
				a.date_desc,
				a.regis_all,
				a.book_all
		 from course_detail a
		 where a.active!='' $con $con_course_detail_id
		 order by $con_orders
		 limit $limit_row";
   $r = $db->get($q);   
   return $r;
}



function get_course_discount($con="", $course_discount_id="", $order=false){
   if($con=="" && $course_discount_id=="") return array();
   global $db;   
   $con_course_discount_id = $course_discount_id ? " and a.course_discount_id=$course_discount_id" : "";
   $con = $course_discount_id ? " and a.course_discount_id=$course_discount_id" : $con;
   $con_orders = ($order==true) ? " a.course_discount_id " : " a.course_discount_id desc";
   $q = "select a.course_discount_id,
				a.code,
				a.name,
				a.name_eng,
				a.discount,
				a.parent_id,
				a.use_course,
				a.date_start,
				a.date_stop,
				a.active,
				a.recby_id,
				a.rectime,
				a.remark,
				a.section_id,
				a.coursetype_id
		 from course_discount a
		 where a.active!='' $con $con_course_discount_id
		 order by $con_orders
		 limit 1000";
   $r = $db->get($q);   
   return $r;
}


function get_course_detail_list($con="", $order=false){
   if($con=="") return array();
   global $db;   
   $con_orders = ($order==true) ? " a.course_detail_id " : " a.course_detail_id desc";
   $q = "select a.course_detail_list_id,
				a.course_detail_id,
				a.course_id,
				b.day,
				b.date,
				b.time,
				b.address,
				b.address_detail,
				b.sit_all,
				b.chair_all,
				b.`status`,
				b.open_regis,
				b.end_regis,
				b.datetime_add,
				b.datetime_update,
				b.active,
				b.recby_id,
				b.rectime,
				b.remark,
				b.inhouse,
				b.book,
				b.regis_all,
				b.book_all
		 from course_detail_list a inner join course_detail b on a.course_detail_id=b.course_detail_id and b.active='T'
		 where b.active!='' $con
		 order by $con_orders
		 limit 1000";
   $r = $db->get($q);   
   return $r;
}


function map_course_type($str){
	if($str=="aob_rom") return 1;
	else if($str=="sorb") return 2;
	else if($str=="aob_rom_sorb") return 3;
	else if($str=="aob_rom_tor_ar_yu") return 4;
}

function get_register($con="", $register_id="", $order=false){
	if($con=="" && $register_id=="") return array();
	global $db;	
	$con_register_id = $register_id ? " and a.register_id=$register_id" : "";
	$con = $register_id ? "" : $con;
	$con_orders = ($order==true) ? " a.register_id " : " a.register_id desc";
	$q = "select a.register_id,
			a.`no`,
			a.runyear,
			a.docno,
			a.runno,
			a.member_id,
			a.title,
			a.fname,
			a.lname,
			a.cid,
			a.slip_type,
			a.receipttype_id,
			a.receipttype_text,
			a.receipt_id,
			a.taxno,
			a.slip_name,
			a.slip_address1,
			a.slip_address2,
			a.date,
			a.register_by,
			a.ref1,
			a.ref2,
			a.`status`,
			a.expire_date,
			a.exam_date,
			a.course_id,
			a.course_detail_id,
			a.course_price,
			a.course_discount,
			a.pay,
			a.pay_date,
			a.pay_method,
			a.pay_yr,
			a.pay_mn,
			a.pay_id,
			a.pay_price,
			a.pay_diff,
			a.approve,
			a.approve_by,
			a.approve_date,
			a.register_mod,
			a.last_mod,
			a.last_mod_date,
			a.result,
			a.result_date,
			a.result_by,
			a.receipt_title,
			a.receipt_fname,
			a.receipt_lname,
			a.receipt_no,
			a.receipt_gno,
			a.receipt_moo,
			a.receipt_soi,
			a.receipt_road,
			a.receipt_district_id,
			a.receipt_amphur_id,
			a.receipt_province_id,
			a.receipt_postcode,
			a.active,
			a.recby_id,
			a.rectime,
			a.remark,
			a.coursetype_id,
			a.section_id,
			a.pay_status,
			a.branch, 
			a.runno, 
			a.docno,
			a.runyear, 
			a.auto_del, 
			a.usebook,
			a.te_results,
            b.name as receipt_district_name,
            c.name as receipt_amphur_name,
            d.name as receipt_province_name,
            e.name as receipt_name
	from register a left join district b on b.district_id=a.receipt_district_id
        left join amphur c on c.amphur_id=a.receipt_amphur_id
        left join province d on d.province_id=a.receipt_province_id        
        left join receipt e on e.receipt_id=a.receipt_id
    where a.active!='' $con $con_register_id
	order by $con_orders
	limit 1000";
   $r = $db->get($q);   
   return $r;
}

function str_course_type($str){
	if($str==1) return "aob_rom";
	else if($str==2) return "sorb";
	else if($str==3) return "aob_rom_sorb";
	else if($str==4) return "aob_rom_tor_ar_yu";
}

function get_use_chair($course_detail_id){
	if($course_detail_id=="") return 0;
	global $db;	
	$q = "select get_course_detail_sum($course_detail_id)";
	return $db->data($q);
/*	if($course_detail_id=="") return 0;
	global $db;	
	$get_all = 0;
	$not_id = "";	
	$q = " select a.register_id from register_course_detail a inner join register b on a.register_id=b.register_id where b.active='T' and a.course_detail_id=$course_detail_id and b.pay_status in (1,3,5,6) and b.usebook='F'";
	$array_get_all = $db->get($q);
	if($array_get_all){
		foreach ($array_get_all as $c_aga => $v_aga) {
			$not_id .= ",".$v_aga["register_id"];
			$get_all++;
		}
	}

	if($not_id!=""){
		$not_id = trim($not_id, ",");
		$con_agra = " and register_id not in ($not_id)";
	}
	$get_register_all = 0;
	$q = " select register_id from register where active='T' and course_detail_id='$course_detail_id' and pay_status in (1,3,5,6) $con_agra and usebook='F'";
	$array_gra = $db->get($q);
	if($array_gra){
		foreach ($array_gra as $c_agra => $v_agra) {						
			$get_register_all++;
		}
	}	
	return ((int) $get_all + (int) $get_register_all);	*/
}

function get_use_regis($course_detail_id, $status="1,3,5,6"){
	if($course_detail_id=="") return 0;
	global $db;	
	$q = " select count(register_id) from register where active='T' and course_detail_id like '%{$course_detail_id}%' and pay_status in ($status) and usebook='F'";
	return $db->data($q);

/*	$get_all = 0;
	$not_id = "";	
	$q = " select a.register_id from register_course_detail a inner join register b on a.register_id=b.register_id where b.active='T' and a.course_detail_id=$course_detail_id and b.pay_status in ($status) and b.usebook='F'";
	$array_get_all = $db->get($q);
	if($array_get_all){
		foreach ($array_get_all as $c_aga => $v_aga) {
			$not_id .= ",".$v_aga["register_id"];
			$get_all++;
		}
	}

	if($not_id!=""){
		$not_id = trim($not_id, ",");
		$con_agra = " and register_id not in ($not_id)";
	}
	$get_register_all = 0;
	$q = " select register_id from register where active='T' and course_detail_id='$course_detail_id' and pay_status in ($status) $con_agra and usebook='F'";
	$array_gra = $db->get($q);
	if($array_gra){
		foreach ($array_gra as $c_agra => $v_agra) {						
			$get_register_all++;
		}
	}	
	return ((int) $get_all + (int) $get_register_all);	*/
}


function get_use_book($course_detail_id){
	if($course_detail_id=="") return 0;
	global $db;	
	$get_register_all = 0;
	$q = " select count(register_id) from register where active='T' and usebook='T' and course_detail_id='$course_detail_id' and pay_status in (1,3,5,6)";
	return $db->data($q);
/*

	$q = " select register_id from register where active='T' and usebook='T' and course_detail_id='$course_detail_id' and pay_status in (1,3,5,6)";
	$array_gra = $db->get($q);
	if($array_gra){
		foreach ($array_gra as $c_agra => $v_agra) {						
			$get_register_all++;
		}
	}	
	return  (int) $get_register_all;	*/
}

function get_course_status_name($sit_all=0, $status="", $coursetype_id){
	global $db, $EMPID;
	$q = "SELECT name FROM `coursetype` WHERE coursetype_id=$coursetype_id";
	$name_type = $db->data($q);
	if($sit_all == 0 && $status != "ปิดรับสมัคร"){
		$status = "รอบ{$name_type}เต็ม";
	}
	if($status == "รอบ{$name_type}เต็ม"){
		$status = "<span style='color:#F00;'>รอบ{$name_type}เต็ม</span>";
	}
	return $status;	
}

function update_bill_payment_upload_list_details($data=array(), $ids=""){
	global $db;
	$date_now = date('Y-m-d H:i:s');
	$date = date('Y-m-d');

	// echo $date_now;die();

	if(!$ids){
		$bill_payment_upload_list_id = $data["bill_payment_upload_list_id"];
		$register_id = $data["register_id"];
		$q = "select bill_payment_upload_list_details_id from bill_payment_upload_list_details where bill_payment_upload_list_id={$bill_payment_upload_list_id} and register_id=$register_id";
		$ids = $db->data($q);
		if($ids) return;

	}
	$args = array(
		'table' => 'bill_payment_upload_list_details',
		'record_type' => $data["record_type"],
		'sequence_number' => $data["sequence_number"],
		'bank_code' => $data["bank_code"],
		'company_account' => $data["company_account"],
		'payment_date' => $data["payment_date"],
		'payment_time' => $data["payment_time"],
		'customer_name' => iconv("tis-620", "utf-8", $data['customer_name']) ,
		'customer_no_ref1' => $data["customer_no_ref1"],
		'ref2' => $data["ref2"],
		'ref3' => $data["ref3"],
		'branch_no' => $data["branch_no"],
		'teller_no' => $data["teller_no"],
		'transaction_type' => $data["transaction_type"],
		'transaction_code' => $data["transaction_code"],
		'cheque_number' => $data["cheque_number"],
		'amount' => $data["amount"],
		'cheque_bank_code' => $data["cheque_bank_code"],
		'payee_fee_same_zone' => $data["payee_fee_same_zone"],
		'payee_fee_diff_zone' => $data["payee_fee_diff_zone"],
		'filler' => $data["filler"],
		'new_cheque_no' => $data["new_cheque_no"],
		'bill_payment_upload_list_id' => (int) $data["bill_payment_upload_list_id"],
		'bill_payment_upload_id' => (int) $data["bill_payment_upload_id"],
		'register_id' => (int) $data["register_id"],
		'rectime' => $date_now
	);
	if($ids!=""){
		$args["id"] = $ids;
	}
	$ret = $db->set($args);
	return ($ids) ? $ids : $ret;
}


function update_payment_compare_all_list($data=array(), $ids="", $tb_ref_id=""){
	global $db, $EMPID;
	$date_now = date('Y-m-d H:i:s');
	$date = date('Y-m-d');
	
	// d($data);
	// die();
	$register_id = $data["register_id"];

	// echo $date_now;die();
	//update_payment_compare_list
	if(!$ids){
		$payment_compare_file_list_id = $data["bill_payment_upload_list_id"];

		$q = "SELECT payment_compare_all_list_id 
			FROM payment_compare_all_list 
			WHERE active='T' AND check_result='Y'
				AND payment_compare_file_list_id={$payment_compare_file_list_id} 
				AND register_id={$register_id}";
		$ids = $db->data($q);
		// if($ids) return;
		// echo $q."<br>";
		// die();
		// var_dump($ids);
		// die();
	}

	$q = "select b.course_detail_id , b.register_course_detail_course_detail_id, b.register_course_detail_course_id, b.course_id
		   from register AS r inner join view_register_course_detail b on r.register_id=b.view_register_course_detail_id
		   where r.register_id={$register_id}";
	$x = $db->rows($q);

	$t = $x["register_course_detail_course_detail_id"];
	$tt = $x["register_course_detail_course_id"];

	$payment_date = $data["payment_date"];
	$payment_date_dd = substr($payment_date, 0, 2);
	//$args["payment_date_dd"] = $payment_date_dd;
	$payment_date_mm = substr($payment_date, 2, 2);
	//$args["payment_date_mm"] = $payment_date_mm;
	$payment_date_yy = substr($payment_date, 4, 4);
	//$args["payment_date_yy"] = $payment_date_yy;
	$payment_time = $data["payment_time"];
	$payment_time_hh = substr($payment_time, 0, 2);
	$payment_time_mm = substr($payment_time, 2, 2);
	$payment_time_ss = substr($payment_time, 4, 2);
	$payment_date = "{$payment_date_yy}-{$payment_date_mm}-{$payment_date_dd}";
	$payment_time = "{$payment_time_hh}:{$payment_time_mm}:{$payment_time_ss}";

	$amount = (int)$data["amount"];
	$amount = substr($amount, 0, -2);
	// echo $amount; 

	$args = array(
		'table' => 'payment_compare_all_list',
		'payment_compare_file_list_id' => (int)$data["bill_payment_upload_list_id"],
		'table_payment_compare_ref_id' => (int)$tb_ref_id,
		'payment_date' => $payment_date,
		'payment_type' => 'bank',
		'bank_code' => $data["bank_code"],
		'amount' => $amount,
		'fee' => 0,
		'check_result' => 'Y',
		'pay_date_chk' => $payment_date,
		'active' => 'T',
		'recby_id' => $EMPID,
		'register_id' => (int)$register_id,
		// 'date_create' => $date_now
	);
	if($ids!=""){
		$args["id"] = $ids;
		$args["date_update"] = $date_now;
	}else{
		$args["date_create"] = $date_now;
	}
	$args["course_detail_id"] = ($t!="") ? $t : $x["course_detail_id"];
	$args["course_id"] = ($tt!="") ? $tt : $x["course_id"];
	$ret = $db->set($args);
	// pr($args);
	// die();
	// $ret = $db->set($args, false, true);
	// echo "<br>ret => ".$ret;
	return ($ids) ? $ids : $ret;
}//end func

function register_log_save($register_id = null, $save_type="", $from="", $new_remark=true){
	global $db;
	if(!$register_id) return;
	$v = get_register("", $register_id);
	// d($v);die();
	if($v){
		$v = $v[0];
		$info = $v;

		$q = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'register_log'";
		$column_name = $db->get($q);
		// d($column_name);die();
		$register_log_data = array("table"=>'register_log');
		foreach ($column_name as $key => $field) {
			$field_name = $field["COLUMN_NAME"];
			if($info[$field_name]=="") continue;
			$register_log_data[$field_name] = $info[$field_name];
		}

		if ( $new_remark ) {
			$str = "มีการ {$save_type} จากหน้า {$from}";
			$register_log_data["remark"] = $str;			
		}//end if

		unset($register_log_data["register_log_id"]);
		$id = $db->set($register_log_data);
		// d($db->set($register_log_data, true, true));die();
	}//end if
	// echo "5555555555555";die();
}//end func

?>
