<?php 

// header('Location: https://register.ati-asco.org/index.php');
/*
if (! isset($_SERVER['HTTPS']) or $_SERVER['HTTPS'] == 'off' ) {
    $redirect_url = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    header("Location: $redirect_url");
    exit();
}
*/
include_once "./share/authen.php";
include_once "./lib/lib.php";
include_once "./connection/connection.php";
?>
<?php include ('include/header.php'); ?>
<body>
<?php include ('include/top-menu.php'); ?>

<?php
global $db, $aLogin;
$member_id = $aLogin["member_id"];

$_SESSION["activate"] = !empty($aLogin["activate_code"]) ? 'T' : 'F';

if( !empty($member_id) && ($_GET["p"]!="new-step-four") ){

	$q = "SELECT a.register_id 
		FROM register a
		WHERE a.active='T' 
			AND a.auto_del='T' 
			AND a.pay_status IN (3,5,6)
			-- AND ( a.pay IS NOT NULL AND a.pay<>'' ) 
			AND a.member_id={$member_id} 
	";
	// echo $q;
	$reg_ids = $db->get($q);
	if ( !empty($reg_ids) ) {
		foreach ($reg_ids as $key => $v) {
			$register_id = $v["register_id"];
			$args = array();
			$args["table"] = "register";
			$args["id"] = $register_id;
			$args["remark"] = 'มีการอัพเดท auto_del=F จากหน้า index.php';
			$args["auto_del"] = 'F';
			$db->set($args);
		}//end loop $v
	}//end if

	// d($args); die();

	$q = "SELECT register_id 
		FROM register 
		WHERE 1
			AND active='T'
			AND auto_del='T' 
			AND member_id=$member_id 
	";
	$reg_id = $db->get($q);
	if($reg_id && count($reg_id)>0){
		foreach ($reg_id as $key => $data) {
			$id = $data["register_id"];
			// $q = "delete from register where register_id=$id";
			$q = "UPDATE register SET active='F', remark='มีการอัพเดท active=F จากหน้า index.php' WHERE register_id={$id}";
			if($id) $db->query($q);
			// $q = "delete from register_course where register_id=$id";
			$q = "UPDATE register_course SET active='F' WHERE register_id={$id}";
			if($id) $db->query($q);
			// $q = "delete from register_course_detail where register_id=$id";
			$q = "UPDATE register_course_detail SET active='F' WHERE register_id={$id}";
			if($id) $db->query($q);
		}//end loop $data
	}//end if	
}//end if

/*
$r = "581012110101N";
$rs = genDigit($r);
$x = checkDigit($rs);
var_dump($x);*/
if($is_login){
	$args = array(
		'regis' => 'register.php',
		'forget-password' => 'forgot-password.php',
		'change-password' => 'change-password.php',
		'main' => 'main.php',
		'new-step-one' => 'new-step-one.php',
		'new-step-two' => 'new-step-two.php',
		'new-step-three' => 'new-step-three.php',
		'new-step-four' => 'new-step-four.php',
		'new-step-five' => 'new-step-five.php',
		'register-choice' => 'register-choice.php',
		'invoice' => 'invoice.php',
		'new-condition-one' => 'new-condition-one.php',
		'profile' => 'member-profile.php',
		'profile2' => 'member-profile2.php',
		'news' => 'news-detail.php',
		'register-history' => 'register-history.php',
		'register-detail' => 'register-detail.php'
	);
	
}else{
	$args = array(
			'default' => 'begin.php',
			'regis' => 'register.php',
			'forget-password' => 'forgot-password.php',
			'import' => 'import.php'
		);
}

$call_page = isset($_GET['p']) ? $_GET['p'] : "";
if(!$call_page || !($args[$call_page])) 
	$call_page = ($is_login) ? "main" : "default";

render_pages($args[$call_page]);
?>

<?php include ('include/footer.php') ?>

</body>
</html>

<?php
$db->disconnect();
?>