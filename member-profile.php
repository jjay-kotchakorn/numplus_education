<?php
include_once "./lib/lib.php";
include_once "./share/member.php";
include_once "./share/datatype.php";
global $db;
$info = $_SESSION["login"]["info"];
$educationlevel = datatype(" and a.active='T'", "educationlevel", true);
$province = datatype_sort_name(" and a.active='T'", "province", true);
$receipttype = datatype_sort_name(" and a.active='T'", "receipttype", true);
$university = datatype_university(true);

$member_id = $info["member_id"];
$q = "select activate_code, private, fa, img, email1, email2 from member where member_id={$member_id}";
$r = $db->get($q);
 // d($r);

$r = $r[0];
$private=$r["private"];
$member_fa=$r["fa"];
$fullIMG = "";
// echo $private;

$email1 = trim($r["email1"]);
$email2 = trim($r["email2"]);
$q = "SELECT email_blacklist_id 
  FROM email_blacklist 
  WHERE active='T' 
    AND (name='{$email1}' OR name='{$email2}')
";
$email_blacklist_id = $db->data($q);
unset($q);

if (!empty($r["img"])) {
  $fullIMG = $r["img"];
  // $fullIMG = "";
}

if($r["activate_code"] == NULL){
	$result_msg = 'กรุณาอัพเดทข้อมูลให้ถูกต้อง';
	echo '<script type="text/javascript">alertD("'.$result_msg.'");</script>';
}//end if

?>

<script type="text/javascript" src="./js/jquery-ui-1.8.10.offset.datepicker.min.js"></script>
<form name="form_m" id="form_m" action="update-profile.php" method="post">
	<div class="bgprofile-detai2">
		<div class="bodyproselect">
<?php 
      if ( !empty($email_blacklist_id) ) {
?>      
        <br>
        <div >
          <center><span class="red center" style="
    font-size: 22px;
">** กรุณาอัพเดท Email ที่ท่านใช้งานในปัจจุบัน และข้อมูลอื่นๆตามที่ระบบต้องการ **</span></center>
        </div>
<?php 
      }//end if
?>
        <div class="blueheader">ข้อมูลสมาชิก</div>
        <!-- <div class="subheader">ข้อมูลสมาชิก</div>
         -->

        <div class="subheader" style="">ข้อมูลสมาชิก</div>     
        
        <div class="row">

        <div class="pcontainer col-sm-8">
        
        <table width="93%" cellpadding="10" class="form pinfo">
  <tbody>
                    <tr>
                    <td width="20%">เลขที่บัตรประชาชน <span class="red">*</span>   </td>
                     <td width="24%"><input type="text" readonly="true" style="background:#eee;" class="required cart_id" tabindex="1" onkeypress="enterToSubmit();" name="cid" id="cid" maxlength="13" /></td>
                      <td width="10%"></td>
                      <td width="16%"></td>
                      <td width="10%"></td>
                      <td width="18%"></td>
                    </tr>
                     <tr>
                    <td>คำนำหน้าชื่อ<span class="red"> *</span> 
                       </td>
                     <td> <select name="title_th" id="title_th" class="regis_select">
							<option value="นาย">นาย</option>
							<option value="นาง">นาง</option>
                            <option value="นางสาว">นางสาว</option>
							<option value="อื่นๆ">อื่นๆ</option>
						</select>    
                                <input type="text" name="title_th_text" maxlength="50" tabindex="2" onkeypress="enterToSubmit();" id="title_th_text" class="othern th_font" hidden="true"  >
                                
                       </td>
                      <td>ชื่อ <span class="red">*</span> </td>
                      <td><input type="text" onkeypress="enterToSubmit();" tabindex="3" class="required th_font" name="fname_th" id="fname_th" maxlength="50"></td>
                      <td>นามสกุล <span class="red">*</span> </td>
                      <td><input type="text" name="lname_th" tabindex="4"   class="required th_font" onkeypress="enterToSubmit();" id="lname_th" maxlength="50"></td>
                     
                    </tr>
                     <tr>
                    <td>Title <span class="red">*</span> </td>
                     <td> <select name="title_en" id="title_en" class="regis_select">
							<option value="Mr">Mr.</option>
							<option value="Mrs">Mrs.</option>
                            <option value="Ms">Ms.</option>
                            <option value="Other">Other</option>
						</select>  
                            <input type="text" name="title_en_text" maxlength="50" tabindex="2" onkeypress="enterToSubmit();" id="title_en_text" class="othern eng_font" hidden="true">
                                
                       </td>
                      <td>First name <span class="red">*</span></td>
                      <td><input type="text" onkeypress="enterToSubmit();" tabindex="5"  class="required eng_font" name="fname_en" id="fname_en" maxlength="50"></td>
                      <td>Last name <span class="red">*</span></td>
                      <td><input type="text" name="lname_en"  tabindex="6"   class="required eng_font" onkeypress="enterToSubmit();" id="lname_en" maxlength="50"></td>
                    </tr>
                     <tr>
                    <td>วัน / เดือน / ปีเกิด <span class="red">*</span> </td>
                     <td><input type="text" class="required" tabindex="8" onkeypress="enterToSubmit();" name="birthdate" id="birthdate" ></td>
                      <td>เพศ <span class="red">*</span> </td>
                      <td><select name="gender" id="gender" class="regis_select required">
							<option value="">---- เลือก ----</option>
							<option value="M">ชาย</option>
							<option value="F">หญิง</option>
						</select></td>
                      <td></td>
                      <td></td>
                    </tr>
                 </tbody>
              </table>
 </div>


<?php
        if ( $member_fa=='T' ) {
?>
          <div class="col-sm-2">
            <div class="form-group">
            <div class="clear"></div>               
              <?php 
                if($fullIMG){ 
              ?>
                  <a class="fancybox" href="<?php echo "./fa-backoffice/".$fullIMG;?>">
                    <img id="img" name="img" src="<?php echo "./fa-backoffice/".$fullIMG;?>" style="max-width:100%;" onclick=view_img(); class="thumbnail"/>
                  </a>  
              <!-- <h4 class="col-sm-12"><a rel="fancybox" href="<?php //echo $fullIMG; ?>">Images Preview </a> </h4>
               -->
              <?php 
                }else{
              ?>
                 <!-- <img id="img" src="images/no-avatar-male.jpg" style="max-width:100%; cursor: pointer;" onClick="removeImg();" class="thumbnail"> -->
                 <img id="img" src="images/no-avatar-male.jpg" style="max-width:100%;" class="thumbnail">
              <?php      
                }//end else
              ?>
             <!--  <img id="img" src="images/no-avatar-male.jpg" style="max-width:100%; cursor: pointer;" onClick="removeImg();" class="thumbnail"> -->
<!--              
 <h4 class="col-sm-12">Image Upload</h4>
 <div class="col-sm-12">
   <input type="file" name="memberimg" id="memberimg" size="19" onChange=" readURL(this);">              
 </div>
 -->
            </div>
          </div> 
<?php
        }//end if
?>

        </div> 
        <br>
        <div class="subheader">ข้อมูลรหัสผ่าน</div>
        <div class="pcontainer">
       <table width="100%" cellpadding="10" class="form pinfo">
                    <tbody>
                    <tr>
                    <td width="175">รหัสผ่าน
<span class="red" style="font-size:14px;">* (อย่างน้อย 8 ตัวอักษร)</span>   </td>
                     <td width="190"><input type="password" onkeypress="enterToSubmit();" tabindex="9" class="required password" name="password" id="password" maxlength="32"></td>
                      <td width="108">ยืนยันรหัสผ่าน <span class="red">*</span></td>
                      <td width="201"> <input type="password" equalTo="#password" tabindex="10" onkeypress="enterToSubmit();"  name="password2" id="password2" maxlength="32"></td>
                      <td width="84"></td>
                      <td width="169"></td>
                    </tr>
                    
                    </tbody>
                    </table>
        </div>
         <div class="subheader">ที่อยู่ปัจจุบันที่ใช้จัดส่งเอกสาร</div>
        <div class="pcontainer">
        <table width="88%" cellpadding="10" class="form">
                    <tbody>
                    <tr>
                    <td width="36%">บ้านเลขที่ <span class="red">*</span>   <input type="text" tabindex="11" class="required" onkeypress="enterToSubmit();" id="no" name="no"> </td>
                     <td width="3%">&nbsp;</td>
                     <td width="35%">หมู่บ้าน / คอนโด / อาคาร <input type="text" tabindex="12" onkeypress="enterToSubmit();" id="gno" name="gno" ></td>
                      <td width="2%">&nbsp;</td>
                      <td width="24%"><span class="notright">หมู่ที่
                          <input type="text"  onkeypress="enterToSubmit();" tabindex="13" id="moo" name="moo" maxlength="5"  pattern="\d{0,}" title="กรอกเฉพาะตัวเลข" />
                      </span></td>
                    </tr>
                     <tr>
                       <td><span class="notright">ซอย
                           <input type="text"  onkeypress="enterToSubmit();" tabindex="14" id="soi" name="soi"   />
                       </span></td>
                       <td>&nbsp;</td>
                       <td>ถนน
                       <input type="text" id="road" tabindex="15" onkeypress="enterToSubmit();" name="road"  /></td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                     </tr>
                     <tr>
                  
                     <td>จังหวัด <span class="red">*</span>
                       <select name="province_id" tabindex="16" id="province_id" class="required regis_select" onchange="getDropDown('#amphur_id', this.value, 'amphur', 'data/province.php')" >
                         <option value="">---- เลือก ----</option>
                         <?php foreach ($province as $key => $value) {
        										$id = $value['province_id'];
        										$name = $value['name'];
        										echo  "<option value='$id'>$name</option>";
        								} ?>
                       </select></td>
                      <td>&nbsp;</td>
                      <td>อำเภอ / เขต <span class="red">*</span>
                      <select name="amphur_id" tabindex="17" id="amphur_id" class="required regis_select" onchange="getDropDown('#district_id', this.value, 'district', 'data/province.php')" >
                        <option value="">---- เลือก ----</option>
                      </select></td>
                     <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                     <tr>
                     <td>ตำบล / แขวง <span class="red">*</span>
                      <select name="district_id" tabindex="18" id="district_id" class="required regis_select" >
                        <option value="">---- เลือก ----</option>
                      </select></td>
                     <td>&nbsp;</td>
                     <td>รหัสไปรษณีย์ <span class="red">*</span>
                       <input type="text" tabindex="19" id="postcode" class="required" onkeypress="enterToSubmit();" name="postcode" maxlength="5"  pattern="\d{5}" title="กรอกเฉพาะตัวเลขให้ครบ 5 หลัก" /></td>
                      <td></td>
                      <td></td>
                    </tr>
                     
                     <tr>
                    <td>E-mail	<span class="red">*</span> <input type="text" id="email1" tabindex="20" class="email" onkeypress="enterToSubmit();" name="email1" maxlength="50"></td>
                     <td>&nbsp;</td>
                     <td>E-mail สำรอง <input tabindex="21" type="text" id="email2" class="email" onkeypress="enterToSubmit();" name="email2" maxlength="50"></td>
                      <td></td>
                      <td></td>
                    </tr>
                     <tr>
                    <td>โทรศัพท์บ้าน <span class="sample">(022640889)</span>  <input type="text" onkeypress="enterToSubmit();" tabindex="22" id="tel_home" name="tel_home" maxlength="9" pattern="[0]+\d{8}" title="กรอกเฉพาะตัวเลขให้ครบ 9 หลัก">
                    	                  </td>
                     <td>&nbsp;</td>
                     <td>โทรศัพท์มือถึอ <span class="red">*</span> <span class="sample">(0813456789)</span>    <input tabindex="23" type="text" id="tel_mobile" class="required" onkeypress="enterToSubmit();" name="tel_mobile" maxlength="10" pattern="[0]+\d{9}" title="กรอกเฉพาะตัวเลขให้ครบ 10 หลัก">
	                                       </td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                    <td>โทรศัพท์ที่ทำงาน <span class="red"> </span><span class="sample">(022640889)</span>  
                      <input type="text" tabindex="24"  onkeypress="enterToSubmit();" id="tel_office" 
                        name="tel_office" maxlength="9" pattern="[0]+\d{8}" title="กรอกเฉพาะตัวเลขให้ครบ 9 หลัก">
                    </td>
                     <td>&nbsp;</td>
                     <td>เบอร์ต่อ (ถ้ามี) <input type="text" id="tel_office_ext" tabindex="25" onkeypress="enterToSubmit();" name="tel_office_ext" maxlength="10" pattern="\d{0,}" title="กรอกเฉพาะตัวเลข"></td>
                      <td></td>
                      <td></td>
                    </tr>
                    </tbody>
                    </table>
                    </div>
       <div class="subheader">ข้อมูลบนใบเสร็จรับเงิน</div>
        <div class="pcontainer">    
         <table width="100%" cellpadding="10" class="form">
                    <tbody>
                    <tr>
                    <td width="20%"><div class="radiob"><input type="radio" name="slip_type" id="individuals" value="individuals" onclick="select_normal_niti('normal');">ออกในนามบุคคลธรรมดา <span class="red">*</span></div></td>
                     <td width="33%"><div class="radiob"><input type="radio" name="slip_type" id="corparation" value="corparation" onclick="select_normal_niti('niti');">ออกในนามนิติบุคคล (สำหรับเบิกค่าใช้จ่าย) <span class="red">*</span></div></td>
                      <td width="47%"></td>
                    </tr>
                    
                    </tbody>
                    </table>
                
                <div id="receipt_detail"  hidden="true">
                        <div class="pro4-colum1-r1">
                    <table width="92%" cellpadding="10" class="form">
                    <tbody>
                    <tr id="receipt_detail_title" >
                    	<td colspan="5">กรุณาเลือกบริษัท</td>
                    </tr>
                    <tr>
                      <td width="14%">
                        <div class="radiob">
                          <input class="required" type="radio" name="receipttype_id"  id="receipttype_id" value="1" 
                            onclick="getDropDown('#receipt_id', this.value, 'receipt', 'data/receipt.php');trReceiptInfoShow(); receipttype_text_required('no');">บริษัทหลักทรัพย์
                        </div>
                      </td>
                      <td width="10%" >
                        <div class="radiob">
                          <input class="required" type="radio" name="receipttype_id" id="receipttype_id" value="2" 
                            onclick="getDropDown('#receipt_id', this.value, 'receipt', 'data/receipt.php');trReceiptInfoShow(); receipttype_text_required('no');">ธนาคาร
                        </div>
                      </td>
                      <td width="20%">
                        <div class="radiob">
                          <input class="required" type="radio" name="receipttype_id" id="receipttype_id" value="3" 
                            onclick="getDropDown('#receipt_id', this.value, 'receipt', 'data/receipt.php');trReceiptInfoShow(); receipttype_text_required('no');">บริษัทหลักทรัพย์จัดการกองทุน
                        </div>
                      </td>
                      <td width="14%">
                        <div class="radiob">
                          <input class="required" type="radio" name="receipttype_id"  id="receipttype_id" value="4" 
                            onclick="getDropDown('#receipt_id', this.value, 'receipt', 'data/receipt.php');trReceiptInfoShow(); receipttype_text_required('no');">บริษัทประกันชีวิต
                        </div>
                      </td>
                      <td width="45%">
                        <div class="radiob">
                          <input type="radio" name="receipttype_id" id="receipttype_id" value="5" 
                            onclick="receipt_other(); receipttype_text_required('yes');">อื่นๆ (โปรดระบุ) 
                          <input type="text" onkeypress="enterToSubmit();" maxlength="100" name="receipttype_text" 
                            id="receipttype_text" placeholder="กรุณากรอกชื่อบริษัท" class="otherinput">
                        </div>
                      </td>
                    </tr>
                      <tr id="trReceiptInfo">
                        <td colspan="5">
                        <table width="101%" border="0" cellspacing="2" cellpadding="0">
                          <tr>
                            <td width="18%">ออกใบเสร็จในนาม <span class="red">*</span></td>
                            <td width="45%"><div class="notright">
                              <div class="from-address" id="wait_menu">
                                <select name="receipt_id" id="receipt_id" onchange="receipt_info(this.value);" style="width:290px!important; margin-left:-5px;"  class="required">
                                  <option value="">---- เลือก ----</option>
                                </select>
                              </div>
                            </div></td>
                            <td width="39%"><div class="notright">สาขาที่ <span class="red">*</span>
                                  <input aria-required="true" name="branch" class="required" maxlength="50" onkeypress="enterToSubmit();" id="branch" type="text" />
                            </div></td>
                          </tr>
                        </table>                        
                        </td>
                      </tr>
                    </tbody>
                    </table>
                        </div>
                  <table width="88%" cellpadding="10" class="form">
                    <tbody>
                     <tr id="trSlipName">
                       	<td>
                        	ออกใบเสร็จในนาม <span class="red">*</span> 
                            <select name="receipt_title" id="receipt_title" class="regis_select">
              								<option value="นาย">นาย</option>
              								<option value="นาง">นาง</option>
                              <option value="นางสาว">นางสาว</option>
              								<option value="อื่นๆ">อื่นๆ</option>
            					  		</select>    
                    		    <input type="text" name="receipt_title_text" maxlength="50" tabindex="2" onkeypress="enterToSubmit();" id="receipt_title_text" class="receipgroup othern regis_receipt_title_other" hidden="true"  >
                       </td> 
						           <td></td>
                     	 <td>ชื่อ <span class="red">*</span><input type="text" name="receipt_fname" class="required" maxlength="50"  onkeypress="enterToSubmit();" id="receipt_fname" /> </td>
                     	 <td></td>
                       <td>นามสกุล <span class="red">*</span>  <input type="text" name="receipt_lname" class="required" onkeypress="enterToSubmit();" maxlength="50" id="receipt_lname"></td>
                    </tr>
                    <tr id="receipt_taxno_fix">
                    	<td>เลขที่บัตรประจำตัวผู้เสียภาษี <span class="red">*</span> <input type="text" name="taxno" id="taxno" maxlength="13" class="required" ></td>
                      <td colspan="4"></td>
                    </tr>
                     <tr>
                     <td colspan="5">
                     	<div id="same_address" class="tick"> ที่อยู่ (ที่จะแสดงบนใบเสร็จรับเงิน)&nbsp;&nbsp;
                        	<span id="same_address_checkbox"><input type="checkbox" name="same_addr" id="same_addr" onClick="checkboxaddress();"> ใช้ข้อมูลที่อยู่ปัจจุบัน</span>
                        </div></td>
                   
                    </tr>
                    <tr>
                    <td width="33%">บ้านเลขที่ <span class="red">*</span>   <input id="receipt_no" class="required" name="receipt_no" onkeypress="enterToSubmit();" type="text"> </td>
                     <td>&nbsp;</td>
                     <td width="33%">หมู่บ้าน / คอนโด / อาคาร <input type="text" id="receipt_gno"   onkeypress="enterToSubmit();" name="receipt_gno" /></td>
                      <td>&nbsp;</td>
                      <td width="33%">หมู่ที่
                      <input id="receipt_moo" maxlength="5" type="text" onkeypress="enterToSubmit();" name="receipt_moo"  pattern="\d{0,}" title="กรอกเฉพาะตัวเลข"  > </td>
                    </tr>
                     <tr>
                    <td>ซอย <input type="text"  name="receipt_soi" onkeypress="enterToSubmit();" id="receipt_soi" /></td>
                     <td>&nbsp;</td>
                     <td>ถนน
                       <input type="text" name="receipt_road" onkeypress="enterToSubmit();" maxlength="50" id="receipt_road" /></td>
                      <td>&nbsp;</td>
                      <td>จังหวัด <span class="red">*</span>
                        <select name="receipt_province_id" tabindex="16" id="receipt_province_id" class="required regis_select" 
                          onclick="getDropDown('#receipt_amphur_id', this.value, 'amphur', 'data/province.php')"
                          onchange="getDropDown('#receipt_amphur_id', this.value, 'amphur', 'data/province.php')">
                          <option value="">---- เลือก ----</option>
                          <?php foreach ($province as $key => $value) {
																	$id = $value['province_id'];
																	$name = $value['name'];
																	echo  "<option value='$id'>$name</option>";
															} ?>
                        </select></td>
                    </tr>
                     <tr>
                    <td>อำเภอ / เขต <span class="red">*</span>
                      <select name="receipt_amphur_id" tabindex="17" id="receipt_amphur_id" class="required regis_select" 
                        onclick="getDropDown('#receipt_district_id', this.value, 'district', 'data/province.php')"
                        onchange="getDropDown('#receipt_district_id', this.value, 'district', 'data/province.php')" >
                        <option value="">---- เลือก ----</option>
                      </select></td>
                     <td>&nbsp;</td>
                     <td>ตำบล / แขวง <span class="red">*</span>
                       <select name="receipt_district_id" tabindex="18" id="receipt_district_id" class="required regis_select" >
                         <option value="">---- เลือก ----</option>
                       </select></td>
                      <td></td>
                      <td>รหัสไปรษณีย์ <span class="red">*</span>
                        <input type="text" name="receipt_postcode" class="required" maxlength="5" onkeypress="enterToSubmit();" id="receipt_postcode"  pattern="\d{5}" title="กรอกเฉพาะตัวเลขให้ครบ 5 หลัก" /></td>
                    </tr>
                    </tbody>
                    </table></div>
        </div>
        
         <div class="subheader">ประวัติการทำงานปัจจุบัน</div>
        <div class="pcontainer">
         <table width="84%" cellpadding="10" class="form">
                    <tbody>
                      <tr>
                        <td width="14%"><div class="radiob"><input type="radio" class="required" name="orgtype_id"  id="orgtype_id" value="1" onclick="getDropDown('#org_name', this.value, 'receipt', 'data/receipt.php'); orgtype_text_required('no');">บริษัทหลักทรัพย์</div></td>
                        <td width="12%"><div class="radiob"><input type="radio" class="required" name="orgtype_id" id="orgtype_id" value="2" onclick="getDropDown('#org_name', this.value, 'receipt', 'data/receipt.php'); orgtype_text_required('no');">ธนาคาร</div></td>
                        <td width="20%"><div class="radiob"><input type="radio" class="required" name="orgtype_id" id="orgtype_id" value="3" onclick="getDropDown('#org_name', this.value, 'receipt', 'data/receipt.php'); orgtype_text_required('no');">บริษัทหลักทรัพย์จัดการกองทุน</div></td>
                        <td width="14%"><div class="radiob"><input type="radio" class="required" name="orgtype_id"  id="orgtype_id" value="4" onclick="getDropDown('#org_name', this.value, 'receipt', 'data/receipt.php'); ; orgtype_text_required('no');">บริษัทประกันชีวิต</div></td>
                        <td width="45%">
                          <div class="radiob">
                            <input type="radio" class="required" name="orgtype_id" id="orgtype_id" value="5" 
                              onclick="orgtype_other(); orgtype_text_required('yes');">อื่นๆ (โปรดระบุ) 
                            <input type="text" onkeypress="enterToSubmit();" maxlength="100" name="orgtype_text" 
                              id="orgtype_text" placeholder="กรุณากรอกชื่อบริษัท" class="otherinput" >
                          </div>
                        </td>
                      </tr>
                    </tbody>
                    </table>
        <table  width="72%" cellpadding="10" class="form">
                    <tbody>
                    <tr>
                    <td id="table_Org_Name" width="50%">บริษัท <span class="red">*</span>  
<select name="org_name" id="org_name" class="required"  style="float:inherit;width:290px;" >
                          <option value="">---- เลือก ----</option>
                      </select>
                     </td>
                     <td class="table_Org_Name"width="4%">&nbsp;</td>
                     <td width="45%">ตำแหน่ง <span class="red">*</span> <input type="text" class="required" onkeypress="enterToSubmit();" name="org_position" id="org_position" maxlength="50"  style="float:inherit;width:295px;"></td>
                      <td width="1%"></td>
                    </tr>
                    </tbody>
                    </table>
                    </div>
                      <div class="subheader">การศึกษา</div>
        <div class="pcontainer">
                           <table width="90%" cellpadding="10" class="form">
                    <tbody>
                    <tr>
                    <td width="41%" height="39">
                    <div class="notright">ระดับ <span class="red">*</span>   	<select name="grd_lv1" id="grd_lv1" class="required">
									<option value="">---- เลือก ----</option>									<?php foreach ($educationlevel as $key => $value) {
																				$id = $value['educationlevel_id'];
																				$name = $value['name'];
																				echo  "<option value='$id'>$name</option>";
																		} ?>
																	</select> ปีที่สำเร็จการศึกษา <span class="red">*</span> <select id="grd_yr1" name="grd_yr1" class="required">
																			<option value="">ไม่ระบุ</option>
																			<?php
																			$yearBegin = date('Y')*1+543;
																			for($i=0; $i<=64; $i++){
																				$my_y = $yearBegin*1-$i;
																				echo "<option value='$my_y'>$my_y</option>
																				";
																			}
																			?>
															</select></div></td>
                     <td width="59%" height="39">สถาบัน <span class="red">*</span> 
       	      			<select id="select_university" name="select_university" class="required regis_university"  style="float:inherit;height:29px;" onchange="change_select_university();">
							<option value="">ไม่ระบุ</option>
                            <?php foreach ($university as $key => $value) {
									$ugrp = $value['UGRP'];
									$uid = $value['UID'];
									$id = $ugrp."-".$uid;
									$name = $value['UNAME'];
									echo  "<option value='$id'>$name</option>";
							} ?>
                            <!-- <option value="0-0">อื่นๆ</option> -->
                            <option value="0-0">อื่นๆ</option>
						</select>
                        <input type="hidden" id="grd_ugrp1" name="grd_ugrp1" value="0" />
                        <input type="hidden" id="grd_uid1" name="grd_uid1" value="0" />
                        <input type="hidden" id="grd_uname1" name="grd_uname1" value="" />&nbsp;
                        
                     </td>
                    
                    </tr>
                     <tr>
                        <td height="39">สาขาวิชา <span class="red">*</span> <input type="text" class="required" onkeypress="enterToSubmit();" name="grd_major1" id="grd_major1" maxlength="50"  style="float:inherit;width:295px;"></td>
                        <!-- <td height="39">อื่น ๆ (ระบุเอง)   	<input type="text" onkeypress="enterToSubmit();" name="grd_other" id="grd_other" maxlength="100" style="float:inherit;" class="otherinput">
                      </td> -->
                        <td height="39">อื่นๆ (โปรดระบุ) <input type="text" onkeypress="enterToSubmit();" name="grd_other" id="grd_other" 
                          maxlength="100" style="float:inherit;" class="otherinput" placeholder="กรุณาเลือกสถาบันการศึกษา"></td>
                     <td height="39"></td>
                      <td height="39"></td>
                    </tr>
                    </tbody>
                    </table>  
                    </div>
<?php
                if ( $member_fa=='T' ) {
                  // $private = ($private=='F') ? "" : 'checked';
                  $private_value = false;
                  if ( $private=='T' ) {
                    $private_value = true;
                    $private = 'checked';
                  }
?>
                    <div class="subheader">ความเป็นส่วนตัว</div>
                    <div class="pcontainer">
                      <table>
                        <tr>                      
                            <td width="100%" class="center">
                              <span></span>
                              <div class="">
                                <input type="checkbox" class="" name="private" id="private" value="" onchange="private_toggle()" <?php echo $private;?> ><span style="font-size: 22px;">เปิดเผยข้อมูล ชื่อ-สกุล ให้แก่บริษัทสมาชิกชมรมวาณิชธนกิจ</span>
                              </div>
                            </td>                 
                        </tr>                      
                      </table>
                    </div>
                    

<?php
                }//end if
?>     
                    </div>
											<div style="display:none" class="pro-fix2">
											<input type="checkbox" name="get_news1" id="get_news1" value="1" >
												ยินยอมรับข่าวสารทาง e-mail จาก ATI (ฟรี)<input name="ch_submit_edit_member" type="hidden" value="yes" id="ch_submit_edit_member" />
													<input name="id_edit" type="hidden" value="<?php echo $M_ID_tm; ?>" id="id_edit" />
												
												<input type="hidden" name="member_id" value="<?php echo $member_id; ?>">

										</div>
                        <div class="" style="height: 30px;margin: 0 auto; display:block; text-align:center; clear:both; padding-top:40px;">
                          <div class="se-botton-next" onclick="javascript: window.open('index.php?p=main','_self');" style="float:none;display:inline-block;"><center><p>กลับสู่หน้าหลัก</p></center></div>
                          <a href="#" onclick="chFrmSubmit();"><div class="se-botton-next" style="float:none;display:inline-block;"><center><p>อัพเดท</p></center></div></a>
                        </div>

                        </div>

									</form>


<script type="text/javascript">
	$(document).ready(function($) {
		var id = "<?php echo $member_id; ?>";
		if(id) getInfo(id);

    $("a[rel=fancybox]").fancybox();

    var ans = ( $("#private").is(':checked') ) ? 1 : 0;
    $("#private").val(ans);

		$("#form_m").validate();
		var msg = "<?php echo $_GET["update_state"]; ?>";
    	if(msg=="T"){
      		alertD("บันทึกข้อมูลเรียบร้อยแล้ว");
    	}
    //dateThai("#birthdate");
    var d = new Date();
    var toDay = d.getDate() + '/' + (d.getMonth() + 1) + '/' + (d.getFullYear() + 543);
    
    $("#birthdate").datepicker({
      maxDate: 0,
      dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, yearRange:"-100:+0", isBuddhist: true, defaultDate: toDay, dayNames: ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'],
      dayNamesMin: ['อา.', 'จ.', 'อ.', 'พ.', 'พฤ.', 'ศ.', 'ส.'],
      monthNames: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
      monthNamesShort: ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.']
    });
	
		$.validator.addMethod( "captcha", function( value, element ) {
			var captcha = $("#captcha").val(); 
			return this.optional( element ) || value==captcha;
		}, "รหัสตรวจสอบความปลอดภัย ไม่ถูกต้อง" );

		$(function(){

      $("#province_id").click(function(event) {
        var select = $("#district_id");
        var options = select.attr('options');
        $('option', select).remove();
        $(select).append('<option value="">---- เลือก ----</option>');
      });      
      $("#receipt_province_id").click(function(event) {
        var select = $("#receipt_district_id");
        var options = select.attr('options');
        $('option', select).remove();
        $(select).append('<option value="">---- เลือก ----</option>');
      });
      $("#same_addr").click(function() {
          if($(this).is(":checked")){
          }else{
            checkboxaddress_reset();
          }
      });      
      $("input[name=receipttype_id]").change(function() {
          $("#taxno").val('');
          $("#branch").val('');
          checkboxaddress_reset();
      });
	  
	  if ($("input[name=receipttype_id]").val() == '5')
	  {
		  //$("#receipttype_text").removeAttr('disabled');
	  }
	  else
	  {
		  //$("#receipttype_text").attr('disabled','disabled');
	  }
	  
	  
	  if ($("input[name=orgtype_id]").val() == '5')
	  {
		  $("#orgtype_text").removeAttr('disabled');
	  }
	  else
	  {
		  $("#orgtype_text").attr('disabled','disabled');
	  }
	  
	   
	  if ($("#select_university option:selected").val() != '0-0')
	  {
		   $("#grd_other").attr('disabled','disabled');
	  }
	  else
	  {
		  $("#grd_other").removeAttr('disabled');
	  }
	  
			$("#title_th").change(function(){
				if($(this).val() == 'นาย'){ 
				  	$("#title_en").val("Mr");
					$("#title_th_text").hide();
					$("#title_en_text").hide();
				  	if($("#title_th_text").hasClass('required'))  $("#title_th_text").removeClass('required');
					if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
					$("#gender").val("M");
				}
				else if($(this).val() == 'นาง'){ 
				  	$("#title_en").val("Mrs");
					$("#title_th_text").hide();
					$("#title_en_text").hide();
				  	if($("#title_th_text").hasClass('required'))  $("#title_th_text").removeClass('required');
					if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
					$("#gender").val("F");
				}
				else if($(this).val() == 'นางสาว'){ 
				    $("#title_en").val("Ms");
					$("#title_th_text").hide();
					$("#title_en_text").hide();
					if($("#title_th_text").hasClass('required'))  $("#title_th_text").removeClass('required');
					if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
					$("#gender").val("F");
				}
				else if($(this).val() == 'อื่นๆ'){ 
				 	$("#title_en").val("Other");
					$("#title_th_text").show();
					$("#title_en_text").show();
	            	$("#title_th_text").addClass('required');
					$("#title_en_text").addClass('required');
					$("#gender").val("");
	            }
	        });

      $("#title_en").change(function(){
        if($(this).val() == 'Mr'){ 
          //$("#title_th").val("นาย");
          //$("#title_th_text").hide();
          $("#title_en_text").hide();
          //if($("#title_th_text").hasClass('required'))  $("#title_th_text").removeClass('required');
          if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
          $("#gender").val("M");
        }
        else if($(this).val() == 'Mrs'){ 
          //$("#title_th").val("นาง");
          //$("#title_th_text").hide();
          $("#title_en_text").hide();
          //if($("#title_th_text").hasClass('required'))  $("#title_th_text").removeClass('required');
          if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
          $("#gender").val("F");
        }
        else if($(this).val() == 'Ms'){ 
          //$("#title_th").val("นางสาว");
          //$("#title_th_text").hide();
          $("#title_en_text").hide();
        //  if($("#title_th_text").hasClass('required'))  $("#title_th_text").removeClass('required');
          if($("#title_en_text").hasClass('required'))  $("#title_en_text").removeClass('required');
          $("#gender").val("F");
        }
        else if($(this).val() == 'Other'){ 
         // $("#title_th").val("อื่นๆ");
          //$("#title_th_text").show();
          $("#title_en_text").show();
         //$("#title_th_text").addClass('required');
          $("#title_en_text").addClass('required');
          $("#gender").val("");
              }
          });
			
			$("#receipt_title").change(function(){
				if($(this).val() == 'นาย'){ 
					$("#receipt_title_text").hide();
				  	if($("#receipt_title_text").hasClass('required'))  $("#receipt_title_text").removeClass('required');
				}
				else if($(this).val() == 'นาง'){ 
					$("#receipt_title_text").hide();
				  	if($("#receipt_title_text").hasClass('required'))  $("#receipt_title_text").removeClass('required');
				}
				else if($(this).val() == 'นางสาว'){ 
					$("#receipt_title_text").hide();
				  	if($("#receipt_title_text").hasClass('required'))  $("#receipt_title_text").removeClass('required');
				}
				else if($(this).val() == 'อื่นๆ'){ 
					$("#receipt_title_text").show();
	            	$("#receipt_title_text").addClass('required');
	            }
	        });

	       // $("input[type=radio][name='slip_type']").change(function(){   
//	            if($(this).val() == 'corparation'){ 
//	            	$("#taxno").addClass('required');
//	            }else{ 
//	            	if($("#taxno").hasClass('required'))  $("#taxno").removeClass('required');
//	            }
//	        });	 
			   
	         $("input[type=radio][name='receipttype_id']").change(function(){   
	            if($(this).val() == '5'){ 
	            	$("#receipttype_text").addClass('required');
	            }else{ 
	            	if($("#receipttype_text").hasClass('required'))  $("#receipttype_text").removeClass('required');
	            }
	        });
			
			 $("input[type=radio][name='orgtype_id']").change(function(){   
	            if($(this).val() == '5'){ 
	            	//$("#orgtype_text").addClass('required');
	            }else{ 
	            	//if($("#orgtype_text").hasClass('required'))  $("#orgtype_text").removeClass('required');
                 $("#orgtype_text").val('');
	            }
	        });

          if($("input[type=radio][name='orgtype_id']:checked").val() == 5)
          {
            $("#orgtype_text").addClass('required');
            $("#table_Org_Name").hide();
            $(".table_Org_Name").hide();
            $("#orgtype_text").removeAttr('disabled');
          }
          else
          {
            $("#orgtype_text").removeClass('required');
            $("#orgtype_text").removeClass('error');
            $("#orgtype_text-error").hide();
            $("#table_Org_Name").show();
            $(".table_Org_Name").show();
            $("#orgtype_text").attr('disabled','disabled');
          }	
		});
	});

	function getInfo(id){
		if(typeof id=="undefined") return;
		var url = "data/member-info.php";
		var param = "id="+id;
		$.ajax( {
			"dataType":'json', 
			"type": "POST",
			"async": false, 
			"url": url,
			"data": param, 
			"success": function(data){	
				$.each(data, function(index, array){			 				 	
					
        //console.log(array);  

					var t = array.slip_type;
					$("#"+t).trigger('click');
					if(t=='corparation'){
						select_normal_niti('niti');
					}	
	
					$("#receipt_province_id").val(array.receipt_province_id);
					 
					var select = $("#receipt_amphur_id");
					$('option', select).remove();
					$(select).append('<option value="'+array.receipt_amphur_id+'"> '+array.receipt_amphur_name+' </option>');	
					var select = $("#receipt_district_id");
					$('option', select).remove();
					$(select).append('<option value="'+array.receipt_district_id+'"> '+array.receipt_district_name+' </option>');				
					var select = $("#amphur_id");
					$('option', select).remove();
					$(select).append('<option value="'+array.amphur_id+'"> '+array.amphur_name+' </option>');	
					var select = $("#district_id");
					$('option', select).remove();
					$(select).append('<option value="'+array.district_id+'"> '+array.district_name+' </option>');
					var select = $("#receipt_id");
					$('option', select).remove();
					$(select).append('<option value="'+array.receipt_id+'"> '+array.receipt_name+' </option>');
	        setVal("#form_m", array);
          if(!array.org_name_text){
              array.org_name_text = '-';
          }
          var select = $("#org_name");
          $('option', select).remove();
          $(select).append('<option value="'+array.org_name+'"> '+array.org_name_text+' </option>');
          
          if($("#title_th").val() == 'อื่นๆ'){ 
            $("#title_th_text").show();
            $("#title_th_text").addClass('required');
          }          
          if($("#title_en").val() == 'Other'){ 
            $("#title_en_text").show();
            $("#title_en_text").addClass('required');
          }
          if($("#receipt_title").val() == 'อื่นๆ'){ 
            $("#receipt_title_text").show();
            $("#receipt_title_text").addClass('required');
          }


          //console.log(data);
          if (array.receipttype_id==5) {
             $("#trReceiptInfo").hide();
             $("#receipttype_text").val(array.receipttype_text);
             $("#receipttype_text").removeAttr('disabled');
          }


				});				 
			}
		});
/*
    var private = "<?php echo $private; ?>";
    console.log(private);
    if( private ){
      
    }
*/
	}
	
	function receipt_info(id){
		if(typeof id=="undefined") return;
		var url = "data/receipt.php";
		var param = "name=receipt_detail&value="+id;
		$.ajax( {
			"dataType":'json', 
			"type": "GET",
			"async": false, 
			"url": url,
			"data": param, 
			"success": function(data){
				//console.log(data);	
				 $.each(data, function(index, array){			 				 	
            $("#taxno").val(array.taxno);
            $("#branch").val(array.branch);
            $("#receipt_no").val(array.no);
  					$("#receipt_gno").val(array.gno);
  					$("#receipt_moo").val(array.moo);
  					$("#receipt_soi").val(array.soi);
  					$("#receipt_road").val(array.road);
  					$("#receipt_postcode").val(array.postcode);
  					$("select[name=receipt_province_id] option").filter(function() {
  						return $(this).val() == array.province_id; 
  					}).attr('selected', true);		
  					var select = $("#receipt_amphur_id");
  					$('option', select).remove();
  					$(select).append('<option value="'+array.amphur_id+'"> '+array.amphur_name+' </option>');	
  					var select = $("#receipt_district_id");
  					$('option', select).remove();
  					$(select).append('<option value="'+array.district_id+'"> '+array.district_name+' </option>');

				  });				 
			}

		});
	}
	
  function enterToSubmit(){
    $(document).keypress(function(event) {
      var keycode = (event.keyCode ? event.keyCode : event.which);
      if(keycode == '13') {
        $("#form_m").submit();    
      }
    });
  }
  function chFrmSubmit(){
		$("#form_m").submit();    
	}

	function display_element(str, show){
		if(typeof str=="undefined") return false;
		var show = (typeof show!="undefined") ? true : false;
		var data = str.split(', ');
		for(var i=0;i<data.length;i++){
			if(show) $("#"+data[i]).show();
			else $("#"+data[i]).hide();
		}
	}
	
	function receipt_other(){
		var select = $("#receipt_id");
		var options = select.attr('options');
		$('option', select).remove();
		$(select).append('<option value="">---- เลือก ----</option>');
		$("#trReceiptInfo").hide();
	}
	
	function trReceiptInfoShow()
	{
		$("#trReceiptInfo").show();
	}
	
  
  function select_normal_niti(select_in){
    $("#receipt_detail").show();
    
    var ele = "typeCom1, typeCom2, typeCom3, typeCom4, typeCom5, typeCom6, tax_text, tax_input, select_menu_typeCom";
    if(select_in == "normal"){      
      $("#receipttype_text").val('');
      $("#taxno").val($("#cid").val());
      //$("#taxno").attr('disabled',true);
      $(".pro4-colum1-r1 input").attr('disabled',true);
      $(".pro4-colum1-r1 input").removeAttr('checked');     
      $(".pro4-colum1-r1").hide();
      $("#same_addr").prop( "checked", true );
      
      $("#receipt_detail_title").hide();
      //$("#receipt_taxno").hide();

      $("#same_address").show();
      $("#trSlipName").show();
      var select = $("#receipt_id");
      var options = select.attr('options');
      $('option', select).remove();
      $(select).append('<option value="">---- เลือก ----</option>');    
      checkboxaddress();
	  $("#same_address_checkbox").show();
      
    }else if(select_in == "niti"){
      $("#taxno").val('');
      $("#branch").val('');
      $(".pro4-colum1-r1 input").attr('disabled',false);
      //$("#taxno").attr('disabled',false);     
      $(".pro4-colum1-r1").show();
      $("#same_addr").prop( "checked", false );
      
      $("#receipt_detail_title").show();
      //$("#receipt_taxno").show();
      $("#trSlipName").hide();
	  $("#same_address_checkbox").hide();
      checkboxaddress_reset();
       receipttype_text_required("no");
    }else{
      $("#receipttype_text").val('');
      $("#taxno").val('');
      $("#branch").val('');
      //$("#taxno").attr('disabled',true);
      $(".pro4-colum1-r1 input").attr('disabled',true);
      $(".pro4-colum1-r1 input").removeAttr('checked');
      $(".pro4-colum1-r1").hide();
      $("#same_addr").prop( "checked", true );
      $("#receipt_detail_title").hide();
      //$("#receipt_taxno").hide();
      $("#same_address").show();
      $("#trSlipName").show();
	  $("#same_address_checkbox").show();
      checkboxaddress();

    }
  }
  function checkboxaddress(){
    
    if($("#same_addr").is(':checked'))
    {
      $("#receipt_title").val($("#title_th").val());
      if($("#receipt_title").val() == 'อื่นๆ'){ 
        $("#receipt_title_text").show();
        $("#receipt_title_text").addClass('required');
        $("#receipt_title_text").val($("#title_th_text").val());
      }
      else
      {
        $("#receipt_title_text").hide();
        $("#receipt_title_text").removeClass('required');
        $("#receipt_title_text").val("");
      }
      $("#receipt_fname").val($("#fname_th").val());
      $("#receipt_lname").val($("#lname_th").val());    
      
      $("#taxno").val($("#cid").val());
      $("#receipt_no").val($("#no").val());
      $("#receipt_gno").val($("#gno").val());
      $("#receipt_moo").val($("#moo").val());
      $("#receipt_soi").val($("#soi").val());
      $("#receipt_road").val($("#road").val());
      
      $('#receipt_province_id').val($('#province_id').val());
      
      var select = $("#receipt_amphur_id");
      $('option', select).remove();
      $(select).append('<option value="'+$("#amphur_id").val()+'"> '+ $("#amphur_id").find(":selected").text()+' </option>'); 
      
      var select = $("#receipt_district_id");
      $('option', select).remove();
      $(select).append('<option value="'+$("#district_id").val()+'"> '+$("#district_id").find(":selected").text()+' </option>');  
          
      $("#receipt_postcode").val($("#postcode").val());
    }
  }
  
  function checkboxaddress_reset()
  {
    //$("#same_address").hide();
    
     $("#receipt_title").val("");
     $("#receipt_title_text").hide();
        $("#receipt_title_text").removeClass('required');
        $("#receipt_title_text").val("");
      $("#receipt_fname").val("");
      $("#receipt_lname").val("");    
      
      $("#taxno").val("");
      $("#receipt_no").val("");
      $("#receipt_gno").val("");
      $("#receipt_moo").val("");
      $("#receipt_soi").val("");
      $("#receipt_road").val("");
      
      $('#receipt_province_id').val("");
      
      var select = $("#receipt_amphur_id");
      $('option', select).remove();
      $(select).append('<option value="">---- เลือก ----</option>');
      
      var select = $("#receipt_district_id");
      $('option', select).remove();
      $(select).append('<option value="">---- เลือก ----</option>');
          
      $("#receipt_postcode").val("");
  }

	function orgtype_other()
	{
		var select = $("#org_name");
		var options = select.attr('options');
		$('option', select).remove();
		$(select).append('<option value="">---- เลือก ----</option>');
	}
	
	function change_select_university()
	{
		 var id = $("#select_university").val().split("-");
		 $("#grd_ugrp1").val(id[0]);
		 $("#grd_uid1").val(id[1]);
		 $("#grd_uname1").val($("#select_university option:selected").text());       
		    
	  if ($("#select_university option:selected").val() != '0-0')
	  {
		   $("#grd_other").attr('disabled','disabled');
		   $("#grd_other").val("");
	  }
	  else
	  {
		  $("#grd_other").removeAttr('disabled');
	  }
	}
	
	function orgtype_text_required(value)
	{
		if(value == 'yes')
		{
			$("#orgtype_text").addClass('required');
      $("#table_Org_Name").hide();
			$(".table_Org_Name").hide();
			$("#orgtype_text").removeAttr('disabled');
		}
		else
		{
			$("#orgtype_text").val("");
      $("#orgtype_text").removeClass('required');
			$("#orgtype_text").removeClass('error');
			$("#orgtype_text-error").hide();
      $("#table_Org_Name").show();
			$(".table_Org_Name").show();
			$("#orgtype_text").attr('disabled','disabled');
		}
	}
	
	function receipttype_text_required(value)
	{
		if(value == 'yes')
		{
			$("#receipttype_text").addClass('required');
			$("#receipttype_text").removeAttr('disabled');
		}
		else
		{
			$("#receipttype_text").val("");
			$("#receipttype_text").removeAttr('class');
			$("#receipttype_text-error").hide();
			$("#receipttype_text").attr('disabled','disabled');
		}
	}

  function private_toggle(){
    //$('#private').is(":checked");    
    var ans = ( $("#private").is(':checked') ) ? 1 : 0;
    $("#private").val(ans);
    //alert(ans);
  }
	
  function view_img(){
    $(".fancybox").fancybox({});
  }

</script>
