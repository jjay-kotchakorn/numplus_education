<?php
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";
include_once "./share/course.php";
global $db;
global $EMPID;
$date_now = date('Y-m-d H:i:s');

if (!session_id()) {//initialize the session
  session_start();
}
global $thai_month_arr;
$sec_id = $_POST['sec_id'];
$sec_action = $_POST['sec_action'];
$type_id = map_course_type($sec_action);
$q = "SELECT name FROM `coursetype` WHERE coursetype_id=$type_id";
$name_type = $db->data($q);
$my_select_course = $_POST['my_select_course'];
// $my_select_course = "1128,1126,1125";
$my_select_course_detail = $_POST['my_select_course_detail'];
$format = get_select_course($sec_id, $sec_action);
$con = " and a.course_id in ($my_select_course)";
$r = get_course($con);
$title = "";
$parent_id = 0;
$single_discount = 0;
$price = 0;
$today = date("Y-m-d 00:00:00");
$promotion = 0;
$not_discount = false;
$parent_promotion = 0;

// d($q);
// d($_GET);
// d($r);
// die();


//member data
$member_info = $_SESSION["login"]["info"];
$member_id = $member_info['member_id'];
$test_id = $_SESSION["last_register_login"][$member_id];

$date_ref1 = "";
foreach ($r as $key => $row) {
	$course_id = $row['course_id'];
	$section_id = $row['section_id'];
/*	$date_start = $row["date_start"];
	$date_stop = $row["date_stop"];
	if($date_start!="" && $date_stop!="" && $today>=$date_start && $today<=$date_stop){
		$promotion += $row["discount"];
	}else{
		$single_discount += $row["discount"];
	}*/
	$price += $row['price'];
	$parent_id = (int)$row['parent_id'];
}
if($format==2 && $parent_id>0){
    $p_con = " and a.course_id = $parent_id";
    $p_r = get_course($p_con);
	$parent_price = $p_r[0]['price'];

/*start*/
	$date_start = $p_r[0]["date_start"];
	$date_stop = $p_r[0]["date_stop"];
	if($date_start!="" && $date_stop!="" && $today>=$date_start && $today<=$date_stop){
		$parent_promotion += $p_r[0]["discount"];
	}

/*end*/    
    if($price > $parent_price && $parent_price>0){
		$price = $parent_price;
		$not_discount = true;
    }
}//end loop


$list = explode(",", $_POST['my_select_course']);
$list_detail = explode(",", trim($_POST['my_select_course_detail'], ","));
$details_id = "";
$details = "";
if($format==3){
 	foreach ($list_detail as $key => $value) {
 		$c = explode(":", $value);
 		$details_id[$c[0]] .= ",".$c[1];
 		$details .= ",".$c[1];
 	}
	$details = trim($details, ",");
	$con = " and a.active='T' and  a.course_id in ($my_select_course)";
	$r = get_course($con);
	$title = "";
	$no = 1;
	foreach ($r as $key => $course) {
		$course_id = $course['course_id'];
		$id = trim($details_id[$course_id], ",");
		if($id){	    
			$con = " and a.active='T' and a.course_detail_id IN ($id) ";
			$res = get_course_detail($con);
			$msg_data .= "<br/>";
	    	
			$q = "select title from course where active='T' and course_id=$course_id";
			$name = $db->data($q);	    	
	    	if($res){
	    		
				$msg_data .= "
					<tr class='tr-header-fa'>
						<td class='td-col-1' colspan='10'>{$name}</td>
					</tr> 
				";  	    		
				foreach ($res as  $row) {
		          $course_detail_id = $row['course_detail_id'];
		          $course_id = $row['course_id'];
		  
		          $con_c = " and a.active='T'  and a.course_id = $course_id";
		          $r_c = get_course($con_c);
		          $title = $r_c[0]['title'];

		          $day = $row['day'];
		          if(!$date_ref1) $date_ref1 = $row["date"];
		          $date = revert_date($row['date']);
		          $time = $row['time'];
		          $address = $row['adress'];
				$date_other = trim($row["date_other"]);
				$pr = "";
				if($date_other!="") $date .= " <hr>".$date_other; 
				$address_detail = $pr.$row['address_detail'];

				$get_all = (!$test_id) ? 1 : 0;
				$not_id = "";
				
				$use = $row["sit_all"];
				//$use = get_use_chair($course_detail_id);				 
				$chair_all = $row['chair_all'];
				$sit_all = $chair_all - ($use + $row["book"]+$get_all);
		          $status = $row['status'];
		          $open_regis = $row['open_regis'];
		          $end_regis = $row['end_regis'];
		          $datetime_add = $row['datetime_add'];
		          $datetime_update = $row['datetime_update'];
		                $date_start = $row["date_start"];
				$date_stop = $row["date_stop"];
				if($date_start!="" && $date_stop!="" && $today>=$date_start && $today<=$date_stop){
					$promotion += $row["discount"];
				}else{
					$single_discount += $row["discount"];
				}
				$price += $row['price'];
					if($tr_current_bg == "tr-bg-white"){
						$tr_current_bg = "tr-bg-silver";
					} else if($tr_current_bg == "tr-bg-silver"){
						$tr_current_bg = "tr-bg-white";
					}
		            else {
		            	$tr_current_bg = "tr-bg-silver"; 
		            }
		            
		            $msg_data .= "<tr class='$tr_current_bg tr-item-fa'>
		          				  <td class='td-col-0'> </td>
		                          <td class='td-col-1'>$title</td>
	                      				<td class='td-col-2'>
										 <span class='span-day'>$day.</span>
										 <span class='span-date'>$date</span>
									</td>
									<td class='td-col-3'>$time</td>
									<td class='td-col-4'>$address</td>
									<td class='td-col-5'>$address_detail</td>
									<td class='td-col-6'>$sit_all</td>
									<td class='td-col-7'>$chair_all</td>
									<td class='td-col-8'><center>$status</center></td>
									<td class='td-col-9'><center>$open_regis</center></td>
									<td class='td-col-10'><center>$end_regis</center></td>
		                      </tr> ";		                      
		      	}
	    	}
		}
		$no++;
	}

}else if($format==2){
 	foreach ($list_detail as $key => $value) {
 		$c = explode(":", $value);
 		$details_id[$c[0]] .= ",".$c[1];
 		$details .= ",".$c[1];
 	}
	$details = trim($details, ",");
	$con = " and a.active='T' and a.course_id in ($my_select_course)";
	$r = get_course($con);
	$title = "";
	$no = 1;
	foreach ($r as $key => $row) {
		$course_id = $row['course_id'];
		$parent_title = $row['title'];
		$id = trim($details_id[$course_id], ",");
		if($id){	    
			$con = " and a.active='T' and a.course_detail_id in ($id) ";
			$res = get_course_detail($con);
			$msg_data .= "<br/>";	    	
			$q = "select title from course where active='T' and course_id=$course_id";
			$name = $db->data($q);	    	
	    	if($res){
	    		
				$msg_data .= "
					<tr class='tr-header-fa'>
						<td class='td-col-1' colspan='10'>{$name}</td>
					</tr> 
				";  	    			    		
				foreach ($res as  $row) {
		          $course_detail_id = $row['course_detail_id'];
		          $course_id = $row['course_id'];
		  
		          $con_c = " and a.active='T' and a.course_id = $course_id";
		          $r_c = get_course($con_c);
		          $title = $r_c[0]['title'];

				$day = $row['day'];
				if(!$date_ref1) $date_ref1 = $row["date"];
				$date = revert_date($row['date']);
				$time = $row['time'];
				$address = $row['address'];
				$date_other = trim($row["date_other"]);
				$pr = "";
				if($date_other!="") $date .= " <hr>".$date_other; 
				$address_detail = $pr.$row['address_detail'];
				$get_all = (!$test_id) ? 1 : 0;
				$not_id = "";
				
				$use = $row["sit_all"];
				//$use = get_use_chair($course_detail_id);				 
				$chair_all = $row['chair_all'];
				$sit_all = $chair_all - ($use + $row["book"]+$get_all);

				$status = $row['status'];
				$open_regis = revert_date($row['open_regis']);
				$end_regis = revert_date($row['end_regis']);
		          $datetime_add = $row['datetime_add'];
		          $datetime_update = $row['datetime_update'];
		                $date_start = $row["date_start"];
				$date_stop = $row["date_stop"];
				if($date_start!="" && $date_stop!="" && $today>=$date_start && $today<=$date_stop){
					$promotion += $row["discount"];
				}else{
					$single_discount += $row["discount"];
				}
				$price += $row['price'];
					if($tr_current_bg == "tr-bg-white"){
						$tr_current_bg = "tr-bg-silver";
					} else if($tr_current_bg == "tr-bg-silver"){
						$tr_current_bg = "tr-bg-white";
					}
		            else {
		            	$tr_current_bg = "tr-bg-silver"; 
		            }
		            
		            
					$msg_data .= "
						<tr class='$tr_current_bg tr-item-fa'>
								 <td class='td-col-0'> </td>											
								<td class='td-col-2'>
									 <span class='span-day'>$day.</span>
									 <span class='span-date'>$date</span>
								</td>
								<td class='td-col-3'>$time</td>
								<td class='td-col-4'>$address</td>
								<td class='td-col-5'>$address_detail</td>
								<td class='td-col-6'>$sit_all</td>
								<td class='td-col-7'>$chair_all</td>
								<td class='td-col-8'><center>$status</center></td>
								<td class='td-col-9'><center>$open_regis</center></td>
								<td class='td-col-10'><center>$end_regis</center></td>
						</tr> ";
		       					
		                    
		      	}
			$no++; 
	    	}
		}
	}
}else{
		$con = " and a.active='T' and a.course_id in ($my_select_course)";
		$r = get_course($con);
		$title = "";
		//where and time ...my_select_course_detail

		$parent_con = " and a.active='T' and a.course_id = $parent_id";
		$parent_rec = get_course($parent_con);
		$parent_title = $parent_rec[0]['title'];
		foreach ($r as $key => $row) {
			$course_id = $row['course_id'];
		    $parent_id = $row['parent_id'];
			$section_id = $row['section_id'];
			$code = $row['code'];
			$title = $row['title'];
			$set_time = $row['set_time'];
			$life_time = $row['life_time'];
			$price = $row['price'];
			$rereg_date = $row['rereg_date'];
			$status = $row['status'];	
		}
		
	    $row = get_course_detail(" and a.course_detail_id={$my_select_course_detail}");
	    if($row) $row = $row[0];
	    $course_detail_id = $row['course_detail_id'];
	    $course_id = $row['course_id'];
	    $day = $row['day'];
	    if(!$date_ref1) $date_ref1 = $row["date"];
	    $date = revert_date($row['date']);
	    $time = $row['time'];
	    $address = $row['address'];
				$date_other = trim($row["date_other"]);
				$pr = "";
				if($date_other!="") $date .= " <hr>".$date_other; 
				$address_detail = $pr.$row['address_detail'];
				$get_all = (!$test_id) ? 1 : 0;
				$not_id = "";
				
				$use = $row["sit_all"];
				//$use = get_use_chair($course_detail_id);				 
				$chair_all = $row['chair_all'];
				$sit_all = $chair_all - ($use + $row["book"]+$get_all);
				
	    $status = $row['status'];
	    $open_regis = $row['open_regis'];
	    $end_regis = $row['end_regis'];
	    $datetime_add = $row['datetime_add'];
	    $datetime_update = $row['datetime_update'];
                $date_start = $row["date_start"];
		$date_stop = $row["date_stop"];
		if($date_start!="" && $date_stop!="" && $today>=$date_start && $today<=$date_stop){
			$promotion += $row["discount"];
		}else{
			$single_discount += $row["discount"];
		}
		$price += $row['price'];
}      
$discount = 0;
$xx = false;
if($parent_id>0 && $xx=="open"){
	$t = explode(",", trim($my_select_course,","));
	$count = count($t);
	$q = "select a.discount
		  from discount a 
		  where parent_id=$parent_id and use_course={$count}" ;
	$dc = $db->data($q);
	if($dc){
		$discount = $dc;
	}
}else{
	if($not_discount){
		$discount = $parent_promotion;
	}else{
		$discount = $single_discount + $promotion;

	}
} 


$ch_register_course = $_POST['ch_register_course'];
$slip_type_text = "";
if($_POST["slip_type"]=="individuals") $slip_type_text = "ออกในนามบุคคลธรรมดา";
if($_POST["slip_type"]=="corparation") $slip_type_text = "ออกในนามนิติบุคคล (สำหรับเบิกค่าใช้จ่าย)";
$q = "select name from receipttype where receipttype_id={$_POST["receipttype_id"]}";
$receipttype_name = $db->data($q);
$q = "select name from district where district_id={$_POST["receipt_district_id"]}";
$district_name = $db->data($q);
$q = "select name from amphur where amphur_id={$_POST["receipt_amphur_id"]}";
$amphur_name = $db->data($q);
$q = "select name from province where province_id={$_POST["receipt_province_id"]}";
$province_name = $db->data($q);

$course_id = $_POST["my_select_course"];
$sec_id = $_POST["sec_id"];
if($format==3 || $format==2){
		$q = " select a.register_id from register_course_detail a inner join register b on a.register_id=b.register_id where b.active='T' and a.course_detail_id=$course_detail_id and  b.member_id = '$member_id' and b.pay_status=1";
		$id = $db->data($q);
}else{
	$q = "select register_id from register a where a.active='T' and  a.course_detail_id='$course_detail_id' and a.member_id = '$member_id' and a.pay_status=1";
	$id = $db->data($q);
}

if(!$id){
	$_POST["receipt_title"] = ($_POST["receipt_title"]=="อื่นๆ") ? $_POST["receipt_title_text"] : $_POST["receipt_title"];
	$args["table"] = "register";
	$args["member_id"] = $member_id;
	$args["cid"] = $_POST["username"];
	$args["title"] = $_POST["title_th"];
	if($args["title"]=="อื่นๆ"){
		$t = $db->data("select title_th_text from member where member_id={$member_id}"); 
		if ($t) {
			$args["title"] = $t;
		}else{
			$args["title"] = "คุณ";
		}		
	}
	$args["fname"] = $_POST["fname_th"];
	$args["lname"] = $_POST["lname_th"];
	$args["slip_type"] = $_POST["slip_type"];
	$args["date"] = date("Y-m-d H:i:s");
	$args["status"] = 6;
	$args["pay_status"] = 6;
	$args["pay"] = 'free';
	$args["pay_date"] = $date_now;
	$args["receipttype_id"] =  (int)$_POST["receipttype_id"];
	$args["receipttype_text"] = $_POST["receipttype_text"];
	$args["receipt_id"] = (int)$_POST["receipt_id"];
	$args["taxno"] = $_POST["taxno"];
	$args["receipt_title"] = $_POST["receipt_title"];
	$args["receipt_fname"] = $_POST["receipt_fname"];
	$args["receipt_lname"] = $_POST["receipt_lname"];
	$args["receipt_no"] = $_POST["receipt_no"];
	$args["receipt_gno"] = $_POST["receipt_gno"];
	$args["receipt_moo"] = $_POST["receipt_moo"];
	$args["receipt_soi"] = $_POST["receipt_soi"];
	$args["receipt_road"] = $_POST["receipt_road"];
	$args["receipt_province_id"] = (int)$_POST["receipt_province_id"];
	$args["receipt_amphur_id"] = (int)$_POST["receipt_amphur_id"];
	$args["receipt_district_id"] = (int)$_POST["receipt_district_id"];
	$args["receipt_postcode"] = $_POST["receipt_postcode"];
	$args["branch"] = $_POST["branch"];
	$args["course_id"] = $_POST["my_select_course"];
	$args["course_detail_id"] = $_POST["my_select_course_detail"];
	$args["course_price"] = $price;
	// $args["course_discount"] = (int)$discount;
	$args["course_discount"] = 0;
	$args["section_id"] = (int)$sec_id;
	$args["coursetype_id"] = (int)$type_id;
	$args["auto_del"] = "F";
	$args["rectime"] = date("Y-m-d H:i:s");
	$id = $register_id = $db->set($args);
	$_SESSION["last_register_login"][$member_id] = $id;
	if($register_id && ($format==3 || $format==2)){
		$course_detail = array();
		foreach ($list_detail as $key => $value) {
	 		$c = explode(":", $value);
	 		$course_detail[$c[0]][]  = $c[1];
	 	}
	 	foreach ($course_detail as $key => $value) {
	 		$args = array();
			$args["table"] = "register_course";	
			$args["course_id"] = (int)$key;
			$args["register_id"] = (int)$register_id;
			$args["recby_id"] = (int)$EMPID;
			$args["rectime"] = date("Y-m-d H:i:s");
			$db->set($args);
	 		foreach ($value as $k => $v) {
				$args = array();
				$args["table"] = "register_course_detail";	
				$args["course_id"] = (int)$key;
				$args["register_id"] = (int)$register_id;
				$args["course_detail_id"] = (int)$v;
				$args["recby_id"] = (int)$EMPID;
				$args["rectime"] = date("Y-m-d H:i:s");
				$db->set($args);
	 		}
	 	}
	}

}//end if
?>

<script type="text/javascript">
function redirect_url(){
	// alert('555555');
	window.open('index.php?p=register-choice&sec_id=<?php echo $sec_id; ?>&sec_action=<?php echo $sec_action; ?>&msg=success','_self');
}

function register_sendmails(pay_type){
	var url = "data/register-sendmails.php";
	var id = "<?php echo $id; ?>";
	var param = "pay="+pay_type+"&register_id="+id+"&section_id=<?php echo $section_id; ?>&sec_action=<?php echo $sec_action; ?>";
	$.ajax( {
		"dataType":'json', 
		"type": "POST",
		"async": true, 
		"url": url,
		"data": param, 
		"success": function(data){
		}	
	});

	$.ajax({
		"type": "POST",
		"async": false, 
		"url": "./backoffice/check-course-elearning.php",
		"data": {'register_id': id, 'pay_status': 6}, 
		"success": function(data){	
			//console.log(data);			      							 
		}
	});
}//end func		
</script>

<?php
echo "<script type=\"text/javascript\">
	// alert('5555');
	register_sendmails('free');
	redirect_url();
</script>";

die();
//////////////////////////////////////////////////////////////////////////////////

$q = "select pay from register where register_id={$id}";
$pay = $db->data($q);

$total_price = $price - $discount;

if($id){

	$q = "select ref1 from register where register_id=$id and active='T'";
	$r = $db->data($q);
	//if(!$r){
	if( !isset($r) || empty($r) ){

		//chk default expire_date / chk config expire_date to pay-update.php
		$t = convert_mktime(date("Y-m-d H:i:s")) + (2 * 24 * 60 * 60);
	    $pay_date = date("d", $t);
	    $section_id = (int)$sec_id;
	    $coursetype_id = (int)$type_id;
	    $y = date("y")+43;
	    $q = "select count(register_id) from register where active='T' and section_id={$section_id} and coursetype_id={$coursetype_id} and runyear='$y'";
	    //$runNo = $db->data($q);
	    $runNo = substr($id, -4);
	    $x = explode("-", $date_ref1);
	    $course_detail_date = date("y", strtotime($date_ref1));
	    $course_detail_day = $x[2];
	    $course_detail_month = $x[1];
	    $convert_type = "";
		if($section_id!=3){
		    if($section_id==1){
		    	if($coursetype_id==4) $convert_type = 3;
		    	else if($coursetype_id==3) $convert_type = 4;
		    	else $convert_type = $coursetype_id;
		    }else if($section_id==2){
		    	if($coursetype_id==1) $convert_type = 4;
		    	else $convert_type = $coursetype_id - 1;
		    }

		    $digit_ref1 = (($course_detail_date)+43).$course_detail_month.$course_detail_day.$section_id.$convert_type. sprintf("%04d", $runNo)."N";
		    $digit_ref2 = (date("y")+43).date("md"). sprintf("%05d", $total_price)."N";
		   //echo $digit_ref1;
		    /*
			$r = "581012110101N";
			$rs = gen_digit($r);
			$x = check_digit($rs);
			var_dump($x);*/
		 	$ref1 = gen_digit($digit_ref1);
		 	$ref2 = gen_digit2($digit_ref2);
/*		 	echo $ref1;
		 	echo "<hr>";
		 	echo $ref2;*/
		 	$args = array();
		 	$args["table"] = "register";
		 	$args['id'] = array('key'=>'register_id', 'value'=>$id);
		 	$args["ref1"] = $ref1;
		 	$args["ref2"] = $ref2;
		 	$args["runyear"] = $y;		 	
		 	$args["expire_date"] = date("Y-m-d", $t);
		 	$db->set($args);

		}
	}

}
/*
	//write log
	$str = $id."|r=".$r."|";
	// $str .= $chk_if_1."|".$chk_if_2."|".$chk_if_3."|";
	$str .= "ref1=".$ref1."|ref2=".$ref2;

	$date_now = date('Y-m-d H:i:s');
	$date_log = date('Y-m-d');
	$log_name = "./log/new-step-4_".$date_log.".log";
	$file = fopen($log_name, 'a');
	$str_txt = $date_now."|".$str."\r\n";
	fwrite($file, $str_txt);
	fclose($file);
*/
//title_show
if ($member_info["title_th"] == "อื่นๆ" || $member_info["title_en"] == "Other") {
	$_POST["title_th"] = $member_info["title_th_text"];
}

//print_link
if ($sec_id == '3') {
	$link_print = "invoice-ati";
}else{
	$link_print = "invoice";
}

?>
<div class="bgselect4" style="height:auto;padding-bottom:32px;">
		<div class="bodyselect4">
			<div class="imgstep1">
            <table id="Table_01" width="1248" height="52" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
		  <a href="#" onclick="goBackStepOne();"><img src="images/step04_01.jpg" width="302" height="52" alt=""></a></td>
		<td>
			<a href="#" onclick="goBackSteptwo();"><img src="images/step04_02.jpg" width="306" height="52" alt=""></a></td>
		<td>
			<a href="#" onclick="goBack();"><img src="images/step04_03.jpg" width="308" height="52" alt=""></a></td>
		<td>
			<img src="images/step04_04.jpg" width="332" height="52" alt=""></td>
	</tr>
</table>

<?php 

$q = "select data_value from config where name='payment_options_setting'";
$array_data = $db->data($q);
if(!$array_data){
	$array_data = array();
}else{
	$array_data = json_decode($array_data, true);
}
/*echo $sec_action;*/
/*echo "<pre>";
print_r($array_data[$section_id][$sec_action]);
echo "</pre>";
die();*/
$option_billpayment = $array_data[$section_id][$sec_action][2]["ck"];
$option_paysbuy = $array_data[$section_id][$sec_action][1]["ck"];
$option_ati = $array_data[$section_id][$sec_action][3]["ck"];
$option_asco = $array_data[$section_id][$sec_action][4]["ck"];
$item = 0;
foreach ($array_data[$section_id][$sec_action] as $k => $v) {
	if($v["ck"]=="T"){
		$item++;
	}
}

 ?>
            </div>
			<div class="se4-pay-area">
				<div class="se4-pay-title"><center><p>วิธีการชำระค่าธรรมเนียมการ<?php echo $name_type; ?></p></center></div>
				<div class="se4-pay-body">



					<div class="se4-pay-body1" <?php echo ($item==2) ? 'style="margin-left:217px;"' : ''; ?> <?php echo ($option_billpayment=="F") ? 'style="display:none;"' : ''; ?>>
						<div class="se4-pay-area1">
							<div class="se3-from-radio">
								<!-- <p><input type="radio" onclick="update_pay(this.value);" name="payment_type" value="bill_payment" id="payment_type1"> -->
							<p><input type="radio" onclick="update_pay('bill_payment');" name="payment_type" value="bill_payment" id="payment_type1">
						ชำระเงินผ่าน Bill-payment</p>
							</div>
						</div>
						<div class="se4-pay-picbank">
							<img src="./images/paybank.png">
						</div>
						<div class="se4-pay-text3">
							<center><p>เลือกชำระค่าธรรมเนียมได้ 3 ช่องทาง</p></center>
						</div>																	
						<div class="se4-pay-text3">
							<p>
								1) Teller Counter ทุกสาขาทั่วประเทศ โดยนำ Pay in Slip ไปแสดงเพื่อชำระ</br>
								2) ATM (<a href="https://register.ati-asco.org/pdf/Bill_Payment_ATM.pdf" title="" target="_blank">คู่มือของธนาคารกรุงเทพ</a>) (<a href="http://www.ati-asco.org/uploads/upfiles/files/BillPayment_kbank_ATM.pdf" title="" target="_blank">คู่มือของธนาคารกสิกรไทย</a>)</br>
								3) Internet Banking (<a href="http://www.ati-asco.org/uploads/upfiles/files/billpaymentibanking.pdf" title="" target="_blank">คู่มือของธนาคารกรุงเทพ</a>) (<a href="https://online.kasikornbankgroup.com/K-Online/preLogin/popupPreLogin.jsp?lang=th&type=" title="" target="_blank">คู่มือของธนาคารกสิกรไทย</a>)</br>
							</p>
						</div>
					</div>

					<div class="se4-pay-body1" <?php echo ($item==2) ? 'style="border-right:none;"' : ''; ?> <?php echo ($option_paysbuy=="F") ? 'style="display:none;"' : ''; ?>>
						<div class="se4-pay-area1">
							<div class="se3-from-radio">
								<p><input onclick="update_pay(this.value);" type="radio" name="payment_type" value="paysbuy" id="payment_type2">
					    			ชำระช่องทางอื่นๆ</p>
					    		</div>
				    		</div>
						<!-- <div class="se4-pay-picpaysbuy" style="margin-bottom: -15px;margin-top: 1px;">
							<img src="./images/paysbuy.png">
						</div> -->
						<div class="se4-pay-picpaysbuy" style="margin-bottom: -10px; margin-top: 2px;">
							<img src="./images/pay_new.jpg">
						</div>
                     	<div class="se4-pay-text2">
                     		<span class="red" style="text-align:center; font-size:13pt;">สำหรับช่องทางชำระแบบเงินสด ท่านสามารถ Print แบบฟอร์มชำระเงินย้อนหลังได้ที่ E-Mail ที่ส่งให้ท่านจาก Paysbuy เท่านั้น โปรดกรอก E-Mail ให้ถูกต้อง<span>
                     	</div>
					</div>
					<div class="se4-pay-body2" <?php echo ($option_ati=="F" && $option_asco=="F") ? 'style="display:none;"' : ''; ?> <?php echo ($section_id==3) ? 'style="float:none;margin:0 auto;"' : ''; ?>>
						<div class="se4-pay-area1">
							<div class="se3-from-radio">
								<p><input type="radio" onclick="update_pay(this.value);" name="payment_type" 
									value="<?php echo  ($option_asco=="T") ? "at_asco" : "at_ati" ?>" id="payment_type3"> 
									<?php echo ($option_asco=="T") ? "ชำระด้วยเช็คที่ชมรมวาณิชธนกิจ หรือโอนเงินผ่านบัญชีธนาคาร" : "ชำระด้วยเงินสดที่ ATI"; ?>
								</p>
							</div>
						</div>
						<?php $str = ($option_asco=="T") ? "5" : "2" ?>
						<?php $logo = ($option_asco=="T") ? "logo2.png" : "logo1.png" ?>
						<div class="se4-pay-picpaysbuy"><img src="./images/<?php echo $logo; ?>"></div>
						<br>
						<div class="se4-pay-text3">
							<!-- <center><p>เลือกชำระค่าธรรมเนียมได้ 2 ช่องทาง</p></center> -->
						</div>
						<div class="se4-pay-text-1">
							<!-- <center><p>อาคารชุดเลครัชดาออฟฟิศคอมเพล็กซ์ 2 ชั้น <?php echo $str; ?>
							</br>เลขที่ 195/3 ถนนรัชดาภิเษก แขวงคลองเตย เขตคลองเตย </br>กรุงเทพฯ 10110</p></center> -->
							<p style="font-size:13pt;">
								1) ชำระเงินสดที่ ATI กับเจ้าหน้าที่<br>
								2) นำฝากเงินสดเช็กเข้าบัญชี สถาบันฝึกอบรม สมาคมบริษัทหลักทรัพย์ไทย ประเภทบัญชี ออมทรัพย์ ธนาคารกสิกรไทย จำกัด (มหาชน)
							</p>

						</div>
					</div>
				</div>
				<div class="se4-pay-body3"><p>จำนวนเงินที่ต้องชำระ <?php echo set_comma($total_price); ?> บาท</p></div>
			</div>
 <?php /*           
			<div class="se4-Ltitle result_message" style="<?php echo ($section_id==3) ? 'display:none;' : '';?>"><p>“ระบบได้ทำการรับสมัคร<?php echo $name_type; ?>ของท่านเรียบร้อยแล้ว”</p></div>
*/?>			
			<div class="se4-botton2" style="<?php echo ($section_id==3) ? 'margin-top:20px;' : '';?>">
				<a href="#" onClick="checkPayType();">
					<div style="margin-top:9px;" class="se-botton-next">
						<center><p>ยืนยันช่องทางการชำระเงิน</p></center>
					</div>
				</a>
			</div>

			<div class="se-title"><center><p>สรุปข้อมูลการลงทะเบียน <?php //echo "{$thai_month_arr[$_SESSION["select_month"]]} {$_SESSION["select_year"]} ";?></p></center></div>
              <?php  if($format==2){ ?>
             			<div class="se3-title"><p>หลักสูตรที่สมัคร</p></div>                      
                         <table class="new-step-three tb-fa" width="100%">
							<tr class="tr-header-fa">
									<td class="td-col-0"></td>												
									<td class="td-col-2" style="width:12%;">วัน</td>
									<td class="td-col-3" style="width:7%;">เวลา</td>
									<td class="td-col-4">สถานที่</td>
									<td class="td-col-5">ข้อมูลสถานที่<?php echo $name_type; ?></td>
									<td class="td-col-6" style="width:7%;">ที่นั่ง</br>คงเหลือ</td>
									<td class="td-col-7" style="width:7%;">ที่นั่ง</br>ทั้งหมด</td>
									<td class="td-col-8" style="width:7%;">สถานะ</td>
									<td class="td-col-9" style="width:7%;">เปิด</br>รับสมัคร</td>
									<td class="td-col-10" style="width:7%;">ปิด</br>รับสมัคร</td>
							</tr> 
                            <?php echo $msg_data; ?>
                         </table>
			 <?php } else if($format==3){ ?>
             			<div class="se3-title"><p>หลักสูตรที่สมัคร</p></div>
                         <table class="new-step-three tb-fa" width="100%">
                          <tr class="tr-header-fa">
									<td class="td-col-0"></td>												
									<td class="td-col-2" style="width:12%;">วัน</td>
									<td class="td-col-3" style="width:7%;">เวลา</td>
									<td class="td-col-4">สถานที่</td>
									<td class="td-col-5">ข้อมูลสถานที่<?php echo $name_type; ?></td>
									<td class="td-col-6" style="width:7%;">ที่นั่ง</br>คงเหลือ</td>
									<td class="td-col-7" style="width:7%;">ที่นั่ง</br>ทั้งหมด</td>
									<td class="td-col-8" style="width:7%;">สถานะ</td>
									<td class="td-col-9" style="width:7%;">เปิด</br>รับสมัคร</td>
									<td class="td-col-10" style="width:7%;">ปิด</br>รับสมัคร</td>
                          </tr>
                            <?php echo $msg_data; ?>
                         </table>
			 <?php } else { ?>
                      <div class="se3-title"><p>หลักสูตรที่สมัคร</p></div>
                      <div class="se3-colum-title"><p>หลักสูตร</p></div>
                      <div class="se3-colum-text"><p><?php echo $title; ?></p><input name="c_title" type="hidden" value="<?php echo $title; ?>" id="c_title" /></div>
                      <div class="se3-line"></div>
                      <div class="se3-title"><p>เวลา / ห้อง</p></div>
                      <div class="se3-colum-title"><p>วัน</p></div>
                      <div class="se3-colum-text2"><div class="se3-colum2-day">
                      <center><p><?php echo $day; ?></p><input name="day" type="hidden" value="<?php echo $day; ?>" id="day" /></center></div>
                      <div class="se3-colum-textday"><p><?php echo $date; ?></p><input name="date" type="hidden" value="<?php echo $date; ?>" id="date" /></div></div>	
                      <div class="se3-colum-title"><p>เวลา</p></div>
                      <div class="se3-colum-text"><p><?php echo $time; ?></p><input name="time" type="hidden" value="<?php echo $time; ?>" id="time" /></div>
                      <div class="se3-colum-title"><p>สถานที่</p></div>
                      <div class="se3-colum-text"><p><?php echo $address; ?></p><input name="address_detail" type="hidden" value="<?php echo $address_detail; ?>" id="address_detail" /></div>
                      <div class="se3-colum-title"><p>ข้อมูลสถานที่<?php echo $name_type; ?></p></div>
                      <div class="se3-colum-text"><p><?php echo $address_detail; ?></p><input name="address" type="hidden" value="<?php echo $address; ?>" id="address" /></div>
			 <?php } ?>
                
                		
			<div class="se3-line"></div>
			<div class="se3-title"><p>ข้อมูลผู้สมัคร</p></div>
			<div class="se3-colum-title"><p>เลขที่บัตรประจำตัว 13 หลัก</p></div>
			<div class="se3-colum-text"><p><?php echo $_POST["username"]; ?></p></div>
			<div class="se3-colum-title"><p>คำนำหน้า - ชื่อ - นามสกุล</p></div>
			<div class="se3-colum-text">
			<p><?php echo $_POST["title_th"]; ?> &nbsp&nbsp&nbsp&nbsp&nbsp <?php echo $_POST["fname_th"]; ?> &nbsp&nbsp&nbsp&nbsp&nbsp <?php echo $_POST["lname_th"]; ?> </p></div>		
			<div class="se3-line"></div>
			<div class="se3-title"><p>ข้อมูลสำหรับแสดงบนใบเสร็จรับเงิน</p></div>
			<div class="se3-colum-title"><p>ออกใบเสร็จ</p></div>
			<div class="se3-colum-text"><p><?php echo $slip_type_text; ?></p></div>
			<div class="se3-colum-title"><p>ออกใบเสร็จในนาม</p></div>
			<div class="se3-colum-text">
				<p><?php  
					$receipt_id = $_POST["receipt_id"];
					if($receipttype_name!="" && $_POST["slip_type"]=="corparation"){
						$branch = $_POST["branch"] ? "".$_POST["branch"] : "";
						if($_POST["receipttype_id"]==5){
							$receipttype_name = "";
							$name = $_POST["receipttype_text"];
						}else{
							$name = $db->data("select name from receipt where receipt_id=$receipt_id");
						}
						echo /*$receipttype_name." ".*/$name." &nbsp;".$branch;
					}else{						
						echo $_POST["receipt_title"]." ".$_POST["receipt_fname"]."&nbsp&nbsp&nbsp&nbsp".$_POST["receipt_lname"]; 
					}
					?>
				</p>
			</div>
			<div class="se3-colum-title"><p>เลขที่ประจำตัวผู้เสียภาษี</p></div>
			<div class="se3-colum-text"><p><?php echo $_POST["taxno"]; ?></p></div>
			<div class="se3-colum-title"><p>บ้านเลขที่</p></div>
			<div class="se3-colum-text"><p><?php echo $_POST["receipt_no"]; ?></p></div>
			<div class="se3-colum-title"><p>หมู่บ้าน / คอนโด / อาคาร</p></div>
			<div class="se3-colum-text"><p><?php echo $_POST["receipt_gno"]; ?></p></div>
			<div class="se3-colum-title"><p>หมู่ที่</p></div>
			<div class="se3-colum-text"><p><?php echo $_POST["receipt_moo"]; ?></p></div>
			<div class="se3-colum-title"><p>ซอย</p></div>
			<div class="se3-colum-text"><p><?php echo $_POST["receipt_soi"]; ?></p></div>
			<div class="se3-colum-title"><p>ถนน</p></div>
			<div class="se3-colum-text"><p><?php echo $_POST["receipt_road"]; ?></p></div>
			<div class="se3-colum-title"><p>ตำบล / แขวง</p></div>
			<div class="se3-colum-text"><p><?php echo $district_name; ?></p></div>
			<div class="se3-colum-title"><p>อำเภอ / เขต </p></div>
			<div class="se3-colum-text"><p><?php echo $amphur_name; ?></p></div>
			<div class="se3-colum-title"><p>จังหวัด</p></div>
			<div class="se3-colum-text"><p><?php echo $province_name; ?></p></div>
			<div class="se3-colum-title"><p>รหัสไปรษณีย์</p></div>
			<div class="se3-colum-text"><p><?php echo $_POST["receipt_postcode"]; ?></p></div>
			<div class="se3-line"></div>
			
			<div class="se-bottonarea2" style="margin-top:38px; margin-button:30px;">
				<a href="#" onClick="redirect_home();"><div class="se-botton"><center><p>กลับสู่หน้าหลัก</p></center></div></a>
				<div class="se-bottonarearight">	
				<a href="#" onClick="goBack();" style="float:right"><div class="se-botton"><center><p>ย้อนกลับ</p></center></div></a>				
				</div>
			</div><div style="clear:both"></div>
	</div>
<form id='formBack' name='formBack' method='post' action='' enctype='application/x-www-form-urlencoded'>
<input name="sec_id" type="hidden" value="<?php echo $sec_id; ?>" id="sec_id" />
<input name="sec_action" type="hidden" value="<?php echo $sec_action; ?>" id="sec_action" />
<input name="my_select_course" type="hidden" value="<?php echo $my_select_course; ?>" id="my_select_course" />
<input name="my_select_course_detail" type="hidden" value="<?php echo $my_select_course_detail; ?>" id="my_select_course_detail" />
<input name="ch_submit_section" type="hidden" value="yes" id="ch_submit_section" />
</form>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		var pay = "<?php echo $pay ?>";
		$("input[name=payment_type]").each(function(index, val) {
			if($(this).val()==pay){
				$(this).attr("checked", true);
			}
		});
		
	});
	function checkPayType(){
		 var url = '<?php echo $link_print ?>.php?register_id=<?php echo $id; ?>';
		 var c = 0;
		 var pay_type = "";
		$("input[name=payment_type]").each(function(index, val) {
			var t = $(this).attr("checked");
			if(typeof t!="undefined"){
				c = 1;
				pay_type = $(this).val();
			}
		});
		if(c==0){
			msgError("คุณยังไม่ได้เลือก วิธีการชำระค่าธรรมเนียมการ<?php echo $name_type; ?>");
			return false;
		}
		register_auto_del_update();
		register_sendmails(pay_type);
		if(pay_type=="paysbuy"){
			paysbuyNow();
		}else{
			if(pay_type=="at_ati") url = 'invoice-ati.php?register_id=<?php echo $id; ?>';
			window.open(url,'_blank');;
		}
		//$sec_id = $_POST['sec_id'];
		//$sec_action = $_POST['sec_action'];
		window.open('index.php?p=register-choice&sec_id=<?php echo $sec_id; ?>&sec_action=<?php echo $sec_action; ?>','_self');
	}
	function update_pay(value){
			var url = "data/pay-update.php";
			var id = "<?php echo $id; ?>";
			var param = "pay="+value+"&register_id="+id+"&section_id=<?php echo $section_id; ?>&sec_action=<?php echo $sec_action; ?>";
			$.ajax( {
				"dataType":'json', 
				"type": "POST",
				"async": true, 
				"url": url,
				"data": param, 
				"success": function(data){

				}	
			});
	}
	function register_sendmails(pay_type){
			var url = "data/register-sendmails.php";
			var id = "<?php echo $id; ?>";
			var param = "pay="+pay_type+"&register_id="+id+"&section_id=<?php echo $section_id; ?>&sec_action=<?php echo $sec_action; ?>";
			$.ajax( {
				"dataType":'json', 
				"type": "POST",
				"async": true, 
				"url": url,
				"data": param, 
				"success": function(data){

				}	
			});
	}	
	function register_auto_del_update(){
			var url = "data/auto-del-update.php";
			var id = "<?php echo $id; ?>";
			var param = "register_id="+id;
			$.ajax( {
				"type": "POST",
				"async": false, 
				"url": url,
				"data": param, 
				"success": function(data){

				}	
			});
	}

	function del_last_id(){
			var url = "data/del-last-register.php";
			var id = "<?php echo $id; ?>";
			var param = "flag=del";
			$.ajax( {
				"type": "POST",
				"async": false, 
				"url": url,
				"data": param, 
				"success": function(data){

				}	
			});
	}
	function paysbuyNow(){
		var register_id = '<?php echo $id; ?>';
		var url = "./paysbuy/paynow.php?register_id="+register_id;
		window.open(url);
	}
function goBackStepOne(){
	del_last_id();
	$('#formBack').attr('action', 'index.php?p=new-step-one');
	$('#formBack').attr('target', '_self');
	$('#formBack').submit();
}

function goBack(){
	del_last_id();
	$('#formBack').attr('action', 'index.php?p=new-step-three');
	$('#formBack').attr('target', '_self');
	$('#formBack').submit();
}

function redirect_home(){
	del_last_id();
	window.open('index.php?p=select_s1','_self');
}
function goBackSteptwo(){
	del_last_id();
	$('#formBack').attr('action', 'index.php?p=new-step-two');
	$('#formBack').attr('target', '_self');
	$('#formBack').submit();
}

</script>