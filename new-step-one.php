﻿<?php
include_once "./lib/lib.php";
include_once "./share/course.php";

global $db;
if(!isset($_POST['ch_submit_section'])){
echo <<<holyy
<script>
window.open('index.php','_self');
</script>
holyy;
}
if($_POST['ch_submit_section'] != "yes"){
echo <<<holy
<script>
window.open('index.php?p=select_s1','_self');
</script>
holy;
}
$sec_id = $_POST['sec_id'];
$sec_action = $_POST['sec_action'];
$type_id = map_course_type($sec_action);

$q = " and a.active='T' and a.section_id=$sec_id and a.coursetype_id in ($type_id) and (a.parent_id IS NULL or a.parent_id<=0) and a.inhouse='F'";
$res = get_course($q, "", true);
$format = get_select_course($sec_id, $sec_action);
$count = count($res);
$tr_row = "";
if($res){
	$column = 2;
	$runing = 1;
	foreach ($res as  $row) {
		$course_id = $row['course_id'];
		$section_id = $row['section_id'];
		$course_code = $row['code'];
		$course_title = $row['title'];
		$detail = $row["detail"];
		$course_title .= (trim($detail!="")) ? "<div class='detail'>{$row['detail']}</div>" : "";
		$course_set_time = $row['set_time'];
		$course_life_time = $row['life_time'];
		$course_price = set_comma($row['price']);
		$course_rereg_date = $row['rereg_date'];
		$course_status = $row['status'];
		$row_class = ($runing%$column==0) ? "silver" : "white";
		if($format==1){
			$select = "<input type='radio' name='course' value='$course_id' id='course$course_id' onclick=\"getCourse('#course$course_id');\">";
			$tr_row  .= '<tr class="'.$row_class.'">
							<td class="text-center"><input type="radio" name="course" value="'.$course_id.'" id="course'.$course_id.'" onclick="getCourse(\'#course'.$course_id.'\');"></td>
							<td>'.$course_title.'</td>
							<td class="text-center">'.$course_price.'</td>
						 </tr>';
		}else if($format==2){
			$tr_row  .= '<tr class="'.$row_class.'">
							<td class="text-center"><input type="radio" name="course_parent" value="'.$course_id.'" id="course_parent'.$course_id.'" onclick="getCCourset(\''.$course_id.'\');"></td>
							<td>'.$course_title.'</td>
							<td class="text-center">'.$course_price.'</td>
						 </tr>';
		}else if($format==3){
			$tr_row  .= '<tr class="'.$row_class.'">
							<td class="text-center"><input type="checkbox" name="course[]" value="'.$course_id.'" id="course'.$course_id.'" onclick="getMCourse(\'#course'.$course_id.'\');"></td>
							<td>'.$course_title.'</td>
							<td class="text-center">'.$course_price.'</td>
						 </tr>';
		}else if($format==4){
			$tr_row  .= '<tr class="'.$row_class.'">
							<td class="text-center"><input type="radio" name="course" value="'.$course_id.'" id="course'.$course_id.'" onclick="getCourse(\'#course'.$course_id.'\');"></td>
							<td>'.$course_title.'</td>
							<td class="text-center">'.$course_price.'</td>
						 </tr>';
		} 
		$runing++;
	}//end foreach
}
?>
<div id="dialog" title="เลือกหลักสูตร" class="dialog"> 
</div>
	<div class="bgselect1" style="padding-bottom: 28px; height:auto;">
		<div class="bodyselect1" style="overflow:hidden; height:auto;">
			<div class="imgstep1">
				<table id="Table_01" width="1248" height="52" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td>
							<img src="images/step01_1.jpg" width="302" height="52" alt="">
						</td>
						<td>
							<img src="images/step01_2.jpg" width="306" height="52" alt="">
						</td>
						<td>
							<img src="images/step01_3.jpg" width="308" height="52" alt="">
						</td>
						<td>
							<img src="images/step01_4.jpg" width="332" height="52" alt="">
						</td>
					</tr>
				</table>
			</div>
			<table id="content-parent" width="100%" class="custom-table">
				<thead>					
					<tr class="header">
						<th colspan="3">เลือกหลักสูตร</td>
					</tr>
					<tr class="subheader">
						<td width="5%">เลือก</td>
						<td>ชื่อหลักสูตร</td>
						<td width="10%">ราคา (บาท)</td>
					</tr>
				</thead>
				<tbody>				
					<?php echo $tr_row; ?>
				</tbody>
			</table>
           <div id="content-child" style="clear:both;"></div>
			<div class="se-tdbottom"></div>
			<div class="se-bottonarea">
			<div onClick="javascript: window.open('index.php?p=main','_self');"><div class="se-botton"><center><p>กลับสู่หน้าหลัก</p></center></div></div>
			<div  class="se-bottonarearight"> 
				<div id="getCCourse_prev" style="display:none;" onClick="javascript: getCCourse_prev()"><div class="se-botton"><center><p>ย้อนกลับ</p></center></div></div>
				<a onClick="my_f_submit();"><div class="se-botton-next"><center><p>ไปขั้นตอนต่อไป</p></center></div></a><!-- javascript: window.open('index.php?p=new_s2','_self'); -->
			</div>
			</div>
		</div>
	</div>
<form id='form1' name='form1' method='post' action='' enctype='application/x-www-form-urlencoded'>
<input name="sec_id" type="hidden" value="<?php echo $sec_id; ?>" id="sec_id" />
<input name="sec_action" type="hidden" value="<?php echo $sec_action; ?>" id="sec_action" />
<input name="ch_submit_section" type="hidden" value="yes" id="ch_submit_section" />
<input name="my_select_course" type="hidden" value="want" id="my_select_course" />

</form>
<input name="child_tmp" type="hidden" value="0" id="child_tmp" />
<script type="text/javascript">
$(document).ready(function() {
	var count  = "<?php echo $count; ?>";
	if(count==1){
		//$("input[name=course]").trigger('click');
	}
});
function getCourse(radio_id_in){
	$("#my_select_course").val( $(radio_id_in).val());
}
function getCCourse_prev(){
	$("#getCCourse_prev").hide();
	$("#content-parent").show();
	$("#content-child").html(" ");
	$("#child_tmp").val(0);
	$("#content-parent tr td input[type=radio]:checked").attr("checked", false);
}
function getCCourset(id){
	$("#child_tmp").val(id);
}
function getCCourse(id){
	//$("#my_select_course").val( $(radio_id_in).val());
		var url = "data/child-course.php";
		var param = "course_id="+id;
		$.ajax( {
			"type": "GET",
			"async": false, 
			"url": url,
			"data": param, 
			"success": function(data){
				if(data=="0"){
					$("#my_select_course").val(id);
					$("#content-parent tr td input[type=radio]:checked").attr("checked", true);
					$("#getCCourse_prev").hide();
					$("#content-parent").show();
					$("#content-child").html(" ");					
					//setTimeout(function(){msgSuccess("")}, 1000);
					//my_f_submit();
				}else{
					$("#content-parent").hide();
					$("#getCCourse_prev").show();
					$("#my_select_course").val("want");
					$("#content-child").html(data);
					$("#child_tmp").val(0);				 
				}
			}
		});
}
function getMCourse(radio_id_in){
	var sList = "";
	$('.bodyselect1 input[type=checkbox]:checked').each(function () {
    	var sThisVal = (this.checked ? $(this).val() : "");
    	if(sThisVal=="") return;
    	sList += (sList=="" ? sThisVal : "," + sThisVal);
	});
	if(sList=="") sList = "want";
	$("#my_select_course").val(sList);
}
function dialog(){
	$("#dialog").dialog({
	    modal: true,
	    draggable: false,
	    resizable: false,
	    closeOnEscape: true,
	    show: 'blind',
	    hide: 'blind',
	    width: 300,
		dialogClass: 'showdialog',
        height: $(window).height()-120,
	    buttons: {
	        "ตกลง": function() {
	            $(this).dialog("close");
	        }
	    }
	});
	if(navigator.userAgent.indexOf("Firefox") != -1){
		$(document).scrollTop(0);
		setTimeout(function() {
			var newTop = (screen.height - $(".ui-dialog").height())/2;
			newTop = newTop-100;
			$(".ui-dialog").css( "top", newTop );
			$(document).scrollTop(0);
			$( window ).scroll(function() {
				var newTop = (screen.height - $(".ui-dialog").height())/2;
				newTop = newTop + $(document).scrollTop()-100;
				$(".ui-dialog").css( "top", newTop );
				$(".ui-dialog").css( "position", "absolute" );
			});
		}, 200);
		setTimeout(function() {
			var newTop = (screen.height - $(".ui-dialog").height())/2;
			newTop = newTop-100;
			$(".ui-dialog").css( "top", newTop );
			$(document).scrollTop(0);
			$( window ).scroll(function() {
				var newTop = (screen.height - $(".ui-dialog").height())/2;
				newTop = newTop + $(document).scrollTop()-100;
				$(".ui-dialog").css( "top", newTop );
				$(".ui-dialog").css( "position", "absolute" );
			});
		}, 300);
		setTimeout(function() {
			var newTop = (screen.height - $(".ui-dialog").height())/2;
			newTop = newTop-100;
			$(".ui-dialog").css( "top", newTop );
			$(document).scrollTop(0);
			$( window ).scroll(function() {
				var newTop = (screen.height - $(".ui-dialog").height())/2;
				newTop = newTop + $(document).scrollTop()-100;
				$(".ui-dialog").css( "top", newTop );
				$(".ui-dialog").css( "position", "absolute" );
			});
		}, 400);
		setTimeout(function() {
			var newTop = (screen.height - $(".ui-dialog").height())/2;
			newTop = newTop-100;
			$(".ui-dialog").css( "top", newTop );
			$(document).scrollTop(0);
			$( window ).scroll(function() {
				var newTop = (screen.height - $(".ui-dialog").height())/2;
				newTop = newTop + $(document).scrollTop()-100;
				$(".ui-dialog").css( "top", newTop );
				$(".ui-dialog").css( "position", "absolute" );
			});
		}, 500);
	}//end if(navigator.userAgent.indexOf("Firefox") != -1){
}
function my_f_submit(){
	var format = "<?php echo $format; ?>";
	if( $('#my_select_course').val() == "want" && $("#child_tmp").val()>0 && format==2){
		var id = $("#child_tmp").val();
		getCCourse(id);
		var id = $("#child_tmp").val();
		if(id==0){
			return;
		}
	}
	$('#form1').attr('action', 'index.php?p=new-step-two');
	$('#form1').attr('target', '_self');
	
	if( $('#my_select_course').val() == "want" ){
		$("#dialog").html("คุณยังไม่ได้เลือกหลักสูตร");
		dialog();
	} else {
		$('#form1').submit();
	}
}
</script>