<?php
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/datatype.php";
global $db;
$bill_payment_upload_id = $_GET["bill_payment_upload_id"];
$q = "select name, lock_edit from bill_payment_upload where bill_payment_upload_id=$bill_payment_upload_id";
$r = $db->rows($q);

$str = "";
$d = "";
if($r){
	$str = $r["name"];
	$lock = $r["lock_edit"];
}else{
	$str = "เพิ่มรายการ";
/*	$d = date("Y-m-d H:i:s");
	$d = revert_date($d, true);*/
}
?>

<div id="cl-wrapper">
	<div class="container-fluid" id="pcont">
		<div class="cl-mcont">
			<div class="row">
				  <div class="col-md-12">			      
					<div class="block-flat">
					  <div class="header">							
					  	<ol class="breadcrumb">
							<li><a href="#" onClick="clearPage('<?php echo $_GET['p'] ?>');">หน้าหลัก</a></li>
							<li class="active"><?php echo $str; ?></li>
						</ol>
					  </div>
					  <div class="content">
					  		<?php 

					  			if(!$r){
					  				?>
					  				<div class="alert alert-info">
					  					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					  					<i class="fa fa-info-circle sign"></i><strong>Info!</strong> กรุณา กด Save changes เพื่อเริ่มต้นการทำรายการ
					  				</div>

					  				<?php
					  			}
					  		 ?>
						  <form id="frmMain" name="frmMain" class="form-horizontal group-border-dashed"  method="post" enctype="multipart/form-data" action="update-bill-payment-upload.php">
						  <input type="hidden" name="bill_payment_upload_id" id="bill_payment_upload_id" value="<?php echo $bill_payment_upload_id; ?>">
						    <input type="hidden" name="tmpimg" />
  							<input type="hidden" name="delimg" />
						  <div class="col-sm-12">
							  <div class="form-group row">             			                
								<label class="col-sm-1 control-label">วันที่</label>
								<div class="col-sm-7">
								  <input class="form-control" readonly="readonly" name="name" id="name" placeholder="วันที่" type="text" value="<?php echo $d; ?>">
								</div>			                			                
									<label class="col-sm-1 control-label">ผู้บันทึก</label>
									<div class="col-sm-3">
										<input class="form-control" readonly="readonly" name="recby_name" id="recby_name"   type="text" >
									</div>
							  </div>				                
							  <div class="form-group row">
							  <label class="col-sm-1 control-label">รายละเอียด</label>
							 
							  	<div class="col-sm-11">
							  		<textarea id="remark"  class="form-control" name="remark" placeholder="Description"></textarea>
							  	</div>
							  </div>				                
							  <div class="form-group row">
							  	<div class="col-sm-12">
							  	<label class="control-label" <?php echo ($bill_payment_upload_id) ? '' : 'style="visibility:hidden;"'; ?>>ไฟล์ bill payment จาก ธนาคาร</label>
							  	<div class="clear"></div><br>
									<div id="file_upload_list">
										
									</div>							  		
							  	</div>
							  </div>
							  <div class="form-group row">
							  	<div class="col-sm-12">
							  	
							  	<div class="clear"></div><br>
									<div id="file_map_list">
										
									</div>							  		
									<div id="show_overdue_list">
										
									</div>
							  	</div>
							  </div>
						  </div>
						  <div class="clear"></div>
						  <div class="form-group row" style="padding-left:10px;">
						  	<?php if($lock=="F" || !$lock){ ?>
								<div class="col-sm-12" id="lock_button" <?php echo ($lock=="F" || !$lock) ? '' : 'style="visibility:hidden;"'; ?>>
									<button type="button" class="btn btn-primary" onClick="ckSave()">Save changes</button>
									<button type="button" class="btn" onClick="clearPage('<?php echo $_GET['p'] ?>');">Cancel</button>
									<div class="pull-right" >										
										<button type="button" id="bt_upload" <?php echo ($bill_payment_upload_id) ? '' : 'style="visibility:hidden;"'; ?> class="btn btn-primary" onClick="get_upload(<?php echo $bill_payment_upload_id; ?>)">Upload</button>
										<button type="button" id="update_bill_payment_map" style="display:none;" class="btn btn-primary" onClick="getbill_payment_map(<?php echo $bill_payment_upload_id; ?>, 'T')">Update Bill Payment</button>
										<button type="button" id="get_bill_payment"  style="display:none;"   class="btn btn-primary" onClick="getbill_payment_map(<?php echo $bill_payment_upload_id; ?>)">Map ข้อมูลการชำระเงิน</button>
										<button type="button" id="clear_overdue"   class="btn btn-primary" onClick="clear_overdues()" style="display:none;">เตะที่นั่ง</button>
										
									</div>
								</div>
								<?php }else{ ?>
									<div class="pull-right">
										<button type="button" id="show_overdue"   class="btn btn-primary" onClick="show_overdues()">แสดงรายชื่อเตะที่นั่ง</button>
										<button type="button" id="clear_overdue"   class="btn btn-primary" onClick="clear_overdues()" style="display:none;">เตะที่นั่ง</button>
									</div>

								<?php } ?>
						  </div>
						</form>
						
					  </div>
					</div>
					
				  </div>
				</div>

		</div>
	</div> 
</div>
<?php include_once ('inc/js-script.php'); ?>



<script type="text/javascript">
  $(document).ready(function() {
   $("#frmMain").validate();
   var bill_payment_upload_id = "<?php echo $_GET["bill_payment_upload_id"]?>";
   if(bill_payment_upload_id) {
   	viewInfo(bill_payment_upload_id);
   	var lock = "<?php echo $lock ?>";
   	if(lock=="T")
   		loadbill_payment_map(bill_payment_upload_id);
   }
   
       
 });
 function viewInfo(bill_payment_upload_id){
   if(typeof bill_payment_upload_id=="undefined") return;
   getbill_payment_uploadInfo(bill_payment_upload_id);
}

function clear_overdues(){
	var t = confirm("ยืนยันเตะที่นั่ง");
	if(!t) return;
	var url = "data/con-register-overdue-bill-payment.php";
	var param = "bill_payment_upload_id=T&use_click=T";
	var wmsg = '<div class="alert alert-warning"><button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button><i class="fa fa-warning sign"></i><strong>ข้อควรระวัง</strong> เนื่องจาก เตะที่นั่ง อาจจะใช้เวลานาน กรุณารออย่าเปลี่ยนหน้า จนกว่าระบบจะทำงานเสร็จ</div>';
		$("#show_overdue_list").html('<p>Loading.....</p>'+wmsg);	
	$.ajax( {
		"type": "POST",
		"async": false, 
		"url": url,
		"data": param, 
		"success": function(data){
			$("#show_overdue_list").html(data);
	 	}
	});	
}
function show_overdues(){
	$("#show_overdue").hide();
	var url = "data/show-register-overdue-bill-payment.php";
	var param = "bill_payment_upload_id=T";
	$.ajax( {
		"type": "POST",
		"async": true, 
		"url": url,
		"data": param, 
		"success": function(data){
			$("#clear_overdue").show();
			$("#show_overdue_list").html(data);
	 	}
	});	
}

function getbill_payment_uploadInfo(id){
	if(typeof id=="undefined") return;
	var url = "data/bill-payment-upload-list.php";
	var param = "bill_payment_upload_id="+id+"&single=T";
	dataUrl(url, param,"#frmMain");
	reload_upload();
}

function getbill_payment_map(id, update){
	if(typeof id=="undefined") return;
	if(typeof update!="undefined"){
		var t = confirm("Update Bill Payment ?");
		if(!t) return;
		var c = 0;
		$("#file_map_list table").each(function(index, el) {
			var ck = $(this).find("tbody tr").length;
			if(ck>0){
				c = 1;
			}
		});
/*		if(c==0){
			msgError("ไม่สามารถ update bill payment ได้ <br>กรุณาตรวจสอบอีกครั้ง");
			return;
		}*/
		$("#get_bill_payment").css({visibility: 'hidden'});
		var wmsg = '<div class="alert alert-warning"><button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button><i class="fa fa-warning sign"></i><strong>ข้อควรระวัง</strong> เนื่องจาก การอัพเดท billpayment อาจจะใช้เวลานาน กรุณารออย่าเปลี่ยนหน้า จนกว่าระบบจะทำงานเสร็จ</div>';
 		$("#file_map_list").html('<p>Loading.....</p>'+wmsg);
	}else{
		 $("#file_map_list").html('<p>Loading.....</p>');
		$("#get_bill_payment").hide();
		$("#bt_upload").hide();
		$("#update_bill_payment_map").show();
		$("#show_overdue").show();
	}
	var url = "data/bill-payment-map-list.php";
	var param = "bill_payment_upload_id="+id;
	if(typeof update!="undefined") param += "&update_log=T";
	$.ajax( {
		"type": "POST",
		"async": true, 
		"url": url,
		"data": param, 
		"success": function(data){
		   $("#file_map_list").html(data);
		   reload_upload();
	 	}
	});
	
}


function loadbill_payment_map(id){
	if(typeof id=="undefined") return;
	var url = "data/bill-payment-map-list.php";
	var param = "bill_payment_upload_id="+id;
	if(typeof update!="undefined") param += "&update_log=T";
	$.ajax( {
		"type": "POST",
		dataType : 'html',		
		"async": true, 
		"url": url,
		"data": param, 
		success: function(data){
		   $("#file_map_list").html(data);
		   reload_upload();
	 	}
	});	
}

function get_upload(id){
	if(typeof id=="undefined") return;
	var url = "bill-payment-upload-file.php?bill_payment_upload_id="+id;
	popUp(url, 700, 450);
}

function reView(){
	reload_upload();
}

function delete_file(id){
	if(typeof id=="undefined") return;
		var t = confirm("ลบไฟล์นี้ ? ");
		if(!t) return;
		var url = "update-upload.php";
		var param = "bill_payment_upload_list_id="+id;
		$.ajax( {
			"type": "POST",
			"async": false, 
			"url": url,
			"data": param, 
			"success": function(data){
			   reload_upload();
		 	}
		});
}
function reload_upload(){
		var url = "data/uploadlist.php";
		var param = "bill_payment_upload_id=<?php echo $bill_payment_upload_id; ?>";
		$.ajax( {
			"type": "POST",
			"async": false, 
			"url": url,
			"data": param, 
			"success": function(data){
			   $("#file_upload_list").html(data);
			   if($("#file_map_list table").length>0){ }else if($("#file_upload_list tbody tr").length>0){$("#get_bill_payment").show();}
		 	}
		});

}

function ckSave(id){
  onCkForm("#frmMain");
  $("#frmMain").submit();
}

function registerInfo(id){
	if(typeof id=="undefined") return;
   var url = "index.php?p=register&register_id="+id+"&type=info";
   window.open(url,'_blank');
}

</script>
<style>
	#gritter-notice-wrapper{
		width: 400px;
		margin-left: -200px;
		top: 20%;
	}
	.gritter-without-image, .gritter-with-image{
		width: 100%;
	}
</style>