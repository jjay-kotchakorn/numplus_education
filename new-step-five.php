<?php
// received result from paysbuy with data result,apCode,amt,fee,methos,confirm_cs
$result = $_POST["result"];
$apCode = $_POST["apCode"];
$amt = $_POST["amt"];
$fee = $_POST["fee"];
$method = $_POST["method"];
$paydate = $_POST["payment_date"];
$confirm_cs = strtolower(trim($_POST["confirm_cs"]));

//$result = "00000019";
//$apCode = "76807";
//$amt =  "7276.00";
//$fee = "291.0386";
//$method = "02";
//$confirm_cs = strtolower(trim(""));
log_file($_POST, "z_step5LogFile.log");
//print_r($_POST);
$len = strlen($result);
$payment_status = substr($result, 0,2);
$strInvoice = substr($result, 2,$len-2);
$message = "";

if ($payment_status == "00") {
	$message = "ระบบได้รับการชำระเงินของท่านเรียบร้อยแล้ว";
}
else {
	$message = "การชำระเงินของท่านยังไม่เรียบร้อย";
}

$member_info = $_SESSION["login"]["info"];
$member_id = $member_info['member_id'];

$showDetail = false;
if( $strInvoice != '')
{
	include_once "./share/authen.php";
	include_once "./lib/lib.php";
	include_once "./share/course.php";
	global $db;

	$register_id = (int) $strInvoice;

	$rs = get_register("", $register_id);
	$rs = ($rs) ? $rs[0] : "";
		  
	if($rs){
		if($register_id){
			if ($payment_status == "00") {
				$message = "ระบบได้รับการชำระเงินของท่านเรียบร้อยแล้ว";
			    if ($method == "06") { 
			        if ($confirm_cs == "true") {
			            $args["table"] = "register";
			            $args["id"] = $register_id;
			            $args["pay_method"] = $method;
			            $args["pay_price"] = $amt; // จำนวนเงินที่ชำระ
			            $args["pay_diff"] = $fee; // ค่าธรรมเนียม;
			            $args["pay_id"] = $apCode; // 
			            $args["pay_date"] = thai_to_timestamp($paydate, true); // 
			            $args["pay_status"] = 3; // 
			            $docNo = $rs["docno"];
			             if($docNo==""){
							$q = "select coursetype_id, section_id from register where register_id=$register_id";
							$v = $db->rows($q);
							$section_id = (int)$v["section_id"];
							$coursetype_id = (int)$v["coursetype_id"];
							$t = rundocno($coursetype_id, $section_id);
							$args = array_merge($args, $t);	
						}
			            $db->set($args);         
			            echo "Success"; 
			        } else if ($confirm_cs == "false") { 
			            echo "Fail"; 
			        } else {         
			            echo "Process"; 
			        } 
			    } else {
			        $args["table"] = "register";
			        $args["id"] = $register_id;
			        $args["pay_method"] = $method;
			        $args["pay_price"] = $amt; // จำนวนเงินที่ชำระ
			        $args["pay_diff"] = $fee; // ค่าธรรมเนียม;
			        $args["pay_id"] = $apCode; // 
			        $args["pay_date"] = thai_to_timestamp($paydate, true); // 
			        $args["pay_status"] = 3; // 
		            $docNo = $rs["docno"];
		            if($docNo==""){
						$q = "select coursetype_id, section_id from register where register_id=$register_id";
						$v = $db->rows($q);
						$section_id = (int)$v["section_id"];
						$coursetype_id = (int)$v["coursetype_id"];
						$t = rundocno($coursetype_id, $section_id);
						$args = array_merge($args, $t);	
					}
			        $db->set($args); 
			        //echo "Success"; 
			    } 
			    /*START SEND MAIL*/

				if($register_id){
					  $rs = get_register("", $register_id);
					  $data = $rs[0];
					$q = "SELECT a.member_id, a.password, a.email1, a.email2, a.fname_th, a.cid, a.lname_th FROM member a WHERE a.member_id={$data["member_id"]} and a.active='T'";
					$rowM = $db->rows($q);
					$M_PWD = $rowM['password'];
					$M_EMAIL1 = $rowM['email1'];
					$M_EMAIL2 = $rowM['email2'];
					//$pay_status = $rowM['pay_status'];	  
					  $sCourse_id = $data["course_id"];
					  $con = " and a.course_id in ($sCourse_id)";
					  $r = get_course($con);
					  $title = "";
					  $parent_id = 0;
					  $price = 0;
					  foreach ($r as $key => $row) {
					    $course_id = $row['course_id'];
					    $section_id = $row['section_id'];
					    $code = $row['code'];
					    $title .= $row['title'].", ";
					    $set_time = $row['set_time'];
					    $life_time = $row['life_time'];
					    $status = $row['status']; 
					    $parent_id = (int)$row['parent_id'];
					  }
					  $q = "select a.course_detail_id from register_course_detail a where a.register_id={$register_id} and a.course_id in ($sCourse_id)";
					  $cd = $db->get($q);

					  $title = trim($title, ", ");
					  $price = $data["course_price"];
					  $discount = $data["course_discount"];
					  if($cd){    
					    $display_date = "";
					    $display_address = "";
					    foreach ($cd as $kk => $vv) {
					      $row = get_course_detail(" and a.course_detail_id={$vv["course_detail_id"]}");
					      if($row) $row = $row[0];
					      $course_detail_id = $row['course_detail_id'];
					      $course_id = $row['course_id'];
					      $day = $row['day'];
					      $date = $row['date'];
					      $time = $row['time'];

					      $date_other = $row['date_other'];
					      $date_desc = $row['date_desc'];

					      $display_date .= revert_date($date)."&nbsp; ".$time.", ";
					      $display_address .= $row['address_detail']." ".$row['address'].", ";
					    }

					  }else{
					      $row = get_course_detail(" and a.course_detail_id={$data["course_detail_id"]}");
					      if($row) $row = $row[0];
					      $course_detail_id = $row['course_detail_id'];
					      $course_id = $row['course_id'];
					      $day = $row['day'];
					      $date = $row['date'];
					      $time = $row['time'];

					      $date_other = $row['date_other'];
					      $date_desc = $row['date_desc'];

					      $display_date .= revert_date($date)."&nbsp;เวลา ".$time.", ";
					      $display_address .= $row['address']." ".$row['address_detail'].", ";
					  }

					  $display_date = trim($display_date, ", ");
					  $display_address = trim($display_address, ", ");
					  $slip_type_text = "";
					  if($data["slip_type"]=="individuals") $slip_type_text = "ออกในนามบุคคลธรรมดา";
					  if($data["slip_type"]=="corparation") $slip_type_text = "ออกในนามนิติบุคคล (สำหรับเบิกค่าใช้จ่าย)";
					  $q = "select name from receipttype where receipttype_id={$data["receipttype_id"]}";
					  $receipttype_name = $db->data($q);
					  $q = "select name from district where district_id={$data["receipt_district_id"]}";
					  $district_name = $db->data($q);
					  $q = "select name from amphur where amphur_id={$data["receipt_amphur_id"]}";
					  $amphur_name = $db->data($q);
					  $q = "select name from province where province_id={$data["receipt_province_id"]}";
					  $province_name = $db->data($q);
					  $q = "select register_id from register a where a.active='T' and  a.course_id='$course_id'";
					  $id = $db->data($q);
					  $total_price = $price - $discount;
					  $t = convert_mktime($data["rectime"]) + (2 * 24 * 60 * 60);
					  $pay_date = $data["expire_date"];

					  $receipt_id = $data["receipt_id"];
					  $slip_name = "";
					  $slip_address = $data["receipt_no"]." ".$data["receipt_gno"]." ".$data["receipt_moo"]." ".$data["receipt_soi"]." ";
					  $slip_address .= $data["receipt_road"]." ".$district_name." ".$amphur_name." ".$province_name." ".$data["receipt_postcode"];

					  if ($data["slip_type"] == 'individuals') {
					  	$slip_name = $data["receipt_fname"]." ".$data["receipt_lname"];
					  }elseif($data["slip_type"] == 'corparation'){
					  	$q = "select name from receipt a where a.receipt_id=$receipt_id";
					  	$slip_name = $db->data($q);
					  }

					  	$ttl_price = $data["course_price"]-$data["course_discount"];
					  	$pay = $data['pay'];

					  	//$line_pay = "";
						$payMsg = "";
						if($pay=="bill_payment"){
							$line_pay = "Bill Payment : ";
							$payMsg = 'ช่องทางชำระ Bill Payment :<br>					
									รหัสการลงทะเบียน<br>
									Ref 1 : '.$data["ref1"] .'<br>
									Ref 2 : '.$data["ref2"] .'<br>
									ช่องทางชำระ Paysbuy : -<br>
									ช่องทางชำระโดยชำระเงินที่ ATI : -<br>
									ช่องทางชำระโดยชำระเงินที่ ASCO : -<br>';
						}else{
							//$payMsg = sprintf("%06d", $register_id);
							$pay_id = sprintf("%06d", $register_id);

								if($pay=='paysbuy'){
									//$line_pay = "Paysbuy : ";
									$payMsg = "ช่องทางชำระ Bill Payment : -<br>";
									$payMsg .= "ช่องทางชำระ Paysbuy : ".$pay_id."<br>";
									$payMsg .= "ช่องทางชำระโดยชำระเงินที่ ATI : -<br>";
									$payMsg .= "ช่องทางชำระโดยชำระเงินที่ ASCO : -<br>";
								}
								if($pay=='at_ati'){
									//$line_pay = "ATI : ";
									$payMsg = "ช่องทางชำระ Bill Payment : -<br>";
									$payMsg .= "ช่องทางชำระ Paysbuy : -<br>";
									$payMsg .= "ช่องทางชำระโดยชำระเงินที่ ATI : ".$pay_id."<br>";
									$payMsg .= "ช่องทางชำระโดยชำระเงินที่ ASCO : -<br>";
								}
								if($pay=='at_asco'){
									//$line_pay = "ASCO : ";
									$payMsg = "ช่องทางชำระ Bill Payment : -<br>";
									$payMsg .= "ช่องทางชำระ Paysbuy : -<br>";
									$payMsg .= "ช่องทางชำระโดยชำระเงินที่ ATI : -<br>";
									$payMsg .= "ช่องทางชำระโดยชำระเงินที่ ASCO : ".$pay_id."<br>";
								}	

						}

						$message = "เรียน คุณ ".$data["fname"]." ".$data["lname"];
						$subject = "";
						$header = "";
						$html = "";
						//pay status
						$pay_status = $data['pay_status'];
						switch ($pay_status) {
							/*case '1':
								$subject .= "แจ้งข้อมูลการลงทะเบียน";
								$header .= "ทางเราได้รับแจ้งว่าท่านมีความประสงค์ที่จะลงทะเบียนกิจกรรมโดยมีรายละเอียดดังนี้";
								$html .= '
								<table class="deviceWidth" align="center" border="0" cellpadding="0" cellspacing="0" width="100%"> 
									<tbody>
										<tr>
											<td class="center" style="font-size: 12px; color: #687074; font-weight: bold; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 20px 10px; " bgcolor="#ffffff">
												'.$message.'
												<br/>
												'.$header.'<br><br>
												ข้อมูลการลงทะเบียน<br>
												หลักสูตร : '.$title.'<br>
												วันที่ : '.$display_date.'<br>
												วันที่เป็นชุด : '.$date_other.'<br>
												description : '.$date_desc.'<br>
												สถานที่ : '.$display_address.'<br>
												ลงทะเบียนเมื่อ : '.$data["rectime"].'<br><br>								
												'.$payMsg.'<br>
												สถานะการลงทะเบียน : ลงทะเบียนแล้ว รอการชำระเงิน<br><br>
												ขอความกรุณาท่านชำระค่าธรรมเนียมภายในวันที่กำหนด<br><br>
												สถาบันฝึกอบรม สมาคมบริษัทหลักทรัพย์ (ATI)<br>
												ส่วนฝึกอบรมติดต่อ:Training@ati-asco.org โทร 02-264-0909 ต่อ 223<br>
												ส่วนทดสอบผู้แนะนำการลงทุนฯติดต่อ: Examination@ati-asco.org โทร 02-264-0909 ต่อ 203-207<br>
												ชมรมวาณิชธนกิจติดต่อ: fatraining@asco.or.th โทร 02-264-0909 ต่อ 124,125<br>
											</td>
										</tr>		
									</tbody>
								</table>
								';
								break;*/
							case '2':
								$subject .= "แจ้งการชำระเงินเกินกำหนด";
								$header .= "ทางเราได้รับแจ้งว่าท่านได้ชำระค่าธรรมเนียมเกินกำหนด ให้ท่านติดต่อเจ้าหน้าที่ที่เกี่ยวข้องโดยด่วน เพื่อเป็นการยืนยันสิทธิ์ในกิจกรรมที่ท่านได้ลงทะเบียนไว้";
								$html .= '
								<table class="deviceWidth" align="center" border="0" cellpadding="0" cellspacing="0" width="100%"> 
									<tbody>
										<tr>
											<td class="center" style="font-size: 12px; color: #687074; font-weight: bold; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 20px 10px; " bgcolor="#ffffff">
												'.$message.'
												<br/>
												'.$header.'<br><br>
												ข้อมูลการลงทะเบียน<br>
												หลักสูตร : '.$title.'<br>
												วันที่ : '.$display_date.'<br>
												วันที่เป็นชุด : '.$date_other.'<br>
												description : '.$date_desc.'<br>
												สถานที่ : '.$display_address.'<br>
												ลงทะเบียนเมื่อ : '.$data["rectime"].'<br><br>								
												'.$payMsg.'<br>
												สถานะการลงทะเบียน : ชำระเงินเกินกำหนด<br><br>
												ข้อมูลเพื่อการออกใบเสร็จ<br><br>
												ค่าธรรมเนียม: 1000 บาท (รวมภาษีมูลค่าเพิ่ม 7%)<br>
												ออกใบเสร็จในนาม: ธ. กรุงไทย จำกัด (มหาชน)<br>
												สาขาที่ : สำนักงานใหญ่<br>
												เลขประจำตัวผู้เสียภาษี : 0107537000882<br>
												ที่อยู่ :35 ถ.สุขุมวิท แขวงคลองเตยเหนือ เขตวัฒนา กรุงเทพมหานคร 10110<br><br>
												สถาบันฝึกอบรม สมาคมบริษัทหลักทรัพย์ (ATI)<br>
												ส่วนฝึกอบรมติดต่อ:Training@ati-asco.org โทร 02-264-0909 ต่อ 223<br>
												ส่วนทดสอบผู้แนะนำการลงทุนฯติดต่อ: Examination@ati-asco.org โทร 02-264-0909 ต่อ 203-207<br>
												ชมรมวาณิชธนกิจติดต่อ: fatraining@asco.or.th โทร 02-264-0909 ต่อ 124,125<br>
											</td>
										</tr>		
									</tbody>
								</table>
								';
								break;
							case '3':
								$subject .= "ยืนยันการชำระค่าธรรมเนียม";
								$header .= "ทางเราได้รับค่าธรรมเนียม จากท่านเป็นที่เรียบร้อยแล้ว และขอยืนยันการลงทะเบียนของท่าน ตามรายละเอียดดังนี้:";
								$html .= '
								<table class="deviceWidth" align="center" border="0" cellpadding="0" cellspacing="0" width="100%"> 
									<tbody>
										<tr>
											<td class="center" style="font-size: 12px; color: #687074; font-weight: bold; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 20px 10px; " bgcolor="#ffffff">
												'.$message.'
												<br/>
												'.$header.'<br><br>
												ข้อมูลการลงทะเบียน<br>
												หลักสูตร : '.$title.'<br>
												วันที่ : '.$display_date.'<br>
												วันที่เป็นชุด : '.$date_other.'<br>
												description : '.$date_desc.'<br>
												สถานที่ : '.$display_address.'<br>
												ลงทะเบียนเมื่อ : '.$data["rectime"].'<br><br>								
												'.$payMsg.'<br>
												สถานะการลงทะเบียน : ชำระเงินแล้ว ยืนยันการลงทะเบียน<br><br>
												ข้อมูลเพื่อการออกใบเสร็จ<br><br>
												ค่าธรรมเนียม: '.$ttl_price.' บาท (รวมภาษีมูลค่าเพิ่ม 7%)<br>
												ออกใบเสร็จในนาม: '.$slip_name.'<br>
												เลขประจำตัวผู้เสียภาษี : '.$data["taxno"].'<br>
												ที่อยู่ :'.$slip_address.'<br><br>
												ระเบียบการเข้าสอบ '.$title.' <a style="text-decoration: none; color: #27d7e7;" href="https://register.ati-asco.org/pdf/rule_test.pdf"> คลิกที่นี่</a><br><br>
												สถาบันฝึกอบรม สมาคมบริษัทหลักทรัพย์ (ATI)<br>
												ส่วนฝึกอบรมติดต่อ:Training@ati-asco.org โทร 02-264-0909 ต่อ 223<br>
												ส่วนทดสอบผู้แนะนำการลงทุนฯติดต่อ: Examination@ati-asco.org โทร 02-264-0909 ต่อ 203-207<br>
												ชมรมวาณิชธนกิจติดต่อ: fatraining@asco.or.th โทร 02-264-0909 ต่อ 124,125<br>
											</td>
										</tr>		
									</tbody>
								</table>
								';
								break;													
							default:
								
								break;
						}
						
					}

					/*var_dump($html);
					die();*/

					/**
					 * This example shows making an SMTP connection with authentication.
					 */

					//SMTP needs accurate times, and the PHP time zone MUST be set
					//This should be done in your php.ini, but this is how to do it if you don't have access to that
					//date_default_timezone_set('Etc/UTC');

					include_once  "./PHPMailer-master/PHPMailerAutoload.php";

					//Create a new PHPMailer instance
					$mail = new PHPMailer;
					//Tell PHPMailer to use SMTP
					$mail->isSMTP();
					//Enable SMTP debugging
					// 0 = off (for production use)
					// 1 = client messages
					// 2 = client and server messages
					//$mail->SMTPDebug = 2; 
					//Ask for HTML-friendly debug output

					//$email = $_POST['email'];


					//$message = "เรียน คุณ ".$data["fname"]." ".$data["lname"];
					
					$mail->CharSet = "utf-8";
					$mail->ContentType = "text/html";

					$mail->Debugoutput = 'html'; 
					//Set the hostname of the mail server
					$mail->Host = "mail.numplus.com"; 
					//Set the SMTP port number - likely to be 25, 465 or 587
					$mail->Port = 25; 
					//Whether to use SMTP authentication
					$mail->SMTPAuth = true; 
					//Username to use for SMTP authentication
					$mail->Username = "ati@numplus.com"; 
					//Password to use for SMTP authentication
					$mail->Password = "ati8899"; //Set who the message is to be sent from
					$mail->setFrom('register@ati-asco.org', 'ASCO Training Institute (ATI)'); 
					//Set an alternative reply-to address
					// $mail->addReplyTo('thictyouth@numplus.in.th', 'Koom2'); 
					//Set who the message is to be sent to
					$mail->addAddress($M_EMAIL1, 'You'); 
					if($M_EMAIL2 != ""){
					//Set Sent to CC
						$mail->addCC($M_EMAIL2); 
					}
					$mail->AddCC('Examination@ati-asco.org');
					$mail->AddCC('reg_training@ati-asco.org');
					//Set the subject line
					$mail->Subject = $subject;
					//$mail->Subject = 'แจ้งข้อมูลการลงทะเบียน';   
					//Read an HTML message body from an external file, convert referenced images to embedded,
					//convert HTML into a basic plain-text alternative body
					$mail->msgHTML($html); 

					//Replace the plain text body with one created manually
					// $mail->AltBody = 'This is a Automatic Mail'; 
					//Attach an image file


					//send the message, check for errors
					unset($_SESSION["reister_info_sendmain"]);
					if (!$mail->send()) {
						echo "UPDATE ERROR";
					  	$_SESSION["reister_info_sendmain"]["error"] = " มีข้อผิดพลาดเกิดขึ้น กรุณาติดต่อผู้ดูแลระบบ";
					} else {
					   echo "UPDATE SUCCESSFULLY";
					   $_SESSION["reister_info_sendmain"]["msg"] = "ระบบได้ส่ง รหัสผ่านไปยัง Email ของท่านเรียบร้อยแล้ว";
					}
			    /*END MAIL*/
			    //echo "<hr>";
			    //print_r($args);
			}
			$showDetail = true;
			$rs = get_register("", $register_id);
			$data = $rs[0];
			$course_id = $data["course_id"];
			$type_id = $data["coursetype_id"];
			$q = "SELECT name FROM `coursetype` WHERE coursetype_id=$type_id";
			$name_type = $db->data($q);
			$con = " and a.course_id in ($course_id)";
			$r = get_course($con);
			$title = "";
			$parent_id = 0;
			$price = 0;
			
			$msg_data = "<br/>";
 			$no = 1;
 		
			$slip_type_text = "";
			if($data["slip_type"]=="individuals") $slip_type_text = "ออกในนามบุคคลธรรมดา";
			if($data["slip_type"]=="corparation") $slip_type_text = "ออกในนามนิติบุคคล (สำหรับเบิกค่าใช้จ่าย)";
			
			$receipt_id = $data["receipt_id"];
			$q = "select name from receipt where receipt_id = $receipt_id";
			$receipt_id_name = $db->data($q);
			  
			foreach ($r as $key => $row) {
			  $course_id = $row['course_id'];
			  $section_id = $row['section_id'];
			  $code = $row['code'];
			  $title = $row['title'];
			  $set_time = $row['set_time'];
			  $life_time = $row['life_time'];
			  $status = $row['status']; 
			  $parent_id = (int)$row['parent_id'];
					

			  if($section_id == 3)
			  {
				  $row_d = get_course_detail(" and course_id = $course_id");
				  if($row_d) $row_d = $row_d[0];
					
				  $day = $row_d['day'];
				  $date = revert_date($row_d['date']);
				  $time = $row_d['time'];
				  $address_detail = $row_d['address_detail'];
				  
				  if($tr_current_bg == "tr-bg-white"){
					  $tr_current_bg = "tr-bg-silver";
				  } else if($tr_current_bg == "tr-bg-silver"){
					  $tr_current_bg = "tr-bg-white";
				  } else {
					  $tr_current_bg = "tr-bg-silver"; 
				  }
				  
				  $msg_data .= "<tr class='$tr_current_bg tr-item-fa'>
								<td class='td-col-0'>$no.</td>
								<td class='td-col-1'>$title</td>
								<td class='td-col-2'>
									 <span class='span-day'>$day</span>
									 <span class='span-date'>$date</span>
								</td>
								<td class='td-col-3'>$time</td>
								<td class='td-col-4'>$address</td>
								<td class='td-col-5'>$address_detail</td>
							</tr> ";
				  $no++;
			  }
			} // end foreach
		
			$price = $data["course_price"];
			$discount = $data["course_discount"];
		
			if($section_id == 3)
			{
				$p_con = " and a.course_id = $parent_id";
				$p_r = get_course($p_con);
				$parent_title = $p_r[0]['title'];
				//$parent_price = $p_r[0]['price'];
			}
			else
			{
				$row = get_course_detail("", $data["course_detail_id"]);
				if($row) $row = $row[0];
				$course_detail_id = $row['course_detail_id'];
				$course_id = $row['course_id'];
				$day = $row['day'];
				$date = revert_date($row['date']);
				$time = $row['time'];
				$address = $row['address'];
				$address_detail = $row['address_detail'];
				$sit_all = $row['sit_all'];
				$chair_all = $row['chair_all'];
				$status = $row['status'];
				$open_regis = $row['open_regis'];
				$end_regis = $row['end_regis'];
				$datetime_add = $row['datetime_add'];
				$datetime_update = $row['datetime_update'];
			}
		
			$slip_type_text = "";
			if($data["slip_type"]=="individuals") $slip_type_text = "ออกในนามบุคคลธรรมดา";
			if($data["slip_type"]=="corparation") $slip_type_text = "ออกในนามนิติบุคคล (สำหรับเบิกค่าใช้จ่าย)";
			$q = "select name from receipttype where receipttype_id={$data["receipttype_id"]}";
			$receipttype_name = $db->data($q);
			$q = "select name from district where district_id={$data["receipt_district_id"]}";
			$district_name = $db->data($q);
			$q = "select name from amphur where amphur_id={$data["receipt_amphur_id"]}";
			$amphur_name = $db->data($q);
			$q = "select name from province where province_id={$data["receipt_province_id"]}";
			$province_name = $db->data($q);
			$q = "select register_id from register a where a.active='T' and  a.course_id='$course_id'";
			$id = $db->data($q);
			$total_price = $price - $discount;
			$t = convert_mktime($data["rectime"]) + (2 * 24 * 60 * 60);
			  $pay_date = date("Y-m-d H:i:s", $t);
		}
	}
}
else
{
	$message = "ข้อมูลไม่ถูกต้อง"; 
}
	
?>
	<div class="bgselect4">
			<div class="bodyselect4">
				<div class="imgstep1">
				<table id="Table_01" width="1248" height="52" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td>
			  <img src="images/step04_01.jpg" width="302" height="52" alt=""></td>
			<td>
				<img src="images/step04_02.jpg" width="306" height="52" alt=""></td>
			<td>
				<img src="images/step04_03.jpg" width="308" height="52" alt=""></td>
			<td>
				<img src="images/step04_04.jpg" width="332" height="52" alt=""></td>
		</tr>
	</table>
				</div>
				<div class="se4-pay-area">
					<div class="se4-pay-title"><center>
					  <p>ผลการชำระเงินช่องทางอื่นๆ </p></center>
					  <div class="se4-pay-picpaysbuy"><img src="./images/pay_new.jpg"></div>                       
                    </div>
					<div class="se4-pay-body3"><p>จำนวนเงินที่ต้องชำระ <?php echo set_comma($total_price); ?> บาท</p></div>
				<div class="se4-Ltitle result_message"><p>“<?php echo $message;?>”</p></div>
				<?php if ($showDetail == true){ ?>
                    <div class="se-title"><center>
                      <p>ข้อมูลการลงทะเบียน</p></center></div>
                  <?php  if($section_id == 3){ ?>
             			<div class="se3-title"><p>หลักสูตรที่สมัคร</p></div>
						<div class="se3-colum-title"><p>หลักสูตร</p></div>
                       <div class="se3-colum-text"><p><?php echo $parent_title; ?></p><input name="c_title" type="hidden" value="<?php echo $parent_title; ?>" id="c_title" /></div>
                         <div class="se3-title"><p>เวลา / ห้อง</p></div>
                         <table class="new-step-three tb-fa" width="100%">
                          <tr class="tr-header-fa">
							  <td class="td-col-0"></td>
                              <td class="td-col-1">ชื่อหลักสูตร</td>
                              <td class="td-col-2">วัน</td>
                              <td class="td-col-3">เวลา</td>
                              <td class="td-col-4">สถานที่</td>
                              <td class="td-col-5">ข้อมูลสถานที่<?php echo $name_type; ?></td>
                          </tr>
                            <?php echo $msg_data; ?>
                         </table>
			 <?php } else { ?>
			          <div class="se3-title"><p>หลักสูตรที่สมัคร</p></div>
                      <div class="se3-colum-title"><p>หลักสูตร</p></div>
                      <div class="se3-colum-text"><p><?php echo $title; ?></p><input name="c_title" type="hidden" value="<?php echo $title; ?>" id="c_title" /></div>
                      <div class="se3-line"></div>
                      <div class="se3-title"><p>เวลา / ห้อง</p></div>
                      <div class="se3-colum-title"><p>วัน</p></div>
                      <div class="se3-colum-text2"><div class="se3-colum2-day">
                      <center><p><?php echo $day; ?>.</p><input name="day" type="hidden" value="<?php echo $day; ?>" id="day" /></center></div>
                      <div class="se3-colum-textday"><p><?php echo $date; ?></p><input name="date" type="hidden" value="<?php echo $date; ?>" id="date" /></div></div>	
                      <div class="se3-colum-title"><p>เวลา</p></div>
                      <div class="se3-colum-text"><p><?php echo $time; ?></p><input name="time" type="hidden" value="<?php echo $time; ?>" id="time" /></div>
                      <div class="se3-colum-title"><p>สถานที่</p></div>
                      <div class="se3-colum-text"><p><?php echo $address; ?></p><input name="address_detail" type="hidden" value="<?php echo $address_detail; ?>" id="address_detail" /></div>
                      <div class="se3-colum-title"><p>ข้อมูลสถานที่<?php echo $name_type; ?></p></div>
                      <div class="se3-colum-text"><p><?php echo $address_detail; ?></p><input name="address" type="hidden" value="<?php echo $address; ?>" id="address" /></div>
			 <?php } ?>	
                    <div class="se3-line"></div>
                    <div class="se3-title"><p>ข้อมูลผู้สมัคร</p></div>
                    <div class="se3-colum-title"><p>เลขที่บัตรประจำตัว 13 หลัก</p></div>
                    <div class="se3-colum-text"><p><?php echo $member_info["username"]; ?></p></div>
                    <div class="se3-colum-title"><p>คำนำหน้า - ชื่อ - นามสกุล</p></div>
                    <div class="se3-colum-text">
                    <p><?php echo $data["title"]; ?> <?php echo $data["fname"]; ?> &nbsp&nbsp&nbsp&nbsp&nbsp <?php echo $data["lname"]; ?> </p></div>		
                    <div class="se3-line"></div>
                    <div class="se3-title"><p>ข้อมูลสำหรับแสดงบนใบเสร็จรับเงิน</p></div>
                    <div class="se3-colum-title"><p>ออกใบเสร็จ</p></div>
                    <div class="se3-colum-text"><p><?php echo $slip_type_text; ?></p></div>
                    <div class="se3-colum-title"><p>ออกใบเสร็จในนาม</p></div>
                    <div class="se3-colum-text"><p><?php echo ($receipt_id_name!="") ? $receipt_id_name : $data["receipt_title"] ; ?> <?php echo $data["receipt_fname"]; ?> &nbsp&nbsp&nbsp&nbsp <?php echo $data["receipt_lname"]; ?></p></div>
                    <div class="se3-colum-title"><p>เลขที่บัตรประจำตัวผู้เสียภาษี</p></div>
                    <div class="se3-colum-text"><p><?php echo $data["taxno"]; ?></p></div>
                    <div class="se3-line"></div>
                    <div class="se3-colum-title"><p>บ้านเลขที่</p></div>
                    <div class="se3-colum-text"><p><?php echo $data["receipt_no"]; ?></p></div>
                    <div class="se3-colum-title"><p>หมู่บ้าน / คอนโด / อาคาร</p></div>
                    <div class="se3-colum-text"><p><?php echo $data["receipt_gno"]; ?></p></div>
                    <div class="se3-colum-title"><p>หมู่ที่</p></div>
                    <div class="se3-colum-text"><p><?php echo $data["receipt_moo"]; ?></p></div>
                    <div class="se3-colum-title"><p>ซอย</p></div>
                    <div class="se3-colum-text"><p><?php echo $data["receipt_soi"]; ?></p></div>
                    <div class="se3-colum-title"><p>ถนน</p></div>
                    <div class="se3-colum-text"><p><?php echo $data["receipt_road"]; ?></p></div>
                    <div class="se3-colum-title"><p>ตำบล / แขวง</p></div>
                    <div class="se3-colum-text"><p><?php echo $district_name; ?></p></div>
                    <div class="se3-colum-title"><p>อำเภอ / เขต</p></div>
                    <div class="se3-colum-text"><p><?php echo $amphur_name; ?></p></div>
                    <div class="se3-colum-title"><p>จังหวัด</p></div>
                    <div class="se3-colum-text"><p><?php echo $province_name; ?></p></div>
                    <div class="se3-colum-title"><p>รหัสไปรษณีย์</p></div>
                    <div class="se3-colum-text"><p><?php echo $data["receipt_postcode"]; ?></p></div>
                    <div class="se3-line"></div>
                <?php } ?>
		</div>
	</div>