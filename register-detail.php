<?php
include_once "./share/authen.php";
include_once "./lib/lib.php";
include_once "./share/course.php";
global $db;

$member_info = $_SESSION["login"]["info"];
$member_id = $member_info['member_id'];
$con = " and a.member_id=$member_id and a.active='T'";
$register_id = $_GET["register_id"];
if($register_id){
	$rs = get_register("", $register_id);
	$data = $rs[0];
	$course_id = $data["course_id"];
	$con = " and a.course_id in ($course_id)";
	$r = get_course($con);
	$title = "";
	$parent_id = 0;
	$price = 0;
	foreach ($r as $key => $row) {
		$course_id = $row['course_id'];
		$section_id = $row['section_id'];
		$code = $row['code'];
		$title .= $row['title'].", ";
		$set_time = $row['set_time'];
		$life_time = $row['life_time'];
		$status = $row['status'];	
		$parent_id = (int)$row['parent_id'];
	}

	$title = trim($title, ", ");
	$price = $data["course_price"];
	$discount = $data["course_discount"];

	$row = get_course_detail("", $data["course_detail_id"]);
	if($row) $row = $row[0];
	$course_detail_id = $row['course_detail_id'];
	$course_id = $row['course_id'];
	$day = $row['day'];
	$date = $row['date'];
	$time = $row['time'];
	$address = $row['address'];
	$address_detail = $row['address_detail'];
	$sit_all = $row['sit_all'];
	$chair_all = $row['chair_all'];
	$status = $row['status'];
	$open_regis = $row['open_regis'];
	$end_regis = $row['end_regis'];
	$datetime_add = $row['datetime_add'];
	$datetime_update = $row['datetime_update'];


	$slip_type_text = "";
	if($data["slip_type"]=="individuals") $slip_type_text = "ออกในนามบุคคลธรรมดา";
	if($data["slip_type"]=="corparation") $slip_type_text = "ออกในนามนิติบุคคล (สำหรับเบิกค่าใช้จ่าย)";
	$q = "select name from receipttype where receipttype_id={$data["receipttype_id"]}";
	$receipttype_name = $db->data($q);
	$q = "select name from district where district_id={$data["receipt_district_id"]}";
	$district_name = $db->data($q);
	$q = "select name from amphur where amphur_id={$data["receipt_amphur_id"]}";
	$amphur_name = $db->data($q);
	$q = "select name from province where province_id={$data["receipt_province_id"]}";
	$province_name = $db->data($q);

	$q = "select register_id from register a where a.active='T' and  a.course_id='$course_id'";
	$id = $db->data($q);

	$total_price = $price - $discount;

	?>
	<div class="bgselect4">
			<div class="bodyselect4">
				<br><br>
				<div class="se-title"><center><p>สรุปข้อมูลการลงทะเบียน</p></center></div>
				<div class="se3-title"><p>หลักสูตรที่สมัคร</p></div>
				<div class="se3-colum-title"><p>หลักสูตร</p></div>
				<div class="se3-colum-text"><p><?php echo $title; ?></p></div>
				<div class="se3-line"></div>
				<div class="se3-title"><p>เวลา / ห้อง</p></div>
				<div class="se3-colum-title"><p>วัน</p></div>
				<div class="se3-colum-text2"><div class="se3-colum2-day">
				<center><p><?php echo $day; ?></p></center></div>
				<div class="se3-colum-textday"><p><?php echo $date; ?></p></div></div>	
				<div class="se3-colum-title"><p>เวลา</p></div>
				<div class="se3-colum-text"><p><?php echo $time; ?></p></div>
				<div class="se3-colum-title"><p>สถานที่</p></div>
				<div class="se3-colum-text"><p><?php echo $address_detail; ?></p></div>			
				<div class="se3-line"></div>
				<div class="se3-title"><p>ข้อมูลผู้สมัคร</p></div>
				<div class="se3-colum-title"><p>เลขที่บัตรประจำตัว 13 หลัก</p></div>
				<div class="se3-colum-text"><p><?php echo $data["cid"]; ?></p></div>
				<div class="se3-colum-title"><p>คำนำหน้า - ชื่อ - สกุล</p></div>
				<div class="se3-colum-text">
				<p><?php echo $data["title"]; ?> &nbsp;<?php echo $data["fname"]; ?>&nbsp;&nbsp;<?php echo $data["lname"]; ?> </p></div>		
				<div class="se3-line"></div>
				<div class="se3-title"><p>ข้อมูลสำหรับแสดงบนใบเสร็จรับเงิน</p></div>
				<div class="se3-colum-title"><p>ออกใบเสร็จ</p></div>
				<div class="se3-colum-text"><p><?php echo $slip_type_text; ?></p></div>
				<div class="se3-colum-title"><p>ออกใบเสร็จสำหรับ</p></div>
				<div class="se3-colum-text"><p><?php echo $receipttype_name; ?></p></div>
				<div class="se3-colum-title"><p>เลขที่บัตรประจำตัวผู้เสียภาษี</p></div>
				<div class="se3-colum-text"><p><?php echo $data["taxno"]; ?></p></div>
				<div class="se3-colum-title"><p>ออกใบเสร็จในนาม</p></div>
				<div class="se3-colum-text"><p><?php echo $data["receipt_title"] ; ?>&nbsp;<?php echo $data["receipt_fname"]; ?>&nbsp; <?php echo $data["receipt_lname"]; ?></p></div>
				<div class="se3-line"></div>
				<div class="se3-title"><p>ที่อยู่ (ที่จะแสดงบนใบเสร็จรับเงิน)</p></div>
				<div class="se3-colum-title"><p>บ้านเลขที่</p></div>
				<div class="se3-colum-text"><p><?php echo $data["receipt_no"]; ?></p></div>
				<div class="se3-colum-title"><p>หมู่บ้าน / คอนโด / อาคาร</p></div>
				<div class="se3-colum-text"><p><?php echo $data["receipt_gno"]; ?></p></div>
				<div class="se3-colum-title"><p>หมู่ที่</p></div>
				<div class="se3-colum-text"><p><?php echo $data["receipt_moo"]; ?></p></div>
				<div class="se3-colum-title"><p>ซอย</p></div>
				<div class="se3-colum-text"><p><?php echo $data["receipt_soi"]; ?></p></div>
				<div class="se3-colum-title"><p>ถนน</p></div>
				<div class="se3-colum-text"><p><?php echo $data["receipt_road"]; ?></p></div>
				<div class="se3-colum-title"><p>ตำบล / แขวง</p></div>
				<div class="se3-colum-text"><p><?php echo $district_name; ?></p></div>
				<div class="se3-colum-title"><p>อำเภอ</p></div>
				<div class="se3-colum-text"><p><?php echo $amphur_name; ?></p></div>
				<div class="se3-colum-title"><p>จังหวัด</p></div>
				<div class="se3-colum-text"><p><?php echo $province_name; ?></p></div>
				<div class="se3-colum-title"><p>รหัสไปรษณีย์</p></div>
				<div class="se3-colum-text"><p><?php echo $data["receipt_postcode"]; ?></p></div>
				<div class="se3-line"></div>
		</div>
	</div>
<?php } ?>