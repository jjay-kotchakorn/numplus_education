<?php 
include_once "../lib/lib.php";
// $url = "http://exam1.tsi-thailand.org/info/inforpc0.php";
$url = "http://exam1.set.or.th/info/inforpc0.php";
$info_data = file_get_contents($url);

$info = simplexml_load_string($info_data) or die("Error: Cannot create object");
$items = $info->items[0];

$result = "";
$flag = 'T';

foreach ($items as $item){
	//$date = $item->date;
	if($item->date!=""){
		$arr_data = explode("/", $item->date);
		$date= sprintf("%02d/%02d/%04d", $arr_data[0], $arr_data[1], $arr_data[2]);
	}else{
		$date = "";
	}
	$title = $item->title;
	$doc = $item->doc;
	
	if($flag == 'T'){
		$flag = 'F';
		
		$result	.=	'<div class="table-all">
						<div class="table-hot1"><p>'.$date.'</p></div>
						<div class="table-hot2"><img src="./images/tag_new.png"></div>
						<div class="table-hot3"><p>'.$title.'</p></div>
						<div class="table-hot4"><a href="'.$doc.'" target="_blank">คลิกที่นี่</a></div>
					</div>';
	}
	else
	{
		$result	.=	'<div class="table-all">
						<div class="table-old1"><p>'.$date.'</p></div>
						<div class="table-old2"></div>
						<div class="table-old3"><p>'.$title.'</p></div>
						<div class="table-old4"><a href="'.$doc.'" target="_blank">คลิกที่นี่</a></div>
					</div>';
	}
		
}

echo $result;
?>
