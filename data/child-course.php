<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/course.php";
include_once "../share/datatype.php";
global $db;

$data = array();
$id = $_GET['course_id'];
$msg_c = "";

// Parent Course
$parent = get_course("", $id);
//echo "//" .print_r($parent);

$parent_course_set_time = $parent[0]['set_time'];
$parent_course_price = set_comma($parent[0]['price']);
//

// Child Course
$q = " and a.parent_id in ($id) and a.active='T' and a.inhouse='F' ";
$res = get_course($q, "", true);
$section_id = $res[0]["section_id"];
$coursetype_id = $res[0]["coursetype_id"];
//
$q = "SELECT name FROM `coursetype` WHERE coursetype_id={$res[0]["coursetype_id"]}";
$name_type = $db->data($q);
$column = 2;
$runing = 2;
if($res){
    foreach ($res as  $row) {
        $course_id = $row['course_id'];
        $section_id = $row['section_id'];
        $course_code = $row['code'];
        $course_title = $row['title'];
        $course_title .= "<div class='detail'>".$row['detail']."</div>";
        $course_set_time = $row['set_time'];
        $course_set_time_fa = $row['set_time_fa'];
        $course_life_time = $row['life_time'];
        $course_price = set_comma($row['price']);
        $course_rereg_date = $row['rereg_date'];
        $course_status = $row['status'];
        $row_class = ($runing%$column==0) ? "silver" : "white";

        if($section_id==3 && $coursetype_id==2){
			$tr_row  .= '<tr class="'.$row_class.'">
							<td class="text-center"><input type="checkbox" name="course[]" value="'.$course_id.'" id="course'.$course_id.'" onclick="getMCourse(\'#course'.$course_id.'\'); calSubTotal();"></td>
							<td>'.$course_title.'</td>
							<td class="center">'.$course_set_time.' '.$row["unit_time"].'</td>
							<!-- <td class="center">'.$course_set_time_fa.' '.$row["unit_time_fa"].'</td> -->
							<td class="center"><div id="div-price-'.$course_id.'">'.$course_price.'</div></td>
						 </tr>';  
        }

        if($section_id==3 && $coursetype_id==4){
			$tr_row  .= '<tr class="'.$row_class.'">
							<td class="text-center"><input type="checkbox" name="course[]" value="'.$course_id.'" id="course'.$course_id.'" onclick="getMCourse(\'#course'.$course_id.'\'); calSubTotal();"></td>
							<td>'.$course_title.'</td>
							<td class="center">'.$course_set_time.' '.$row["unit_time"].'</td>
							<td class="center">'.$course_set_time_fa.' '.$row["unit_time_fa"].'</td>
							<td class="center"><div id="div-price-'.$course_id.'">'.$course_price.'</div></td>
						 </tr>';  
        }

        else{        	
			$tr_row  .= '<tr class="'.$row_class.'">
							<td class="text-center"><input type="checkbox" name="course[]" value="'.$course_id.'" id="course'.$course_id.'" onclick="getMCourse(\'#course'.$course_id.'\'); calSubTotal();"></td>
							<td>'.$course_title.'</td>
							<td class="center"></td>
							<td class="center"><div id="div-price-'.$course_id.'">'.$course_price.'</div></td>
						 </tr>';        
        }
		$runing++;
    } 
    $q = "select a.title, a.code from course a where a.course_id in ($id)";
    $rs = $db->rows($q);
    ?>
	<table id="tb-child" width="100%" class="custom-table">
		<thead>					
			<tr class="header">
				<th colspan="5"><center><?php echo $rs["code"]." ".$rs["title"]; ?></center></th>
			</tr>
			<tr class="subheader">
				<td width="5%">เลือก</td>
				<td>ชื่อหลักสูตร</td>
				<?php 
				if($section_id==3 && $coursetype_id==2) { ?>
					<td width="10%" class="center">เวลาในการ<?php echo $name_type;?></td>
					<!-- <td width="10%" class="center">จำนวนชั่วโมงที่นับเป็นการ<?php echo $name_type;?>	<br />เพื่อทบทวนและเพิ่มพูนความรู้ฯ</td> -->
				<?php }
				if($section_id==3 && $coursetype_id==4) { ?>
					<td width="10%" class="center">เวลาในการ<?php echo $name_type;?></td>
					<td width="10%" class="center">จำนวนชั่วโมงที่นับเป็นการ<?php echo $name_type;?>	<br />เพื่อทบทวนและเพิ่มพูนความรู้ฯ</td>	
				<?php }else{ ?>
					<!-- <td width="10%"></td> -->
				<?php } ?>
				<td width="10%" class="center">ราคา (บาท)</td>
			</tr>
			<tr class="white">
				<td width="5%"><input type='checkbox' name='parentCourse' id='parentCourse' onclick="parentCourseClick();" value="<?php echo $id; ?>"></td>
				<td><?php echo $name_type;?>	ทั้งหลักสูตร</td>
				<td width="10%"></td>
				<?php if($section_id==3 && $coursetype_id==2) { ?>
					<!-- <td width="10%"></td> -->
				<?php } ?>
				<?php if($section_id==3 && $coursetype_id==4) { ?>
					<td width="10%"></td>
				<?php } ?>
				<td width="10%" class="center"><?php echo $parent_course_price; ?></td>
			</tr>
		</thead>
		<tbody>				
			<?php echo $tr_row; ?>
		</tbody>
		<tfoot>
			<?php $row_class = ($runing%$column==0) ? "silver" : "white"; $runing++;?>
			<tr class="<?php echo $row_class; ?>">
				<td width="5%"> </td>
				<td></td>
				<?php if($section_id==3 && $coursetype_id==2) { ?>
					<!-- <td width="10%"></td> -->
				<?php } ?>
				<?php if($section_id==3 && $coursetype_id==4) { ?>
					<td width="10%"></td>
				<?php } ?>
				<td width="10%">รวมทั้งสิ้น</td>
				<td width="10%" class="center"><center><p><div id="div-subtotal">0</div></p></center></td>
			</tr>
			<?php $row_class = ($runing%$column==0) ? "silver" : "white"; $runing++;?>			
			<tr class="<?php echo $row_class; ?> center">
				<td colspan="5" style="padding:10px;">
				 	 * หมายเหตุ : กรณีสมัคร<?php echo $name_type;?>	เกินกว่าจำนวน <?php echo $parent_course_price; ?> บาท จะเสียค่าธรรมเนียมเท่ากับการ<?php echo $name_type;?>	ทั้งหลักสูตร <br/>
         		อัตราค่าธรรมเนียมข้างต้นนี้ รวมภาษีมูลค่าเพิ่ม 7% แล้ว
				</td>
			</tr>			
		</tfoot>
	</table>
  
<?php
}else{
	echo "0";
	die();
}
?>

<script type="text/javascript">
	function parentCourseClick()
	{
		if($("#parentCourse").is(':checked'))
		{
			$("#tb-child input[type=checkbox]").not("input[name=parentCourse]").prop("checked", true).attr("disabled", true);
			$("#div-subtotal").html("<?php echo $parent_course_price; ?>");
		}
		else 
		{
		  	$("#tb-child input[type=checkbox]").attr("disabled", false);
			$("#div-subtotal").html("<?php echo $parent_course_price; ?>");
		}
		
		var sList = "";
		$('#tb-child input[type=checkbox]').not("#parentCourse").each(function () {
			var sThisVal = (this.checked ? $(this).val() : "");
			if(sThisVal=="") return;
			sList += (sList=="" ? sThisVal : "," + sThisVal);
		});
		
		$("#my_select_course").val(sList);
	}
	
	function calSubTotal()
	{
		var subtotal = 0;
		
		$("#tb-child td input[type=checkbox]").each(function () {
       		var id = (this.checked ? $(this).val() : "");
			if(id != "")
			{
				var price = parseFloat(($("#div-price-"+id).text()).replace(",", ""));
				subtotal = subtotal + price;
			}
  		});
		
		var maxtotal = parseFloat(("<?php echo $parent_course_price; ?>").replace(",", ""));
		if(subtotal > maxtotal)
		{
			subtotal = maxtotal;
		}
		
		subtotal = commaSeparateNumber(subtotal);
		$("#div-subtotal").html(subtotal);
	}
	
	function commaSeparateNumber(x) {
		var parts = x.toString().split(".");
		parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");

		if(parts.length == 1)
		{
			 parts[1] = "00";
		}

		return parts.join(".");
	}
</script>