<?php
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/datatype.php";
include_once "../share/course.php";

header("Content-type:text/xml; charset=UTF-8");              
header("Cache-Control: no-store, no-cache, must-revalidate");             
header("Cache-Control: post-check=0, pre-check=0", false);  
echo '<?xml version="1.0" encoding="utf-8"?>'; 

// $date = new DateTime('now', new DateTimeZone('Asia/Bangkok'));
// $date_now =  $date->format('Y-m-d');
$date_time_now = date("Y-m-d H:m:s");
$date_now = date("Y-m-d");

$q = "SELECT cd.course_detail_id 
		, cd.end_regis
	FROM course_detail cd
	WHERE cd.active='T' 
		AND cd.section_id=1 
		AND cd.inhouse = 'F' 
		AND cd.date >= '{$date_now}' 
		AND cd.course_id IS NULL
	ORDER BY cd.date, cd.date_phase1 asc";
$cos_dtail_ids = $db->get($q);

	echo "<exam>";
		echo "<items>";

		foreach ($cos_dtail_ids as $key => $v) {
			$id = $v["course_detail_id"];
			$exd = explode(" ", $v["end_regis"]);
			$t_end_regis = $exd[0];
			$date_end = $t_end_regis." 23:59:59";

			$info = get_course_detail("", $id);
			$info = $info[0];
			$status_id = 0;
			$full_address = $info["address_detail"]." ".$info["address"];
			$full_address = str_replace("<u>", "", $full_address);
			$full_address = str_replace("</u>", "", $full_address);

			//switch status
			switch ( trim($info["status"]) ) {
				case 'ปิดรับสมัครชั่วคราว':
					$status_id = 0;
					break;
				case 'เปิดรับสมัคร':
					$status_id = 1;
					break;
				case 'รอบสอบเต็ม':
					$status_id = 2;
					break;
				case 'ยกเลิกรอบสอบ':
					$status_id = 3;
					break;
			}//end sw


			$book_all = 0;
			if($info["book"]>0) {
				$book_all += $info["book_all"];
				//$usebook += get_use_book($course_detail_id);
			}//end if
			// $sit_ava = $info["chair_all"] - $info["sit_all"] - ($info["book"] - $info["book_all"]);
			$sit_ava = $info["chair_all"] - $info["book"] - $info["sit_all"] + $book_all;
			if($sit_ava<0) $sit_ava = 0;

			if ( ($date_end < $date_time_now) && ($status_id==1) ) {
				$info["status"]='ปิดรับ Online';
				$status_id=4;
			}//end if
			if($sit_ava == 0 && $status_id != 4){
				// $status_id = "รอบ{$name_type}เต็ม";
				$status_id = 2;
			}//end if

			echo "<item>";

			echo "<date>".$info["date"]."</date>";
			echo "<time>".$info["time"]."</time>";
			//echo "<location>".$info["address_detail"]." ".$info["address"]."</location>";

			echo "<location>".htmlspecialchars($full_address)."</location>";
			echo "<province>".$info["address"]."</province>";
			echo "<status>".$info["status"]."</status>";
			echo "<status_id>".$status_id."</status_id>";
			echo "<available>".$sit_ava."</available>";
			echo "<url>https://register.ati-asco.org/</url>";

			echo "</item>";
			//break;
		}// end loop

		echo "</items>";
	echo "</exam>";
?>

