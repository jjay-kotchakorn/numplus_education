<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
global $db;
$q = "select data_value from config where name='payment_options_setting'";
$array_data = $db->data($q);
if(!$array_data){
	$array_data = array();
}else{
	$array_data = json_decode($array_data, true);
}

$section_id = $_POST["section_id"];
$sec_action = $_POST["sec_action"];

$option_billpayment = (int) $array_data[$section_id][$sec_action][2]["day"];
$option_paysbuy = (int) $array_data[$section_id][$sec_action][1]["day"];
$option_ati = (int) $array_data[$section_id][$sec_action][3]["day"];
$option_asco = (int) $array_data[$section_id][$sec_action][4]["day"];
$option_mpay = (int) $array_data[$section_id][$sec_action][5]["day"];
/*print_r( $array_data[$section_id][$sec_action]);*/
$pay = $_POST["pay"];
$register_id = $_POST["register_id"];
if($pay=="bill_payment") $d = $option_billpayment;
if($pay=="paysbuy") $d = $option_paysbuy;
if($pay=="at_ati") $d = $option_ati;
if($pay=="at_asco") $d = $option_asco;
if($pay=="mpay") $d = $option_mpay;
if($d==0){
	$d = 2;
}
$t = convert_mktime(date("Y-m-d H:i:s")) + ($d * 24 * 60 * 60);

if($register_id && $pay){
	$args["table"] = "register";
	$args["id"] = $register_id;
	$args["pay"] = $pay;
	// $args["expire_date"] = date("Y-m-d", $t);
	$db->set($args);
}//end if

?>