<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/member.php";
include_once "../share/datatype.php";

$data = array();
$id = $_GET['value'];
$qtype = $_GET['name'];
$output = '<option value="">---- เลือก ----</option>';
if($qtype == 'receipt') {
    if($id!=""){
        $con = " and a.active='T' and a.receipttype_id={$id}";
        $receipt = datatype_sort_name($con, "receipt", true);
        if($receipt>0){
            foreach ($receipt as $key => $v) {
                 //$data[] = array($v["receipt_id"],$v["name"]);
                 $output .= '<option value="'.$v["receipt_id"].'">'.$v["name"].'</option>';
            }
        }
        echo $output;
    }   
}else if($qtype == 'receipt_detail') {
    if($id!=""){    	
 		$receipt = get_receipt('', $id);
        if($receipt>0){
        	foreach ($receipt as $key => $v) {
        		 $data[] = $v;
        	}
        }
    }
    echo json_encode($data);
}
?>