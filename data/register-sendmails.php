<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/course.php";
global $db;

$date_now = date('Y-m-d H:i:s');
$date = date('Y-m-d');

$q = "select data_value from config where name='payment_options_setting'";
$array_data = $db->data($q);
if(!$array_data){
	$array_data = array();
}else{
	$array_data = json_decode($array_data, true);
}

$section_id = $_POST["section_id"];
$sec_action = $_POST["sec_action"];

$option_billpayment = (int) $array_data[$section_id][$sec_action][2]["day"];
$option_paysbuy = (int) $array_data[$section_id][$sec_action][1]["day"];
$option_ati = (int) $array_data[$section_id][$sec_action][3]["day"];
$option_asco = (int) $array_data[$section_id][$sec_action][4]["day"];
/*print_r( $array_data[$section_id][$sec_action]);*/
$pay = $_POST["pay"];
$register_id = $_POST["register_id"];

//test
//$register_id=1552;

$rs = get_register("", $register_id);
$data = $rs[0];
//$pay = $data["pay"];
$ctype_id = (int)$data["coursetype_id"];
$sec_id = (int)$data["section_id"];

if($pay=="bill_payment") $d = $option_billpayment;
if($pay=="paysbuy") $d = $option_paysbuy;
if($pay=="at_ati") $d = $option_ati;
if($pay=="at_asco") $d = $option_asco;
if($d==0){
	$d = 2;
}
$t = convert_mktime(date("Y-m-d H:i:s")) + ($d * 24 * 60 * 60);

if($register_id && $pay){
	if($register_id){
	  /*$rs = get_register("", $register_id);
	  $data = $rs[0];*/
	$q = "SELECT a.member_id, a.password, a.email1, a.email2, a.fname_th, a.cid, a.lname_th FROM member a WHERE a.member_id={$data["member_id"]} and a.active='T'";
	$rowM = $db->rows($q);
	$M_PWD = $rowM['password'];
	$M_EMAIL1 = $rowM['email1'];
	$M_EMAIL2 = $rowM['email2'];	  
	  $sCourse_id = $data["course_id"];
	  $con = " and a.course_id in ($sCourse_id)";
	  $r = get_course($con);
	  $title = "";
	  $parent_id = 0;
	  $price = 0;
	  foreach ($r as $key => $row) {
	    $course_id = $row['course_id'];
	    $section_id = $row['section_id'];
	    $code = $row['code'];
	    $title .= $row['title'].", ";
	    $set_time = $row['set_time'];
	    $life_time = $row['life_time'];
	    $status = $row['status']; 
	    $parent_id = (int)$row['parent_id'];
	  }
	  $q = "select a.course_detail_id from register_course_detail a where a.register_id={$register_id} and a.course_id in ($sCourse_id)";
	  $cd = $db->get($q);

	  $title = trim($title, ", ");
	  $price = $data["course_price"];
	  $discount = $data["course_discount"];
	  if($cd){    
	    $display_date = "";
	    $display_address = "";
	    foreach ($cd as $kk => $vv) {
	      $row = get_course_detail(" and a.course_detail_id={$vv["course_detail_id"]}");
	      if($row) $row = $row[0];
	      $course_detail_id = $row['course_detail_id'];
	      $course_id = $row['course_id'];
	      $day = $row['day'];
	      $date = $row['date'];
	      $time = $row['time'];
	      $display_date .= revert_date($date)."&nbsp; ".$time.", ";
	      $display_address .= $row['address_detail']." ".$row['address'].", ";
	    }

	  }else{
	      $row = get_course_detail(" and a.course_detail_id={$data["course_detail_id"]}");
	      if($row) $row = $row[0];
	      $course_detail_id = $row['course_detail_id'];
	      $course_id = $row['course_id'];
	      $day = $row['day'];
	      $date = $row['date'];
	      $time = $row['time'];
	      $display_date .= revert_date($date)."&nbsp; ".$time.", ";
	      $display_address .= $row['address']." ".$row['address_detail'].", ";
	  }

	  $display_date = trim($display_date, ", ");
	  $display_address = trim($display_address, ", ");
	  $slip_type_text = "";
	  if($data["slip_type"]=="individuals") $slip_type_text = "ออกในนามบุคคลธรรมดา";
	  if($data["slip_type"]=="corparation") $slip_type_text = "ออกในนามนิติบุคคล (สำหรับเบิกค่าใช้จ่าย)";
	  $q = "select name from receipttype where receipttype_id={$data["receipttype_id"]}";
	  $receipttype_name = $db->data($q);
	  $q = "select name from district where district_id={$data["receipt_district_id"]}";
	  $district_name = $db->data($q);
	  $q = "select name from amphur where amphur_id={$data["receipt_amphur_id"]}";
	  $amphur_name = $db->data($q);
	  $q = "select name from province where province_id={$data["receipt_province_id"]}";
	  $province_name = $db->data($q);
	  $q = "select register_id from register a where a.active='T' and  a.course_id='$course_id'";
	  $id = $db->data($q);
	  $total_price = $price - $discount;
	  $t = convert_mktime($data["rectime"]) + (2 * 24 * 60 * 60);
	  $pay_date = $data["expire_date"];
		$payMsg = "";
		if($pay=="bill_payment"){
			$payMsg = '					
					รหัสการลงทะเบียน<br>
					Ref 1 : '.$data["ref1"] .'<br>
					Ref 2 : '.$data["ref2"] .'';
		}else{
			$payMsg = sprintf("%06d", $register_id);
			$payMsg = "รหัสการลงทะเบียน ".$payMsg."";
		}

		$pay_status = "ลงทะเบียนแล้ว รอการชำระเงิน";
		//line pay
		switch ($pay) {
			case 'bill_payment':
				$line_pay = "Bill Payment";
				break;
			case 'paysbuy':
				$line_pay = "Paysbuy";
				$pay_status = "ลงทะเบียนแล้ว <br>สถานะ : รอการชำระเงิน";
				break;
			case 'mpay':
				$line_pay = "mPAY";
				$pay_status = "ลงทะเบียนแล้ว <br>สถานะ : รอการชำระเงิน";
				break;
			case 'at_ati':
				$line_pay = "ATI";
				break;
			case 'at_asco':
				$line_pay = "ASCO";
				break;			
			case 'free':
				$line_pay = "-";
				$pay_status = "ลงทะเบียนแล้ว <br>สถานะ : ยกเว้นค่าธรรมเนียม";
				break;
			
			default:
				# code...
				break;
		}
	}

	/*if($pay=="bill_payment") $d = $option_billpayment;
	if($pay=="paysbuy") $d = $option_paysbuy;
	if($pay=="at_ati") $d = $option_ati;
	if($pay=="at_asco") $d = $option_asco;*/

	/**
	 * This example shows making an SMTP connection with authentication.
	 */

	//SMTP needs accurate times, and the PHP time zone MUST be set
	//This should be done in your php.ini, but this is how to do it if you don't have access to that
	//date_default_timezone_set('Etc/UTC');

	include_once  "../PHPMailer-master/PHPMailerAutoload.php";

	//Create a new PHPMailer instance
	$mail = new PHPMailer;
	//Tell PHPMailer to use SMTP
	$mail->isSMTP();
	//Enable SMTP debugging
	// 0 = off (for production use)
	// 1 = client messages
	// 2 = client and server messages
	$mail->SMTPDebug = 2; 
	//Ask for HTML-friendly debug output

	//$email = $_POST['email'];


	$message = "เรียน คุณ ".$data["fname"]." ".$data["lname"];
	$html = '
	<table class="deviceWidth" align="center" border="0" cellpadding="0" cellspacing="0" width="100%"> 
		<tbody>
			<tr>
				<td class="center" style="font-size: 12px; color: #687074; font-weight: bold; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 20px 10px; " bgcolor="#ffffff">
					'.$message.'
					<br/>
					ทางเราได้รับแจ้งว่าท่านมีความประสงค์ที่จะลงทะเบียนกิจกรรมโดยมีรายละเอียดดังนี้<br><br>
					ข้อมูลการลงทะเบียน<br>
					หลักสูตร : '.$title.'<br>
					วันที่ : '.$display_date.'<br>
					สถานที่ : '.$display_address.'<br>
					ลงทะเบียนเมื่อ : '.revert_date($data["date"], true).'<br>
					ช่องทางชำระ : '.$line_pay.'<br>
					'.$payMsg.'<br>
					สถานะการลงทะเบียน : '.$pay_status.'<br>
					ขอความกรุณาท่านชำระค่าธรรมเนียมภายในวันที่กำหนด<br>
					สถาบันฝึกอบรม สมาคมบริษัทหลักทรัพย์ (ATI)<br>
					ส่วนฝึกอบรมติดต่อ:Training@ati-asco.org โทร 02-264-0909 ต่อ 223<br>
					ส่วนทดสอบผู้แนะนำการลงทุนฯติดต่อ: Examination@ati-asco.orgโทร 02-264-0909 ต่อ 203-207<br>
					ชมรมวาณิชธนกิจ (IB Club) ส่วนฝึกอบรมและทดสอบติดต่อ: fatraining@asco.or.th โทร 02-264-0909 ต่อ 124,125
<br>
				</td>

			</tr>		
		</tbody>
	</table>
	';
	$mail->CharSet = "utf-8";
	$mail->ContentType = "text/html";

	$mail->Debugoutput = 'html'; 
	//Set the hostname of the mail server
	//$mail->Host = "mail.numplus.com"; 
	$mail->Host = "mail.ati-asco.org"; 
	//Set the SMTP port number - likely to be 25, 465 or 587
	$mail->Port = 25; 
	//Whether to use SMTP authentication
	$mail->SMTPAuth = true; 
	//Username to use for SMTP authentication
	//$mail->Username = "ati@numplus.com"; 
	$mail->Username = "register@ati-asco.org"; 
	//Password to use for SMTP authentication
	//$mail->Password = "ati8899"; //Set who the message is to be sent from
	$mail->Password = "Ati_1234"; //Set who the message is to be sent from
	$mail->setFrom('register@ati-asco.org', 'ASCO Training Institute (ATI)'); 
	//Set an alternative reply-to address
	// $mail->addReplyTo('thictyouth@numplus.in.th', 'Koom2'); 
	//Set who the message is to be sent to
	$mail->addAddress($M_EMAIL1, 'You'); 
	if($M_EMAIL2 != ""){
	//Set Sent to CC
		$mail->addCC($M_EMAIL2); 
	}

	// $mail->AddCC('Examination@ati-asco.org');
	// $mail->AddCC('reg_training@ati-asco.org');

	if ( $sec_id==1 && $ctype_id==2 ) {
		$mail->AddCC('Examination@ati-asco.org');
		//$ccto = "Examination";
	}elseif ( ($sec_id==1 && $ctype_id!=2) || ($sec_id==2) ) {
		$mail->AddCC('reg_training@ati-asco.org');
		//$ccto = "reg_training";
	}elseif ($sec_id==3) {
		$mail->AddCC('fatraining@asco.or.th');
		//$ccto = "fatraining";
	}

	//Set the subject line
	$mail->Subject = 'แจ้งข้อมูลการลงทะเบียน';   
	//Read an HTML message body from an external file, convert referenced images to embedded,
	//convert HTML into a basic plain-text alternative body
	$mail->msgHTML($html); 

	//Replace the plain text body with one created manually
	// $mail->AltBody = 'This is a Automatic Mail'; 
	//Attach an image file


	//send the message, check for errors
	unset($_SESSION["reister_info_sendmain"]);
	$rs = $mail->send();
	$msg_error = "";
	if (!$rs) {
	  	$_SESSION["reister_info_sendmain"]["error"] = " มีข้อผิดพลาดเกิดขึ้น กรุณาติดต่อผู้ดูแลระบบ";
	  	$msg_error = $mail->ErrorInfo;
	} else {
	   $_SESSION["reister_info_sendmain"]["msg"] = "ระบบได้ส่ง รหัสผ่านไปยัง Email ของท่านเรียบร้อยแล้ว";
	}
/*	print_r($_SESSION["reister_info_sendmain"]);
	print_r($mail);
	echo $html;*/

	$date_log = date('Y-m-d');
	$str_txt = $date_now;
	$str_txt .= "|register_id={$register_id}|rs={$rs}|msg_error={$msg_error}";
	$str_txt .= "\r\n";

	$file_log = "./log/register-sendmails_{$date_log}.log";
	$file = fopen($file_log, 'a');
	fwrite($file, $str_txt);
	fclose($file);

}
?>