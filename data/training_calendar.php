<?php
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/datatype.php";
include_once "../share/course.php";

header("Content-type:text/xml; charset=UTF-8");              
header("Cache-Control: no-store, no-cache, must-revalidate");             
header("Cache-Control: post-check=0, pre-check=0", false);  
echo '<?xml version="1.0" encoding="utf-8"?>'; 

?>

<?php
		$date = new DateTime('now', new DateTimeZone('Asia/Bangkok'));
		$date_now =  $date->format('Y-m-d');

		$q = "SELECT cd.course_detail_id
				,cd.time 
				,cd.chair_all 
				,cd.status 
				,cd.date
				,cd.diff_date
			    ,c.title
			    ,cd.inhouse
			    ,s.name AS section_id
			    ,ct.name AS coursetype_id
			FROM (
				SELECT cd.course_detail_id
					,cd.time 
			        ,cd.chair_all 
			        ,cd.status 
					,cd.date
			        ,cd.course_id
			        ,cd.inhouse
			        ,cd.section_id
			        ,cd.coursetype_id
					,DATEDIFF(cd.date , CURDATE()) AS diff_date
				FROM course_detail cd
				WHERE cd.active='T' 
					AND cd.section_id <> 3
					AND ( cd.section_id = 2 OR cd.section_id = 1 AND cd.coursetype_id IN (1,3,4) ) 
				) cd
			LEFT JOIN course c ON c.course_id = cd.course_id AND c.inhouse = 'F' 
			LEFT JOIN section s ON s.section_id = cd.section_id
			LEFT JOIN coursetype ct ON ct.coursetype_id = cd.coursetype_id
			WHERE cd.inhouse = 'F' AND cd.diff_date >= 0 AND cd.date >= '$date_now'  
			ORDER BY cd.date asc
			;";
		$list_cos = $db->get($q);

	echo "<exam>";
		echo "<items>";
		foreach ($list_cos as $key => $v) {
			$id = $v["course_detail_id"];
			$info = get_course_detail("", $id);
			$info = $info[0];

			if ($info["book"] == NULL) {
				$info["book"] = 0;
			}
			//$sit_ava = $info["chair_all"] - ($info["sit_all"] + $info["book"]);
			$sit_ava = $info["chair_all"] - $info["sit_all"] - ($info["book"] - $info["book_all"]);

			echo "<item>";

			// echo "<sec>".$v["section_id"]."</sec>";
			// echo "<cty>".$v["coursetype_id"]."</cty>";
			// echo "<id>".$id."</id>";
			// echo "<q>".$q."</q>";
			echo "<date>".$v["date"]."</date>";
			echo "<time>".$v["time"]."</time>";
			echo "<title>".$v["title"]."</title>";
			echo "<status>".$v["status"]."</status>";
			echo "<available>".$sit_ava."</available>";
			echo "<url>https://register.ati-asco.org/</url>";

			echo "</item>";

		}// end loop

		echo "</items>";
	echo "</exam>";
?>

