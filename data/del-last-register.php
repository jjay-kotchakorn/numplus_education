<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
global $db;
$member_info = $_SESSION["login"]["info"];
$member_id = $member_info['member_id'];

$str_tmp = "";

if(isset($_SESSION["last_register_login"][$member_id])){
	$id = $_SESSION["last_register_login"][$member_id];

	$str_tmp .= "register_id={$id}|\r\n";
	// $q = "delete from register where register_id=$id";
	$q = "UPDATE register 
		SET active='F'
			,runno=NULL
			,docno=''
			,remark='มีการเปลี่ยนแปลงข้อมูล active=F โดยการ fix code จากหน้า data/del-last-register'
		WHERE register_id=$id
	";
	$str_tmp .= $q."\r\n";
	if($id) $db->query($q);
	// $q = "delete from register_course where register_id=$id";
	$q = "UPDATE register_course SET active='F' WHERE register_id=$id";
	$str_tmp .= $q."\r\n";
	if($id) $db->query($q);
	// $q = "delete from register_course_detail where register_id=$id";
	$q = "UPDATE register_course_detail SET active='F' WHERE register_id=$id";
	$str_tmp .= $q."\r\n";
	if($id) $db->query($q);
	unset($_SESSION["last_register_login"][$member_id]);

	//write log
	$str_txt = "";
	$str_txt = $str_tmp;
	write_log("del-last-register", $str_txt, "a","./log/");
}//end if

?>