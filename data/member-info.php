<?php 
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/member.php";

$id = (int) $_POST["id"];
$args = array();
if ($_POST) {
   $r = view_member("", $id);
    foreach ($r as $k => $v) {
    	$v["select_university"] = $v["grd_ugrp1"]."-".$v["grd_uid1"];
    	$v["birthdate"] = revert_date($v["birthdate"]);
    	$v["password2"] = $v["password"];
        $args[] = $v;
    }
}

echo json_encode($args);
?>