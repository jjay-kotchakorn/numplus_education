<?php
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/datatype.php";
include_once "../share/course.php";

header("Content-type:text/xml; charset=UTF-8");              
header("Cache-Control: no-store, no-cache, must-revalidate");             
header("Cache-Control: post-check=0, pre-check=0", false);  
echo '<?xml version="1.0" encoding="utf-8"?>'; 

?>

<?php
		$date = new DateTime('now', new DateTimeZone('Asia/Bangkok'));
		$date_now =  $date->format('Y-m-d');
		$q = "SELECT cd.course_detail_id 
			FROM course_detail cd
			WHERE cd.active='T' AND cd.section_id=1 AND cd.inhouse = 'F' AND cd.date >= '$date_now' AND cd.course_id is null
			ORDER BY cd.date asc";
		$cos_dtail_ids = $db->get($q);

	echo "<exam>";
		echo "<items>";

		foreach ($cos_dtail_ids as $key => $v) {
			$id = $v["course_detail_id"];
			$info = get_course_detail("", $id);
			$info = $info[0];
			
			/*
			$q = "SELECT count(r.register_id) as reg
				FROM( SELECT c.course_detail_id
					, c.date
				    , c.open_regis
				    , c.end_regis
					FROM course_detail c
					WHERE c.active='T' AND c.course_detail_id = $id 
						AND '$date_now 23:59:59' BETWEEN c.open_regis AND c.end_regis 
					    AND '$date_now 23:59:59' <= c.date) a
				LEFT JOIN (SELECT r.register_id, $id AS cos_id 
				FROM register r
				WHERE r.active='T' AND r.course_detail_id LIKE '%$id%') r ON r.cos_id = a.course_detail_id
				;";
			
			$sit = $db->get($q);
			$sit = $sit[0];
			$sit_ava = $info["chair_all"] - $sit["reg"];*/

			//$sit_ava = $info["chair_all"] - $info["sit_all"];
			if ($info["book"] == NULL) {
				$info["book"] = 0;
			}

			$sit_ava = $info["chair_all"] - ($info["sit_all"] + $info["book"]);

			echo "<item>";

			echo "<date>".$info["date"]."</date>";
			echo "<time>".$info["time"]."</time>";
			echo "<location>".htmlspecialchars($info["address_detail"]." ".$info["address"])."</location>";
			echo "<status>".$info["status"]."</status>";
			echo "<available>".$sit_ava."</available>";
			echo "<url>https://register.ati-asco.org/</url>";

			echo "</item>";
			//break;
		}// end loop

		echo "</items>";
	echo "</exam>";
?>

