﻿<?php
include_once "./share/authen.php";
include_once "./connection/connection.php";
include_once "./lib/lib.php";
include_once "./share/member.php";

if(isset($_POST)){
	$your_id = $_POST['your_id'];
	$q = "SELECT a.member_id, a.password, a.email1, a.email2, a.fname_th, a.cid, a.lname_th FROM member a WHERE a.username={$your_id} and a.active='T'";
	$row = $db->rows($q);
	if(!$row){
		$_SESSION["forgot"]["error"] = " เลขบัตรประชาชน $your_id ไม่มีอยู่ในระบบ หรือ ถูกระงับการใช้งาน <br> กรุณาติดต่อผู้ดูแลระบบ";	
		redirect_url();	
	}
	$M_PWD = $row['password'];
	$M_EMAIL1 = $row['email1'];
	$M_EMAIL2 = $row['email2'];

	/**
	 * This example shows making an SMTP connection with authentication.
	 */

	//SMTP needs accurate times, and the PHP time zone MUST be set
	//This should be done in your php.ini, but this is how to do it if you don't have access to that
	//date_default_timezone_set('Etc/UTC');

	require 'PHPMailer-master/PHPMailerAutoload.php';

	//Create a new PHPMailer instance
	$mail = new PHPMailer;
	//Tell PHPMailer to use SMTP
	$mail->isSMTP();
	//Enable SMTP debugging
	// 0 = off (for production use)
	// 1 = client messages
	// 2 = client and server messages
	//$mail->SMTPDebug = 2; 
	//Ask for HTML-friendly debug output

	//$email = $_POST['email'];

	$Body = "<html>
	<head>
	    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	    
	</head>
	<body>
	<style type='text/css'>
	    .contrainer{margin: 10px 0 0 10px; }
	    .contrainer tr { font-size: 16px; font-family: 'Tahoma'; line-height: 8px;}
	    .title p { font-size: 18px ; font-family:'Tahoma';  line-height:10px; color: red ;  } 
	</style>
	    <div class='contrainer'>
	    <div class='title'>
	    <p>ระบบกู้รหัสผ่าน ATI</p></div>
		<table width='100%' border='0' cellspacing='0' cellpadding='0' style='width: 250px;'>
	  <tr>
	    <td style='width: 75px;'>Password: </td>
	    <td>$M_PWD</td>
	  </tr>
	</table>
	    </div>
	</body>
	</html>";
	//$Body = htmlspecialchars($Body, ENT_QUOTES);
	$message = "เรียน คุณ ".$row["fname_th"]." ".$row["lname_th"];
	$html = '
	<table class="deviceWidth" align="center" border="0" cellpadding="0" cellspacing="0" width="100%"> 
		<tbody>
			<tr>
				<td class="center" style="font-size: 12px; color: #687074; font-weight: bold; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 20px 10px; " bgcolor="#ffffff">
					'.$message.'
					<br>ข้อมูลในการเข้าสู่ระบบของคุณคือ 
					<br><br>Username : '.$row["cid"].'
					<br>Password : '.$M_PWD.'								 
				</td>
			</tr>
			<tr>
				<td class="center" style="font-size: 12px; color: #687074; font-weight: bold; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 20px 10px; " bgcolor="#ffffff">
					โดยหากท่านต้องการเปลี่ยนรหัสผ่าน สามารถใช้ข้อมูลข้างต้น เข้าสู่ระบบและเลือกเมนูเปลี่ยนรหัสผ่านเพื่อตั้งรหัสผ่านใหม่ได้ตามที่ต้องการ <a style="text-decoration: none; color: #27d7e7;" href="https://register.ati-asco.org/index.php">คลิกที่นี่</a>							 
				</td>
			</tr>
			<tr>
				<td class="center" style="font-size: 12px; color: #687074; font-weight: bold; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 20px 10px; " bgcolor="#ffffff">
					สถาบันฝึกอบรม สมาคมบริษัทหลักทรัพย์ (ATI)<br>
					ส่วนฝึกอบรมติดต่อ:Training@ati-asco.org โทร 02-264-0909 ต่อ 223<br>
					ส่วนทดสอบผู้แนะนำการลงทุนฯติดต่อ: Examination@ati-asco.orgโทร 02-264-0909 ต่อ 203-207<br>
					ชมรมวาณิชธนกิจ (IB Club) ส่วนฝึกอบรมและทดสอบติดต่อ: fatraining@asco.or.th โทร 02-264-0909 ต่อ 124,125
<br>						 
				</td>
			</tr>	
		</tbody>
	</table>
	';

	$mail->CharSet = "utf-8";
	$mail->ContentType = "text/html";

	$mail->Debugoutput = 'html'; 
	//Set the hostname of the mail server
	$mail->Host = "mail.ati-asco.org"; 
	//Set the SMTP port number - likely to be 25, 465 or 587
	$mail->Port = 25; 
	//Whether to use SMTP authentication
	$mail->SMTPAuth = true; 
	//Username to use for SMTP authentication
	$mail->Username = "register@ati-asco.org"; 
	//Password to use for SMTP authentication
	$mail->Password = "Ati_1234"; //Set who the message is to be sent from
	$mail->setFrom('register@ati-asco.org', 'ASCO Training Institute (ATI)'); 
	//Set an alternative reply-to address
	// $mail->addReplyTo('thictyouth@numplus.in.th', 'Koom2'); 
	//Set who the message is to be sent to
	$mail->addAddress($M_EMAIL1, 'You'); 
	if($M_EMAIL2 != ""){
	//Set Sent to CC
		$mail->addCC($M_EMAIL2); 
	}
	$mail->AddCC('Examination@ati-asco.org');
	$mail->AddCC('reg_training@ati-asco.org');
	//Set the subject line
	$mail->Subject = 'ATI - ระบบส่งรหัสผ่าน';   
	//Read an HTML message body from an external file, convert referenced images to embedded,
	//convert HTML into a basic plain-text alternative body
	$mail->msgHTML($html); 

	//Replace the plain text body with one created manually
	// $mail->AltBody = 'This is a Automatic Mail'; 
	//Attach an image file


	//send the message, check for errors
	if (!$mail->send()) {
	  	$_SESSION["forgot"]["error"] = "มีข้อผิดพลาดเกิดขึ้น กรุณาติดต่อผู้ดูแลระบบ";
	} else {
	   $_SESSION["forgot"]["msg"] = "ระบบได้ส่ง รหัสผ่านไปยัง Email ของท่านเรียบร้อยแล้ว";
	}
	redirect_url();
}
die("error");
?>
