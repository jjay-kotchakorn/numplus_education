<?php
/* 
* @Author: bankeyboy
* @Date:   2015-04-01 16:05:19
* @Last Modified by:   bankeyboy
* @Last Modified time: 2015-08-14 15:49:51
*/

$text = file('080115.8');

$array_data = array();
foreach($text as $k=>$v){
	$line = substr($v, 0,1);
	if($line=="H"){
		$record_type = substr($v,0,1);
		$sequence_number = substr($v,1,6);
		$bank_code = substr($v,7,3);
		$company_account = substr($v,10,10);
		$company_name = substr($v,20,40);
		$effective_date = substr($v,60,8);
		$service_code = substr($v,68,8);
		$filler = substr($v,76,180);

		$array_data["H"][]  = array(
			'record_type' => $record_type,
			'sequence_number' => $sequence_number,
			'bank_code' => $bank_code,
			'company_account' => $company_account,
			'company_name' => $company_name,
			'effective_date' => $effective_date,
			'service_code' => $service_code,
			'filler' => $filler
			);

	}else if($line=="D"){
		$record_type = substr($v,0,1);
		$sequence_number = substr($v,1,6);
		$bank_code = substr($v,7,3);
		$company_account = substr($v,10,10);
		$payment_date = substr($v,20,8);
		$payment_time = substr($v,28,6);
		$customer_name = substr($v,34,50);
		$customer_no_ref1 = substr($v,84,20);
		$ref2 = substr($v,104,20);
		$ref3 = substr($v,124,20);
		$branch_no = substr($v,144,4);
		$teller_no = substr($v,148,4);
		$transaction_type = substr($v,152,1);
		$transaction_code = substr($v,153,3);
		$cheque_number = substr($v,156,7);
		$amount = substr($v,163,13);
		$cheque_bank_code = substr($v,176,3);
		$payee_fee_same_zone = substr($v,179,8);
		$payee_fee_diff_zone = substr($v,187,8);
		$filler = substr($v,195,51);
		$new_cheque_no = substr($v,246,10);

		$array_data["D"][] = array(
			'record_type' => $record_type,
			'sequence_number' => $sequence_number,
			'bank_code' => $bank_code,
			'company_account' => $company_account,
			'payment_date' => $payment_date,
			'payment_time' => $payment_time,
			'customer_name' => $customer_name,
			'customer_no_ref1' => $customer_no_ref1,
			'ref2' => $ref2,
			'ref3' => $ref3,
			'branch_no' => $branch_no,
			'teller_no' => $teller_no,
			'transaction_type' => $transaction_type,
			'transaction_code' => $transaction_code,
			'cheque_number' => $cheque_number,
			'amount' => $amount,
			'cheque_bank_code' => $cheque_bank_code,
			'payee_fee_same_zone' => $payee_fee_same_zone,
			'payee_fee_diff_zone' => $payee_fee_diff_zone,
			'filler' => $filler,
			'new_cheque_no' => $new_cheque_no,
			);

	}else if($line=="T"){
		$record_type = substr($v,0,1);
		$sequence_number = substr($v,1,6);
		$bank_code = substr($v,7,3);
		$company_account_number = substr($v,10,10);
		$total_debit_amount = substr($v,20,13);
		$total_debit_transaction = substr($v,33,6);
		$total_credit_amount = substr($v,39,13);
		$total_credit_transaction = substr($v,52,6);
		$filler = substr($v,58,198);

		$array_data["T"][] = array(
			'record_type' => $record_type,
			'sequence_number' => $sequence_number,
			'bank_code' => $bank_code,
			'company_account_number' => $company_account_number,
			'total_debit_amount' => $total_debit_amount,
			'total_debit_transaction' => $total_debit_transaction,
			'total_credit_amount' => $total_credit_amount,
			'total_credit_transaction' => $total_credit_transaction,
			'filler' => $filler,
			);
	}
}
echo "<pre>";
print_r($array_data);
echo "</pre>";

/*foreach ($array_data["D"] as $key => $value) {
		foreach ($value as $x => $d) {
			//print_r($d);
			//
			if($x=="customer_no_ref1"){
				$l = strlen($d);
				echo $l;
				$start = $l-13;
				$d = substr($d, $start, 13);
			}
			echo $x."-".($d)."<br>";
		}
	echo "<hr>";
}*/
/*
$text = file('f.txt');
$line = file('fline.txt');
$t = array();
//echo 'array(';
foreach($text as $k=>$v){
	$x = $line[$k];
	$t = explode(",", $x);
	echo '$'.$v.' = substr($v,'.($t[1]-1).','.($t[0]).');<br>';
	//echo '\''.trim($v).'\' => $'.trim($v).',<br>';
}
//echo ');';

*/


?>
