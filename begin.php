<?php 
$msg = $_SESSION["login"]["msg"];
unset($_SESSION["login"]["msg"]);

// include "maintenance.php";
// die();

?>
<div id="mainbody">
	<div class="homecontainer">
		<div id="loginbox">
			<div class="logintitle">เข้าสู่ระบบสมาชิก</div>
			<div class="login">
				<table width="100%" cellpadding="10">
					<tbody>
						<tr>
							<td class="borderright" valign="top" width="50%"><h5>ยินดีต้อนรับเข้าสู่ระบบสมาชิก</h5>
								<table width="100%" cellpadding="10" style="padding-top:20px;">
									<tbody>
										<tr>
											<td width="50%" align="center" class="borderright"><img src="./images/regisindex2.png"><br/><br/>
												<div class="whitebotton" onClick="javascript: window.open('./pdf/ATI_Manual_Registration.pdf','_blank');">
คู่มือการสมัครสมาชิกใหม่</div></td>
											<td align="center" ><img src="./images/regisindexFA.png"><br/><br/><div class="whitebotton" onClick="javascript: window.open('index.php?p=regis','_self');">สมัครสมาชิกใหม่</div></td>
										</tr>
									</tbody>
								</table>
							</td>
							<td valign="top" class="right">
                            	<h5><img src="./images/log.png">&nbsp;&nbsp;ระบบงานสมาชิก</h5>
								<form action="update-login.php" method="post" name="form_login" id="form_login">
									<div class="righttext1" style="padding-left: 10px;">เลขที่บัตรประชาชน</div>
									<div class="form">
										<input type="text" class="required cart_id" name="f_id_login" maxlength="25" value="" id="f_id_login">
										<img src="./images/idcard.png">
									</div>
									<div class="righttext1" style="padding-left: 10px; margin-left: -26px;">รหัสผ่าน</div>
									<div class="form">
										<input type="password" class="required" name="f_password_login" id="f_password_login" value="">						
										<img src="./images/passindex.png">
									</div>
									<table width="100%" cellpadding="10">
									<tbody>
										<tr>
											<td width="40%">
                                            	<div class="fogotp">
													<a href="#" onClick="javascript: window.open('index.php?p=forget-password','_self');">ลืมรหัสผ่าน</a>
												</div>
                                            </td>
											<td>
                                            	<div class="bluebutton">
													<input type="submit" class="se-botton-next" value="เข้าสู่ระบบ">	
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</form>
						</td>
					</tr>
				</tbody>
			</table>
		</div>

		<hr>
		<div class="" style="">
			<!-- <a href="#" onclick="popup()"><h2>เบราเซอร์ที่รองรับการใช้งาน</h2></a> -->
		</div>

	</div>

	<!-- <br>
	<div id="loginbox">
		
		<table class="browser-support" width="90%" cellpadding="10" align="center">
			<thead>
				<tr>
					<th colspan="2"><h2>สถาบันฝึกอบรม สมาคมบริษัทหลักทรัพย์ไทย</h2></th>
				</tr>
				<tr>
					<th colspan="2" style="text-align: left;">สถาบันฝึกอบรม สมาคมบริษัทหลักทรัพย์ไทย ขอแจ้งรายละเอียดเครื่องคอมพิวเตอร์ที่ผู้สมัครสามารถใช้ในการสมัครสอบ/อบรม ควรมีคุณสมบัติขั้นต่ำดังนี้</th>
				</tr>
				<tr>
					<th colspan="2" style="padding-top: 20px;"><h2>เบราว์เซอร์ขั้นต่ำที่รองรับ</h2></th>
				</tr>
			</thead>
		</table>
		<table class="browser-support" width="90%" cellpadding="10" align="center">	
			<thead style="border: 1px solid black; font-weight: bold;  background-color: #4169E1;
    color: white;">
				<tr>
					<td class="color-boder">Browser</td>
					<td>Minimum Browser Version</td>
				</tr>
			</thead>
			<tbody>
				<tr class="color-boder">
					<td class="color-boder">Internet Explorer</td>
					<td>9</td>		
				</tr>
				<tr class="color-boder">
					<td class="color-boder">Firefox</td>
					<td>32.0</td>		
				</tr>
				<tr class="color-boder">
					<td class="color-boder">Chrome</td>
					<td>31.0</td>		
				</tr>
				<tr class="color-boder">
					<td class="color-boder">Mac Safari</td>
					<td>7.1.8</td>		
				</tr>
				<tr class="color-boder">
					<td class="color-boder">Windows Safari</td>
					<td>5.1.7</td>		
				</tr>
			</tbody>	
			
		</table>	
		
	</div>	 -->

</div>
</div>
<style>
	#f_id_login.error{
		margin-bottom: 24px;
	}
	#f_id_login-error{
		position: absolute;
		display: block;
		color: #F00;
		font-weight: normal;
		font-family: "circularregular";
		font-size: 16px;
		white-space: nowrap;
		margin-left: -6px;
		margin-top: -23px;
		padding: 5px;
	}
	#f_password_login-error{
		position: absolute;
		display: block;
		color: #F00;
		font-weight: normal;
		font-family: "circularregular";
		font-size: 16px;
		white-space: nowrap;
		margin-left: -4px;
		margin-top: -15px;
		padding: 5px;
	}
	.browser-support{
		table-layout: fixed; 
		margin-left: 50px;
		margin-top: 15px;
		margin-bottom: 15px;
	}
	.tbody tr, td{
		text-align: center;
	    /*border: 1px solid black;*/

	}
	h2{
		text-align: center;
		font-size: 20px;
		color: #4264a1;
		margin-bottom: 10px;
	}
	.color-boder{
		border: 1px solid black;
	}
</style>
<script language="javascript" type="text/javascript">
	$(document).ready(function(){
		//$("#form_login").validate();
	});
	var msg = "<?php echo $msg?>";
	if(msg!=""){
		msgError(msg);
		//var options =  $.parseJSON('{"text":"<p>'+msg+'</p>","layout":"center","type":"error"}');
		//noty(options); 
	}

	function popup(){
		var myWindow = window.open("begin-popup.php", "MsgWindow", "width=800,height=600");
    	//myWindow.document.write("<p>test</p>");
	}
</script>