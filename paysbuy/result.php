<?php
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/course.php";
global $db;

// received result from paysbuy with data result,apCode,amt,fee,methos,confirm_cs
$result = $_POST["result"];
$apCode = $_POST["apCode"];
$amt = $_POST["amt"];
$fee = $_POST["fee"];
$method = $_POST["method"];
$paydate = $_POST["payment_date"];
$confirm_cs = strtolower(trim($_POST["confirm_cs"]));

/*
$result = "00000019";
$apCode = "76807";
$amt =  "7276.00";
$fee = "291.0386";
$method = "02";
$confirm_cs = strtolower(trim(""));
Array ( [result] => 00084057 [apCode] => 83827 [amt] => 800.00 [fee] => 30.00 [method] => 01 [create_date] => 4/8/2558 5:52:17 [payment_date] => 4/8/2558 5:52:17 )
*/

$len = strlen($result);
$payment_status = substr($result, 0, 2);
$strInvoice = substr($result, 2, $len-2);
// status result
//00=Success
//99=Fail
//02=Process
        
$register_id = (int) $strInvoice;
$rs = get_register("", $register_id);
$rs = ($rs) ? $rs[0] : "";

$message = "";
$track = "";

if ($payment_status == "00") { 
    if ($method == "06") { 
        if ($confirm_cs == "true") {
            $args["table"] = "register";
            $args["id"] = $register_id;
            $args["pay_method"] = $method;
            $args["pay_price"] = $amt; // จำนวนเงินที่ชำระ
            $args["pay_diff"] = $fee; // ค่าธรรมเนียม;
            $args["pay_id"] = $apCode; // 
            $args["pay_date"] = date("Y-m-d H:i:s", strtotime($paydate)); // 
            $args["pay_status"] = 3; // 
            $docNo = $rs["docno"];
            if($docNo==""){
                $q = "select coursetype_id, section_id from register where register_id=$register_id";
                $v = $db->rows($q);
                $section_id = (int)$v["section_id"];
                $coursetype_id = (int)$v["coursetype_id"];
                $t = rundocno($coursetype_id, $section_id);
                $args = array_merge($args, $t); 
            }
            $db->set($args);         
            echo "Success"; 
        } else if ($confirm_cs == "false") { 
            echo "Fail"; 
        } else {         
            echo "Process"; 
        } 
    } else {
        $args["table"] = "register";
        $args["id"] = $register_id;
        $args["pay_method"] = $method;
        $args["pay_price"] = $amt; // จำนวนเงินที่ชำระ
        $args["pay_diff"] = $fee; // ค่าธรรมเนียม;
        $args["pay_id"] = $apCode; // 
        $args["pay_date"] = thai_to_timestamp($paydate, true); // 
        $args["pay_status"] = 3; // 
            $docNo = $rs["docno"];
            if($docNo==""){
            $q = "select coursetype_id, section_id from register where register_id=$register_id";
            $v = $db->rows($q);
            $section_id = (int)$v["section_id"];
            $coursetype_id = (int)$v["coursetype_id"];
            $t = rundocno($coursetype_id, $section_id);
            $args = array_merge($args, $t); 
        }
        $db->set($args); 
        echo "Success"; 
    } 
} else if ($payment_status == "99") { 
    echo "Fail"; 
} else if ($payment_status == "02") { 
    echo "Process"; 
} else { 
    echo "Error"; 
}
?>
