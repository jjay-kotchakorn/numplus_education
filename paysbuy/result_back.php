<?php
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/course.php";
include_once "pay-status-sendmail-func.php";
global $db;

$date_now = date('Y-m-d H:i:s');
$date = date('Y-m-d');

// received result from paysbuy with data result,apCode,amt,fee,methos,confirm_cs
$result = $_POST["result"];
$apCode = $_POST["apCode"];
$amt = $_POST["amt"];
$fee = $_POST["fee"];
$method = $_POST["method"];
$paydate = $_POST["payment_date"] ? $_POST["payment_date"] : $date_now;
$confirm_cs = strtolower(trim($_POST["confirm_cs"]));

$amt_conv = split(",", $amt);
$amt = null;
foreach ($amt_conv as $key => $v) {
  $amt = $amt."".$v;
}

log_file($_POST, "z_LogFile.log");

/*
$result = "00000019";
$apCode = "76807";
$amt =  "7276.00";
$fee = "291.0386";
$method = "02";
$confirm_cs = strtolower(trim(""));
Array ( [result] => 00084057 [apCode] => 83827 [amt] => 800.00 [fee] => 30.00 [method] => 01 [create_date] => 4/8/2558 5:52:17 [payment_date] => 4/8/2558 5:52:17 )
*/

$len = strlen($result);
$payment_status = substr($result, 0, 2);
$strInvoice = substr($result, 2, $len-2);
// status result
//00=Success
//99=Fail
//02=Process
        
$register_id = (int) $strInvoice;
$rs = get_register("", $register_id);
$rs = ($rs) ? $rs[0] : "";

$message = "";
$track = "";
$status_update = "error";

if ($payment_status == "00") { 
    if ($method == "06") { 
        //if ($confirm_cs == "true") {
			$args["table"] = "register";
			$args["id"] = $register_id;
			$args["pay_method"] = $method;
			$args["pay_price"] = $amt; // จำนวนเงินที่ชำระ
			$args["pay_diff"] = $fee; // ค่าธรรมเนียม;
			$args["pay_id"] = $apCode; // 
			$args["pay_date"] = date("Y-m-d H:i:s", strtotime($paydate)); // 
			//$args["pay_date"] = thai_to_timestamp($paydate, true); // 
			$args["pay_status"] = 3; // 
            $docNo = $rs["docno"];
 			if($docNo==""){
				$q = "select coursetype_id, section_id from register where register_id=$register_id";
				$v = $db->rows($q);
				$section_id = (int)$v["section_id"];
				$coursetype_id = (int)$v["coursetype_id"];
				$t = rundocno($coursetype_id, $section_id);
				$args = array_merge($args, $t);	
			}
			$db->set($args);
			$status_update = "success";         
            echo "Success"; 
/*            
        } else if ($confirm_cs == "false") { 
            echo "Fail"; 
        } else {         
            echo "Process"; 
        }
*/ 
    } else {
		$args["table"] = "register";
		$args["id"] = $register_id;
		$args["pay_method"] = $method;
		$args["pay_price"] = $amt; // จำนวนเงินที่ชำระ
		$args["pay_diff"] = $fee; // ค่าธรรมเนียม;
		$args["pay_id"] = $apCode; // 
		$args["pay_date"] = thai_to_timestamp($paydate, true); // 
		$args["pay_status"] = 3; // 
        $docNo = $rs["docno"];
 		if($docNo==""){
			$q = "select coursetype_id, section_id from register where register_id=$register_id";
			$v = $db->rows($q);
			$section_id = (int)$v["section_id"];
			$coursetype_id = (int)$v["coursetype_id"];
			$t = rundocno($coursetype_id, $section_id);
			$args = array_merge($args, $t);	
		}
		$db->set($args);
		$status_update = "success"; 
        echo "Success"; 
    }

    $this_file = basename($_SERVER["SCRIPT_FILENAME"], '.php');
    register_log_save($register_id, 'update' ,$this_file);

    $array_register[$register_id] = $register_id;
    pay_status_sendmail_func($array_register); 
} else if ($payment_status == "99") { 
    echo "Fail"; 
} else if ($payment_status == "02") { 
	$status_update = "process";
    echo "Process"; 
} else { 
    echo "Error"; 
}

//write result to log
$str_txt = $date_now;
$str_txt .= "|result={$result}|apCode={$apCode}";
$str_txt .= "|method={$method}|paydate={$paydate}|confirm_cs={$confirm_cs}";
$str_txt .= "|amt={$amt}|fee={$fee}|status_update={$status_update}";
$str_txt .= "\r\n";

$file_log = "./log/result_back-{$date}.log";
$file = fopen($file_log, 'a');
fwrite($file, $str_txt);
fclose($file);

/*if ($payment_status == "00") {
	$track .= "1";
    if ($method == "02") {
		$track .= "2";
		
		$args["table"] = "register";
		$args["id"] = $register_id;
		$args["pay_method"] = $method;
		$args["pay_price"] = $amt; // จำนวนเงินที่ชำระ
		$args["pay_diff"] = $fee; // ค่าธรรมเนียม;
		$args["pay_id"] = $apCode; // 
		$args["pay_date"] = $paydate; // 
		$db->set($args);

        //if ($confirm_cs == "true") {
//			$track .= "3";
//            if($rs){
//				$track .= "4";
//                if($register_id){
//					$track .= "5";
//                   // $args["table"] = "register";
////                    $args["id"] = $register_id;
////                    $args["pay_method"] = $method;
////                    $args["pay_price"] = $amt; // จำนวนเงินที่ชำระ
////                    $args["pay_diff"] = $fee; // ค่าธรรมเนียม;
////                    $db->set($args);
//                }
//            }
//            $message = "Success1";
//        } else if ($confirm_cs == "false") {
//			$track .= "6";
//			// Update database
//            $message = "Fail1";
//        } else {
//			$track .= "7";
//			// Update database
//            $message = "Process1";
//        }
    } else {
		$track .= "8";
        if($rs){
			$track .= "9";
            if($register_id){
				$track .= "10";
               // $args["table"] = "register";
//                $args["id"] = $register_id;
//                $args["pay_method"] = $method;
//                $args["pay_price"] = $amt; // จำนวนเงินที่ชำระ
//                $args["pay_diff"] = $fee; // ค่าธรรมเนียม;
//                $db->set($args);
            }
        }
		// Update database
        $message = "Success2";
    }
} else if ($payment_status == "99") {
	$track .= "11";
	// Update database
    $message = "Fail2";
} else if ($payment_status == "02") {
	$track .= "12";
	// Update database
    $message = "Process2";
} else {
	$track .= "13";
	// Update database
    $message = "Error";
}
*/

// -- TEST
 //$File = "log.txt"; 
// if (!file_exists($File))
// {
//	print 'File not found<br/>';
// }
// else
// {
//	  	print 'File found<br/>';
//	  
//	  	if(!$Handle = fopen($File, 'at')) 
//		{
//			print "Can't open file<br/>"; 
//		  	
//	  	}
//	  	else
//	  	{
//			$Data = "result: ".$result.", apCode: ".$apCode.",amt: ".$amt.", fee: ".$fee.", method: ".$method.", confirm_cs: ".$confirm_cs.", track:".$track.", timestamp".date("Y-m-d H:i:s")."\n"; 
//			//$Data = "result : ".$result.", register_id : ".$register_id.", message : ".$message." , timestamp".date("Y-m-d H:i:s")."\n"; 
//			fwrite($Handle, $Data); 
//			  
//			print $Data; 
//			fclose($Handle); 
//		}
// }

?>