<?php
include_once "../share/authen.php";
include_once "../lib/lib.php";
include_once "../connection/connection.php";
include_once "../share/course.php";
include("lib/nusoap.php");
include("lib/config.php");
global $db;

$member_info = $_SESSION["login"]["info"];
$member_id = $member_info['member_id'];
$con = " and a.member_id=$member_id and a.active='T'";
$register_id = $_GET["register_id"];
$order_no = "";
if($register_id){
	$rs = get_register("", $register_id);
	$data = $rs[0];
	$course_id = $data["course_id"];
	$con = " and a.course_id in ($course_id)";
	$r = get_course($con);
	$title = "";
	$parent_id = 0;
	$price = 0;
	foreach ($r as $key => $row) {
		$course_id = $row['course_id'];
		$section_id = $row['section_id'];
		$code = $row['code'];
		$title .= $row['title'].", ";
		$set_time = $row['set_time'];
		$life_time = $row['life_time'];
		$status = $row['status'];	
		$parent_id = (int)$row['parent_id'];
	}
	$title = trim($title, ", ");
	$price = $data["course_price"];
	$discount = $data["course_discount"];
	$row = get_course_detail("", $data["course_detail_id"]);
	if($row) $row = $row[0];
	$course_detail_id = $row['course_detail_id'];
	$course_id = $row['course_id'];
	$day = $row['day'];
	$date = $row['date'];
	$time = $row['time'];
	$address = $row['address'];
	$address_detail = $row['address_detail'];
	$sit_all = $row['sit_all'];
	$chair_all = $row['chair_all'];
	$status = $row['status'];
	$open_regis = $row['open_regis'];
	$end_regis = $row['end_regis'];
	$datetime_add = $row['datetime_add'];
	$datetime_update = $row['datetime_update'];
	$slip_type_text = "";
	if($data["slip_type"]=="individuals") $slip_type_text = "ออกในนามบุคคลธรรมดา";
	if($data["slip_type"]=="corparation") $slip_type_text = "ออกในนามนิติบุคคล (สำหรับเบิกค่าใช้จ่าย)";
	$q = "select name from receipttype where receipttype_id={$data["receipttype_id"]}";
	$receipttype_name = $db->data($q);
	$q = "select name from district where district_id={$data["receipt_district_id"]}";
	$district_name = $db->data($q);
	$q = "select name from amphur where amphur_id={$data["receipt_amphur_id"]}";
	$amphur_name = $db->data($q);
	$q = "select name from province where province_id={$data["receipt_province_id"]}";
	$province_name = $db->data($q);
	$q = "select register_id from register a where a.active='T' and  a.course_id='$course_id'";
	$id = $db->data($q);
	$total_price = $price - $discount;
	$t = convert_mktime($data["rectime"]) + (2 * 24 * 60 * 60);
    $pay_date = date("Y-m-d H:i:s", $t);
}
$email = ($member_info["email1"]) ? $member_info["email1"] : $member_info["email2"];

// Order Data
$inv = sprintf("%06d", $register_id); // เลขที่การสั่งซื้อสินค้า
$itm = $title;  // รายละเอียดสินค้า
$amt = $total_price>0 ? $total_price : "1"; // จานวนเงินรวมที่ต้องการชาระ จานวนต้องมีค่าไม่ต่ากว่า 1
$opt_name = $data["receipt_title"]." ".$data["receipt_fname"]." ".$data["receipt_lname"]; // option : ชื่อของผู้ชาระเงิน
$opt_email = $email;  // option : อีเมล์ของผู้ชาระเงิน
$opt_mobile = $member_info["tel_mobile"];  // option : เบอร์โทรศัพท์ของผู้ชาระเงิน
$opt_address = $data["receipt_no"]." ".$data["receipt_gon"]." ".$data["receipt_moo"]." ".$data["receipt_soi"]. " ".$data["receipt_road"]." ".$province_name." ".$amphur_name." ".$province_name;  // option : ที่อยู่ของผู้ชาระเงิน
$opt_detail = "";  // option : ข้อความเพิ่มเติมของผู้ชาระเงิน

$result = "";

$client = new nusoap_client($url, true);

//1. Step 1 call method api_paynow_authentication
$params = array("psbID"=>$psbID, "username"=>$username, "secureCode"=>$secureCode, "inv"=>$inv, "itm"=>$itm, "amt"=>$amt, "paypal_amt"=>$paypal_amt, "curr_type"=>$curr_type, "com"=>$com, "method"=>$method, "language"=>$language, "resp_front_url"=>$resp_front_url, "resp_back_url"=>$resp_back_url, "opt_fix_redirect"=>$opt_fix_redirect, "opt_fix_method"=>$opt_fix_method, "opt_name"=>$opt_name, "opt_email"=>$opt_email, "opt_mobile"=>$opt_mobile, "opt_address"=>$opt_address, "opt_detail"=>$opt_detail);

$result = $client->call('api_paynow_authentication_new', array('parameters' => $params), 'http://tempuri.org/', 'http://tempuri.org/api_paynow_authentication_new', false, true);

if ($client->getError()) {
    echo "<h2>Constructor error</h2><pre>" . $client->getError() . "</pre>";
} else {
    $result = $result["api_paynow_authentication_newResult"];
}
//echo "<br>result ->".$result;

$approveCode = substr($result,0,2);

//echo "<br>approveCode->".$approveCode;

$intLen = strlen($result);
$strRef = substr($result,2, $intLen-2);

//2. If authentication is successful, then the server responds 00, The process continues redirect to PAYSBUY API Page.
if($approveCode=="00") {
	echo "loadding.....";
	$q = "select register_no from register where register_id=$register_id";
	$register_no = $db->data($q);
	if($register_id){
		$args["table"] = "register";
		$args["id"] = $register_id;
		if(!$register_no) $args["register_no"] = $inv;
		$db->set($args);
	}
    echo "<meta http-equiv='refresh' content='0;url=".$urlRedirect."?refid=".$strRef."'>";
} else {
    echo "<br>Can't login to paysbuy server";
}
?>
