<?php 

function thai_to_timestamp($date, $time=false){
	if(!$date) return 'NULL';
	$a = explode(" ",$date);
	$r = str_replace("/", "-", $a[0]);
	$arr_data = explode("-", $r);
	$arr_data[2] = ($arr_data[2]>2400) ? $arr_data[2]-543 : $arr_data[2];
	$t = (strlen($a[1])>5) ? substr($a[1], 0, 5) : $a[1];
	if($time==true)
		$d = $arr_data[2]."-".$arr_data[1]."-".$arr_data[0]." ".$t;
	else
		$d = $arr_data[2]."-".$arr_data[1]."-".$arr_data[0];
	return $d ;
}
function revert_date($date, $time=false){
	if($date=="")return "";
	if(strlen($date)>19){
		$date = substr($date, 0, 19);
	}
	$a = explode(" ",$date);
	$arr_data = explode("-", $a[0]);
	$arr_data[0] = $arr_data[0]+543;
	if($time==true)
		$d = sprintf("%02d-%02d-%04d", $arr_data[2], $arr_data[1], $arr_data[0])." ".$a[1];
	else
		$d = sprintf("%02d-%02d-%04d", $arr_data[2], $arr_data[1], $arr_data[0]);
	return $d ;
}

function revert_date_slash($date, $time=false){
	if($date=="")return "";
	if(strlen($date)>19){
		$date = substr($date, 0, 19);
	}
	$a = explode(" ",$date);
	$arr_data = explode("-", $a[0]);
	$arr_data[0] = $arr_data[0]+543;
	if($time==true)
		$d = sprintf("%02d/%02d/%04d", $arr_data[2], $arr_data[1], $arr_data[0])." ".$a[1];
	else
		$d = sprintf("%02d/%02d/%04d", $arr_data[2], $arr_data[1], $arr_data[0]);
	return $d ;
}

function set_comma($num, $r=2){
	$sign = ($num<0) ? "-" : "";
	$num = abs($num);
	$num = round(str_replace(",", "", "$num"), $r)."";
	$point = "".round( ( ($num - ((int)$num))*pow(10,$r) ), 0);
	for(;strlen($point)<$r;)
		$point = "0".$point;

	$num = ( (int)$num )."";
	$l = strlen($num);
	$ret = "";
	$c = 0;

	for($i=$l; $i>=1; $i--){
		$c++;
		$ret = $num[$i-1].$ret;
		if($i!=1 && $c==3){
			$ret = ",".$ret;
			$c = 0;
		}
	}
	$ret =  $ret.".$point";
	return ($ret>0 ? $sign : "").$ret;
}

function get_age($birthdate, $str=false, $y=true){
	$aD = array();
	if(!$birthdate) return $aD;
	if(strlen($birthdate)>19){
	   $birthdate = substr($birthdate, 0, 19);
	}
	if(strlen($birthdate)>10){
		$list = explode(" ", $birthdate);
		$birthdate = $list[0];
	}
	$today = date("Y-m-d");
	list($b_year, $b_month, $b_day)= explode("-", $birthdate);
	list($t_year, $t_month, $t_day)= explode("-", $today);
	$m_birthdat_y = mktime(0, 0, 0, $b_month, $b_day, $b_year);
	$m_now = mktime(0, 0, 0, $t_month, $t_day, $t_year);
	$m_age = ($m_now - $m_birthdat_y);
	$arr_data[0] = date("Y", $m_age)-1970;
	$arr_data[1] = date("m", $m_age)-1;
	$arr_data[2] = date("d", $m_age)-1;
	if($str==true){
		$str  = $arr_data[0] ? $arr_data[0]." ปี " : "";
		if($y==true) return $str;
		$str .= $arr_data[1] ? $arr_data[1]." เดือน " : "";
		$str .= $arr_data[2] ? $arr_data[2]." วัน " : "";
		return $str;
	}
	return $arr_data;

}

$thai_day_arr=array("อาทิตย์","จันทร์","อังคาร","พุธ","พฤหัสบดี","ศุกร์","เสาร์");
$thai_month_arr=array(
		"0"=>"",
		"01"=>"มกราคม",
		"02"=>"กุมภาพันธ์",
		"03"=>"มีนาคม",
		"04"=>"เมษายน",
		"05"=>"พฤษภาคม",
		"06"=>"มิถุนายน",
		"07"=>"กรกฎาคม",
		"08"=>"สิงหาคม",
		"09"=>"กันยายน",
		"10"=>"ตุลาคม",
		"11"=>"พฤศจิกายน",
		"12"=>"ธันวาคม"
);

function date_thai($time){
	global $thai_day_arr,$thai_month_arr;
	$thai_date_return = array();
	$thai_date_return[0]= $thai_day_arr[date("w",$time)];
	$thai_date_return[1]= date("d",$time);
	$thai_date_return[2]= $thai_month_arr[date("m",$time)];
	$thai_date_return[3]= (date("Y",$time)+543);	 
	return $thai_date_return;
}

function convert_mktime($input){
	$list = explode(" ",$input);
	$data = explode("-",$list[0]);
	$y = $data[0];
	$m = $data[1];
	$d = $data[2];
	$data2 = explode(':',$list[1]);
	$h = $data2[0];
	$mm = $data2[1];
	$s = $data2[2];
	$value = mktime($h,$mm,$s,$m,$d,$y);
	return $value;
}

function get_num_day($start,$stop){
	$start = convert_mktime($start);
	$stop = convert_mktime($stop);
	$time = $stop - $start;
	$full_day = (int) ( $time/(24*60*60) );
	$remain_hour = ($time - $full_day*(24*60*60))/(60*60);
	$hour=23;
	if($hour && $remain_hour>$hour) $full_day++;
	return $full_day;
}

function get_page_url() {
   //return substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);
   return substr($_SERVER["REQUEST_URI"],strrpos($_SERVER["REQUEST_URI"],"/")+1);
}

function get_config($name, $load=false){
   if(isset( $_SESSION['cache']['configSetting'][$name]) && $load==false){
      return $_SESSION['cache']['configSetting'][$name];
   }else{
      global $db;
      if($db){
         $q = "select data_value from config where name='$name'";
         $data = $db->data($q);
         if(!isset($data)){
            $fd = fopen("/tmp/e_configError.log", "a+");
            fwrite($fd, "Config Name '$name' Not in Table config File {$_SERVER["SCRIPT_FILENAME"]}\n");
            fclose($fd);
         }
         $_SESSION['cache']['configSetting'][$name] = $data;
      }
   }
   return $data;
}

function log_file($str, $file=""){
   if(!$file) $file = "/tmp/z_LogFileDebug.log";
   if(is_array($str)){
      $a = $str;
      $str = "";
      foreach($a as $k=>$v)
         if(!is_numeric($k))
         $str = $str."$k = > $v \n";
      $str = $str."\n";
   }
   fwrite( fopen($file, "a+"), "\n\n$str\n");
}

function gen_array_post($array){
   if(!is_array($array)) return;
   echo "<pre>";
   foreach($array as $k=>$v){
      echo '$args["',$k,'"] = $_POST["',$k,'"];<br>';
   }
}

function ck_mobile(){
   if(preg_match('/(alcatel|amoi|android|avantgo|blackberry|benq|cell|cricket|docomo|elaine|htc|iemobile|iphone|ipad|ipaq|ipod|j2me|java|midp|mini|mmp|mobi|motorola|nec-|nokia|palm|panasonic|philips|phone|playbook|sagem|sharp|sie-|silk|smartphone|sony|symbian|t-mobile|telus|up\.browser|up\.link|vodafone|wap|webos|wireless|xda|xoom|zte)/i', $_SERVER['HTTP_USER_AGENT']))
      return true;
   else
      return false;
}


function gen_passwords ($length = 6){
   // start with a blank password
   $password = "";
   // define possible characters - any character in this string can be
   // picked for use in the password, so if you want to put vowels back in
   // or add special characters such as exclamation marks, this is where
   // you should do it
   $possible = "123467890abcdfghjkmnpqrtvwxyzABCDFGHJKLMNPQRTVWXYZ9876543210";
   //$possible .= $unix;
   // we refer to the length of $possible a few times, so let's grab it now
   $maxlength = strlen($possible);
   // check for length overflow and truncate if necessary
   if ($length > $maxlength){
      $length = $maxlength;
   }
   // set up a counter for how many characters are in the password so far
   $i = 0;
   // add random characters to $password until $length is reached
   while ($i < $length) {
      // pick a random character from the possible ones
      $char = substr($possible, mt_rand(0, $maxlength-1), 1);
      // have we already used this character in $password?
      if (!strstr($password, $char)) {
         // no, so it's OK to add it onto the end of whatever we've already got...
         $password .= $char;
         // ... and increase the counter by one
         $i++;
      }
   }
   // done!
   return $password;

}

function unix_time($time){
   $time = explode(" ", $time);
   $d = explode("-", $time[0]);
   $t = explode(":", $time[1]);
   $ret = mktime((int)$t[0], (int)$t[1], (int)$t[2], (int)$d[1], (int)$d[2], (int)$d[0]);
   return $ret;
}

function get_median($arr_data){
   if(!is_array($arr_data)) return "N/A";
   $total = count($arr_data);
   $middle = floor(($total+1) / 2)-1;
   $ck_number = ($total)%2;
   $median = 0;
   if($ck_number==0){
      $low = $arr_data[$middle];
      $high = $arr_data[$middle + 1];
      $median = round(($low + $high) / 2);
   }else{
      $median = $arr_data[$middle];
   }
   return $median;
}

function get_mode(array $arr_data){
   $counts = array_count_values($arr_data);
   arsort($counts); // Sort counts in descending order
   $modes  = array_keys($counts, current($counts), TRUE);
   // If each value only occurs once, there is no mode
   if (count($arr_data) === count($counts))
      return FALSE;
   // Only one modal value
   if (count($modes) === 1)
      return $modes[0];
   // Multiple modal values
   return $modes;
}

function redirect_url($params="", $clear_arg=false, $arr_clear_arg=false){
	if( strpos($_SERVER["HTTP_REFERER"], "http://")!==false){
		$url = substr($_SERVER["HTTP_REFERER"], strpos($_SERVER["HTTP_REFERER"], "/", 15));
	}else{
		$url = $_SERVER["HTTP_REFERER"];
	}

	$arg = array();
	if(strpos($url, "?")!==false){
		$urlRef = $url;
		$url = substr($url, 0, strpos($url, "?")+1);
		$sArg = substr($urlRef, strpos($urlRef, "?")+1);
		while(strpos($sArg, "&&")!==false)
			$sArg = str_replace("&&", "&", $sArg);
		$sArg = explode("&", $sArg);
		foreach($sArg as $index=>$val){
			$aTmp = explode("=", $val, 2);
			$arg[$aTmp[0]] = $aTmp[1];
		}
		$arg_not_convert = $arg;
	}else{
		$url = "$url?";
	}

	if($params){
		foreach($params as $index=>$val){
			$arg[$index] = $val;
		}
	}

	if($arr_clear_arg){
		foreach($arr_clear_arg as $name)
			if($name) unset($arg[$name]);
	}
	if($clear_arg===true){
		$url = str_replace("?", "", $url);
	}else{
		foreach($arg as $index=>$val){
			if(!$index) continue;
			if($arg_not_convert[$index]==$val)
				$url .= "&$index=".($val);
			else
				$url .= "&$index=".urlencode($val);
		}
	}
	header("Location: $url");
	exit();
}

function render_pages($call_page){
	if($call_page){
		include_once ("$call_page");
	}	
}
function digit_name($i){
   $i = (int)$i;
   switch($i){
      case 1 : return "หนึ่ง";
      case 2 : return "สอง";
      case 3 : return "สาม";
      case 4 : return "สี่";
      case 5 : return "ห้า";
      case 6 : return "หก";
      case 7 : return "เจ็ด";
      case 8 : return "แปด";
      case 9 : return "เก้า";
   }
}

function number_letter($n){
   $int = (int)$n;
   if($int==0) return "";
   $int = "$int";
   $len = strlen($int);
   if($len<=6) return convert_number_name($int)."บาทถ้วน";
   else {
      $first = substr($int, 0, $len-6 );
      $last = substr($int, $len-6 );
      return convert_number_name( $first )."ล้าน".convert_number_name( $last )."บาทถ้วน";;
   }
}

function convert_number_name($n){
   $ret = "";
   $len = strlen($n);
   for($i=$len; $i>=1; $i--){
      $di = $n[$len-$i];
      $ldi = $n[$len-$i-1];
      if($di=="0") continue;
      switch($i){
         case 6 : $unit = "แสน"; break;
         case 5 : $unit = "หมื่น"; break;
         case 4 : $unit = "พัน"; break;
         case 3 : $unit = "ร้อย"; break;
         case 2 : $unit = "สิบ"; break;
         case 1 : $unit = ""; break;
      }
      $name = "";
      if($unit=="สิบ" && $di=="1"){
         $name = "";
         $ret = $ret.$name.$unit;
         continue;
      }else if($unit=="สิบ" && $di=="2"){
         $name = "ยี่";
      }else if($unit=="" && $di=="1" && $i==1 && $ret){
         $name = "เอ็ด";
      }
      if(!$name) $name = digit_name($di);
      $ret = $ret.$name.$unit;
   }
   return $ret;
}

function thai_number($num){
	if(!$num) return "";
    return str_replace(array( '0' , '1' , '2' , '3' , '4' , '5' , '6' ,'7' , '8' , '9' ),
	array( "o" , "๑" , "๒" , "๓" , "๔" , "๕" , "๖" , "๗" , "๘" , "๙" ),
    $num);
}

function sendmail($to_email, $subject, $message){
	include_once "mailSetting.php";
	ini_set("SMTP", $SMTP_HOST);								
	ini_set("smtp_port", $SMTP_PORT);									
	ini_set("sendmail_from", $SMTP_USER);	
	//ini_set("password", $SMTP_PASSWORD);	
	$headers = 'From: '.$MAIL_FROM_DISNPLAY_NAME.'<'. $MAIL_FROM . ">\r\n" .
		'Reply-To: '.$SMTP_USER."\r\n" .
		'X-Mailer: PHP/' . phpversion(). "\r\n";
	$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
	$headers .= 'CC: Examination@ati-asco.org, reg_training@ati-asco.org' . "\r\n";
	$content = '
	<html>
	<head>
		<title>'.$title.'</title>
	</head>
	<body>
		'.$message.'
	</body>
	</html>';
	try {  
		if(mail($to_email, $subject, $content, $headers)) {
			return "Successfully";	
		}else{
			return "Failed";
		}
	}catch (Exception $e) {  
		return "Error : " . $e;
	}  
}

function get_select_course($section_id="", $sec_action){
	global $db;
	$t = get_config("select_course_setting", true);
	$r = json_decode($t, true);

	if($section_id && $sec_action){
		$section = $r[$section_id];
		return $section[$sec_action];
	}else{
		return 1;
	}
}

/*5810121100012*/
function check_digit($number) {
    if(strlen($number) != 13) return false;
    for($i=0, $sum=0; $i<12;$i++)
        $sum += (int)($number{$i})*(13-$i);
    if((11-($sum%11))%10 == (int)($number{12}))
        return true;
    return false;
}

function check_digit2($number) {
    if(strlen($number) != 12) return false;
    for($i=0, $sum=0; $i<11;$i++)
        $sum += (int)($number{$i})*(12-$i);
    if((10-($sum%10))%9 == (int)($number{11}))
        return true;
    return false;
}

function gen_digit($number) {
    if(strlen($number) != 13) return false;
    for($i=0, $sum=0; $i<12;$i++)
        $sum += (int)($number{$i})*(13-$i);
    $digit =  (11-($sum%11))%10;
    return str_replace("N", $digit, $number);
}


function gen_digit2($number) {
    if(strlen($number) != 12) return false;
    for($i=0, $sum=0; $i<11;$i++)
        $sum += (int)($number{$i})*(12-$i);
    $digit =  (10-($sum%10))%9;
    return str_replace("N", $digit, $number);
}

function rundocno($coursetype_id, $section_id){
	global $db;
    $y = date("y")+43;    
    if($section_id==3){
    	$prefix = "AT";
    }
	//}else if($section_id==1 && $coursetype_id==2) {
	if($section_id==1 && $coursetype_id==2) {	
    	$prefix = "AE";
    }else{
    	$prefix = "AT";
    }
    $q = "select max(runno) from register where runyear='$y' and doc_prefix='$prefix' ";
    $runno = $db->data($q) + 1;
    $args["runyear"] = $y;
    $args["runno"] = $runno;
    $args["doc_prefix"] = $prefix;
    $args["docno"] = "{$prefix}{$y}/" . sprintf("%06d", $runno);	
    return $args;
}

function d($data){
	echo "<pre>";
	print_r($data);
	echo "</pre>";
	//die();
}

function write_log($file_name="", $msg="", $write_mode="a", $dir_log="../log/"){
	$str = $msg;	
	$date_now = date('Y-m-d H:i:s');
	$date_log = date('Y-m-d');
	$log_name = $dir_log.$file_name."_".$date_log.".log";
	$file = fopen($log_name, $write_mode);
	$str_txt = $date_now."|".$str."\r\n";
	fwrite($file, $str_txt);
	fclose($file);
}//end func

function call_day($date, $month, $year_en){
	$first = 1; 
	$last = cal_days_in_month(CAL_GREGORIAN, date($month), date($year_en));
    $cur = strtotime(date($month) . "/" . $date . "/" . date($year_en));	 
    $cur_day = date("l", $cur);
    switch ($cur_day) {
    	case 'Sunday':
    		$day_th = 'อา';
    		break;
    	case 'Monday':
    		$day_th = 'จ';
    		break;
    	case 'Tuesday':
    		$day_th = 'อ';
    		break;
		case 'Wednesday':
    		$day_th = 'พ';
    		break;
		case 'Thursday':
    		$day_th = 'พฤ';
    		break;
    	case 'Friday':
    		$day_th = 'ศ';
    		break;
    	case 'Saturday':
    		$day_th = 'ส';
    		break;
    }//end sw
	//var_dump($target_day);
	return $day_th;
}//end func
?>
