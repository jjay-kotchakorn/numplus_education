<?php 
include_once "./share/authen.php";
include_once "./lib/lib.php";
include_once "./connection/connection.php";
include_once "./share/course.php";
global $db;

$member_info = $_SESSION["login"]["info"];
$member_id = $member_info['member_id'];
$con = " and a.member_id=$member_id and a.active='T'";
$register_id = $_GET["register_id"];
if($register_id){
  $rs = get_register("", $register_id);
  $data = $rs[0];
  $pay_by = $data["pay"];

  //write log
/*  
  $str = "register_id=".$register_id."|pay=".$pay_by."|";
  $str .= "ref1=".$data["ref1"]."|ref2=".$data["ref2"];
*/
  $str = "";
  foreach ($data as $key => $v) {
    $str .= "{$key}={$v}|";
  }//end loop $v

  if ( empty($pay_by) ) {
    $args = array();
    $args["table"] = "register";
    $args['id'] = $register_id;
    $args["pay"] = "bill_payment";
    $db->set($args);

    $str .= "=====>";
    foreach ($args as $key => $v) {
      $str .= "{$key}={$v}|";
    }//end loop $v
    unset($args);
  }//end if

  $date_now = date('Y-m-d H:i:s');
  $date_log = date('Y-m-d');
  $log_name = "./log/invoice_".$date_log.".log";
  $file = fopen($log_name, 'a');
  $str_txt = $date_now."|".$str."\r\n";
  fwrite($file, $str_txt);
  fclose($file);


  $sCourse_id = $data["course_id"];
  $con = " and a.course_id in ($sCourse_id)";
  $r = get_course($con);
  $title = "";
  $parent_id = 0;
  $price = 0;
  foreach ($r as $key => $row) {
    $course_id = $row['course_id'];
    $section_id = $row['section_id'];
    $code = $row['code'];
    $title .= $row['title'].", ";
    $set_time = $row['set_time'];
    $life_time = $row['life_time'];
    $status = $row['status']; 
    $parent_id = (int)$row['parent_id'];
  }
  $q = "select a.course_detail_id from register_course_detail a where a.register_id={$register_id} and a.course_id in ($sCourse_id)";
  $cd = $db->get($q);

  $title = trim($title, ", ");
  $price = $data["course_price"];
  $discount = $data["course_discount"];
  if($cd){    
    $display_date = "";
    $display_address = "";
    foreach ($cd as $kk => $vv) {
      $row = get_course_detail(" and a.course_detail_id={$vv["course_detail_id"]}");
      if($row) $row = $row[0];
      $course_detail_id = $row['course_detail_id'];
      $course_id = $row['course_id'];
      $day = $row['day'];
      $date = $row['date'];
      $time = $row['time'];
      $display_date .= revert_date($date)."&nbsp;เวลา ".$time.", ";
      $display_address .= $row['address_detail']." ".$row['address'].", ";
    }

  }else{
      $row = get_course_detail(" and a.course_detail_id={$data["course_detail_id"]}");
      if($row) $row = $row[0];
      $course_detail_id = $row['course_detail_id'];
      $course_id = $row['course_id'];
      $day = $row['day'];
      $date = $row['date'];
      $time = $row['time'];
      $display_date .= revert_date($date)."&nbsp;เวลา ".$time.", ";
      $display_address .= $row['address']." ".$row['address_detail'].", ";
  }

  $display_date = trim($display_date, ", ");
  $display_address = trim($display_address, ", ");
  $slip_type_text = "";
  if($data["slip_type"]=="individuals") $slip_type_text = "ออกในนามบุคคลธรรมดา";
  if($data["slip_type"]=="corparation") $slip_type_text = "ออกในนามนิติบุคคล (สำหรับเบิกค่าใช้จ่าย)";
  $q = "select name from receipttype where receipttype_id={$data["receipttype_id"]}";
  $receipttype_name = $db->data($q);
  $q = "select name from district where district_id={$data["receipt_district_id"]}";
  $district_name = $db->data($q);
  $q = "select name from amphur where amphur_id={$data["receipt_amphur_id"]}";
  $amphur_name = $db->data($q);
  $q = "select name from province where province_id={$data["receipt_province_id"]}";
  $province_name = $db->data($q);
  $q = "select register_id from register a where a.active='T' and  a.course_id='$course_id'";
  $id = $db->data($q);
  $total_price = $price - $discount;
  $t = convert_mktime($data["rectime"]) + (2 * 24 * 60 * 60);
  $expire_date = $data["expire_date"];
}

 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bill Payment</title>
<link rel="stylesheet" type="text/css" media="print, handheld" href="./css/print.css" />
<link href="./css/style.css" rel="stylesheet" type="text/css" />
</head>
<style>
    @media print{
      .buttonb,
      #btnPrint{
        display: none;
      }
    }
    .buttonb1 {
      background: linear-gradient(to bottom, #6580b5 0%, #6580b5 56%, #6580b5 56%, #4264a1 56%, #4264a1 100%);
      margin-top: 20px;
      margin-bottom: 20px;
      float: left;
      border-radius: 5px;
      color: #FFF;
      padding: 10px;
      font-size: 20px;
    }
    .buttonb1:hover {
      background: linear-gradient(to bottom, #6580b5 0%, #6580b5 56%, #6580b5 56%, #254487 56%, #254487 100%);
      cursor: pointer;
    }
    body{
      padding: 0px;
    }
    .container{
      width: 1000px;
    }
    .billinfo{
      padding-top: 25px;
      padding-bottom: 10px;
    }
    .bill1 table, .bill2 table{
      font-size: 18px;
    }
</style>
<body>
<div class="container">
<div class="billinfo">
<h5>แบบฟอร์มชำระค่าธรรมเนียม</h5>
<?php if ($section_id == 3){ ?>
  <script>
      if(pay_type=="at_ati") url = 'invoice-ati.php?register_id='+<?php echo $register_id; ?>;
      window.open(url,'_blank');;
  </script>

<?php

			//header("Location: invoice-ati.php?register_id=".$register_id);
	 } else { ?>
      <ol>
      <li>ผู้สมัครต้องดำเนินการชำระค่าธรรมเนียมภายในระยะเวลาที่ปรากฏในแบบฟอร์มชำระค่าธรรมเนียม หากไม่ชำระภายในระยะเวลาที่กำหนด ระบบจะยกเลิกการสมัครของท่านโดยอัตโนมัติ</li>
      <li>ห้ามชำระค่าธรรมเนียมหลังกำหนดเวลาที่กำหนดไว้ในแบบฟอร์มชำระค่าธรรมเนียม หรือชำระค่าธรรมเนียมโดยวิธีอื่น เนื่องจากจะทำให้เกิดความผิดพลาด และความไม่สมบูรณ์ในการสมัคร</li>
      <li>หลังจากที่ท่านชำระค่าธรรมเนียมแล้ว สถาบันฯ จะมี E-mail แจ้งยืนยันการรับชำระค่าธรรมเนียมไปยังท่านภายในวันถัดไป นอกจากนี้ ท่านสามารถตรวจสอบสถานะการสมัครของท่านผ่านระบบลงทะเบียนได้ด้วยตนเอง (ภายใน 3 วันนับจากวันที่ท่านชำระค่าธรรมเนียม)</li>
      <li>กรณีที่ผู้สมัครไม่ปฏิบัติตามระเบียบการและข้อกำหนดข้างต้น อันเป็นเหตุให้เกิดความผิดพลาดในการสมัคร ทางสถาบันฯ และ ธนาคาร จะไม่รับผิดชอบต่อความเสียหายที่เกิดขึ้น</li>
      <li>กรุณานำหลักฐานการชำระค่าธรรมเนียม พร้อมกับบัตรประจำตัวประชาชน หรือ ใบขับขี่ หรือ หนังสือเดินทาง มาแสดงในวันสอบ เพื่อที่สถาบันฯ จะทำการออกใบเสร็จรับเงินที่ถูกต้องตามกฎหมายให้ผู้สมัครในวันสอบ</li>
      <li>สถาบันฯ ขอสงวนสิทธิ์ไม่คืนเงินแก่ผู้สมัครไม่ว่ากรณีใดๆ</li>
      </ol>
<?php } ?>
</div>
<div class="cut"></div>
<div class="bill1">
<div class="header">
<table width="100%">
<tbody>
<tr>
<td width="50%"><img src="images/logo1.png"/></td>
<!-- <td width="50%"><img src="images/logo2.png"/></td> -->
<td align="right">ส่วนที่ 1 ส่วนของลูกค้า</td>
</tr>
</tbody>
</table>
</div>
<table width="100%" cellpadding="10">
<tbody>
<tr>
<td class="gray">วันสอบ/อบรม</td>
<td class="fix-tdline" width="50%"><?php echo $display_date; ?></td>
<td class="gray">วันที่สมัคร</td>
<td><?php echo revert_date($data["rectime"]); ?></td>
</tr>
<tr>
<td class="gray">ผู้สมัครสอบ/อบรม</td>
<td class="fix-tdline" width="50%"><?php echo $data["title"]; ?> &nbsp;<?php echo $data["fname"]; ?>&nbsp;&nbsp;<?php echo $data["lname"]; ?></td>
<td class="gray">ชำระเงินภายในวันที่</td>
<td><?php echo revert_date($expire_date); ?></td>
</tr>
<tr>
<td class="gray">สถานที่สอบ/อบรม</td>
<td class="fix-tdline" width="50%"><?php echo $display_address; ?></td>
<td class="gray">จำนวนเงินที่ต้องชำระ</td>
<td><?php echo set_comma($data["course_price"]-(int)$data["course_discount"]); ?> บาท</td>
</tr>
<tr>
<td rowspan="2" valign="top" class="gray">หลักสูตรที่สอบ/อบรม</td>
<td class="fix-tdline" width="50%" rowspan="2" valign="top"><?php echo $title; ?></td>
<td class="gray">Ref 1</td>
<td><?php echo $data["ref1"] ?></td>
</tr>
<tr>
<td class="gray">Ref 2</td>
<td><?php echo $data["ref2"] ?></td>
</tr>
</tbody>
</table>
</div>
<div class="cut"></div>
<div class="bill2">
<div class="header">
<table width="100%">
<tbody>
<tr>
<td width="50%"><img src="images/logo1.png"/></td>
<!-- <td width="50%"><img src="images/logo2.png"/></td> -->
<td align="right">ส่วนที่ 2 ส่วนของธนาคาร</td>
</tr>
</tbody>
</table>
</div>
<div class="choose">

    <p>
     <table cellpadding="5" class="noborder">
     <tbody>
     <tr>
     <td><img src="images/circle.jpg" /></td>
       <td><img src="images/bbl.jpg" /></td>
         <td> บมจ.ธนาคารกรุงเทพ     Comp Code : <span class="blue">92601</span>  /  Service Code : <span class="blue">ASCO</span></td>
     </tr>
       <tr>
     <td><img src="images/circle.jpg" /></td>
       <td> <img src="images/kbank.jpg" /></td>
         <td> บมจ.ธนาคารกสิกรไทย   Comp Code : <span class="blue">35358</span> / เลขที่บัญชี : <span class="blue">718-1-050521</span></td>
     </tr>
     </tbody>
     </table>
    </p>
  
</div>

<table width="100%" cellpadding="10">
<tbody>
<tr>
<td class="gray">ชื่อสมาชิก / Member</td>
<td class="fix-tdline" width="50%"><?php echo $data["title"]; ?> &nbsp;<?php echo $data["fname"]; ?>&nbsp;&nbsp;<?php echo $data["lname"]; ?></td>
<td class="gray">วันที่ / Date</td>
<td><?php //echo revert_date($data["rectime"]); ?></td>
</tr>
<tr>
<td class="gray">Ref 1</td>
<td><?php echo $data["ref1"] ?></td>
<td class="gray"></td>
<td></td>
</tr>
<tr>
<td class="gray">Ref 2</td>
<td><?php echo $data["ref2"] ?></td>
<td class="gray"></td>
<td></td>
</tr>

</tbody>
</table>
<div class="choose">

       <table cellpadding="5" class="noborder">
     <tbody>
     <tr>
     <td><img src="images/circle.jpg" /></td>
       <td>หมายเลขเช็ค _______________________ ธนาคาร __________________ สาขา __________________ เช็คลงวันที่ _________________</td>
     </tr>
       <tr>
     <td><img src="images/circle.jpg" /></td>
       <td>เงินสด</td>
     </tr>
     </tbody>
     </table>
 

  </div>
  <table width="80%" cellpadding="10" align="center" class="moneyresult">
<tbody>
<tr>
<th width="70%" style="border-right:1px solid #c9c9c9;">จำนวนเงินตัวอักษร</th>
<th>จำนวนเงินตัวเลข (บาท)</th>
</tr>
<tr>
<td style="border-right:1px solid #c9c9c9;"><?php echo number_letter($data["course_price"]-(int)$data["course_discount"]); ?></td>
<td><?php echo set_comma($data["course_price"]-(int)$data["course_discount"]); ?></td>
</tr>
</tbody>
</table>
</div>

<div class="row">
  <div class="col-md-6">
    <div id="btnHome" onclick="redirect_home()" class="buttonb1 homebutton">
      กลับหน้าแรก
    </div>
  </div>
  <div class="col-md-6">
    <div id="btnPrint" onclick="PrintElem()" class="buttonb printbutton">
     <img src="images/print.png" /> พิมพ์ใบ Pay-in Slip
   </div>
 </div>
</div>

<script type="text/javascript">
	function PrintElem()
	{
		window.print();
	}

  function redirect_home(){
    window.open('./index.php?p=main','_self');
  }
</script>

</div>

</body>
</html>
